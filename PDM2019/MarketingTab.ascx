<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MarketingTab.ascx.cs" Inherits="YourCompany.Modules.PDM2019.MarketingTab" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;    }
    .readonly { background-color:#e8e2e2; }
   
</style>

<script language="javascript" type="text/javascript">
    function ToggleMarketingMore(toggleWhat) {
        var link = jQuery('[id*="lnkMarketingViewMore' + toggleWhat + '"]');
        var lbl = jQuery('[id*="lblMarketing' + toggleWhat + '"]');


        if (link.text() == "View More") {
            link.text("View Less");
            lbl.css("max-height", "");
        }
        else {
            link.text("View More");
            lbl.css("max-height", "200px");
        }
        return false;
    }
</script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>

    <asp:Panel id="pnlMarketing" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead" valign="top">Copy:</td>
                <td style="width:600px; border:solid 1px #e8e2e2;padding:2px;" colspan="4">
                    <asp:Label id="lblMarketingCopy" runat="server" CssClass="Normal" Table="vwMarketingInfos" Field="Copy" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkMarketingViewMoreCopy" runat="server" Text="View More" OnClientClick="javascript:return ToggleMarketingMore('Copy');" /></div>
                </td>
            </tr>

            <tr>
                <td class="SubSubHead" valign="top">Description:</td>
                <td colspan="4" valign="top"><asp:Label ID="lblInspectionNotes" runat="server" CssClass="Normal placeholder" Table="Parts" Field="Description" Width="580" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Product Categories:</td>
                <td colspan="4" class="SubSubHead">1:&nbsp;<asp:Label ID="lblProductCategory1" runat="server" CssClass="Normal" Table="vwMarketingInfos" Field="ProductCategory1" /></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="4" class="SubSubHead">2:&nbsp;<asp:Label ID="Label1" runat="server" CssClass="Normal"  Table="vwMarketingInfos" Field="ProductCategory2" /></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="4" class="SubSubHead">3:&nbsp;<asp:Label ID="Label2" runat="server"  CssClass="Normal" Table="vwMarketingInfos" Field="ProductCategory3" /></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="4" class="SubSubHead">4:&nbsp;<asp:Label ID="Label3" runat="server" CssClass="Normal" Table="vwMarketingInfos" Field="ProductCategory4" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Sub Brand:</td>
                <td colspan="4" class="SubSubHead">NOT YET IMPLEMENTED. Need Specs:<asp:Label ID="lblSubBrand" runat="server" CssClass="Normal" Table="vwMarketingInfos" Field="SubBrand" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Keywords:</td>
                <td style="border:solid 1px #e8e2e2;padding:2px;" colspan="4">
                    <asp:Label id="lblMarketingKeywords" runat="server" CssClass="Normal" Table="vwMarketingInfos" Field="Keywords" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkMarketingViewMoreKeywords" runat="server" Text="View More" OnClientClick="javascript:return ToggleMarketingMore('Keywords');" /></div>
                </td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Awards:</td>
                <td colspan="4" style="border-bottom:solid 1px #e8e2e2;">
                    <asp:Repeater ID="rptAwards" runat="server" >
                        <ItemTemplate>
                            <asp:Label ID="lblAward" runat="server" CssClass="Normal" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
                        </ItemTemplate>
                        <SeparatorTemplate>, </SeparatorTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr>
                <td class="SubSubHead">Product Instructions:</td>
                <td class="SubSubHead" colspan="4"><asp:Label ID="lblInstructions" runat="server" CssClass="Normal placeholder" Width="580" Table="vwMarketingInfos" Field="ProductInstructionsBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Video:</td>
                <td class="SubSubHead" colspan="4"><asp:Label ID="Label4" runat="server" CssClass="Normal placeholder" Width="580" Table="vwMarketingInfos" Field="VideoBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="Image1" runat="server" Table="vwMarketingInfos" Field="ExemptFromTax" />&nbsp;Exempt from Tax</td>
                <td />
                <td class="SubSubHead"><asp:Image ID="imgPatent" runat="server" Table="vwPartInfos" Field="Patent" />&nbsp;Patent</td>
                <td class="SubSubHead"><asp:Image ID="imgPatentPending" runat="server" Table="vwPartInfos" Field="PatentPending" />&nbsp;Patent Pending</td>
                <td class="SubSubHead"><asp:Image ID="imgPatentExists" runat="server" Table="vwPartInfos" Field="PatentExistsAtCreation" />&nbsp;Patent Exists at Part Creation</td>
            </tr>
            <tr>
                <td />
                <td />
                <td class="SubSubHead"><asp:Image ID="imgTrademark" runat="server" Table="vwPartInfos" Field="Trademark" />&nbsp;Trademark</td>
                <td class="SubSubHead"><asp:Image ID="imgRegisterTrademark" runat="server" Table="vwPartInfos" Field="RegisterTrademark" />&nbsp;Register Trademark</td>
                <td />
            </tr>
       
        </table>
    </asp:Panel>
    
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




