<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Settings.ascx.cs" Inherits="YourCompany.Modules.PDM2019.Settings" %>


<table width="700" cellpadding="4" cellspacing="0" border="0">
    <tr>
        <td style="width:100px;" class="SubSubHead">Portal Id:</td>
        <td style="width:200px;"><asp:TextBox ID="txtPortalId" runat="server" CssClass="Box" Width="25" /></td>
        <td style="width:200px;" />
        <td style="width:200px;" />
    </tr>
    <tr>
        <td class="SubSubHead">Part Url:</td>
        <td colspan="3"><asp:TextBox ID="txtPartUrlTemplate" runat="server" CssClass="Box" Width="450" /></td>
    </tr>
    <tr>
        <td class="SubSubHead" style="border-bottom:solid 1px black;">Emails:</td>
        <td style="border-bottom:solid 1px black;" align="center" >Submit</td>
        <td style="border-bottom:solid 1px black;" align="center"> Reject</td>
        <td style="border-bottom:solid 1px black;" align="center"> Approve</td>
    </tr>
    <tr>
        <td />
        <td align="left" valign="top">
            <asp:CheckBoxList ID="chkSubmit" runat="server" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow"
                        CssClass="SubSubHead" TextAlign="Right" >
                <asp:ListItem Text="&nbsp;Administrators" Value="Administrators" />
                <asp:ListItem Text="&nbsp;Accounting" Value="PDM Accounting" />
                <asp:ListItem Text="&nbsp;Creative" Value="PDM Creative" />
                <asp:ListItem Text="&nbsp;Marketing" Value="PDM Marketing" />
                <asp:ListItem Text="&nbsp;Product Development" Value="PDM Product Development" />
                <asp:ListItem Text="&nbsp;Purchasing" Value="PDM Purchasing" />
                <asp:ListItem Text="&nbsp;Quality Assurance" Value="PDM Quality Assurance" />
                <asp:ListItem Text="&nbsp;Sales" Value="PDM Sales" />

            </asp:CheckBoxList>
        </td>
        <td align="left" valign="top">
            <asp:CheckBoxList ID="chkReject" runat="server" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow"
                        CssClass="SubSubHead" TextAlign="Right" >
                <asp:ListItem Text="&nbsp;Administrators" Value="Administrators" />
                <asp:ListItem Text="&nbsp;Accounting" Value="PDM Accounting" />
                <asp:ListItem Text="&nbsp;Creative" Value="PDM Creative" />
                <asp:ListItem Text="&nbsp;Marketing" Value="PDM Marketing" />
                <asp:ListItem Text="&nbsp;Product Development" Value="PDM Product Development" />
                <asp:ListItem Text="&nbsp;Purchasing" Value="PDM Purchasing" />
                <asp:ListItem Text="&nbsp;Quality Assurance" Value="PDM Quality Assurance" />
                <asp:ListItem Text="&nbsp;Sales" Value="PDM Sales" />

            </asp:CheckBoxList>
        </td>
        <td align="left" valign="top">
            <asp:CheckBoxList ID="chkApprove" runat="server" RepeatColumns="1" RepeatDirection="Vertical" RepeatLayout="Flow"
                        CssClass="SubSubHead" TextAlign="Right" >
                <asp:ListItem Text="&nbsp;Administrators" Value="Administrators" />
                <asp:ListItem Text="&nbsp;Accounting" Value="PDM Accounting" />
                <asp:ListItem Text="&nbsp;Creative" Value="PDM Creative" />
                <asp:ListItem Text="&nbsp;Marketing" Value="PDM Marketing" />
                <asp:ListItem Text="&nbsp;Product Development" Value="PDM Product Development" />
                <asp:ListItem Text="&nbsp;Purchasing" Value="PDM Purchasing" />
                <asp:ListItem Text="&nbsp;Quality Assurance" Value="PDM Quality Assurance" />
                <asp:ListItem Text="&nbsp;Sales" Value="PDM Sales" />

            </asp:CheckBoxList>
        </td>

    </tr>
    <tr>
        <td colspan="2" align="left" style="border-top:solid 1px black; background-color:#e8e2e2;">
            <asp:RadioButtonList ID="rdoEntity" runat="server" CssClass="SubSubHead" TextAlign="Right" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="ToggleEnvironment">
                <asp:ListItem Text="&nbsp;Learning Resources&nbsp;&nbsp;" Value="LRH" Selected="True" />
                <asp:ListItem Text="&nbsp;Hand2mind" Value="H2M" />
            </asp:RadioButtonList>
        </td>
        <td colspan="2" align="right" style="border-top:solid 1px black; background-color:#e8e2e2;">
            <asp:RadioButtonList ID="rdoEnvironment" runat="server" CssClass="SubSubHead" TextAlign="Right" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="ToggleEnvironment">
                <asp:ListItem Text="&nbsp;Production&nbsp;&nbsp;" Value="Production" Selected="True" />
                <asp:ListItem Text="&nbsp;Development" Value="Development" />
            </asp:RadioButtonList>
        </td>

    </tr>
</table>