<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LegalEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.LegalEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

   
    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }


</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
        <table width="800" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td class="SubSubHead">
                    <asp:CheckBox ID="chkPatent" runat="server" Table="vwPartInfos" Field="Patent" TextAlign="Right" Text="&nbsp;Patent" />
                </td>
                <td class="SubSubHead" colspan="3">
                    <asp:CheckBox ID="chkPatentExists" runat="server" Table="vwPartInfos" Field="PatentExistsAtCreation" TextAlign="Right" Text="&nbsp;Patent Exists at Time of Part Creation." />
                </td>
            </tr>
            <tr>
                <td style="width:200px;" class="SubSubHead">
                    <asp:CheckBox ID="Image2" runat="server" Table="vwPartInfos" Field="PatentPending" TextAlign="Right" Text="&nbsp;Patent Pending" />
                </td>
                <td style="width:200px;" class="SubSubHead">
                    <asp:CheckBox ID="Image3" runat="server" Table="vwPartInfos" Field="Trademark" TextAlign="Right" Text="&nbsp;Trademark" />
                </td>
                <td style="width:200px;" class="SubSubHead">
                    <asp:CheckBox ID="Image4" runat="server" Table="vwPartInfos" Field="RegisterTrademark" TextAlign="Right" Text="&nbsp;Register Trademark" />
                </td>
                <td style="width:200px;" class="SubSubHead">
                </td>
            </tr>
        </table>
    </asp:Panel>


    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" OnClientClick="javascript:return ValidateAwards();" />
            </td>
        </tr>
    
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
