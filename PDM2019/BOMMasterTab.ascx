<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BOMMasterTab.ascx.cs" Inherits="YourCompany.Modules.PDM2019.BOMMasterTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        ToggleLabels();
        
        //var msg = jQuery('[id*="hdnShowMessage"]').val();
        //if (msg != "") {
        //    alert(msg);
       // }

        //jQuery('[id*="hdnShowMessage"]').val("");

        //var url = jQuery('[id*="hdnPDF"]').val();
        //if (url != "") {
        //    window.open(url);
       // }
       // jQuery('[id*="hdnPDF"]').val("");

    });

    function ToggleLabels() {

        jQuery('[id*="lblStatus"]').each(function (index) {
            var cls = $(this).text();
            $(this).addClass(cls);
        });


    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <table width="100%">
            <tr>
                <td style="width:50%;"  align="left"><asp:HyperLink ID="lnkAddChildPart" runat="server"><span class="actionButton">Add Child Part</span></asp:HyperLink></td>
                <td style="width:50%;" align="right">&nbsp;</td>
            </tr>
        </table>
        
    </asp:Panel>

    <asp:Label ID="lblChildParts" runat="server" CssClass="SubSubHead" Text="Child Parts:" />
    <table width="750" class="HeaderRow">
        <tr>
            <td style="width:150px;padding-left:4px;">Part Id</td>
            <td style="width:350px;">Description</td>
            <td style="width:100px;" align="center">Qty/Per</td>
            <td style="width:100px;" align="center">Status</td>
            <td style="width:50px;">&nbsp;</td>
        </tr>
    </table>
    <asp:Panel id="pnlMasterPart" runat="server" Width="750" Height="600" ScrollBars="Auto" style="border-bottom:solid 1px #e8e2e2;" >
        <asp:Repeater ID="rptChildren" runat="server">
            <HeaderTemplate>
                <table width="730" class="table table-condensed table-hover">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="width:150px;"><asp:HyperLink ID="lnkPartId" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChildPartId") %>' 
                                                        RecordId='<%# DataBinder.Eval(Container.DataItem, "Id") %>'
                                            /></td>
                    <td style="width:350px;"><asp:Label id="lblDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' /></td>
                    <td style="width:100px;" align="center"><asp:Label ID="lblQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>' /></td>
                    <td style="width:100px;" align="center"><asp:Label ID="lblStatus" CssClass="button"  runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Status") %>' />   </td>
                    <td style="width:20px;" align="center"><asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete.gif" OnClientClick="javascript:return confirm('Are you sure you want to delete this record?');" 
                                                                RecordId='<%# DataBinder.Eval(Container.DataItem, "Id") %>'
                                                                PartId = '<%# DataBinder.Eval(Container.DataItem, "ChildPartId") %>'
                                                                OnCommand="DeleteChildPart" 
                                                                    />
                    </td>
                </tr>            
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
         <asp:Label ID="lblNoChildren" runat="server" style="padding-left:25px;" Text="No Child Parts have been added." />
    </asp:Panel>

</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />


