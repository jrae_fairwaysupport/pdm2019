<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDSpecifications.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PDSpecificationsTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<script language="javascript" type="text/javascript">
    function PDToggleMore() {

        lnk = jQuery('[id*="lnkPDViewMore"]').first();

        if (lnk.text() == "View Less") {
            lnk.text("View More");
            jQuery('[id*="lblPDSpecs"]').css("max-height", "200px");
        }
        else {
            lnk.text("View Less");
            jQuery('[id*="lblPDSpecs"]').css("max-height", "");
        }
        return false;
    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;" >
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlSpecifications" runat="server" Width="750" Height="600" ScrollBars="Auto">

        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td class="SubSubHead" colspan="5">General Description:</td>
            </tr>
            <tr>
                <td style="width:150px;" class="SubSubHead" valign="top" />
                <td style="width:600px;border:solid 1px #e8e2e2;padding:2px;" colspan="4" valign="top">                                        
                    <asp:Label id="lblPDSpecs" runat="server" CssClass="Normal" Table="vwPartSpecifications" Field="GeneralDescriptionBR" style="max-height:200px; overflow:hidden; min-height:100px;" Width="580" />
                    <div style="float:right;"><asp:LinkButton ID="lnkPDViewMore" runat="server" Text="View More" OnClientClick="javascript:return PDToggleMore();" /></div>
                </td>
            </tr>
            <tr>
                <td class="SubSubHead">Color:</td>
                <td style="width:150px;"><asp:Label ID="lblColor" runat="server" CssClass="Normal placeholder" Width="130" Table="vwPartSpecifications" Field="ProductColor" /></td>
                <td style="width:150px;" />
                <td colspan="2" />
            </tr>
            <tr>
                <td class="SubSubHead" colspan="5"><asp:Image ID="imgmulp" runat="server" Table="vwPartSpecifications" Field="MultilingualProduct" />&nbsp;Multilingual Product</td>
            </tr>
            <tr>
                <td />
                <td style="width:150px;" class="SubSubHead"><asp:Image ID="Image8" runat="server" Table="vwPartSpecifications" Field="English" />&nbsp;English</td>
                <td style="width:150px;" class="SubSubHead"><asp:Image ID="Image9" runat="server" Table="vwPartSpecifications" Field="Spanish" />&nbsp;Spanish</td>
                <td style="width:150px;" class="SubSubHead"><asp:Image ID="Image10" runat="server" Table="vwPartSpecifications" Field="French" />&nbsp;French</td>
                <td style="width:150px;" class="SubSubHead"><asp:Image ID="Image11" runat="server" Table="vwPartSpecifications" Field="German" />&nbsp;German</td>
            </tr>

            <tr>
                <td />
                <td class="SubSubHead" colspan="4">
                    <asp:Label ID="lbllanguageother" runat="server" Width="100" Text="Other:" />
                    <asp:Label ID="lblLanguageOtherx" runat="server" CssClass="Normal placeholder" Width="480" Table="vwPartSpecifications" Field="MultiLingualProductLanguage" />
                </td>
            </tr>

            <tr>
                <td class="SubSubHead"><asp:Image ID="imgMagnet" runat="server" Table="vwPartSpecifications" Field="Magnets" />&nbsp;Magnets</td>
                <td class="SubSubHead"><asp:Image ID="imgAssemblyRequired" runat="server" Table="vwPartInfos" Field="AssemblyRequired" />&nbsp;Assembly Required</td>
                <td class="SubSubHead"><asp:Image ID="imgWaterproof" runat="server" Table="vwPartInfos" Field="Waterproof" />&nbsp;Waterproof</td>
                <td class="SubSubHead" colspan="2"><asp:Image ID="imgTempuratureControlled" runat="server" Table="vwPartInfos" Field="TempuratureControlled" />&nbsp;Temperature Controlled</td>
            
            </tr>
            
            <tr>
                <td class="SubSubHead" colspan="4"><asp:Image ID="imgContainsPaint" runat="server" Table="vwQAPartInfos" Field="PaintTesting" />&nbsp;Component Contains Paint (not screen-printed)</td>
            </tr>

            <tr>
                <td class="SubSubHead"><asp:Image ID="imgElectronic" runat="server" Table="vwPartSpecifications" Field="Electronic" />&nbsp;Electronic</td>
                <td class="SubSubHead"><asp:Image ID="chkwifi" runat="server" Table="vwPartSpecifications" Field="WifiEnabled" />&nbsp;Wi-fi Enabled</td>
                <td class="SubSubHead"><asp:Image ID="chkpowerswitch" runat="server" Table="vwPartSpecifications" Field="PowerSwitch" />&nbsp;Power Switch</td>
                <td class="SubSubHead"><asp:Image ID="chkPowerIndicator" runat="server" Table="vwPartSpecifications" Field="PowerIndicator" />&nbsp;Power Indicator</td>
                <td class="SubSubHead"><asp:Image ID="chkledpower" runat="server" Table="vwPartSpecifications" Field="LEDTechnology" />&nbsp;LED Technology</td>
            </tr>
            <tr>
                <td />
                <td class="SubSubHead" ><asp:Image ID="imgRemote" runat="server" Table="vwPartSpecifications" Field="IncludesRemote" />&nbsp;Includes Remote</td>
                <td class="SubSubHead">Battery Size:&nbsp;<asp:Label ID="lblremotebatterysize" runat="server" CssClass="Normal placeholder" Width="25" Table="vwPartSpecifications" Field="RemoteBatterySize" /></td>
                <td class="SubSubHead">Battery Quantity:&nbsp;<asp:Label ID="lblremotebatteryqty" runat="server" CssClass="Normal placeholder" Table="vwPartSpecifications" Field="RemoteBatteryQuantity" Width="25"/></td>
                <td class="SubSubHead"><asp:Image ID="lblbatteryincluded" runat="server" Table="vwPartSpecifications" Field="RemoteBatteryIncluded" />&nbsp;Battery Included</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="SubSubHead">Jacks:</td>
                <td class="SubSubHead"><asp:Image ID="imgUSB" runat="server" Table="vwPartSpecifications" Field="Usb" />&nbsp;USB</td>
                <td class="SubSubHead"><asp:Image ID="imgusbcable" runat="server" Table="vwPartSpecifications" Field="UsbCableIncluded" />&nbsp;Cable Included</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="SubSubHead" colspan="2">&nbsp;</td>
                <td class="SubSubHead"><asp:Image ID="imgheadphone" runat="server" Table="vwPartSpecifications" Field="HeadphoneJacks" />&nbsp;Headphone Jack</td>
                <td class="SubSubHead">Quantity:&nbsp;<asp:Label ID="lblHeadphoneJack" runat="server" CssClass="Normal placeholder" Width="25" Table="vwPartSpecifications" Field="HeadphoneJackQuantity" /></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="SubSubHead">Power Sources:</td>
                <td class="SubSubHead"><asp:Image ID="imgAC" runat='server' Table="vwPartSpecifications" Field="ACPower" />&nbsp;A/C Power</td>
                <td class="SubSubHead"><asp:Image ID="imgusbpower" runat="server" Table="vwPartSpecifications" Field="USBPower" />&nbsp;USB Power</td>
                <td class="SubSubHead"><asp:Image ID="imgbatteries" runat="server" Table="vwPartSpecifications" Field="Batteries" />&nbsp;Batteries Required</td>
            </tr>
            <tr>
                <td colspan="5" align="right">
                    <asp:Panel ID="pnlBatteries" runat="server" Width="600" Table="vwPartSpecifications" Field="Batteries" style="padding-right:25px;">
                        <table width="325" class="HeaderRow">
                            <tr>
                                <td style="width:150px;padding-left:4px;">Battery Size</td>
                                <td style="width:75px;" align="center">Quantity</td>
                                <td style="width:50px;" align="center">Included</td>
                                <td style="width:50px; padding:2px 4px 2px 2px;" align="right" valign="middle">
                                    <%--<asp:HyperLink ID="lnkBatteryAdd" runat="server" ImageUrl="~/images/plus2.gif" Hint="Editor"  />--%>
                                </td>
                            </tr>
                        </table>
                        <asp:DataList ID="lstBatteries" runat="server" CssClass="table table-hover table-condensed" Table="vwBatteries" Width="325" OnDataBinding="VerifyDataListHasData" NoDataLabel="lblNoBatteries" >
                            <ItemTemplate>
                                <asp:Label ID="lblbatterysize" Width="150" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BatterySize") %>' />
                                <asp:Label ID="lblbatteryqty" runat="server" Width="75" style="text-align:center;" Text='<%# DataBinder.Eval(Container.DataItem, "BatteryQuantity") %>' />
                                <asp:Label ID="Label15" runat="server" Width="20" Text="&nbsp;" /><asp:Image id="imgbatteryincldue" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "BatteryIncludedImage") %>' />
                                <asp:Label ID="lblSpace2" runat="server" Width="20" Text="&nbsp;" />
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:Label ID="lblNoBatteries" runat="server" CssClass="Normal" Width="325" Text="No batteries have been added to Part." style="text-align:left;padding-left:4px;" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="HeaderRow">
                        <tr>
                            <td style="width:750px;">Components:</td>
                        </tr>
                    </table>
                    <asp:DataList ID="lstComponents" runat="server" Width="100%" CssClass="table table-hover table-condensed" Table="vwPartPackagingComponents" OnDataBinding="VerifyDataListHasData" NoDataLabel="lblNoComponents">
                        <ItemTemplate>
                            <asp:Label ID="lblMolds" runat="server" Width="710" CssClass="SubSubHead" Text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' />
                            <br />
                            <asp:Label ID="lblQuantity" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="125" Text="Quantity:" />
                            <asp:Label ID="lblQty" runat="server" CssClass="Normal" Width="100" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>' />
                            <asp:Label ID="lblSize" runat="server" CssClass="SubSubHead" Width="75" Text="Size:" Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), false) %>' />
                            <asp:Label ID="Label12" runat="server" CssClass="SubSubHead" Width="75" Text="Size (in.):" Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                            <asp:Label ID= "lblSize1" runat="server" CssClass="Normal" Width="225" Text='<%# DataBinder.Eval(Container.DataItem, "Size") %>' Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), false) %>' />
                            <asp:Label ID="lblSizeL" runat="server" CssClass="SubSubHead" Width="25" Text='L:' Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                            <asp:Label id="lblSizeLV" runat="server" CssClass="Normal" Width="50" Text='<%# DataBinder.Eval(Container.DataItem, "Length") %>' Visible = '<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                            <asp:Label ID="Label8" runat="server" CssClass="SubSubHead" Width="25" Text='W:' Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                            <asp:Label id="Label9" runat="server" CssClass="Normal" Width="50" Text='<%# DataBinder.Eval(Container.DataItem, "Width") %>' Visible = '<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                            <asp:Label ID="Label10" runat="server" CssClass="SubSubHead" Width="25" Text='H:' Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                            <asp:Label id="Label11" runat="server" CssClass="Normal" Width="50" Text='<%# DataBinder.Eval(Container.DataItem, "Height") %>' Visible = '<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                            <br />
                            <asp:Label ID="Label13" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="125" Text="Material:" />
                            <asp:Label ID="Label14" runat="server" CssClass="Normal" Width="500" Text='<%# DataBinder.Eval(Container.DataItem, "MaterialsDisplay") %>' />
                            <br />
                            <asp:Label ID="Label16" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="125" Text="Details:" />
                            <asp:Label ID="Label17" runat="server" CssClass="Normal" Width="500" Text='<%# DataBinder.Eval(Container.DataItem, "Details") %>' />
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:Label id="lblNoComponents" runat="server" style="padding-left:15px;"  CssClass="Normal" Text="No Components have been added to this part." />                
                </td>
            </tr>
        </table>
                

    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




