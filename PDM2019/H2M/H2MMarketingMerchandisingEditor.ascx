<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingMerchandisingEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingMerchandisingEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    .readonly { background-color:#e8e2e2; }
         
    span.required
    {
        border-bottom: double 2px red;
    }          
    
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:800px;" >
                <asp:Label ID="labelcatalogtitle" runat="server" CssClass="SubSubHead" Text="Catalog Title:<br />" />
                <asp:TextBox ID="txtCatalogdescription" runat="server" Width="780" MaxLength="250" Table="vwMarketingMerchandising" Field="CatalogTitle" Placeholder="Catalog Title" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="labelMarketingCopy" runat="server" CssClass="SubSubHead" Text="Marketing Copy:<br />" />
                <asp:TextBox ID="TextBox6" runat="server" Width="780" TextMode="MultiLine" Height="50" Table="vwMarketingMerchandising" Field="MarketingCopy" Placeholder="Marketing Copy" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Product Benefits:<br />" />
                <asp:TextBox ID="TextBox1" runat="server" Width="780" TextMode="MultiLine" Height="50" Table="vwMarketingMerchandising" Field="ProductBenefits" Placeholder="Product Benefits" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Skills Copy:<br />" />
                <asp:TextBox ID="TextBox2" runat="server" Width="780" TextMode="MultiLine" Height="50" Table="vwMarketingMerchandising" Field="SkillsCopy" Placeholder="Skills Copy" />
            </td>
        </tr>
        <tr>
            <td  >
                <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Legal/Tech Spec Footnote:<br />" />
                <asp:TextBox ID="TextBox7" runat="server" Width="780" MaxLength="250" Table="vwMarketingMerchandising" Field="LegalTechSpecFootnote" Placeholder="Legal/Tech Spec Footnote" />
            </td>
        </tr>
        <tr>
            <td  >
                <asp:Label ID="label4" runat="server" CssClass="SubSubHead" Text="Catalog Page:<br />" />
                <asp:TextBox ID="TextBox8" runat="server" Width="150" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwMarketingMerchandising" Field="CatalogPage" Placeholder="Catalog Page" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="label5" runat="server" CssClass="SubSubHead" Text="Brand Subhead 1:<br />" />
                <asp:TextBox ID="TextBox9" runat="server" Width="780" TextMode="MultiLine" Height="50" MaxLength="500" Table="vwMarketingMerchandising" Field="BrandSubhead" Placeholder="Brand Subhead 1" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="label6" runat="server" CssClass="SubSubHead" Text="Brand Copy 1:<br />" />
                <asp:TextBox ID="TextBox10" runat="server" Width="780" TextMode="MultiLine" Height="50" MaxLength="500" Table="vwMarketingMerchandising" Field="BrandCopy1" Placeholder="Brand Copy 1" />
            </td>
        </tr>

        <tr>
            <td >
                <asp:Label ID="label7" runat="server" CssClass="SubSubHead" Text="Brand Copy 2:<br />" />
                <asp:TextBox ID="TextBox11" runat="server" Width="780" TextMode="MultiLine" Height="50" MaxLength="500" Table="vwMarketingMerchandising" Field="BrandCopy2" Placeholder="Brand Copy 2" />
            </td>
        </tr>

    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
