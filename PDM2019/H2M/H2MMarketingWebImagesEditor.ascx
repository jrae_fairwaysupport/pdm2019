<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingWebImagesEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingWebImagesEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

   
    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }


</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
        <asp:Panel ID="pnlInside" runat='server' style="min-height:410px;" >
        <table width="800" class="HeaderRow">
            <tr>
                <td style="width:775px;padding-left:4px;">Web Images</td>
                <td style="width:25px;padding-right:4px;"><asp:ImageButton id="imgAdd" runat="server" ImageUrl="~/images/plus2.gif" AlternateText="Add Image" ToolTip="Add Image"
                        OnCommand="DoEdit"
                        RecordId="-1" 
                        CommandName="ImageButton"

                     /></td>
            </tr>
        </table>

        <asp:DataList ID="lstWebImages" runat="server" Width="800" CssClass="table table-hover table-condensed"   OnDataBinding="VerifyDataListHasData" NoDataLabel="lblNoWebImages" >
            <ItemTemplate>
                <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete.gif" OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');"
                            RecordId = '<%# DataBinder.Eval(Container.DataItem, "RecordId") %>'
                            Image = '<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                            Priority = '<%# DataBinder.Eval(Container.DataItem, "Priority") %>'
                            OnCommand="DoDelete"
                />
                <asp:LinkButton ID="lnkImage" runat="server" Width="700" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                            RecordId = '<%# DataBinder.Eval(Container.DataItem, "RecordId") %>'
                            Image = '<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                            Priority = '<%# DataBinder.Eval(Container.DataItem, "Priority") %>'
                            OnCommand="DoEdit"
                            CommandName="LinkButton"
                />
                <asp:Label width="75" ID="lblVendorId" runat="server" style="vertical-align:top;" Text='<%# DataBinder.Eval(Container.DataItem, "Priority") %>' />
            </ItemTemplate>
            <AlternatingItemStyle BackColor="#e8e2e2" />
        </asp:DataList>
        <asp:Label ID="lblNoWebImages" runat="server" CssClass="Normal" Width="720" style="padding-left:4px;" Text="No Web Images have been added to this part." />    
        </asp:Panel>

        <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" OnClientClick="javascript:return ValidateAwards();" />
            </td>
        </tr>
    
    </table>
    </asp:Panel>
</asp:Panel>

<asp:Panel ID="pnlEditor" runat="server" Width="800" Height="450" Visible="false">
    <table width="100%">
        <tr>
            <td style="width:100%; height:450px;" align="center" valign="middle">
                <table width="400" cellpadding="4" cellspacing="0" border="0">
                    <tr>
                        <td colspan="2" style="border:solid 1px black;" class="SubHead" align="center"><asp:Label ID="lblAction" runat="server" CssClass="SubSubHead" /></td>
                    </tr>
                    <tr>
                        <td style="width:100px;border-left: solid 1px black;" class="SubSubHead">Image:</td>
                        <td style="width:300px;border-right: solid 1px black;"><asp:TextBox ID="txtImage" runat="server" CssClass="Box" Width="280" MaxLength="250" Placeholder="Image Name" /></td>
                    </tr>
                    <tr>
                        <td style="border-left:solid 1px black;" class="SubSubHead">Priority:</td>
                        <td style="border-right:solid 1px black;"><asp:TextBox id="txtPriority" runat="server" CssClass="Box" Width="100" MaxLength="10" onkeypress="return isNumberKey(event);" Placeholder="Priority" /></td>
                    </tr>
                    <tr>
                        <td style="padding-top:15px;border-left:solid 1px black;border-bottom:solid 1px black;" align="left"><asp:LinkButton ID="lnkCancelEdit" runat="server" Text="Cancel" OnCommand="DoCancelEdit" /></td>
                        <td style="padding-top:15px;border-right:solid 1px black;border-bottom:solid 1px black;" align="right"><asp:LinkButton id="lnkSaveEdit" runat="server" Text="Update" OnCommand="DoSaveEdit" /></td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnImagesXML" runat="server" Value="" />
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
