<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingMarketplaceEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingMarketplaceEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    .readonly { background-color:#e8e2e2; }
         
    span.required
    {
        border-bottom: double 2px red;
    }          
    
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;" >
                <asp:Label ID="labelcatalogtitle" runat="server" CssClass="SubSubHead" Text="Catalog Title:" />
            </td>
            <td style="width:200px;" />
            <td style="width:200px;" />
            <td style="width:200px;" />
        </tr>
        <tr>
            <td colspan="4">
                <asp:TextBox ID="txtCatalogdescription" runat="server" CssClass="Box" Width="780" MaxLength="100" Table="vwMarketingMarketplace" Field="MarketplaceTitle" Placeholder="MarketplaceTitle" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="labelMarketingCopy" runat="server" CssClass="SubSubHead" Text="Marketplace Description:<br />" />
                <asp:TextBox ID="TextBox6" runat="server" Width="780" TextMode="MultiLine" Height="50" Table="vwMarketingMarketplace" Field="MarketplaceDescription" Placeholder="Marketplace Description" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Features 1:<br />" />
                <asp:TextBox ID="TextBox1" runat="server" CssClass="Box" Width="180" MaxLength="500" Table="vwMarketingMarketplace" Field="Features1" Placeholder="Features 1" />
            </td>
            <td >
                <asp:Label ID="label8" runat="server" CssClass="SubSubHead" Text="Features 2:<br />" />
                <asp:TextBox ID="TextBox3" runat="server" CssClass="Box" Width="180" MaxLength="500" Table="vwMarketingMarketplace" Field="Features2" Placeholder="Features 2" />
            </td>
            <td >
                <asp:Label ID="label9" runat="server" CssClass="SubSubHead" Text="Features 3:<br />" />
                <asp:TextBox ID="TextBox4" runat="server" CssClass="Box" Width="180" MaxLength="500" Table="vwMarketingMarketplace" Field="Features3" Placeholder="Features 3" />
            </td>
            <td >
                <asp:Label ID="label10" runat="server" CssClass="SubSubHead" Text="Features 4:<br />" />
                <asp:TextBox ID="TextBox5" runat="server" CssClass="Box" Width="180" MaxLength="500" Table="vwMarketingMarketplace" Field="Features4" Placeholder="Features 4" />
            </td>

        </tr>

        <tr>
            <td >
                <asp:Label ID="label11" runat="server" CssClass="SubSubHead" Text="Features 5:<br />" />
                <asp:TextBox ID="TextBox12" runat="server" CssClass="Box" Width="180" MaxLength="500" Table="vwMarketingMarketplace" Field="Features5" Placeholder="Features 5" />
            </td>
            <td />
            <td />
            <td />
        </tr>

        <tr>
            <td >
                <asp:Label ID="label12" runat="server" CssClass="SubSubHead" Text="Keywords 1:<br />" />
                <asp:TextBox ID="TextBox13" runat="server" CssClass="Box" Width="180" Table="vwMarketingMarketplace" Field="Keywords1" Placeholder="Keywords 1" />
            </td>
            <td >
                <asp:Label ID="label13" runat="server" CssClass="SubSubHead" Text="Keywords 2:<br />" />
                <asp:TextBox ID="TextBox14" runat="server" CssClass="Box" Width="180" Table="vwMarketingMarketplace" Field="Keywords2" Placeholder="Keywords 2" />
            </td>
            <td >
                <asp:Label ID="label14" runat="server" CssClass="SubSubHead" Text="Keywords 3:<br />" />
                <asp:TextBox ID="TextBox15" runat="server" CssClass="Box" Width="180" Table="vwMarketingMarketplace" Field="Keywords3" Placeholder="Keywords 3" />
            </td>
            <td >
                <asp:Label ID="label15" runat="server" CssClass="SubSubHead" Text="Keywords 4:<br />" />
                <asp:TextBox ID="TextBox16" runat="server" CssClass="Box" Width="180" Table="vwMarketingMarketplace" Field="Keywords4" Placeholder="Keywords 4" />
            </td>

        </tr>

        <tr>
            <td >
                <asp:Label ID="label16" runat="server" CssClass="SubSubHead" Text="Keywords 5:<br />" />
                <asp:TextBox ID="TextBox17" runat="server" CssClass="Box" Width="180" Table="vwMarketingMarketplace" Field="Keywords5" Placeholder="Keywords 5" />
            </td>
            <td />
            <td />
            <td />
        </tr>

        <tr>
            <td colspan="4" style="padding-top:15px;border-bottom:solid 1px #e8e2e2;" class="Head">Marketplaces</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:CheckBox ID="chkazsc" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Amazon - Seller Central" Table="vwMarketingMarketplace" Field="MKAmazonSellerCentral" Width="180" />
                <asp:CheckBox ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Amazon - Vendor Central" Table="vwMarketingMarketplace" Field="MKAmazonVendorCentral" Width="180" />
                <asp:CheckBox ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Zulily" Table="vwMarketingMarketplace" Field="Zulily" Width="80" />
                <asp:CheckBox ID="CheckBox3" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Walmart - Marketplace" Table="vwMarketingMarketplace" Field="WalmartMarketplace" Width="180" />
                <asp:CheckBox ID="CheckBox4" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Walmart - Vendor" Table="vwMarketingMarketplace" Field="WalmartVendor" Width="160" />
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="labelother1" runat="server" CssClass="SubSubHead" Text="Other Marketplace:<br />" />
                <asp:TextBox ID="txtother1" runat="server" Width="180" CssClass="Box" MaxLength="100" Table="vwMarketingMarketplace" Field="Other1" Placeholder="Other Marketplace" />
            </td>
            <td>
                <asp:Label ID="label17" runat="server" CssClass="SubSubHead" Text="Other Marketplace:<br />" />
                <asp:TextBox ID="TextBox18" runat="server" Width="180" CssClass="Box" MaxLength="100" Table="vwMarketingMarketplace" Field="Other2" Placeholder="Other Marketplace" />
            </td>
            <td>
                <asp:Label ID="label18" runat="server" CssClass="SubSubHead" Text="Other Marketplace:<br />" />
                <asp:TextBox ID="TextBox19" runat="server" Width="180" CssClass="Box" MaxLength="100" Table="vwMarketingMarketplace" Field="Other3" Placeholder="Other Marketplace" />
            </td>
            <td />
        </tr>

        <tr>
            <td style="padding-top:10px;" valign="bottom">
                <asp:Label ID="labelasin" runat="server" CssClass="SubSubHead" Text="ASIN:<br />" />
                <asp:TextBox ID="txtasing" runat="server" CssClass="Box" Width="180" MaxLength="50" Table="vwMarketingMarketplace" Field="ASIN" Placeholder="ASIN" />
            </td>
            <td style="padding-top:10px;" valign="bottom">
            </td>
            <td style="padding-top:10px;" valign="bottom">
                <asp:CheckBox ID="chkaxnsci" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Amazon - Seller Central" Table="vwMarketingMarketplace" Field="AmazonSellerCentral" />
            </td>
            <td style="padding-top:10px;" valign="bottom">
                <asp:CheckBox ID="CheckBox5" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Amazon - Vendor Central" Table="vwMarketingMarketplace" Field="AmazonVendorCentral" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="label19" runat="server" CssClass="SubSubHead" Text="Amazon Min. Order Qty.:<br />" />
                <asp:TextBox ID="txtAmazonMinOrderQty" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" style="text-align:right;" Table="vwMarketingMarketplace" Field="AmazonMinOrderQuantity" Placeholder="Amazon Min. Order Qty." />
            </td>
            <td >
                <asp:Label ID="label20" runat="server" CssClass="SubSubHead" Text="Amazon List Price:<br />" />
                <asp:TextBox ID="txtAmazonListPrice" runat="server" CssClass="Box" Width="180" MaxLength="10" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" style="text-align:right;" Table="vwMarketingMarketplace" Field="AmazonListPrice" Placeholder="Amazon List Price" />
            </td>
            <td >
                <asp:Label ID="label21" runat="server" CssClass="SubSubHead" Text="Amazon Price:<br />" />
                <asp:TextBox ID="txtAmazonPrice" runat="server" CssClass="Box" Width="180" MaxLength="10" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" style="text-align:right;" Table="vwMarketingMarketplace" Field="AmazonPrice" Placeholder="Amazon Price" />
            </td>
            <td >
                <asp:Label ID="label22" runat="server" CssClass="SubSubHead" Text="Amazon Sell Price:<br />" />
                <asp:TextBox ID="txtAmazonSellPrice" runat="server" CssClass="Box" Width="180" MaxLength="10" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" style="text-align:right;" Table="vwMarketingMarketplace" Field="AmazonSellPrice" Placeholder="Amazon Sell Price" />
            </td>
        </tr>


        <tr>
            <td style="padding-top:10px;">
                <asp:Label ID="labelfamisitem" runat="server" CssClass="SubSubHead" Text="FAMIS Item Number:<br />" />
                <asp:TextBox ID="txtfamis" runat="server" CssClass="Box" Width="180" MaxLength="50" Table="vwMarketingMarketplace" Field="FAMISItemNumber" Placeholder="FAMIS Item Number" />
            </td>
            <td style="padding-top:10px;">
                <asp:Label ID="label23" runat="server" CssClass="SubSubHead" Text="FAMIS Price:<br />" />
                <asp:TextBox ID="txtFamisPrice" runat="server" CssClass="Box" Width="180" MaxLength="10" Format="Decimal" Places="4" style="text-align:right;" onkeypress="return isDecimal(event);"  Table="vwMarketingMarketplace" Field="FAMISPrice" Placeholder="FAMIS Price" />
            </td>
            <td style="padding-top:10px;">
                <asp:Label ID="label24" runat="server" CssClass="SubSubHead" Text="FAMIS Start Date:<br />" />
                <telerik:RadDatePicker id="dtFamisstart" runat="server" CssClass="Box" Width="180" Table="vwMarketingMarketplace" Field="FAMISStartDate" />
            </td>
            <td style="padding-top:10px;">
                <asp:Label ID="label25" runat="server" CssClass="SubSubHead" Text="FAMIS Expiration Date:<br />" />
                <telerik:RadDatePicker id="dtFamisstartx" runat="server" CssClass="Box" Width="180" Table="vwMarketingMarketplace" Field="FAMISExpirationDate" />
            </td>

        </tr>
        
        <tr>
            <td colspan="2">
                <asp:Label ID="labelurl" runat="server" CssClass="SubSubHead" Text="Url Submitted to FAMIS:<br />" />
                <asp:TextBox ID="txturl" runat="server" CssClass="Box" Width="380" MaxLength="250" Table="vwMarketingMarketplace" Field="UrlSubmittedToFAMIS" Placeholder="Url Submitted to FAMIS" />
            </td>
            <td colspan="2">
                <asp:Label ID="label26" runat="server" CssClass="SubSubHead" Text="Bundle Form Submitted to FAMIS:<br />" />
                <asp:TextBox ID="TextBox25" runat="server" CssClass="Box" Width="380" MaxLength="250" Table="vwMarketingMarketplace" Field="BundleFAMIS" Placeholder="Bundle Form Submitted to FAMIS" />
            </td>
        </tr>



    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
