<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingTab.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingContainer" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="H2MMarketingWeb.ascx" TagName="web" TagPrefix="pdm" %>
<%@ Register Src="H2MMarketingWebImages.ascx" TagName="webImages" TagPrefix="pdm" %>
<%@ Register Src="H2MMarketingMerchandising.ascx" TagName="merchandising" TagPrefix="pdm" %>
<%@ Register Src="H2MMarketingMarketplace.ascx" TagName="marketplace" TagPrefix="pdm" %>
<%@ Register Src="H2MMarketingPearson.ascx" TagName="pearson" TagPrefix="pdm" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server"  AutoPostBack="true" OnTabClick="LoadData"  >
        <Tabs>
            <telerik:RadTab Text="Web"/>
            <telerik:RadTab Text="Web Image" />
            <telerik:RadTab Text="Merchandising" />
            <telerik:RadTab Text="Marketplace" />
            <telerik:RadTab Text="Pearson Portal" />
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;" >

        <telerik:RadPageView runat="server" ID="pgWeb" >
            <pdm:web id="ctlWeb" runat="server" Editors="m-*|ad-*" Editor="H2MMarketingWebEditor"  />
        </telerik:RadPageView> 

        <telerik:RadPageView runat="server" ID="pgWebImage" >
            <pdm:webImages id="ctlWebImages" runat="server" Editors="m-*|ad-*" Editor="H2MMarketingWebImagesEditor" />
        </telerik:RadPageView> 
    
        <telerik:RadPageView runat="server" ID="pgMerchandising">
            <pdm:merchandising id="ctlMerchandising" runat="server"  Editors="m-*|ad-*"  Editor="H2MMarketingMerchandisingEditor"  />
        </telerik:RadPageView> 

        <telerik:RadPageView runat="server" ID="pgMarketplace">
            <pdm:marketplace id="ctlMarketplace" runat="server"  Editors="m-*|ad-*"  Editor="H2MMarketingMarketplaceEditor"  />
        </telerik:RadPageView> 

        <telerik:RadPageView runat="server" ID="pgPearsongPortal">
            <pdm:pearson id="ctlPearson" runat="server"  Editors="m-*|ad-*"  Editor="H2MMarketingPearsonEditor"  />
        </telerik:RadPageView> 

    </telerik:RadMultiPage>
</asp:Panel>


<asp:HiddenField ID="hdnModuleId" runat="server" Value="-1" />




