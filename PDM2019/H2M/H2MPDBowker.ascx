<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPDBowker.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPDBowker" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:187px;" class="SubSubHead">Supplier Name:</td>
                <td style="width:563px;" colspan="3"><asp:Label id="Label22" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="Name" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Title Bowker:</td>
                <td colspan="3"><asp:Label id="Label1" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="Title" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Subtitle:</td>
                <td colspan="3"><asp:Label id="Label2" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="SubTitleBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" ><asp:Image ID="imglisted" runat="server" Table="vwBowker" Field="ListedBowker" />&nbsp;ETA ISBN Listed Bowker</td>
                <td />
                <td class="SubSubHead">Language:</td>
                <td ><asp:Label id="Label4" runat="server" CssClass="Normal placeholder" Width="169" Table="vwBowker" Field="LanguageText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Contributor 1 Function:</td>
                <td style="width:187px;"><asp:Label id="Label23" runat="server" CssClass="Normal placeholder" Width="167" Table="vwBowker" Field="Contributor1Text" /></td>
                <td style="width:187px;" class="SubSubHead">Contributor 2 Function:</td>
                <td style="width:189px;"><asp:Label id="Label24" runat="server" CssClass="Normal placeholder" Width="169" Table="vwBowker" Field="Contributor2Text" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Product:</td>
                <td colspan="3" ><asp:Label id="Label3" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="ProductFormText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Audience:</td>
                <td colspan="3" ><asp:Label id="Label5" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="AudienceText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">BISAC Subject Code:</td>
                <td colspan="3" ><asp:Label id="Label6" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="BISACSubjectText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Publisher:</td>
                <td colspan="3"><asp:Label id="Label9" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="PublisherBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Imprint Name:</td>
                <td colspan="3" ><asp:Label id="Label7" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="ImprintNameText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Publishing Status:</td>
                <td colspan="3" ><asp:Label id="Label8" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="PublishStatusText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Sales Rights Type:</td>
                <td ><asp:Label id="Label10" runat="server" CssClass="Normal placeholder" Width="167" Table="vwBowker" Field="SalesRightsTypeText" /></td>
                <td class="SubSubHead">Sales Rights Location(s):</td>
                <td ><asp:Label id="Label11" runat="server" CssClass="Normal placeholder" Width="169" Table="vwBowker" Field="SalesRightsLocationText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Barcode Indicator:</td>
                <td colspan="3" ><asp:Label id="Label12" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="BarcodeIndicatorText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Returnable Indicator:</td>
                <td ><asp:Label id="Label13" runat="server" CssClass="Normal placeholder" Width="167" Table="vwBowker" Field="ReturnableIndicatorText" /></td>
                <td />
                <td />
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Description:</td>
                <td colspan="3"><asp:Label id="Label14" runat="server" CssClass="Normal placeholder" Width="543" Table="vwBowker" Field="DescriptionBR" /></td>
            </tr>


        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




