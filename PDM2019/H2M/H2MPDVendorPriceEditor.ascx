<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPDVendorPriceEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPDVendorPriceEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestVendorId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:30px; top:165px; font-size:small; width:300px;}
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    
    .readonly { background-color:#e8e2e2; }
    input.required { border: solid 1px orange; }           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        ToggleComboPricing();

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    function ToggleComboPricing() {

        var checked = jQuery('[id*="chkComboPricing"]:checked').length > 0;
        var showLabels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        if (checked) {
            jQuery('[id*="txtComboPrice"]').show();
            if (showLabels) jQuery('[id*="labelComboPrice"]').show();
        }
        else {
            jQuery('[id*="txtComboPrice"]').hide();
            jQuery('[id*="labelComboPrice"]').hide();
        }
    }
</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label ID="labelDevelopment" runat="server" CssClass="SubSubHead" Text="Development Costs:" /></td>
            <td style="width:200px;" />
            <td style="width:200px;"><asp:Label id="labelInitial" runat="server" CssClass="SubSubHead" Text="Initial PO Sent:" /></td>
            <td style="width:200px;" />
        </tr>
        <tr>
            <td><asp:TextBox ID="txtDevelopmentCosts" runat="server" CssClass="Box" Width="180" MaxLength="10" Format="Decimal" Places="4" style="text-align:right;" onkeypress="return isDecimal(event);" Table="vwPartVendorPrices" Field="DevelopmentCosts" Placeholder="Development Costs" /></td>
            <td />
            <td colspan="2"><asp:TextBox ID="txtInitialPOSEnt" runat="server" CssClass="Box" Width="380" MaxLength="50" Table="vwPartVendorPrices" Field="InitialPOSent" Placeholder="Initial PO Sent" />
        </tr>
        <tr>
            <td>
                <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Mold:<br />" />
                <asp:TextBox ID="txtMold" runat="server" CssClass="Box" Width="180" MaxLength="10" Format="Decimal" Places="4" style="text-align:right;" onkeypress="return isDecimal(event);" Table="vwPartVendorPrices" Field="Mold" Placeholder="Mold" />
            </td>
            <td />
            <td >
                <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Initial Sail Date:<br />" />
                <telerik:RadDatePicker id="dtInitialSailDate" runat="server" width="180" Table="vwPartVendorPrices" Field="InitialSailDate" Placeholder="InitialSailDate" />
            </td>
            <td />
        </tr>
        <tr>
            <td>
                <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Printing Fee:<br />" />
                <asp:TextBox ID="txtPrintingFee" runat="server" CssClass="Box" Width="180" MaxLength="10" Format="Decimal" Places="4" style="text-align:right;" onkeypress="return isDecimal(event);" Table="vwPartVendorPrices" Field="PrintingFee" Placeholder="Printing Fee" />
            </td>
            <td />
            <td >
                <asp:Label ID="label4" runat="server" CssClass="SubSubHead" Text="Date to Production:<br />" />
                <telerik:RadDatePicker id="dtDateToProduction" runat="server" width="180" Table="vwPartVendorPrices" Field="DateToProduction" Placeholder="Date to Production" />
            </td>
            <td />
        </tr>
        <tr>
            <td>
                <asp:Label ID="label5" runat="server" CssClass="SubSubHead" Text="Prototype:<br />" />
                <asp:TextBox ID="txtPrototype" runat="server" CssClass="Box" Width="180" MaxLength="10" Format="Decimal" Places="4" style="text-align:right;" onkeypress="return isDecimal(event);" Table="vwPartVendorPrices" Field="Prototype" Placeholder="Prototype" />
            </td>
            <td />
            <td />
            <td />
        </tr>
        <tr>
            <td>
                <asp:Label ID="label6" runat="server" CssClass="SubSubHead" Text="Other Costs:<br />" />
                <asp:TextBox ID="txtOtherCosts" runat="server" CssClass="Box" Width="180" MaxLength="10" Format="Decimal" Places="4" style="text-align:right;" onkeypress="return isDecimal(event);" Table="vwPartVendorPrices" Field="OtherCosts" Placeholder="OtherCosts" />
            </td>
            <td />
            <td />
            <td />
        </tr>
        <tr>
            <td valign="bottom">
                <asp:CheckBox ID="chkComboPricing" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Combo Pricing" Table="vwPartVendorPrices" Field="ComboPricing" onclick="javascript:ToggleComboPricing();" />
            </td>
            <td valign="bottom">
                <asp:Label ID="labelComboPrice" runat="server" CssClass="SubSubHead" Text="Combo Pricing:<br />" />
                <asp:TextBox ID="txtComboPrice" runat="server" CssClass="Box" Width="180" Table="vwPartVendorPrices" Field="ComboPrice" Format="Decimal" Placeholder="ComboPrice" style="text-align:right;" onkeypress="return isDecimal(event);" MaxLength="10" />
            </td>
            <td />
            <td />
        </tr>

            <tr>
                <td colspan="4" valign="top" style="padding:0px;">
                    <table width="720" class="HeaderRow">
                        <tr>
                            <td style="width:670px;padding-left:4px;">Vendor Price Breaks</td>
                            <td style="width:50px;padding: 2px 4px 2px 2px;" align="right"><asp:ImageButton ID="lnkAddVendorPriceBreak" runat="server" ImageUrl="~/images/plus2.gif" OnCommand="VendorPriceBreak_Toggle" CommandArgument="Open" RecordId="-1" /></td>
                        </tr>
                    </table>

                    <asp:DataList ID="lstVendorPriceBreaks" runat="server" Width="720" CssClass="table table-hover table-condensed" Table="vwVendorPriceBreaks">
                        <ItemTemplate>
                            <asp:Label width="625" ID="lblVendorId" runat="server" style="vertical-align:top; border-bottom:solid 1px orange;" Text='<%# DataBinder.Eval(Container.DataItem, "VendorSuggest") %>' />
                            <asp:ImageButton id="lnkEditVendorPriceBreak" runat="server" style="vertical-align:top;" ImageUrl="~/images/edit.gif" Hint="Editor"
                                                       RecordId = '<%# DataBinder.Eval(Container.DataItem, "Id") %>' OnCommand="VendorPriceBreak_Toggle" CommandArgument="Open"
                                                       
                             />
                            <asp:Label ID="lblspace" runat="server" Width="10" Text="&nbsp;" />
                            <asp:ImageButton ID="imgDelete" runat="server" style="vertical-align:top;" ImageUrl="~/images/delete.gif" Hint="Editor" 
                                                       OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');"
                                                       CommandArgument = '<%# DataBinder.Eval(Container.DataItem, "Id") %>'
                                                       OnCommand="VendorPriceBreak_Delete"
                                                       RecordId = '<%# DataBinder.Eval(Container.DataItem, "Id") %>'
                            />
                            <br />

                            <asp:Label ID="lblVendorPart" runat="server" Width="125" style="padding-left:25px;" CssClass="SubSubHead" Text="Vendor Part Id:" />
                            <asp:Label ID="lblvendorPartId" runat="server" Width="100" Text='<%# DataBinder.Eval(Container.DataItem, "VendorPartId") %>' style="border-bottom:solid 1px orange;" />
                            <asp:Label ID="lblQuantity" runat="server" CssClass="SubSubHead" Width="75" Text="Quantity:" />
                            <asp:Label Width="60" id="lblQty1" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity1") %>' />
                            <asp:Label Width="60" id="Label1" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity2") %>' />
                            <asp:Label Width="60" id="Label2" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity3") %>' />
                            <asp:Label Width="60" id="Label10" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity4") %>' />
                            <asp:Label Width="60" id="Label11" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity5") %>' />
                            <br />

                            <asp:Label ID="lblstuff" runat="server" CssClass="SubSubHead" style="padding-left:25px;" Width="125" Text='&nbsp;' />
                            <asp:Label ID="lblstuff2" runat="server" Width="100" Text="&nbsp;" />
                            <asp:Label ID="lbl14" runat="server" CssClass="SubSubHead" Width="75" Text="Price:" />
                            <asp:Label Width="60" id="lbl15" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price1Text") %>' />
                            <asp:Label Width="60" id="lbls6" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price2Text") %>' />
                            <asp:Label Width="60" id="lbll17" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price3Text") %>' />
                            <asp:Label Width="60" id="lbl18" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price4Text") %>' />
                            <asp:Label Width="60" id="lbl19" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price5Text") %>' />
                            
                            

                        </ItemTemplate>
                        <AlternatingItemStyle BackColor= "#e8e2e2" />
                    </asp:DataList>
                    <asp:Label ID="lblNoVendorPriceBreaks" runat="server" CssClass="Normal" Width="720" style="padding-left:4px;" Text="No Vendor Price Breaks have been added to this part." />
                </td>
            </tr>


    </table>
    </asp:Panel>

    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" Checked="true" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" />
            </td>
        </tr>
    
    </table>
</asp:Panel>

<asp:Panel ID="pnlAddVendorPriceBreak" runat="server" Width="800" style="padding:4px;" visible="false">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td style="width:100%; height:450px;" valign="middle" align="center">
                <asp:Panel ID="pnlborder" runat="server" style="border:solid 1px black;">
                    <table width="800" cellpadding="4" cellspacing="0" border="0">
                        <tr>
                            <td colspan="4" align="center" style="border-bottom:solid 1px black;"><asp:Label ID="lblAddPriceBreak" runat="server" CssClass="SubSubHead" /></td>
                        </tr>
                        <tr>
                            <td style="width:800px;" colspan="4">
                                <asp:Label ID="lblVendorId" runat="server" CssClass="SubSubHead" Text="VendorId:<br />" />
                                <DNN:DNNTextSuggest ID="txtVendorId" runat="server" 
                                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                                    CssClass="Box required"  CaseSensitive="false" TextSuggestCssClass="suggestVendorId" 
                                    Table="vwVendorPriceBreaks" Field="VendorSuggest" TargetField="VendorId" NameIDSplit="-" Width="600" Placeholder="Vendor Id"
                                    queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                                    validation="SELECT COUNT(*) AS HITS FROM vwVENDORS WHERE ID = @P0 OR NAME = @P1"

                                />
                            </td>
                        </tr>

                        <tr>
                            <td style="width:200px;">
                                <asp:Label ID="lblVendorPartId" runat="server" CssClass="SubSubHead" Text="Vendor Part Id:<br />" />
                                <asp:TextBox ID="txtVendorPartId" runat="server" CssClass="Box required" Width="180" MaxLength="30" Placeholder="Vendor Part Id" Table="vwVendorPriceBreaks" Field="VendorPartId"  />
                            </td>

                            <td style="width:200px;" align="center">
                                <asp:Label ID="lblQuantity" runat="server" CssClass='SubSubHead' Text="Quantity:<br />" />
                                <asp:TextBox ID="txtQuantity1" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 1" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity1" />
                            </td>
                            <td style="width:200px;" align="center">
                                <asp:Label ID="lblPrice" runat="server" CssClass="SubSubHead" Text="Unit Price:<br />" />
                                <asp:TextBox ID="txtPrice1" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 1" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price1" onkeypress="return isDecimal(event)"/>
                            </td>
                            <td style="width:200px;">&nbsp;</td>
                        </tr>

                        <tr>
                            <td >&nbsp;</td>
                            <td align="center">
                                <asp:TextBox ID="txtQuantity2" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 2" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity2" />
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtPrice2" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 2" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price2" onkeypress="return isDecimal(event)" />
                            </td>
                            <td style="width:200px;">&nbsp;</td>
                        </tr>

                        <tr>
                            <td >&nbsp;</td>
                            <td align="center">
                                <asp:TextBox ID="txtQuantity3" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 3" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity3" />
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtPrice3" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 3" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price3" onkeypress="return isDecimal(event)" />
                            </td>
                            <td style="width:200px;">&nbsp;</td>
                        </tr>

                        <tr>
                            <td >&nbsp;</td>
                            <td align="center">
                                <asp:TextBox ID="txtQuantity4" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 4" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity4" />
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtPrice4" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 4" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price4" onkeypress="return isDecimal(event)" />
                            </td>
                            <td style="width:200px;">&nbsp;</td>
                        </tr>

                        <tr>
                            <td >&nbsp;</td>
                            <td align="center">
                                <asp:TextBox ID="txtQuantity5" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 5" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity5" />
                            </td>
                            <td align="center">
                                <asp:TextBox ID="txtPrice5" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 5" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price5"  onkeypress="return isDecimal(event)"/>
                            </td>
                            <td style="width:200px;">&nbsp;</td>
                        </tr>


                        <tr>
                            <td colspan="2" style="border-top:solid 1px black;"><asp:Button ID="btnCancelComponent" runat="server" Text="Cancel" OnCommand="VendorPriceBreak_Toggle"  CommandArgument="Close" /></td>
                            <td colspan="2"  style="border-top:solid 1px black;" align="right"><asp:Button id="btnAddComponentSave" runat="server" Text="Save" OnCommand="VendorPriceBreak_Save" CommandArgument="-1" /></td>
                        </tr>
                    </table>           
                </asp:Panel>
            </td>
        </tr>
    </table>


 
</asp:Panel>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<%--<asp:HiddenField ID="hdnShowPanel" runat="server" Value="Main" />--%>
