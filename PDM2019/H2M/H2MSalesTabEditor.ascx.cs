using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Text;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MSalesTabEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Customer Price Breaks"; } }

        protected override void Page_Load(object sender, EventArgs e)
        {

            try
            {
                DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
                currPage.Title = PAGE_TITLE;


                if (!IsPostBack)
                {
                    PopulateControls();

                    if (!string.IsNullOrEmpty(CustomerId))
                    {
                        LoadData();
                        btnClear.Visible = false;
                        CustomerIdText = txtCustomerId.Text;
                    }
                    
                }


            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        protected override void PopulateControls()
        {

            Controller controller = new Controller(entityId, environment);
            string sql = "SELECT * FROM UNITS ORDER BY DESCRIPTION";
            DataTable dt = controller.vmfg.ExecuteReader(sql);

            DataRow dr = dt.NewRow();
            dr["UNIT_OF_MEASURE"] = "";
            dr["DESCRIPTION"] = "Unit of Measure";
            dr["SCALE"] = 0;
            dt.Rows.InsertAt(dr, 0);

            ddlUnitOfMeasure.DataTextField = "DESCRIPTION";
            ddlUnitOfMeasure.DataValueField = "UNIT_OF_MEASURE";
            ddlUnitOfMeasure.DataSource = dt;
            ddlUnitOfMeasure.DataBind();

            ddlUnitOfMeasure.SelectedValue = "EA";
        }



        //setting dnntextsuggest enabled = false clears out its contents on postback??? wtf.
        private string CustomerIdText
        {
            get { return hdnCustomerIdText.Value; }
            set { hdnCustomerIdText.Value = value; }
        }

        private string CustomerId
        {
            get
            {
                //1. Check QueryString...we're editing
                if (!string.IsNullOrEmpty(Request.QueryString["cid"])) return Request.QueryString["cid"];

                Controller controller = new Controller(entityId, environment);
                string idValue = string.Empty;
                string nameValue = string.Empty;

                //2. Check CustomerIdText...we did a search, adding a new customer to mix
                if (!string.IsNullOrEmpty(CustomerIdText))
                {
                    controller.DNNTextSuggestValues(CustomerIdText, out idValue, out nameValue, '-');
                    return idValue;
                }

                //3. Only thing left to use...either nothing or current text in the search box
                if (string.IsNullOrEmpty(txtCustomerId.Text)) return string.Empty; //don't waste a search

                controller.DNNTextSuggestValues(txtCustomerId.Text, out idValue, out nameValue, '-');
                return idValue;
            }
        }
        
        //private string customerId { get { return Request.QueryString["cid"]; } }
        private string PartQueryBase
        {
            get
            {
                if (partStatus.Equals("Approved")) return "SELECT C.NAME AS CUSTOMER_NAME, P.CUSTOMER_ID || ' - ' || C.NAME AS CUSTOMER_SUGGEST, "
                         + "CASE WHEN COALESCE(P.CUSTOMER_PART_ID, 'EMPTY') <> 'EMPTY' THEN 'Customer Part Id: ' || P.CUSTOMER_PART_ID ELSE NULL END AS CustomerPartId, "
                         + "P.* "
                         + "FROM CUSTOMER_PRICE P INNER JOIN CUSTOMER C ON P.CUSTOMER_ID = C.ID ";

                string remote = environment.Equals("Development") ? "ETA_TEST_VMLREI" : "ETA_VMLREI";

                return string.Format("SELECT C.NAME AS CUSTOMER_NAME, dbo.FormatTextSuggest(P.CustomerId, C.NAME) AS CUSTOMER_SUGGEST, "
                        + "CASE WHEN COALESCE(P.CustomerPartId, 'EMPTY') <> 'EMPTY' THEN 'Customer Part Id: ' + P.CustomerPartId ELSE NULL END AS CustomerPartId, "
                        + "P.CustomerId AS CUSTOMER_ID, P.Part_Id AS PART_ID, P.UnitOfMeasure AS SELLING_UM, P.CustomerPartId AS CUSTOMER_PART_ID, "
                        + "P.QuantityBreak1 AS QTY_BREAK_1, P.QuantityBreak2 AS QTY_BREAK_2, P.QuantityBreak3 AS QTY_BREAK_3, P.QuantityBreak4 AS QTY_BREAK_4, P.QuantityBreak5 AS QTY_BREAK_5, "
                        + "P.QuantityBreak6 AS QTY_BREAK_6, P.QuantityBreak7 AS QTY_BREAK_7, P.QuantityBreak8 AS QTY_BREAK_8, P.QuantityBreak9 AS QTY_BREAK_9, P.QuantityBreak10 AS QTY_BREAK_10, "
                        + "P.UnitPrice1 AS UNIT_PRICE_1, P.UnitPrice2 AS UNIT_PRICE_2, P.UnitPrice3 AS UNIT_PRICE_3, P.UnitPrice4 AS UNIT_PRICE_4, P.UnitPrice5 AS UNIT_PRICE_5, "
                        + "P.UnitPrice6 AS UNIT_PRICE_6, P.UnitPrice7 AS UNIT_PRICE_7, P.UnitPrice8 AS UNIT_PRICE_8, P.UnitPrice9 AS UNIT_PRICE_9, P.UnitPrice10 AS UNIT_PRICE_10, "
                        + "P.DefaultPrice AS DEFAULT_UNIT_PRICE FROM SalesPartInfos P INNER JOIN [H2M-REP\\H2M_REPLICATION].{0}.SYSADM.CUSTOMER C ON P.CustomerId = C.ID ", remote);
                //+ "INNER JOIN Parts ON P.Part_Id = Parts.Id "

            }
        }

        private DataTable LoadPriceBreaks(string CustomerId, out bool lockdown)
        {
            lockdown = false;
            Controller controller = new Controller(entityId, environment);

            //really only concerned with whether or not this is a valid customer, not if any breaks have yet been configured
            string sql = "SELECT COUNT(*) AS THECOUNT FROM CUSTOMER WHERE ID = @P0";
            int count = controller.vmfg.FetchSingleValue<int>(sql, "THECOUNT", CustomerId);
            lockdown = (count > 0);


            DataTable dt = null;
            if (partStatus.Equals("Approved"))
            {
                sql = PartQueryBase + "WHERE CUSTOMER_ID = @P0 AND PART_ID = @P1";
                dt = controller.vmfg.ExecuteReader(sql, CustomerId, sku);
                dt.TableName = "CUSTOMER_PRICE";
            }
            else
            {
                sql = PartQueryBase + "WHERE CustomerId = @P0 AND Part_Id = @P1";
                dt = controller.pdm.ExecuteReader(sql, CustomerId, PartId);
                dt.TableName = "CUSTOMER_PRICE";
            }

            return dt;
        }

        private void LockDownControls(object parent, bool breaksFound)
        {
            string css = breaksFound ? "Box" : "Box readonly";

            foreach (Control child in ((Control)parent).Controls)
            {
                switch (child.GetType().Name)
                {
                    case "TextBox":
                        TextBox txt = (TextBox)child;
                        txt.CssClass = css;
                        txt.Enabled = breaksFound;
                        break;
                    case "DropDownList":
                        DropDownList ddl = (DropDownList)child;
                        ddl.CssClass = css;
                        ddl.Enabled = breaksFound;
                        break;
                    case "DNNTextSuggest":
                        DNNTextSuggest suggest = (DNNTextSuggest)child;
                        suggest.CssClass = breaksFound ? "Box readonly" : "Box";
                        suggest.Enabled = !breaksFound;
                        break;
                }
                LockDownControls(child, breaksFound);
            }
        }

        protected override DataSet LoadData()
        {
            Controller controller = new Controller(entityId, environment);
            bool lockdown = false;
            DataTable dt = LoadPriceBreaks(CustomerId, out lockdown);

            controller.PopulateForm(pnlMain, dt);
            LockDownControls(pnlMain, lockdown);
            btnSearch.Visible = !lockdown; // (dt.Rows.Count == 0);
            btnClear.Visible = lockdown; // (dt.Rows.Count > 0);

            if (!string.IsNullOrEmpty(CustomerId))
            {
                if (!lockdown) hdnShowMessage.Value = CustomerId + " was not found.";
            }

            //to comply with base class definition
            return new DataSet();
        }
         
        protected void DoSearch(object obj, CommandEventArgs args)
        {
            Controller controller = new Controller(entityId, environment);

            string idVal = string.Empty;
            string nameVal = string.Empty;
            string raw = txtCustomerId.Text.Trim();
            if (string.IsNullOrEmpty(raw)) 
            {
                hdnShowMessage.Value = "Please specify the Customer to search for.";
                return;
            }

            CustomerIdText = raw;
            //controller.DNNTextSuggestValues(raw, out idVal, out nameVal, '-');
            
            LoadData(); //will parse CustomerId from CustomerIdText;
            //CustomerIdText = raw;
            txtCustomerId.Text = raw;
            if (ddlUnitOfMeasure.SelectedValue == "") ddlUnitOfMeasure.SelectedValue = "EA";
            //btnSearch.Visible = false;
            //btnClear.Visible = true;
        }

        protected void DoClear(object obj, CommandEventArgs args)
        {
            Controller controller = new Controller(entityId, environment);
            controller.StripAllErrors(pnlMain);
            CustomerIdText = string.Empty;

            LoadData();
            ddlUnitOfMeasure.SelectedValue = "EA";
            txtCustomerId.Focus();
            CustomerIdText = string.Empty;
            //btnSearch.Visible = true;
            //btnClear.Visible = false;
        }

        protected override bool Validate()
        {
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            if (string.IsNullOrEmpty(CustomerIdText)) controller.ValidateText(txtCustomerId, true, "Customer Id", ref msg);
            controller.ValidateText(ddlUnitOfMeasure, true, "Unit of Measure", ref msg);
            controller.ValidateDecimal(txtDefaultPrice, false, "Default Price", ref msg);

            TextBox[] quantities = new TextBox[] { txtQuantity1, txtQuantity2, txtQuantity3, txtQuantity4, txtQuantity5, 
                                                   txtQuantity6, txtQuantity7, txtQuantity8, txtQuantity9, txtQuantity10 };
            TextBox[] prices = new TextBox[] { txtPrice1, txtPrice2, txtPrice3, txtPrice4, txtPrice5, txtPrice6, txtPrice7, txtPrice8, txtPrice9, txtPrice10 };

            for (int index = 1; index <= 10; index++)
            {
                int? qty = controller.ValidateNumber(quantities[index - 1], false, "Quantity " + index.ToString(), ref msg);
                decimal? price = controller.ValidateDecimal(prices[index - 1], false, "Unit Price " + index.ToString(), ref msg);

                if ((qty.HasValue && !price.HasValue) || (!qty.HasValue && price.HasValue))
                {
                    msg += string.Format("Break {0}: Requires both a Quantity and Price value.\r\n", index);
                }
            }

            controller.DNNTestSuggestIsSelectionValid(txtCustomerId, "Customer Id", ref msg);


            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }


        protected void DoSave(object obj, CommandEventArgs args)
        {
            
            if (!Validate())
            {
                txtCustomerId.Text = CustomerIdText;
                return;
            }



            Controller controller = new Controller(entityId, environment);
            string idval = string.Empty;
            string nameVal = string.Empty;
            controller.DNNTextSuggestValues(CustomerIdText, out idval, out nameVal, '-');


            //controller.SavePriceBreak(this.UserInfo.Username, PartId, sku, idval, pnlMain);
            SavePriceBreak();


            CloseForm();
        }


        public void SavePriceBreak()
        {
            if (!partStatus.Equals("Approved"))
            {
                SavePendingPriceBreak();
                return;
            }

            Controller controller = new Controller(entityId, environment);
            string sql = "SELECT COUNT(*) AS THECOUNT FROM CUSTOMER_PRICE WHERE PART_ID = @P0 AND CUSTOMER_ID = @P1";
            int count = controller.vmfg.FetchSingleValue<int>(sql, "THECOUNT", sku, CustomerId);
            bool newRecord = (count == 0);


            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            controller.ParseForm(pnlMain, tables);
            TableFields table = tables["CUSTOMER_PRICE"];
            table.AddField("PART_ID", null, sku);
            if (table.Fields.ContainsKey("CUSTOMER_ID")) table.Fields.Remove("CUSTOMER_ID");
            table.AddField("CUSTOMER_ID", null, CustomerId);
            table.AddField("REC_PARSED", null, "N");
            Dictionary<string, FieldChange> fields = table.Fields;

            StringBuilder oldHistory = new StringBuilder();
            StringBuilder newHistory = new StringBuilder();

            string tabName = "Sales.Price Breaks";
            string fieldName = "Customer: " + CustomerId;
            oldHistory.AppendFormat("PartId: {0}, UM: {1}, Default Price: {2}<br />", fields["CUSTOMER_PART_ID"].oldHistory, fields["SELLING_UM"].oldHistory, fields["DEFAULT_UNIT_PRICE"].oldHistory);
            newHistory.AppendFormat("PartId: {0}, UM: {1}, Default Price: {2}<br />", fields["CUSTOMER_PART_ID"].newHistory, fields["SELLING_UM"].newHistory, fields["DEFAULT_UNIT_PRICE"].newHistory);
            for (int index = 1; index <= 10; index++)
            {
                oldHistory.AppendFormat("{0}:{1} - {2}<br />", index, fields["QTY_BREAK_" + index.ToString()].oldHistory, fields["UNIT_PRICE_" + index.ToString()].oldHistory);
                newHistory.AppendFormat("{0}:{1} - {2}<br />", index, fields["QTY_BREAK_" + index.ToString()].newHistory, fields["UNIT_PRICE_" + index.ToString()].newHistory);
            }


            if (newRecord)
            {
                controller.history.Log(PartId, this.UserInfo.Username, tabName, fieldName, string.Empty, "Created Price Breaks:<br />" + newHistory.ToString());
                controller.vmfg.InsertRecord("CUSTOMER_PRICE", "PART_ID", table.GetParametersAsArray(true));
            }
            else
            {
                if (table.IsDirty("PART_ID", "CUSTOMER_ID", "REC_PARSED"))
                {
                    controller.history.Log(PartId, this.UserInfo.Username, tabName, fieldName, oldHistory.ToString(), newHistory.ToString());
                    List<object> parameters = new List<object>();
                    sql = table.PrepareUpdateQuery("CUSTOMER_PRICE", true, ref parameters, "PART_ID", "CUSTOMER_ID", "REC_PARSED");
                    sql = table.FinalizeUpdateQuery(sql, ref parameters, "PART_ID", "CUSTOMER_ID");
                    controller.vmfg.ExecuteCommand(sql, parameters.ToArray());
                }
            }




        }

        private void SavePendingPriceBreak()
        {
            Controller controller = new Controller(entityId, environment);

            string sql = "SELECT COUNT(*) AS THECOUNT FROM SalesPartInfos WHERE PART_ID = @P0 AND CustomerId = @P1";
            int count = controller.pdm.FetchSingleValue<int>(sql, "THECOUNT", PartId, CustomerId);
            bool newRecord = (count == 0);


            //keep this here so that history logging is consistent between pdm and vmfg breaks
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            controller.ParseForm(pnlMain, tables);
            TableFields table = tables["CUSTOMER_PRICE"];
            table.AddField("PART_ID", null, sku);
            if (table.Fields.ContainsKey("CUSTOMER_ID")) table.Fields.Remove("CUSTOMER_ID");
            table.AddField("CUSTOMER_ID", null, CustomerId);
            Dictionary<string, FieldChange> fields = table.Fields;

            StringBuilder oldHistory = new StringBuilder();
            StringBuilder newHistory = new StringBuilder();

            string tabName = "Sales.Price Breaks";
            string fieldName = "Customer: " + CustomerId;
            oldHistory.AppendFormat("PartId: {0}, UM: {1}, Default Price: {2}<br />", fields["CUSTOMER_PART_ID"].oldHistory, fields["SELLING_UM"].oldHistory, fields["DEFAULT_UNIT_PRICE"].oldHistory);
            newHistory.AppendFormat("PartId: {0}, UM: {1}, Default Price: {2}<br />", fields["CUSTOMER_PART_ID"].newHistory, fields["SELLING_UM"].newHistory, fields["DEFAULT_UNIT_PRICE"].newHistory);
            for (int index = 1; index <= 10; index++)
            {
                oldHistory.AppendFormat("{0}:{1} - {2}<br />", index, fields["QTY_BREAK_" + index.ToString()].oldHistory, fields["UNIT_PRICE_" + index.ToString()].oldHistory);
                newHistory.AppendFormat("{0}:{1} - {2}<br />", index, fields["QTY_BREAK_" + index.ToString()].newHistory, fields["UNIT_PRICE_" + index.ToString()].newHistory);
            }

            //rename to pdm fields
            table.RenameField("CUSTOMER_ID", "CustomerId");
            table.RenameField("CUSTOMER_PART_ID", "CustomerPartId");
            table.RenameField("DEFAULT_UNIT_PRICE", "DefaultPrice");
            table.RenameField("SELLING_UM", "UnitOfMeasure");
            for (int index = 1; index <= 10; index++)
            {
                string orig = "QTY_BREAK_" + index.ToString();
                string tgt = "QuantityBreak" + index.ToString();
                table.RenameField(orig, tgt);

                orig = "UNIT_PRICE_" + index.ToString();
                tgt = "UnitPrice" + index.ToString();
                table.RenameField(orig, tgt);
            }
            fields.Remove("PART_ID");
            table.AddField("PART_ID", null, PartId);


            table.AddField("ModifiedDate", null, DateTime.Now);
            if (newRecord)
            {
                controller.history.Log(PartId, this.UserInfo.Username, tabName, fieldName, string.Empty, "Created Price Breaks:<br />" + newHistory.ToString());
                table.AddField("CreatedDate", null, DateTime.Now);
                controller.pdm.InsertRecord("SalesPartInfos", "ID", table.GetParametersAsArray(true));
            }
            else
            {
                if (table.IsDirty("PART_ID", "CustomerId"))
                {
                    controller.history.Log(PartId, this.UserInfo.Username, tabName, fieldName, oldHistory.ToString(), newHistory.ToString());
                    List<object> parameters = new List<object>();
                    sql = table.PrepareUpdateQuery("SalesPartInfos", true, ref parameters, "PART_ID", "CustomerId");
                    sql = table.FinalizeUpdateQuery(sql, ref parameters, "PART_ID", "CustomerId");
                    controller.pdm.ExecuteCommand(sql, parameters.ToArray());
                }
            }




        }


    
    
    }
}
 