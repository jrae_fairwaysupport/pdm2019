<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPDContainer.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPDContainer" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="H2MPDPartInformation.ascx" TagName="partInformation" TagPrefix="pdm" %>
<%@ Register Src="H2MPDVendorPrice.ascx" TagName="vendorPrice" TagPrefix="pdm" %>
<%@ Register Src="H2MPDSpecifications.ascx" TagName="specifications" TagPrefix="pdm" %>
<%@ Register Src="H2MPDManagement.ascx" TagName="management" TagPrefix="pdm" %>
<%@ Register Src="H2MPDBowker.ascx" TagName="bowker" TagPrefix="pdm" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server"  AutoPostBack="true" OnTabClick="LoadData"  >
        <Tabs>
            <telerik:RadTab Text="Part Information"/>
            <telerik:RadTab Text="Vendor Price" />
            <telerik:RadTab Text="Specifications" />
            <telerik:RadTab Text="Management" />
            <telerik:RadTab Text="Bowker" />
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;" >

        <telerik:RadPageView runat="server" ID="pgPartInformation" >
            <pdm:partInformation id="ctlPartInformation" runat="server" Editors="pd-*|ad-*" Editor="H2MPDPartInformationEditor"  />
        </telerik:RadPageView> 

        <telerik:RadPageView runat="server" ID="pgVendorPrice" >
            <pdm:vendorPrice id="ctlVendorPrice" runat="server" Editors="pd-*|ad-*" Editor="H2MPDVendorPriceEditor" />
        </telerik:RadPageView> 
    
        <telerik:RadPageView runat="server" ID="pgSpecifications">
            <pdm:specifications id="ctlSpecifications" runat="server"  Editors="pd-*|ad-*"  Editor="H2MPDSpecificationsEditor"  />
        </telerik:RadPageView> 

        <telerik:RadPageView runat="server" ID="pgManagement" >
            <pdm:management id="ctlManagement" runat="server" Editors="pd-*|ad-*" Editor="H2MPDManagementEditor" />
        </telerik:RadPageView> 


        <telerik:RadPageView runat="server" ID="pgBowker" >
            <pdm:bowker id="ctlBowker" runat="server"  Editors="pd-*|ad-*" Editor="H2MPDBowkerEditor" />
        </telerik:RadPageView> 

    </telerik:RadMultiPage>
</asp:Panel>


<asp:HiddenField ID="hdnModuleId" runat="server" Value="-1" />




