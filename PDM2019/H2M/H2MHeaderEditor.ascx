<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MHeaderEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MHeaderEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    .readonly { background-color:#e8e2e2; }
         
    span.required
    {
        
    }      
    
    input.required
    {
        border: solid 1px orange;
    }
        
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Part Id:" /></td>
            <td style="width:200px;"><asp:Label ID="labelDescription" runat="server" CssClass="SubSubHead" Text="Description:" /></td>
            <td style="width:200px;"></td>
            <td style="width:200px;"></td>
        </tr>
        <tr>
            <td >
                <asp:TextBox ID="txtPartId" runat="server" CssClass="Box required" Width="180" MaxLength="25" Table="Parts" Field="PartId" Placeholder="Part Id" />
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtDescription" runat="server" CssClass="Box required" Width="580" MaxLength="40" Table="Parts" Field="Description" Placeholder="Description"  />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="labelABC" runat="server" CssClass="SubSubHead" Text="ABC Code:<br />" />
                <asp:TextBox ID="txtabc" runat="server" CssClass="Box" Width="140" MaxLength="25" Table="Parts" Field="ABCCode" Placeholder="ABC Code" />
            </td>
            <td>
                <asp:Label ID="labelUPC" runat="server" CssClass="SubSubHead" Text="Item Type:<br />" />
                <asp:DropDownList ID="ddlItemType" runat="server" CssClass="Box" Width="180" Table="vwETAPartInfos" Field="ItemTypeName" TargetField="ItemType" />
            </td>
            <td />
            <td />
        </tr>
        <tr>
            <td><asp:CheckBox ID="chkThirdParty" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Third Party Item" Table="vwETAPartInfos" Field="IsThirdPartyItem" /></td>
            <td><asp:CheckBox ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Marketplace" Table="vwETAPartInfos" Field="ForMarketPlace" /></td>
            <td><asp:CheckBox ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Web Item" Table="vwETAPartInfos" Field="IsWebItem" /></td>
            <td><asp:CheckBox ID="CheckBox3" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Drop Ship Item" Table="vwETAPartInfos" Field="DropShipItem" /></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="labelExtended" runat="server" CssClass="SubSubHead" Text="Extended Description:<br />" />
                <asp:TextBox ID="txtExtended" runat="server" CssClass="Box" Width="580" MaxLength="500" TextMode="MultiLine" Height="100" Table="Parts" Field="ExtendedDescription" Placeholder="Extended Description" />
            </td>
        </tr>

    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
