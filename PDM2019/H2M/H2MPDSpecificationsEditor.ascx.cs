using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MPartSpecificationsEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Part Specifications"; }}

        protected override void SetDefaultValues(DataSet ds)
        {
            bool newRecord = NewRecord(ds, "vwProductSpecifications");
            chkPrintMaterial.Checked = !newRecord;
        }

        protected override void PopulateControls()
        {
            Controller controller = new Controller(entityId, environment);
            controller.PopulateDropDownListOptions(ddlMaterial, "Product Development", "Material", "Select Material");
            controller.PopulateDropDownListOptions(ddlBinderyType, "Product Development", "Bindery_Type", "Select Bindery Tye");
            controller.PopulateDropDownListOptions(ddlCoverPaperGrade, "Product Development", "Cover_Paper_Grade", "Select Cover Paper Grade");
            controller.PopulateDropDownListOptions(ddlBodyPaperGrade, "Product Development", "Body_Paper_Grade", "Select Body Paper Grade");
            controller.PopulateDropDownListOptions(ddlFinishingType, "Product Development", "Finishing_Type", "Select Finishing Tye");
            controller.PopulateDropDownListOptions(ddlCoverPaperColor, "Product Development", "Cover_Paper_Color", "Select Cover Paper Color");
            controller.PopulateDropDownListOptions(ddlBodyPaperColor, "Product Development", "Body_Paper_Color", "Select Body Paper Color");
            controller.PopulateDropDownListOptions(ddlCoverPaperWeight, "Product Development", "Cover_Paper_Weight", "Select Cover Paper Weight");
            controller.PopulateDropDownListOptions(ddlBodyPaperWeight, "Product Development", "Body_Paper_Weight", "Select Body Paper Weight");
            controller.PopulateDropDownListOptions(ddlCoverCoating, "Product Development", "Cover_Coating", "Select Cover Coating");
            controller.PopulateDropDownListOptions(ddlPackaging, "Product Development", "Packaging", "Select Packaging");
            controller.PopulateDropDownListOptions(ddlCoverPagesInkColor, "Product Development", "Cover_Pages_Ink_Color", "Select Cover Pages Ink Color");
            controller.PopulateDropDownListOptions(ddlBodyPagesInkColor, "Product Development", "Body_Pages_Ink_Color", "Select Body Pages Ink Color");
            controller.PopulateDropDownListOptions(ddlBookSet, "Product Development", "Book_Set_(single_6bks_6-pack_BB)", "Select Book Set");
            controller.PopulateDropDownListOptions(ddlBookType, "Product Development", "Book_Type_(hard_cover_or_soft_cover)", "Select Book Type");
            controller.PopulateDropDownListOptions(ddlDRA, "Product Development", "DRA", "Select DRA");
            controller.PopulateDropDownListOptions(ddlGenre, "Product Development", "Genre", "Select Genre");
            controller.PopulateDropDownListOptions(ddlGuidedReadingLevel, "Product Development", "Guided_Reading_Level", "Select Guided Reading Level");
            controller.PopulateDropDownListOptions(ddlTextType, "Product Development", "Text_Type", "Select Text Type");    
        }

        protected override bool Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);
            if (chkOther.Checked) controller.ValidateText(txtOther, true, "Other OEM Packaging", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {

            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {

                //Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "id", PartId.ToString(), "r", viewRole), true);
                CloseForm();
                return;
            }


            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwProductSpecifications");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);
            SaveTableEdits(tables, "vwProductSpecifications", "Product Development.Specifications", newRecord, controller, "ProductSpecifications_PartId");
            SaveTableEdits(tables, "Parts", "Product Development.Specifications", false, controller, "Id");
            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwProductSpecifications")) return;

            TableFields table = tables["vwProductSpecifications"];
            if (!table.IsDirty()) return;


            //custom logic
            Dictionary<string, FieldChange>fields = table.Fields;
            if (fields["PrintSpecs"].HasChanged() || fields["PrintSpecs2"].HasChanged())
            {
                string newValue = fields["PrintSpecs"].newValue.ToString();
                if (fields["PrintSpecs2"].HasChanged())
                {
                    if (!string.IsNullOrEmpty(newValue)) newValue += "|";
                    newValue += fields["PrintSpecs2"].newValue.ToString();
                }
                fields["PrintSpecs"].newValue = newValue;
            }
            table.RemoveField("PrintSpecs2");
        }








    }
}
 