using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MPDManagementEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Product Management"; }}

        protected override void PopulateControls()
        {
            

            Controller controller = new Controller(entityId, environment);
            controller.PopulateDropDownListOptions(ddlBrandingLogoStatus, "Product Development", "h2m_Branding_Logo_Status", "Select Branding Logo Status");
            controller.PopulateDropDownListOptions(ddlReorderFlag, "Product Development", "Reorder_Flag", "Select Reorder Flag");
            controller.PopulateDropDownListOptions(ddlReprintStatus, "Product Development", "Reprint_Status", "Select Reprint Status");
            controller.PopulateDropDownListOptions(ddlFileStatus, "Product Development", "File_Status", "Select File Status");

         }


        protected override void SetRequirements()
        {
            //labelPartId.CssClass = "SubSubHead required";

        }

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = true; 
        }

        protected override bool Validate()
        {
            
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;


            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
        
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);

            PrepTableData(tables, controller);
            
            SaveTableEdits(tables, "vwProductManagement", "Product Development.Management", NewRecord(ds, "vwProductManagement"), controller, "ProductManagement_PartId");

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwProductManagement")) return;

            TableFields table = tables["vwProductManagement"];
            if (!table.IsDirty()) return;


            //custom logic
            table.AddField("UpdatedBy", null, this.UserInfo.Username);
        }
     
    }
}
 