using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MPDPartInformationEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Part Information"; }}

        protected override void PopulateControls()
        {
            

            Controller controller = new Controller(entityId, environment);
            PopulateProductCategory(controller, ddlProductCategory1, 1);
            PopulateProductCategory(controller, ddlProductCategory2, 2);
            PopulateProductCategory(controller, ddlProductCategory3, 3);
            PopulateProductCategory(controller, ddlProdcutCategory4, 4);
            controller.PopulateDropDownListOptions(ddlDiscontinuedReason, "Product Development", "Discontinued_reason", "Select Discontinued Reason");
            controller.PopulateDropDownListOptions(ddlNMFCCode, "Product Development", "NMFC Codes", "Select NMFC Code");
            controller.PopulateDropDownListOptions(ddlLowGrade, "Product Development", "Grades", "Select Low Grade");
            controller.PopulateDropDownListOptions(ddlHighGrade, "Product Development", "Grades", "Select High Grade");
            controller.PopulateDropDownListOptions(ddlItemDiscount, "Product Development", "Item Discount", "Select Item Discount");


         }

        private void PopulateProductCategory(Controller controller, DropDownList ddl, int index)
        {
            string sql = string.Format("SELECT * FROM LR_CATEGORY_{0} ORDER BY DESCRIPTION", index);
            DataTable dt = controller.vmfg.ExecuteReader(sql);

            DataRow dr = dt.NewRow();
            dr["ID"] = -1;
            dr["DESCRIPTION"] = "Product Category " + index.ToString();
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "DESCRIPTION";
            ddl.DataValueField = "ID";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        protected override void SetRequirements()
        {
            //labelPartId.CssClass = "SubSubHead required";

        }

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = true; 
        }

        protected override bool Validate()
        {
            
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            controller.ValidateDecimal(txtMaterialCost, false, "Material Cost", ref msg);
            controller.ValidateDecimal(txtRetailPrice, false, "Retail Price", ref msg);
            controller.ValidateDecimal(txtNetPrice, false, "Net Price", ref msg);

            controller.ValidateDecimal(txtLength, false, "Length", ref msg);
            controller.ValidateDecimal(txtWidth, false, "Width", ref msg);
            controller.ValidateDecimal(txtHeight, false, "Height", ref msg);
            controller.ValidateDecimal(txtWeight, false, "Weight", ref msg);
            controller.ValidateDecimal(txtPackageSize, false, "Package Size", ref msg);

            controller.DNNTestSuggestIsSelectionValid(txtPreferredVendor, "Preferred Vendor", ref msg);
            controller.DNNTestSuggestIsSelectionValid(txtSecondaryVendor, "Secondary Vendor", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
        
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);

            SaveTableEdits(tables, "Parts", "Product Development.Part Information", NewRecord(ds, "Parts"), controller, "Id");
            SaveTableEdits(tables, "vwPartInfos", "Product Development.Part Information", NewRecord(ds, "vwPartInfos"), controller);
            SaveTableEdits(tables, "vwEtaPartInfos", "Product Development.Part Information", NewRecord(ds, "vwEtaPartInfos"), controller);
            SaveTableEdits(tables, "vwMarketingInfos", "Product Development.Part Information", NewRecord(ds, "vwMarketingInfos"), controller);

            CloseForm();
        }

       
     
    }
}
 