using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MCreativeTabEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Creative"; } }


        protected override void SetDefaultValues(DataSet ds)
        {
            //bool newRecord = NewRecord(ds, "vwCreativeInfos");
            //chkToggleLabels.Checked = !newRecord;
            chkToggleLabels.Checked = true;
        }

        protected override bool Validate()
        {
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;



            hdnCreativeMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwCreativeInfos");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwCreativeInfos", "Creative.Part Information", newRecord, controller);

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller) 
        {
            if (!tables.ContainsKey("vwCreativeInfos")) return;


            TableFields table = tables["vwCreativeInfos"];
            if (!table.IsDirty()) return;

            //custom
        }
    }
}
 