<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingWebEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingWebEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    .readonly { background-color:#e8e2e2; }
         
    span.required
    {
        border-bottom: double 2px red;
    }          
    
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Web Title:" /></td>
            <td style="width:200px;"></td>
            <td style="width:200px;"><asp:Label ID="labelbuyer" runat="server" CssClass="SubSubHead" Text="Warning Copy:" /></td>
            <td style="width:200px;"></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TextBox ID="txtBurdenPercent" runat="server" CssClass="Box" Width="380" MaxLength="250" Table="vwMarketingWeb" Field="WebTitle" Placeholder="Web Title"   />
            </td>
            <td colspan="2">
                <asp:TextBox ID="TextBox1" runat="server" CssClass="Box" Width="300" MaxLength="250" Table="vwMarketingWeb" Field="WarningCopy" Placeholder="Warning Copy"   />
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <asp:Label ID="labelWebCopy" runat="server" CssClass="SubSubHead" Text="Copy:<br />" />
                <asp:TextBox ID="txtwebcopy" runat="server" CssClass="Box" Width="780" Height="100" TextMode="MultiLine" Table="vwMarketingWeb" Field="WebCopy" Placeholder="Copy" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Headline:<br />" />
                <asp:TextBox ID="TextBox2" runat="server" CssClass="Box" Width="380" MaxLength="250" Table="vwMarketingWeb" Field="Headline" Placeholder="Headline" />
            </td>
            <td colspan="2">
                <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Meta Product Title:<br />" />
                <asp:TextBox ID="TextBox3" runat="server" CssClass="Box" Width="380" MaxLength="250" Table="vwMarketingWeb" Field="MetaProductTitle" Placeholder="Meta Product Title" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Meta Description:<br />" />
                <asp:TextBox ID="TextBox4" runat="server" CssClass="Box" Width="780" Height="100" TextMode="MultiLine" MaxLength="500" Table="vwMarketingWeb" Field="MetaDescription" Placeholder="Meta Description" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="label4" runat="server" CssClass="SubSubHead" Text="Brand:<br />" />
                <asp:DropDownList ID="ddlBrand" runat="server" CssClass="Box" Width="180" Table="vwMarketingWeb" Field="Brand" />
            </td>
            <td>
                <asp:Label ID="label5" runat="server" CssClass="SubSubHead" Text="Grade (Facet):<br />" />
                <asp:TextBox ID="TextBox5" runat="server" CssClass="Box" Width="180" MaxLength="300" Table="vwMarketingWeb" Field="GradeFacet" Placeholder="Grade (Facet)" />
            </td>
            <td>
                <asp:Label ID="label6" runat="server" CssClass="SubSubHead" Text="Age Level:<br />" />
                <asp:TextBox ID="TextBox6" runat="server" CssClass="Box" Width="180" MaxLength="300" Table="vwMarketingWeb" Field="AgeLevel" Placeholder="Age Level" />
            </td>
            <td />
        </tr>

        <tr>
            <td >
                <asp:Label ID="label7" runat="server" CssClass="SubSubHead" Text="Item is Best Seller:<br />" />
                <telerik:RadDatePicker id="dtBestSeller" runat="server" width="180" Table="vwMarketingWeb" Field="BestSeller" Placeholder="Best Seller" />
            </td>
            <td >
                <asp:Label ID="label8" runat="server" CssClass="SubSubHead" Text="Item is New:<br />" />
                <telerik:RadDatePicker id="dtNew" runat="server" width="180" Table="vwMarketingWeb" Field="New" Placeholder="New" />
            </td>
            <td >
                <asp:Label ID="label9" runat="server" CssClass="SubSubHead" Text="Item is On Sale:<br />" />
                <telerik:RadDatePicker id="dtOnSale" runat="server" width="180" Table="vwMarketingWeb" Field="OnSale" Placeholder="On Sale" />
            </td>
            <td />
        </tr>
        <tr>
            <td><asp:CheckBox ID="chkLive" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Live Material" Table="vwMarketingWeb" Field="LiveMaterial" /></td>
            <td><asp:CheckBox ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Web Exclusive" Table="vwMarketingWeb" Field="WebExclusive" /></td>
            <td><asp:CheckBox ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Product Outlet" Table="vwMarketingWeb" Field="Outlet" /></td>
            <td><asp:CheckBox ID="CheckBox3" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Show Single SKU Page" Table="vwMarketingWeb" Field="ShowSkuPage" /></td>
        </tr>
        <tr>
            <td valign="bottom">
                <asp:Label ID="label11" runat="server" CssClass="SubSubHead" Text="Number of Students:<br />" />
                <asp:TextBox ID="TextBox7" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwMarketingWeb" Field="NumberOfStudents" Placeholder="Number of Students" />
            </td>
            <td valign="bottom">
                <asp:Label ID="label10" runat="server" CssClass="SubSubHead" Text="Display Order:<br />" />
                <asp:TextBox ID="txtDisplayOrder" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwMarketingWeb" Field="DisplayOrder" Placeholder="Display Order" />
            </td>
            <td valign="bottom">
                <asp:Label ID="label12" runat="server" CssClass="SubSubHead" Text="Unit Measure Description:<br />" />
                <asp:TextBox ID="TextBox8" runat="server" CssClass="Box" Width="180" MaxLength="100" Table="vwMarketingWeb" Field="UnitMeasureDescription"  Placeholder="Unit Measure Description"/>
            </td>
            <td valign="bottom">
                <asp:CheckBox ID="chkAddtocatalog" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Add to Catalog Request" Table="vwMarketingWeb" Field="AddToCatalogRequest" />
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                <asp:Label ID="label13" runat="server" CssClass="SubSubHead" Text="Overstock Min Quantity:<br />" />
                <asp:TextBox ID="TextBox9" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwMarketingWeb" Field="OverstockMinQty" Placeholder="Overstock Min Qty."/>
            </td>
            <td valign="bottom">
                <asp:Label ID="label14" runat="server" CssClass="SubSubHead" Text="Web Seearch Sort Priority:<br />" />
                <asp:TextBox ID="TextBox10" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwMarketingWeb" Field="SearchSortPriority" Placeholder="Search Sort Priority" />
            </td>
            <td />
            <td />
        </tr>
        <tr>
            <td valign="bottom" colspan="4">
                <asp:Label ID="label15" runat="server" CssClass="SubSubHead" Text="Search Keywords:<br />" />
                <asp:TextBox ID="TextBox11" runat="server" CssClass="Box" Width="780" MaxLength="300" Height="50" TextMode="MultiLine" Table="vwMarketingWeb" Field="SearchKeywords" Placeholder="Search Keywords"/>
            </td>
            
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="labelVideocopy" runat="server" CssClass="SubSubHead" Text="Video Copy/Description:<br />" />
                <asp:TextBox ID="TextBox12" runat="server" CssClass="Box" Width="380" MaxLength="300" Height="50" TextMode="MultiLine" Table="vwMarketingWeb" Field="VideoDescription" Placeholder="Video Copy/Description"/>
            </td>
            <td colspan="2">
                <asp:Label ID="label16" runat="server" CssClass="SubSubHead" Text="Video Url:<br />" />
                <asp:TextBox ID="TextBox13" runat="server" CssClass="Box" Width="380" MaxLength="300" Height="50" TextMode="MultiLine" Table="vwMarketingWeb" Field="VideoUrl" Placeholder="Video Url"/>
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                <asp:CheckBox ID="chkDigital" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Digital Delivery" Table="vwMarketingWeb" Field="DigitalDelivery" />
            </td>
            <td valign="bottom" colspan="2">
                <asp:CheckBox ID="CheckBox4" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Digital Delivery Exclude from Web" Table="vwMarketingWeb" Field="DigitalDeliveryExcludFromWeb" />
            </td>
            <td valign="bottom">
                <asp:CheckBox ID="CheckBox5" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;eBooks Collection" Table="vwMarketingWeb" Field="EBookCollection" />
            </td>
            <td />
        </tr>
        <tr>
            <td valign="bottom" colspan="4">
                <asp:Label ID="label17" runat="server" CssClass="SubSubHead" Text="eBook Download Instruction:<br />" />
                <asp:TextBox ID="TextBox14" runat="server" CssClass="Box" Width="780" Height="100" TextMode="MultiLine" Table="vwMarketingWeb" Field="EBookDownloadInstruction" Placeholder="eBook Download Instruction"/>
            </td>
            
        </tr>
        <tr>
            <td colspan="2" valign="top">
                <asp:Label ID="label18" runat="server" CssClass="SubSubHead" Text="eBook Print Url:<br />" />
                <asp:TextBox ID="TextBox15" runat="server" CssClass="Box" Width="380" MaxLength="250" Height="50" TextMode="MultiLine" Table="vwMarketingWeb" Field="EBookPrintUrl" Placeholder="eBook Print Url"/>
            </td>
            <td valign="top">
                <asp:Label ID="label19" runat="server" CssClass="SubSubHead" Text="eBook Purchase Type:<br />" />
                <asp:DropDownList ID="ddlPurchaseType" runat="server" CssClass="Box" Width="180" Table="vwMarketingWeb" Field="EBookPurchaseType" />
            </td>
            <td valign="top">
                <asp:Label ID="label20" runat="server" CssClass="SubSubHead" Text="eBook Subject:<br />" />
                <asp:DropDownList ID="ddlSubject" runat="server" CssClass="Box" Width="180" Table="vwMarketingWeb" Field="EBookSubject" />
            </td>

        </tr>
        <tr>
            <td colspan="3" valign="bottom">
                <asp:Label ID="label21" runat="server" CssClass="SubSubHead" Text="eBook Standards:<br />" />
                <asp:TextBox ID="TextBox16" runat="server" CssClass="Box" Width="580" Height="50" TextMode="MultiLine" Table="vwMarketingWeb" Field="EBookStandards" Placeholder="eBook Standards"/>
            </td>
            <td valign="bottom">
                <asp:CheckBox ID="CheckBox6" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Available for Sale for Portal" Table="vwMarketingWeb" Field="AvailableForSaleForPortal" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="label22" runat="server" CssClass="SubSubHead" Text="Cross Sell:<br />" />
                <asp:TextBox ID="TextBox17" runat="server" CssClass="Box" Width="380" MaxLength="300" Height="50" TextMode="MultiLine" Table="vwMarketingWeb" Field="CrossSell" Placeholder="Cross Sell"/>
            </td>
            <td colspan="2">
                <asp:Label ID="label23" runat="server" CssClass="SubSubHead" Text="Accessories:<br />" />
                <asp:TextBox ID="TextBox18" runat="server" CssClass="Box" Width="380" MaxLength="300" Height="50" TextMode="MultiLine" Table="vwMarketingWeb" Field="Accessories" Placeholder="Accessories"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="label24" runat="server" CssClass="SubSubHead" Text="Other Assets:<br />" />
                <asp:TextBox ID="TextBox19" runat="server" CssClass="Box" Width="380" MaxLength="300" Height="50" TextMode="MultiLine" Table="vwMarketingWeb" Field="OtherAssets" Placeholder="Other Assets"/>
            </td>
            <td />
            <td />
        </tr>

    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
