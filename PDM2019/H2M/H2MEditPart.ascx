<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MEditPart.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MEditPart" %>

<%@ Register Src="H2MSidebar.ascx" TagName="sidebar" TagPrefix="pdm" %>
<%@ Register Src="H2MPDContainer.ascx" TagName="productDevelopment" TagPrefix="pdm" %>
<%@ Register Src="H2MPCHContainer.ascx" TagName="purchasing" TagPrefix="pdm" %>
<%@ Register Src="H2MQualityContainer.ascx" TagName="quality" TagPrefix="pdm" %>
<%@ Register Src="H2MCreativeTab.ascx" TagName="creative" TagPrefix="pdm" %>
<%@ Register Src="H2MSalesTab.ascx" TagName="h2msales" TagPrefix="pdm" %>
<%@ Register Src="H2MMarketingTab.ascx" TagName="h2mMarketing" TagPrefix="pdm" %>

<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Paging" TagPrefix="pg" Namespace="Paging" %>
<%@ Register Assembly="FilterParser" TagPrefix="fltr" Namespace="FilterParser" %>

<style>
    p { padding-top:4px;
        margin-bottom: 0px;
    }
    
    span.placeholder { border-bottom: solid 1px #e8e2e2; }
   
    label 
    {
        font-size:12px;
        color: #000000;
        font-weight:bold;
    }
    
    A, A:link, A:active, A:visited, A:hover, .Link_list li { color: #0932bf; }
    
    span.required
    {
        border-bottom: solid 1px orange;
    }
    
    input.required
    {
        border: solid 1px orange;
    }
        

</style>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


        var openForm = jQuery('[id*="hdnOpenForm"]').val();
        jQuery('[id*="hdnOpenForm"]').val("");
        if (openForm != "") eval(openForm);

        //var url = jQuery('[id*="hdnPDF"]').val();
        //if (url != "") {
        //    window.open(url);
        // }
        // jQuery('[id*="hdnPDF"]').val("");

    });


</script>

<asp:Panel ID="pnlMain" runat="server" >

    <asp:Panel ID="pnlPartName" runat="server">
        <asp:Label id="lblPartIdLabel" runat="server" CssClass="SubHead" Text="Part Id: " />
        <asp:Label ID="lblPartId" runat="server" CssClass="SubSubHead" Table="Parts" Field="PartId" />
        <asp:Label ID="lblPartIdSpace" runat="server" CssClass="SubSubHead" Text=" - " />
        <asp:Label ID="lblDescription" runat="server" CssClass="SubSubHead" Table="Parts" Field="Description" />
        <asp:ImageButton ID="lnkEdit" runat="server" ImageUrl="~/images/edit.gif" ToolTip="Edit Part Header" OnCommand="DoEditHeader" />
    </asp:Panel>
</asp:Panel>

<asp:Panel ID="pnlTabs" runat="server" >
    <table width="1000" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;" align="left" valign="top">
                <pdm:sidebar id="ctlSideBar" runat="server" />
            </td>

            <td style="width:800px;padding-left:4px;" align="left" valign="top">
                <asp:Panel ID="pnlMainTabs" runat="server">
                    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server" AutoPostBack="true" OnTabClick="LoadData">
                        <Tabs>
                            <telerik:RadTab Text="Product Development"/>
                            <telerik:RadTab Text="Purchasing" />
                            <telerik:RadTab Text="Quality Assurance" />
                            <telerik:RadTab Text="Creative" />
                            <telerik:RadTab Text="Sales" />
                            <telerik:RadTab Text="Marketing" />
                        </Tabs>
                    </telerik:RadTabStrip>

                    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;" >

                        <telerik:RadPageView runat="server" ID="pgProductDevelopment" >
                            <asp:Panel id="pnlPart" runat="server" Width="780" >
                                <pdm:productDevelopment id="ctlProductDevelopmentContainer" runat="server" />
                            </asp:Panel>
                        </telerik:RadPageView> 

                        <telerik:RadPageView runat="server" ID="pgPurchasing" >
                            <asp:Panel id="pnlPurchasing" runat="server" Width="780" >
                                <pdm:purchasing id="ctlPurchasingContainer" runat="server" />
                            </asp:Panel>
                        </telerik:RadPageView> 

                        <telerik:RadPageView runat="server" ID="pgQualityAssurance" >
                            <asp:Panel id="pnlQualityAsurance" runat="server" Width="780">
                                <pdm:quality id="ctlQualityContainer" runat="server" />
                            </asp:Panel>
                        </telerik:RadPageView> 

                        <telerik:RadPageView runat="server" ID="pgCreative" >
                            <asp:Panel id="pnlCreative" runat="server" Width="780">
                                <pdm:creative id="ctlCreativeTab" runat="server" />
                            </asp:Panel>
                        </telerik:RadPageView> 

                        <telerik:RadPageView runat="server" ID="pgSales" >
                            <asp:Panel id="pnlSales" runat="server" Width="780" >
                                <pdm:h2msales id="ctlSalesTab" runat="server" />
                            </asp:Panel>
                        </telerik:RadPageView> 

                        <telerik:RadPageView runat="server" ID="pgMarketingTb" >
                            <asp:Panel id="pnlMarketing" runat="server" Width="780" >
                                <pdm:h2mMarketing id="ctlMarketingTab" runat="server" />
                            </asp:Panel>
                        </telerik:RadPageView> 

                    </telerik:RadMultiPage>
                </asp:Panel>

            </td>
        </tr>
    </table>
</asp:Panel>



<asp:HiddenField ID="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnOpenForm" runat="server" Value="" />
<asp:HiddenField ID="hdnViewRole" runat="server" Value="" />
<asp:HiddenField ID="hdnPDF" runat="server" Value="" />

