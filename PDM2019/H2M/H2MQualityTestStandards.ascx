<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MQualityTestStandards.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MQualityTestStandards" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
    .readonly { background-color:#e8e2e2; }
    
</style>

<script language="javascript" type="text/javascript">
    function ToggleMore() {
        lnk = jQuery('[id*="lnkViewMore"]');

        if (lnk.text() == "View More") {
            lnk.text("View Less");
            jQuery('[id*="lblSpecs"]').css("max-height", "");
        }
        else {
            lnk.text("View More");
            jQuery('[id*="lblSpecs"]').css("max-height", "200px");
        }
        return false;
    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td colspan="3" class="SubHead">USA: ASTM F963 Toy Safety</td>
            </tr>
            <tr>
                <td class="SubSubHead" style="width:320px;padding-left:15px;"><asp:Image ID="imgLatexAllergen" runat="server" Table="vwTestStandards" Field="MechanicalAndPhysical" />&nbsp;Mechanical &amp; Physical</td>
                <td class="SubSubHead" style="width:215px;"><asp:Image ID="imgPvcAllergen" runat="server" Table="vwTestStandards" Field="FlammabilityTest" />&nbsp;Flammability Test</td>
                <td class="SubSubHead" style="width:215px;"><asp:Image ID="imgBPAAllergen" runat="server" Table="vwTestStandards" Field="HeavyMetalTest" />&nbsp;Heavy Metal Test</td>
            </tr>


            <tr>
                <td colspan="3" class="SubHead">USA: CPSIA 2008 (H.R.4040) Children's Product</td>
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image1" runat="server" Table="vwTestStandards" Field="LeadContentOnSurfaceCoating" />&nbsp;Lead Content on Surface Coating (16 CFR 1303)</td>
                <td class="SubSubHead"><asp:Image ID="Image2" runat="server" Table="vwTestStandards" Field="LeadContentInSubstrate" />&nbsp;Lead Content in Substrate</td>
                <td class="SubSubHead"><asp:Image ID="Image3" runat="server" Table="vwTestStandards" Field="OtherTest" />&nbsp;Other Test</td>
                
            </tr>

            <tr>
                <td class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image4" runat="server" Table="vwTestStandards" Field="TestForBisphenol" />&nbsp;Test for Bisphenol A (BPA)</td>
                <td class="SubSubHead" ><asp:Image ID="Image6" runat="server" Table="vwTestStandards" Field="Lead" />&nbsp;Custom Testing - Contact QA</td>
                <td class="SubSubHead" ><asp:Image ID="Image7" runat="server" Table="vwTestStandards" Field="PhthalatesContent" />&nbsp;Phthalates</td>
            </tr>

            <tr>
                <td class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image5" runat="server" Table="vwTestStandards" Field="OtherCheck" />&nbsp;Wood Moisture 7% < MOISTURE LEVEL < 14%</td>
                <td class="SubSubHead" >&nbsp;</td>
                <td class="SubSubHead" ><asp:Image ID="Image9" runat="server" Table="vwTestStandards" Field="Other2Check" />&nbsp;ASTM D4236: LHAMA</td>
            </tr>

            <tr>
                <td colspan="2" class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image11" runat="server" Table="vwTestStandards" Field="Other1Check" />&nbsp;GCC Required (16 CFR 1303/CPSIA SECTION 102)</td>
                <td class="SubSubHead" ></td>
            </tr>


            <tr>
                <td colspan="3" class="SubHead">EU: EN71 Toy Safety</td>
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image8" runat="server" Table="vwTestStandards" Field="EN71Part1" />&nbsp;Part 1: Physical & Mechanical</td>
                <td class="SubSubHead"><asp:Image ID="Image10" runat="server" Table="vwTestStandards" Field="EN71Part2" />&nbsp;Part 2: Flammability Test</td>
                <td class="SubSubHead"><asp:Image ID="Image12" runat="server" Table="vwTestStandards" Field="EN71Part3" />&nbsp;Part 3: Toxic Elements Test</td>
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image13" runat="server" Table="vwTestStandards" Field="EN71TotalCadmiun" />&nbsp;84/500/EC Total Cadmium</td>
                <td class="SubSubHead"><asp:Image ID="Image14" runat="server" Table="vwTestStandards" Field="EN71Phthalates" />&nbsp;2005/84/EC Phthalates</td>
                <td class="SubSubHead"></td>
            </tr>
            
            <tr>
                <td colspan="3" class="SubHead">Canadian Toys Safety Requirement (CCPSA)</td>
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image15" runat="server" Table="vwTestStandards" Field="CanadianBenzeneContent" />&nbsp;Benzene Content</td>
                <td class="SubSubHead"><asp:Image ID="Image16" runat="server" Table="vwTestStandards" Field="ToyRegulations" />&nbsp;Toy Regulations (SOR/2011-17)</td>
                <td class="SubSubHead"><asp:Image ID="Image18" runat="server" Table="vwTestStandards" Field="CanadianPhysicalAndMechanical" />&nbsp;Physical & Mechanical</td>
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image17" runat="server" Table="vwTestStandards" Field="CanadianPhthalates" />&nbsp;Phthalates Regulations (SOR/2010-298)</td>
                <td class="SubSubHead"><asp:Image ID="Image19" runat="server" Table="vwTestStandards" Field="CanadianFlammabilityTest" />&nbsp;Flammability Test</td>
                <td class="SubSubHead"><asp:Image ID="Image20" runat="server" Table="vwTestStandards" Field="CanadianToxicElementsTest" />&nbsp;Toxic Elements Test</td>
            </tr>
            <tr>
                <td colspan="2" class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image21" runat="server" Table="vwTestStandards" Field="TotalLeadContentInSurfaceCoatings" />&nbsp;Total Lead Content in Surface Coatings (SOR/2011-14)</td>
                <td class="SubSubHead"></td>
            </tr>

            
            <tr>
                <td colspan="3" class="SubHead">Other International Toy Safety Requirements</td>
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-left:15px;"><asp:Image ID="Image22" runat="server" Table="vwTestStandards" Field="InternationalPhysicalAndMechanical" />&nbsp;International Testing - Contact QA</td>
                <td class="SubSubHead"><asp:Image ID="Image23" runat="server" Table="vwTestStandards" Field="ASNZS8124" />&nbsp;AS/NZS ISO 8124</td>
                <td class="SubSubHead"></td>
            </tr>
	
	
	
	
	
	



        </table>
    </asp:Panel>
</asp:Panel>          
	
	<hr />

	
	


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




