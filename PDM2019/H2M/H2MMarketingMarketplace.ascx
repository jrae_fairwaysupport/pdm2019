<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingMarketplace.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingMarketplace" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:187px;" class="SubSubHead">Marketplace Title:</td>
                <td style="width:563px;" colspan="3" ><asp:Label id="Label22" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMarketplace" Field="MarketplaceTitle" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Marketplace Description:</td>
                <td colspan="3" ><asp:Label id="Label14" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMarketplace" Field="MarketplaceDescriptionBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Features 1:</td>
                <td style="width:187px;" ><asp:Label id="Label1" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Features1" /></td>
                <td class="SubSubHead" valign="top" style="width:187px;">Features 2:</td>
                <td style="width:189px;" valign="top" ><asp:Label ID="lblfeatures3" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Features2" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Features 3:</td>
                <td ><asp:Label id="Label8" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Features3" /></td>
                <td class="SubSubHead" valign="top">Features 4:</td>
                <td  valign="top" ><asp:Label ID="Label9" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Features4" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Features 5:</td>
                <td  ><asp:Label id="Label10" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Features5" /></td>
                <td />
                <td />
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Keywords 1:</td>
                <td ><asp:Label id="Label11" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Keywords1" /></td>
                <td class="SubSubHead" valign="top">Keywords 2:</td>
                <td  valign="top" ><asp:Label ID="Label12" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Keywords2" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Keywords 3:</td>
                <td ><asp:Label id="Label13" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Keywords3" /></td>
                <td class="SubSubHead" valign="top">Keywords 4:</td>
                <td  valign="top" ><asp:Label ID="Label15" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Keywords4" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Keywords 5:</td>
                <td ><asp:Label id="Label16" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Keywords5" /></td>
                <td />
                <td />
            </tr>
    
            <tr>
                <td colspan="4" class="Head" style="border-bottom:solid 1px #e8e2e2;padding-top:15px;">Marketplaces</td>
            </tr>

            <tr>
                <td class="SubSubHead"><asp:Image ID="imgaznsc" runat="server" Table="vwMarketingMarketplace" Field="MKAmazonSellerCentral" />&nbsp;Amazon - Seller Central</td>
                <td class="SubSubHead"><asp:Image ID="imgwmmar" runat="server" Table="vwMarketingMarketplace" Field="WalmartMarketplace" />&nbsp;Walmart - Marketplace</td>
                <td class="SubSubHead"><asp:Image ID="imgzul" runat="server" Table="vwMarketingMarketplace" Field="Zulily" />&nbsp;Zulily</td>
                <td />
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgaznvc" runat="server" Table="vwMarketingMarketplace" Field="MKAmazonVendorCentral" />&nbsp;Amazon - Vendor Central</td>
                <td class="SubSubHead"><asp:Image ID="imgwmv" runat="server" Table="vwMarketingMarketplace" Field="WalmartVendor" />&nbsp;Walmart - Vendor</td>
                <td class="SubSubHead" valign="top">Other:</td>
                <td ><asp:Label id="Label17" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Other1" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Other:</td>
                <td  valign="top" ><asp:Label ID="Label18" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Other2" /></td>
                <td class="SubSubHead" valign="top">Other:</td>
                <td ><asp:Label id="Label19" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="Other3" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-top:10px;">ASIN:</td>
                <td style="padding-top:10px;"><asp:Label ID="labelasin" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="ASIN" /></td>
                <td  style="padding-top:10px;" class="SubSubHead">Amazon List Price:</td>
                <td  style="padding-top:10px;"><asp:Label ID="lblaznlp" runat="server" CssClass="Normal placeholder" Width="167" style="text-align:right;" Format="Decimal" Places="4" Table="vwMarketingMarketplace" Field="AmazonListPrice" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Amazon Min. Order Qty.:</td>
                <td><asp:Label ID="label20" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="AmazonMinOrderQuantity" /></td>
                <td class="SubSubHead">Amazon Price:</td>
                <td><asp:Label ID="Label21" runat="server" CssClass="Normal placeholder" Width="167" style="text-align:right;" Format="Decimal" Places="4" Table="vwMarketingMarketplace" Field="AmazonPrice" /></td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgazonscc" runat="server" Table="vwMarketingMarketplace" Field="AmazonSellerCentral" />&nbsp;Amazon - Seller Central</td>
                <td class="SubSubHead"><asp:Image ID="Image1" runat="server" Table="vwMarketingMarketplace" Field="AmazonVendorCentral" />&nbsp;Amazon - Vendor Central</td>
                <td class="SubSubHead">Amazon Sell Price:</td>
                <td><asp:Label ID="Label24" runat="server" CssClass="Normal placeholder" Width="167" style="text-align:right;" Format="Decimal" Places="4" Table="vwMarketingMarketplace" Field="AmazonSellPrice" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-top:10px;">FAMIS Item Number:</td>
                <td style="padding-top:10px;"><asp:Label ID="lblfamisin" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingMarketplace" Field="FAMISItemNumber" /></td>
                <td class="SubSubHead" style="padding-top:10px;">FAMIS Price:</td>
                <td style="padding-top:10px;"><asp:Label ID="Label23" runat="server" CssClass="Normal placeholder" Width="167" style="text-align:right;" Format="Decimal" Places="4" Table="vwMarketingMarketplace" Field="FAMISPrice" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">FAMIS Start Date:</td>
                <td><asp:Label ID="label25" runat="server" CssClass="Normal placeholder" Width="167" style="text-align:right;" Table="vwMarketingMarketplace" Field="FAMISStartDateText" /></td>
                <td class="SubSubHead">FAMIS Expiration Date:</td>
                <td><asp:Label ID="Label26" runat="server" CssClass="Normal placeholder" Width="167" style="text-align:right;" Table="vwMarketingMarketplace" Field="FAMISExpirationDateText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Url Submitted to FAMIS:</td>
                <td colspan="3"><asp:Label ID="lblurlfam" runat="server" CssClass="Normal placeholder" Width="563" Table="vwMarketingMarketplace" Field="UrlSubmittedToFAMIS" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Bundle Form Submitted to FAMIS:</td>
                <td colspan="3"><asp:Label ID="Label27" runat="server" CssClass="Normal placeholder" Width="563" Table="vwMarketingMarketplace" Field="BundleFAMIS" /></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




