using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MMarketingContainer: LRHTab // PortalModuleBase, IActionable
    {
        
        protected void LoadData(object sender, RadTabStripEventArgs e)
        {
            ToggleTab();
        }

        protected override void ToggleTab()
        {
            //LoadData();
            int selectedIndex = 0;
            if (tabs.SelectedIndex != null) selectedIndex = tabs.SelectedIndex;
            //corresponds to pdm tabs structure
            SaveUserSetting(PartId, "SubTab", selectedIndex);

            switch (selectedIndex)
            {
                case 0:
                    ctlWeb.Initialize();
                    break;
                case 1:
                    ctlWebImages.Initialize();
                    break;
                case 2:
                    ctlMerchandising.Initialize();
                    break;
                case 3:
                    ctlMarketplace.Initialize();
                    break;
                case 4:
                    ctlPearson.Initialize();
                    break;

            }
        }

       
        
    }
}
 