using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MPDBowkerEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Bowker"; }}

        protected override void PopulateControls()
        {
            

            Controller controller = new Controller(entityId, environment);
            controller.PopulateDropDownListOptions(ddlContributor1, "Product Development", "Constributor_1_Function_Bowker", "Select Contributor 1 Function");
            controller.PopulateDropDownListOptions(ddlContributor2, "Product Development", "Constributor_2_Function_Bowker", "Select Contributor 2 Function");

            controller.PopulateDropDownListOptions(ddlProduct, "Product Development", "Product_Form_Bowker", "Select Product Form");
            controller.PopulateDropDownListOptions(ddlLanguage, "Product Development", "Language_Bowker", "Select Language");
            controller.PopulateDropDownListOptions(ddlAudience, "Product Development", "Audience_Bowker", "Select Audience");

            controller.PopulateDropDownListOptions(ddlBisac, "Product Development", "BISAC_Subject_Code_1_Bowker", "Select BISAC Subject Code");
            controller.PopulateDropDownListOptions(ddlImprint, "Product Development", "Imprint_Name_Bowker", "Select Imprint Name");
            controller.PopulateDropDownListOptions(ddlPublishStatus, "Product Development", "Publishing_Status_Bowker", "Select Publishing Status");
            
	        controller.PopulateDropDownListOptions(ddlSalesRights, "Product Development", "Sales_Rights_Type_Bowker", "Select Sales Rights Type");
            controller.PopulateDropDownListOptions(ddlSalesLocation, "Product Development", "Sales_rights_Location(s)_Bowker", "Select Sales Rights Location");
            controller.PopulateDropDownListOptions(ddlBarcodeIndicator, "Product Development", "Barcode_Indicator_Bowker", "Select Barcode Indicator");
            controller.PopulateDropDownListOptions(ddlReturnableIndicator, "Product Development", "Returnable_Indicator_Bowker", "Select Returnable Indicator");
         }


        protected override void SetRequirements()
        {
            //labelPartId.CssClass = "SubSubHead required";

        }

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = true; 
        }

        protected override bool Validate()
        {
            
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;


            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
        
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);

            PrepTableData(tables, controller);
            
            SaveTableEdits(tables, "vwBowker", "Product Development.Bowker", NewRecord(ds, "vwBowker"), controller, "Bowker_PartId");

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwBowker")) return;

            TableFields table = tables["vwBowker"];
            if (!table.IsDirty()) return;


            //custom logic
            
        }
     
    }
}
 