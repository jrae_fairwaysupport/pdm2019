using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;
using System.Text;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MMarketingWebImagesEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Marketing Web Images"; } }

        protected override void SetDefaultValues(DataSet ds)
        {
            bool newRecord = NewRecord(ds, "vwMarketingWebImages");
            chkToggleLabels.Checked = !newRecord;
        }

        protected override DataSet LoadData()
        {
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "vwMarketingWebImages");

            dtImages = ds.Tables["vwMarketingWebImages"];
            LoadImages();
            return ds;
        }

        private void LoadImages()
        {
            DataTable dt = dtImages;
            DataRow[] arr = dt.Select("Deleted = 0");
            DataTable tmp = dt.Clone();
            if (arr.Length > 0) tmp = arr.CopyToDataTable();

            lstWebImages.DataSource = tmp;
            lstWebImages.DataBind();

        }

        protected override bool Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);
            controller.ValidateText(txtImage, true, "Image Name", ref msg);
            controller.ValidateNumber(txtPriority, true, "Priority", ref msg);
            
            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {
                CloseForm();
                return;
            }

            

            Controller controller = new Controller(entityId, environment);
            DataTable dt = dtImages;

            SaveDeletes(controller, dt);
            SaveEdits(controller, dt);

            CloseForm();
        }

        private void SaveEdits(Controller controller, DataTable dt)
        {
            string tabName = "Marketing.Web Images";
            DataRow[] arr = dt.Select("Dirty = 1");
            foreach (DataRow dr in arr)
            {
                StringBuilder msg = new StringBuilder();

                string recordId = dr["RecordId"].ToString();
                string name = dr["Name"].ToString();
                string priority = dr["Priority"].ToString();

                int test = -1;
                if (Int32.TryParse(recordId, out test))
                {
                    msg.AppendLine("Modified Image:<br />");
                    msg.AppendFormat("Image: {0}<br />Priority: {1}", name, priority);

                    controller.pdm.UpdateRecord("MarketingWebImages", "Id", test, "Name", name, "Priority", priority, "ModifiedDate", DateTime.Now);
                }
                else
                {
                    //new
                    msg.AppendLine("Added Image:<br />");
                    msg.AppendFormat("Image: {0}<br />Priority: {1}", name, priority);

                    controller.pdm.InsertRecord("MarketingWebImages", "Id", "Name", name, "Priority", priority, "ModifiedDate", DateTime.Now, "CreatedDate", DateTime.Now, "ImagesPartId", PartId);
                }
                controller.history.Log(PartId, this.UserInfo.Username, tabName, name, string.Empty, msg.ToString());

            }
        }

        private void SaveDeletes(Controller controller, DataTable dt)
        {
            string tabName = "Marketing.Web Images";
            DataRow[] arr = dt.Select("Deleted = 1");
            foreach (DataRow dr in arr)
            {
                StringBuilder msg = new StringBuilder();
                msg.AppendLine("Deleted Image:<br />");

                string recordId = dr["RecordId"].ToString();
                //defense -- if it's a guid, we have nothing to do
                int test = -1;
                if (Int32.TryParse(recordId, out test))
                {
                    string name = dr["Name"].ToString();
                    string priority = dr["Priority"].ToString();
                    msg.AppendFormat("Image: {0}<br />Priority: {1}", name, priority);

                    controller.history.Log(PartId, this.UserInfo.Username, tabName, name, string.Empty, msg.ToString());

                    string sql = "DELETE FROM MarketingWebImages WHERE Id = @P0 AND ImagesPartId = @P1";
                    controller.pdm.ExecuteCommand(sql, test, PartId);
                }

                dt.Rows.Remove(dr);
            }
        }
        
        protected void DoEdit(object obj, CommandEventArgs args) 
        {
            string recordId = string.Empty;
            if (args.CommandName.Equals("ImageButton"))
            {
                ImageButton btn = (ImageButton)obj;
                recordId = btn.Attributes["RecordId"];
            }
            else
            {
                LinkButton btn = (LinkButton)obj;
                recordId = btn.Attributes["RecordId"];
            }

            lblAction.Text = (recordId == "-1") ? "Add New Image" : "Edit Image";
            lnkSaveEdit.CommandArgument = (recordId == "-1") ? Guid.NewGuid().ToString() : recordId;
            txtImage.Text = string.Empty;
            txtPriority.Text = string.Empty;

            pnlMain.Visible = false;
            pnlEditor.Visible = true;

            if (recordId == "-1") return;

            DataTable dt = dtImages;
            DataRow[] arr = dt.Select(string.Format("RecordId = '{0}'", recordId));
            if (arr.Length > 0)
            {
                txtImage.Text = arr[0]["Name"].ToString();
                txtPriority.Text = arr[0]["Priority"].ToString();
            }
        }
        
        protected void DoDelete(object obj, CommandEventArgs args) 
        {
            ImageButton btn = (ImageButton)obj;
            string recordId = btn.Attributes["RecordId"];

            DataTable dt = dtImages;
            DataRow[] arr = dt.Select(string.Format("RecordId = '{0}'", recordId));

            int test = -1;
            if (Int32.TryParse(recordId, out test))
            {
                arr[0]["Deleted"] = true;
            }
            else
            {
                dt.Rows.Remove(arr[0]);
            }
            dtImages = dt;
            IsDirty = true;
            LoadImages();
        }

        protected void DoSaveEdit(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            LinkButton btn = (LinkButton)obj;
            string recordId = btn.CommandArgument.ToString();
            string name = txtImage.Text.Trim();
            string priority = txtPriority.Text.Trim();

            DataTable dt = dtImages;
            DataRow[] arr = dt.Select(string.Format("RecordId = '{0}'", recordId));
            
            if (arr.Length > 0)
            {
                arr[0]["Name"] = name;
                arr[0]["Priority"] = priority;
                arr[0]["Dirty"] = true;
                arr[0]["Deleted"] = false;
            }
            else
            {
                DataRow dr = dt.NewRow();
                dr["RecordId"] = recordId;
                dr["Name"] = name;
                dr["Priority"] = priority;
                dr["Dirty"] = true;
                dr["Deleted"] = false;
                dt.Rows.Add(dr);
            }

            dtImages = dt;
            LoadImages();

            pnlEditor.Visible = false;
            pnlMain.Visible = true;
        }

        protected void DoCancelEdit(object obj, CommandEventArgs args)
        {
            pnlMain.Visible = true;
            pnlEditor.Visible = false;
        }

        private XmlDocument images
        {
            get
            {
                string raw = hdnImagesXML.Value;
                if (string.IsNullOrEmpty(raw)) raw = "<root />";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(raw);
                return doc;
            }

            set
            {
                hdnImagesXML.Value = value.OuterXml;
            }
        }

        private DataTable dtImages
        {
            get
            {
                XmlDocument doc = images;
                XmlElement root = doc.DocumentElement;

                DataTable dt = new DataTable();
                dt.Columns.Add("RecordId", System.Type.GetType("System.String"));
                dt.Columns.Add("Name", System.Type.GetType("System.String"));
                dt.Columns.Add("Priority", System.Type.GetType("System.String"));
                dt.Columns.Add("Deleted", System.Type.GetType("System.Boolean"));
                dt.Columns.Add("Dirty", System.Type.GetType("System.Boolean"));

                XmlNodeList ls = root.SelectNodes("Image");
                if (ls == null) return dt;

                foreach (XmlNode node in ls)
                {
                    DataRow dr = dt.NewRow();
                    dr["RecordId"] = node.SelectSingleNode("RecordId").InnerText;
                    dr["Name"] = node.SelectSingleNode("Name").InnerText;
                    dr["Priority"] = node.SelectSingleNode("Priority").InnerText;
                    dr["Deleted"] = node.SelectSingleNode("Deleted").InnerText;
                    dr["Dirty"] = node.SelectSingleNode("Dirty").InnerText;
                    dt.Rows.Add(dr);
                }

                return dt;
            }

            set
            {
                if (!value.Columns.Contains("Deleted"))
                {
                    value.Columns.Add("Dirty", System.Type.GetType("System.Boolean"));
                    value.Columns.Add("Deleted", System.Type.GetType("System.Boolean"));
                    foreach (DataRow dr in value.Rows) 
                    {
                        dr["Deleted"] = false;
                        dr["Dirty"] = false;
                    }

                }

                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("root");
                doc.AppendChild(root);

                foreach (DataRow dr in value.Rows)
                {
                    XmlElement img = doc.CreateElement("Image");
                    CreateElement(doc, img, "RecordId", dr["RecordId"].ToString());
                    CreateElement(doc, img, "Name", dr["Name"].ToString());
                    CreateElement(doc, img, "Priority", dr["Priority"].ToString());
                    CreateElement(doc, img, "Deleted", dr["Deleted"].ToString());
                    CreateElement(doc, img, "Dirty", dr["Dirty"].ToString());
                    root.AppendChild(img);
                }

                images = doc;
            }
        }

        private XmlElement CreateElement(XmlDocument doc, XmlElement parent, string name, string value)
        {
            XmlElement newbie = doc.CreateElement(name);
            newbie.InnerText = value;
            parent.AppendChild(newbie);
            return newbie;
        }

     
    }
}
 