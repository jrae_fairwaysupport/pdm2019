<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MCreativeTabEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MCreativeTabEditor" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }

           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();
        ToggleGrade('Min');
        ToggleGrade('Max');

        var msg = jQuery('[id*="hdnCreativeMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnCreativeMessage"]').val("");



    });

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });
    }


</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
      <asp:Panel ID="pnlCreative" runat="server" >
            <table width="800" cellpadding="4" cellspacing="0" border="0">
                <tr>
                    <td style="width:200px;">
                        <asp:Label ID="labelLinksToDocs" runat="server" CssClass="SubSubHead" Text="Links to Documents:" />
                    </td>
                    <td style="width:200px;"></td>
                    <td style="width:200px;"></td>
                    <td style="width:200px;"></td>
                </tr>
                <tr>
                    <td colspan="4" align="left" valign="top">
                        <asp:TextBox ID="txtLinksToDocuments" runat="server" Width="780" Rows="4" TextMode="MultiLine" Table="vwCreativeInfos" Field="LinksToDocuments" Placeholder="Links to Documents" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="label10" runat="server" CssClass="SubSubHead" Text="Links to Images:<br />" />
                        <asp:TextBox ID="TextBox8" runat="server" Width="780" Rows="4" TextMode="MultiLine" Table="vwCreativeInfos" Field="LinksToImages" Placeholder="Links to Images" />
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Other Links:<br />" />
                        <asp:TextBox ID="txtOtherLinks" runat="server" Width="780" Rows="4" TextMode="MultiLine" Table="vwCreativeInfos" Field="OtherLinks" Placeholder="Other Links" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnCreativeMessage" runat="server" Value="" />

