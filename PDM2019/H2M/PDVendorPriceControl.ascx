<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDVendorPriceControl.ascx.cs" Inherits="YourCompany.Modules.PDM2019.VendorPriceBreaksControl" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestVendorId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:25px; top:65px; font-size:small; width:600px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        
    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

        
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

</script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:800px;" colspan="4">
                <asp:Label ID="labelVendorId" runat="server" CssClass="SubSubHead" Text="VendorId:<br />" />
                <DNN:DNNTextSuggest ID="txtParentPartId" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggestVendorId" 
                    Table="vwVendorPriceBreaks" Field="VendorSuggest" TargetField="VendorId" NameIDSplit="-" Width="600" Placeholder="Vendor Id" Required="true"
                />
            </td>
        </tr>

        <tr>
            <td style="width:200px;">
                <asp:Label ID="labelVendorPartId" runat="server" CssClass="SubSubHead" Text="Vendor Part Id:<br />" />
                <asp:TextBox ID="txtVendorPartId" runat="server" CssClass="Box" Width="180" MaxLength="30" Placeholder="Vendor Part Id" Table="vwVendorPriceBreaks" Field="VendorPartId" Required="true" />
            </td>

            <td style="width:200px;" align="center">
                <asp:Label ID="labelQuantity" runat="server" CssClass='SubSubHead' Text="Quantity:<br />" />
                <asp:TextBox ID="txtQuantity1" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 1" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity1" />
            </td>
            <td style="width:200px;" align="center">
                <asp:Label ID="labelPrice" runat="server" CssClass="SubSubHead" Text="Unit Price:<br />" />
                <asp:TextBox ID="txtPrice1" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 1" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price1" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td >&nbsp;</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity2" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 2" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity2" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice2" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 2" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price2" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td >&nbsp;</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity3" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 3" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity3" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice3" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 3" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price3" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td >&nbsp;</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity4" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 4" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity4" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice4" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 4" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price4" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td >&nbsp;</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity5" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 5" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="vwVendorPriceBreaks" Field="Quantity5" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice5" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 5" Format="Decimal" MaxLength="25" Table="vwVendorPriceBreaks" Field="Price5" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>


    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" Checked="true" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />

