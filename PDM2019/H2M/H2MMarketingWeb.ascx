<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingWeb.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingWeb" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:187px;" />
                <td style="width:187px;" />
                <td style="width:187px;" />
                <td style="width:189px;" />
            </tr>

            <tr>
                <td class="SubSubHead" valign="top">Web Title:</td>
                <td colspan="3"><asp:Label ID="lblWebTitle" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="WebTitle" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Warning Copy:</td>
                <td colspan="3"><asp:Label ID="Label1" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="WarningCopy" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Copy:</td>
                <td colspan="3"><asp:Label ID="Label2" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="WebCopyBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Headline:</td>
                <td colspan="3"><asp:Label ID="Label3" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="Headline" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Meta Product Title:</td>
                <td colspan="3"><asp:Label ID="Label4" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="MetaProductTitle" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Meta Description:</td>
                <td colspan="3"><asp:Label ID="Label5" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="MetaDescriptionBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Brand:</td>
                <td colspan="3"><asp:Label ID="Label6" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="BrandText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Grade (Facet):</td>
                <td colspan="3"><asp:Label ID="Label7" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="GradeFacet" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Age Level:</td>
                <td colspan="3"><asp:Label ID="Label8" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="AgeLevel" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" >Best Seller:</td>
                <td ><asp:Label ID="Label9" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingWeb" Field="BestSellerText" /></td>
                <td class="SubSubHead" >New Item:</td>
                <td ><asp:Label ID="Label10" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingWeb" Field="NewText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" >On Sale:</td>
                <td ><asp:Label ID="Label11" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingWeb" Field="OnSale" /></td>
                <td class="SubSubHead">Number of Students:</td>
                <td ><asp:Label ID="Label12" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingWeb" Field="NumberOfStudents" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" >Display Order:</td>
                <td ><asp:Label ID="Label13" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingWeb" Field="DisplayOrder" /></td>
                <td class="SubSubHead">Unit Measure Description:</td>
                <td ><asp:Label ID="Label14" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingWeb" Field="UnitMeasureDescription" /></td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgLive" runat="server" Table="vwMarketingWeb" Field="LiveMaterial" />&nbsp;Live Material</td>
                <td class="SubSubHead"><asp:Image ID="Image1" runat="server" Table="vwMarketingWeb" Field="WebExclusive" />&nbsp;Web Exclusive</td>
                <td class="SubSubHead"><asp:Image ID="Image2" runat="server" Table="vwMarketingWeb" Field="Outlet" />&nbsp;Product Outlet</td>
                <td class="SubSubHead"><asp:Image ID="Image3" runat="server" Table="vwMarketingWeb" Field="ShowSKUPage" />&nbsp;Show Single SKU Page</td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="Image4" runat="server" Table="vwMarketingWeb" Field="AddToCatalogRequest" />&nbsp;Add To Catalog Request</td>
                <td />
                <td />
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Web Overstock Min Qty.:</td>
                <td ><asp:Label ID="Label15" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingWeb" Field="OverstockMinQty" /></td>
                <td class="SubSubHead">Search Sort Priority:</td>
                <td ><asp:Label ID="Label18" runat="server" CssClass="Normal placeholder" Width="167" Table="vwMarketingWeb" Field="SearchSortPriority" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Search Keywords:</td>
                <td colspan="3"><asp:Label ID="Label16" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="SearchKeywords" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Video Description:</td>
                <td colspan="3"><asp:Label ID="Label17" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="VideoDescription" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Video Url:</td>
                <td colspan="3"><asp:Label ID="Label19" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="VideoUrl" /></td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="Image5" runat="server" Table="vwMarketingWeb" Field="DigitalDelivery" />&nbsp;Digital Delivery</td>
                <td class="SubSubHead" colspan="2"><asp:Image ID="Image6" runat="server" Table="vwMarketingWeb" Field="DigitalDeliveryExcludFromWeb" />&nbsp;Digital Delivery Exclude from Web</td>
                <td class="SubSubHead"><asp:Image ID="Image8" runat="server" Table="vwMarketingWeb" Field="EBookCollection" />&nbsp;eBook Collection</td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">eBook Instruction:</td>
                <td colspan="3"><asp:Label ID="Label20" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="EBookDownloadInstruction" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">eBook Print Url:</td>
                <td colspan="3"><asp:Label ID="Label21" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="EBookPrintUrl" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">eBook Purchase Type:</td>
                <td colspan="3"><asp:Label ID="Label22" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="EBookPurchaseType" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">eBook Standards:</td>
                <td colspan="3"><asp:Label ID="Label23" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="EBookStandards" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">eBook Subject:</td>
                <td colspan="3"><asp:Label ID="Label24" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="EBookSubject" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" colspan="2"><asp:Image ID="imgforportal" runat="server" Table="vwMarketingWeb" Field="AvailableForSaleForPortal" />&nbsp;Available for Sale for Portal</td>
                <td />
                <td />
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Cross Sell:</td>
                <td colspan="3"><asp:Label ID="Label25" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="CrossSell" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Accessories:</td>
                <td colspan="3"><asp:Label ID="Label26" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="Accessories" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Other Assets:</td>
                <td colspan="3"><asp:Label ID="Label27" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingWeb" Field="OtherAssets" /></td>
            </tr>

        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




