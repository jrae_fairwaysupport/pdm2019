<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPDSpecificationsEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPartSpecificationsEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
    input.required { border:solid 1px orange; }           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label id="labelMaterial" runat="server" CssClass="SubSubHead" Text="Material:" /></td>
            <td style="width:200px;"><asp:Label ID="labelColor" runat="server" CssClass="SubSubHead" Text="Product Color(s):" /></td>
            <td style="width:200px;" />
            <td style="width:200px;" />
        </tr>
        <tr>
            <td><asp:DropDownList ID="ddlMaterial" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="Material" /></td>
            <td colspan="2"><asp:TextBox ID="txtColor" runat="server" CssClass="Box" Width="380" MaxLengh="50" Table="vwProductSpecifications" Field="ProductColor" PlaceHolder="Product Color(s)" /></td>
            <td><asp:CheckBox ID="chkPrintMaterial" runat="server" CssClass="SubSubHead" TextAlign="right" Table="vwProductSpecifications" Field="PrintMaterial" Text="&nbsp;Print Material" /></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="labelKeyRequirement" runat="server" CssClass="SubSubHead" Text="Key Requirement:<br />" />
                <asp:TextBox ID="txtKeyRequirement" runat="server" CssClass="Box" Width="780" MaxLength="200" Table="vwProductSpecifications" Field="KeyRequirement" Placeholder="Key Requirement" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Product Specifications:<br />" />
                <asp:TextBox ID="TextBox1" runat="server" CssClass="Box" Width="780" MaxLength="1000" Table="vwProductSpecifications" Field="ProductSpecification" Placeholder="Product Specifications" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="labelotherprinting" runat="server" CssClass="SubSubHead" Text="Other Printing on Product:<br />" />
                <asp:TextBox ID="txtOtherPrinting" runat="server" CssClass="Box" Width="380" MaxLength="100" Table="vwProductSpecifications" Field="OtherPrinting" Placeholder="Other Printing on Product" />
            </td>
            <td colspan="2">
                <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Trademark:<br />" />
                <asp:TextBox ID="TextBox2" runat="server" CssClass="Box" Width="380" MaxLength="100" Table="vwProductSpecifications" Field="Trademark" Placeholder="Trademark" />
            </td>
        </tr>

        <tr>
            <td colspan="4" style="border-bottom: solid 1px #e8e2e2;padding-top:15px;" class="Head">Type of OEM Packaging</td>
        </tr>
        <tr>
            <td><asp:CheckBox ID="chkHeader" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="Parts" Field="Header" Text="&nbsp;Header Card" /></td>
            <td><asp:CheckBox ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="Parts" Field="Polybag" Text="&nbsp;Polybag" /></td>
            <td><asp:CheckBox ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="Parts" Field="ZiplocBag" Text="&nbsp;Ziploc Bag" /></td>
            <td><asp:CheckBox ID="CheckBox3" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="Parts" Field="Tote" Text="&nbsp;Tote" /></td>
        </tr>
        <tr>
            <td><asp:CheckBox ID="CheckBox4" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="Parts" Field="Box" Text="&nbsp;Box" /></td>
            <td><asp:CheckBox ID="CheckBox5" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="Parts" Field="Blister" Text="&nbsp;Blister/Clam" /></td>
            <td><asp:CheckBox ID="CheckBox6" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="Parts" Field="FFP" Text="&nbsp;Frustration Free Packaging" /></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:CheckBox ID="chkOther" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="Parts" Field="Other" Text="&nbsp;Other:&nbsp;&nbsp;" />
                <asp:TextBox ID="txtOther" runat="server" CssClass="Box" Width="620" MaxLength="50" Placeholder="Other" Table="vwProductSpecifications" Field="PrintSpecs" />
            </td>
        </tr>

        <tr>
            <td colspan="4" style="border-bottom:solid 1px #e8e2e2;padding-top:15px;" class="Head">Print Product Specs</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="labelPrintSpecs" runat="server" CssClass="SubSubHead" Text="Print Specs Page Count:<br />" />
                <asp:TextBox ID="txtPrintSpecs" runat="server" CssClass="Box" Width="180" MaxLength="50" Table="vwProductSpecifications" Field="PrintSpecs" Placeholder="Print Specs Page Count" TargetField="PrintSpecs2" />
            </td>
            <td></td>
            <td class="SubSubHead" align="center" valign="bottom"><u>Cover</u></td>
            <td class="SubSubHead" align="center" valign="bottom"><u>Body</u></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="labelBindery" runat="server" CssClass="SubSubHead" Text="Bindery Type:<br />" />
                <asp:DropDownList ID="ddlBinderyType" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="BinderyType" />
            </td>
            <td valign="bottom" class="SubSubHead">Paper Grade:</td>
            <td valign="bottom"><asp:DropDownList ID="ddlCoverPaperGrade" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="CoverPaperGrade" /></td>
            <td valign="bottom"><asp:DropDownList ID="ddlBodyPaperGrade" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="BodyPaperGrade" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Finishing Type:<br />" />
                <asp:DropDownList ID="ddlFinishingType" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="FinishingType" />
            </td>
            <td valign="bottom" class="SubSubHead">Paper Color:</td>
            <td valign="bottom"><asp:DropDownList ID="ddlCoverPaperColor" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="CoverPaperColor" /></td>
            <td valign="bottom"><asp:DropDownList ID="ddlBodyPaperColor" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="BodyPaperColor" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="label4" runat="server" CssClass="SubSubHead" Text="Finish Size:<br />" />
                <asp:TextBox ID="txtFinishSize" runat="server" CssClass="Box" Width="180" MaxLength="100" Table="vwProductSpecifications" Field="FinishSize" Placeholder="Finish Size" />
            </td>
            <td valign="bottom" class="SubSubHead">Paper Weight:</td>
            <td valign="bottom"><asp:DropDownList ID="ddlCoverPaperWeight" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="CoverPaperWeight" /></td>
            <td valign="bottom"><asp:DropDownList ID="ddlBodyPaperWeight" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="BodyPaperWeight" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="label5" runat="server" CssClass="SubSubHead" Text="Flat Size:<br />" />
                <asp:TextBox ID="TextBox3" runat="server" CssClass="Box" Width="180" MaxLength="100" Table="vwProductSpecifications" Field="FlatSize" Placeholder="Flat Size" />
            </td>
            <td valign="bottom" class="SubSubHead">Coating:</td>
            <td valign="bottom"><asp:DropDownList ID="ddlCoverCoating" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="CoverCoating" /></td>
            <td valign="bottom"></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="label6" runat="server" CssClass="SubSubHead" Text="Packaging:<br />" />
                <asp:DropDownList ID="ddlPackaging" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="Packaging" />
            </td>
            <td valign="bottom" class="SubSubHead">Pages, Ink Color:</td>
            <td valign="bottom"><asp:DropDownList ID="ddlCoverPagesInkColor" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="CoverPagesInkColor" /></td>
            <td valign="bottom"><asp:DropDownList ID="ddlBodyPagesInkColor" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="BodyPagesInkColor" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="label7" runat="server" CssClass="SubSubHead" Text="Quantity per Package:<br />" />
                <asp:TextBox ID="txtQuantityPerPackage" runat="server" CssClass="Box" Width="180" MaxLength="8" onkeypress="return isNumberKey(event);" Table="vwProductSpecifications" Field="QuantityPerPackage" Placeholder="Quantity Per Package" />
            </td>
            <td valign="bottom" class="SubSubHead"></td>
            <td valign="bottom"><asp:CheckBox ID="chkCoverPagesBleed" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Cover Pages Bleed" Table="vwProductSpecifications" Field="CoverPagesBleed" /></td>
            <td valign="bottom"><asp:CheckBox ID="CheckBox7" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Body Pages Bleed" Table="vwProductSpecifications" Field="BodyPagesBleed" /></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="labelPrintSpecNotes" runat="server" CssClass="SubSubHead" Text="Print Spec Notes:<br />" />
                <asp:TextBox ID="txtPrintSpecNotes" runat="server" CssClass="Box" Width="780" MaxLength="400" Table="vwProductSpecifications" Field="PrintSpecNotes" Placeholder="Print Spec Notes" />
            </td>
        </tr>

        <tr>
            <td colspan="4" style="border-bottom:solid 1px #e8e2e2;padding-top:15px;" class="Head">Print Product Specs</td>
        </tr>
        <tr>
            <td colspan="3" valign="bottom">
                <asp:Label ID="labelAboutAuthor" runat="server" CssClass="SubSubHead" Text="About Author:<br />" />
                <asp:TextBox ID="txtAboutAuthor" runat="server" CssClass="Box" Width="580" MaxLength="200" Table="vwProductSpecifications" Field="AboutAuthor" PlaceHolder="About Author" />
            </td>
            <td valign="bottom"><asp:CheckBox ID="chkDualLanguae" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Dual Language" Table="vwProductSpecifications" Field="DualLanguage" /></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="label8" runat="server" CssClass="SubSubHead" Text="Author:<br />" />
                <asp:TextBox ID="TextBox4" runat="server" CssClass="Box" Width="580" MaxLength="100" Table="vwProductSpecifications" Field="Author" PlaceHolder="Author" />
            </td>
            <td>
                <asp:Label ID="label12" runat="server" CssClass="SubSubHead" Text="DRA:<br />" />
                <asp:DropDownList ID="ddlDRA" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="DRA" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="label9" runat="server" CssClass="SubSubHead" Text="Book Set:<br />" />
                <asp:DropDownList ID="ddlBookSet" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="BookSet" />
            </td>
            <td>
                <asp:Label ID="label10" runat="server" CssClass="SubSubHead" Text="Book Type:<br />" />
                <asp:DropDownList ID="ddlBookType" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="BookType" />
            </td>
            <td>
                <asp:Label ID="label11" runat="server" CssClass="SubSubHead" Text="Curriculum Area:<br />" />
                <asp:TextBox ID="TextBox5" runat="server" CssClass="Box" Width="180" MaxLength="50" Table="vwProductSpecifications" Field="CurriculumArea" PlaceHolder="CurriculumArea" />
            </td>
            <td>
                <asp:Label ID="label18" runat="server" CssClass="SubSubHead" Text="Number of Pages:<br />" />
                <asp:TextBox ID="TextBox9" runat="server" CssClass="Box required" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwProductSpecifications" Field="NumberOfPages" PlaceHolder="Number Of Pages" />
            </td>
        </tr>
        <tr>
            
            <td valign="bottom">
                <asp:Label ID="label13" runat="server" CssClass="SubSubHead" Text="Early Intervention Level:<br />" />
                <asp:TextBox ID="TextBox6" runat="server" CssClass="Box" Width="180" MaxLength="50" Table="vwProductSpecifications" Field="EarlyInterventionLevel" PlaceHolder="Early Intervention Level" />
            </td>
            <td>
                <asp:Label ID="label15" runat="server" CssClass="SubSubHead" Text="Guided Reading Level:<br />" />
                <asp:DropDownList ID="ddlGuidedReadingLevel" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="GuidedReadingLevel" />
            </td>
            <td>
                <asp:Label ID="label14" runat="server" CssClass="SubSubHead" Text="Genre:<br />" />
                <asp:DropDownList ID="ddlGenre" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="Genre" />
            </td>
            <td>
                <asp:Label ID="label16" runat="server" CssClass="SubSubHead" Text="Illustrator/Photographer:<br />" />
                <asp:TextBox ID="TextBox7" runat="server" CssClass="Box" Width="180" MaxLength="100" Table="vwProductSpecifications" Field="Illustrator" PlaceHolder="Illustrator/Photographer" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="label17" runat="server" CssClass="SubSubHead" Text="Lexile Level:<br />" />
                <asp:TextBox ID="TextBox8" runat="server" CssClass="Box" Width="180" MaxLength="50" Table="vwProductSpecifications" Field="LexileLevel" PlaceHolder="Lexile Level" />
            </td>
            <td>
                <asp:Label ID="label19" runat="server" CssClass="SubSubHead" Text="Strand:<br />" />
                <asp:TextBox ID="TextBox10" runat="server" CssClass="Box" Width="180" MaxLength="50" Table="vwProductSpecifications" Field="Strand" PlaceHolder="Strand" />
            </td>
            <td>
                <asp:Label ID="label20" runat="server" CssClass="SubSubHead" Text="Text Type:<br />" />
                <asp:DropDownList ID="ddlTextType" runat="server" CssClass="Box" Width="180" Table="vwProductSpecifications" Field="TextType" />
            </td>
            <td />
        </tr>
        <tr>
            <td>
                <asp:Label ID="label21" runat="server" CssClass="SubSubHead" Text="Theme:<br />" />
                <asp:TextBox ID="TextBox11" runat="server" CssClass="Box" Width="180" MaxLength="100" Table="vwProductSpecifications" Field="Theme" PlaceHolder="Theme" />
            </td>
            <td>
                <asp:Label ID="label22" runat="server" CssClass="SubSubHead" Text="Topic:<br />" />
                <asp:TextBox ID="TextBox12" runat="server" CssClass="Box" Width="180" MaxLength="100" Table="vwProductSpecifications" Field="Topic" PlaceHolder="Topic" />
            </td>
            <td />
            <td />
        </tr>
    </table>
    </asp:Panel>
    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" Checked="true" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" />
            </td>
        </tr>
    
    </table>

</asp:Panel>



<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<%--<asp:HiddenField ID="hdnShowPanel" runat="server" Value="Main" />--%>


