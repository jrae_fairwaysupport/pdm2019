<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPCHPlanningEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPCHPlanningEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    .readonly { background-color:#e8e2e2; }
         
    span.required
    {
        border-bottom: double 2px red;
    }          
    
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    input.required { border: solid 1px orange; }
    
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Planner User Id:" /></td>
            <td style="width:200px;"><asp:Label ID="labelbuyer" runat="server" CssClass="SubSubHead" Text="Buyer User Id:" /></td>
            <td style="width:200px;"><asp:Label ID="labelsafety" runat="server" CssClass="SubSubHead" Text="Lead Time:" /></td>
            <td style="width:200px;"><asp:Label ID="labelStatus" runat="server" CssClass="SubSubHead" Text="Product Status:" /></td>
        </tr>
        <tr>
            <td >
                <asp:TextBox ID="txtCatalogdescription" runat="server" Width="180" MaxLength="100" Table="vwPurchasingPartInfos" Field="PlannerUserId" Placeholder="Planner User Id" />
            </td>
            <td >
                <asp:TextBox ID="TextBox6" runat="server" Width="180" MaxLength="100" Table="vwPartInfos" Field="BuyerUserId" Placeholder="Buyer User Id" CssClass="Box required" />
            </td>
            <td >
                <asp:TextBox ID="TextBox7" runat="server" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwPartInfos" Field="LeadTime" Placeholder="Lead Time" CssClass="Box required" />
            </td>
            <td >
                <asp:TextBox ID="TextBox2" runat="server" Width="180" MaxLength="25" Table="vwPurchasingPartInfos" Field="ProductStatus" Placeholder="Product Status" />
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="label33" runat="server" CssClass="SubSubHead" Text="Safety Stock Qty:<br />" />
                <asp:TextBox ID="txtSafetyStockQty" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwPartInfos" Field="SafetyStockQty" Placeholder="Safety Stock Qty" Format="Decimal" Places="0" />
            </td>
            <td>
                <asp:Label ID="label11" runat="server" CssClass="SubSubHead" Text="Minimum Order Qty:<br />" />
                <asp:TextBox ID="TextBox3" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwPartInfos" Field="MinimumOrderQty" Placeholder="Minimum Order Qty" Format="Decimal" Places="0" />
            </td>
            <td>
                <asp:Label ID="label12" runat="server" CssClass="SubSubHead" Text="Multiple Order Qty:<br />" />
                <asp:TextBox ID="TextBox4" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwPartInfos" Field="MultipleOrderQty" Placeholder="Multiple Order Qty" Format="Decimal" Places="0" />
            </td>
            <td>
                <asp:Label ID="label36" runat="server" CssClass="SubSubHead" Text="Order Policy:<br />" />
                <asp:DropDownList ID="ddlOrderPolicy" runat="server" CssClass="Box" Width="180"  Table="vwPartInfos" Field="OrderPolicy"/>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="label39" runat="server" CssClass="SubSubHead" Text="Days of Supply:<br />" />
                <asp:TextBox ID="TextBox5" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwPartInfos" Field="DaysOfSupply" Placeholder="Days of Supply"  Format="Decimal" Places="0"/>
            </td>
            <td>
                <asp:Label ID="label40" runat="server" CssClass="SubSubHead" Text="Order Point:<br />" />
                <asp:TextBox ID="txtOrderPoint" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isDecimal(event);" Table="vwPartInfos" Field="OrderPoint" Format="Decimal" Places="4" Placeholder="Order Point" />
            </td>
            <td />
            <td />
        </tr>

    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
