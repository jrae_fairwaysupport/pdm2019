using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MSidebar : LRHViewer //PortalModuleBase, IActionable
    {
        public event EventHandler SomethingHistoricalHappened;

        public void SetWidth(int newWidth)
        {
            pnlMain.Width = (Unit)newWidth;
        }


        public void Initialize(DataSet ds)
        {
            DataTable dt = ds.Tables["Parts"];
            if (dt == null) return;
            if (dt.Rows.Count == 0) return;


            string status = GetUserSetting<string>(PartId, "Status", "Draft");
            lblStatus.Text = status;
            lblStatus.CssClass = lblStatus.CssClass.Append(status, " ");
            if (status.Equals("Rejected")) lblStatus.ToolTip = dt.Rows[0]["RejectionReason"].ToString();

            //string sku = GetUserSetting<string>(PartId, "Sku", string.Empty);
            lnkPartHistory.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "History", "id", PartId.ToString(), "e", environment, "eid", entityId); //sku's become useless with weird characters

            string viewRole = ViewRole;
            lnkSubmit.Visible = viewRole.Equals("pd") || viewRole.Equals("ad");
            lnkApprove.Visible = viewRole.Equals("p") || viewRole.Equals("ad");
            lnkReject.Visible = viewRole.Equals("p") || viewRole.Equals("ad");
            lnkReject.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "Reject", "id", PartId.ToString(), "e", environment, "eid", entityId);

            if (status.Equals("Approved"))
            {
                lnkSubmit.Visible = false;
                lnkApprove.Visible = false;
                lnkReject.Visible = false;
            }


        }

        
        private string GetModuleSetting(string key)
        {
            key = entityId + environment + key;
            PDM2019Settings settingsData = new PDM2019Settings(this.TabModuleId);
            return settingsData.ReadSetting<string>(key, string.Empty);
        }

        protected void DoSubmit(object obj, CommandEventArgs args)
        {
            string msg = string.Empty;

            SubmitPart(ref msg);

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowSidebarMessage.Value = msg;
                return;
            }

            EmailController email = new EmailController(entityId, environment);
            email.SendSubmit(PartId, GetModuleSetting("PartUrlTemplate"), this.PortalId, GetModuleSetting("SubmitEmails"));

            lblStatus.Text = "Pending";
            lblStatus.CssClass = "button Pending";

            hdnShowSidebarMessage.Value = "Part has been submitted for Approval.";

            

            if (this.SomethingHistoricalHappened != null) this.SomethingHistoricalHappened(this, new EventArgs());
        }

        protected void DoApprove(object obj, CommandEventArgs args)
        {
            string msg = string.Empty;

            DoApprove(ref msg);

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowSidebarMessage.Value = msg;
                return;
            }

            EmailController email = new EmailController(entityId, environment);
            email.SendSubmit(PartId, GetModuleSetting("PartUrlTemplate"), this.PortalId, GetModuleSetting("ApproveEmails"));

            lblStatus.Text = "Approved";
            lblStatus.CssClass = "button Approved";

            hdnShowSidebarMessage.Value = "Part has been Approved.";



            if (this.SomethingHistoricalHappened != null) this.SomethingHistoricalHappened(this, new EventArgs());
        }

        protected void DoApprove(ref string msg)
        {
            msg = string.Empty;
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "Parts", "vwPartInfos", "vwPurchasingPartInfos");

            SubmitPart_Validate(controller, ds, "Parts", "PartId", "Part Id", ref msg);
            SubmitPart_Validate(controller, ds, "Parts", "Description", "Description", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "MaterialCost", "Material Cost", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "NetPrice", "Catalog/Net Price", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "RetailPrice", "Retail Price", ref msg);

            SubmitPart_Validate(controller, ds, "vwPartInfos", "PreferredVendor", "Preferred Vendor", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "YearIntroduced", "Year Introduced", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "PackageSize", "Package Size", ref msg);

            SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "BuyerUserId", "Buyer User Id", ref msg);
            SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "CountryOfOrigin", "Country of Manufacture", ref msg);
            SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "PortOfOrigin", "Port of Origin", ref msg);
            SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "BuyerUserId", "Buyer User Id", ref msg);

            if (!string.IsNullOrEmpty(msg))
            {
                msg = "The following fields need values before the Part may be approved:\r\n" + msg;
                return;
            }

            controller.history.Log(PartId, this.UserInfo.Username, "Header", "Status", partStatus, "Approved");
            controller.pdm.UpdateRecord("Parts", "id", PartId, "Status", "Approved", "ModifiedDate", DateTime.Now, "WasApproved", true, "LoadEvents", true);
        }


        private void SubmitPart(ref string msg)
        {
            msg = string.Empty;
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "Parts", "vwPartInfos");

            SubmitPart_Validate(controller, ds, "Parts", "PartId", "Part Id", ref msg);
            SubmitPart_Validate(controller, ds, "Parts", "Description", "Description", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "MaterialCost", "Material Cost", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "NetPrice", "Catalog/Net Price", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "RetailPrice", "Retail Price", ref msg);

            SubmitPart_Validate(controller, ds, "vwPartInfos", "PreferredVendor", "Preferred Vendor", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "YearIntroduced", "Year Introduced", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "PackageSize", "Package Size", ref msg);

            if (!string.IsNullOrEmpty(msg))
            {
                msg = "The following fields need values before the Part may be submitted:\r\n" + msg;
                return;
            }

            controller.history.Log(PartId, this.UserInfo.Username, "Header", "Status", partStatus, "Pending");
            controller.pdm.UpdateRecord("Parts", "id", PartId, "Status", "Pending", "SubmitUserId", this.UserInfo.Username, "ModifiedDate", DateTime.Now);
        }

        private void SubmitPart_Validate(Controller controller, DataSet ds, string table, string column, string field, ref string msg)
        {
            bool error = false;

            DataTable dt = ds.Tables[table];
            if (dt == null)
            {
                error = true;
            }
            else
            {
                if (dt.Rows.Count == 0) error = true;
            }

            if (!error)
            {
                DataRow dr = dt.Rows[0];
                if (string.IsNullOrEmpty(controller.pdm.GetValue<string>(dr, column, string.Empty))) error = true;
            }

            if (error)
            {
                if (!string.IsNullOrEmpty(msg)) msg += ", ";
                msg += field;
            }
        }
    }
}
 