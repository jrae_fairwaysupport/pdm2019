<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPDPartInformationEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPDPartInformationEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    .readonly { background-color:#e8e2e2; }
         
         
    
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    input.required
    {
        border: solid 1px orange;
    }
    
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Catalog Description:" /></td>
            <td style="width:200px;"></td>
            <td style="width:200px;"></td>
            <td style="width:200px;"></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:TextBox ID="txtCatalogdescription" runat="server" Width="780" Height="100" TextMode="MultiLine" MaxLength="300" Table="vwMarketingInfos" Field="WebPartDescription" Placeholder="Catalog Description" />
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <asp:Label ID="labelPackaging" runat="server" CssClass="SubSubHead" Text="Packaging Product Description:<br />" />
                <asp:TextBox ID="TextBox1" runat="server" Width="780" Height="100" TextMode="MultiLine" MaxLength="250" Table="vwEtaPartInfos" Field="ProductDescription" Placeholder="Packaging Product Description" />
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <asp:Label ID="labelPreferredVendor" runat="server" CssClass="SubSubHead" Text="Preferred Vendor:<br />" />
                <DNN:DNNTextSuggest ID="txtPreferredVendor" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box required"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPartInfos" Field="PreferredVendorSuggest" TargetField="PreferredVendor" NameIDSplit="-" Width="780" Placeholder="Preferred Vendor" 
                    queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwVENDORS WHERE ID = @P0 OR NAME = @P1"
                />
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <asp:Label ID="label22" runat="server" CssClass="SubSubHead" Text="Secondary Vendor:<br />" />
                <DNN:DNNTextSuggest ID="txtSecondaryVendor" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPartInfos" Field="SecondaryVendorSuggest" TargetField="SecondaryVendorId" NameIDSplit="-" Width="780" Placeholder="Secondary Vendor" 
                    queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwVENDORS WHERE ID = @P0 OR NAME = @P1"
                />
            </td>
        </tr>
    </table>
    <table width="800" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;" valign="top" align="left">
                <table cellpadding="4" cellspacing="0" border="0" width="200">
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="labelMaterialCost" runat="server" CssClass="SubSubHead" Text="Material Cost:<br />" />
                            <asp:TextBox ID="txtMaterialCost" runat="server" CssClass="Box required" style="text-align:right;" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" Table="vwPartInfos" Field="MaterialCost" Placeholder="Material Cost" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="labelnet" runat="server" CssClass="SubSubHead" Text="Catalog/Net Price:<br />" />
                            <asp:TextBox ID="txtNetPrice" runat="server" CssClass="Box required" style="text-align:right;" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" Table="vwPartInfos" Field="NetPrice" Placeholder="Catalog/Net Price" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Retail Price:<br />" />
                            <asp:TextBox ID="txtRetailPrice" runat="server" CssClass="Box required" style="text-align:right;" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" Table="vwPartInfos" Field="RetailPrice" Placeholder="Retail Price" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label4" runat="server" CssClass="SubSubHead" Text="Year Introduced:<br />" />
                            <asp:TextBox ID="txtYearIntroduced" runat="server" CssClass="Box required" onkeypress="return isNumberKey(event);" Table="vwPartInfos" Field="YearIntroduced" Placeholder="Year Introduced" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label6" runat="server" CssClass="SubSubHead" Text="Copyright Year:<br />" />
                            <asp:TextBox ID="txtCopyrightYear" runat="server" CssClass="Box" MaxLength="4" onkeypress="return isNumberKey(event);" Table="vwPartInfos" Field="CopyrightYear" Placeholder="Copyright Year" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label8" runat="server" CssClass="SubSubHead" Text="ISBN 13:<br />" />
                            <asp:TextBox ID="TextBox2" runat="server" CssClass="Box" onkeypress="return isNumberKey(event);" MaxLength="13" Table="vwPartInfos" Field="ISBN13" Placeholder="ISBN 13" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label10" runat="server" CssClass="SubSubHead" Text="ISBN 10:<br />" />
                            <asp:TextBox ID="TextBox4" runat="server" CssClass="Box" onkeypress="return isNumberKey(event);" MaxLength="10" Table="vwPartInfos" Field="ISBN10" Placeholder="ISBN 10" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label12" runat="server" CssClass="SubSubHead" Text="UPC Code:<br />" />
                            <asp:TextBox ID="TextBox5" runat="server" CssClass="Box" onkeypress="return isNumberKey(event);" MaxLength="14" Table="Parts" Field="UpcCode" Placeholder="UPC Code" Width="180" />
                        </td>
                    </tr>
                    <tr>
                    
                    </tr>
                </table>
            </td>
            <td style="width:200px;" valign="top" align="left">
                <table width="200" cellpadding="4" cellspacing="0" border="0">
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Length (in.):<br />" />
                            <asp:TextBox ID="txtLength" runat="server" CssClass="Box" style="text-align:right;" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" Table="vwEtaPartInfos" Field="Length" Placeholder="Length" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Width (in.):<br />" />
                            <asp:TextBox ID="txtWidth" runat="server" CssClass="Box" style="text-align:right;" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" Table="vwEtaPartInfos" Field="Width" Placeholder="Width" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label5" runat="server" CssClass="SubSubHead" Text="Height (in.):<br />" />
                            <asp:TextBox ID="txtHeight" runat="server" CssClass="Box" style="text-align:right;" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" Table="vwEtaPartInfos" Field="Height" Placeholder="Height" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label7" runat="server" CssClass="SubSubHead" Text="Weight (in.):<br />" />
                            <asp:TextBox ID="txtWeight" runat="server" CssClass="Box" style="text-align:right;" Format="Decimal" Places="4" onkeypress="return isDecimal(event);" Table="vwEtaPartInfos" Field="Weight" Placeholder="Height" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label9" runat="server" CssClass="SubSubHead" Text="Package Size:<br />" />
                            <asp:TextBox ID="txtPackageSize" runat="server" CssClass="Box required" onkeypress="return isNumberKey(event);" MaxLength="10" Table="vwPartInfos" Field="PackageSize" Placeholder="Package Size" Width="180" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label20" runat="server" CssClass="SubSubHead" Text="Low Grade:<br />" />
                            <asp:DropDownList ID="ddlLowGrade" runat="server" Width="180" CssClass="Box" Table="vwEtaPartInfos" Field="LowGrade" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="label13" runat="server" CssClass="SubSubHead" Text="High Grade:<br />" />
                            <asp:DropDownList ID="ddlHighGrade" runat="server" Width="180" CssClass="Box" Table="vwEtaPartInfos" Field="HighGrade" />
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:400px;" valign="top" align="left">
                <table width="400" cellpadding="4" cellspacing="0" border="0">
                    <tr>
                        <td valign="bottom">
                            <asp:Label ID="labelspacer" runat="server" CssClass="SubSubHead" Text="&nbsp;<br />" />
                            <asp:CheckBox ID="chkExclusiveItem" runat="server" CssClass="SubSubHead" TextAlign="right" Text="&nbsp;Exclusive Item" Table="vwEtaPartInfos" Field="ExclusiveItem" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:CheckBox ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="right" Text="&nbsp;Commodity Item" Table="vwEtaPartInfos" Field="CommodityItem" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:CheckBox ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="right" Text="&nbsp;Teacher Use" Table="vwEtaPartInfos" Field="TeacherUse" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:CheckBox ID="CheckBox3" runat="server" CssClass="SubSubHead" TextAlign="right" Text="&nbsp;Saleable Item" Table="vwEtaPartInfos" Field="SaleableItem" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:CheckBox ID="CheckBox4" runat="server" CssClass="SubSubHead" TextAlign="right" Text="&nbsp;Royalty Item" Table="vwPartInfos" Field="RoyaltyItem" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:CheckBox ID="CheckBox5" runat="server" CssClass="SubSubHead" TextAlign="right" Text="&nbsp;Do Not Ship International" Table="vwPartInfos" Field="DoNotShipInternational" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="bottom">
                            <asp:CheckBox ID="CheckBox6" runat="server" CssClass="SubSubHead" TextAlign="right" Text="&nbsp;Print on Packlist" Table="vwEtaPartInfos" Field="PrintonPacklist" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:Label ID="labeldiscount" runat="server" CssClass="SubSubHead" Text="Item Discount:<br />" />
                <asp:DropDownList ID="ddlItemDiscount" runat="server" CssClass="Box" Width="180" Table="vwEtaPartInfos" Field="ItemDiscount" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="labelprodcat1" runat="server" CssClass="SubSubHead" Text="Product Category 1:<br />" />
                <asp:DropDownList ID="ddlProductCategory1" runat="server" CssClass="Box" Width="780" Table="vwMarketingInfos" Field="ProductCategory1" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="label11" runat="server" CssClass="SubSubHead" Text="Product Category 2:<br />" />
                <asp:DropDownList ID="ddlProductCategory2" runat="server" CssClass="Box" Width="780" Table="vwMarketingInfos" Field="ProductCategory2" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="label14" runat="server" CssClass="SubSubHead" Text="Product Category 3:<br />" />
                <asp:DropDownList ID="ddlProductCategory3" runat="server" CssClass="Box" Width="780" Table="vwMarketingInfos" Field="ProductCategory3" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="label15" runat="server" CssClass="SubSubHead" Text="Product Category 4:<br />" />
                <asp:DropDownList ID="ddlProdcutCategory4" runat="server" CssClass="Box" Width="780" Table="vwMarketingInfos" Field="ProductCategory4" />
            </td>
        </tr>
        <tr>
            <td colspan="4" >
                <asp:Label ID="label16" runat="server" CssClass="SubSubHead" Text="Discontinued Reason:<br />" />
                <asp:DropDownList ID="ddlDiscontinuedReason" runat="server" CssClass="Box" Width="780" Table="vwPartInfos" Field="DiscontinedReason" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="label17" runat="server" CssClass="SubSubHead" Text="NMFC Code:<br />" />
                <asp:DropDownList ID="ddlNMFCCode" runat="server" CssClass="Box" Width="780" Table="vwEtaPartInfos" Field="NMFCCodeId" />
            </td>
        </tr>
        <tr>
            <td >
                <asp:Label ID="labelexpectedavail" runat="server" CssClass="SubSubHead" Text="Expected Available Date:<br />" />
                <telerik:RadDatePicker id="dtExpectedAvailableDate" runat="server" width="180" Placeholder="Expected Available Date" Table="vwPartInfos" Field="ExpectedAvailableDateTime" TargetField="ExpectedAvailable" />
            </td>
            <td  >
                <asp:Label ID="labelprintonpackllist" runat="server" CssClass="SubSubHead" Text="Annual Forecast Qty.:<br />" />
                <asp:TextBox ID="txtAnnualForecastQty" runat="server" Width="180" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwPartInfos" Field="FirstYearForecast" Placeholder="Annual Forecast Qty." />
            </td>
            <td >
                <asp:Label ID="labelFroecaststart" runat="server" CssClass="SubSubHead" Text="Forecast Start Date:<br />" />
                <telerik:RadDatePicker id="dtForecastStartDate" runat="server" width="180" Placeholder="Forecast Start Date" Table="vwEtaPartInfos" Field="ForecastStartDate" />
            </td>
            <td />
        </tr>
    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
