using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MPCHCostingEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Costing"; }}

        protected override void PopulateControls()
        {
            

            Controller controller = new Controller(entityId, environment);

         }


        protected override void SetRequirements()
        {
            //labelPartId.CssClass = "SubSubHead required";

        }

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = !NewRecord(ds, "vwPurchasingPartInfos");
        }

        protected override bool Validate()
        {
            
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            controller.ValidateDecimal(txtBurdenPercent, false, "Burden Percent", ref msg);
            controller.ValidateDecimal(txtBurdenPerUnit, false, "Burden Per Unit", ref msg);
            

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
        
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);

            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwPurchasingPartInfos", "Purchasing.Costing", NewRecord(ds, "vwPurchasingPartInfos"), controller);


            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwPurchasingPartInfos")) return;

            TableFields table = tables["vwPurchasingPartInfos"];
            if (!table.IsDirty()) return;


            //custom logic
        }
     
    }
}
 