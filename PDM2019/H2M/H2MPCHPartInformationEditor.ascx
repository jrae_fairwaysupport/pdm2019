<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPCHPartInformationEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPCHPartInformationEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    .readonly { background-color:#e8e2e2; }
         
    span.required
    {
        border-bottom: double 2px red;
    }          
    
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    input.required { border:solid 1px orange; }
    
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Planner User Id:" /></td>
            <td style="width:200px;"><asp:Label ID="labelbuyer" runat="server" CssClass="SubSubHead" Text="Buyer User Id:" /></td>
            <td style="width:200px;"><asp:Label ID="labelsafety" runat="server" CssClass="SubSubHead" Text="Safety Stock:" /></td>
            <td style="width:200px;"></td>
        </tr>
        <tr>
            <td >
                <asp:TextBox ID="txtCatalogdescription" runat="server" Width="180" MaxLength="100" Table="vwPurchasingPartInfos" Field="PlannerUserId" Placeholder="Planner User Id" />
            </td>
            <td >
                <asp:TextBox ID="TextBox6" runat="server" Width="180" MaxLength="100" Table="vwPurchasingPartInfos" Field="BuyerUserId" Placeholder="Buyer User Id" CssClass="Box required" />
                <asp:Label ID="lblBuyerUserId" runat='server' Visible="false" Table="PartInfos" Field="BuyerUserId" CssClass="required" />
            </td>
            <td >
                <asp:TextBox ID="TextBox7" runat="server" Width="180" MaxLength="10" Table="vwPurchasingPartInfos" Field="SafetyStock" Placeholder="Safety Stock" CssClass="Box required" />
            </td>
            <td />
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="labelPreferredVendor" runat="server" CssClass="SubSubHead" Text="Preferred Vendor:<br />" />
                <DNN:DNNTextSuggest ID="txtPreferredVendor" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPurchasingPartInfos" Field="PreferredVendorSuggest" TargetField="PreferredVendorId" NameIDSplit="-" Width="380" Placeholder="Preferred Vendor" 
                    queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwVENDORS WHERE ID = @P0 OR NAME = @P1"
                />

            </td>
            <td colspan="2">
                <asp:Label ID="label32" runat="server" CssClass="SubSubHead" Text="HTS Code:<br />" />
                <DNN:DNNTextSuggest ID="txtHTSCode" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPurchasingPartInfos" Field="HTSCodeSuggest" TargetField="HTSCode" NameIDSplit="-" Width="380" Placeholder="HTS Code" 
                    queryTmpl8 = "SELECT HTS_CODE AS ID, DESCRIPTION AS NAME FROM vwHTSCodes WHERE ((HTS_CODE LIKE '{0}%') OR (DESCRIPTION LIKE '{0}%')) ORDER BY HTS_CODE"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwHTSCodes WHERE HTS_CODE = @P0 OR DESCRIPTION = @P1"
                />

            </td>           
        </tr>

        <tr>
            <td>
                <asp:Label ID="label33" runat="server" CssClass="SubSubHead" Text="Duty Amount:<br />" />
                <asp:TextBox ID="txtDutyAmount" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="DutyRate" Placeholder="Duty Amount" />
            </td>
            <td>
                <asp:Label ID="label34" runat="server" CssClass="SubSubHead" Text="Add Rate:<br />" />
                <asp:TextBox ID="txtAddRate" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="AddRate" Placeholder="Add Rate" />
            </td>
            <td>
                <asp:Label ID="label35" runat="server" CssClass="SubSubHead" Text="Material Code:<br />" />
                <asp:TextBox ID="TextBox9" runat="server" CssClass="Box" Width="180" MaxLength="25" Table="vwPurchasingPartInfos" Field="MaterialCode" Placeholder="Material Code" />
            </td>
            <td>
                <asp:Label ID="label36" runat="server" CssClass="SubSubHead" Text="Engineering Master Id:<br />" />
                <asp:TextBox ID="TextBox10" runat="server" CssClass="Box required" Width="180" MaxLength="25" Table="vwPurchasingPartInfos" Field="EngineeringMasterId" Placeholder="Engineering Master Id" />
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <asp:Label ID="label37" runat="server" CssClass="SubSubHead" Text="Primaery Warehouse:<br />" />
                <DNN:DNNTextSuggest ID="txtPrimaryWarehouse" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box required"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPurchasingPartInfos" Field="PrimaryWarehouseSuggest" TargetField="PrimaryWarehouse" NameIDSplit="-" Width="380" Placeholder="Primary Warehouse" 
                    queryTmpl8 = "SELECT ID, DESCRIPTION AS NAME FROM vwWarehouse WHERE ((ID LIKE '{0}%') OR (DESCRIPTION LIKE '{0}%')) ORDER BY DESCRIPTION"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwWarehouse WHERE ID = @P0 OR DESCRIPTION = @P1"
                />

            </td>
            <td colspan="2">
                <asp:Label ID="label38" runat="server" CssClass="SubSubHead" Text="Foreign Warehouse:<br />" />
                <DNN:DNNTextSuggest ID="txtForeignWarehouse" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPurchasingPartInfos" Field="ForeignWarehouseSuggest" TargetField="ForeignWarehouse" NameIDSplit="-" Width="380" Placeholder="Foreign Warehouse" 
                    queryTmpl8 = "SELECT ID, DESCRIPTION AS NAME FROM vwWarehouse WHERE ((ID LIKE '{0}%') OR (DESCRIPTION LIKE '{0}%')) ORDER BY DESCRIPTION"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwWarehouse WHERE ID = @P0 OR DESCRIPTION = @P1"
                />

            </td>           
        </tr>
        <tr>
            <td>
                <asp:Label ID="label39" runat="server" CssClass="SubSubHead" Text="Product Status:<br />" />
                <asp:TextBox ID="TextBox11" runat="server" CssClass="Box" Width="180" MaxLength="25" Table="vwPurchasingPartInfos" Field="ProductStatus" Placeholder="Product Status" />
            </td>
            <td>
                <asp:Label ID="label40" runat="server" CssClass="SubSubHead" Text="Product Code:<br />" />
                <asp:TextBox ID="TextBox12" runat="server" CssClass="Box" Width="180" MaxLength="25" Table="vwPurchasingPartInfos" Field="ProductCode" Placeholder="Product Code" />
            </td>
            <td>
                <asp:Label ID="label41" runat="server" CssClass="SubSubHead" Text="Container Size:<br />" />
                <asp:DropDownList ID="ddlContainerSize" runat="server" CssClass="Box" Width="180" Table="vwPurchasingPartInfos" Field="ContainerSize" />
            </td>
            <td >
                <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Cubic Feet:<br />" />
                <asp:TextBox ID="txtCubicFeet" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="CubicFeet" Placeholder="Cubic Feet" />
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <asp:Label ID="label42" runat="server" CssClass="SubSubHead" Text="Country of Manufacture:<br />" />
                <DNN:DNNTextSuggest ID="txtCountryOfManufacture" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box required"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPurchasingPartInfos" Field="CountryOfOriginSuggest" TargetField="CountryOfOrigin" NameIDSplit="-" Width="380" Placeholder="Country of Manufacture" 
                    queryTmpl8 = "SELECT ID, DISPLAY_TEXT AS NAME FROM vwCountries WHERE ((ID LIKE '{0}%') OR (DISPLAY_TEXT LIKE '{0}%')) ORDER BY DISPLAY_TEXT"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwCountries WHERE ID = @P0 OR DISPLAY_TEXT = @P1"
                />

            </td>
            <td colspan="2">
                <asp:Label ID="label43" runat="server" CssClass="SubSubHead" Text="Port of Origin:<br />" />
                <DNN:DNNTextSuggest ID="txtPortOfOrigin" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box required"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPurchasingPartInfos" Field="PortOfOriginSuggest" TargetField="PortOfOrigin" NameIDSplit="-" Width="380" Placeholder="Port of Origin" 
                    queryTmpl8 = "SELECT ID, DISPLAY_TEXT AS NAME FROM vwCountries WHERE ((ID LIKE '{0}%') OR (DISPLAY_TEXT LIKE '{0}%')) ORDER BY DISPLAY_TEXT"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwCountries WHERE ID = @P0 OR DISPLAY_TEXT = @P1"
                />

            </td>           
        </tr>
        <tr>
            <td>
                <asp:Label ID="labelqafirst" runat="server" CssClass="SubSubHead" Text="QA First Time Receipts:<br />" />
                <asp:TextBox ID="txtQa" runat="server" CssClass="Box" Width="180" MaxLength="25" Table="vwPurchasingPartInfos" Field="QAFirstTimeReceipts" Placeholder="QA First Time Receipts" />
            </td>
            <td>
                <asp:Label ID="label10" runat="server" CssClass="SubSubHead" Text="Hold Receipts:<br />" />
                <asp:TextBox ID="TextBox1" runat="server" CssClass="Box" Width="180" MaxLength="25" Table="vwPurchasingPartInfos" Field="HoldReceipts" Placeholder="Hold Receipts" />
            </td>
            <td />
            <td />
        </tr>
        <tr>
            <td >
                <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Each Box Size Length (in.):<br />" />
                <asp:TextBox ID="txtEachBoxSizeLength" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="EachBoxSizeLength" Placeholder="Each Box Size Length (in.)" />
            </td>
            <td >
                <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Each Box Size Width (in.):<br />" />
                <asp:TextBox ID="txtEachBoxSizeWidth" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="EachBoxSizeWidth" Placeholder="Each Box Size Width (in.)" />
            </td>
            <td >
                <asp:Label ID="label4" runat="server" CssClass="SubSubHead" Text="Each Box Size Height (in.):<br />" />
                <asp:TextBox ID="txtEachBoxSizeHeight" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="EachBoxSizeHeight" Placeholder="Each Box Size Height (in.)" />
            </td>
            <td >
                <asp:Label ID="label5" runat="server" CssClass="SubSubHead" Text="Each Box Weight (lbs.):<br />" />
                <asp:TextBox ID="txtEachBoxWeight" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="EachWeight" Placeholder="Each Box Weight (lbs.)" />
            </td>
        
        </tr>
        <tr>
            <td >
                <asp:Label ID="label6" runat="server" CssClass="SubSubHead" Text="Case Box Size Length (in.):<br />" />
                <asp:TextBox ID="txtCaseBoxSizeLength" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="CaseBoxSizeLength" Placeholder="Case Box Size Length (in.)" />
            </td>
            <td >
                <asp:Label ID="label7" runat="server" CssClass="SubSubHead" Text="Case Box Size Width (in.):<br />" />
                <asp:TextBox ID="txtCaseBoxSizeWidth" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="CaseBoxSizeWidth" Placeholder="Case Box Size Width (in.)" />
            </td>
            <td >
                <asp:Label ID="label8" runat="server" CssClass="SubSubHead" Text="Case Box Size Height (in.):<br />" />
                <asp:TextBox ID="txtCaseBoxSizeHeight" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="CaseBoxSizeHeight" Placeholder="Case Box Size Height (in.)" />
            </td>
            <td >
                <asp:Label ID="label9" runat="server" CssClass="SubSubHead" Text="Case Box Weight (lbs.):<br />" />
                <asp:TextBox ID="txtCaseBoxWeight" runat="server" CssClass="Box" Width="180" MaxLength="10" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="vwPurchasingPartInfos" Field="CaseWeight" Placeholder="Case Box Weight (lbs.)" />
            </td>
        
        </tr>
        <tr>
            <td><asp:CheckBox ID="chkapprove" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Approve BOM" Table="vwPurchasingPartInfos" Field="ApproveBOM" /></td>
            <td colspan="2"><asp:CheckBox ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Review Completed for Part Import" Table="vwPurchasingPartInfos" Field="ReviewCompletedForPartImport" /></td>
            <td><asp:CheckBox ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Co-locate Flag" Table="vwPurchasingPartInfos" Field="CoLocateFlag" /></td>
        </tr>

        <tr>
            <td colspan="4">
                <asp:Label ID="labelSpecs" runat="server" CssClass="SubSubHead" Text="Purchasing Notes Printed:<br />" />
                <asp:TextBox ID="txtspecs" runat="server" CssClass="Box" Width="780" Height="100" Table="vwPurchasingPartInfos" Field="specification" Placeholder="Purchasing Notes Printed" TextMode="MultiLine"/>
            </td>
        </tr>

    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
