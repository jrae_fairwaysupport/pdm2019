using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MPCHPartInformationEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Purchasing Part Information"; }}

        protected override void PopulateControls()
        {
            

            Controller controller = new Controller(entityId, environment);
            controller.PopulateDropDownListOptions(ddlContainerSize, "Purchasing", "Container Size", "Select Container Size");
         }


        protected override void SetRequirements()
        {
            //labelPartId.CssClass = "SubSubHead required";

        }

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = !NewRecord(ds, "vwPurchasingPartInfos");
        }

        protected override bool Validate()
        {
            
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            controller.ValidateDecimal(txtDutyAmount, false, "Duty Amount", ref msg);
            controller.ValidateDecimal(txtAddRate, false, "Add Rate", ref msg);
            controller.ValidateDecimal(txtEachBoxSizeLength, false, "Each Box Size Length", ref msg);
            controller.ValidateDecimal(txtEachBoxSizeWidth, false, "Each Box Size Width", ref msg);
            controller.ValidateDecimal(txtEachBoxSizeHeight, false, "Each Box Size Height", ref msg);
            controller.ValidateDecimal(txtEachBoxWeight, false, "Each Box Weight", ref msg);
            controller.ValidateDecimal(txtCaseBoxSizeLength, false, "Case Box Size Length", ref msg);
            controller.ValidateDecimal(txtCaseBoxSizeWidth, false, "Case Box Size Width", ref msg);
            controller.ValidateDecimal(txtCaseBoxSizeHeight, false, "Case Box Size Height", ref msg);
            controller.ValidateDecimal(txtCaseBoxWeight, false, "Case Box Weight", ref msg);


            controller.DNNTestSuggestIsSelectionValid(txtPreferredVendor, "Preferred Vendor", ref msg);
            controller.DNNTestSuggestIsSelectionValid(txtHTSCode, "HTS Code", ref msg);
            controller.DNNTestSuggestIsSelectionValid(txtPrimaryWarehouse, "Primary Warehouse", ref msg);
            controller.DNNTestSuggestIsSelectionValid(txtForeignWarehouse, "Foreign Warehouse", ref msg);
            controller.DNNTestSuggestIsSelectionValid(txtCountryOfManufacture, "Country of Manufacture", ref msg);
            controller.DNNTestSuggestIsSelectionValid(txtPortOfOrigin, "Port of Origin", ref msg);


            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
        
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);

            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwPurchasingPartInfos", "Purchasing.Part Information", NewRecord(ds, "vwPurchasingPartInfos"), controller);
            SaveTableEdits(tables, "PartInfos", "Purchasing.Part Information", NewRecord(ds, "PartInfos"), controller);

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwPurchasingPartInfos")) return;

            TableFields table = tables["vwPurchasingPartInfos"];
            if (!table.IsDirty()) return;


            //custom logic
            if (table.Fields["BuyerUserId"].HasChanged())
            {
                if (!tables.ContainsKey("PartInfos"))
                {
                    tables.Add("PartInfos", new TableFields());
                    tables["PartInfos"].AddField("BuyerUserId", table.Fields["BuyerUserId"].oldValue, table.Fields["BuyerUserId"].newValue);
                }
            }
            table.RemoveField("BuyerUserId");
        }
     
    }
}
 