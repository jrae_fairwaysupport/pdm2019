<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPDBowkerEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPDBowkerEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    .readonly { background-color:#e8e2e2; }
         
    span.required
    {
        border-bottom: double 2px red;
    }          
    
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Supplier Name:" /></td>
            <td style="width:200px;"></td>
            <td style="width:200px;"></td>
            <td style="width:200px;"></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:TextBox ID="txtCatalogdescription" runat="server" Width="780" MaxLength="200" Table="vwBowker" Field="Name" Placeholder="Supplier Name" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="labelTitle" runat="server" CssClass="SubSubHead" Text="Title Bowker:<br />" />
                <asp:TextBox ID="TextBox1" runat="server" Width="780" MaxLength="200" Table="vwBowker" Field="Title" Placeholder="Title Bowker" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="label19" runat="server" CssClass="SubSubHead" Text="Subtitle:<br />" />
                <asp:TextBox ID="TextBox2" runat="server" Width="780" MaxLength="500" Table="vwBowker" Field="SubTitle" Placeholder="Subtitle" TextMode="MultiLine" Height="100" />
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                <asp:CheckBox ID="chkListedBowker" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;ETA ISBN Listed Bowker" Table="vwBowker" Field="ListedBowker" />
            </td>
            <td>
                <asp:Label ID="labelcontribut1" runat="server" CssClass="SubSubHead" Text="Contributor 1 Function:<br />" />
                <asp:DropDownList ID="ddlContributor1" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="Contributor1" />
            </td>
            <td>
                <asp:Label ID="label20" runat="server" CssClass="SubSubHead" Text="Contributor 2 Function:<br />" />
                <asp:DropDownList ID="ddlContributor2" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="Contributor2" />
            </td>
            <td />
        </tr>
        <tr>
            <td>
                <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Product:<br />" />
                <asp:DropDownList ID="ddlProduct" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="ProductForm" />
            </td>
            <td>
                <asp:Label ID="label5" runat="server" CssClass="SubSubHead" Text="Language:<br />" />
                <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="Language" />
            </td>
            <td>
                <asp:Label ID="label21" runat="server" CssClass="SubSubHead" Text="Audience:<br />" />
                <asp:DropDownList ID="ddlAudience" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="Audience" />
            </td>
            <td>
                <asp:Label ID="label22" runat="server" CssClass="SubSubHead" Text="BISAC Subject Code:<br />" />
                <asp:DropDownList ID="ddlBisac" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="BISACSubject" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="label27" runat="server" CssClass="SubSubHead" Text="Publisher:<br />" />
                <asp:TextBox ID="TextBox3" runat="server" Width="780" MaxLength="200" Table="vwBowker" Field="Publisher" Placeholder="Publisher" TextMode="MultiLine" Height="100" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="label23" runat="server" CssClass="SubSubHead" Text="Imprint Name:<br />" />
                <asp:DropDownList ID="ddlImprint" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="ImprintName" />
            </td>
            <td>
                <asp:Label ID="label24" runat="server" CssClass="SubSubHead" Text="Publishing Status:<br />" />
                <asp:DropDownList ID="ddlPublishStatus" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="PublishingStatus" />
            </td>
            <td>
                <asp:Label ID="label25" runat="server" CssClass="SubSubHead" Text="Currency:<br />" />
                <asp:TextBox ID="TextBox4" runat="server" Width="180" MaxLength="10" Table="vwBowker" Field="Currency" Placeholder="Currency" />
            </td>
            <td />
        </tr>
        <tr>
            <td>
                <asp:Label ID="label26" runat="server" CssClass="SubSubHead" Text="Sales Rights Type:<br />" />
                <asp:DropDownList ID="ddlSalesRights" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="SalesRightsType" />
            </td>
            <td>
                <asp:Label ID="label28" runat="server" CssClass="SubSubHead" Text="Sales Rights Location(s):<br />" />
                <asp:DropDownList ID="ddlSalesLocation" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="SalesRightsLocation" />
            </td>
            <td>
                <asp:Label ID="label29" runat="server" CssClass="SubSubHead" Text="Barcode Indicator:<br />" />
                <asp:DropDownList ID="ddlBarcodeIndicator" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="BarcodeIndicator" />
            </td>
            <td>
                <asp:Label ID="label30" runat="server" CssClass="SubSubHead" Text="Returnable Indicator:<br />" />
                <asp:DropDownList ID="ddlReturnableIndicator" runat="server" CssClass="Box" Width="180" Table="vwBowker" Field="ReturnableIndicator" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Label ID="label31" runat="server" CssClass="SubSubHead" Text="Description:<br />" />
                <asp:TextBox ID="TextBox5" runat="server" Width="780" MaxLength="500" Table="vwBowker" Field="Description" Placeholder="Description" TextMode="MultiLine" Height="100" />
            </td>
        </tr>


    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
