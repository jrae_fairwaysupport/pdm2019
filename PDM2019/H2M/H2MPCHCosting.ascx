<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPCHCosting.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPCHCosting" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:187px;" class="SubSubHead">Burden Percent:</td>
                <td style="width:187px;" ><asp:Label id="Label22" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="BurdenPercent" Format="Decimal" Places="4" style="text-align:right;" /></td>
                <td style="width:187px;" class="SubSubHead">Burden Per Unit:</td>
                <td style="width:189px;" ><asp:Label id="Label15" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="BurdenPerUnit" Format="Decimal" Places="4" style="text-align:right;" /></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




