<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPDPartInformation.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPDPartInformation" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
<%--        <asp:HyperLink ID="lnkLists" runat="server" TabName="Product Development"><span class="actionButton">Lists</span></asp:HyperLink>
        <asp:Label ID="lblSpace" runat="server" Width="50" Text="&nbsp;" />--%>
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:200px;" class="SubSubHead">Catalog Description:</td>
                <td style="width:550px;" ><asp:Label id="lblCompany" runat="server" CssClass="Normal placeholder" Width="530" Table="vwMarketingInfos" Field="WebPartDescriptionBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Packaging Product Description:</td>
                <td class="SubSubHead"><asp:Label ID="lblpackaging" runat="server" CssClass="Normal placeholder" Width="530" Table="vwEtaPartInfos" Field="ProductDescriptionBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead required">Preferred Vendor:</td>
                <td class="SubSubHead"><asp:Label ID="Label1" runat="server" CssClass="Normal placeholder required" Width="530" Table="vwPartInfos" Field="PreferredVendorSuggest" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Secondary Vendor:</td>
                <td class="SubSubHead"><asp:Label ID="Label2" runat="server" CssClass="Normal placeholder" Width="530" Table="vwPartInfos" Field="SecondaryVendorSuggest" /></td>
            </tr>
        </table>
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:100px;" class="SubSubHead">Material Cost:</td>
                <td style="width:140px;"><asp:Label ID='lblmaterial' runat="server" CssClass="Normal placeholder required" Width="120" style="text-align:right;" Table="vwPartInfos" Field="MaterialCostText" /></td>
                <td style="width:100px;" class="SubSubHead">Length (in.):</td>
                <td style="width:140px;"><asp:Label ID='Label3' runat="server" CssClass="Normal placeholder" Width="120" style="text-align:right;" Table="vwEtaPartInfos" Field="Length" Format="Decimal" Places="4" /></td>
                <td style="width:270px;" class="SubSubHead"><asp:Image id="imgExclusive" runat="server" Table="vwEtaPartInfos" Field="ExclusiveItem" />&nbsp;Exclusive Item</td>
            </tr>
            <tr>
                <td class="SubSubHead">Catalog/Net Price:</td>
                <td><asp:Label ID="lblcatalog" runat="server" CssClass="Normal placeholder required" Width="120" style="text-align:right;" Table="vwPartInfos" Field="NetPriceText" /></td>
                <td colspan="3" />
            </tr>
            <tr>
                <td class="SubSubHead">Retail Price:</td>
                <td ><asp:Label ID='Label4' runat="server" CssClass="Normal placeholder required" Width="120" style="text-align:right;" Table="vwPartInfos" Field="RetailPriceText" /></td>
                <td class="SubSubHead">Width (in.):</td>
                <td ><asp:Label ID='Label5' runat="server" CssClass="Normal placeholder" Width="120" style="text-align:right;" Table="vwEtaPartInfos" Field="Width" Format="Decimal" Places="4" /></td>
                <td class="SubSubHead"><asp:Image id="Image1" runat="server" Table="vwEtaPartInfos" Field="CommodityItem" />&nbsp;Commodity Item</td>
            </tr>
            <tr>
                <td class="SubSubHead">Year Introduced:</td>
                <td ><asp:Label ID='Label6' runat="server" CssClass="Normal placeholder required" Width="120" style="text-align:right;" Table="vwPartInfos" Field="YearIntroduced" /></td>
                <td class="SubSubHead">Height (in.):</td>
                <td ><asp:Label ID='Label7' runat="server" CssClass="Normal placeholder" Width="120" style="text-align:right;" Table="vwEtaPartInfos" Field="Height" Format="Decimal" Places="4" /></td>
                <td class="SubSubHead"><asp:Image id="Image2" runat="server" Table="vwEtaPartInfos" Field="TeacherUse" />&nbsp;Teacher Use</td>
            </tr>
           <tr>
                <td class="SubSubHead">Copyright Year:</td>
                <td ><asp:Label ID='Label8' runat="server" CssClass="Normal placeholder" Width="120" style="text-align:right;" Table="vwPartInfos" Field="CopyrightYear" /></td>
                <td class="SubSubHead">Weight (lbs.):</td>
                <td ><asp:Label ID='Label9' runat="server" CssClass="Normal placeholder" Width="120" style="text-align:right;" Table="vwEtaPartInfos" Field="Weight" Format="Decimal" Places="4" /></td>
                <td class="SubSubHead"><asp:Image id="Image3" runat="server" Table="vwEtaPartInfos" Field="SaleableItem" />&nbsp;Saleable Item</td>
            </tr>
            <tr>
                <td class="SubSubHead">ISBN 13:</td>
                <td ><asp:Label ID='Label10' runat="server" CssClass="Normal placeholder" Width="120" Table="vwPartInfos" Field="ISBN13" /></td>
                <td class="SubSubHead" >Package Size:
                <td ><asp:Label ID='Label20' runat="server" CssClass="Normal placeholder required" Width="120" Table="vwPartInfos" Field="PackageSize"   /></td>
                <td class="SubSubHead"><asp:Image id="Image4" runat="server" Table="vwPartInfos" Field="RoyaltyItem" />&nbsp;Royalty Item</td>
            </tr>
            <tr>
                <td class="SubSubHead">ISBN 10:</td>
                <td ><asp:Label ID='Label12' runat="server" CssClass="Normal placeholder" Width="120" Table="vwPartInfos" Field="ISBN10" /></td>
                <td class="SubSubHead">Low Grade:</td>
                <td ><asp:Label ID='Label13' runat="server" CssClass="Normal placeholder" Width="120" Table="vwEtaPartInfos" Field="LowGrade" /></td>
                <td class="SubSubHead"><asp:Image id="Image6" runat="server" Table="vwPartInfos" Field="DoNotShipInternational" />&nbsp;Do Not Ship International</td>
            </tr>
            <tr>
                <td class="SubSubHead">UPC Code:</td>
                <td ><asp:Label ID='Label14' runat="server" CssClass="Normal placeholder" Width="120" Table="Parts" Field="UpcCode" /></td>
                <td class="SubSubHead">High Grade:</td>
                <td ><asp:Label ID='Label17' runat="server" CssClass="Normal placeholder" Width="120" Table="vwEtaPartInfos" Field="HighGrade" /></td>
                <td class="SubSubHead"><asp:Image id="Image7" runat="server" Table="vwEtaPartInfos" Field="PrintOnPacklist" />&nbsp;Print on Packlist</td>
            </tr>
         
            <tr>
                <td class="SubSubHead">Item Discount:</td>
                <td colspan="2"><asp:Label ID="labeldiscount" runat="server" CssClass="Normal placeholder" Width="260" Table="vwETAPartInfos" Field="ItemDiscountText" /></td>
                <td />
                <td />
            </tr>
        </table>

        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead">Product Category 1:</td>
                <td style="width:600px;" colspan="3" ><asp:Label id="lblpc1" runat="server" CssClass="Normal placeholder" Width="580" Table="vwMarketingInfos" Field="ProductCategoryDescription1" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Product Category 2:</td>
                <td colspan="3"><asp:Label ID="lblpc2" runat="server" CssClass="Normal placeholder" Width="580" Table="vwMarketingInfos" Field="ProductCategoryDescription2" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Product Category 3:</td>
                <td colspan="3"><asp:Label ID="Label18" runat="server" CssClass="Normal placeholder" Width="580" Table="vwMarketingInfos" Field="ProductCategoryDescription3" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Product Category 4:</td>
                <td colspan="3"><asp:Label ID="Label19" runat="server" CssClass="Normal placeholder" Width="580" Table="vwMarketingInfos" Field="ProductCategoryDescription4" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Discontinued Reason:</td>
                <td colspan="3"><asp:Label ID="Lbldisc" runat="server" CssClass="Normal placeholder" Width="580" Table="vwPartInfos" Field="DiscontinuedReasonText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">NMFC Code:</td>
                <td colspan="3"><asp:Label ID="lblnmfc" runat="server" CssClass="Normal placeholder" Width="580" Table="vwEtaPartInfos" Field="NMFCCodeText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Expected Available Date:</td>
                <td ><asp:Label ID='Label11' runat="server" CssClass="Normal placeholder" Width="155" Table="vwPartInfos" Field="ExpectedAvailableText"   /></td>
                <td class="SubSubHead" style="width:175px;">Annual Forecast Qty.:</td>
                <td style="width:175px;" ><asp:Label ID='Label15' runat="server" CssClass="Normal placeholder" Width="155" Table="vwPartInfos" Field="FirstYearForecast"   /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Forecast Start Date:</td>
                <td><asp:Label ID='Label16' runat="server" CssClass="Normal placeholder" Width="155" Table="vwEtaPartInfos" Field="ForecastStartDateDisplay"   /></td>
                <td />
                <td />
            </tr>

            <tr>
                <td class="SubSubHead">Item Disposition:</td>
                <td><asp:Label ID="Label21" runat="server" CssClass="Normal placeholder" Width="180" Table="vwMarketingInfos" Field="ItemDisposition" /></td>
                <td class="SubSubHead">Product Originator:</td>
                <td><asp:Label ID="lbloriginator" runat="server" CssClass="Normal placeholder" Width="155" Table="vwEtaPartInfos" Field="ProductOriginator" /></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




