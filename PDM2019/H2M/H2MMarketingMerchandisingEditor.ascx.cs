using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MMarketingMerchandisingEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Marketing Merchandising"; }}

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = !NewRecord(ds, "vwMarketingMerchandising");
        }

        protected override bool Validate()
        {
            
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;


            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
        
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);

            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwMarketingMerchandising", "Marketing.Merchandising", NewRecord(ds, "vwMarketingMerchandising"), controller);

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwMarketingMerchandising")) return;

            TableFields table = tables["vwMarketingMerchandising"];
            if (!table.IsDirty()) return;


            //custom logic
        }
     
    }
}
 