using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MPDVendorPriceEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Vendor Price Breaks"; } }

        protected override DataSet LoadData()
        {
            DataSet ds = base.LoadData();

            lblNoVendorPriceBreaks.Visible = ds.Tables["vwVendorPriceBreaks"].Rows.Count == 0;
            return ds;
        }

        protected override void LockDownControls()
        {
            if (!partStatus.Equals("Pending")) return;
            if (!ViewRole.Equals("pd")) return;

            lnkAddVendorPriceBreak.Visible = false;

        }

        protected override bool Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);

            controller.ValidateDecimal(txtDevelopmentCosts, false, "Development Costs", ref msg);
            controller.ValidateDecimal(txtMold, false, "Mold", ref msg);
            controller.ValidateDecimal(txtPrintingFee, false, "Printing Fee", ref msg);
            controller.ValidateDecimal(txtPrototype, false, "Prototype", ref msg);
            controller.ValidateDecimal(txtOtherCosts, false, "Other Costs", ref msg);
            if (chkComboPricing.Checked) controller.ValidateDecimal(txtComboPrice, false, "Combo Price", ref msg);

            controller.DNNTestSuggestIsSelectionValid(txtVendorId, "Vendor Id", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {

                CloseForm();
                return;
            }


            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwPartVendorPrices");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);
            SaveTableEdits(tables, "vwPartVendorPrices", "Product Development.Vendor Price", newRecord, controller);
            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields>tables, Controller controller)
        {
            if (!tables.ContainsKey("vwPartVendorPrices")) return;


            TableFields table = tables["vwPartVendorPrices"];
            if (!table.IsDirty()) return; 

            //custom logic

        }
        
        protected void VendorPriceBreak_Delete(object obj, CommandEventArgs args) 
        {
            ImageButton btn = (ImageButton)obj;
            int recordId = Int32.Parse(btn.Attributes["RecordId"]);

            Controller controller = new Controller(entityId, environment);
            controller.VendorPriceBreak_Delete(this.UserInfo.Username, recordId, PartId);
            IsDirty = true;
            VendorPriceBreak_Load();
        }

        protected void VendorPriceBreak_Save(object obj, CommandEventArgs args) 
        {
            string msg = string.Empty;
            Controller controller = new Controller(entityId, environment);

            controller.ValidateText(txtVendorId, true, "Vendor Id", ref msg);
            controller.ValidateText(txtVendorPartId, true, "Vendor Part Id", ref msg);
            controller.ValidateNumber(txtQuantity1, false, "Quantity 1", ref msg);
            controller.ValidateDecimal(txtPrice1, false, "Unit Price 1", ref msg);
            controller.ValidateNumber(txtQuantity2, false, "Quantity 2", ref msg);
            controller.ValidateDecimal(txtPrice2, false, "Unit Price 2", ref msg);
            controller.ValidateNumber(txtQuantity3, false, "Quantity 3", ref msg);
            controller.ValidateDecimal(txtPrice3, false, "Unit Price 3", ref msg);
            controller.ValidateNumber(txtQuantity4, false, "Quantity 4", ref msg);
            controller.ValidateDecimal(txtPrice4, false, "Unit Price 4", ref msg);
            controller.ValidateNumber(txtQuantity5, false, "Quantity 5", ref msg);
            controller.ValidateDecimal(txtPrice5, false, "Unit Price 5", ref msg);
            
            hdnShowMessage.Value = msg;

            if (!string.IsNullOrEmpty(msg)) return;

            int recordId = Int32.Parse(btnAddComponentSave.CommandArgument.ToString());
            controller.VendorPriceBreak_Save(this.UserInfo.Username, recordId, PartId, pnlAddVendorPriceBreak);
            IsDirty = true;
            VendorPriceBreak_Load();

            VendorPriceBreak_Toggle(false);
        }

        protected void VendorPriceBreak_Load () 
        {
            Controller controller = new Controller(entityId, environment);

            DataSet ds = controller.LoadPart(PartId, "vwVendorPriceBreaks");
            lstVendorPriceBreaks.DataSource = ds.Tables["vwVendorPriceBreaks"];
            lstVendorPriceBreaks.DataBind();
            lblNoVendorPriceBreaks.Visible = ds.Tables["vwVendorPriceBreaks"].Rows.Count == 0;
        }

        protected void VendorPriceBreak_Toggle(object obj, CommandEventArgs args)
        {
            bool open = args.CommandArgument.ToString().Equals("Open");
            VendorPriceBreak_Toggle(open);

            if (!open) return;
            ImageButton btn = (ImageButton)obj;
            int recordId = Int32.Parse(btn.Attributes["RecordId"]);
            btnAddComponentSave.CommandArgument = recordId.ToString();
            lblAddPriceBreak.Text = (recordId == -1) ? "Add Vendor Price Break" : "Edit Vendor Price Break";


            if (recordId == -1) return;

            Controller controller = new Controller(entityId, environment);
            DataTable dt = controller.VendorPriceBreak_Load(recordId);
            if (dt.Rows.Count == 0) return;

            controller.PopulateForm(pnlAddVendorPriceBreak, dt);

        }

        private void VendorPriceBreak_Toggle(bool open)
        {
            pnlMain.Visible = !open;
            pnlAddVendorPriceBreak.Visible = open;

            Controller controller = new Controller(entityId, environment);
            DataTable dt = controller.VendorPriceBreak_Load(-1);

            controller.PopulateForm(pnlAddVendorPriceBreak, dt);
            controller.StripAllErrors(pnlAddVendorPriceBreak);
        }

       
 }
}
 