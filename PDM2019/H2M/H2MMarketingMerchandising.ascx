<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingMerchandising.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingMerchandising" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:187px;" class="SubSubHead">Catalog Title:</td>
                <td style="width:563px;" colspan="3" ><asp:Label id="Label22" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMerchandising" Field="CatalogTitle" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Marketing Copy:</td>
                <td colspan="3" ><asp:Label id="Label14" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMerchandising" Field="MarketingCopyBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Product Benefits:</td>
                <td colspan="3" ><asp:Label id="Label1" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMerchandising" Field="ProductBenefitsBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Skills Copy:</td>
                <td colspan="3" ><asp:Label id="Label2" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMerchandising" Field="SkillsCopyBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Legal/Tech Spec Footnote:</td>
                <td colspan="3" ><asp:Label id="Label3" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMerchandising" Field="LegalTechSpecFootnote" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Catalog Page:</td>
                <td colspan="3" ><asp:Label id="Label4" runat="server" CssClass="Normal placeholder" Width="150" Table="vwMarketingMerchandising" Field="CatalogPage" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Brand Subhead 1:</td>
                <td colspan="3" ><asp:Label id="Label5" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMerchandising" Field="BrandSubheadBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Brand Copy 1:</td>
                <td colspan="3" ><asp:Label id="Label6" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMerchandising" Field="BrandCopy1BR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Brand Copy 2:</td>
                <td colspan="3" ><asp:Label id="Label7" runat="server" CssClass="Normal placeholder" Width="543" Table="vwMarketingMerchandising" Field="BrandCopy2BR" /></td>
            </tr>



        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




