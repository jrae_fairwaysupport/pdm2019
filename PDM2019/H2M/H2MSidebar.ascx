<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MSidebar.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MSidebar" %>

<style>
    div.offset { padding-left:4px;
    }
    td.flush { padding: 2px 2px 2px 1px; 
               text-align:left;
               vertical-align:middle;
               width:100%;
               }
    td.indent { padding: 2px 2px 2px 5px; 
                text-align:left;
                vertical-align:middle;
                width:100%;
                }

    span.button 
    {
        width:75px;
        height:20px;
        margin-left:15px;
        text-align:center;
        vertical-align:baseline;
        font-size:12px;
        border-radius:3.5px;
        padding: 2.1px 6.3px 3.15px 6.3px;
        color: #FFFFFF;
        display:inline-block;
    }
    
    span.actionButton 
    {
        width:100px;
        margin-top:2px;
        margin-left:15px;
        text-align:center;
        vertical-align:middle;
        font-size:12px;
        border-radius:3.5px;
        border-color: #224295;
        border-width:1px;
        border-style:solid;
        padding: 2px 4px 4px 4px;
        
        color: #224295;
        display:inline-block;
    }
    
    span.actionButton:hover
    {
        color: #FFFFFF;
        background-color: #224295;
    }

    span.Pending { background-color: #224295; }
    span.Rejected { background-color: #d9534f; }
    span.Draft { background-color: #777; }
    span.Approved { background-color: #5cb85c; }
    span.Revision { background-color: #69f; }                    

</style>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        var msg = jQuery('[id*="hdnShowSidebarMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowSidebarMessage"]').val("");



    });


</script>

<asp:Panel ID="pnlMain" runat="server" Width="200" style="border-radius: 5px; border:solid 1px black; padding: 4px;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td class="flush">
                <asp:Label ID="lblextended" runat="server" CssClass="SubSubHead" Text="Extended Description:" />
            </td>
        </tr>
        <tr>
            <td class="indent">
                <asp:Label ID="lblextendeddesc" runat="server" CssClass="Normal" Table="Parts" Field="ExtendedDescription" />
            </td>
        </tr>
        <tr>
            <td class="flush">
                <asp:Label ID="Label1" runat="server" CssClass="SubSubHead" Text="Item Type:" />
            </td>
        </tr>
        <tr>
            <td class="indent">
                <asp:Label ID="Label2" runat="server" CssClass="Normal" Table="vwETAPartInfos" Field="ItemTypeName" />
            </td>
        </tr>
        <tr>
            <td class="indent Normal">
                <asp:Image ID="Image9" runat="server" Table="vwETAPartInfos" Field="IsThirdPartyItem" />&nbsp;Third Party Item<br />
                <asp:Image ID="Image1" runat="server" Table="vwETAPartInfos" Field="ForMarketPlace" />&nbsp;Marketplace<br />
                <asp:Image ID="Image2" runat="server" Table="vwETAPartInfos" Field="IsWebItem" />&nbsp;Web Item<br />
                <asp:Image ID="Image3" runat="server" Table="vwETAPartInfos" Field="DropShipItem" />&nbsp;Drop Ship Item
            </td>
        </tr>


        <tr>
            <td class="flush">
                <asp:Label ID="lblabc" runat="server" CssClass="SubSubHead" Text="ABC Code:" />            
            </td>        
        </tr>
        <tr>
            <td class="indent">
                <asp:Label ID="lblabcvalue" runat="server" CssClass="Normal" Table="Parts" Field="ABCCode" />    
            </td>
        </tr>

        <tr>
            <td class="flush" style="border-top:solid 1px #e2e8e8; text-align:center;">
                <asp:Label ID="lblStatus" runat="server" CssClass="button"  />
            </td>
        </tr>
        
        <tr>
            <td class="flush" style="border-top:solid 1px #e2e8e8; text-align:center;">
                <asp:LinkButton ID="lnkSubmit" runat="server" OnClientClick="javascript:return confirm('Are you sure you want to Submit this Part for Approval?');" OnCommand="DoSubmit"><span class="actionButton">Submit</span></asp:LinkButton>
                <asp:LinkButton ID="lnkApprove" runat="server" OnClientClick="javascript:return confirm('Are you sure you wish to Approve this Part?');" OnCommand="DoApprove" ><span class="actionButton">Approve</span></asp:LinkButton>
                <asp:HyperLink ID="lnkReject" runat="server" ><span class="actionButton">Reject</span></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="flush" style="border-top:solid 1px #e2e8e8; text-align:center;">
                <asp:HyperLink ID="lnkPartHistory" runat="server"><span class="actionButton">View History</span></asp:HyperLink>
            </td>
        </tr>

    </table>
</asp:Panel>


<%--<asp:HiddenField ID="hdnModuleId" runat="server" Value="-1" />--%>
<asp:HiddenField ID="hdnShowSidebarMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />        