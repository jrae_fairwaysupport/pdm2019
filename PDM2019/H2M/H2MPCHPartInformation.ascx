<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPCHPartInformation.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPCHPartInformation" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:187px;" class="SubSubHead">Planner UserId:</td>
                <td style="width:187px;" ><asp:Label id="Label22" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="PlannerUserId" /></td>
                <td style="width:187px;" class="SubSubHead">Buyer UserId:</td>
                <td style="width:189px;" ><asp:Label id="Label15" runat="server" CssClass="Normal placeholder required" Width="167" Table="vwPurchasingPartInfos" Field="BuyerUserId" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Safety Stock:</td>
                <td ><asp:Label id="Label1" runat="server" CssClass="Normal placeholder required" Width="167" Table="vwPurchasingPartInfos" Field="SafetyStock" /></td>
                <td />
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Preferred Vendor Id:</td>
                <td colspan="3"><asp:Label id="Label16" runat="server" CssClass="Normal placeholder" Width="543" Table="vwPurchasingPartInfos" Field="PreferredVendorSuggest" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">HTS Code:</td>
                <td colspan="3"><asp:Label id="Label17" runat="server" CssClass="Normal placeholder" Width="543" Table="vwPurchasingPartInfos" Field="HTSCodeSuggest" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Duty Amount:</td>
                <td  ><asp:Label id="Label18" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="DutyRate" style="text-align:right;" Format="Decimal" Places="4" /></td>
                <td class="SubSubHead">Material Code:</td>
                <td style="width:189px;" ><asp:Label id="Label19" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="MaterialCode" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Add Rate:</td>
                <td  ><asp:Label id="Label20" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="AddRate" style="text-align:right;" Format="Decimal" Places="4" /></td>
                <td class="SubSubHead">Engineering Master Id:</td>
                <td style="width:189px;" ><asp:Label id="Label21" runat="server" CssClass="Normal placeholder required" Width="167" Table="vwPurchasingPartInfos" Field="EngineeringMasterId" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Primary Warehouse:</td>
                <td colspan="3"  ><asp:Label id="Label25" runat="server" CssClass="Normal placeholder required" Width="543" Table="vwPurchasingPartInfos" Field="PrimaryWarehouseSuggest" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Foreign Warehouse:</td>
                <td colspan="3" ><asp:Label id="Label27" runat="server" CssClass="Normal placeholder" Width="543" Table="vwPurchasingPartInfos" Field="ForeignWarehouseSuggest" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Product Status:</td>
                <td ><asp:Label id="Label26" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="ProductStatus" /></td>
                <td class="SubSubHead">Product Code:</td>
                <td  ><asp:Label id="Label28" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="ProductCode" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Country of Manufacture:</td>
                <td  ><asp:Label id="Label29" runat="server" CssClass="Normal placeholder required" Width="167" Table="vwPurchasingPartInfos" Field="CountryOfOriginSuggest" /></td>
                <td class="SubSubHead">Container Size:</td>
                <td  ><asp:Label id="Label30" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="ContainerSize" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Port of Origin:</td>
                <td  ><asp:Label id="Label31" runat="server" CssClass="Normal placeholder required" Width="167" Table="vwPurchasingPartInfos" Field="PortOfOriginSuggest" /></td>
                <td class="SubSubHead">Cubit Feet:</td>
                <td> <asp:Label id="Label4" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="CubicFeet" Format="Decimal" Places="4" style="text-align:right;" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">QA First Time Receipts:</td>
                <td  ><asp:Label id="Label2" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="QAFirstTimeReceipts" /></td>
                <td class="SubSubHead">Hold Receipts:</td>
                <td  ><asp:Label id="Label3" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="HoldReceipts" /></td>
            </tr>
        </table>
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:375px;" class="SubSubHead" />
                <td style="width:125px;" />
                <td style="width:125px;" class="SubSubHead" align="center"><u>Each</u></td>
                <td style="width:125px;" class="SubSubHead" align="center"><u>Case</u></td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgApproveBom" runat="server" Table="vwPurchasingPartInfos" Field="ApproveBOM" />&nbsp;Approve BOM</td>
                <td class="SubSubHead" align="right" style="padding-right:4px;">Length (in.):</td>
                <td> <asp:Label id="Label5" runat="server" CssClass="Normal placeholder" Width="115" Table="vwPurchasingPartInfos" Field="EachBoxSizeLength" Format="Decimal" Places="4" style="text-align:right;" /></td>
                <td> <asp:Label id="Label6" runat="server" CssClass="Normal placeholder" Width="115" Table="vwPurchasingPartInfos" Field="CaseBoxSizeLength" Format="Decimal" Places="4" style="text-align:right;" /></td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgreview" runat="server" Table="vwPurchasingPartInfos" Field="ReviewCompletedForPartImport" />&nbsp;Review Completed for Part Import</td>
                <td class="SubSubHead" align="right" style="padding-right:4px;">Width (in.):</td>
                <td> <asp:Label id="Label7" runat="server" CssClass="Normal placeholder" Width="115" Table="vwPurchasingPartInfos" Field="EachBoxSizeWidth" Format="Decimal" Places="4" style="text-align:right;" /></td>
                <td> <asp:Label id="Label8" runat="server" CssClass="Normal placeholder" Width="115" Table="vwPurchasingPartInfos" Field="CaseBoxSizeWidth" Format="Decimal" Places="4" style="text-align:right;" /></td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgcolocate" runat="server" Table="vwPurchasingPartInfos" Field="CoLocateFlag" />&nbsp;Co-locate Flag</td>
                <td class="SubSubHead" align="right" style="padding-right:4px;">Height (in.):</td>
                <td> <asp:Label id="Label9" runat="server" CssClass="Normal placeholder" Width="115" Table="vwPurchasingPartInfos" Field="EachBoxSizeHeight" Format="Decimal" Places="4" style="text-align:right;" /></td>
                <td> <asp:Label id="Label10" runat="server" CssClass="Normal placeholder" Width="115" Table="vwPurchasingPartInfos" Field="CaseBoxSizeHeight" Format="Decimal" Places="4" style="text-align:right;" /></td>
            </tr>
            <tr>
                <td />
                <td class="SubSubHead" align="right" style="padding-right:4px;">Weight (lbs.):</td>
                <td> <asp:Label id="Label11" runat="server" CssClass="Normal placeholder" Width="115" Table="vwPurchasingPartInfos" Field="EachWeight" Format="Decimal" Places="4" style="text-align:right;" /></td>
                <td> <asp:Label id="Label12" runat="server" CssClass="Normal placeholder" Width="115" Table="vwPurchasingPartInfos" Field="CaseWeight" Format="Decimal" Places="4" style="text-align:right;" /></td>
            </tr>
        </table>
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:187px;" class="SubSubHead" valign="top">Purchasing Notes Printed:</td>
                <td style="width:563px;" ><asp:Label id="Label13" runat="server" CssClass="Normal placeholder" Width="543" Table="vwPurchasingPartInfos" Field="SpecificationBR" /></td>
            </tr>

        </table>
        <hr />
        NOTE: <b>MOLDS</b> skipped as it appears to not be used (no records exist in database).
        <hr />
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




