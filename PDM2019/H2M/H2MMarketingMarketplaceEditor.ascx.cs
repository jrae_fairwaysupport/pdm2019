using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MMarketingMarketplaceEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Marketing Marketplace"; }}

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = !NewRecord(ds, "vwMarketingMarketplace");
        }

        protected override bool Validate()
        {
            
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            controller.ValidateNumber(txtAmazonMinOrderQty, false, "Amazon Min. Order Qty.", ref msg);
            controller.ValidateDecimal(txtAmazonListPrice, false, "Amazon List Price", ref msg);
            controller.ValidateDecimal(txtAmazonPrice, false, "Amazon Price", ref msg);
            controller.ValidateDecimal(txtAmazonSellPrice, false, "Amazon Sell Price", ref msg);
            controller.ValidateDecimal(txtFamisPrice, false, "FAMIS Price", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
        
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);

            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwMarketingMarketplace", "Marketing.Marketplace", NewRecord(ds, "vwMarketingMarketplace"), controller);

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwMarketingMarketplace")) return;

            TableFields table = tables["vwMarketingMarketplace"];
            if (!table.IsDirty()) return;


            //custom logic
        }
     
    }
}
 