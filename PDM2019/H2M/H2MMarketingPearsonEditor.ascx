<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingPearsonEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingPearsonEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    .readonly { background-color:#e8e2e2; }
         
    span.required
    {
        border-bottom: double 2px red;
    }          
    
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Kit Item Number:" /></td>
            <td style="width:200px;"></td>
            <td style="width:200px;"><asp:Label ID="labelbuyer" runat="server" CssClass="SubSubHead" Text="Pearson Kit Type:" /></td>
            <td style="width:200px;"></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtBurdenPercent" runat="server" CssClass="Box" Width="180" MaxLength="20" onkeypress="return isNumberKey(event);" Table="vwMarketingPearsonPortal" Field="KitItemNumber" Placeholder="Kit Item Number"/>
            </td>
            <td />
            <td>
                <asp:DropDownList ID="ddlkittype" runat="server" CssClass="Box" Width="180" Table="vwMarketingPearsonPortal" Field="PearsonKitType" />
            </td>
            <td />
            
        </tr>

        <tr>
            <td colspan="4">
                <asp:Label ID="labelname" runat="server" CssClass="SubSubHead" Text="Customer Name:<br />" />
                <asp:TextBox ID="txtname" runat="server" CssClass="Box" Width="780" MaxLength="250" Placeholder="Customer Name" Table="vwMarketingPearsonPortal" Field="CustomerName" />
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <asp:Label ID="labelpartof" runat="server" CssClass="SubSubHead" Text="Portal - Item Part Of:<br />" />
                <asp:TextBox ID="txtpartof" runat="server" CssClass="Box" Width="780" Height="50" MaxLength="50" TextMode="MultiLine" Placeholder="Portal - Item Part Of" Table="vwMarketingPearsonPortal" Field="ItemPartOf" />
            </td>
        </tr>
    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
