<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPDManagement.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPDManagement" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:187px;" class="SubSubHead">Patent Information:</td>
                <td style="width:563px;" colspan="3"><asp:Label id="Label22" runat="server" CssClass="Normal placeholder" Width="543" Table="vwProductManagement" Field="PatentInformation" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">h2m Branding Logo Status:</td>
                <td style="width:187px;"><asp:Label id="Label23" runat="server" CssClass="Normal placeholder" Width="167" Table="vwProductManagement" Field="BrandingLogoStatusText" /></td>
                <td style="width:100px;" class="SubSubHead">Reorder Flag:</td>
                <td style="width:289px;"><asp:Label id="Label24" runat="server" CssClass="Normal placeholder" Width="269" Table="vwProductManagement" Field="ReorderFlagText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Forecast PO Date:</td>
                <td ><asp:Label id="Label26" runat="server" CssClass="Normal placeholder" Width="167" Table="vwProductManagement" Field="ForecastPOText" /></td>
                <td class="SubSubHead">Reprint Status:</td>
                <td ><asp:Label id="Label25" runat="server" CssClass="Normal placeholder" Width="269" Table="vwProductManagement" Field="ReprintStatusText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead"></td>
                <td ></td>
                <td class="SubSubHead">File Status:</td>
                <td ><asp:Label id="Label27" runat="server" CssClass="Normal placeholder" Width="269" Table="vwProductManagement" Field="FileStatusText" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top">Reprint Notes:</td>
                <td colspan="3"><asp:Label id="Label28" runat="server" CssClass="Normal placeholder" Width="543" Table="vwProductManagement" Field="ReprintNotesBR" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Last Updated By:</td>
                <td ><asp:Label id="Label29" runat="server" CssClass="Normal placeholder" Width="167" Table="vwProductManagement" Field="UpdatedBy" /></td>
                <td class="SubSubHead">Last Updated On:</td>
                <td ><asp:Label id="Label30" runat="server" CssClass="Normal placeholder" Width="169" Table="vwProductManagement" Field="ModifiedDateText" /></td>
            </tr>


        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




