<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MCreativeTab.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MCreativeTab" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }
   
</style>


<script language="javascript" type="text/javascript">
    function ToggleCreativeMore(toggleWhat) {
        var link = jQuery('[id*="lnkCreativeViewMore' + toggleWhat + '"]');
        var lbl = jQuery('[id*="lblCreative' + toggleWhat + '"]');


        if (link.text() == "View More") {
            link.text("View Less");
            lbl.css("max-height", "");
        }
        else {
            link.text("View More");
            lbl.css("max-height", "200px");
        }
        return false;
    }
</script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>

    <asp:Panel id="pnlCreative" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead" valign="top">Links To Documents:</td>
                <td style="width:600px; border:solid 1px #e8e2e2;padding:2px;" colspan="4">
                    <asp:Label id="lblCreativeDocuments" runat="server" CssClass="Normal" Table="vwCreativeInfos" Field="LinksToDocumentsBR" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkCreativeViewMoreDocuments" runat="server" Text="View More" OnClientClick="javascript:return ToggleCreativeMore('Documents');" /></div>
                </td>
            </tr>

            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>

            <tr>
                <td style="width:150px;" class="SubSubHead" valign="top">Links To Images:</td>
                <td style="width:600px; border:solid 1px #e8e2e2;" colspan="4">
                    <asp:Label id="lblCreativeImages" runat="server" CssClass="Normal" Table="vwCreativeInfos" Field="LinksToImagesBR" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkCreativeViewMoreImages" runat="server" Text="View More" OnClientClick="javascript:return ToggleCreativeMore('Images');" /></div>
                </td>
            </tr>

            <tr>
                <td colspan="5">&nbsp;</td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top" >Other Links:</td>
                <td style="border:solid 1px #e8e2e2;" colspan="4">
                    <asp:Label id="lblCreativeOther" runat="server" CssClass="Normal" Table="vwCreativeInfos" Field="OtherLinksBR" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkCreativeViewMoreOther" runat="server" Text="View More" OnClientClick="javascript:return ToggleCreativeMore('Other');" /></div>
                </td>
            </tr>

  
        </table>


    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




