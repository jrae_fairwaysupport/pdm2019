<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPDSpecifications.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPDSpecifications" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<script language="javascript" type="text/javascript">
    function PDToggleMore() {

        lnk = jQuery('[id*="lnkPDViewMore"]').first();

        if (lnk.text() == "View Less") {
            lnk.text("View More");
            jQuery('[id*="lblPDSpecs"]').css("max-height", "200px");
        }
        else {
            lnk.text("View Less");
            jQuery('[id*="lblPDSpecs"]').css("max-height", "");
        }
        return false;
    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;" >
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlSpecifications" runat="server" Width="750" Height="600" ScrollBars="Auto">

        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead">Material:</td>
                <td style="width:150px;"><asp:Label ID="lblMaterial" runat="server" CssClass="Normal placeholder" Width="130" Table="vwProductSpecifications" Field="MaterialText" /></td>
                <td style="width:150px;" class="SubSubHead">Product Color(s):</td>
                <td style="width:150px;"><asp:Label id="lblColor" runat="server" CssClass="Normal placeholder" Width="130" Table="vwProductSpecifications" Field="ProductColor" /></td>
                <td style="width:150px;" align="right" class="SubSubHead"><asp:Image ID="imgprintmaterial" runat="server" Table="vwProductSpecifications" Field="PrintMaterial" />&nbsp;Print Material</td>
            </tr>
            <tr>
                <td class="SubSubHead">Key Requirement:</td>
                <td colspan="4"><asp:Label ID="Label1" runat="server" CssClass="Normal placeholder" Width="580" Table="vwProductSpecifications" Field="KeyRequirement" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Product Specifications:</td>
                <td colspan="4"><asp:Label ID="Label4" runat="server" CssClass="Normal placeholder" Width="580" Table="vwProductSpecifications" Field="ProductSpecification" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Other Printing on Product:</td>
                <td ><asp:Label ID="Label2" runat="server" CssClass="Normal placeholder" Width="130" Table="vwProductSpecifications" Field="OtherPrinting" /></td>
                <td class="SubSubHead">Trademark:</td>
                <td colspan="2"><asp:Label ID="Label3" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="Trademark" /></td>
            </tr>

            <tr>
                <td colspan="5" style="border-bottom: solid 1px #e8e2e2;padding-top:15px;" class="Head">Type of OEM Packaging</td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgHeader" runat="server" Table="Parts" Field="Header" />&nbsp;Header Card</td>
                <td class="SubSubHead"><asp:Image ID="imgPoly" runat="server" Table="Parts" Field="Polybag" />&nbsp;Polybag</td>
                <td class="SubSubHead"><asp:Image ID="imgZiploc" runat="server" Table="Parts" Field="ZiplocBag" />&nbsp;Ziploc Bag</td>
                <td class="SubSubHead"><asp:Image ID="imgTote" runat="server" Table="Parts" Field="Tote" />&nbsp;Tote</td>
                <td class="SubSubHead"><asp:Image ID="imgBox" runat="server" Table="Parts" Field="Box" />&nbsp;Box</td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgBlister" runat="server" Table="Parts" Field="Blister" />&nbsp;Blister/Clam</td>
                <td class="SubSubHead" colspan="2"><asp:Image ID="imgFrustration" runat="server" Table="Parts" Field="FFP" />&nbsp;Frustration Free Packaging</td>
                <td class="SubSubHead" colspan="2"><asp:Image ID="imgOther" runat="server" Table="Parts" Field="Other" />&nbsp;Other:&nbsp;&nbsp;<asp:Label ID="lblOtherPackaging" runat="server" CssClass="Normal placeholder" Width="200" Table="vwProductSpecifications" Field="PrintSpecs" /></td>
            </tr>

            <tr>
                <td colspan="5" style="border-bottom: solid 1px #e8e2e2;padding-top:15px;" class="Head">Print Product Specs</td>
            </tr>

            <tr>
                <td class="SubSubHead">Print Specs Page Count:</td>
                <td><asp:Label ID="lblPring" runat="server" CssClass="Normal placeholder" Width="130" Table="vwProductSpecifications" Field="PrintSpecs" /></td>
                <td />
                <td class="SubSubHead" align="center" ><u>Cover</u></td>
                <td class="SubSubHead" align="center" ><u>Body</u></td>
            </tr>
            <tr>
                <td class="SubSubHead">Bindery Type:</td>
                <td><asp:Label ID="lblBindery" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="BinderyTypeText" /></td>
                <td class="SubSubHead">Paper Grade:</td>
                <td valign="bottom"><asp:Label ID="lblcoverpp" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="CoverPaperGradeText"  /></td>
                <td valign="bottom"><asp:Label ID="Label5" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="BodyPaperGradeText"  /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Finishing Type:</td>
                <td><asp:Label ID="Label18" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="FinishingTypeText" /></td>
                <td class="SubSubHead">Paper Color:</td>
                <td valign="bottom"><asp:Label ID="Label19" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="CoverPaperColorText"  /></td>
                <td valign="bottom"><asp:Label ID="Label20" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="BodyPaperColorText"  /></td>
            </tr>
            

            <tr>
                <td class="SubSubHead">Finish Size:</td>
                <td><asp:Label ID="Label6" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="FinishSize" /></td>
                <td class="SubSubHead">Paper Weight:</td>
                <td valign="bottom"><asp:Label ID="Label7" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="CoverPaperWeightText"  /></td>
                <td valign="bottom"><asp:Label ID="Label8" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="BodyPaperWeightText"  /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Flat Size:</td>
                <td><asp:Label ID="Label9" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="FlatSize" /></td>
                <td class="SubSubHead">Coating:</td>
                <td><asp:Label ID="lblcoatin" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="CoverCoatingText" /></td>
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Packaging:</td>
                <td><asp:Label ID="Label11" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="PackagingText" /></td>
                <td class="SubSubHead">Pages, Ink Color:</td>
                <td valign="bottom"><asp:Label ID="Label12" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="CoverPagesInkColorText"  /></td>
                <td valign="bottom"><asp:Label ID="Label13" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="BodyPagesInkColorText"  /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Quantity per Package:</td>
                <td><asp:Label ID="Label14" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="QuantityPerPackage" /></td>
                <td class="SubSubHead"></td>
                <td valign="bottom" class="SubSubHead"><asp:Image ID="Label15" runat="server" Table="vwProductSpecifications" Field="CoverPagesBleed"  />&nbsp;Cover Pages Bleed</td>
                <td valign="bottom" class="SubSubHead"><asp:Image ID="Label16" runat="server" Table="vwProductSpecifications" Field="BodyPagesBleed"  />&nbsp;Body Pages Bleed</td>
            </tr>
            <tr>
                <td class="SubSubHead">Print Spec Notes:</td>
                <td colspan="4"><asp:Label ID="Label17" runat="server" CssClass="Normal placeholder" Width="580" Table="vwProductSpecifications" Field="PrintSpecNotes" /></td>
            </tr>

            <tr>
                <td colspan="5" style="border-bottom: solid 1px #e8e2e2;padding-top:15px;" class="Head">Print Product Specs</td>
            </tr>
            <tr>
                <td class="SubSubHead">About Author:</td>
                <td colspan="2"><asp:Label ID="lblaboutauth" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="AboutAuthor" /></td>
                <td />
                <td class="SubSubHead"><asp:Image ID="imgduallanguae" runat="server" Table="vwProductSpecifications" Field="DualLanguage" />&nbsp;Dual Language</td>
            </tr>

            <tr>
                <td class="SubSubHead">Author:</td>
                <td colspan="2" ><asp:Label ID="Label10" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="Author" /></td>
                <td class="SubSubHead">DRA:</td>
                <td ><asp:Label ID="Label24" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="DraText" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">Book Set:</td>
                <td ><asp:Label ID="Label21" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="BooksetText" /></td>
                <td colspan="2" class="SubSubHead">Book Type (hard cover or soft cover):</td>
                <td ><asp:Label ID="Label22" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="BookTypeText" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">Curriculum Area:</td>
                <td colspan="2" ><asp:Label ID="Label23" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="CurriculumArea" /></td>
                <td class="SubSubHead">Number of Pages:</td>
                <td ><asp:Label ID="Label30" runat="server" CssClass="Normal placeholder required" Width="120" Table="vwProductSpecifications" Field="NumberOfPages" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">Early Intervention Level:</td>
                <td colspan="2" ><asp:Label ID="Label25" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="EarlyInterventionLevel" /></td>
                <td class="SubSubHead">Guided Reading Level:</td>
                <td ><asp:Label ID="Label27" runat="server" CssClass="Normal placeholder" Width="120" Table="vwProductSpecifications" Field="GuidedReadingLevel" /></td>
            </tr>

            <tr>

                <td class="SubSubHead">Genre:</td>
                <td colspan="2" ><asp:Label ID="Label26" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="GenreText" /></td>
                <td></td>
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Illustrator/Photographer:</td>
                <td colspan="2" ><asp:Label ID="Label28" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="Illustrator" /></td>
                <td></td>
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Lexile Level:</td>
                <td colspan="2" ><asp:Label ID="Label29" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="LexileLevel" /></td>
                <td></td>
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Strand:</td>
                <td colspan="2" ><asp:Label ID="Label31" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="Strand" /></td>
                <td />
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Text Type:</td>
                <td colspan="2" ><asp:Label ID="Label32" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="TextTypeText" /></td>
                <td></td>
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Theme:</td>
                <td colspan="2" ><asp:Label ID="Label33" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="Theme" /></td>
                <td />
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Topic:</td>
                <td colspan="2" ><asp:Label ID="Label34" runat="server" CssClass="Normal placeholder" Width="280" Table="vwProductSpecifications" Field="Topic" /></td>
                <td></td>
                <td />
            </tr>

        </table>
                

    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




