<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MSidebarReject.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MSidebarReject" %>

<style>
    div.offset { padding-left:4px;
    }
   
</style>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        var msg = jQuery('[id*="hdnSidebarMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnSidebarMessage"]').val("");


        var url = jQuery('[id*="hdnCloseForm"]').val();
        if (url == "") return;
        jQuery('[id*="hdnCloseForm"]').val("");
        dnnModal.closePopUp(true, url);


    });

</script>


<asp:Panel ID="pnlMain" runat="server"  style="border-bottom:solid 1px black;">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:100%;" align="left" valign="top">
                <asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Please specify your reasons for Rejecting this Part:<br />" />
                <asp:TextBox ID="txtReason" runat="server" CssClass="Box" Width="780" TextMode="MultiLine" Height="450" Placeholder="Please specify reason for Rejecting this Part, and/or any revisions requested." Table="Parts" Field="RejectionReason" />
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtStatus" runat="server" Visible="false" Table="Parts" Field="Status" />
    <asp:TextBox ID="txtSku" runat="server" Visible="false" Table="Parts" Field="PartId" />
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:50%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="right" style="width:50%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnSidebarMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnCloseForm" runat="server" Value="" />





