<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MMarketingWebImages.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MMarketingWebImages" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server" ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="720" class="HeaderRow">
            <tr>
                <td style="width:100%;padding-left:4px;">Web Images</td>
            </tr>
        </table>

        <asp:DataList ID="lstWebImages" runat="server" Width="720" CssClass="table table-hover table-condensed" Table="vwMarketingWebImages"  OnDataBinding="VerifyDataListHasData" NoDataLabel="lblNoWebImages" >
            <ItemTemplate>
                <asp:Label width="600" ID="lblVendorId" runat="server" style="vertical-align:top;" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
                <asp:Label Width="100" ID="lblpriority" runat="server" style="vertical-align:top; text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Priority") %>' />
                <br />
            </ItemTemplate>
        </asp:DataList>
        <asp:Label ID="lblNoWebImages" runat="server" CssClass="Normal" Width="720" style="padding-left:4px;" Text="No Web Images have been added to this part." />
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




