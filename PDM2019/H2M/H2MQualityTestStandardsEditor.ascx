<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MQualityTestStandardsEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MQualityTestStandardsEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }


    function ToggleElectronics() {
        var checked = jQuery('[id*="chkElectronic"]:checked').length > 0;
        var labels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        if (checked) {
            jQuery('[id*="chkheadphones"]').show();
            jQuery('label[for*="chkheadphones"]').show();
            if (labels) jQuery('[id*="labelheadphoneqty"]').show();
            jQuery('[id*="lblheadphoneqty"]').show();
            jQuery('[id*="txtheadphoneqty"]').show();
            jQuery('[id*="chkBatteriesRequired"]').show();
            jQuery('label[for*="chkBatteriesRequired"]').show();
            
        }
        else {
            jQuery('[id*="chkheadphones"]').hide();
            jQuery('label[for*="chkheadphones"]').hide();
            jQuery('[id*="labelheadphoneqty"]').hide();
            jQuery('[id*="lblheadphoneqty"]').hide();
            jQuery('[id*="txtheadphoneqty"]').hide();
            jQuery('[id*="chkBatteriesRequired"]').hide();
            jQuery('label[for*="chkBatteriesRequired"]').hide();
        }
        ToggleBatteries();
    }

    function ToggleBatteries() {
        var checked = jQuery('[id*="chkBatteriesRequired"]:checked').length > 0;
        var electronic = jQuery('[id*="chkElectronic"]:checked').length > 0;

        if (checked && electronic)
            jQuery('[id*="pnlBatteries"]').show();
        else
            jQuery('[id*="pnlBatteries"]').hide();
    }

    

</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
        <table width="800" cellpadding="2" cellspacing="0" border="0">
            
            <tr>
                <td style="width:800px;" >
                    <asp:Label ID="labelAllergens" runat="server" CssClass="SubHead" Width="780" Text="USA: ASTM F963 Toy Safety" />
                </td>
            </tr>
            <tr>
                <td style="border:solid 1px #e8e2e2; padding-left:15px;">
                    <asp:CheckBox Width="325" ID="CheckBox6" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Mechanical &amp; Physical" Table="vwTestStandards" Field="MechanicalAndPhysical"  />
                    <asp:CheckBox Width="220" ID="CheckBox7" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Flammability Test" Table="vwTestStandards" Field="FlammabilityTest" />
                    <asp:CheckBox Width="220" ID="CheckBox8" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Heavy Metal Test" Table="vwTestStandards" Field="HeavyMetalTest"  />

                </td>
            </tr>

            <tr>
                <td style="width:800px;" >
                    <asp:Label ID="label1" runat="server" CssClass="SubHead" Width="780" Text="USA: CPSIA 2008 (H.R.4040) Children's Product" />
                </td>
            </tr>
            <tr>
                <td style="border:solid 1px #e8e2e2; padding-left:15px;">
                    <asp:CheckBox Width="325" ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Lead Content on Surface Coating (16 CFR 1303)" Table="vwTestStandards" Field="LeadContentOnSurfaceCoating"  />
                    <asp:CheckBox Width="220" ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="LeadContentInSubstrate" Text="&nbsp;Lead Content in Substrate" />
                    <asp:CheckBox Width="220" ID="CheckBox3" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="OtherTest" Text="&nbsp;Other Test" />
                    <br />

                    <asp:CheckBox Width="325" ID="CheckBox4" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="TestForBisphenol" Text="&nbsp;Test for Bisphenol A (BPA)" />
                    <asp:CheckBox Width="220" ID="CheckBox5" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="Lead" Text="&nbsp;Custom Testing - Contact QA" />
                    <asp:CheckBox Width="220" ID="CheckBox9" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="PhthalatesContent" Text="&nbsp;Phthalates" />
                    <br />

                    <asp:CheckBox Width="325" ID="CheckBox10" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="OtherCheck" TargetField="Other" Text="&nbsp;Wood Moisture 7% < MOISTURE LEVEL < 14%" CheckedValue="true" UncheckedValue="false" />
                    <asp:CheckBox Width="220" ID="CheckBox11" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="Other2Check" TargetField="Other2" Text="ASTM D4236: LHAMA"  CheckedValue="true" UncheckedValue="false" />
                    <br />

                    <asp:CheckBox Width="325" ID="CheckBox12" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="Other1Check" TargetField="Other1" Text="&nbsp;GCC Required (16 CFR 1303/CPSIA SECTION 102)" CheckedValue="true" UncheckedValue="false" />
                </td>
            </tr>

            <tr>
                <td style="width:800px;" >
                    <asp:Label ID="label2" runat="server" CssClass="SubHead" Width="780" Text="EU: EN71 Toy Safety" />
                </td>
            </tr>
            <tr>
                <td style="border:solid 1px #e8e2e2; padding-left:15px;">
                    <asp:CheckBox Width="325" ID="CheckBox13" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards"  Field="EN71Part1" Text="&nbsp;Part 1: Physical & Mechanical" />
                    <asp:CheckBox Width="220" ID="CheckBox14" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="EN71Part2" Text="&nbsp;Part 2: Flammability Test" />
                    <asp:CheckBox Width="220" ID="CheckBox15" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="EN71Part3" Text="&nbsp;Part 3: Toxic Elements Test" />
                    <br />

                    <asp:CheckBox Width="325" ID="CheckBox16" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards"  Field="EN71TotalCadmiun" Text="&nbsp;84/500/EC Total Cadmium" />
                    <asp:CheckBox Width="220" ID="CheckBox17" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="EN71Phthalates" Text="&nbsp;2005/84/EC Phthalates" />
                </td>
            </tr>

            <tr>
                <td style="width:800px;" >
                    <asp:Label ID="label3" runat="server" CssClass="SubHead" Width="780" Text="Canadian Toys Safety Requirement (CCPSA)" />
                </td>
            </tr>
            <tr>
                <td style="border:solid 1px #e8e2e2; padding-left:15px;">
                    <asp:CheckBox Width="325" ID="CheckBox18" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards"  Field="CanadianBenzeneContent" Text="&nbsp;Benzene Content" />
                    <asp:CheckBox Width="220" ID="CheckBox19" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="ToyRegulations" Text="&nbsp;Toy Regulations (SOR/2011-17)" />
                    <asp:CheckBox Width="220" ID="CheckBox20" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="CanadianPhysicalAndMechanical" Text="&nbsp;Physical & Mechanical" />
                    <br />

                    <asp:CheckBox Width="325" ID="CheckBox21" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards"  Field="CanadianPhthalates" Text="&nbsp;Phthalates Regulations (SOR/2010-298)" />
                    <asp:CheckBox Width="220" ID="CheckBox22" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="CanadianFlammabilityTest" Text="&nbsp;Flammability Test" />
                    <asp:CheckBox Width="220" ID="CheckBox23" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="CanadianToxicElementsTest" Text="&nbsp;Toxic Elements Test" />
                    <br />

                    <asp:CheckBox ID="CheckBox24" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards"  Field="TotalLeadContentInSurfaceCoatings" Text="&nbsp;Total Lead Content in Surface Coatings (SOR/2011-14)" />
                </td>
            </tr>

            <tr>
                <td style="width:800px;" >
                    <asp:Label ID="label4" runat="server" CssClass="SubHead" Width="780" Text="Other International Toy Safety Requirements" />
                </td>
            </tr>
            <tr>
                <td style="border:solid 1px #e8e2e2; padding-left:15px;">
                    <asp:CheckBox Width="325" ID="CheckBox25" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards"   Field="InternationalPhysicalAndMechanical" Text="&nbsp;International Testing - Contact QA" />
                    <asp:CheckBox Width="220" ID="CheckBox26" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwTestStandards" Field="ASNZS8124" Text="&nbsp;AS/NZS ISO 8124" />
                </td>
            </tr>


            
        </table>
    </asp:Panel>


    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" />
            </td>
        </tr>
    
    </table>
</asp:Panel>



<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<%--<asp:HiddenField ID="hdnShowPanel" runat="server" Value="Main" />--%>
