<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPCHContainer.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPCHContainer" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="H2MPCHPartInformation.ascx" TagName="partInformation" TagPrefix="pdm" %>
<%@ Register Src="H2MPCHPlanning.ascx" TagName="planning" TagPrefix="pdm" %>
<%@ Register Src="H2MPCHCosting.ascx" TagName="costing" TagPrefix="pdm" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server"  AutoPostBack="true" OnTabClick="LoadData"  >
        <Tabs>
            <telerik:RadTab Text="Part Information"/>
            <telerik:RadTab Text="Planning" />
            <telerik:RadTab Text="Costing" />
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;" >

        <telerik:RadPageView runat="server" ID="pgPartInformation" >
            <pdm:partInformation id="ctlPCHPartInformation" runat="server" Editors="p-*|ad-*" Editor="H2MPCHPartInformationEditor"  />
        </telerik:RadPageView> 

        <telerik:RadPageView runat="server" ID="pgPlanning" >
            <pdm:planning id="ctlPlanning" runat="server" Editors="p-*|ad-*" Editor="H2MPCHPlanningEditor" />
        </telerik:RadPageView> 
    
        <telerik:RadPageView runat="server" ID="pgCosting">
            <pdm:costing id="ctlCosting" runat="server"  Editors="p-*|ad-*"  Editor="H2MPCHCostingEditor"  />
        </telerik:RadPageView> 

    </telerik:RadMultiPage>
</asp:Panel>


<asp:HiddenField ID="hdnModuleId" runat="server" Value="-1" />




