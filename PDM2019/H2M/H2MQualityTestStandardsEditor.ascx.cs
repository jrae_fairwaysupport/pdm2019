using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MQualityTestStandardsEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Test Standards"; } }

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = true;
        }

        protected override bool Validate()
        {
            return true;
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwTestStandards");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwTestStandards", "Quality.Test Standards", newRecord, controller, "QAPartInfo_PartId");
            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwTestStandards")) return;

            TableFields table = tables["vwTestStandards"];
            if (!table.IsDirty()) return;

            //custom
        }


    
    }
}
 