<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MQualityPartInformation.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MQualityPartInformation" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
    .readonly { background-color:#e8e2e2; }
    
</style>

<script language="javascript" type="text/javascript">
    function ToggleMore() {
        lnk = jQuery('[id*="lnkViewMore"]');

        if (lnk.text() == "View More") {
            lnk.text("View Less");
            jQuery('[id*="lblSpecs"]').css("max-height", "");
        }
        else {
            lnk.text("View More");
            jQuery('[id*="lblSpecs"]').css("max-height", "200px");
        }
        return false;
    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead" valign="top">Specifications:</td>
                <td style="width:600px; border:solid 1px #e8e2e2;padding:2px;" colspan="4">
                    <asp:Label id="lblSpecs" runat="server" CssClass="Normal" Table="vwQAPartInfos" Field="SpecificationBR" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkViewMore" runat="server" Text="View More" OnClientClick="javascript:return ToggleMore();" /></div>
                </td>
            </tr>

            <tr>
                <td class="SubSubHead" valign="top">Inspection Notes:</td>
                <td colspan="4" valign="top"><asp:Label ID="lblInspectionNotes" runat="server" CssClass="Normal placeholder" Table="vwQAPartInfos" Field="QAInspectionNotesBR" Width="580" /></td>
            </tr>

            <tr>
                <td class="SubSubHead"><asp:Image ID="imgFeatureSet" runat="server" Table="vwPartSpecifications" Field="FeatureSet" />&nbsp;Feature Set</td>
                <td class="SubSubHead" style="width:150px;"><asp:Image id="imgLotType" runat="server" Table="vwQAPartInfos" Field="LotTypeChecked" />&nbsp;Lot Type</td>
                <td style="width:150px;" />
                <td style="width:150px;" class="SubSubHead"><asp:Image ID="imgHeadphoneJacks" runat="server" Table="vwPartSpecifications" Field="HeadphoneJacks" />&nbsp;Headphone Jacks</td>
                <td class="SubSubHead" style="width:150px;">Quantity:&nbsp;<asp:Label ID="lblHeadphoneJack" runat="server" CssClass="Normal placeholder" Width="75" Table="vwPartSpecifications" Field="HeadphoneJackQuantity" /></td>
            </tr>

            <tr>
                <td />
                <td class="SubSubHead" ><asp:Image ID="imgBatteries" runat="server" Table="vwPartSpecifications" Field="Batteries" />&nbsp;Batteries Required</td>
                <td class="SubSubHead"><asp:Image ID="imgBatteryIncluded" runat="server" Table="vwPartSpecifications" Field="BatteriesIncluded" />&nbsp;Batteries Included</td>
                <td class="SubSubHead">Battery Size:&nbsp;<asp:Label ID="labelBatterySize" runat="server" CssClass="Normal placeholder" Width="75" Table="vwPartSpecifications" Field="BatterySize" /></td>
                <td class="SubSubHead">Quantity:&nbsp;<asp:Label ID="labelBatteryQty" runat="server" CssClass="Normal placeholder" Width="75" Table="vwPartSpecifications" Field="BatteryQuantity" /></td>
            </tr>
            
            
            <tr>
                <td class="SubSubHead"><asp:Image ID="chkDiswasherSave" runat="server" Table="vwQAPartInfos" Field="DishwasherSafe" />&nbsp;Dishwasher Safe</td>
                <td class="SubSubHead"><asp:Image ID="chkFoodSafe" runat="server" Table="vwQAPartInfos" Field="FoodSafe" />&nbsp;Food Safe</td>
                <td class="SubSubHead"><asp:Image ID="chkExempt" runat="server" Table="vwQAPartInfos" Field="Exempt" />&nbsp;Exempt</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="chkComponentContainsPaint" runat="server" Table="vwQAPartInfos" Field="PaintTesting" />&nbsp;Component Contains Paint</td>
            </tr>
            
            <tr>
                <td colspan="3" class="SubSubHead">Initial Report Date:&nbsp;<asp:Label ID="lblInitialReportDate" runat="server" Table="vwQAPartInfos" Field="InitialTestReportDate" Format="Date" Width="75" CssClass="Normal placeholder" /></td>
                <td colspan="2" class="SubSubHead"><asp:Image ID="imgHazardousMaterials" runat="server" Table="vwQAPartInfos" Field="HazardousMaterial" />&nbsp;Hazardous Materials</td>
            </tr>

            <tr>
                <td class="SubSubHead">Allergens</td>
                <td class="SubSubHead"><asp:Image ID="imgLatexAllergen" runat="server" Table="vwQAPartInfos" Field="LatexAllergen" />&nbsp;Latex</td>
                <td class="SubSubHead"><asp:Image ID="imgPvcAllergen" runat="server" Table="vwQAPartInfos" Field="PvcAllergen" />&nbsp;PVC</td>
                <td class="SubSubHead" style="width:150px;"><asp:Image ID="imgBPAAllergen" runat="server" Table="vwQAPartInfos" Field="BPAAllergen" />&nbsp;BPA</td>
                <td class="SubSubHead" style="width:150px;"><asp:Image id="imgPolycarbonatesAllergen" runat="server" Table="vwQAPartInfos" Field="PolycarbonatesAllergen" />&nbsp;Polycarbonates</td>
            </tr>

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead"><asp:Image ID="imgTCEPAllergen" runat="server" Table="vwQAPartInfos" Field="TCEPAllergen" />&nbsp;TCEP</td>
                <td class="SubSubHead"><asp:Image ID="imgPVsAllergen" runat="server" Table="vwQAPartInfos" Field="PVSAllergen" />&nbsp;PVS</td>
                <td class="SubSubHead"><asp:Image ID="imgTolueneAllergen" runat="server" Table="vwQAPartInfos" Field="TolueneAllergen" />&nbsp;Toluene</td>
                <td class="SubSubHead"><asp:Image ID="imgFormaldehydeAllergen" runat="server" Table="vwQAPartInfos" Field="FormaldehydeAllergen" />&nbsp;Formaldehyde</td>
            </tr>
            
            <tr>
                <td class="SubSubHead">Package Warnings</td>
                <td class="SubSubHead"><asp:Image id="imgcewarning" runat="server" Table="vwQAPartInfos" Field="CEWarning" />&nbsp;CE </td>
                <td class="SubSubHead"><asp:Image id="Image1" runat="server" Table="vwQAPartInfos" Field="SmallPartsWarning" />&nbsp;Small Parts </td>
                <td class="SubSubHead"><asp:Image id="Image2" runat="server" Table="vwQAPartInfos" Field="SmallBallWarning" />&nbsp;Small Ball </td>
                <td class="SubSubHead"><asp:Image id="Image3" runat="server" Table="vwQAPartInfos" Field="OwlPellets" />&nbsp;Owl Pellets </td>
            </tr>
            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead"><asp:Image id="Image4" runat="server" Table="vwQAPartInfos" Field="MarbleWarning" />&nbsp;Marble </td>
                <td class="SubSubHead"><asp:Image id="Image5" runat="server" Table="vwQAPartInfos" Field="LatexBalloonsWarning" />&nbsp;Latext Balloons</td>
                <td class="SubSubHead"><asp:Image id="Image6" runat="server" Table="vwQAPartInfos" Field="NoBabyWarning" />&nbsp;No Baby</td>
                <td class="SubSubHead"><asp:Image id="Image7" runat="server" Table="vwQAPartInfos" Field="CordWarning" />&nbsp;Cord</td>
            </tr>
            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead"><asp:Image id="Image8" runat="server" Table="vwQAPartInfos" Field="MagnetWarning" />&nbsp;Magnet</td>
                <td class="SubSubHead"><asp:Image id="Image9" runat="server" Table="vwQAPartInfos" Field="SharpFunctionalPointWarning" />&nbsp;Sharp Functional Point</td>
                <td class="SubSubHead"><asp:Image id="Image10" runat="server" Table="vwQAPartInfos" Field="MarbleInKit" />&nbsp;Marble In Kit</td>
                <td class="SubSubHead"><asp:Image id="Image11" runat="server" Table="vwQAPartInfos" Field="BallInKit" />&nbsp;Ball In Kit</td>
            </tr>

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead"><asp:Image id="Image36" runat="server" Table="vwQAPartInfos" Field="Projectile" />&nbsp;Projectile</td>
                <td class="SubSubHead"></td>
                <td class="SubSubHead"></td>
                <td class="SubSubHead"></td>
            </tr>


            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image12" runat="server" Table="vwQAPartInfos" Field="NotHumanConsumption" />&nbsp;Not for Human Consumption</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image15" runat="server" Table="vwQAPartInfos" Field="NotSafetyProtection" />&nbsp;Not for Safety Protection</td>
            </tr>

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead" ><asp:Image id="Image16" runat="server" Table="vwQAPartInfos" Field="CCCWarning" />&nbsp;CCC</td>
                <td class="SubSubHead" colspan="2">Description:&nbsp;<asp:Label ID="lblCCCDescription" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="CCCDescription" /></td>
                <td class="SubSubHead" >CCC Date:&nbsp;<asp:Label ID="cccdate" runat="server" CssClass="Normal placeholder" Width="75" Table="vwQAPartInfos" Field="CCCDate" Format="Date" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead" ><asp:Image id="Image13" runat="server" Table="vwQAPartInfos" Field="WashBeforeUseWarning" />&nbsp;Wash Before Use</td>
                <td class="SubSubHead" colspan="3">Description:&nbsp;<asp:Label ID="Label1" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="WashBeforeUseDescription" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead" ><asp:Image id="Image14" runat="server" Table="vwQAPartInfos" Field="ShellfishWarning" />&nbsp;Shell Fish</td>
                <td class="SubSubHead" colspan="3">Description:&nbsp;<asp:Label ID="Label2" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="ShellfishDescription" /></td>
            </tr>
	
            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead"><asp:Image id="Image17" runat="server" Table="vwQAPartInfos" Field="AdultSupervision" />&nbsp;Adult Supervision</td>
                <td class="SubSubHead"><asp:Image id="Image18" runat="server" Table="vwQAPartInfos" Field="Prop65CarcinogensWarning" />&nbsp;Prop65-Carcinogens</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image19" runat="server" Table="vwQAPartInfos" Field="MeetsANSIZ87" />&nbsp;Meets ANSIZ87.1 Standards</td>
            </tr>

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead"><asp:Image id="Image20" runat="server" Table="vwQAPartInfos" Field="LatextWarning" />&nbsp;Latex Warning</td>
                <td class="SubSubHead" colspan="3"><asp:Image id="Image21" runat="server" Table="vwQAPartInfos" Field="Prop65ReproductiveToxicantsWarning" />&nbsp;Prop65-Reproductive Toxicants</td>
            </tr>

           <tr>
                <td class="SubSubHead">Material Info</td>
                <td class="SubSubHead"><asp:Image id="Image22" runat="server" Table="vwQAPartInfos" Field="Steel" />&nbsp;Steel </td>
                <td class="SubSubHead"><asp:Image id="Image23" runat="server" Table="vwQAPartInfos" Field="MDFWood" />&nbsp;MDF Wood</td>
                <td class="SubSubHead"><asp:Image id="Image24" runat="server" Table="vwQAPartInfos" Field="Cotton" />&nbsp;Cotton </td>
                <td class="SubSubHead"><asp:Image id="Image25" runat="server" Table="vwQAPartInfos" Field="MineralMaterial" />&nbsp;Mineral</td>
            </tr>

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead"><asp:Image id="Image26" runat="server" Table="vwQAPartInfos" Field="LatexMaterial" />&nbsp;Latex</td>
                <td class="SubSubHead"><asp:Image id="Image27" runat="server" Table="vwQAPartInfos" Field="ABS" />&nbsp;ABS</td>
                <td class="SubSubHead"><asp:Image id="Image28" runat="server" Table="vwQAPartInfos" Field="StainlessSteelMaterial" />&nbsp;Stainless Steel</td>
                <td class="SubSubHead"><asp:Image id="Image29" runat="server" Table="vwQAPartInfos" Field="IronMaterial" />&nbsp;Iron</td>
            </tr>            

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead"><asp:Image id="Image30" runat="server" Table="vwQAPartInfos" Field="BeechwoodMaterial" />&nbsp;Beech Wood</td>
                <td class="SubSubHead"><asp:Image id="Image31" runat="server" Table="vwQAPartInfos" Field="PineWoodMaterial" />&nbsp;Pine Wood</td>
                <td class="SubSubHead"><asp:Image id="Image32" runat="server" Table="vwQAPartInfos" Field="PolyesterMaterial" />&nbsp;Polyester</td>
                <td class="SubSubHead"><asp:Image id="Image33" runat="server" Table="vwQAPartInfos" Field="PolyCottonBlendMaterial" />&nbsp;Poly-cotton Blend</td>
            </tr>            

            <tr>
                <td class="SubSubHead">&nbsp;</td>                                                                     
                <td class="SubSubHead" colspan="2"><asp:Image id="Image34" runat="server" Table="vwQAPartInfos" Field="PolypropyleneMaterial" />&nbsp;CoPP (Co-polymer Polypropylene)</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image35" runat="server" Table="vwQAPartInfos" Field="GPPSGeneral" />&nbsp;GPPS (General Purpose Polystyrene)</td>
            </tr>            

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image50" runat="server" Table="vwQAPartInfos" Field="HDPE" />&nbsp;HDPE (High Density Polyethylene)</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image51" runat="server" Table="vwQAPartInfos" Field="HIPS" />&nbsp;HIPS (High Impact Polystyrene)</td>
            </tr>

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image38" runat="server" Table="vwQAPartInfos" Field="LDPE" />&nbsp;LDPE (Low Density Polyethylene)</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image39" runat="server" Table="vwQAPartInfos" Field="PePolyethylene" />&nbsp;PE (Polyethylene)</td>
            </tr>            

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image48" runat="server" Table="vwQAPartInfos" Field="PetPolyethylene" />&nbsp;PET (Polyethylene Terephthalate)</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image49" runat="server" Table="vwQAPartInfos" Field="PPPolyethylene" />&nbsp;PP (Polypropylene)</td>
            </tr>            

            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image43" runat="server" Table="vwQAPartInfos" Field="PomPolyoxmethylene" />&nbsp;POM (Polyoxmethylene)</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="Image46" runat="server" Table="vwQAPartInfos" Field="Thermoplastic" />&nbsp;Thermoplastic Rubber</td>
            </tr>            


            <tr>
                <td class="SubSubHead">&nbsp;</td>
                <td class="SubSubHead"><asp:Image id="Image42" runat="server" Table="vwQAPartInfos" Field="PMMA" />&nbsp;PMMA</td>
                <td class="SubSubHead"><asp:Image id="Image47" runat="server" Table="vwQAPartInfos" Field="EVAMaterial" />&nbsp;EVA</td>
                <td class="SubSubHead"><asp:Image id="Image44" runat="server" Table="vwQAPartInfos" Field="PsPolystyrene" />&nbsp;PS (Polystyrene)</td>
                <td class="SubSubHead"><asp:Image id="Image45" runat="server" Table="vwQAPartInfos" Field="PVC" />&nbsp;PVC</td>
            </tr>            

            <tr>
                <td class="SubSubHead" colspan="2"><asp:Image ID="imgwercs" runat="server" Table="vwQAPartInfos" Field="WERCS" />&nbsp;Registered with WERCS?</td>
                <td colspan="3" class="SubSubHead">WERCS Classification:&nbsp;<asp:Label ID="lblwercsclassification" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="WERCSClassification" /></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




