<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MQualityContainer.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MQualityContainer" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="H2MQualityPartInformation.ascx" TagName="partInformation" TagPrefix="pdm" %>
<%@ Register Src="H2MQualityTestStandards.ascx" TagName="testStandards" TagPrefix="pdm" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server"  AutoPostBack="true" OnTabClick="LoadData" >
        <Tabs>
            <telerik:RadTab Text="Part Information"/>
            <telerik:RadTab Text="Test Standards" />
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;" >

        <telerik:RadPageView runat="server" ID="pgPartInformation" >
            <pdm:partInformation id="ctlPartInformation" runat="server" Editors="q-*|ad-*" Editor="H2MQualityPartInformationEditor" />
        </telerik:RadPageView> 
    
        <telerik:RadPageView runat="server" ID="pgTestStandards">
            <pdm:testStandards id="ctlTestStandards" runat="server" Editors="q-*|ad-*" Editor="H2MQualityTestStandardsEditor"  />
        </telerik:RadPageView> 

    </telerik:RadMultiPage>
</asp:Panel>







