using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MHeaderEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return PartId > 0 ? "PDM Part Header Editor" : "PDM New Part"; }}

        protected override void PopulateControls()
        {
            chkToggleLabels.Checked = (PartId > 0);

            Controller controller = new Controller(entityId, environment);
            controller.PopulateDropDownListOptions(ddlItemType, "Sidebar", "ItemTypes", "Specify an Item Type");

         }
    
        protected override void SetRequirements()
        {
            labelPartId.CssClass = "SubSubHead required";
            labelDescription.CssClass = "SubSubHead required";

        }

        protected override void SetDefaultValues(DataSet ds)
        {
            //this is only valid for HeaderEditor, need another mechanism for all other editors that gets in a DS?
            if (PartId != -1) return;
            txtabc.Text = "N";
        }

        protected override bool Validate()
        {
            txtPartId.Text = txtPartId.Text.Trim().ToUpper();
            txtDescription.Text = txtDescription.Text.Trim().ToUpper();

            
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            string mSku = controller.ValidateText(txtPartId, true, "Part Id", ref msg);
            string description = controller.ValidateText(txtDescription, true, "Description", ref msg);

            //if it's empty, we'll already have an error to throw
            if (!string.IsNullOrEmpty(mSku))
            {
                string sql = "SELECT COUNT(*) AS THECOUNT FROM vwPartList WHERE PartId = @P0 AND (id <> @P1 OR @P1 = -1)";
                int count = controller.pdm.FetchSingleValue<int>(sql, "THECOUNT", mSku, PartId);
                if (count > 0) msg += "\r\nPart Id must be unique in the database.\r\n";
            }

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
        
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);

            bool newPart = (PartId == -1);

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);

            TableFields table = tables["Parts"];
            table.AddField("ModifiedDate", null, DateTime.Now);
            int mPartId = PartId;


            if (newPart)
            {
                table.AddField("CreatedDate", null, DateTime.Now);
                table.AddField("Status", null, "Draft");
                table.AddField("WasApproved", null, false);
                table.AddField("IsLockedForRevision", null, false);
                table.AddField("IsRevisedPart", null, false);
                table.AddField("LoadPartRevisions", null, false);
                table.AddField("Active", null, true);

                mPartId = controller.pdm.InsertRecord("Parts", "Id", table.GetParametersAsArray(false));
                controller.history.Log(mPartId, this.UserInfo.Username, "Header", "Created Part", string.Empty, string.Empty);

                SaveUserSetting(mPartId, "Status", "Draft");
            }
            else
            {
                if (tables["Parts"].IsDirty("Id"))
                {
                    controller.history.Log(mPartId, this.UserInfo.Username, "Header", table.Fields, "ModifiedDate", "CreatedDate", "Id");
                    controller.pdm.UpdateRecord("Parts", "Id", mPartId, table.GetParametersAsArray(true, "Id"));
                }
            }


            SaveUserSetting(mPartId, "Sku", txtPartId.Text);

            table = tables["vwETAPartInfos"];
            newPart = NewRecord(ds, "vwETAPartInfos");
            if (table.IsDirty())
            {
                table.AddField("ModifiedDate", null, DateTime.Now);
                if (newPart)
                {
                    //controller.history.Log(mPartId, this.UserInfo.Username, "Header", table.Fields, "ModifiedDate");
                    table.AddField("PartId", null, mPartId);
                    table.AddField("CreatedDate", null, DateTime.Now);
                    table.AddField("ProductOriginator", null, this.UserInfo.Username);
                    controller.pdm.InsertRecord("EtaPartInfos", "PartId", table.GetParametersAsArray(false));
                }
                else
                {
                    controller.history.Log(mPartId, this.UserInfo.Username, "Header", table.Fields, "ModifiedDate");
                    controller.pdm.UpdateRecord("EtaPartInfos", "PartId", mPartId, table.GetParametersAsArray(true, "ModifiedDate"));
                }
            }

            if (newPart)
            {
                string tmpl8 = string.Format("~/PDMPartEditor{0}/e/{1}/id/{2}/r/{3}/eid/{0}", entityId, environment, mPartId, ViewRole);
                string url = ResolveUrl(tmpl8);

                Response.Redirect(url, true);
                //Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "id", newPartId.ToString(), "new", newPart.ToString(), "r", viewRole), true);

            }
            else
            {
                CloseForm();
            }

        }

       
     
    }
}
 