using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class H2MQualityPartInformationEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Quality Part Information"; } }

        protected override bool Validate()
        {
            return true;
        }

        protected override void SetDefaultValues(DataSet ds)
        {
            //bool newRecord = NewRecord(ds, "vwQAPartInfos");
            //chkToggleLabels.Checked = !newRecord;
            chkToggleLabels.Checked = true;
        }

 

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {

                CloseForm();
                return;
            }

            if (!Validate()) return;

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwQAPartInfos");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwPartSpecifications", "Quality.Specifications", NewRecord(ds, "vwPartSpecifications"), controller);
            SaveTableEdits(tables, "vwQAPartInfos", "Quality.Part Information", newRecord, controller);
          

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwQAPartInfos")) return;

            TableFields table = tables["vwQAPartInfos"];
            if (! table.IsDirty()) return;


            //custom logic
            if (!(bool)table.Fields["CCCWarning"].newValue)
            {
                table.Fields["CCCDescription"].newValue = null;
                table.Fields["CCCDate"].newValue = null;
            }

            if (!(bool)table.Fields["WashBeforeUseWarning"].newValue) table.Fields["WashBeforeUseDescription"].newValue = null;
            if (!(bool)table.Fields["ShellfishWarning"].newValue) table.Fields["ShellfishDescription"].newValue = null;
        }


   
        

    
    }
}
 