<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="H2MPCHPlanning.ascx.cs" Inherits="YourCompany.Modules.PDM2019.H2MPCHPlanning" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:187px;" class="SubSubHead">Planner UserId:</td>
                <td style="width:187px;" ><asp:Label id="Label22" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPurchasingPartInfos" Field="PlannerUserId" /></td>
                <td style="width:187px;" class="SubSubHead">Buyer UserId:</td>
                <td style="width:189px;" ><asp:Label id="Label15" runat="server" CssClass="Normal placeholder required" Width="167" Table="vwPartInfos" Field="BuyerUserId" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Lead Time:</td>
                <td ><asp:Label id="Label14" runat="server" CssClass="Normal placeholder required" Width="167" Table="vwPartInfos" Field="LeadTime" /></td>
                <td class="SubSubHead">Safety Stock Qty:</td>
                <td ><asp:Label id="Label1" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPartInfos" Field="SafetyStockQty"  Format="Decimal" Places="0"/></td>
            </tr>
            <tr>
                <td class="SubSubHead">Minimum Order Qty:</td>
                <td ><asp:Label id="Label16" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPartInfos" Field="MinimumOrderQty"  Format="Decimal" Places="0" /></td>
                <td class="SubSubHead">Multiple Order Qty:</td>
                <td ><asp:Label id="Label17" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPartInfos" Field="MultipleOrderQty"   Format="Decimal" Places="0"/></td>
            </tr>
            <tr>
                <td class="SubSubHead">Order Policy:</td>
                <td  ><asp:Label id="Label18" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPartInfos" Field="OrderPolicyText"  /></td>
                <td class="SubSubHead">Days of Supply:</td>
                <td style="width:189px;" ><asp:Label id="Label19" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPartInfos" Field="DaysOfSupply"   Format="Decimal" Places="0"/></td>
            </tr>
            <tr>
                <td class="SubSubHead">Order Point:</td>
                <td  ><asp:Label id="Label20" runat="server" CssClass="Normal placeholder" Width="167" Table="vwPartInfos" Field="OrderPoint" style="text-align:right;" Format="Decimal" Places="4" /></td>
                <td />
                <td />
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




