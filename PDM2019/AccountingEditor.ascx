<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountingEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.AccountingEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
    input.required { border: solid 1px orange; }
    select.required { border: solid 1px orange; }                 
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

   
    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }


</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
        <table width="800" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="width:200px;"><asp:Label ID="labelFreightInCost" runat="server" CssClass="SubSubHead" Text="Freight In Cost:" /></td>                        
                <td style="width:200px;"></td>
                <td style="width:200px;" />
                <td style="width:200px;" />
            </tr>

            <tr>
                <td valign="bottom">
                    <asp:TextBox ID="txtFreightInCost" runat="server" Width="180" MaxLength="10" CssClass="Box readonly" Enabled="false" style="text-align:right;" onkeypress="return isDecimal(event);" Table="VMFG.Extensions" Field="FreightInCostText" Placeholder="Freight in Cost"  />
                </td>
                <td />
                <td />
                <td valign="bottom">
                    <asp:CheckBox ID="chkCalculateInboundFreightPDM" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Calculate Inbound Freight" Table="vwAccounting" Field="CalculateInboundFreight" />
                </td>
            </tr>

            <tr>
                <td valign="bottom">
                    <asp:Label id="labelLandedCost" runat="server" CssClass="SubSubHead" Text="Landed Cost:<br />" />
                    <asp:TextBox ID="txtLandedCost" runat="server" Width="180" MaxLength="10"  CssClass="Box readonly" Enabled="false" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="4" Table="VMFG.Extensions" Field="LANDED_COST" Placeholder="Landed Cost" />
                </td>
                <td />
                <td />
                <td valign="bottom">
                    <asp:CheckBox ID="chkTaxExempt" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Tax Exempt" Table="vwMarketingInfos" Field="ExemptFromTax" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="labelDuties" runat="server" CssClass="SubSubHead" Text="Duties:<br />" />
                    <asp:TextBox ID="txtDuties" runat="server" Width="180" MaxLength="10"  CssClass="Box" style="text-align:right;" onkeypress="return isDecimal(event);" Table="vwPartInfos" Field="Duties" Placeholder="Duties" Format="Decimal" Places="4" />
                </td>
                <td />
                <td />
                <td>
                    <asp:Label ID="labelCategory" runat="server" CssClass="SubSubHead" Text="Category:<br />" />
                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="Box" Width="180" Table="vwAccounting" Field="Category" />    
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="labelSellingPrice" runat="server" CssClass="SubSubHead" Text="Selling/Net Price:<br />" />
                    <asp:TextBox ID="txtSellingPrice" runat="server" Width="180" MaxLength="10"  CssClass="Box required" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="2" Table="vwPartInfos" Field="NetPrice" Placeholder="Selling/Net Price" />
                </td>
                <td>
                    <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Selling/Retail Price:<br />" />
                    <asp:TextBox ID="txtRetailPrice" runat="server" Width="180" MaxLength="10"  CssClass="Box required" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Places="2" Table="vwPartInfos" Field="RetailPrice" Placeholder="Selling/Retail Price" />
                </td>
                <td />
                <td>
                    <asp:Label ID="labelQuantityConversion" runat="server" CssClass="SubSubHead" Text="Quantity Conversion:<br />" />
                    <asp:TextBox ID="txtquantityconversion" runat="server" Width="180" MaxLength="10" CssClass="Box required" style="text-align:right;" onkeypress="return isNumberKey(event);" Table="vwPartInfos" Field="ParentPartQuantityConversion" Placeholder="Parent Part Quantity Conversion" />
                </td>
            </tr>
            

            <tr>
                <td valign="bottom">
                    <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Duty Rate:<br />" />
                    <asp:TextBox ID="txtDutyAmount" runat="server" Width="180" MaxLength="10"  CssClass="Box" style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Table="vwPurchasingPartInfos" Field="DutyRate" Placeholder="Duty Rate" />
                </td>
                <td valign="bottom">
                    <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Add/CVD Rate (%):<br />" />
                    <asp:TextBox ID="txtAddRate" runat="server" Width="180" MaxLength="10"  CssClass="Box required"  style="text-align:right;" onkeypress="return isDecimal(event);" Format="Decimal" Table="vwPurchasingPartInfos" Field="AddRateText" Placeholder="Add Rate" Places="4" TargetField="AddRate"/>
                </td>
                <td valign="bottom" >
                    <asp:Label ID="labelFirstYearForecast" runat="server" CssClass="SubSubHead" Text="First Year Forecast:<br />" />
                    <asp:TextBox ID="txtFirstYearForecast" runat="server" CssClass="Box" Width="180" MaxLength="10" Table="vwPartInfos" Field="FirstYearForecast" onkeypress="return isNumberKey(event)" Placeholder="First Year Forecast" Format="Number" />
                </td>
                <td valign="bottom">
                    <asp:CheckBox ID="chkSellable" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Sellable" Table="vwPartInfos" Field="Sellable" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Image ID="imgNeedSomethingToGrabPartsData" runat="server" Visible="false" Table="Parts" Field="WasApproved" />


    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" OnClientClick="javascript:return ValidateAwards();" />
            </td>
        </tr>
    
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
