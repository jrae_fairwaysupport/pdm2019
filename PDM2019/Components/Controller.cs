﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using DotNetNuke;

namespace YourCompany.PDM2019.Components
{
    //public enum PDMTab
    //{
    //    ProductDevelopment = 0,
    //    Purchasing = 1,
    //    QualityAssurance = 2,
    //    Creative = 3,
    //    Sales = 4,
    //    Marketing = 5,
    //    Legal=6,
    //    Accounting = 7,
    //    PDPartInformation = 0,
    //    PDSpecifications = 1,
    //    PDPackaging = 2,
    //    PDVendorPrice = 3,
    //    PDWeightsAndMeasures = 4,
    //    PCHPartInformation = 0,
    //    PCHVendorPrice = 1,
    //    PCHChecklist = 2,
    //    QualityPartInformation = 0,
    //    QualityTestStandards = 1,
    //    BOMMasterPart = 0,
    //    BOMTreeView = 1,
    //    CreativePartInformation = 0,
    //    CreativePackaging = 1
    //}

    public partial class Controller
    {
        protected string entityId { get; set; }
        protected string environment { get; set; }

        public SvcLibrary.DBHandler pdm { get; set; }
        public SvcLibrary.DBHandler vmfg { get; set; }
        public SvcLibrary.DBHandler dcms { get; set; }

        public History history { get; set; }

        public Controller(string EntityId, string Environment) 
        {
             
            entityId = EntityId;
            environment = Environment;

            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            pdm = new SvcLibrary.DBHandler(config, null, environment, entityId + "_PDM");
            string dbName = entityId.Equals("LRH") ? "VMLREI11" : "H2MVMFG";
            vmfg = new SvcLibrary.DBHandler(config, null, environment, dbName);

            dcms = new SvcLibrary.DBHandler(config, null, environment, entityId + "DCMS11");

            history = new History(entityId, environment);

            pdm.ThrowErrors = true;
            vmfg.ThrowErrors = true;
            dcms.ThrowErrors = true;
        }


        public DataSet LoadPart(int partId, object parent, bool formatViews = false)
        {
            List<string> tables = new List<string>();
            ParseTables(parent, tables);
            DataSet ds = LoadPart(partId, tables.ToArray());

            if (!formatViews) return ds;
            foreach (DataTable tbl in ds.Tables)
            {
                if (tbl.TableName.StartsWith("vw")) tbl.TableName = tbl.TableName.Substring(2);
            }
            return ds;
        }


        public DataSet LoadPart(int partId, params string[] tables)
        {

            if (tables.Length == 0) tables = new string[] { "Parts", "vwPartInfos", "vwPartSpecifications", "vwBatteries", "vwPartPackagingComponents", "PartVendorPrices", "vwVendorPriceBreaks", "vwCreativeInfos" };
            

            DataSet ds = new DataSet();
            string tmpl8 = "SELECT * FROM {0} WHERE {1} = @P0";

            foreach (string table in tables)
            {
                bool skip = false;
                string key = (table.Equals("Parts")) ? "Id" : "PartId";
                string sql = string.Format(tmpl8, table, key);

                switch (table)
                {
                    case "vwBatteries":
                        sql += " AND BatteriesFor = 'Part' ORDER BY RecordId";
                        break;
                    case "DCMS":
                        skip = true;
                        break;
                    case "VMFG.Extensions":
                        skip = true;
                        break;
                    default:
                        break;
                }

                if (!skip)
                {
                    DataTable dt = pdm.ExecuteReader(sql, partId);
                    dt.TableName = table;
                    ds.Tables.Add(dt);
                }
            }

            string sku = GetSkuFromPartsTable(ds);

            if (tables.Contains("DCMS"))
            {
                string partTable = entityId.Equals("LRH") ? "VMLREI.LEARNINGRESOURCES.COM" : "h2mvmfg.learningresources.com";
                string qry = "SELECT 1 AS UNIT_QTY, E.LENGTH AS UNIT_LENGTH, E.WIDTH AS UNIT_WIDTH, E.HEIGHT AS UNIT_HEIGHT, S.WEIGHT AS UNIT_WEIGHT, "
                           + "P.CASE_QTY, C.LENGTH AS CASE_LENGTH, C.WIDTH AS CASE_WIDTH, C.HEIGHT AS CASE_HEIGHT, S.CASE_WEIGHT, PLT.PACK_QTY AS PALLET_QTY, "
                           + "PLT.LENGTH AS PALLET_LENGTH, PLT.WIDTH AS PALLET_WIDTH, PLT.HEIGHT AS PALLET_HEIGHT, ROUND(NVL(C.LENGTH, 0) * NVL(C.WIDTH, 0) * NVL(C.HEIGHT, 0) / 1728, 3) AS CPQ, "
                    //+ "CASE WHEN COALESCE(P.USER_4, 'EMPTY') <> 'EMPTY' THEN 1 ELSE 0 END AS INNER_PACK, 'NOT YET IMPLEMENTED' AS INNER_PACK_DIMS "
                           + "CAST(COALESCE(P.USER_4, '1') AS NUMBER) AS INNER_PACK, "
                           + "CASE WHEN COALESCE(P.USER_4, '1') = '1' THEN P.CASE_QTY ELSE  ROUND(P.CASE_QTY / CAST(COALESCE(P.USER_4, '1') AS NUMBER), 2) END AS INNER_PACKS_PER_MASTER, "
                           + "P.USER_10 AS LandedCost "
                           + "FROM SKU S LEFT JOIN Part@" + partTable + " P ON S.ID = P.ID LEFT JOIN SKU_PACK E ON E.SKU_ID = S.ID AND E.PACK_ID = 'EA' "
                           + "LEFT JOIN SKU_PACK C ON C.SKU_ID = S.ID AND C.PACK_ID = 'Cases' LEFT JOIN SKU_PACK PLT ON PLT.SKU_ID = S.ID AND PLT.PACK_ID = 'Pallets' "
                           + "WHERE S.ID = @P0";

                DataTable dcmsData = dcms.ExecuteReader(qry, sku);

                dcmsData.Columns.Add("LandedCostText", System.Type.GetType("System.String"));

                foreach (DataRow dr in dcmsData.Rows)
                {
                    decimal tmp = dcms.GetValue<decimal>(dr, "LandedCost", 0);
                    dr["LandedCostText"] = tmp.ToString("$ #,0.0000");
                }

                dcmsData.TableName = "DCMS";
                ds.Tables.Add(dcmsData);
            }

            if (tables.Contains("VMFG.Extensions"))
            {
                //just worry about LR for now
                if (entityId.Equals("LRH"))
                {
                    string qry = "SELECT * FROM LR_PART_EXTENSION WHERE PART_ID = @P0";
                    DataTable vmfgExtensions = vmfg.ExecuteReader(qry, sku);

                    vmfgExtensions.Columns.Add("FreightInCostText", System.Type.GetType("System.String"));
                    vmfgExtensions.Columns.Add("DutiesText", System.Type.GetType("System.String"));
                    vmfgExtensions.Columns.Add("AddRateText", System.Type.GetType("System.String"));

                    foreach (DataRow dr in vmfgExtensions.Rows)
                    {
                        decimal tmp = vmfg.GetValue<decimal>(dr, "FRT_IN_COST", 0);
                        dr["FreightInCostText"] = tmp.ToString("$ #,0.0000");

                        tmp = vmfg.GetValue<decimal>(dr, "DUTIES", 0);
                        dr["DutiesText"] = tmp.ToString("$ #,0.0000");

                        tmp = vmfg.GetValue<decimal>(dr, "ADD_RATE", 0);
                        dr["AddRateText"] = (tmp/100).ToString("% #,0.0000");

                    }

                    vmfgExtensions.TableName = "VMFG.Extensions";
                    ds.Tables.Add(vmfgExtensions);
                }
            }

            return ds;


        }

        private string GetSkuFromPartsTable(DataSet ds)
        {
            string sku = "jtrtmp";
            DataTable parts = ds.Tables["Parts"];
            if (parts != null)
            {
                if (parts.Rows.Count > 0) sku = pdm.GetValue<string>(parts.Rows[0], "PartId", sku);
            }
            return sku;
        }

        public DataSet OpenPartEditor(int partId)
        {
            DataSet ds = LoadPart(partId);

            //string sql = "SELECT * FROM vwHistory WHERE PartId =@P0 ORDER BY EventTime DESC";
            //DataTable history = pdm.ExecuteReader(sql, partId);
            //history.TableName = "vwHistory";
            //ds.Tables.Add(history);

            //needed for sidebar
            if (!ds.Tables.Contains("Parts"))
            {
                string sql = "SELECT * FROM Parts WHERE Id = @P0";
                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = "Parts";
                ds.Tables.Add(dt);
            }
            return ds;
        }

        protected string VisualBool(DataSet ds, string tableName, string columnName)
        {
            DataTable dt = ds.Tables[tableName];
            if (dt == null) return "N";
            if (dt.Rows.Count == 0) return "N";
            return VisualBool(dt.Rows[0], columnName);
        }

        protected string VisualBool(DataRow dr, string columnName)
        {
            if (pdm.GetValue<bool>(dr, columnName, false)) return "Y";
            return "N";
        }

        public string GetBestRole(DotNetNuke.Entities.Users.UserInfo info, bool fullName)
        {
            //List<string> PDMLRHRoles = new List<string>() { "Administrators", "PDM LRH Product Development", "PDM LRH Purchasing", "PDM LRH Quality Assurance", "PDM LRH Creative", "PDM LRH Sales", "PDM LRH Marketing", "PDM LRH Accounting" }; //, "PDM LRH Legal"
            //List<string> PDMH2MRoles = new List<string>() { "Administrators", "PDM H2M Product Development", "PDM H2M Purchasing", "PDM H2M Quality Assurance", "PDM H2M Creative", "PDM H2M Sales", "PDM H2M Marketing" };

            List<string> roles = new List<string>() { "Product Development", "Purchasing", "Quality Assurance", "Creative", "Sales", "Marketing", "Accounting" };


            if (info.IsSuperUser) return fullName ? "Administrators" : "ad";
            if (info.IsInRole("Administrators")) return fullName ? "Administrators" : "ad";

            string retval = "Unknown";
            string fullRoleName = "Unknown";

            foreach (string role in roles)
            {
                string test = string.Format("PDM {0} {1}", entityId, role);
                if (info.IsInRole(test))
                {
                    retval = role;
                    fullRoleName = test;
                    break;
                }
            }

            if (retval.Equals("Product Development")) return fullName ? fullRoleName : "pd";

            retval = retval.ToLower().Substring(0, 1);
            return fullName ? fullRoleName : retval;
        }
    }

 
}