using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public class BOM : Controller 
    {
        History history { get; set; }

        public BOM(string EntityId, string Environment) : base(EntityId, Environment) 
        {
            history = new History(EntityId, Environment);
        }

        public DataSet LoadPart(int partId, params string[] tables)
        {
            if (tables.Length == 0) tables = new string[] { "vwBillOfMaterialsInfos", "vwChildParts" };
            DataSet ds = new DataSet();

            string tmpl8 = "SELECT * FROM {0} WHERE PartId = @P0";
            foreach (string table in tables)
            {
                string sql = string.Format(tmpl8, table);
                if (table.Equals("Parts")) sql = "SELECT * FROM Parts WHERE Id = @P0";

                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = table;
                ds.Tables.Add(dt);

            }
            

            return ds;
        }

        //public void SaveMaster(string userName, int partId, object parent)
        //{
        //    DataSet ds = LoadPart(partId, "vwBillOfMaterialsInfos");
        //    DataTable dt = ds.Tables["vwBillOfMaterialsInfos"];
        //    bool newRecord = dt.Rows.Count == 0;

        //    Dictionary<string, TableFields> tables = ParseForm(parent);
        //    TableFields table = tables["vwBillOfMaterialsInfos"];

        //    if (!table.IsDirty()) return;
        //    table.AddField("PartId", null, partId);
        //    table.AddField("ModifiedDate", null, DateTime.Now);

        //    if (newRecord)
        //    {
        //        history.Log(partId, userName, "Bill of Materials", string.Empty, string.Empty, "Added Bill of Materials Data");
        //        table.AddField("CreatedDate", null, DateTime.Now);
        //        pdm.InsertRecord("BillOfMaterialsInfos", "PartId", table.GetParametersAsArray(true));

        //    }
        //    else
        //    {
        //        history.Log(partId, userName, "Bill of Materials", table.Fields, "ModifiedDate", "PartId");
        //        pdm.UpdateRecord("BillOfMaterialsInfos", "PartId", partId, table.GetParametersAsArray(true, "PartId"));
        //    }
        //}

        //public void ChildPartSave(string userName, int partId, int childPartRecordId, int childPartUid, string childPartId, string childPartDescription, decimal quantity, decimal originalQuantity)
        //{
        //    string fieldName = "Child Part: " + childPartId;

        //    if (childPartRecordId == -1)
        //    {
        //        history.Log(partId, userName, "Bill of Materials", fieldName, string.Empty, "Added to BOM<br />Quantity: " + quantity.ToString());
        //        pdm.InsertRecord("ChildParts", "id", "Part_Id", partId, "ChildPartUid", childPartUid, "Quantity", quantity, "CreatedDate", DateTime.Now, "ModifiedDate", DateTime.Now);
        //    }
        //    else
        //    {
        //        history.Log(partId, userName, "Bill of Materials", fieldName, originalQuantity.ToString(), quantity.ToString());
        //        pdm.UpdateRecord("ChildParts", "id", childPartRecordId, "Quantity", quantity, "ModifiedDate", DateTime.Now);
        //    }
        //}

        //public void ChildPartDelete(string userName, int partId, int childPartRecordId, string childPartId)
        //{
        //    string fieldName = "Child Part: " + childPartId;

        //    history.Log(partId, userName, "Bill of Materials", fieldName, string.Empty, "Deleted Child");
        //    string sql = "DELETE FROM ChildParts WHERE Part_Id = @P0 AND Id = @P1";
        //    pdm.ExecuteCommand(sql, partId, childPartRecordId);
        //}
    }
}
