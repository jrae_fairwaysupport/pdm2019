﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotNetNuke.Services.Scheduling;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public class Exchange : SchedulerClient
    {
        private SvcLibrary.DBHandler db { get; set; }


        public Exchange()
        {
            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            db = new SvcLibrary.DBHandler(config, null, "Production", "DNN");
        }

        public Exchange(ScheduleHistoryItem historyItem) : base()
        {
            this.ScheduleHistoryItem = historyItem;
        }

        public override void  DoWork()
        {
 	        this.Progressing();

            this.ScheduleHistoryItem.AddLogNote("Where are the notes?");

            // do stuff
            try
            {
                SvcLibrary.Configuration config = new SvcLibrary.Configuration();
                db = new SvcLibrary.DBHandler(config, null, "Production", "DNN");
                db.ThrowErrors = true;

                string sql = "SELECT * FROM PDM_EXCHANGE WHERE Processed = 0"; //null hasn't been launched yet, may still be configuring
                DataTable dt = db.ExecuteReader(sql);
                ProcessData(dt);

                this.ScheduleHistoryItem.AddLogNote("dnn: " + db.ConnectionString);
                //throw new Exception("forced: " + db.ConnectionString);                

                this.ScheduleHistoryItem.Succeeded = true;
            }
            catch (Exception ex)
            {
                this.Errored(ref ex);
                this.ScheduleHistoryItem.Succeeded = false;
            }

            this.Completed();
        }

        private void ProcessData(DataTable dt)
        {
            this.ScheduleHistoryItem.AddLogNote("Rows found: " + dt.Rows.Count.ToString());

            if (dt.Rows.Count == 0) return;

            foreach (DataRow dr in dt.Rows)
            {
                string batchId = db.GetValue<string>(dr, "BatchId", string.Empty);
                string entityId = db.GetValue<string>(dr, "EntityId", string.Empty);
                string partId = db.GetValue<string>(dr, "PartId", string.Empty);
                string dbName = db.GetValue<string>(dr, "DatabaseName", string.Empty);
                string tableName = db.GetValue<string>(dr, "TableName", string.Empty);
         
                List<object>fields = GetBatchFields(batchId);
                Controller controller =new Controller(entityId, "Production"); 

                //rather than try to be everything to everyone, be specific, and add support as needed
                bool result = false;
                int records = 0;

                string sql = string.Empty;
                switch (tableName)
                {
                    case "SKU":
                        records = controller.dcms.UpdateRecord("SKU", "ID", partId, fields);
                        break;
                        //add support for others...we had more, but lost due to virus and ended up handling differently since.
                }

                result = records > 0;

                this.ScheduleHistoryItem.AddLogNote("Processed Batch: " + batchId);
                db.UpdateRecord("PDM_EXCHANGE", "BatchId", batchId, "LastCheck", DateTime.Now, "Processed", result);
                
            }
        }


        private List<object> GetBatchFields(string batchId)
        {
            string sql = "SELECT * FROM PDM_ExchangeFields WHERE BatchId = @P0";
            DataTable dt = db.ExecuteReader(sql, batchId);
            List<object>parms = new List<object>();

            foreach (DataRow dr in dt.Rows)
            {
                string fieldName = db.GetValue<string>(dr, "FieldName", string.Empty);
                string fieldValue = db.GetValue<string>(dr, "FieldValue", string.Empty);
                string fieldType = db.GetValue<string>(dr, "FieldType", "STRING");
                object value = null;

                switch(fieldType)
                {
                    case "DATE":
                        DateTime? val = null;
                        DateTime test;
                        if (DateTime.TryParse(fieldValue, out test)) val = test;
                        value = val;
                        break;
                    default:
                        value = fieldValue;
                        break;

                }

                parms.Add(fieldName);
                parms.Add(value);

            }

            return parms;
        }
    
        public string CreateBatch(string entityId, string partId, int recordId, string databaseName, string tableName)
        {

            string batchId = Guid.NewGuid().ToString();
            db.InsertRecord("PDM_EXCHANGE", "BatchId", "BatchId", batchId, "CreatedDate", DateTime.Now, "EntityId", entityId, "PartId", partId, "RecordId", recordId,
                                "DataBaseName", databaseName, "TableName", tableName);
            return batchId;
        }

        //CURRENTLY ONLY DATE HAS BEEN ID'ED
        public void AddBatchFields(string batchId, string fieldName, object fieldValue, string fieldType = null, bool launch = false)
        {
            db.InsertRecord("PDM_EXCHANGEFields", "BatchId", "BatchId", batchId, "FieldName", fieldName, "FieldValue", fieldValue, "FieldType", fieldType);
            if (launch) LaunchBatch(batchId);
        }

        public void LaunchBatch(string batchId)
        {
            db.UpdateRecord("PDM_EXCHANGE", "BatchId", batchId, "Processed", false);
        }
    }
}



