﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace YourCompany.PDM2019.Components
{
    public partial class Controller
    {
        public void Battery_Add(string userId, int partId, string batteryFor, string size, int qty, bool included)
        {
            int recordId = pdm.InsertRecord("Batteries", "RecordId", "PartId", partId, "BatteriesFor", batteryFor, "BatterySize", size, "BatteryQuantity", qty, "BatteryIncluded", included);

            string msg = string.Format("Added {0} Battery: {0}, {1}, {2}", size, qty, (included ? "Included" : "Not Included"), batteryFor);

            History history = new History(entityId, environment);
            history.Log(partId, userId, "Part Specifications", "Batteries", string.Empty, msg);
        }

        public void Battery_Delete(string userId, int recordId, int partId, string batteryFor, string size, int qty, bool included)
        {
            //our workaround until we can clean up the db design
            if (recordId == -1)
            {
                //we're in the specification database
                string sql = "UPDATE PartSpecifications SET BatterySize = NULL, BatteryQuantity = NULL, BatteriesIncluded = NULL WHERE PartId = @P0";
                pdm.ExecuteCommand(sql, partId);
            }
            else
            {
                string sql = "DELETE FROM Batteries WHERE RecordId = @P0";
                pdm.ExecuteCommand(sql, recordId);
            }

            string msg = string.Format("Deleted {0} Battery: {1}, {2}, {3}", batteryFor, size, qty, (included ? "Included" : "Not Included"));
            History history = new History(entityId, environment);
            history.Log(partId, userId, "Part Specifications", "Batteries", string.Empty, msg);


        }

        public void Battery_SaveRemote(string userName, int partId, TableFields table)
        {
            table.RenameField("RemoteBatteryQuantity", "BatteryQuantity");
            table.RenameField("RemoteBatterySize", "BatterySize");
            table.RenameField("RemoteBatteryIncluded", "BatteryIncluded");

            Dictionary<string, FieldChange> fields = table.Fields;
            if (!fields.ContainsKey("IncludesRemote")) return; //Not called from a valid path

            History history = new History(entityId, environment);

            //remote battery
            bool oldValue = false;
            bool.TryParse(fields["IncludesRemote"].oldHistory, out oldValue); //unset or false
            bool newValue = (bool)fields["IncludesRemote"].newValue;
            Dictionary<string, FieldChange> batteries = table.GetSubFields("BatteryQuantity", "BatterySize", "BatteryIncluded");
            if (newValue)
            {
                string newHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].newHistory, batteries["BatteryQuantity"].newHistory, (bool)batteries["BatteryIncluded"].newValue ? "Included" : "Not Included");

                if (oldValue)
                {
                    string sql = "UPDATE Batteries SET BatteryQuantity = @P0, BatterySize = @P1, BatteryIncluded = @P2 WHERE PartId = @P3 AND BatteriesFor = 'Remote'";
                    pdm.ExecuteCommand(sql, batteries["BatteryQuantity"].newValue, batteries["BatterySize"].newValue, batteries["BatteryIncluded"].newValue, partId);
                    string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
                    history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", oldHistory, newHistory);
                }
                else
                {
                    pdm.InsertRecord("Batteries", "RecordId", "PartId", partId, "BatteryQuantity", batteries["BatteryQuantity"].newValue, "BatterySize", batteries["BatterySize"].newValue,
                                            "BatteryIncluded", batteries["BatteryIncluded"].newValue, "BatteriesFor", "Remote");
                    history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Created: " + newHistory);
                }
            }
            else
            {
                if (oldValue)
                {
                    string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
                    history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Deleted: " + oldHistory);

                    string sql = "DELETE FROM Batteries WHERE PartId = @P0 AND BatteriesFor = 'Remote'";
                    pdm.ExecuteCommand(sql, partId);
                }
            }


        }






        public DataTable PackagingComponent_Load(int recordId)
        {
            string sql = "SELECT * FROM vwPartPackagingComponents WHERE Id = @P0";
            DataTable dt = pdm.ExecuteReader(sql, recordId);
            dt.TableName = "vwPartPackagingComponents";
            return dt;
        }

        public void PackagingComponent_Save(string userName, int recordId, int partId, object parent)
        {
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);

            string description = (string)tables["vwPartPackagingComponents"].Fields["Description"].newValue;
            tables["vwPartPackagingComponents"].AddField("Part_Id", null, partId);
            tables["vwPartPackagingComponents"].AddField("ModifiedDate", null, DateTime.Now);

            History history = new History(entityId, environment);
            if (recordId == -1)
            {
                tables["vwPartPackagingComponents"].AddField("CreatedDate", null, DateTime.Now);
                history.Log(partId, userName, "Product Development.Specifications", "Packaging Component", string.Empty, "Created Component - " + description);
                pdm.InsertRecord("PartPackagingComponents", "Id", tables["vwPartPackagingComponents"].GetParametersAsArray(true));
            }
            else
            {
                history.Log(partId, userName, "Packaging Component - " + description, tables["vwPartPackagingComponents"].Fields, "Part_Id", "ModifiedDate", "CreatedDate");
                pdm.UpdateRecord("PartPackagingComponents", "Id", recordId, tables["vwPartPackagingComponents"].GetParametersAsArray(true, "Part_Id"));
            }
        }

        public void PackagingComponent_Delete(string userName, int recordId, int partId, string description)
        {
            History history = new History(entityId, environment);
            history.Log(partId, userName, "Packaging Component", description, string.Empty, "Deleted Component");
            string sql = "DELETE FROM PartPackagingComponents WHERE id = @P0";
            pdm.ExecuteCommand(sql, recordId);

        }

        public DataTable VendorPriceBreak_Load(int recordId)
        {
            string sql = "SELECT * FROM vwVendorPriceBreaks WHERE Id = @P0";
            DataTable dt = pdm.ExecuteReader(sql, recordId);
            dt.TableName = "vwVendorPriceBreaks";
            return dt;
        }

        public void VendorPriceBreak_Save(string userName, int recordId, int partId, object parent)
        {
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);
            TableFields table = tables["vwVendorPriceBreaks"];

            VendorPriceBreak_Save(userName, recordId, partId, table);
        }

        private void VendorPriceBreak_Save(string userName, int recordId, int partId, TableFields table)
        {
            Dictionary<string, FieldChange> fields = table.Fields;

            StringBuilder log = new StringBuilder();
            log.AppendFormat("{0}<br />Vendor: {1}<br />Vendor Part Id: {2}<br />", (recordId == -1 ? "Created Price Break" : "Updated Price Break"), fields["VendorId"].newHistory, fields["VendorPartId"].newHistory);
            for (int index = 1; index <= 5; index++)
            {
                log.AppendFormat("Break {0}: {1} - {2}<br />", index, fields["Quantity" + index.ToString()].newHistory, fields["Price" + index.ToString()].newHistory);
            }
            History history = new History(entityId, environment);
            history.Log(partId, userName, "Product Development.Vendor Price", "Price Breaks", string.Empty, log.ToString());

            table.AddField("PartVendorPrice_PartId", null, partId);
            table.AddField("ModifiedDate", null, DateTime.Now);

            if (recordId == -1)
            {
                table.AddField("CreatedDate", null, DateTime.Now);
                pdm.InsertRecord("VendorPriceBreaks", "Id", table.GetParametersAsArray(true));
            }
            else
            {
                pdm.UpdateRecord("VendorPriceBreaks", "Id", recordId, table.GetParametersAsArray(true));
            }
        }

        public void VendorPriceBreak_Delete(string userName, int recordId, int partId)
        {
            DataSet ds = LoadPart(partId, "vwVendorPriceBreaks");
            DataTable dt = ds.Tables["vwVendorPriceBreaks"];
            DataRow[] arr = dt.Select("Id = " + recordId.ToString());
            if (arr.Length == 0) return;

            DataRow dr = arr[0];
            StringBuilder log = new StringBuilder();
            log.AppendFormat("Deleted Price Break<br />Vendor Id: {0}<br />Vendor Part Id: {1}<br />", pdm.GetValue<string>(dr, "VendorId", string.Empty), pdm.GetValue<string>(dr, "VendorPartId", string.Empty));
            for (int index = 1; index <= 5; index++)
            {
                log.AppendFormat("Break {0}: {1} - {2}<br />", index, pdm.GetValue<string>(dr, "Quantity" + index.ToString(), ""), pdm.GetValue<string>(dr, "Price" + index.ToString() + "Text", ""));
            }

            History history = new History(entityId, environment);
            history.Log(partId, userName, "Product Development.Vendor Price", "Price Breaks", string.Empty, log.ToString());

            string sql = "DELETE FROM VendorPriceBreaks WHERE Id = @P0";
            pdm.ExecuteCommand(sql, recordId);
        }


        public void Specifications_Save(int partId, string sku, string generalDescription)
        {
            StringBuilder specs = new StringBuilder();
            string sql = "SELECT * FROM PartPackagingComponents WHERE PART_ID = @P0 ORDER BY ID";
            DataTable dt = pdm.ExecuteReader(sql, partId);

            foreach (DataRow dr in dt.Rows)
            {
                int Quantity = pdm.GetValue<int>(dr, "Quantity", 0);
                string Description = pdm.GetValue<string>(dr, "Description", string.Empty);
                string Size = pdm.GetValue<string>(dr, "Size", string.Empty);
                string Materials = pdm.GetValue<string>(dr, "Materials", string.Empty);
                decimal Length = pdm.GetValue<decimal>(dr, "Length", 0);
                decimal Width = pdm.GetValue<decimal>(dr, "Width", 0);
                decimal Height = pdm.GetValue<decimal>(dr, "Height", 0);

                if (string.IsNullOrEmpty(Size))
                {
                    if (Length > 0 || Width > 0 || Height > 0)
                    {
                        Size = string.Format("{0}\" x {1}\" x {2}\"", Length.ToString("#.0000"), Width.ToString("#.0000"), Height.ToString("#.0000"));
                    }
                }

                specs.AppendFormat("Quantity {0}, Description {1}, Size {2}, Materials {3}", Quantity, Description, Size, Materials);
                specs.AppendLine();
            }
            specs.AppendLine(generalDescription);
            string fullSpecs = specs.ToString();

            byte[] bits = System.Text.Encoding.Default.GetBytes(fullSpecs);
            int bitLength = bits.Length + 1;


            sql = "SELECT * FROM PART_BINARY WHERE PART_ID = @P0";
            dt = vmfg.ExecuteReader(sql, sku);

            if (dt.Rows.Count > 0)
            {
                sql = "UPDATE PART_BINARY SET BITS = @P0, BITS_LENGTH = @P2 WHERE PART_ID = @P1";
                vmfg.ExecuteCommand(sql, bits, sku, bitLength);
            }
            else
            {
                sql = "INSERT INTO PART_BINARY (PART_ID, TYPE, BITS, BITS_LENGTH) VALUES (@P0, 'D', @P1, @P2)";
                vmfg.ExecuteCommand(sql, sku, bits, bitLength);

            }


        }
        

        private void Warehouse_Check(string partId, string warehouseId)
        {
            //part_warehouse
            string sql = "SELECT * FROM PART_WAREHOUSE WHERE PART_ID = @P0 AND WAREHOUSE_ID = @P1";
            DataTable dt = vmfg.ExecuteReader(sql, partId, warehouseId);

            if (dt.Rows.Count == 0)
            {
                vmfg.InsertRecord("PART_WAREHOUSE", "PART_ID", "PART_ID", partId, "WAREHOUSE_ID", warehouseId, "AUTO_CREATE", "N", "MIN_INV_QTY", 0, "COMMITTED_CO", 0,
                                    "COMMITTED_REQ", 0, "COMMITTED_PLREQ", 0, "COMMITTED_IBT", 0, "AVAILABLE_QTY", 0, "EXPECTED_PO", 0, "EXPECTED_WO", 0,
                                    "EXPECTED_PL", 0, "EXPECTED_IBT", 0, "EXPECT_COMMIT_CO", 0, "EXPECT_COMMIT_REQ", 0, "EXPECT_COMMIT_PLR", 0, "EXPECT_COMMIT_IBT", 0,
                                    "UNALLOC_DEMAND_QTY", 0, "OUTBOUND_QTY", 0, "ON_HOLD_QTY", 0, "LOCKED_QTY", 0, "UNAVAILABLE_QTY", 0, "PLANNING_LEADTIME", 0,
                                    "ORDER_POLICY", " ");
            }

        }

        private void Location_Check(DataTable dt, string partId, string warehouseId, string locationId, string status, string locked, string transit, string defBackflushLoc, string autoIssueLoc,
                                        string defInspectLoc, string dcClassId)
        {
            string filter = string.Format("WAREHOUSE_ID = '{0}' AND LOCATION_ID = '{1}'", warehouseId, locationId);
            DataRow[] arr = dt.Select(filter);
            if (arr.Length > 0) return;

            vmfg.InsertRecord("PART_LOCATION", "PART_ID", "PART_ID", partId, "WAREHOUSE_ID", warehouseId, "LOCATION_ID", locationId, "QTY", 0, "STATUS", status, "LOCKED", locked,
                                "TRANSIT", transit, "COMMITTED_QTY", 0, "DEF_BACKFLUSH_LOC", defBackflushLoc, "AUTO_ISSUE_LOC", autoIssueLoc, "DEF_INSPECT_LOC", defInspectLoc,
                                "DC_CLASS_ID", dcClassId);
        }

        public void PrimaryWarehouse_Save(string partId, string primaryWarehouse)
        {
            string sql = "UPDATE PART SET PRIMARY_WHS_ID = @P0 WHERE ID = @P1";
            vmfg.ExecuteCommand(sql, primaryWarehouse, partId);

            Warehouse_Check(partId, "P");            
            

            //check all the locations
            sql = "SELECT * FROM PART_LOCATION WHERE PART_ID = @P0";
            DataTable dt = vmfg.ExecuteReader(sql, partId);

            //P Location
            Location_Check(dt, partId, "P", "P", "A", "N", "N", "Y", "Y", "Y", "A");
            Location_Check(dt, partId, "P", "P-COMMITTED", "A", "N", "N", "N", "N", "N", "C");
            Location_Check(dt, partId, "P", "P-HOLD", "H", "N", "N", "N", "N", "N", "H");
            Location_Check(dt, partId, "P", "P-INPROCESS", "U", "N", "N", "N", "N", "N", "P");
            Location_Check(dt, partId, "P", "P-INSPECT", "U", "N", "N", "N", "N", "N", "Q");
            Location_Check(dt, partId, "P", "P-RETURN", "U", "N", "N", "N", "N", "N", "R");
            Location_Check(dt, partId, "P", "P-SCRAP", "U", "N", "N", "N", "N", "N", "S");
            Location_Check(dt, partId, "P", "P-UNAVAIL", "U", "N", "N", "N", "N", "N", "D");

            //in cases where PRIMARY_WHS_ID is not P, it looks like it's been set up like a regular foreign warehouse.
            if (!primaryWarehouse.Equals("P")) ForeignWarehouse_Save(partId, primaryWarehouse);
        }

        public void ForeignWarehouse_Save(string partId, string foreignWarehouse)
        {
            Warehouse_Check(partId, foreignWarehouse);
            
            string sql = "SELECT * FROM PART_LOCATION WHERE PART_ID = @P0";
            DataTable dt = vmfg.ExecuteReader(sql, partId);

            Location_Check(dt, partId, foreignWarehouse, foreignWarehouse, "H", "N", "N", "N", "N", "N", null);
            Location_Check(dt, partId, foreignWarehouse, "TRANSIT-" + foreignWarehouse, "A", "N", "Y", "N", "N", "N", null);
        }
    }

 
}