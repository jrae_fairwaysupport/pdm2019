﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YourCompany.PDM2019.Components
{
    public class FieldChange
    {
        public object oldValue { get; set; }
        public object newValue { get; set; }
        public string tabName { get; set; }

        public FieldChange(object oldVal, object newVal)
        {
            oldValue = oldVal;
            newValue = newVal;
        }

        public string oldHistory
        {
            get
            {
                if (oldValue == null) return "unset";
                return oldValue.ToString();
            }
        }

        public string newHistory
        {
            get
            {
                if (newValue == null) return "unset";
                return newValue.ToString();
            }
        }

        public bool HasChanged(bool useBaseValues = false)
        {
            if (useBaseValues) return (oldValue != newValue);
            return !oldHistory.Equals(newHistory);
        }
    }

    public class TableFields
    {
        Dictionary<string, FieldChange> fields = new Dictionary<string, FieldChange>();

        public void AddField(string fieldName, FieldChange edits)
        {
            fields.Add(fieldName, edits);
        }

        public void AddField(string fieldName, object oldValue, object newValue)
        {
            FieldChange newbie = new FieldChange(oldValue, newValue);
            fields.Add(fieldName, newbie);
        }

        public void AddField(string fieldName, object oldValue, object newValue, string tabName)
        {
            FieldChange newbie = new FieldChange(oldValue, newValue);
            newbie.tabName = tabName;
            fields.Add(fieldName, newbie);
        }

        public void RemoveField(string fieldName)
        {
            if (fields.ContainsKey(fieldName)) fields.Remove(fieldName);
        }

        public bool ContainsField(string fieldName)
        {
            return fields.ContainsKey(fieldName);
        }

        public object GetOldValue(string fieldName)
        {
            try { return Fields[fieldName].oldValue; }
            catch { return null; }
        }

        public object GetNewValue(string fieldName)
        {
            try { return Fields[fieldName].newValue; }
            catch { return null; }
        }

        public Dictionary<string, FieldChange> Fields 
        { 
            get { return fields; }
            set { fields = value; }
        }

        public Dictionary<string, FieldChange> Edits
        {
            get
            {
                Dictionary<string, FieldChange> retval = new Dictionary<string, FieldChange>();

                foreach (string key in fields.Keys)
                {
  
                    if (fields[key].oldHistory != fields[key].newHistory) retval.Add(key, fields[key]);
                }
                return retval;
            }
        }

        public bool IsDirty(params string[] omits)
        {
            List<string> excludes = new List<string>();
            foreach (string omit in omits) excludes.Add(omit);

            foreach (string key in fields.Keys)
            {
                if (!excludes.Contains(key))
                {
                    if (fields[key].HasChanged()) return true;
                }
            }
            return false;
        }

        public bool HasFieldChanged(string key, bool useBaseValues = false)
        {
            if (!fields.ContainsKey(key)) return false;
            return fields[key].HasChanged(useBaseValues);
        }

        public void RenameField(string originalName, string newName)
        {
            if (!fields.ContainsKey(originalName)) return;
            fields.Add(newName, new FieldChange(fields[originalName].oldValue, fields[originalName].newValue));
            fields.Remove(originalName);
        }

        public object[] GetParametersAsArray(bool changesOnly, params string[] omits)
        {
            return GetParameters(changesOnly, omits).ToArray();
        }

        public object[] GetPartialParameterArray(bool changesOnly, params string[] includes)
        {
            return GetPartialParameters(changesOnly, includes).ToArray();
        }

        public Dictionary<string, FieldChange> GetSubFields(params string[] includes)
        {
            Dictionary<string, FieldChange> retval = new Dictionary<string, FieldChange>();
            foreach (string include in includes)
            {
                if (fields.ContainsKey(include))
                    retval.Add(include, new FieldChange(fields[include].oldValue, fields[include].newValue));
                else
                    retval.Add(include, new FieldChange(null, null));
            }
            return retval;
        }

        public List<object> GetPartialParameters(bool changesOnly, params string[] includes)
        {
            List<string> includedFields = new List<string>();
            foreach(string included in includes) includedFields.Add(included);

            Dictionary<string, FieldChange> useFields = changesOnly ? Edits : fields;

            List<object> parms = new List<object>();
            foreach (string key in useFields.Keys)
            {
                if (includedFields.Contains(key))
                {
                    parms.Add(key);
                    parms.Add(useFields[key].newValue);
                }
            }
            return parms;
        }

        public List<object> GetParameters(bool changesOnly, params string[] omits)
        {
            List<string> excludes = new List<string>();
            foreach (string omit in omits) excludes.Add(omit);

            Dictionary<string, FieldChange> useFields = changesOnly ? Edits : fields;

            List<object> parms = new List<object>();
            foreach (string key in useFields.Keys)
            {
                if (!excludes.Contains(key))
                {
                    parms.Add(key);
                    parms.Add(useFields[key].newValue);
                }
            }
            return parms;
        }

        //THIS SHOULD BE IN SVCLIBRARY
        public string PrepareUpdateQuery (string tableName, bool changesOnly, ref List<object> queryParameters, params string[] omits)
        {
            queryParameters = new List<object>();
            string sql = "UPDATE " + tableName + " SET ";
            string fieldList = string.Empty;

            Dictionary<string, FieldChange> edits = changesOnly ? Edits : Fields;
            foreach(string key in edits.Keys)
            {
                if (!omits.Contains(key))
                {
                    if (!string.IsNullOrEmpty(fieldList)) fieldList += ", ";
                    fieldList += string.Format("{0} = @P{1}", key, queryParameters.Count);
                    queryParameters.Add(edits[key].newValue);
                }
            }

            //add WHERE so code doesn't accidentally attempt to update a whole table
            sql += fieldList;
            sql += " WHERE ";
            return sql;
        }

        public string FinalizeUpdateQuery(string sql, ref List<object> queryParameters, params string[] whereClauses)
        {
            string where = string.Empty;
            foreach (string key in Fields.Keys)
            {
                if (whereClauses.Contains(key))
                {
                    if (!string.IsNullOrEmpty(where)) where += " AND ";
                    where += string.Format("{0} = @P{1}", key, queryParameters.Count);
                    queryParameters.Add(Fields[key].newValue);
                }
            }
            sql += where;
            return sql;
        }

        public void StageOracleEdits(Dictionary<string, object> edits, params string[] fieldNames)
        {
            if (edits == null) edits = new Dictionary<string, object>();

            foreach (string key in fieldNames)
            {
                string[] arr = key.Split('.');
                string srcName = key;
                string tgtName = key;

                if (arr.Length > 0)
                {
                    srcName = arr[0].Trim();
                    tgtName = arr[1].Trim();
                }
                    

                if (HasFieldChanged(srcName)) edits.Add(tgtName, GetNewValue(srcName));
            }
        }

        public void PushOracleEdits(SvcLibrary.DBHandler db, Dictionary<string, object> edits, string tableName, string identity, string partId)
        {
            List<object> parms = new List<object>();
            foreach (string key in edits.Keys)
            {
                parms.Add(key);
                parms.Add(edits[key]);
            }

            //see if an update works, if not we'll need to do an insert instead...which may muck up this logic (foreign keys, constraints, etc.)
            int records = db.UpdateRecord(tableName, identity, partId, parms.ToArray());
            if (records == 0)
            {
                parms.Add(identity);
                parms.Add(partId);
                db.InsertRecord(tableName, identity, parms.ToArray());
            }
        }
    }
}