using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public class Creative : PDMController 
    {
        History history { get; set; }

        public Creative(string EntityId, string Environment) : base(EntityId, Environment) 
        {
            history = new History(EntityId, Environment);
        }

        public override DataSet LoadPart(int partId, params string[] tables)
        {
            if (tables.Length == 0) tables = new string[] { "vwCreativeInfos", "vwPartInfos", "Parts", "vwPartSpecifications" };
            DataSet ds = new DataSet();

            string tmpl8 = "SELECT * FROM {0} WHERE PartId = @P0";
            foreach (string table in tables)
            {
                string sql = string.Format(tmpl8, table);
                if (table.Equals("Parts")) sql = "SELECT * FROM Parts WHERE Id = @P0";

                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = table;
                ds.Tables.Add(dt);

            }
            

            return ds;
        }

        public DataTable Load(int partId)
        {
            DataSet ds = LoadPart(partId);
            return ds.Tables[0];
        }

        public void Save(string userName, int partId, object parent)
        {
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);
            SaveCreative(userName, partId, tables);

            Controller pd = new Controller(entityId, environment);
           
            //crap
            //pd.SavePartInfos(userName, partId, tables);
            //pd.SavePartSpecifications(userName, partId, tables);
        }


        
        
        public void SaveCreative(string userName, int partId, Dictionary<string, TableFields> tables)
        {

            bool newPart = (partId == -1);
            if (!newPart)
            {
                DataSet ds = LoadPart(partId, "vwCreativeInfos");
                newPart = (ds.Tables["vwCreativeInfos"].Rows.Count == 0);
            }

            TableFields table = tables["vwCreativeInfos"];
            if (!table.IsDirty()) return;

            table.AddField("PartId", new FieldChange(null, partId));
            table.AddField("ModifiedDate", new FieldChange(null, DateTime.Now));

            if (newPart)
            {
                history.Log(partId, userName, "Creative", string.Empty, string.Empty, "Added Creative Data");
                table.AddField("CreatedDate", null, DateTime.Now);
                pdm.InsertRecord("CreativeInfos", "PartId", table.GetParametersAsArray(true));
            }
            else
            {
                history.Log(partId, userName, "Creative", table.Fields, "ModifiedDate", "CreatedDate", "PartId");
                pdm.UpdateRecord("CreativeInfos", "PartId", partId, table.GetParametersAsArray(true, "PartId"));
            }


            //table = tables["vwPartInfos"];
            //if (table.IsDirty())
            //{
            //    history.Log(partId, userName, "Product Development.Part Information", table.Fields);
            //    table.AddField("ModifiedDate", null, DateTime.Now);
            //    table.AddField("PartId", null, partId);
            //    pdm.UpdateRecord("PartInfos", "PartId", partId, table.GetParametersAsArray(true, "PartId"));
            //}


            //table = tables["vwPartSpecifications"];
            //if (table.IsDirty())
            //{
            //    history.Log(partId, userName, "Creative", table.Fields);
            //    table.AddField("ModifiedDate", null, DateTime.Now);
            //    table.AddField("PartId", null, partId);
            //    pdm.UpdateRecord("PartSpecifications", "PartId", partId, table.GetParametersAsArray(true, "PartId"));
            //}
        }
        
    }
}
