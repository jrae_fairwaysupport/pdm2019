using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public class Quality : PDMController 
    {
        History history { get; set; }

        public Quality(string EntityId, string Environment) : base(EntityId, Environment) 
        {
            history = new History(EntityId, Environment);
        }

        public override DataSet LoadPart(int partId, params string[] tables)
        {
            if (tables.Length == 0) tables = new string[] { "vwPartSpecifications", "vwQAPartInfos", "vwBatteries", "vwTestStandards" };
            DataSet ds = new DataSet();

            string tmpl8 = "SELECT * FROM {0} WHERE PartId = @P0";
            foreach (string table in tables)
            {
                string sql = string.Format(tmpl8, table);
                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = table;
                ds.Tables.Add(dt);
            }
            return ds;
        }

        //public void SavePartInformation(string userName, int partId, object parent)
        //{
        //    DataSet ds = LoadPart(partId, "vwQAPartInfos", "vwPartSpecifications");
        //    bool newRecord = ds.Tables["vwQAPartInfos"].Rows.Count == 0;
        //    bool newSpecification = ds.Tables["vwPartSpecifications"].Rows.Count == 0;

        //    Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
        //    ParseForm(parent, tables);

        //    TableFields table = tables["vwQAPartInfos"];
        //    table.AddField("PartId", null, partId);
        //    table.AddField("ModifiedDate", null, DateTime.Now);
        //    Dictionary<string, FieldChange> fields = table.Fields;

        //    //custom logic
        //    if (!(bool)fields["CCCWarning"].newValue)
        //    {
        //        fields["CCCDescription"].newValue = null;
        //        fields["CCCDate"].newValue = null;
        //    }

        //    if (!(bool)fields["WashBeforeUseWarning"].newValue) fields["WashBeforeUseDescription"].newValue = null;
        //    if (!(bool)fields["ShellfishWarning"].newValue) fields["ShellfishDescription"].newValue = null;

            

        //    if (newRecord)
        //    {
        //        history.Log(partId, userName, "Quality.Part Information", string.Empty, string.Empty, "Created Quality.Part Information record.");
        //        table.AddField("CreatedDate", null, DateTime.Now);
        //        pdm.InsertRecord("QAPartInfos", "PartId", table.GetParametersAsArray(true));
        //    }
        //    else
        //    {
        //        if (table.IsDirty("PartId", "ModifiedDate"))
        //        {
        //            history.Log(partId, userName, "Quality.Part Information", table.Fields, "ModifiedDate", "CreatedDate", "PartId");
        //            pdm.UpdateRecord("QAPartInfos", "PartId", partId, table.GetParametersAsArray(true, "PartId"));
        //        }
        //    }

        //    table = tables["vwPartSpecifications"];
        //    if (table.IsDirty())
        //    {
        //        table.AddField("PartId", null, partId);
        //        table.AddField("ModifiedDate", null, DateTime.Now);
                
        //        if (newSpecification)
        //        {
        //            history.Log(partId, userName, "Product Development.Specifications", string.Empty, string.Empty, "Created Product Development.Specifications record.");
        //            table.AddField("CreatedDate", null, DateTime.Now);
        //            pdm.InsertRecord("PartSpecifications", "PartId", tables["vwPartSpecifications"].GetParametersAsArray(true));
        //        }
        //        else
        //        {
        //            history.Log(partId, userName, "Product Development.Specifications", tables["vwPartSpecifications"].Fields, "ModifiedDate", "CreatedDate", "PartId");
        //            pdm.UpdateRecord("PartSpecifications", "PartId", partId, tables["vwPartSpecifications"].GetParametersAsArray(true, "PartId"));
        //        }
        //    }
        //}

        public void SaveTestStandards(string userName, int partId, object parent)
        {
            DataSet ds = LoadPart(partId, "vwTestStandards");
            bool newRecord = ds.Tables["vwTestStandards"].Rows.Count == 0;
            bool newSpecification = ds.Tables["vwTestStandards"].Rows.Count == 0;

            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);

            TableFields table = tables["vwTestStandards"];
            table.AddField("QAPartInfo_PartId", null, partId);
            table.AddField("ModifiedDate", null, DateTime.Now);
            Dictionary<string, FieldChange> fields = table.Fields;

            //custom logic



            if (newRecord)
            {
                history.Log(partId, userName, "Quality.Test Standards", string.Empty, string.Empty, "Created Quality.Test Standards record.");
                table.AddField("CreatedDate", null, DateTime.Now);
                pdm.InsertRecord("TestStandards", "QAPartInfo_PartId", table.GetParametersAsArray(true));
            }
            else
            {
                if (table.IsDirty("QAPartInfo_PartId", "ModifiedDate"))
                {
                    history.Log(partId, userName, "Quality.Test Standards", table.Fields, "ModifiedDate", "CreatedDate", "QAPartInfo_PartId");
                    pdm.UpdateRecord("TestStandards", "QAPartInfo_PartId", partId, table.GetParametersAsArray(true, "QAPartInfo_PartId"));
                }
            }

        }


        
    }
}
