using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public partial class PDM2019Controller 
    {
        public void ProductDevelopment_Packaging_Save(string userName, int partId, object parent)
        {
            //this will never be a new record as Part must exist
             

            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);
            
            TableFields table = tables["Parts"];
            Dictionary<string, FieldChange> fields = table.Fields;
            

            //custom logic

            fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));

            if (table.IsDirty("ModifiedDate"))
            {
                HistoryLog(partId, userName, "Product Development.Packaging", fields, "ModifiedDate");
                pdm.UpdateRecord("Parts", "Id", partId, table.GetParametersAsArray(true));
            }


        }

    }
}
