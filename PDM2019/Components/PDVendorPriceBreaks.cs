using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public partial class PDM2019Controller 
    {
        

        public DataTable VendorPriceBreak_Load(int recordId)
        {
            string sql = "SELECT * FROM vwVendorPriceBreaks WHERE Id = @P0";
            DataTable dt = pdm.ExecuteReader(sql, recordId);
            dt.TableName = "vwVendorPriceBreaks";
            return dt;
        }

        public void VendorPriceBreak_Save(string userName, int recordId, int partId, object parent)
        {
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);
            TableFields table = tables["vwVendorPriceBreaks"];

            VendorPriceBreak_Save(userName, recordId, partId, table);
        }

        private void VendorPriceBreak_Save(string userName, int recordId, int partId, TableFields table)
        {
            Dictionary<string, FieldChange> fields = table.Fields;

            StringBuilder log = new StringBuilder();
            log.AppendFormat("{0}<br />Vendor: {1}<br />Vendor Part Id: {2}<br />", (recordId == -1 ? "Created Price Break" : "Updated Price Break"), fields["VendorId"].newHistory, fields["VendorPartId"].newHistory);
            for (int index = 1; index <= 5; index++)
            {
                log.AppendFormat("Break {0}: {1} - {2}<br />", index, fields["Quantity" + index.ToString()].newHistory, fields["Price" + index.ToString()].newHistory);
            }
            HistoryLog(partId, userName, "Product Development.Vendor Price", "Price Breaks", string.Empty, log.ToString());

            table.AddField("PartVendorPrice_PartId", null, partId);
            table.AddField("ModifiedDate", null, DateTime.Now);

            if (recordId == -1)
            {
                table.AddField("CreatedDate", null, DateTime.Now);
                pdm.InsertRecord("VendorPriceBreaks", "Id", table.GetParametersAsArray(true));
            }
            else
            {
                pdm.UpdateRecord("VendorPriceBreaks", "Id", recordId, table.GetParametersAsArray(true));
            }
        }

        public void VendorPriceBreak_Delete(string userName, int recordId, int partId)
        {
            DataSet ds = LoadPart(partId, "vwVendorPriceBreaks");
            DataTable dt = ds.Tables["vwVendorPriceBreaks"];
            DataRow[] arr = dt.Select("Id = " + recordId.ToString());
            if (arr.Length == 0) return;
            
            DataRow dr = arr[0];
            StringBuilder log = new StringBuilder();
            log.AppendFormat("Deleted Price Break<br />Vendor Id: {0}<br />Vendor Part Id: {1}<br />", pdm.GetValue<string>(dr, "VendorId", string.Empty), pdm.GetValue<string>(dr, "VendorPartId", string.Empty));
            for (int index = 1; index <= 5; index++)
            {
                log.AppendFormat("Break {0}: {1} - {2}<br />", index, pdm.GetValue<string>(dr, "Quantity" + index.ToString(), ""), pdm.GetValue<string>(dr, "Price" + index.ToString() + "Text", ""));
            }
            HistoryLog(partId, userName, "Product Development.Vendor Price", "Price Breaks", string.Empty, log.ToString());

            string sql = "DELETE FROM VendorPriceBreaks WHERE Id = @P0";
            pdm.ExecuteCommand(sql, recordId);
        }




        public void ProductDevelopment_VendorPrice_Save(string userName, int partId, object parent)
        {
            string tabName = "Production Development.Vendor Price";

            DataSet ds = LoadPart(partId, "PartVendorPrices");
            bool newRecord = ds.Tables["PartVendorPrices"].Rows.Count == 0;


            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);

            TableFields table = tables["PartVendorPrices"];
            Dictionary<string, FieldChange> fields = table.Fields;


            //custom logic
            fields.Add("PartId", new FieldChange(null, partId));
            fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));

            if (newRecord)
            {
                fields.Add("CreatedDate", new FieldChange(null, DateTime.Now));
                HistoryLog(partId, userName, tabName, string.Empty, string.Empty, "Created Vendor Price");
                pdm.InsertRecord("PartVendorPrices", "PartId", table.GetParametersAsArray(true));
            }
            else
            {
                if (table.IsDirty("ModifiedDate"))
                {
                    HistoryLog(partId, userName, tabName, table.Fields, "ModifiedDate", "CreatedDate", "PartId");
                    pdm.UpdateRecord("PartVendorPrices", "PartId", partId, table.GetParametersAsArray(true));
                }
            }


            //additional changes
            
        }

    }
}
