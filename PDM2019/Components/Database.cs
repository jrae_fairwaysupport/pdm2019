using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.UI.WebControls;


namespace YourCompany.PDM2019.Components
{
    public abstract partial class PDMController
    {

        public T GetValue<T>(DataRow dr, string fieldName, T defaultValue = default(T))
        {
            return pdm.GetValue<T>(dr, fieldName, defaultValue);
        }

        public DataTable Search(string sql, int pageNumber, ref int pageCount, ref int recordCount, int pageSize, bool visual = false)
        {
            DataTable parts;

            if (visual)
                parts = vmfg.SelectPage(ref pageCount, ref recordCount, pageNumber, pageSize, sql);
            else
                parts = pdm.SelectPage(ref pageCount, ref recordCount, pageNumber, pageSize, sql);



            return parts;
        }

        public int FetchRecordCount(string sql, bool visual = false)
        {
            DataTable dt;
            
            if (visual)
                dt = vmfg.ExecuteReader(sql);
            else
                dt = pdm.ExecuteReader(sql);
            return dt.Rows.Count;
        }

        public DataTable ExecuteReader(string sql, params object[] parameters)
        {
            return pdm.ExecuteReader(sql, parameters);
        }


        public DataTable ExecuteVisualReader(string sql, params object[] parameters)
        {
            return vmfg.ExecuteReader(sql, parameters);
        }
   }
}