using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public partial class PDM2019Controller 
    {
        public DataSet ProductDevelopment_PartInformation_Load(int partId)
        {
            DataSet ds = LoadPart(partId, "Parts", "vwPartInfos" );
            return ds;
        }

        public int ProductDevelopment_PartInformation_Save(string userName, int partId, object parent)
        {
            bool newPart = (partId == -1);
            bool newPartInfos = true;

            if (!newPart)
            {
                DataSet ds = ProductDevelopment_PartInformation_Load(partId);
                newPartInfos = ds.Tables["vwPartInfos"].Rows.Count == 0;
            }

            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);

            tables["Parts"].AddField("Id", partId, partId);


            //custom logic
            //moldsharevendor - if removed, make vendorId unset
            Dictionary<string, FieldChange>fields = tables["vwPartInfos"].Fields;
            if ((bool)fields["MoldShare"].newValue == false) fields["MoldShareVendorId"].newValue = null;

            //royalty take 2
            //create placeholders
            fields.Add("RoyaltyPercentage", new FieldChange(null, fields["RoyaltyInitial"].newValue));
            fields.Add("RoyaltyAmount", new FieldChange(null, fields["RoyaltyInitial"].newValue));

            //assign initial values
            string originalType = (string)fields["RoyaltyType"].oldValue;
            if (!string.IsNullOrEmpty(originalType))
            {
                if (originalType.Equals("PERCENTAGE")) fields["RoyaltyPercentage"].oldValue = fields["RoyaltyInitial"].oldValue;
                else fields["RoyaltyAmount"].oldValue = fields["RoyaltyInitial"].oldValue;
            }

            //get rid of values we don't need
            string newType = (string)fields["RoyaltyType"].newValue;
            if (!string.IsNullOrEmpty(newType))
            {
                if (newType.Equals("PERCENTAGE")) fields["RoyaltyAmount"].newValue = null;
                else fields["RoyaltyPercentage"].newValue = null;
            }

            //see if we're even a royalty item anymore
            if (!(bool)fields["RoyaltyItem"].newValue)
            {
                fields["RoyaltyPercentage"].newValue = null;
                fields["RoyaltyAmount"].newValue = null;
                fields["RoyaltyPayableTo"].newValue = null;
            }

            //get rid of helpers
            fields.Remove("RoyaltyType");
            fields.Remove("RoyaltyInitial");

            //PreferredVendor
            if (string.IsNullOrEmpty(((string)fields["PreferredVendor"].newValue)))
            {
                fields["BuyerUserId"].newValue = null;
            }
            else
            {
                if (fields["PreferredVendor"].HasChanged() || string.IsNullOrEmpty((string)fields["BuyerUserId"].newValue))
                {
                    string sql = "SELECT BUYER FROM vwVendors WHERE ID = @P0";
                    fields["BuyerUserId"].newValue = pdm.FetchSingleValue<string>(sql, "BUYER", (string)fields["PreferredVendor"].newValue);
                }

            }
            if (fields["PreferredVendor"].HasChanged() || fields["BuyerUserId"].HasChanged())
            {
                tables.Add("PurchasingPartInfos", new TableFields());
                tables["PurchasingPartInfos"].AddField("PreferredVendorId", fields["PreferredVendor"].oldValue, fields["PreferredVendor"].newValue);
                tables["PurchasingPartInfos"].AddField("BuyerUserId", fields["BuyerUserId"].oldValue, fields["BuyerUserId"].newValue);
            }
            fields.Remove("PlannerUserId");

            
            //create the header record
            if (newPart)
            {
                tables["Parts"].AddField("CreatedDate", null, DateTime.Now);
                tables["Parts"].AddField("ModifiedDate", null, DateTime.Now);
                tables["Parts"].AddField("Status", null, "Draft");
                partId = pdm.InsertRecord("Parts", "Id", tables["Parts"].GetParametersAsArray(true));
                HistoryLog(partId, userName, "Header", string.Empty, string.Empty, "Created Part");

            }

            fields.Add("PartId", new FieldChange(null, partId));
            fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));
            if (newPartInfos)
            {
                fields.Add("CreatedDate", new FieldChange(null, DateTime.Now));
                HistoryLog(partId, userName, "Product Development.Part Information", string.Empty, string.Empty, "Added Part Information");
                pdm.InsertRecord("PartInfos", "PartId", tables["vwPartInfos"].GetParametersAsArray(true));

            }
            else
            {
                if (tables["vwPartInfos"].IsDirty())
                {
                    HistoryLog(partId, userName, "Product Development.Part Information", tables["vwPartInfos"].Fields, "ModifiedDate", "CreatedDate", "PartId");
                    pdm.UpdateRecord("PartInfos", "PartId", partId, tables["vwPartInfos"].GetParametersAsArray(true, "PartId"));
                }
            }

            if (tables.ContainsKey("PurchasingPartInfos"))
            {
                string sql = "SELECT * FROM PurchasingPartInfos WHERE PartId = @P0";
                DataTable dt = pdm.ExecuteReader(sql, partId);

                tables["PurchasingPartInfos"].AddField("ModifiedDate", null, DateTime.Now);

                if (dt.Rows.Count == 0)
                {
                    tables["PurchasingPartInfos"].AddField("CreatedDate", null, DateTime.Now);
                    tables["PurchasingPartInfos"].AddField("PartId", null, partId);
                    HistoryLog(partId, userName, "Purchasing.Part Information", string.Empty, string.Empty, "Created Purchasing Record");
                    pdm.InsertRecord("PurchasingPartInfos", "PartId", tables["PurchasingPartInfos"].GetParametersAsArray(true));
                }
                else
                {
                    HistoryLog(partId, userName, "Purchasing.Part Information", tables["PurchasingPartInfos"].Fields, "PartId", "CreatedDate", "ModifiedDate");
                    pdm.UpdateRecord("PurchasingPartInfos", "PartId", partId, tables["PurchasingPartInfos"].GetParametersAsArray(true));
                }
            }

           


            return partId;
        }

            }
}
