﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;

namespace YourCompany.PDM2019.Components
{
 
    public class EmailController
    {
        string entityId { get; set; }
        string environment { get; set; }
        

        public EmailController(string EntityId, string Environment)
        {
            entityId = EntityId;
            environment = Environment;
        }

        private Dictionary<string, string> FetchPartData(int partId, string urlTemplate)
        {
            Dictionary<string, string> retval = new Dictionary<string,string>();
            retval.Add("Sku", string.Empty);
            retval.Add("Description", string.Empty);
            retval.Add("UserId", string.Empty);
            retval.Add("Reason", string.Empty);
            retval.Add("Vendor", string.Empty);
            retval.Add("Url", urlTemplate.Replace("[E]", entityId).Replace("[ENV]", environment).Replace("[ID]", partId.ToString()).Replace("[EID]", entityId));

            //http://lrcentral.learningresources.com/DotNetNuke/PDMPartEditor[E]/e/[ENV]/id/[ID]/r/[R]/eid/[E]

            Controller partController = new Controller(entityId, environment);
            DataSet ds;
            if (entityId.Equals("H2M"))
                ds = partController.LoadPart(partId, "Parts", "vwPartInfos");
            else
                ds = partController.LoadPart(partId, "Parts", "vwPartInfos", "vwApprovals");

            DataTable parts = ds.Tables["Parts"];
            if (parts != null)
            {
                if (parts.Rows.Count > 0)
                {
                    DataRow dr = parts.Rows[0];
                    retval["Sku"] = partController.GetValue<string>(dr, "PartId", string.Empty);
                    retval["Description"] = partController.GetValue<string>(dr, "Description", string.Empty);
                    retval["UserId"] = partController.GetValue<string>(dr, "RejectUserId", "Unknown User");
                    retval["Reason"] = partController.GetValue<string>(dr, "RejectionReason", "Unknown Reason");
                }
            }

            DataTable infos = ds.Tables["vwPartInfos"];
            if (infos != null)
            {
                if (infos.Rows.Count > 0)
                {
                    DataRow dr = infos.Rows[0];
                    retval["Vendor"] = partController.GetValue<string>(dr, "PreferredVendorSuggest", "Unknown Vendor");
                }
            }

            return retval;
        }

        public void SendApproval(int partId, string urlTmplate, int portalId, string roles)
        {
            Dictionary<string, string> fields = FetchPartData(partId, urlTmplate);
            Dictionary<string, List<string>> emails = FetchEmailRecipients(partId, roles);

            StringBuilder msg = new StringBuilder();
            msg.AppendFormat("<p>Part #{0} - {1}</p>", fields["Sku"], fields["Description"]);
            msg.AppendLine("<p>This part has been approved in the Part Database Management App.</p>");
            msg.AppendFormat("<p>To view this part, visit <a href=\"{0}\" target=\"_blank\">LR Central</a></p>", fields["Url"]);

            string subject = string.Format("{0} PDM: Part #{1} Approved", entityId, fields["Sku"]);

            foreach (string key in emails.Keys)
            {
                string message = msg.ToString().Replace("[R]", key);
                SvcLibrary.Configuration config = new SvcLibrary.Configuration();
                SvcLibrary.EmailMessage email = new SvcLibrary.EmailMessage(config);

                email.SendEmail("", subject, message, emails[key].ToArray());
            }
        }

        public void SendRejection(int partId, string urlTmplate, int portalId, string roles)
        {
            Dictionary<string, string> fields = FetchPartData(partId, urlTmplate);
            Dictionary<string, List<string>> emails = FetchEmailRecipients(portalId, roles);

            StringBuilder msg = new StringBuilder();
            msg.AppendFormat("<p>Part #{0} - {1}</p>", fields["Sku"], fields["Description"]);
            msg.AppendLine("<p>This part has had a revision requested prior to approval.</p>");
            msg.AppendFormat("<p>The reason given: <br />");
            msg.AppendFormat("{0}<br />-{1}</p>", fields["Reason"], fields["UserId"]);
            msg.AppendFormat("<p>To view this part, visit <a href=\"{0}\" target=\"_blank\">LR Central</a>.</p>", fields["Url"]);

            string subject = string.Format("{0} PDM: Part #{1} Revision Requested", entityId, fields["Sku"]);

            foreach (string key in emails.Keys)
            {
                string message = msg.ToString().Replace("[R]", key);
                SvcLibrary.Configuration config = new SvcLibrary.Configuration();
                SvcLibrary.EmailMessage email = new SvcLibrary.EmailMessage(config);
                email.SendEmail("", subject, message, emails[key].ToArray());
            }
        }


        public void SendSubmit(int partId, string urlTmpl8, int portalId, string submitter, string roles)
        {
            Dictionary<string, string> fields = FetchPartData(partId, urlTmpl8);
            Dictionary<string, List<string>> emails = FetchEmailRecipients(portalId, roles);

            StringBuilder msg = new StringBuilder();
            msg.AppendFormat("<p>Part: #{0} - {1}<br />Vendor: {2}</p>", fields["Sku"], fields["Description"], fields["Vendor"]);
            msg.AppendFormat("<p>This part has been submitted for approval by {0} on {1}.</p>", submitter, DateTime.Today.ToString("MM/dd/yyyy"));
            msg.AppendFormat("<p>To view this part, visit <a href=\"{0}\" target=\"_blank\">LR Central</a>.</p>", fields["Url"]);

            string subject = string.Format("{0} PDM: Part #{1} Submitted for Approval", entityId, fields["Sku"]);

            foreach (string key in emails.Keys)
            {
                string message = msg.ToString().Replace("[R]", key);
                SvcLibrary.Configuration config = new SvcLibrary.Configuration();
                SvcLibrary.EmailMessage email = new SvcLibrary.EmailMessage(config);
                email.SendEmail("", subject, message, emails[key].ToArray());
            }
        }

        public void SendSubmit(int partId, string urlTmpl8, int portalId, string roles)
        {
            Dictionary<string, string> fields = FetchPartData(partId, urlTmpl8);
            Dictionary<string, List<string>> emails = FetchEmailRecipients(portalId, roles);

            StringBuilder msg = new StringBuilder();
            msg.AppendFormat("<p>Part: #{0} - {1}<br />Vendor: {2}</p>", fields["Sku"], fields["Description"], fields["Vendor"]);
            msg.AppendLine("<p>This part has been submitted for approval in the Part Database Management App.</p>");
            msg.AppendFormat("<p>To view this part, visit <a href=\"{0}\" target=\"_blank\">LR Central</a>.</p>", fields["Url"]);

            string subject = string.Format("{0} PDM: Part #{1} Submitted for Approval", entityId, fields["Sku"]);

            foreach (string key in emails.Keys)
            {
                string message = msg.ToString().Replace("[R]", key);
                SvcLibrary.Configuration config = new SvcLibrary.Configuration();
                SvcLibrary.EmailMessage email = new SvcLibrary.EmailMessage(config);
                email.SendEmail("", subject, message, emails[key].ToArray());
            }
        }

        //public void SendSubmit(int partId, string urlTmpl8, int portalId, string roles)
        //{
        //    Dictionary<string, string> fields = FetchPartData(partId, urlTmpl8);

        //    StringBuilder msg = new StringBuilder();
        //    msg.AppendFormat("<p>Part: #{0} - {1}<br />Vendor: {2}</p>", fields["Sku"], fields["Description"], fields["Vendor"]);
        //    msg.AppendLine("<p>This part has been submitted for approval in the Part Database Management App.</p>");
        //    msg.AppendFormat("<p>To view this part, visit <a href=\"{0}\" target=\"_blank\">LR Central</a>.</p>", fields["Url"]);

        //    string subject = string.Format("{0} PDM: Part #{1} Submitted for Approval", entityId, fields["Sku"]);

        //    List<string> emails = FetchEmailRecipients(portalId, roles);
        //    SvcLibrary.Configuration config = new SvcLibrary.Configuration();
        //    SvcLibrary.EmailMessage email = new SvcLibrary.EmailMessage(config);
        //    email.SendEmail("", subject, msg.ToString(), emails.ToArray());
        //}
        //public void SendRejection(int partId, string urlTmplate, int portalId, string roles)
        //{
        //    Dictionary<string, string> fields = FetchPartData(partId, urlTmplate);

        //    StringBuilder msg = new StringBuilder();
        //    msg.AppendFormat("<p>Part #{0} - {1}</p>", fields["Sku"], fields["Description"]);
        //    msg.AppendLine("<p>This part has had a revision requested prior to approval.</p>");
        //    msg.AppendFormat("<p>The reason given: <br />");
        //    msg.AppendFormat("{0}<br />-{1}</p>", fields["Reason"], fields["UserId"]);
        //    msg.AppendFormat("<p>To view this part, visit <a href=\"{0}\" target=\"_blank\">LR Central</a>.</p>", fields["Url"]);

        //    string subject = string.Format("{0} PDM: Part #{1} Revision Requested", entityId, fields["Sku"]);

        //    List<string> emails = FetchEmailRecipients(portalId, roles);
        //    SvcLibrary.Configuration config = new SvcLibrary.Configuration();
        //    SvcLibrary.EmailMessage email = new SvcLibrary.EmailMessage(config);
        //    email.SendEmail("", subject, msg.ToString(), emails.ToArray());
        //}
        //public void SendApproval(int partId, string urlTmplate, int portalId, string roles)
        //{
        //    Dictionary<string, string> fields = FetchPartData(partId, urlTmplate);

        //    StringBuilder msg = new StringBuilder();
        //    msg.AppendFormat("<p>Part #{0} - {1}</p>", fields["Sku"], fields["Description"]);
        //    msg.AppendLine("<p>This part has been approved in the Part Database Management App.</p>");
        //    msg.AppendFormat("<p>To view this part, visit <a href=\"{0}\" target=\"_blank\">LR Central</a></p>", fields["Url"]);

        //    string subject = string.Format("{0} PDM: Part #{1} Approved", entityId, fields["Sku"]);

        //    List<string> emails = FetchEmailRecipients(portalId, roles);
        //    SvcLibrary.Configuration config = new SvcLibrary.Configuration();
        //    SvcLibrary.EmailMessage email = new SvcLibrary.EmailMessage(config);

        //    email.SendEmail("", subject, msg.ToString(), emails.ToArray());
        //}
        
        private Dictionary<string, List<string>> FetchEmailRecipients(int portalId, string roles)
        {
            DotNetNuke.Security.Roles.RoleController controller = new DotNetNuke.Security.Roles.RoleController();
            string[] arr = roles.Split(',');

            Dictionary<string, List<string>> retval = new Dictionary<string, List<string>>();

            //add ad for defense, if empty remove before returning
            retval.Add("ad", new List<string>());

            foreach (string roleName in arr)
            {
                string key = string.Empty;
                if (roleName.Equals("Administrators")) key = "ad";
                if (roleName.Contains("Product Development")) key = "pd";
                if (string.IsNullOrEmpty(key)) key = roleName.Replace("PDM", "").Replace(entityId, "").Trim().ToLower().Substring(0, 1);

                //add this role to the return value -- remove if no entries before return
                if (!retval.ContainsKey(key)) retval.Add(key, new List<string>());

                List<DotNetNuke.Entities.Users.UserInfo> users = (List<DotNetNuke.Entities.Users.UserInfo>)controller.GetUsersByRole(portalId, roleName.Trim());
                foreach (DotNetNuke.Entities.Users.UserInfo usr in users)
                {
                    string email = usr.Email;

                    if (usr.IsSuperUser || usr.IsInRole("Administrators"))
                    {
                        if (!retval["ad"].Contains(email)) retval["ad"].Add(email);
                    }

                    //ensure we add only once to avoid duplicate emails with different role
                    bool add = true;
                    foreach (string roleKey in retval.Keys)
                    {
                        if (retval[roleKey].Contains(email))
                        {
                            add = false;
                            break;
                        }
                    }

                    if (add) retval[key].Add(email);
                }
            }

            //remove empty sets
            //jic
            List<string> roleKeys = retval.Keys.ToList();
            foreach(string roleKey in roleKeys)
            {

                if (retval[roleKey].Count == 0) retval.Remove(roleKey);
            }

            return retval;
        }

        // private List<string> FetchEmailRecipients(int portalId, string roles)
        //{
        //    DotNetNuke.Security.Roles.RoleController controller = new DotNetNuke.Security.Roles.RoleController();

        //    List<string> emails = new List<string>();

        //    string[] arr = roles.Split(',');

        //    foreach (string roleName in arr)
        //    {
        //        List<DotNetNuke.Entities.Users.UserInfo> users = (List<DotNetNuke.Entities.Users.UserInfo>)controller.GetUsersByRole(portalId, roleName.Trim());
        //        foreach (DotNetNuke.Entities.Users.UserInfo usr in users)
        //        {
        //            string email = usr.Email;
        //            if (!emails.Contains(email)) emails.Add(email);
        //        }
        //    }

        //    return emails;
        //}

        private List<string> FetchLRRecipients(int portalId, string role, Dictionary<string, string> approvals = null, string approvalKey = "")
        {
            List<string> retval = new List<string>();

            if (approvals != null)
            {
                if (!string.IsNullOrEmpty(approvals[approvalKey]))
                {
                    retval.Add(approvals[approvalKey]);
                    return retval;
                }
            }

            DotNetNuke.Security.Roles.RoleController controller = new DotNetNuke.Security.Roles.RoleController();
            List<DotNetNuke.Entities.Users.UserInfo> users = (List<DotNetNuke.Entities.Users.UserInfo>)controller.GetUsersByRole(portalId, role.Trim());
            foreach (DotNetNuke.Entities.Users.UserInfo usr in users)
            {
                if (!retval.Contains(usr.Email)) retval.Add(usr.Email);
            }
            return retval;
        }

        private string SendLRNotification_GetUrl(Components.Controller controller, DataSet ds, string urlTemplate, int partId, ref string description, ref string vendor, ref string sku, ref Dictionary<string, string> approvals)
        {
            //string company = string.Empty;
            vendor = string.Empty;
            DataTable dt = ds.Tables["vwPartInfos"];
            if (dt.Rows.Count > 0)
            {
                //company = controller.GetValue<string>(dt.Rows[0], "Company", string.Empty);
                vendor = controller.GetValue<string>(dt.Rows[0], "PreferredVendorSuggest", string.Empty);
            }

            sku = string.Empty;
            description = string.Empty;
            dt = ds.Tables["Parts"];
            if (dt.Rows.Count > 0)
            {
                sku = controller.GetValue<string>(dt.Rows[0], "PartId", string.Empty);
                description = controller.GetValue<string>(dt.Rows[0], "Description", string.Empty);
            }

            //try this as a dictionary
            approvals = new Dictionary<string, string>();
            dt = ds.Tables["vwApprovals"];
            foreach (DataColumn col in dt.Columns)
            {
                string value = controller.GetValue<string>(dt.Rows[0], col.ColumnName, string.Empty);
                approvals.Add(col.ColumnName, value);
            }

            string url = urlTemplate.Replace("[E]", entityId).Replace("[ENV]", environment).Replace("[ID]", partId.ToString()).Replace("[EID]", entityId);
            url += "/r/[R]";

            return url;
        }

        public void SendLRNotification(Components.Controller controller, int partId, DataSet ds, string urlTemplate, int portalId, DotNetNuke.Entities.Users.UserInfo user, string currentStatus, string targetStatus)
        {
            string description = string.Empty;
            string vendor = string.Empty;
            string sku = string.Empty;
            Dictionary<string, string> approvals = new Dictionary<string, string>();

            string url = SendLRNotification_GetUrl(controller, ds, urlTemplate, partId, ref description, ref vendor, ref sku, ref approvals);

            StringBuilder body = new StringBuilder();
            string rowTemplate = @"<tr><td align=""left"" valign=""top"">{0}:</td><td align=""left"" valign=""top"">{1}</td></tr>";
            body.AppendLine(@"<table cellspacing=""0"" cellpadding=""4"">");
            body.AppendFormat(rowTemplate, "Part Id", string.Format(@"<a href=""{0}"" target=""_blank"">{1}</a>", url, sku));
            body.AppendFormat(rowTemplate, "Description", description);
            body.AppendFormat(rowTemplate, "Vendor", vendor);

            string subject = string.Empty;
            List<string> recipients = new List<string>();
            bool accountingEmailNeeded = false;
            bool finalApproval = false;
            string fromRole = string.Empty;
            string toRole = string.Empty;

            switch (targetStatus)
            {
                case "Pending":
                    switch (currentStatus)
                    {
                        case "Rejected":
                            subject = "New Part Re-Submitted";
                            recipients = FetchLRRecipients(portalId, "PDM LRH Purchasing", approvals, "RejectedByEmail");

                            body.AppendFormat(rowTemplate, "Submitted By", user.Email);
                            body.AppendFormat(rowTemplate, "Date Submitted", DateTime.Today.ToString("MM/dd/yyyy"));
                            fromRole = "pd";
                            toRole = "p";
                            break;
                        case "Draft":
                            subject = "New Part Submitted";
                            recipients = FetchLRRecipients(portalId, "PDM LRH Purchasing");

                            body.AppendFormat(rowTemplate, "Submitted By", user.Email);
                            body.AppendFormat(rowTemplate, "Date Submitted", DateTime.Today.ToString("MM/dd/yyyy"));
                            fromRole = "pd";
                            toRole = "p";
                            break;
                        case "Pending": //pending -> pending = approved by purchasing, still needs accounting input
                            subject = "New Part Approved";
                            recipients = FetchLRRecipients(portalId, "PDM LRH Product Development", approvals, "SubmittedByEmail");

                            body.AppendFormat(rowTemplate, "Approved By", user.Email);
                            body.AppendFormat(rowTemplate, "Date Approved", DateTime.Today.ToString("MM/dd/yyyy"));
                            accountingEmailNeeded = true;
                            fromRole = "p";
                            //toRole = "pd";
                            break;
                    }
                    break;
                case "Rejected":
                    subject = "New Part Rejected";
                    recipients = FetchLRRecipients(portalId, "PDM LRH Product Development", approvals, "SubmittedByEmail");

                    body.AppendFormat(rowTemplate, "Date Rejected", DateTime.Today.ToString("MM/dd/yyyy"));
                    body.AppendFormat(rowTemplate, "Rejected By", user.Email);
                    body.AppendFormat(rowTemplate, "Reason", approvals["RejectionReason"]);
                    fromRole = "p";
                    toRole = "pd";
                    break;
                case "Approved": //accounting did their updates
                    subject = "New Part Forecast and Pricing Complete";
                    recipients = FetchLRRecipients(portalId, "PDM LRH Purchasing", approvals, "PurchasingApprovalByEmail");

                    body.AppendFormat(rowTemplate, "Date Completed", DateTime.Today.ToString("MM/dd/yyyy"));
                    body.AppendFormat(rowTemplate, "Completed By", user.Email);
                    finalApproval = true;
                    fromRole = "a";
                    toRole = "p";
                    break;
            }

            
            body.AppendLine("</table>");

            if (!string.IsNullOrEmpty(toRole)) SendLRNotification(recipients, user.Email, fromRole, subject, body.ToString(), toRole);

            if (accountingEmailNeeded)
            {
                subject = "New Part Ready for Forecast and Pricing";
                recipients = FetchLRRecipients(portalId, "PDM LRH Accounting");
                SendLRNotification(recipients, user.Email, fromRole, subject, body.ToString(), "a");
            }

            //here's the monkeywrench...accounting isn't going to want a shit ton of emails AND I think Chris's workflow is wrong
            if (finalApproval)
            {
                //send to creative, and qa...purchasing gets the first above
                List<string>creative = FetchLRRecipients(portalId, "PDM LRH Creative");
                subject = "New Part Sent to Visual";
                SendLRNotification(creative, "", "", subject, body.ToString(), "c");

                List<string>quality = FetchLRRecipients(portalId, "PDM LRH Quality Assurance");
                recipients = new List<string>();
                foreach (string recipient in quality)
                    if (!creative.Contains(recipient)) recipients.Add(recipient);
                SendLRNotification(recipients, "", "", subject, body.ToString(), "q");
            }

        }

        private void SendLRNotification(List<string> recipients, string fromEmail, string fromRole, string subject, string bodyTemplate, string toRole)
        {
            //send the email to the targets
            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            SvcLibrary.EmailMessage message = new SvcLibrary.EmailMessage(config);

            string body = bodyTemplate.Replace("[R]", toRole);
            message.SendEmail("", subject, body, recipients.ToArray());

            if (recipients.Contains(fromEmail)) return;
            if (string.IsNullOrEmpty(fromEmail)) return;

            body = bodyTemplate.Replace("[R]", fromRole);
            body += "<p>The following recipients have been copied on this notice:<ul>";
            foreach (string recipient in recipients) body += string.Format("<li>{0}</li>", recipient);
            body += "</ul></p>";

            message = new SvcLibrary.EmailMessage(config);
            message.SendEmail("", subject, body, fromEmail);

        }

        public void SendLRNotification_WarningToCreative(Components.Controller controller, int partId, DataSet ds, string urlTemplate, int portalId, DotNetNuke.Entities.Users.UserInfo user, string changeList)
        {
            return; //temp fix

            string description = string.Empty;
            string vendor = string.Empty;
            string sku = string.Empty;
            Dictionary<string, string> approvals = new Dictionary<string, string>();

            string url = SendLRNotification_GetUrl(controller, ds, urlTemplate, partId, ref description, ref vendor, ref sku, ref approvals);

            StringBuilder body = new StringBuilder();
            string rowTemplate = @"<tr><td align=""left"" valign=""top"">{0}:</td><td align=""left"" valign=""top"">{1}</td></tr>";
            body.AppendLine(@"<table cellspacing=""0"" cellpadding=""4"">");
            body.AppendFormat(rowTemplate, "Part Id", string.Format(@"<a href=""{0}"" target=""_blank"">{1}</a>", url, sku));
            body.AppendFormat(rowTemplate, "Description", description);
            body.AppendFormat(rowTemplate, "Vendor", vendor);

            string subject = "New Item Requires Safety Warning Label";
            List<string> recipients = FetchLRRecipients(portalId, "PDM LRH Creative");

            body.AppendFormat(rowTemplate, "Submitted By", user.Email);
            body.AppendFormat(rowTemplate, "Date Submitted", DateTime.Today.ToString("MM/dd/yyyy"));

            body.AppendLine("</table>");

            body.AppendLine(changeList);

            SendLRNotification(recipients, user.Email, "q", subject, body.ToString(), "c");

        }
    }
}