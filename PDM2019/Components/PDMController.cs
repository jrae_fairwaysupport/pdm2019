﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace YourCompany.PDM2019.Components
{
    public enum PDMTab
    {
        ProductDevelopment = 0,
        Purchasing = 1,
        QualityAssurance = 2,
        Creative = 3,
        Sales = 4,
        Marketing = 5,
        Legal=6,
        Accounting = 7,
        PDPartInformation = 0,
        PDSpecifications = 1,
        PDPackaging = 2,
        PDVendorPrice = 3,
        PDWeightsAndMeasures = 4,
        PCHPartInformation = 0,
        PCHVendorPrice = 1,
        PCHChecklist = 2,
        QualityPartInformation = 0,
        QualityTestStandards = 1,
        BOMMasterPart = 0,
        BOMTreeView = 1,
        CreativePartInformation = 0,
        CreativePackaging = 1
    }

    public abstract partial class xPDMController
    {
        protected string entityId { get; set; }
        protected string environment { get; set; }

        protected SvcLibrary.DBHandler pdm { get; set; }
        protected SvcLibrary.DBHandler vmfg { get; set; }
        protected SvcLibrary.DBHandler dcms { get; set; }

        public xPDMController(string EntityId, string Environment) 
        {
            entityId = EntityId;
            environment = Environment;

            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            pdm = new SvcLibrary.DBHandler(config, null, environment, entityId + "_PDM");
            string dbName = entityId.Equals("LRH") ? "VMLREI11" : "H2MVMLREI11";
            vmfg = new SvcLibrary.DBHandler(config, null, environment, dbName);

            dcms = new SvcLibrary.DBHandler(config, null, environment, entityId + "DCMS11");
        }

        public abstract DataSet LoadPart(int partId, params string[] tables);

        public DataSet OpenPartEditor(int partId)
        {
            DataSet ds = LoadPart(partId);

            //string sql = "SELECT * FROM vwHistory WHERE PartId =@P0 ORDER BY EventTime DESC";
            //DataTable history = pdm.ExecuteReader(sql, partId);
            //history.TableName = "vwHistory";
            //ds.Tables.Add(history);

            //needed for sidebar
            if (!ds.Tables.Contains("Parts"))
            {
                string sql = "SELECT * FROM Parts WHERE Id = @P0";
                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = "Parts";
                ds.Tables.Add(dt);
            }
            return ds;
        }

        //public DataSet LoadPart(int partId, DataSet ds)
        //{
        //    string sql = "SELECT * FROM vwHistory WHERE PartId = @P0 ORDER BY EventTime DESC";
        //    DataTable dt = pdm.ExecuteReader(sql, partId);
        //    dt.TableName = "vwHistory";
        //    ds.Tables.Add(dt);
        //    return ds;
        //}

        protected string VisualBool(DataSet ds, string tableName, string columnName)
        {
            DataTable dt = ds.Tables[tableName];
            if (dt == null) return "N";
            if (dt.Rows.Count == 0) return "N";
            return VisualBool(dt.Rows[0], columnName);
        }

        protected string VisualBool(DataRow dr, string columnName)
        {
            if (pdm.GetValue<bool>(dr, columnName, false)) return "Y";
            return "N";
        }
    }

 
}