using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public class SideBarController : Controller
    {
        History history { get; set; }

        public SideBarController(string EntityId, string Environment) : base(EntityId, Environment) 
        {
            history = new History(EntityId, Environment);
        }

        //public DataSet xLoadPart(int partId, params string[] tables)
        //{
        //    //tables isn't relevant for this guy

        //    string sql = "SELECT * FROM Parts WHERE Id = @P0";
        //    DataTable dt = pdm.ExecuteReader(sql, partId);
        //    dt.TableName = "Parts";
        //    DataSet ds = new DataSet();
        //    ds.Tables.Add(dt);
            
        //    return ds;
            
        //}

        //public DataTable xLoad(int partId)
        //{
        //    DataSet ds = LoadPart(partId);
        //    return ds.Tables["Parts"];
        //}

        //public bool IsPartIdUnique(int partId, string id)
        //{
        //    string sql = "SELECT COUNT(*) AS THECOUNT FROM vwPartList WHERE PartId = @P0 AND (id <> @P1 OR @P1 = -1)";
        //    int count = pdm.FetchSingleValue<int>(sql, "THECOUNT", id, partId);
        //    return (count == 0);
        //}

        //public int Save(string userName, int partId, object parent)
        //{
        //    Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
        //    ParseForm(parent, tables);
        //    return SaveDetails(userName, partId, tables);
        //}

        //private int SaveDetails(string userName, int partId, Dictionary<string, TableFields>tables)
        //{
        //    bool newPart = partId == -1;
        //    if (!newPart)
        //    {
        //        DataSet ds = LoadPart(partId);
        //        DataTable dt = ds.Tables["Parts"];
        //        newPart = dt.Rows.Count == 0;
        //    }

        //    TableFields table = tables["Parts"];
        //    table.AddField("ModifiedDate", null, DateTime.Now);

        //    if (newPart)
        //    {
        //        history.Log(partId, userName, "Header", string.Empty, string.Empty, "Created Part");
        //        table.AddField("CreatedDate", null, DateTime.Now);
        //        table.AddField("Status", null, "Draft");
        //        table.AddField("WasApproved", null, false);
        //        table.AddField("IsLockedForRevision", null, false);
        //        table.AddField("IsRevisedPart", null, false);
        //        table.AddField("LoadPartRevisions", null, false);


        //        partId = pdm.InsertRecord("Parts", "Id", table.GetParametersAsArray(false));
        //    }
        //    else
        //    {
        //        if (tables["Parts"].IsDirty("Id"))
        //        {
        //            history.Log(partId, userName, "Header", table.Fields, "ModifiedDate", "CreatedDate", "Id");
        //            pdm.UpdateRecord("Parts", "Id", partId, table.GetParametersAsArray(true, "Id"));
        //        }
        //    }

        //    return partId;


        //}


        //public void xReject(string userName, int partId, object parent)
        //{
        //    Dictionary<string, TableFields> tables = ParseForm(parent);
        //    TableFields table = tables["Parts"];

        //    if (table.IsDirty())
        //    {
        //        history.Log(partId, userName, "Header", table.Fields);
        //        table.AddField("ModifiedDate", null, DateTime.Now);
        //        table.AddField("RejectUserId", null, userName);
        //        pdm.UpdateRecord("Parts", "id", partId, table.GetParametersAsArray(true));
        //    }

        //}

        //public void SubmitPart(string userName, int partId, string status, ref string msg)
        //{
        //    msg = string.Empty;
        //    Controller controller = new Controller(entityId, environment);
        //    DataSet ds = controller.LoadPart(partId, "Parts", "vwPartInfos");

        //    SubmitPart_Validate(ds, "Parts", "PartId", "Part Id", ref msg);
        //    SubmitPart_Validate(ds, "Parts", "UPCCode", "UPC Code", ref msg);
        //    SubmitPart_Validate(ds, "Parts", "Description", "Description", ref msg);
        //    SubmitPart_Validate(ds, "vwPartInfos", "Company", "Company", ref msg);
        //    SubmitPart_Validate(ds, "vwPartInfos", "ParentPartId", "Parent Part Id", ref msg);
        //    SubmitPart_Validate(ds, "vwPartInfos", "PreferredVendor", "Preferred Vendor", ref msg);
        //    SubmitPart_Validate(ds, "vwPartInfos", "MaterialCost", "Material Cost", ref msg);
        //    SubmitPart_Validate(ds, "vwPartInfos", "LeadTime", "Lead Time", ref msg);
        //    SubmitPart_Validate(ds, "vwPartInfos", "MinimumOrderQuantity", "Minimum Order Quantity", ref msg);
        //    SubmitPart_Validate(ds, "vwPartInfos", "YearIntroduced", "Year Introduced", ref msg);
        //    SubmitPart_Validate(ds, "vwPartInfos", "FirstYearForecast", "First Year Forecast", ref msg);
        //    if (!string.IsNullOrEmpty(msg))
        //    {
        //        msg = "The following fields need values before the Part may be submitted:\r\n" + msg;
        //        return;
        //    }

        //    history.Log(partId, userName, "Header", "Status", status, "Pending");
        //    pdm.UpdateRecord("Parts", "id", partId, "Status", "Pending", "SubmitUserId", userName, "ModifiedDate", DateTime.Now);
        //}

        //private void SubmitPart_Validate(DataSet ds, string table, string column, string field, ref string msg)
        //{
        //    bool error = false;

        //    DataTable dt = ds.Tables[table];
        //    if (dt == null)
        //    {
        //        error = true;
        //    }
        //    else 
        //    {
        //        if (dt.Rows.Count == 0) error = true;
        //    }

        //    if (!error)
        //    {
        //        DataRow dr = dt.Rows[0];
        //        if (string.IsNullOrEmpty(pdm.GetValue<string>(dr, column, string.Empty))) error = true;
        //    }

        //    if (error)
        //    {
        //        if (!string.IsNullOrEmpty(msg)) msg += ", ";
        //        msg += field;
        //    }
        //}
    }
}
