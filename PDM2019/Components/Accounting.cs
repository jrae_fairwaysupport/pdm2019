﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;

namespace YourCompany.PDM2019.Components
{
    public class Accounting : PDMController
    {
        History history { get; set; }

        public Accounting(string EntityId, string Environment) : base (EntityId, Environment) 
        {
            history = new History(EntityId, Environment);
        }

    
        public override DataSet LoadPart(int partId, params string[] tables)
        {

            if (tables.Length == 0) tables = new string[] { "vwPartInfos" };

            DataSet ds = new DataSet();
            string tmpl8 = "SELECT * FROM {0} WHERE {1} = @P0";

            foreach (string table in tables)
            {
                string key = (table.Equals("Parts")) ? "Id" : "PartId";
                string sql = string.Format(tmpl8, table, key);

                switch (table)
                {
                    case "vwBatteries":
                        sql += " AND BatteriesFor = 'Part' ORDER BY RecordId";
                        break;
                    default:
                        break;
                }

                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = table;
                ds.Tables.Add(dt);
            }

            string sku = "jtrtmp";
            DataTable parts = ds.Tables["Parts"];
            if (parts != null)
            {
                if (parts.Rows.Count > 0) sku = pdm.GetValue<string>(parts.Rows[0], "PartId", sku);
            }

            //string partTable = entityId.Equals("LRH") ? "VMLREI.LEARNINGRESOURCES.COM" : "h2mvmfg.learningresources.com";
            //string qry = "SELECT 1 AS UNIT_QTY, E.LENGTH AS UNIT_LENGTH, E.WIDTH AS UNIT_WIDTH, E.HEIGHT AS UNIT_HEIGHT, S.WEIGHT AS UNIT_WEIGHT, "
            //           + "P.CASE_QTY, C.LENGTH AS CASE_LENGTH, C.WIDTH AS CASE_WIDTH, C.HEIGHT AS CASE_HEIGHT, S.CASE_WEIGHT, PLT.PACK_QTY AS PALLET_QTY, "
            //           + "PLT.LENGTH AS PALLET_LENGTH, PLT.WIDTH AS PALLET_WIDTH, PLT.HEIGHT AS PALLET_HEIGHT, ROUND(NVL(C.LENGTH, 0) * NVL(C.WIDTH, 0) * NVL(C.HEIGHT, 0) / 1728, 3) AS CPQ, "
            //           //+ "CASE WHEN COALESCE(P.USER_4, 'EMPTY') <> 'EMPTY' THEN 1 ELSE 0 END AS INNER_PACK, 'NOT YET IMPLEMENTED' AS INNER_PACK_DIMS "
            //           + "CAST(COALESCE(P.USER_4, '1') AS NUMBER) AS INNER_PACK, "
            //           + "CASE WHEN COALESCE(P.USER_4, '1') = '1' THEN P.CASE_QTY ELSE  ROUND(P.CASE_QTY / CAST(COALESCE(P.USER_4, '1') AS NUMBER), 2) END AS INNER_PACKS_PER_MASTER "
            //           + "FROM SKU S LEFT JOIN Part@" + partTable + " P ON S.ID = P.ID LEFT JOIN SKU_PACK E ON E.SKU_ID = S.ID AND E.PACK_ID = 'EA' "
            //           + "LEFT JOIN SKU_PACK C ON C.SKU_ID = S.ID AND C.PACK_ID = 'Cases' LEFT JOIN SKU_PACK PLT ON PLT.SKU_ID = S.ID AND PLT.PACK_ID = 'Pallets' "
            //           + "WHERE S.ID = @P0";

            //DataTable dcmsData = dcms.ExecuteReader(qry, sku);
            //dcmsData.TableName = "DCMS";
            //ds.Tables.Add(dcmsData);

            

            return ds;

            
        }



       
    //    public int PartInformation_Save(string userName, int partId, object parent)
    //    {
    //        bool newPart = (partId == -1);

    //        SideBarController sidebar = new SideBarController(entityId, environment);
    //        partId = sidebar.Save(userName, partId, parent);


    //        Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
    //        ParseForm(parent, tables);


    //        bool newPartInfos = true;

    //        if (!newPart)
    //        {
    //            DataSet ds = LoadPart(partId, "vwPartInfos");
    //            newPartInfos = ds.Tables["vwPartInfos"].Rows.Count == 0;
    //        }



    //        TableFields table = tables["Parts"];
    //        table.AddField("Id", partId, partId);


    //        //custom logic
    //        //moldsharevendor - if removed, make vendorId unset
    //        Dictionary<string, FieldChange> fields = tables["vwPartInfos"].Fields;
    //        if ((bool)fields["MoldShare"].newValue == false) fields["MoldShareVendorId"].newValue = null;

    //        //royalty take 2
    //        //create placeholders
    //        fields.Add("RoyaltyPercentage", new FieldChange(null, fields["RoyaltyInitial"].newValue));
    //        fields.Add("RoyaltyAmount", new FieldChange(null, fields["RoyaltyInitial"].newValue));

    //        //assign initial values
    //        string originalType = (string)fields["RoyaltyType"].oldValue;
    //        if (!string.IsNullOrEmpty(originalType))
    //        {
    //            if (originalType.Equals("PERCENTAGE")) fields["RoyaltyPercentage"].oldValue = fields["RoyaltyInitial"].oldValue;
    //            else fields["RoyaltyAmount"].oldValue = fields["RoyaltyInitial"].oldValue;
    //        }

    //        //get rid of values we don't need
    //        string newType = (string)fields["RoyaltyType"].newValue;
    //        if (!string.IsNullOrEmpty(newType))
    //        {
    //            if (newType.Equals("PERCENTAGE")) fields["RoyaltyAmount"].newValue = null;
    //            else fields["RoyaltyPercentage"].newValue = null;
    //        }

    //        //see if we're even a royalty item anymore
    //        if (!(bool)fields["RoyaltyItem"].newValue)
    //        {
    //            fields["RoyaltyPercentage"].newValue = null;
    //            fields["RoyaltyAmount"].newValue = null;
    //            fields["RoyaltyPayableTo"].newValue = null;
    //        }

    //        //get rid of helpers
    //        fields.Remove("RoyaltyType");
    //        fields.Remove("RoyaltyInitial");

    //        //PreferredVendor
    //        if (string.IsNullOrEmpty(((string)fields["PreferredVendor"].newValue)))
    //        {
    //            fields["BuyerUserId"].newValue = null;
    //        }
    //        else
    //        {
    //            if (fields["PreferredVendor"].HasChanged() || string.IsNullOrEmpty((string)fields["BuyerUserId"].newValue))
    //            {
    //                string sql = "SELECT BUYER FROM vwVendors WHERE ID = @P0";
    //                fields["BuyerUserId"].newValue = pdm.FetchSingleValue<string>(sql, "BUYER", (string)fields["PreferredVendor"].newValue);
    //            }

    //        }
    //        if (fields["PreferredVendor"].HasChanged() || fields["BuyerUserId"].HasChanged())
    //        {
    //            tables.Add("PurchasingPartInfos", new TableFields());
    //            tables["PurchasingPartInfos"].AddField("PreferredVendorId", fields["PreferredVendor"].oldValue, fields["PreferredVendor"].newValue);
    //            tables["PurchasingPartInfos"].AddField("BuyerUserId", fields["BuyerUserId"].oldValue, fields["BuyerUserId"].newValue);
    //        }
    //        //fields.Remove("PlannerUserId");


            
    //        fields.Add("PartId", new FieldChange(null, partId));
    //        fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));
    //        if (newPartInfos)
    //        {
    //            string msg = (partId == -1) ? "Created Part" : "Added Part Information";
    //            fields.Add("CreatedDate", new FieldChange(null, DateTime.Now));
    //            fields.Add("Active", new FieldChange(null, true));
    //            history.Log(partId, userName, "Product Development.Part Information", string.Empty, string.Empty, msg);
    //            pdm.InsertRecord("PartInfos", "PartId", tables["vwPartInfos"].GetParametersAsArray(true));

    //        }
    //        else
    //        {
    //            if (tables["vwPartInfos"].IsDirty())
    //            {
    //                history.Log(partId, userName, "Product Development.Part Information", tables["vwPartInfos"].Fields, "ModifiedDate", "CreatedDate", "PartId");
    //                pdm.UpdateRecord("PartInfos", "PartId", partId, tables["vwPartInfos"].GetParametersAsArray(true, "PartId"));
    //            }
    //        }

    //        if (tables.ContainsKey("PurchasingPartInfos"))
    //        {
    //            string sql = "SELECT * FROM PurchasingPartInfos WHERE PartId = @P0";
    //            DataTable dt = pdm.ExecuteReader(sql, partId);

    //            tables["PurchasingPartInfos"].AddField("ModifiedDate", null, DateTime.Now);

    //            if (dt.Rows.Count == 0)
    //            {
    //                tables["PurchasingPartInfos"].AddField("CreatedDate", null, DateTime.Now);
    //                tables["PurchasingPartInfos"].AddField("PartId", null, partId);
    //                history.Log(partId, userName, "Purchasing.Part Information", string.Empty, string.Empty, "Created Purchasing Record");
    //                pdm.InsertRecord("PurchasingPartInfos", "PartId", tables["PurchasingPartInfos"].GetParametersAsArray(true));
    //            }
    //            else
    //            {
    //                history.Log(partId, userName, "Purchasing.Part Information", tables["PurchasingPartInfos"].Fields, "PartId", "CreatedDate", "ModifiedDate");
    //                pdm.UpdateRecord("PurchasingPartInfos", "PartId", partId, tables["PurchasingPartInfos"].GetParametersAsArray(true));
    //            }
    //        }




    //        return partId;
    //    }

    //    public void Specifications_Save(string userName, int partId, object parent)
    //    {
    //        DataSet ds = LoadPart(partId, "vwPartSpecifications");
    //        bool newRecord = ds.Tables["vwPartSpecifications"].Rows.Count == 0;


    //        Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
    //        ParseForm(parent, tables);
    //        TableFields table = tables["vwPartSpecifications"];
    //        table.RenameField("RemoteBatteryQuantity", "BatteryQuantity");
    //        table.RenameField("RemoteBatterySize", "BatterySize");
    //        table.RenameField("RemoteBatteryIncluded", "BatteryIncluded");

    //        Dictionary<string, FieldChange> fields = tables["vwPartSpecifications"].Fields;


    //        //custom logic
    //        //if (fields["GeneralDescription"].HasChanged())
    //        //{
    //        //    tables.Add("PartInfos", new TableFields());
    //        //    tables["PartInfos"].AddField("Specification", fields["GeneralDescription"].oldValue, fields["GeneralDescription"].newValue);
    //       //}

    //        //remote battery
    //        bool oldValue = false;
    //        bool.TryParse(fields["IncludesRemote"].oldHistory, out oldValue); //unset or false
    //        bool newValue = (bool)fields["IncludesRemote"].newValue;
    //        Dictionary<string, FieldChange> batteries = table.GetSubFields("BatteryQuantity", "BatterySize", "BatteryIncluded");
    //        if (newValue)
    //        {
    //            string newHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].newHistory, batteries["BatteryQuantity"].newHistory, (bool)batteries["BatteryIncluded"].newValue ? "Included" : "Not Included");

    //            if (oldValue)
    //            {
    //                string sql = "UPDATE Batteries SET BatteryQuantity = @P0, BatterySize = @P1, BatteryIncluded = @P2 WHERE PartId = @P3 AND BatteriesFor = 'Remote'";
    //                pdm.ExecuteCommand(sql, batteries["BatteryQuantity"].newValue, batteries["BatterySize"].newValue, batteries["BatteryIncluded"].newValue, partId);
    //                string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
    //                history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", oldHistory, newHistory);
    //            }
    //            else
    //            {
    //                pdm.InsertRecord("Batteries", "RecordId", "PartId", partId, "BatteryQuantity", batteries["BatteryQuantity"].newValue, "BatterySize", batteries["BatterySize"].newValue,
    //                                        "BatteryIncluded", batteries["BatteryIncluded"].newValue, "BatteriesFor", "Remote");
    //                history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Created: " + newHistory);
    //            }
    //        }
    //        else
    //        {
    //            if (oldValue)
    //            {
    //                string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
    //                history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Deleted: " + oldHistory);

    //                string sql = "DELETE FROM Batteries WHERE PartId = @P0 AND BatteriesFor = 'Remote'";
    //                pdm.ExecuteCommand(sql, partId);
    //            }
    //        }




    //        fields.Remove("BatteryQuantity");
    //        fields.Remove("BatterySize");
    //        fields.Remove("BatteryIncluded");
    //        fields.Add("PartId", new FieldChange(null, partId));
    //        fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));

    //        if (newRecord)
    //        {
    //            fields.Add("CreatedDate", new FieldChange(null, DateTime.Now));
    //            history.Log(partId, userName, "Product Development.Specifications", string.Empty, string.Empty, "Created Specifications");
    //            pdm.InsertRecord("PartSpecifications", "PartId", tables["vwPartSpecifications"].GetParametersAsArray(true));
    //        }
    //        else
    //        {
    //            if (tables["vwPartSpecifications"].IsDirty("ModifiedDate"))
    //            {
    //                history.Log(partId, userName, "Product Development.Specifications", tables["vwPartSpecifications"].Fields, "ModifiedDate", "CreatedDate", "PartId");
    //                pdm.UpdateRecord("PartSpecifications", "PartId", partId, tables["vwPartSpecifications"].GetParametersAsArray(true));
    //            }
    //        }


    //        //additional changes
    //        //if (tables.ContainsKey("PartInfos"))
    //        //{
    //        //    tables["PartInfos"].AddField("ModifiedDate", null, DateTime.Now);
    //        //    history.Log(partId, userName, "Product Development.Specifications", tables["PartInfos"].Fields, "ModifiedDate");
    //        //    pdm.UpdateRecord("PartInfos", "PartId", partId, tables["PartInfos"].GetParametersAsArray(true));
    //        //}
    //    }

    //    public void Packaging_Save(string userName, int partId, object parent)
    //    {
    //        //this will never be a new record as Part must exist


    //        Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
    //        ParseForm(parent, tables);

    //        TableFields table = tables["Parts"];
    //        Dictionary<string, FieldChange> fields = table.Fields;


    //        //custom logic

    //        fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));

    //        if (table.IsDirty("ModifiedDate"))
    //        {
    //            history.Log(partId, userName, "Product Development.Packaging", fields, "ModifiedDate");
    //            pdm.UpdateRecord("Parts", "Id", partId, table.GetParametersAsArray(true));
    //        }


    //    }

    //    public void VendorPrice_Save(string userName, int partId, object parent)
    //    {
    //        string tabName = "Production Development.Vendor Price";

    //        DataSet ds = LoadPart(partId, "PartVendorPrices");
    //        bool newRecord = ds.Tables["PartVendorPrices"].Rows.Count == 0;


    //        Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
    //        ParseForm(parent, tables);

    //        TableFields table = tables["PartVendorPrices"];
    //        Dictionary<string, FieldChange> fields = table.Fields;


    //        //custom logic
    //        fields.Add("PartId", new FieldChange(null, partId));
    //        fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));

    //        if (newRecord)
    //        {
    //            fields.Add("CreatedDate", new FieldChange(null, DateTime.Now));
    //            history.Log(partId, userName, tabName, string.Empty, string.Empty, "Created Vendor Price");
    //            pdm.InsertRecord("PartVendorPrices", "PartId", table.GetParametersAsArray(true));
    //        }
    //        else
    //        {
    //            if (table.IsDirty("ModifiedDate"))
    //            {
    //                history.Log(partId, userName, tabName, table.Fields, "ModifiedDate", "CreatedDate", "PartId");
    //                pdm.UpdateRecord("PartVendorPrices", "PartId", partId, table.GetParametersAsArray(true));
    //            }
    //        }


    //        //additional changes

    //    }



    }
}


