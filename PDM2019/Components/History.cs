using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public class History 
    {
        private string entityId { get; set; }
        private string environment { get; set; }
        private SvcLibrary.DBHandler pdm { get; set; }

        public History(string EntityId, string Environment) 
        {
            entityId = EntityId;
            environment = Environment;

            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            pdm = new SvcLibrary.DBHandler(config, null, environment, entityId + "_PDM");
        }


        public DataTable LoadPart(int partId)
        {
            string sql = "SELECT * FROM vwHistory WHERE PartId = @P0 ORDER BY EventTime DESC";
            DataTable dt = pdm.ExecuteReader(sql, partId);
            dt.TableName = "vwHistory";

            return dt;
        }

        public DataTable LoadFilter(int partId, string tabName, string fieldName)
        {
            List<object>parms = new List<object>() { partId, tabName};
            string sql = "SELECT * FROM vwHistory WHERE PartId = @P0 AND TabName = @P1 ";

            if (!string.IsNullOrEmpty(fieldName))
            {
                parms.Add(fieldName);
                sql += "AND FieldName = @P2 ";
            }
            sql += "ORDER BY EventTime DESC";

            DataTable dt = pdm.ExecuteReader(sql, parms.ToArray());
            dt.TableName = "vwHistory";
            return dt;
        }

        public void Log(int partId, string userName, string tabName, string fieldName, string oldValue, string newValue)
        {
            if (tabName.Length > 100) tabName = tabName.Substring(0, 100);
            if (fieldName.Length > 50) fieldName = fieldName.Substring(0, 50);
            if (oldValue.Length > 400) oldValue = oldValue.Substring(0, 397) + "...";
            if (newValue.Length > 400) newValue = newValue.Substring(0, 397) + "...";

            pdm.InsertRecord("History", "PartId", "PartId", partId, "EventTime", DateTime.Now, "UserName", userName, "TabName", tabName, "FieldName", fieldName, "OldValue", oldValue, "NewValue", newValue);
        }

        public void Log(int partId, string userName, string tabName, Dictionary<string, FieldChange> fields, params string[] omits)
        {
            List<string> excludes = new List<string>();
            foreach (string omit in omits) excludes.Add(omit);

            foreach (string key in fields.Keys)
            {
                if (!excludes.Contains(key))
                {
                    string oldValue = fields[key].oldHistory;
                    string newValue = fields[key].newHistory;
                    if (!oldValue.Equals(newValue)) Log(partId, userName, tabName, key, oldValue, newValue);
                }
            }
        }

        private void Log(int partId, string userName, Dictionary<string, FieldChange> fields, params string[] omits)
        {
            List<string> excludes = new List<string>();
            foreach (string omit in omits) excludes.Add(omit);

            foreach (string key in fields.Keys)
            {
                if (!excludes.Contains(key))
                {
                    if (fields[key].HasChanged())
                    {
                        //if (key.Equals("MinAge") || key.Equals("MaxAge")) break; <--wtf? was this debug?
                        string tabName = fields[key].tabName;
                        Log(partId, userName, tabName, key, fields[key].oldHistory, fields[key].newHistory);
                    }
                }
            }
        }
    }
}
