﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;

namespace YourCompany.PDM2019.Components
{
    public class Visual : Controller
    {

        public Visual(string EntityId, string Environment) : base (EntityId, Environment) 
        {
        }

    
        public  DataSet LoadPart(int partId, params string[] tables)
        {

            if (tables.Length == 0) tables = new string[] { "Parts", "vwPartInfos", "vwVendorPriceBreaks", "vwPurchasingPartInfos", "vwQAPartInfos", "SalesPartInfos"};

            DataSet ds = new DataSet();
            string tmpl8 = "SELECT * FROM {0} WHERE {1} = @P0";

            foreach (string table in tables)
            {
                string key = (table.Equals("Parts")) ? "Id" : "PartId";
                string sql = string.Format(tmpl8, table, key);

                switch (table)
                {
                    case "vwBatteries":
                        sql += " AND BatteriesFor = 'Part' ORDER BY RecordId";
                        break;
                    default:
                        break;
                }

                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = table;
                ds.Tables.Add(dt);
            }


            DataTable marketing = ds.Tables["vwMarketingInfos"];
            for (int index = 1; index <= 4; index++)
            {
                string srcCol = "ProductCategory" + index.ToString();
                string tgtCol = "ProductCategoryName" + index.ToString();
                marketing.Columns.Add(tgtCol, System.Type.GetType("System.String"));

                if (marketing.Rows.Count > 0)
                {
                    DataRow dr = marketing.Rows[0];
                    string id = pdm.GetValue<string>(dr, srcCol, string.Empty);
                    if (!string.IsNullOrEmpty(id))
                    {
                        string sql = string.Format("SELECT * FROM LR_CATEGORY_{0} WHERE ID = @P0", index.ToString());
                        string description = vmfg.FetchSingleValue<string>(sql, "DESCRIPTION", id);
                        dr[tgtCol] = description;
                    }
                }
            }
            

            return ds;

            
        }

        private void Push(Dictionary<string, TableFields>tables, string tableName, string tgtCol, object value)
        {
            if (!tables.ContainsKey(tableName)) tables.Add(tableName, new TableFields());
            TableFields table = tables[tableName];
            table.AddField(tgtCol, null, value);
        }

        private void Push(Dictionary<string, TableFields>tables, string tableName, string tgtCol, DataSet ds, string srcTable, string srcCol)
        {
            DataTable dt = ds.Tables[srcTable];
            if (dt == null) return;
            if (dt.Rows.Count == 0) return;
            DataRow dr = dt.Rows[0];

            if (!tables.ContainsKey(tableName)) tables.Add(tableName, new TableFields());
            TableFields table = tables[tableName];

            table.AddField(tgtCol, null, dr[srcCol]);
        }

        public bool Push(string userName, int partId, ref bool inVisual, ref StringBuilder messages)
        {
            //set to true to trap anything we neeed to show user
            vmfg.ThrowErrors = true;

            //Push to Part
            DataSet ds = OpenPartEditor(partId);

            if (ds.Tables["Parts"] == null)
            {
                messages.AppendLine("Could not load PDM data for export to Visual.");
                return false;
            }

            if (ds.Tables["Parts"].Rows.Count == 0) 
            {
                messages.AppendLine("No PDM data found for export to Visual.");
                return false;
            }

            string sku = pdm.GetValue<string>(ds.Tables["Parts"].Rows[0], "PartId", string.Empty);
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();

            Push(tables, "PART", "ID", ds, "Parts", "PartId");
            Push(tables, "PART", "DESCRIPTION", ds, "Parts", "Description");
            Push(tables, "PART", "ABC_CODE", ds, "Parts", "AbcCode");
            Push(tables, "PART", "USER_1", ds, "Parts", "UpcCode");
            Push(tables, "PART", "PRODUCT_CODE", ds, "vwPartInfos", "Company");
            Push(tables, "PART", "PART_PARENT", ds, "vwPartInfos", "ParentPartId");
            Push(tables, "PART", "PREF_VENDOR_ID", ds, "vwPartInfos", "PreferredVendor");
            Push(tables, "PART", "FabricationLocation", ds, "vwPartInfos", "FabricationLocation");
            Push(tables, "PART", "PLANNER_ID", tables["PART"].Fields["FabricationLocation"].newValue.ToString().ToUpper().Equals("FABUS") ? "USA" : "INTL");
            tables["PART"].Fields.Remove("FabricationLocation");
            Push(tables, "PART", "FABRICATED", VisualBool(ds, "vwPartInfos", "Fabricated"));
            Push(tables, "PART", "PURCHASED", VisualBool(ds, "vwPartInfos", "Purchased"));
            Push(tables, "PART", "UNIT_MATERIAL_COST", ds, "vwPartInfos", "MaterialCost");
            Push(tables, "PART", "USER_10", ds, "vwPartInfos", "LandedCost");
            Push(tables, "PART", "UNIT_PRICE", ds, "vwPartInfos", "NetPrice");
            Push(tables, "PART", "WHSALE_UNIT_COST", ds, "vwPartInfos", "RetailPrice");
            Push(tables, "PART", "PLANNING_LEADTIME", ds, "vwPartInfos", "LeadTime");
            Push(tables, "PART", "MINIMUM_ORDER_QTY", ds, "vwPartInfos", "MinimumOrderQuantity");
            Push(tables, "PART", "MULTIPLE_ORDER_QTY", ds, "vwPartInfos", "MultiplesOf");
            Push(tables, "PART", "USER_9", ds, "vwPartInfos", "YearIntroduced");
            Push(tables, "PART", "PLANNER_USER_ID", ds, "vwPartInfos", "PlannerUserId");
            Push(tables, "PART", "BUYER_USER_ID", ds, "vwPartInfos", "BuyerUserId");
            Push(tables, "PART", "SAFETY_STOCK_QTY", ds, "vwPurchasingPartInfos", "SafetyStock");
            Push(tables, "PART", "PREF_VENDOR_ID", ds, "vwPartInfos", "PreferredVendor");
            Push(tables, "PART", "MATERIAL_CODE", ds, "vwPurchasingPartInfos", "MaterialCode");
            Push(tables, "PART", "ENGINEERING_MSTR", ds, "vwPurchasingPartInfos", "EngineeringMasterId");
            Push(tables, "PART", "PRIMARY_WHS_ID", ds, "vwPurchasingPartInfos", "PrimaryWarehouse");
            Push(tables, "PART", "PRODUCT_STATUS", ds, "vwPurchasingPartInfos", "ProductStatus");
            Push(tables, "PART", "ORIG_COUNTRY_ID", ds, "vwPurchasingPartInfos", "PortOfOrigin");
            Push(tables, "PART", "LOT_TYPE", ds, "vwQAPartInfos", "LotType");


            
            
            Push(tables, "LR_PART_EXTENSION", "PART_ID", sku);
            Push(tables, "LR_PART_EXTENSION", "QTY_CONV", ds, "vwPartInfos", "ParentPartQuantityConversion");
            Push(tables, "LR_PART_EXTENSION", "FRT_IN_COST", ds, "vwPartInfos", "FreightInCost");
            Push(tables, "LR_PART_EXTENSION", "DUTIES", ds, "vwPartInfos", "Duties");
            Push(tables, "LR_PART_EXTENSION", "LANDED_COST", ds, "vwPartInfos", "LandedCost");
            Push(tables, "LR_PART_EXTENSION", "MainComponentPartId", ds, "vwPartInfos", "MainComponentPartId");
            Push(tables, "LR_PART_EXTENSION", "MAIN_COMP", string.IsNullOrEmpty(tables["LR_PART_EXTENSION"].Fields["MainComponentPartId"].newValue.ToString()) ? "N" : "Y");
            Push(tables, "LR_PART_EXTENSION", "DUTIES", ds, "vwPurchasingPartInfos", "DutyRate");
            Push(tables, "LR_PART_EXTENSION", "ADD_RATE", ds, "vwPurchasingPartInfos", "AddRate");

            Push(tables, "LR_PART_ASSEMBLY", "PART_ID", sku);
            if (VisualBool(ds, "vwPartInfos", "Fabricated").Equals("Y")) Push(tables, "LR_PART_ASSEMBLY", "ASSEMBLY_COST", ds, "vwPartInfos", "UnitAssemblyCost");

            Push(tables, "HARMONIZED_TARIFF", "PART_ID", sku);
            Push(tables, "HARMONIZED_TARIFF", "HTS_CODE", ds, "vwPurchasingPartInfos", "HtsCode");

            Push(tables, "WAREHOUSE", "PART_ID", sku);
            Push(tables, "WAREHOUSE", "ID", ds, "vwPurchasingPartInfos", "ForeignWarehouse");

            Push(tables, "LR_PART_MFG", "PART_ID", sku);
            Push(tables, "LR_PART_MFG", "COUNTRY_MFG", ds, "vwPurchasingPartInfos", "CountryOfOrigin");

            Push(tables, "LR_PART_EXT", "PART_ID", sku);
            for (int index = 1; index <= 4; index++)
            {
                string srcCol = "ProductCategory" + index.ToString();
                string tgtCol = "CATEGORY_" + index.ToString();
                Push(tables, "LR_PART_EXT", tgtCol, ds, "vwMarketingInfos", srcCol);

                srcCol = "ProductCategoryName" + index.ToString();
                tgtCol = string.Format("CAT_{0}_DESC", index);
                Push(tables, "LR_PART_EXT", tgtCol, ds, "vwMarketingInfos", srcCol);
            }

            foreach (DataRow dr in ds.Tables["SalesPartInfos"].Rows)
            {
                string customerId = pdm.GetValue<string>(dr, "CustomerId", string.Empty);
                string tableName = "CUSTOMER_PRICE*" + customerId;
                Push(tables, tableName, "PART_ID", sku);
                Push(tables, tableName, "CUSTOMER_ID", customerId);

                decimal test = pdm.GetValue<decimal>(dr, "DefaultPrice", decimal.MinValue);
                if (test == decimal.MinValue) test = pdm.GetValue<decimal>(dr, "UnitPrice1", decimal.MinValue);
                if (test != decimal.MinValue) Push(tables, tableName, "DEFAULT_UNIT_PRICE", test);

                string sellingUM = pdm.GetValue<string>(dr, "UnitOfMeasure", string.Empty);
                Push(tables, tableName, "SELLING_UM", sellingUM);

                string customerPartId = pdm.GetValue<string>(dr, "CustomerPartId", string.Empty);
                Push(tables, tableName, "CUSTOMER_PART_ID", customerPartId);

                Push(tables, tableName, "REC_PARSED", "N");

                for (int index = 1; index <= 10; index++)
                {
                    string field = "QuantityBreak" + index.ToString();
                    int testQty = pdm.GetValue<int>(dr, field, int.MinValue);
                    if (testQty != int.MinValue) Push(tables, tableName, field, testQty);

                    field = "UnitPrice" + index.ToString();
                    decimal testPrice = pdm.GetValue<decimal>(dr, field, decimal.MinValue);
                    if (testPrice != decimal.MinValue) Push(tables, tableName, field, testPrice);

                }
            }
            //if (PART.IsDirty())
            //{
            //    try 
            //    { 
            //        int records = vmfg.InsertRecord("PART", "ID", PART.GetParametersAsArray(true));
            //        inVisual = (records > 0);
            //    }
            //    catch (Exception ex)
            //    {
            //        messages.AppendLine("Part:");
            //        messages.AppendLine(ex.Message);
            //        return false;
            //    }
            //}

            //if (! inVisual) return false;

            ////push other tables after part

            //DataTable vendor = ds.Tables["vwVendorPriceBreaks"];
            //foreach (DataRow pb in vendor.Rows)
            //{
            //    TableFields VENDOR_PART = new TableFields();
            //    VENDOR_PART.AddField("PART_ID", null, sku);
            //    VENDOR_PART.AddField("VENDOR_ID", null, pb["VendorId"]);
            //    string vendorPartId = pdm.GetValue<string>(pb, "VendorPartId", string.Empty);
            //    if (string.IsNullOrEmpty(vendorPartId)) vendorPartId = sku;
            //    VENDOR_PART.AddField("VENDOR_PART_ID", null, vendorPartId);

            //    if (VENDOR_PART.IsDirty("PART_ID"))
            //    {
            //        try { vmfg.InsertRecord("VENDOR_PART", "PART_ID", VENDOR_PART.GetParametersAsArray(true)); }
            //        catch(Exception ex)
            //        {
            //            messages.AppendLine("VENDOR_PART:");
            //            messages.AppendLine(ex.Message);
            //        }
            //    }

            //    TableFields VENDOR_QUOTE = new TableFields();
            //    VENDOR_QUOTE.AddField("VENDOR_ID", null, pb["VendorId"]);
            //    VENDOR_QUOTE.AddField("VENDOR_PART_ID", null, vendorPartId);

            //    for(int index = 1; index <= 5; index++)
            //    {
            //        string src = "Quantity" + index.ToString();
            //        string tgt = "QTY_BREAK_" + index.ToString();
            //        VENDOR_QUOTE.AddField(tgt, null, pb[src]);

            //        src = "Price" + index.ToString();
            //        tgt = "UNIT_PRICE_" + index.ToString();
            //        VENDOR_QUOTE.AddField(tgt, null, pb[src]);
            //    }

            //    VENDOR_QUOTE.AddField("DEFAULT_UNIT_PRICE", null, pb["Price1"]);
            //    VENDOR_QUOTE.AddField("QUOTE_DATE", null, DateTime.Now);

            //    try { vmfg.InsertRecord("VENDOR_QUOTE", "VENDOR_PART_ID", VENDOR_QUOTE.GetParametersAsArray(true)); }
            //    catch (Exception ex)
            //    {
            //        messages.AppendLine("VENDOR_QUOTE:");
            //        messages.AppendLine(ex.Message);
            //    }

            //}

            return true;
        }

    }



}


