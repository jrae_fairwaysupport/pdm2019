using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;
using System.Web.UI.WebControls;

namespace YourCompany.PDM2019.Components
{
    public class Marketing : PDMController 
    {
        History history { get; set; }

        public Marketing(string EntityId, string Environment) : base(EntityId, Environment) 
        {
            history = new History(EntityId, Environment);
        }

        public override DataSet LoadPart(int partId, params string[] tables)
        {
            if (tables.Length == 0) tables = new string[] { "Parts", "vwMarketingInfos", "Awards" };
            DataSet ds = new DataSet();

            string tmpl8 = "SELECT * FROM {0} WHERE PartId = @P0";
            foreach (string table in tables)
            {
                string sql = string.Format(tmpl8, table);
                if (table.Equals("Parts")) sql = "SELECT * FROM Parts WHERE Id = @P0";
                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = table;
                ds.Tables.Add(dt);
            }

            DataTable src = ds.Tables["vwMarketingInfos"];
            DataTable add = new DataTable();
            add.TableName = "Additionals";
            tmpl8 = "SELECT * FROM LR_CATEGORY_{0} WHERE ID = @P0";
            for (int index = 1; index <= 4; index++)
            {
                string columnName = "ProductCategory" + index.ToString();
                string categoryName = string.Empty;
                if (src.Rows.Count > 0)
                {
                    int id = pdm.GetValue<int>(src.Rows[0], columnName, -1);
                    if (id >= 0)
                    {
                        string sql = string.Format(tmpl8, index);
                        categoryName = vmfg.FetchSingleValue<string>(sql, "DESCRIPTION", id);
                    }
                }
                add.Columns.Add(columnName, System.Type.GetType("System.String"));

                if (add.Rows.Count == 0) add.Rows.Add();
                add.Rows[0][columnName] = categoryName;
            }




            ds.Tables.Add(add);

            return ds;
        }

        //public void xPopulateProductCategory(DropDownList ddl, int index)
        //{
        //    string sql = string.Format("SELECT * FROM LR_CATEGORY_{0} ORDER BY DESCRIPTION", index);
        //    DataTable dt = vmfg.ExecuteReader(sql);

        //    DataRow dr = dt.NewRow();
        //    dr["ID"] = -1;
        //    dr["DESCRIPTION"] = "Product Category " + index.ToString();
        //    dt.Rows.InsertAt(dr, 0);

        //    ddl.DataTextField = "DESCRIPTION";
        //    ddl.DataValueField = "ID";
        //    ddl.DataSource = dt;
        //    ddl.DataBind();
        //}

        //private void SaveAwards(string userName, int partId, XmlDocument doc)
        //{
        //    XmlNode root = doc.ChildNodes[0];
        //    foreach (XmlNode award in root.ChildNodes)
        //    {
        //        string recordId = award.Attributes["recordId"].Value;
        //        string name = award.Attributes["name"].Value;
        //        string deleted = award.Attributes["deleted"].Value;

        //        int test = 0;
        //        if (Int32.TryParse(recordId, out test))
        //        {
        //            //existing record
        //            if (deleted == "true")
        //            {
        //                history.Log(partId, userName, "Marketing", "Awards", string.Empty, "Deleted Award: " + name);
        //                string sql = "DELETE FROM AWARDS WHERE PartId = @P0 AND RecordId = @P1";
        //                pdm.ExecuteCommand(sql, partId, test);
        //            }

        //        }
        //        else
        //        {
        //            //newbies with guid's
        //            if (deleted == "false")
        //            {
        //                history.Log(partId, userName, "Marketing", "Awards", string.Empty, "Added Award: " + name);
        //                pdm.InsertRecord("Awards", "RecordId", "PartId", partId, "Name", name);
        //            }
        //        }
        //    }
        //}

        //public void Save(string userName, int partId, XmlDocument doc, object parent)
        //{
        //    SaveAwards(userName, partId, doc);

        //    DataSet ds = LoadPart(partId, "vwMarketingInfos");
        //    bool newRecord = ds.Tables["vwMarketingInfos"].Rows.Count == 0;

        //    Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
        //    ParseForm(parent, tables);

        //    TableFields table = tables["vwMarketingInfos"];
        //    table.AddField("ModifiedDate", null, DateTime.Now);
        //    table.AddField("PartId", null, partId);

        //    if (newRecord)
        //    {
        //        history.Log(partId, userName, "Marketing", string.Empty, string.Empty, "Created Marketing record.");
        //        table.AddField("CreatedDate", null, DateTime.Now);
        //        pdm.InsertRecord("MarketingInfos", "PartId", table.GetParametersAsArray(true));
        //    }
        //    else
        //    {
        //        if (table.IsDirty("PartId", "ModifiedDate"))
        //        {
        //            history.Log(partId, userName, "Marketing", table.Fields, "ModifiedDate", "CreatedDate", "PartId");
        //            pdm.UpdateRecord("MarketingInfos", "PartId", partId, table.GetParametersAsArray(true, "PartId"));
        //        }
        //    }

        //    table = tables["Parts"];
        //    if (table.IsDirty())
        //    {
        //        history.Log(partId, userName, "Header", table.Fields, "PartId");

        //        table.AddField("Id", null, partId);
        //        table.AddField("ModifiedDate", null, DateTime.Now);
        //        pdm.UpdateRecord("Parts", "Id", partId, table.GetParametersAsArray(true, "Id"));
                
        //    }

        //}



        
    }
}
