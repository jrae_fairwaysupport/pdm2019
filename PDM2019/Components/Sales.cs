using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;
using System.Web.UI.WebControls;

namespace YourCompany.PDM2019.Components
{
    public class SalesController : PDMController
    {
        private SvcLibrary.DBHandler useDatabase { get; set; }
        private string PartStatus { get; set; }

        public SalesController(string EntityId, string Environment) : base(EntityId, Environment) {}

        public SalesController(string EntityId, string Environment, string partStatus) : base(EntityId, Environment) 
        {
            PartStatus = partStatus;
            if (partStatus.Equals("Approved"))
                useDatabase = vmfg;
            else
                useDatabase = pdm;
        }


        public override DataSet LoadPart(int partId, params string[] tables)
        {
            //don't use the standard load for this guy
            return new DataSet();
        }

        //public DataTable LoadPart(string partId)
        //{
        //    string sql = "SELECT C.NAME AS CUSTOMER_NAME, P.CUSTOMER_ID || ' - ' || C.NAME AS CUSTOMER_SUGGEST, "
        //        + "CASE WHEN COALESCE(P.CUSTOMER_PART_ID, 'EMPTY') <> 'EMPTY' THEN 'Customer Part Id: ' || P.CUSTOMER_PART_ID ELSE NULL END AS CustomerPartId, "
        //        + "P.* "
        //        + "FROM CUSTOMER_PRICE P INNER JOIN CUSTOMER C ON P.CUSTOMER_ID = C.ID WHERE PART_ID = @P0 "
        //        + "ORDER BY P.CUSTOMER_ID";
        //    DataTable dt = vmfg.ExecuteReader(sql, partId);
        //    dt.TableName = "CustomerPrice";

        //    return dt;
        //}

        public string sku { get; set; }
        public string PartQueryBase
        {
            get
            {
               if (PartStatus.Equals("Approved")) return "SELECT C.NAME AS CUSTOMER_NAME, P.CUSTOMER_ID || ' - ' || C.NAME AS CUSTOMER_SUGGEST, "
                        + "CASE WHEN COALESCE(P.CUSTOMER_PART_ID, 'EMPTY') <> 'EMPTY' THEN 'Customer Part Id: ' || P.CUSTOMER_PART_ID ELSE NULL END AS CustomerPartId, "
                        + "P.* "
                        + "FROM CUSTOMER_PRICE P INNER JOIN CUSTOMER C ON P.CUSTOMER_ID = C.ID ";

               return "SELECT C.NAME AS CUSTOMER_NAME, dbo.FormatTextSuggest(P.CustomerId, C.NAME) AS CUSTOMER_SUGGEST, "
                       + "CASE WHEN COALESCE(P.CustomerPartId, 'EMPTY') <> 'EMPTY' THEN 'Customer Part Id: ' + P.CustomerPartId ELSE NULL END AS CustomerPartId, "
                       + "P.CustomerId AS CUSTOMER_ID, P.Part_Id AS PART_ID, P.UnitOfMeasure AS SELLING_UM, P.CustomerPartId AS CUSTOMER_PART_ID, "
                       + "P.QuantityBreak1 AS QTY_BREAK_1, P.QuantityBreak2 AS QTY_BREAK_2, P.QuantityBreak3 AS QTY_BREAK_3, P.QuantityBreak4 AS QTY_BREAK_4, P.QuantityBreak5 AS QTY_BREAK_5, "
                       + "P.QuantityBreak6 AS QTY_BREAK_6, P.QuantityBreak7 AS QTY_BREAK_7, P.QuantityBreak8 AS QTY_BREAK_8, P.QuantityBreak9 AS QTY_BREAK_9, P.QuantityBreak10 AS QTY_BREAK_10, "
                       + "P.UnitPrice1 AS UNIT_PRICE_1, P.UnitPrice2 AS UNIT_PRICE_2, P.UnitPrice3 AS UNIT_PRICE_3, P.UnitPrice4 AS UNIT_PRICE_4, P.UnitPrice5 AS UNIT_PRICE_5, "
                       + "P.UnitPrice6 AS UNIT_PRICE_6, P.UnitPrice7 AS UNIT_PRICE_7, P.UnitPrice8 AS UNIT_PRICE_8, P.UnitPrice9 AS UNIT_PRICE_9, P.UnitPrice10 AS UNIT_PRICE_10, "
                       + "P.DefaultPrice AS DEFAULT_UNIT_PRICE FROM SalesPartInfos P INNER JOIN [FWS-REPLR\\LR_REPLICATION].VMLREI.SYSADM.CUSTOMER C ON P.CustomerId = C.ID ";
                        //+ "INNER JOIN Parts ON P.Part_Id = Parts.Id "
	        
            }
        }
        public string PartQueryOrderBy
        {
            get
            {
                if (PartStatus.Equals("Approved")) return "P.CUSTOMER_ID";
                return "P.CustomerId";
            }
        }
        public string PartQueryDatabase
        {
            get
            {
                return entityId.Equals("LRH") ? "VMLREI11" : "H2MVMLREI11";
            }
        }

        //public void PopulateUnits(DropDownList ddl)
        //{
        //    string sql = "SELECT * FROM UNITS ORDER BY DESCRIPTION";
        //    DataTable dt = vmfg.ExecuteReader(sql);

        //    DataRow dr = dt.NewRow();
        //    dr["UNIT_OF_MEASURE"] = "";
        //    dr["DESCRIPTION"] = "Unit of Measure";
        //    dr["SCALE"] = 0;
        //    dt.Rows.InsertAt(dr, 0);

        //    ddl.DataTextField = "DESCRIPTION";
        //    ddl.DataValueField = "UNIT_OF_MEASURE";
        //    ddl.DataSource = dt;
        //    ddl.DataBind();
        //}

        public DataTable LoadPriceBreaks(string partId, string customerId, out bool lockdown)
        {
            lockdown = false;

            //really only concerned with whether or not this is a valid customer, not if any breaks have yet been configured
            string sql = "SELECT COUNT(*) AS THECOUNT FROM CUSTOMER WHERE ID = @P0";
            int count = vmfg.FetchSingleValue<int>(sql, "THECOUNT", customerId);
            lockdown = (count > 0);


            DataTable dt = null;
            if (PartStatus.Equals("Approved"))
            {
                sql = PartQueryBase + "WHERE CUSTOMER_ID = @P0 AND PART_ID = @P1";
                dt = vmfg.ExecuteReader(sql, customerId, partId);
                dt.TableName = "CUSTOMER_PRICE";
            }
            else
            {
                sql = PartQueryBase + "WHERE CustomerId = @P0 AND Part_Id = @P1";
                dt = pdm.ExecuteReader(sql, customerId, partId);
                dt.TableName = "CUSTOMER_PRICE";
            }

            return dt;
        }

        public void SavePriceBreak(string userName, int partId, string sku, string customerId, object parent)
        {
            if (!PartStatus.Equals("Approved"))
            {
                SavePendingPriceBreak(userName, partId, sku, customerId, parent);
                return;
            }

            History history = new History(entityId, environment);

            string sql = "SELECT COUNT(*) AS THECOUNT FROM CUSTOMER_PRICE WHERE PART_ID = @P0 AND CUSTOMER_ID = @P1";
            int count = vmfg.FetchSingleValue<int>(sql, "THECOUNT", sku, customerId);
            bool newRecord = (count == 0);


            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);
            TableFields table = tables["CUSTOMER_PRICE"];
            table.AddField("PART_ID", null, sku);
            if (table.Fields.ContainsKey("CUSTOMER_ID")) table.Fields.Remove("CUSTOMER_ID");
            table.AddField("CUSTOMER_ID", null, customerId);
            table.AddField("REC_PARSED", null, "N");
            Dictionary<string, FieldChange> fields = table.Fields;

            StringBuilder oldHistory = new StringBuilder();
            StringBuilder newHistory = new StringBuilder();

            string tabName = "Sales.Price Breaks";
            string fieldName = "Customer: " + customerId;
            oldHistory.AppendFormat("PartId: {0}, UM: {1}, Default Price: {2}<br />", fields["CUSTOMER_PART_ID"].oldHistory, fields["SELLING_UM"].oldHistory, fields["DEFAULT_UNIT_PRICE"].oldHistory);
            newHistory.AppendFormat("PartId: {0}, UM: {1}, Default Price: {2}<br />", fields["CUSTOMER_PART_ID"].newHistory, fields["SELLING_UM"].newHistory, fields["DEFAULT_UNIT_PRICE"].newHistory);
            for (int index = 1; index <= 10; index++)
            {
                oldHistory.AppendFormat("{0}:{1} - {2}<br />", index, fields["QTY_BREAK_" + index.ToString()].oldHistory, fields["UNIT_PRICE_" + index.ToString()].oldHistory);
                newHistory.AppendFormat("{0}:{1} - {2}<br />", index, fields["QTY_BREAK_" + index.ToString()].newHistory, fields["UNIT_PRICE_" + index.ToString()].newHistory);
            }
            

            if (newRecord)
            {
                history.Log(partId, userName, tabName, fieldName, string.Empty, "Created Price Breaks:<br />" + newHistory.ToString());
                vmfg.InsertRecord("CUSTOMER_PRICE", "PART_ID", table.GetParametersAsArray(true));
            }
            else
            {
                if (table.IsDirty("PART_ID", "CUSTOMER_ID", "REC_PARSED"))
                {
                    history.Log(partId, userName, tabName, fieldName, oldHistory.ToString(), newHistory.ToString());
                    List<object> parameters = new List<object>();
                    sql = table.PrepareUpdateQuery("CUSTOMER_PRICE", true, ref parameters, "PART_ID", "CUSTOMER_ID", "REC_PARSED");
                    sql = table.FinalizeUpdateQuery(sql, ref parameters, "PART_ID", "CUSTOMER_ID");
                    vmfg.ExecuteCommand(sql, parameters.ToArray());
                }
            }
            



        }


        public void SavePendingPriceBreak(string userName, int partId, string sku, string customerId, object parent)
        {
            History history = new History(entityId, environment);

            string sql = "SELECT COUNT(*) AS THECOUNT FROM SalesPartInfos WHERE PART_ID = @P0 AND CustomerId = @P1";
            int count = pdm.FetchSingleValue<int>(sql, "THECOUNT", partId, customerId);
            bool newRecord = (count == 0);


            //keep this here so that history logging is consistent between pdm and vmfg breaks
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);
            TableFields table = tables["CUSTOMER_PRICE"];
            table.AddField("PART_ID", null, sku);
            if (table.Fields.ContainsKey("CUSTOMER_ID")) table.Fields.Remove("CUSTOMER_ID");
            table.AddField("CUSTOMER_ID", null, customerId);
            Dictionary<string, FieldChange> fields = table.Fields;

            StringBuilder oldHistory = new StringBuilder();
            StringBuilder newHistory = new StringBuilder();

            string tabName = "Sales.Price Breaks";
            string fieldName = "Customer: " + customerId;
            oldHistory.AppendFormat("PartId: {0}, UM: {1}, Default Price: {2}<br />", fields["CUSTOMER_PART_ID"].oldHistory, fields["SELLING_UM"].oldHistory, fields["DEFAULT_UNIT_PRICE"].oldHistory);
            newHistory.AppendFormat("PartId: {0}, UM: {1}, Default Price: {2}<br />", fields["CUSTOMER_PART_ID"].newHistory, fields["SELLING_UM"].newHistory, fields["DEFAULT_UNIT_PRICE"].newHistory);
            for (int index = 1; index <= 10; index++)
            {
                oldHistory.AppendFormat("{0}:{1} - {2}<br />", index, fields["QTY_BREAK_" + index.ToString()].oldHistory, fields["UNIT_PRICE_" + index.ToString()].oldHistory);
                newHistory.AppendFormat("{0}:{1} - {2}<br />", index, fields["QTY_BREAK_" + index.ToString()].newHistory, fields["UNIT_PRICE_" + index.ToString()].newHistory);
            }

            //rename to pdm fields
            table.RenameField("CUSTOMER_ID", "CustomerId");
            table.RenameField("CUSTOMER_PART_ID", "CustomerPartId");
            table.RenameField("DEFAULT_UNIT_PRICE", "DefaultPrice");
            table.RenameField("SELLING_UM", "UnitOfMeasure");
            for (int index = 1; index <= 10; index++)
            {
                string orig = "QTY_BREAK_" + index.ToString();
                string tgt = "QuantityBreak" + index.ToString();
                table.RenameField(orig, tgt);

                orig = "UNIT_PRICE_" + index.ToString();
                tgt = "UnitPrice" + index.ToString();
                table.RenameField(orig, tgt);
            }
            fields.Remove("PART_ID");
            table.AddField("PART_ID", null, partId);


            table.AddField("ModifiedDate", null, DateTime.Now);
            if (newRecord)
            {
                history.Log(partId, userName, tabName, fieldName, string.Empty, "Created Price Breaks:<br />" + newHistory.ToString());
                table.AddField("CreatedDate", null, DateTime.Now);
                pdm.InsertRecord("SalesPartInfos", "ID", table.GetParametersAsArray(true));
            }
            else
            {
                if (table.IsDirty("PART_ID", "CustomerId"))
                {
                    history.Log(partId, userName, tabName, fieldName, oldHistory.ToString(), newHistory.ToString());
                    List<object> parameters = new List<object>();
                    sql = table.PrepareUpdateQuery("SalesPartInfos", true, ref parameters, "PART_ID", "CustomerId");
                    sql = table.FinalizeUpdateQuery(sql, ref parameters, "PART_ID", "CustomerId");
                    pdm.ExecuteCommand(sql, parameters.ToArray());
                }
            }




        }

        public void DeletePriceBreaks(string userName, int partId, string sku, string customerId)
        {
            string tabName = "Sales.Price Breaks";
            string fieldName = "Customer: " + customerId;
            History history = new History(entityId, environment);
            history.Log(partId, userName, tabName, fieldName, string.Empty, "Deleted All Pricebreaks");

            if (PartStatus.Equals("Approved"))
            {
                string sql = "DELETE FROM CUSTOMER_PRICE WHERE CUSTOMER_ID = @P0 AND PART_ID = @P1";
                vmfg.ExecuteCommand(sql, customerId, sku);
            }
            else
            {
                string sql = "DELETE FROM SalesPartInfos WHERE CustomerId = @P0 and PART_ID = @P1";
                pdm.ExecuteCommand(sql, customerId, partId);
            }
        }

    }
}
