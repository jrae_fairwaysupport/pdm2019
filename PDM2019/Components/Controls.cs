﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace YourCompany.PDM2019.Components
{
    public abstract partial class PDMController
    {
        public void Battery_Add(string userId, int partId, string batteryFor, string size, int qty, bool included)
        {
            int recordId = pdm.InsertRecord("Batteries", "RecordId", "PartId", partId, "BatteriesFor", batteryFor, "BatterySize", size, "BatteryQuantity", qty, "BatteryIncluded", included);

            string msg = string.Format("Added {0} Battery: {0}, {1}, {2}", size, qty, (included ? "Included" : "Not Included"), batteryFor);

            History history = new History(entityId, environment);
            history.Log(partId, userId, "Part Specifications", "Batteries", string.Empty, msg);
        }

        public void Battery_Delete(string userId, int recordId, int partId, string batteryFor, string size, int qty, bool included)
        {
            //our workaround until we can clean up the db design
            if (recordId == -1)
            {
                //we're in the specification database
                string sql = "UPDATE PartSpecifications SET BatterySize = NULL, BatteryQuantity = NULL, BatteriesIncluded = NULL WHERE PartId = @P0";
                pdm.ExecuteCommand(sql, partId);
            }
            else
            {
                string sql = "DELETE FROM Batteries WHERE RecordId = @P0";
                pdm.ExecuteCommand(sql, recordId);
            }

            string msg = string.Format("Deleted {0} Battery: {1}, {2}, {3}", batteryFor, size, qty, (included ? "Included" : "Not Included"));
            History history = new History(entityId, environment);
            history.Log(partId, userId, "Part Specifications", "Batteries", string.Empty, msg);


        }

        public void Battery_SaveRemote(string userName, int partId, TableFields table)
        {
            table.RenameField("RemoteBatteryQuantity", "BatteryQuantity");
            table.RenameField("RemoteBatterySize", "BatterySize");
            table.RenameField("RemoteBatteryIncluded", "BatteryIncluded");

            Dictionary<string, FieldChange> fields = table.Fields;
            if (!fields.ContainsKey("IncludesRemote")) return; //Not called from a valid path

            History history = new History(entityId, environment);

            //remote battery
            bool oldValue = false;
            bool.TryParse(fields["IncludesRemote"].oldHistory, out oldValue); //unset or false
            bool newValue = (bool)fields["IncludesRemote"].newValue;
            Dictionary<string, FieldChange> batteries = table.GetSubFields("BatteryQuantity", "BatterySize", "BatteryIncluded");
            if (newValue)
            {
                string newHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].newHistory, batteries["BatteryQuantity"].newHistory, (bool)batteries["BatteryIncluded"].newValue ? "Included" : "Not Included");

                if (oldValue)
                {
                    string sql = "UPDATE Batteries SET BatteryQuantity = @P0, BatterySize = @P1, BatteryIncluded = @P2 WHERE PartId = @P3 AND BatteriesFor = 'Remote'";
                    pdm.ExecuteCommand(sql, batteries["BatteryQuantity"].newValue, batteries["BatterySize"].newValue, batteries["BatteryIncluded"].newValue, partId);
                    string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
                    history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", oldHistory, newHistory);
                }
                else
                {
                    pdm.InsertRecord("Batteries", "RecordId", "PartId", partId, "BatteryQuantity", batteries["BatteryQuantity"].newValue, "BatterySize", batteries["BatterySize"].newValue,
                                            "BatteryIncluded", batteries["BatteryIncluded"].newValue, "BatteriesFor", "Remote");
                    history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Created: " + newHistory);
                }
            }
            else
            {
                if (oldValue)
                {
                    string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
                    history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Deleted: " + oldHistory);

                    string sql = "DELETE FROM Batteries WHERE PartId = @P0 AND BatteriesFor = 'Remote'";
                    pdm.ExecuteCommand(sql, partId);
                }
            }


        }






        public DataTable PackagingComponent_Load(int recordId)
        {
            string sql = "SELECT * FROM vwPartPackagingComponents WHERE Id = @P0";
            DataTable dt = pdm.ExecuteReader(sql, recordId);
            dt.TableName = "vwPartPackagingComponents";
            return dt;
        }

        public void PackagingComponent_Save(string userName, int recordId, int partId, object parent)
        {
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);

            string description = (string)tables["vwPartPackagingComponents"].Fields["Description"].newValue;
            tables["vwPartPackagingComponents"].AddField("Part_Id", null, partId);
            tables["vwPartPackagingComponents"].AddField("ModifiedDate", null, DateTime.Now);

            History history = new History(entityId, environment);
            if (recordId == -1)
            {
                tables["vwPartPackagingComponents"].AddField("CreatedDate", null, DateTime.Now);
                history.Log(partId, userName, "Product Development.Specifications", "Packaging Component", string.Empty, "Created Component - " + description);
                pdm.InsertRecord("PartPackagingComponents", "Id", tables["vwPartPackagingComponents"].GetParametersAsArray(true));
            }
            else
            {
                history.Log(partId, userName, "Packaging Component - " + description, tables["vwPartPackagingComponents"].Fields, "Part_Id", "ModifiedDate", "CreatedDate");
                pdm.UpdateRecord("PartPackagingComponents", "Id", recordId, tables["vwPartPackagingComponents"].GetParametersAsArray(true, "Part_Id"));
            }
        }

        public void PackagingComponent_Delete(string userName, int recordId, int partId, string description)
        {
            History history = new History(entityId, environment);
            history.Log(partId, userName, "Packaging Component", description, string.Empty, "Deleted Component");
            string sql = "DELETE FROM PartPackagingComponents WHERE id = @P0";
            pdm.ExecuteCommand(sql, recordId);

        }

        public DataTable VendorPriceBreak_Load(int recordId)
        {
            string sql = "SELECT * FROM vwVendorPriceBreaks WHERE Id = @P0";
            DataTable dt = pdm.ExecuteReader(sql, recordId);
            dt.TableName = "vwVendorPriceBreaks";
            return dt;
        }

        public void VendorPriceBreak_Save(string userName, int recordId, int partId, object parent)
        {
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);
            TableFields table = tables["vwVendorPriceBreaks"];

            VendorPriceBreak_Save(userName, recordId, partId, table);
        }

        private void VendorPriceBreak_Save(string userName, int recordId, int partId, TableFields table)
        {
            Dictionary<string, FieldChange> fields = table.Fields;

            StringBuilder log = new StringBuilder();
            log.AppendFormat("{0}<br />Vendor: {1}<br />Vendor Part Id: {2}<br />", (recordId == -1 ? "Created Price Break" : "Updated Price Break"), fields["VendorId"].newHistory, fields["VendorPartId"].newHistory);
            for (int index = 1; index <= 5; index++)
            {
                log.AppendFormat("Break {0}: {1} - {2}<br />", index, fields["Quantity" + index.ToString()].newHistory, fields["Price" + index.ToString()].newHistory);
            }
            History history = new History(entityId, environment);
            history.Log(partId, userName, "Product Development.Vendor Price", "Price Breaks", string.Empty, log.ToString());

            table.AddField("PartVendorPrice_PartId", null, partId);
            table.AddField("ModifiedDate", null, DateTime.Now);

            if (recordId == -1)
            {
                table.AddField("CreatedDate", null, DateTime.Now);
                pdm.InsertRecord("VendorPriceBreaks", "Id", table.GetParametersAsArray(true));
            }
            else
            {
                pdm.UpdateRecord("VendorPriceBreaks", "Id", recordId, table.GetParametersAsArray(true));
            }
        }

        public void VendorPriceBreak_Delete(string userName, int recordId, int partId)
        {
            DataSet ds = LoadPart(partId, "vwVendorPriceBreaks");
            DataTable dt = ds.Tables["vwVendorPriceBreaks"];
            DataRow[] arr = dt.Select("Id = " + recordId.ToString());
            if (arr.Length == 0) return;

            DataRow dr = arr[0];
            StringBuilder log = new StringBuilder();
            log.AppendFormat("Deleted Price Break<br />Vendor Id: {0}<br />Vendor Part Id: {1}<br />", pdm.GetValue<string>(dr, "VendorId", string.Empty), pdm.GetValue<string>(dr, "VendorPartId", string.Empty));
            for (int index = 1; index <= 5; index++)
            {
                log.AppendFormat("Break {0}: {1} - {2}<br />", index, pdm.GetValue<string>(dr, "Quantity" + index.ToString(), ""), pdm.GetValue<string>(dr, "Price" + index.ToString() + "Text", ""));
            }

            History history = new History(entityId, environment);
            history.Log(partId, userName, "Product Development.Vendor Price", "Price Breaks", string.Empty, log.ToString());

            string sql = "DELETE FROM VendorPriceBreaks WHERE Id = @P0";
            pdm.ExecuteCommand(sql, recordId);
        }

    
    
    
    }

 
}