using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.UI.WebControls;
using Telerik.Web.UI;

namespace YourCompany.PDM2019.Components
{
    enum PDMTab
    {
        ProductDevelopment = 0,
        Purchasing = 1,
        QualityAssurance = 2,
        Creative = 3,
        Sales = 4,
        Marketing = 5
    }

    public partial class PDM2019Controller
    {
        protected string environment { get; set; }
        protected string entityId { get; set; }

        protected SvcLibrary.DBHandler pdm { get; set; }
        protected SvcLibrary.DBHandler vmfg { get; set; }

        public PDM2019Controller(string EntityId, string Environment)
        {
            Initialize(EntityId, Environment);
        }

        public virtual DataSet LoadPart(int partId, params string[] tables) { return new DataSet();  }




        public void PopulateForm(object parent, DataTable dt)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dt.Copy());
            PopulateForm(parent, ds);
        }

        public void PopulateForm(object parent, DataSet ds)
        {
            string table = string.Empty;
            string field = string.Empty;
            DataTable dt = null;

            foreach (Control child in ((Control)parent).Controls)
            {
                switch (child.GetType().Name)
                {
                    case "TextBox":
                        TextBox txt = (TextBox)child;
                        table = txt.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = txt.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                dt = ds.Tables[table];
                                string origValue = null;
                                if (dt.Rows.Count > 0) origValue = pdm.GetValue<string>(dt.Rows[0], field);
                                txt.Text = origValue;
                                txt.Attributes.Add("Original", origValue);
                            }
                        }
                        break;
                    case "CheckBox":
                        CheckBox chk = (CheckBox)child;
                        table = chk.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = chk.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                dt = ds.Tables[table];
                                bool? orgValue = null;

                                if (dt.Rows.Count > 0)
                                {
                                    if (!dt.Rows[0].IsNull(field))
                                    {
                                        orgValue = pdm.GetValue<bool>(dt.Rows[0], field, false);
                                    }
                                }

                                if (orgValue != null) chk.Checked = (bool)orgValue;
                                if (orgValue != null) chk.Attributes.Add("Original", orgValue.ToString());
                            }
                        }
                        break;
                    case "DropDownList":
                        DropDownList ddl = (DropDownList)child;
                        table = ddl.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = ddl.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                string selectedValue = null;
                                dt = ds.Tables[table];
                                if (dt.Rows.Count > 0) selectedValue = pdm.GetValue<string>(dt.Rows[0], field);
                                try { ddl.SelectedValue = selectedValue; }
                                catch { ddl.SelectedIndex = 0; }


                                ddl.Attributes.Add("Original", selectedValue);
                            }
                        }
                        break;
                    case "RadioButtonList":
                        RadioButtonList rdo = (RadioButtonList)child;
                        table = rdo.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = rdo.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                string selectedValue = null;
                                dt = ds.Tables[table];
                                if (dt.Rows.Count > 0) selectedValue = pdm.GetValue<string>(dt.Rows[0], field);
                                try { rdo.SelectedValue = selectedValue; }
                                catch { rdo.SelectedIndex = 0; }


                                rdo.Attributes.Add("Original", selectedValue);
                            }
                        }
                        break;
                    case "DNNTextSuggest":
                        DNNTextSuggest suggest = (DNNTextSuggest)child;
                        table = suggest.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = suggest.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                string target = suggest.Attributes["TargetField"];
                                string fullText = null;
                                dt = ds.Tables[table];
                                if (dt.Rows.Count > 0) fullText = pdm.GetValue<string>(dt.Rows[0], field);
                                try { suggest.Text = fullText; }
                                catch { suggest.Text = string.Empty; }


                                if (dt.Rows.Count > 0) suggest.Attributes.Add("Original", pdm.GetValue<string>(dt.Rows[0], target));
                            }
                        }
                        break;
                    case "DataList":
                        DataList lst = (DataList)child;
                        table = lst.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            lst.DataSource = ds.Tables[table];
                            lst.DataBind();
                        }
                        break;
                    case "RadDatePicker":
                        RadDatePicker date = (RadDatePicker)child;
                        table = date.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = date.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                dt = ds.Tables[table];
                                DateTime? origValue = null;
                                if (dt.Rows.Count > 0) origValue = pdm.GetValue<DateTime>(dt.Rows[0], field, DateTime.MinValue);
                                if ((origValue != null) && (origValue != DateTime.MinValue))
                                {
                                    date.DbSelectedDate = origValue;
                                    date.Attributes.Add("Original", ((DateTime)origValue).ToString("MM/dd/yyyy"));
                                }
                            }
                        }
                        break;

                }
                PopulateForm(child, ds);
            }
        }


        protected void ParseForm(object parent, Dictionary<string, TableFields> tables)
        {

            foreach (Control child in ((Control)parent).Controls)
            {
                string table = string.Empty;
                string field = string.Empty;
                string targetField = string.Empty;
                string oldValue = string.Empty;
                string newValue = string.Empty;

                switch (child.GetType().Name)
                {
                    case "TextBox":
                        TextBox txt = (TextBox)child;
                        table = txt.Attributes["Table"];
                        field = txt.Attributes["Field"];
                        targetField = txt.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());

                            newValue = txt.Text.Trim();
                            string origValue = txt.Attributes["Original"];
                            string format = txt.Attributes["Format"];

                            switch (format)
                            {
                                case "Number":
                                    int? intOrig = null;
                                    int? intNew = null;
                                    int test = 0;
                                    if (Int32.TryParse(origValue, out test)) intOrig = test;
                                    if (Int32.TryParse(newValue, out test)) intNew = test;
                                    tables[table].AddField(field, intOrig, intNew);
                                    break;
                                case "Decimal":
                                    decimal? decOrig = null;
                                    decimal? decNew = null;
                                    decimal decTest = 0;
                                    if (Decimal.TryParse(origValue, out decTest)) decOrig = decTest;
                                    if (Decimal.TryParse(newValue, out decTest)) decNew = decTest;
                                    FieldChange decField = new FieldChange(decOrig, decNew);
                                    tables[table].AddField(field, decOrig, decNew);
                                    break;
                                default:
                                    string maxLength = txt.Attributes["MaxLength"];
                                    if (!string.IsNullOrEmpty(maxLength))
                                    {
                                        int maxLen = -1;
                                        Int32.TryParse(maxLength, out maxLen);
                                        if (maxLen > 0 && newValue.Length > maxLen) newValue = newValue.Substring(0, maxLen);
                                    }
                                    if (string.IsNullOrEmpty(newValue)) newValue = null;
                                    tables[table].AddField(field, txt.Attributes["Original"], newValue);
                                    break;
                            }
                        }


                        break;
                    case "DropDownList":
                        DropDownList ddl = (DropDownList)child;
                        table = ddl.Attributes["Table"];
                        field = ddl.Attributes["Field"];
                        targetField = ddl.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            newValue = ddl.SelectedValue;
                            if (string.IsNullOrEmpty(newValue)) newValue = null;
                            tables[table].AddField(field, ddl.Attributes["Original"], newValue);
                        }
                        break;
                    case "RadioButtonList":
                        RadioButtonList rdo = (RadioButtonList)child;
                        table = rdo.Attributes["Table"];
                        field = rdo.Attributes["Field"];
                        targetField = rdo.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            newValue = rdo.SelectedValue;
                            if (string.IsNullOrEmpty(newValue)) newValue = null;
                            tables[table].AddField(field, rdo.Attributes["Original"], newValue);
                        }
                        break;
                    case "CheckBox":
                        CheckBox chk = (CheckBox)child;
                        table = chk.Attributes["Table"];
                        field = chk.Attributes["Field"];
                        targetField = chk.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            bool newVal = chk.Checked;
                            bool? oldVal = null;
                            string rawVal = chk.Attributes["Original"];
                            if (!string.IsNullOrEmpty(rawVal))
                            {
                                bool test = false;
                                bool.TryParse(rawVal, out test);
                                oldVal = test;
                            }
                            tables[table].AddField(field, oldVal, newVal);
                        }
                        break;
                    case "DNNTextSuggest":
                        DNNTextSuggest suggest = (DNNTextSuggest)child;
                        table = suggest.Attributes["Table"];
                        field = suggest.Attributes["Field"];
                        targetField = suggest.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            string oldVal = suggest.Attributes["Original"];
                            string idVal = string.Empty;
                            string nameVal = string.Empty;
                            string rawVal = suggest.Text.Trim();
                            if (!string.IsNullOrEmpty(rawVal)) DNNTextSuggestValues(rawVal, out idVal, out nameVal, '-');
                            if (string.IsNullOrEmpty(idVal)) idVal = null;
                            tables[table].AddField(field, oldVal, idVal);
                        }
                        break;
                    case "RadDatePicker":
                        RadDatePicker dt = (RadDatePicker)child;
                        table = dt.Attributes["Table"];
                        field = dt.Attributes["Field"];
                        targetField = dt.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            DateTime? newDate = dt.SelectedDate;
                            DateTime? oldDate = null;
                            string rawVal = dt.Attributes["Original"];
                            if (!string.IsNullOrEmpty(rawVal))
                            {
                                DateTime test = new DateTime();
                                if (DateTime.TryParse(rawVal, out test)) oldDate = test;
                            }
                            tables[table].AddField(field, oldDate, newDate);
                        }
                        break;

                }
                ParseForm(child, tables);
            }
        }


        private void DNNTextSuggestValues(string raw, out string idValue, out string nameValue, char split)
        {
            idValue = string.Empty;
            nameValue = string.Empty;

            raw = raw.Trim();
            if (string.IsNullOrEmpty(raw)) return;



            if (raw.EndsWith("]"))
            {
                int strPos = raw.LastIndexOf("[");
                idValue = raw.Substring(strPos + 1).Replace("]", "").Trim();
                nameValue = raw.Substring(0, strPos).Trim();
                return;
            }
            else if (raw.IndexOf(split) >= 0)
            {
                string newSplit = " " + split + " ";
                string[] arr = raw.Split(new string[] { newSplit }, 2, StringSplitOptions.RemoveEmptyEntries);
                //string[] arr = raw.Split(new char[] { split }, 2);
                idValue = arr[0].Trim();
                nameValue = arr[1].Trim();
                return;
            }

            //assumption
            idValue = raw;
            nameValue = string.Empty;

        }

        public void StripAllErrors(object parent)
        {
            foreach (Control child in ((Control)parent).Controls)
            {
                StripErrorStyles(child);
                StripAllErrors(child);
            }
        }

        public void StripErrorStyles(object obj)
        {
            switch (obj.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddl = (DropDownList)obj;
                    ddl.Style.Remove("border-color");
                    ddl.Style.Remove("border-style");
                    ddl.Style.Remove("border-width");
                    break;
                case "TextBox":
                    TextBox txt = (TextBox)obj;
                    txt.Style.Remove("border-color");
                    txt.Style.Remove("border-style");
                    txt.Style.Remove("border-width");
                    break;
                case "DNNTextSuggest":
                    DNNTextSuggest suggest = (DNNTextSuggest)obj;
                    suggest.Style.Remove("border-color");
                    suggest.Style.Remove("border-style");
                    suggest.Style.Remove("border-width");
                    break;
                case "RadioButtonList":
                    RadioButtonList rdo = (RadioButtonList)obj;
                    rdo.Style.Remove("border-color");
                    rdo.Style.Remove("border-style");
                    rdo.Style.Remove("border-width");
                    break;

            }
        }

        public decimal? ValidateDecimal(TextBox txt, bool required, string fieldName, ref string msg)
        {
            txt.Style.Remove("border-color");
            txt.Style.Remove("border-style");
            txt.Style.Remove("border-width");

            string raw = txt.Text.Trim();
            if (string.IsNullOrEmpty(raw) && required)
            {
                msg += string.Format("{0} is required.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            if (string.IsNullOrEmpty(raw)) return null;

            decimal test = -1;
            if (!Decimal.TryParse(raw, out test))
            {
                msg += string.Format("{0} has an invalid decimal value.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            return test;
        }

        public int? ValidateNumber(TextBox txt, bool required, string fieldName, ref string msg)
        {
            txt.Style.Remove("border-color");
            txt.Style.Remove("border-style");
            txt.Style.Remove("border-width");

            string raw = txt.Text.Trim();
            if (string.IsNullOrEmpty(raw) && required)
            {
                msg += string.Format("{0} is required.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            if (string.IsNullOrEmpty(raw)) return null;

            int test = -1;
            if (!Int32.TryParse(raw, out test))
            {
                msg += string.Format("{0} has an invalid numeric value.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            return test;
        }

        public string ValidateText(object obj, bool required, string fieldName, ref string msg)
        {
            string raw = string.Empty;

            switch (obj.GetType().Name)
            {
                case "TextBox":
                    TextBox txt = (TextBox)obj;
                    txt.Style.Remove("border-color");
                    txt.Style.Remove("border-width");
                    txt.Style.Remove("border-style");
                    raw = txt.Text.Trim();
                    if (string.IsNullOrEmpty(raw)) raw = null;
                    if (string.IsNullOrEmpty(raw) && required)
                    {
                        msg += string.Format("{0} is required.\r\n", fieldName);
                        txt.Style.Add("border-color", "red");
                        txt.Style.Add("border-width", "1px");
                        txt.Style.Add("border-style", "solid");
                    }
                    break;
                case "DropDownList":
                    DropDownList ddl = (DropDownList)obj;
                    ddl.Style.Remove("border-color");
                    ddl.Style.Remove("border-width");
                    ddl.Style.Remove("border-style");
                    raw = ddl.Text.Trim();
                    if (string.IsNullOrEmpty(raw)) raw = null;
                    if (string.IsNullOrEmpty(raw) && required)
                    {
                        msg += string.Format("{0} is required.\r\n", fieldName);
                        ddl.Style.Add("border-color", "red");
                        ddl.Style.Add("border-width", "1px");
                        ddl.Style.Add("border-style", "solid");
                    }
                    break;
                case "DNNTextSuggest":
                    DNNTextSuggest suggest = (DNNTextSuggest)obj;
                    suggest.Style.Remove("border-color");
                    suggest.Style.Remove("border-width");
                    suggest.Style.Remove("border-style");
                    raw = suggest.Text.Trim();
                    if (string.IsNullOrEmpty(raw)) raw = null;
                    if (string.IsNullOrEmpty(raw) && required)
                    {
                        msg += string.Format("{0} is required.\r\n", fieldName);
                        suggest.Style.Add("border-color", "red");
                        suggest.Style.Add("border-width", "1px");
                        suggest.Style.Add("border-style", "solid");
                    }
                    break;
                case "RadioButtonList":
                    RadioButtonList rdo = (RadioButtonList)obj;
                    rdo.Style.Remove("border-color");
                    rdo.Style.Remove("border-width");
                    rdo.Style.Remove("border-style");
                    raw = rdo.SelectedValue;
                    if (string.IsNullOrEmpty(raw)) raw = null;
                    if (string.IsNullOrEmpty(raw) && required)
                    {
                        msg += string.Format("{0} is required.\r\n", fieldName);
                        rdo.Style.Add("border-color", "red");
                        rdo.Style.Add("border-width", "1px");
                        rdo.Style.Add("border-style", "solid");
                    }
                    break;

            }
            return raw;
        }

        public DateTime? ValidateDate(RadDatePicker txt, bool required, string fieldName, ref string msg)
        {
            txt.Style.Remove("border-color");
            txt.Style.Remove("border-style");
            txt.Style.Remove("border-width");


            DateTime? raw = txt.SelectedDate;
            if ((raw == null) && required)
            {
                msg += string.Format("{0} is required.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            if (!string.IsNullOrEmpty(txt.InvalidTextBoxValue))
            {
                msg += string.Format("{0} has an invalid date value.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            return raw;
        }
    }
}