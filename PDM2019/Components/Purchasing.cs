﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;

namespace YourCompany.PDM2019.Components
{
    public class Purchasing : PDMController
    {
        History history { get; set; }

        public Purchasing(string EntityId, string Environment) : base (EntityId, Environment) 
        {
            history = new History(EntityId, Environment);
        }

    
        public override DataSet LoadPart(int partId, params string[] tables)
        {
            //dependent upon Product Development, soo...
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(partId);
                                                            
            if (tables.Length == 0) tables = new string[] { "PartVendorPrices", "vwVendorPriceBreaks", "vwPartInfos", "vwPurchasingPartInfos", "vwPartContainers", "vwPurchasingMolds", "vwPartSpecifications"};

            string tmpl8 = "SELECT * FROM {0} WHERE {1} = @P0";

            foreach (string table in tables)
            {
                if (!(ds.Tables.Contains(table)))
                {
                    string key = (table.Equals("Parts")) ? "Id" : "PartId";
                    string sql = string.Format(tmpl8, table, key);

                    switch (table)
                    {
                        case "vwBatteries":
                            sql += " AND BatteriesFor = 'Part' ORDER BY RecordId";
                            break;
                        default:
                            break;
                    }

                    DataTable dt = pdm.ExecuteReader(sql, partId);
                    dt.TableName = table;
                    ds.Tables.Add(dt);
                }
            }

            //string sku = "jtrtmp";
            //DataTable parts = ds.Tables["Parts"];
            //if (parts != null)
            //{
            //    if (parts.Rows.Count > 0) sku = pdm.GetValue<string>(parts.Rows[0], "PartId", sku);
            //}

            //string partTable = entityId.Equals("LRH") ? "VMLREI.LEARNINGRESOURCES.COM" : "h2mvmfg.learningresources.com";
            //string qry = "SELECT 1 AS UNIT_QTY, E.LENGTH AS UNIT_LENGTH, E.WIDTH AS UNIT_WIDTH, E.HEIGHT AS UNIT_HEIGHT, S.WEIGHT AS UNIT_WEIGHT, "
            //           + "P.CASE_QTY, C.LENGTH AS CASE_LENGTH, C.WIDTH AS CASE_WIDTH, C.HEIGHT AS CASE_HEIGHT, S.CASE_WEIGHT, PLT.PACK_QTY AS PALLET_QTY, "
            //           + "PLT.LENGTH AS PALLET_LENGTH, PLT.WIDTH AS PALLET_WIDTH, PLT.HEIGHT AS PALLET_HEIGHT, NVL(C.LENGTH, 0) * NVL(C.WIDTH, 0) * NVL(C.HEIGHT, 0) / 12 AS CPQ "
            //           + "FROM SKU S LEFT JOIN Part@" + partTable + " P ON S.ID = P.ID LEFT JOIN SKU_PACK E ON E.SKU_ID = S.ID AND E.PACK_ID = 'EA' "
            //           + "LEFT JOIN SKU_PACK C ON C.SKU_ID = S.ID AND C.PACK_ID = 'Cases' LEFT JOIN SKU_PACK PLT ON PLT.SKU_ID = S.ID AND PLT.PACK_ID = 'Pallets' "
            //           + "WHERE S.ID = @P0";
            //DataTable dcmsData = dcms.ExecuteReader(qry, sku);
            //dcmsData.TableName = "DCMS";
            //ds.Tables.Add(dcmsData);

            return ds;

            
        }

        //public void PopulateHTSCodes(DropDownList ddl)
        //{
        //    string sql = "SELECT HTS_CODE AS DISPLAY_VALUE, HTS_CODE || ' - ' || DESCRIPTION AS DISPLAY_TEXT FROM INFO_HTS "
        //               + "WHERE HTS_CODE IS NOT NULL ORDER BY HTS_CODE";
        //    DataTable dt = vmfg.ExecuteReader(sql);

        //    DataRow dr = dt.NewRow();
        //    dr["DISPLAY_VALUE"] = "";
        //    dr["DISPLAY_TEXT"] = "HTS Code";
        //    dt.Rows.InsertAt(dr, 0);

        //    ddl.DataTextField = "DISPLAY_TEXT";
        //    ddl.DataValueField = "DISPLAY_VALUE";
        //    ddl.DataSource = dt;
        //    ddl.DataBind();
        //}

        //public void PopulateWarehouse(DropDownList ddl, string noSelection)
        //{
        //    string sql = "SELECT ID AS DISPLAY_VALUE, DESCRIPTION AS DISPLAY_TEXT FROM WAREHOUSE ORDER BY DESCRIPTION";
        //    DataTable dt = vmfg.ExecuteReader(sql);

        //    DataRow dr = dt.NewRow();
        //    dr["DISPLAY_VALUE"] = "";
        //    dr["DISPLAY_TEXT"] = noSelection;
        //    dt.Rows.InsertAt(dr, 0);

        //    ddl.DataTextField = "DISPLAY_TEXT";
        //    ddl.DataValueField = "DISPLAY_VALUE";
        //    ddl.DataSource = dt;
        //    ddl.DataBind();
        //}

        //public void PopulateProductStatus(DropDownList ddl)
        //{
        //    string sql = "SELECT DISTINCT PRODUCT_STATUS AS DISPLAY_VALUE, PRODUCT_STATUS AS DISPLAY_TEXT FROM PART "
        //               + "WHERE PRODUCT_STATUS IS NOT NULL ORDER BY PRODUCT_STATUS";
        //    DataTable dt = vmfg.ExecuteReader(sql);

        //    DataRow dr = dt.NewRow();
        //    dr["DISPLAY_VALUE"] = "";
        //    dr["DISPLAY_TEXT"] = "Product Status";
        //    dt.Rows.InsertAt(dr, 0);

        //    ddl.DataTextField = "DISPLAY_TEXT";
        //    ddl.DataValueField = "DISPLAY_VALUE";
        //    ddl.DataSource = dt;
        //    ddl.DataBind();
        //}

        //public void PopulateCountry(DropDownList ddl, string noSelection)
        //{
        //    string sql = "SELECT UPPER(DESCRIPTION) AS DISPLAY_TEXT, UPPER(DESCRIPTION) AS DISPLAY_VALUE FROM COUNTRY ORDER BY DESCRIPTION";
        //    DataTable dt = vmfg.ExecuteReader(sql);

        //    DataRow dr = dt.NewRow();
        //    dr["DISPLAY_VALUE"] = "";
        //    dr["DISPLAY_TEXT"] = noSelection;
        //    dt.Rows.InsertAt(dr, 0);

        //    ddl.DataTextField = "DISPLAY_TEXT";
        //    ddl.DataValueField = "DISPLAY_VALUE";
        //    ddl.DataSource = dt;
        //    ddl.DataBind();
        //}

        //public void PopulateCommodityCode(DropDownList ddl)
        //{
        //    string sql = "select DISTINCT COMMODITY_CODE AS DISPLAY_VALUE, COMMODITY_CODE AS DISPLAY_TEXT FROM PART WHERE COMMODITY_CODE IS NOT NULL ORDER BY COMMODITY_CODE";
        //    DataTable dt = vmfg.ExecuteReader(sql);

        //    DataRow dr = dt.NewRow();
        //    dr["DISPLAY_VALUE"] = "";
        //    dr["DISPLAY_TEXT"] = "Commodity Code";
        //    dt.Rows.InsertAt(dr, 0);

        //    ddl.DataTextField = "DISPLAY_TEXT";
        //    ddl.DataValueField = "DISPLAY_VALUE";
        //    ddl.DataSource = dt;
        //    ddl.DataBind();
        //}


        //public void PopulateClassCodes(DropDownList ddl)
        //{
        //    string sql = "select DISTINCT NMFC_CODE_ID AS DISPLAY_VALUE, NMFC_CODE_ID AS DISPLAY_TEXT FROM SKU WHERE NMFC_CODE_ID IS NOT NULL ORDER BY NMFC_CODE_ID";
        //    DataTable dt = dcms.ExecuteReader(sql);

        //    DataRow dr = dt.NewRow();
        //    dr["DISPLAY_VALUE"] = "";
        //    dr["DISPLAY_TEXT"] = "Class Code";
        //    dt.Rows.InsertAt(dr, 0);

        //    ddl.DataTextField = "DISPLAY_TEXT";
        //    ddl.DataValueField = "DISPLAY_VALUE";
        //    ddl.DataSource = dt;
        //    ddl.DataBind();
        //}
        //private void SaveMolds(string userName, int partId, DataTable molds)
        //{
        //    if (molds.Rows.Count == 0) return;

        //    foreach (DataRow dr in molds.Rows)
        //    {
        //        StringBuilder msg = new StringBuilder();

        //        bool deleted = pdm.GetValue<bool>(dr, "deleted", false);
        //        bool updated = pdm.GetValue<bool>(dr, "updated", false);
        //        if (deleted) msg.Append("Deleted Mold:<br />");

        //        string raw = pdm.GetValue<string>(dr, "RecordId", string.Empty);
        //        int recordId = -1;
        //        Int32.TryParse(raw, out recordId);
                
        //        int numberOfMolds = pdm.GetValue<int>(dr, "NumberOfMolds", 0);
        //        msg.AppendFormat("Number of Molds: {0}<br />", numberOfMolds);
                
        //        DateTime test = pdm.GetValue<DateTime>(dr, "DateMovedText", DateTime.MinValue);
        //        DateTime? dateMoved = null;
        //        if (test != DateTime.MinValue) dateMoved = test;
        //        msg.AppendFormat("Date Moved: {0}<br />", (dateMoved != null) ? dateMoved.Value.ToString("MM/dd/yyyy") : "");

        //        string vendorSuggest = pdm.GetValue<string>(dr, "VendorSuggest", string.Empty);
        //        string vendorId = string.Empty;
        //        string vendorName = string.Empty;
        //        DNNTextSuggestValues(vendorSuggest, out vendorId, out vendorName, '-');
        //        msg.AppendFormat("Vendor: {0}<br />", vendorSuggest);

        //        string trackingId = pdm.GetValue<string>(dr, "TrackingId", string.Empty);
        //        msg.AppendFormat("Tracking Id: {0}<br />", trackingId);

        //        decimal cost = pdm.GetValue<decimal>(dr, "Cost", 0);
        //        decimal? moldCost = null;
        //        if (cost > 0) moldCost = cost;
        //        string costText = moldCost.HasValue ? cost.ToString("$ #.00") : "";
        //        msg.AppendFormat("Cost: {0}<br />", costText);

        //        bool active = pdm.GetValue<bool>(dr, "Active", false);
        //        msg.AppendFormat("Active: {0}<br />", active.ToString());

        //        string agreementUrl = pdm.GetValue<string>(dr, "AgreementUrl", string.Empty);
        //        string agreementFile = pdm.GetValue<string>(dr, "AgreementFile", string.Empty);
        //        string agreementPath = pdm.GetValue<string>(dr, "AgreementPath", string.Empty);
        //        msg.AppendFormat("Agreement: {0}", agreementFile);

        //        if (deleted)
        //        {
        //            if (recordId > 0)
        //            {
        //                history.Log(partId, userName, "Purchasing.Part Information", "Molds", string.Empty, msg.ToString());
        //                string sql = "DELETE FROM PurchasingMolds WHERE ID = @P0";
        //                pdm.ExecuteCommand(sql, recordId);
        //            }
        //        }
        //        else
        //        {
        //            if (recordId > 0)
        //            {
        //                if (updated)
        //                {
        //                    history.Log(partId, userName, "Purchasing.Part Information", "Molds", string.Empty, "Updated Mold:<br />" + msg.ToString());
        //                    pdm.UpdateRecord("PurchasingMolds", "ID", recordId, "NumberOfMolds", numberOfMolds, "DateMoved", dateMoved, "Vendor", vendorId, "TrackingId", trackingId,
        //                                        "ModifiedDate", DateTime.Now, "Cost", moldCost, "Active", active, "AgreementFile", agreementFile, "AgreementPath", agreementPath, "AgreementUrl", agreementUrl);
        //                }
        //            }
        //            else
        //            {
        //                history.Log(partId, userName, "Purchasing.Part Information", "Molds", string.Empty, "Created Mold:<br />" + msg.ToString());
        //                pdm.InsertRecord("PurchasingMolds", "ID", "PurchasingPartInfo_PartId", partId, "NumberOfMolds", numberOfMolds, "DateMoved", dateMoved, "Vendor", vendorId, "TrackingId", trackingId,
        //                                    "ModifiedDate", DateTime.Now, "CreatedDate", DateTime.Now, "Cost", moldCost, "Active", active, "AgreementFile", agreementFile, "AgreementPath", agreementPath, "AgreementUrl", agreementUrl);
        //            }
        //        }
        //    }
        //}

    //    private void SaveContainers(string userName, int partId, DataTable containers)
    //    {
    //        if (containers.Rows.Count == 0) return;

    //        foreach (DataRow dr in containers.Rows)
    //        {
    //            StringBuilder msg = new StringBuilder();

    //            bool deleted = pdm.GetValue<bool>(dr, "deleted", false);
    //            if (deleted) msg.Append("Deleted Mold:<br />");

    //            string raw = pdm.GetValue<string>(dr, "RecordId", string.Empty);
    //            int recordId = -1;
    //            Int32.TryParse(raw, out recordId);

    //            string containerSize = pdm.GetValue<string>(dr, "ContainerSize", string.Empty);
    //            msg.AppendFormat("Container Size: {0}", containerSize);

    //            //only deletes and adds
    //            if (deleted)
    //            {
    //                if (recordId > 0)
    //                {
    //                    history.Log(partId, userName, "Purchasing.Part Information", "Container Size", string.Empty, msg.ToString());
    //                    string sql = "DELETE FROM PartContainers WHERE RecordId = @P0";
    //                    pdm.ExecuteCommand(sql, recordId);
    //                }
    //            }
    //            else
    //            {

    //                if (recordId == 0)
    //                {
    //                    history.Log(partId, userName, "Purchasing.Part Information", "Container Size", string.Empty, "Added Container Size:<br />" + msg.ToString());
    //                    pdm.InsertRecord("PartContainers", "RecordId", "PartId", partId, "ContainerSize", containerSize);
    //                }
    //            }
    //        }
    //    }

    //    public void SavePartInformation(string userName, int partId, DataTable molds, DataTable containers, object parent)
    //    {
    //        DataSet ds = LoadPart(partId, "vwPurchasingPartInfos");
    //        bool newRecord = ds.Tables["vwPurchasingPartInfos"].Rows.Count == 0;

    //        SaveMolds(userName, partId, molds);
    //        SaveContainers(userName, partId, containers);

    //        pdm.ThrowErrors = true;

    //        Dictionary<string, TableFields> tables = ParseForm(parent);

    //        //custom logic
    //        if (tables.ContainsKey("vwPartInfos"))
    //        {
    //            if (tables["vwPartInfos"].HasFieldChanged("PreferredVendor") || tables["vwPartInfos"].HasFieldChanged("BuyerUserId"))
    //            {
    //                if (tables.ContainsKey("vwPurchasingPartInfos"))
    //                {
    //                    tables["vwPurchasingPartInfos"].AddField("PreferredVendorId", tables["vwPartInfos"].GetOldValue("PreferredVendor"), tables["vwPartInfos"].GetNewValue("PreferredVendor"));
    //                    tables["vwPurchasingPartInfos"].AddField("BuyerUserId", tables["vwPartInfos"].GetOldValue("BuyerUserId"), tables["vwPartInfos"].GetNewValue("BuyerUserId"));
    //                }
    //            }
    //        }

    //        if (tables.ContainsKey("vwPartSpecifications"))
    //        {
    //            if (tables["vwPartSpecifications"].HasFieldChanged("GeneralDescription"))
    //            {
    //                if (tables.ContainsKey("vwPurchasingPartInfos"))
    //                {
    //                    tables["vwPurchasingPartInfos"].AddField("Specification", tables["vwPartSpecifications"].GetOldValue("GeneralDescription"), tables["vwPartSpecifications"].GetNewValue("GeneralDescription"));
    //                }
    //            }
    //        }

    //        SavePurchasingPartInfos(userName, partId, tables);


    //        //crap
    //        //ProductDevelopment pd = new ProductDevelopment(entityId, environment);
    //        //pd.SavePartInfos(userName, partId, tables);
    //        //pd.SavePartSpecifications(userName, partId, tables);            

    //}

        //public void Specifications_Save(string userName, int partId, object parent)
        //{
        //    DataSet ds = LoadPart(partId, "vwPartSpecifications");
        //    bool newRecord = ds.Tables["vwPartSpecifications"].Rows.Count == 0;


        //    Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
        //    ParseForm(parent, tables);
        //    TableFields table = tables["vwPartSpecifications"];
        //    table.RenameField("RemoteBatteryQuantity", "BatteryQuantity");
        //    table.RenameField("RemoteBatterySize", "BatterySize");
        //    table.RenameField("RemoteBatteryIncluded", "BatteryIncluded");

        //    Dictionary<string, FieldChange> fields = tables["vwPartSpecifications"].Fields;


        //    //custom logic
        //    //if (fields["GeneralDescription"].HasChanged())
        //    //{
        //    //    tables.Add("PartInfos", new TableFields());
        //    //    tables["PartInfos"].AddField("Specification", fields["GeneralDescription"].oldValue, fields["GeneralDescription"].newValue);
        //   //}

        //    //remote battery
        //    bool oldValue = false;
        //    bool.TryParse(fields["IncludesRemote"].oldHistory, out oldValue); //unset or false
        //    bool newValue = (bool)fields["IncludesRemote"].newValue;
        //    Dictionary<string, FieldChange> batteries = table.GetSubFields("BatteryQuantity", "BatterySize", "BatteryIncluded");
        //    if (newValue)
        //    {
        //        string newHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].newHistory, batteries["BatteryQuantity"].newHistory, (bool)batteries["BatteryIncluded"].newValue ? "Included" : "Not Included");

        //        if (oldValue)
        //        {
        //            string sql = "UPDATE Batteries SET BatteryQuantity = @P0, BatterySize = @P1, BatteryIncluded = @P2 WHERE PartId = @P3 AND BatteriesFor = 'Remote'";
        //            pdm.ExecuteCommand(sql, batteries["BatteryQuantity"].newValue, batteries["BatterySize"].newValue, batteries["BatteryIncluded"].newValue, partId);
        //            string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
        //            history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", oldHistory, newHistory);
        //        }
        //        else
        //        {
        //            pdm.InsertRecord("Batteries", "RecordId", "PartId", partId, "BatteryQuantity", batteries["BatteryQuantity"].newValue, "BatterySize", batteries["BatterySize"].newValue,
        //                                    "BatteryIncluded", batteries["BatteryIncluded"].newValue, "BatteriesFor", "Remote");
        //            history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Created: " + newHistory);
        //        }
        //    }
        //    else
        //    {
        //        if (oldValue)
        //        {
        //            string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
        //            history.Log(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Deleted: " + oldHistory);

        //            string sql = "DELETE FROM Batteries WHERE PartId = @P0 AND BatteriesFor = 'Remote'";
        //            pdm.ExecuteCommand(sql, partId);
        //        }
        //    }




        //    fields.Remove("BatteryQuantity");
        //    fields.Remove("BatterySize");
        //    fields.Remove("BatteryIncluded");
        //    fields.Add("PartId", new FieldChange(null, partId));
        //    fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));

        //    if (newRecord)
        //    {
        //        fields.Add("CreatedDate", new FieldChange(null, DateTime.Now));
        //        history.Log(partId, userName, "Product Development.Specifications", string.Empty, string.Empty, "Created Specifications");
        //        pdm.InsertRecord("PartSpecifications", "PartId", tables["vwPartSpecifications"].GetParametersAsArray(true));
        //    }
        //    else
        //    {
        //        if (tables["vwPartSpecifications"].IsDirty("ModifiedDate"))
        //        {
        //            history.Log(partId, userName, "Product Development.Specifications", tables["vwPartSpecifications"].Fields, "ModifiedDate", "CreatedDate", "PartId");
        //            pdm.UpdateRecord("PartSpecifications", "PartId", partId, tables["vwPartSpecifications"].GetParametersAsArray(true));
        //        }
        //    }


        //    //additional changes
        //    //if (tables.ContainsKey("PartInfos"))
        //    //{
        //    //    tables["PartInfos"].AddField("ModifiedDate", null, DateTime.Now);
        //    //    history.Log(partId, userName, "Product Development.Specifications", tables["PartInfos"].Fields, "ModifiedDate");
        //    //    pdm.UpdateRecord("PartInfos", "PartId", partId, tables["PartInfos"].GetParametersAsArray(true));
        //    //}
        //}

        //public void Packaging_Save(string userName, int partId, object parent)
        //{
        //    //this will never be a new record as Part must exist


        //    Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
        //    ParseForm(parent, tables);

        //    TableFields table = tables["Parts"];
        //    Dictionary<string, FieldChange> fields = table.Fields;


        //    //custom logic

        //    fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));

        //    if (table.IsDirty("ModifiedDate"))
        //    {
        //        history.Log(partId, userName, "Product Development.Packaging", fields, "ModifiedDate");
        //        pdm.UpdateRecord("Parts", "Id", partId, table.GetParametersAsArray(true));
        //    }


        //}

        //public void VendorPrice_Save(string userName, int partId, object parent)
        //{
        //    string tabName = "Production Development.Vendor Price";

        //    DataSet ds = LoadPart(partId, "PartVendorPrices");
        //    bool newRecord = ds.Tables["PartVendorPrices"].Rows.Count == 0;


        //    Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
        //    ParseForm(parent, tables);

        //    TableFields table = tables["PartVendorPrices"];
        //    Dictionary<string, FieldChange> fields = table.Fields;


        //    //custom logic
        //    fields.Add("PartId", new FieldChange(null, partId));
        //    fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));

        //    if (newRecord)
        //    {
        //        fields.Add("CreatedDate", new FieldChange(null, DateTime.Now));
        //        history.Log(partId, userName, tabName, string.Empty, string.Empty, "Created Vendor Price");
        //        pdm.InsertRecord("PartVendorPrices", "PartId", table.GetParametersAsArray(true));
        //    }
        //    else
        //    {
        //        if (table.IsDirty("ModifiedDate"))
        //        {
        //            history.Log(partId, userName, tabName, table.Fields, "ModifiedDate", "CreatedDate", "PartId");
        //            pdm.UpdateRecord("PartVendorPrices", "PartId", partId, table.GetParametersAsArray(true));
        //        }
        //    }


        //    //additional changes

        //}


        public void SavePurchasingPartInfos(string userName, int partId, Dictionary<string, TableFields> tables)
        {
            if (!tables.ContainsKey("vwPurchasingPartInfos")) return;

            TableFields table = tables["vwPurchasingPartInfos"];
            if (!table.IsDirty()) return;

            DataSet ds = LoadPart(partId, "vwPurchasingPartInfos");
            bool newPart = ds.Tables["vwPurchasingPartInfos"].Rows.Count == 0;

            history.Log(partId, userName, table.Fields);

            table.AddField("PartId", null, partId);
            table.AddField("ModifiedDate", null, DateTime.Now);

            if (newPart)
            {
                table.AddField("CreatedDate", null, DateTime.Now);
                pdm.InsertRecord("PurchasingPartInfos", "PartId", table.GetParametersAsArray(true));
            }
            else
            {
                pdm.UpdateRecord("PurchasingPartInfos", "PartId", partId, table.GetParametersAsArray(true));
            }
        }
    }

}


