using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public class PartBrowserController : Controller 
    {
        //client has already been named
        public PartBrowserController(string EntityId, string Environment) : base(EntityId, Environment) { }

        public  DataSet LoadPart(int partId, params string[] tables)
        {
            if (tables.Length == 0) tables = new string[] { "Parts", "vwPartInfos", "vwPartSpecifications", "vwBatteries", "vwPartPackagingComponents", "PartVendorPrices", "vwVendorPriceBreaks" };

            DataSet ds = new DataSet();
            string tmpl8 = "SELECT * FROM {0} WHERE {1} = @P0";

            foreach (string table in tables)
            {
                string key = (table.Equals("Parts")) ? "Id" : "PartId";
                string sql = string.Format(tmpl8, table, key);

                switch (table)
                {
                    case "vwBatteries":
                        sql += " AND BatteriesFor = 'Part' ORDER BY RecordId";
                        break;
                    default:
                        break;
                }

                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = table;
                ds.Tables.Add(dt);
            }

            string sku = "jtrtmp";
            DataTable parts = ds.Tables["Parts"];
            if (parts != null)
            {
                if (parts.Rows.Count > 0) sku = pdm.GetValue<string>(parts.Rows[0], "PartId", sku);
            }

            string partTable = entityId.Equals("LRH") ? "VMLREI.LEARNINGRESOURCES.COM" : "h2mvmfg.learningresources.com";
            string qry = "SELECT 1 AS UNIT_QTY, E.LENGTH AS UNIT_LENGTH, E.WIDTH AS UNIT_WIDTH, E.HEIGHT AS UNIT_HEIGHT, S.WEIGHT AS UNIT_WEIGHT, "
                       + "P.CASE_QTY, C.LENGTH AS CASE_LENGTH, C.WIDTH AS CASE_WIDTH, C.HEIGHT AS CASE_HEIGHT, S.CASE_WEIGHT, PLT.PACK_QTY AS PALLET_QTY, "
                       + "PLT.LENGTH AS PALLET_LENGTH, PLT.WIDTH AS PALLET_WIDTH, PLT.HEIGHT AS PALLET_HEIGHT, ROUND(NVL(C.LENGTH, 0) * NVL(C.WIDTH, 0) * NVL(C.HEIGHT, 0) / 1728, 3) AS CPQ, "
                       + "CASE WHEN COALESCE(P.USER_4, 'EMPTY') <> 'EMPTY' THEN 1 ELSE 0 END AS INNER_PACK, 'NOT YET IMPLEMENTED' AS INNER_PACK_DIMS "
                       + "FROM SKU S LEFT JOIN Part@" + partTable + " P ON S.ID = P.ID LEFT JOIN SKU_PACK E ON E.SKU_ID = S.ID AND E.PACK_ID = 'EA' "
                       + "LEFT JOIN SKU_PACK C ON C.SKU_ID = S.ID AND C.PACK_ID = 'Cases' LEFT JOIN SKU_PACK PLT ON PLT.SKU_ID = S.ID AND PLT.PACK_ID = 'Pallets' "
                       + "WHERE S.ID = @P0";

            DataTable dcmsData = dcms.ExecuteReader(qry, sku);
            dcmsData.TableName = "DCMS";
            ds.Tables.Add(dcmsData);



            return ds;

        }

        public DataSet FindPartIdBySku(string sku, out int id)
        {
            string sql = "SELECT ID FROM Parts WHERE PartId = @P0";
            id = pdm.FetchSingleValue<int>(sql, "ID", sku);
            if (id > 0) return LoadPart(id);
            return null;
        }

        protected List<string> lrh_tables = new List<string> { "ASSETS", "AWARDS", "Batteries", "BillOfMaterialsInfos", "CreativeInfos", "MarketingInfos",
                                                    "PartAssetStoreInfos", "PartContainers", "PartInfos", "PartPackagingComponents", "PartSpecifications", 
                                                    "PartVendorPrices", "PurchasingMolds", "PurchasingPartInfos" ,"QAPartInfos", "TestStandards",
                                                    "VendorPriceBreaks", "ChildParts" }; //, "SalesPartInfos" removed 3/31/20
        //protected List<string> partIgnores = new List<string> { "Status", "WasApproved", "IsLockedForRevision", "IsRevisedPart", "LoadPartRevisions", "SubmitUserId", "RejectionReason", "LoadEvenets" };

        protected List<string> h2m_tables = new List<string> { "AgeFacets", "Assets", "BillOfMaterialsInfos", "Bowker", "ChildParts", "CreativeInfos", "EtaPartInfos",
                                                    "Features", "MarketingInfos", "MarketingMerchandising", "MarketingPearsonPortal", "MarketingWeb", "MarketingWebImages",
                                                    "PartAssetStoreInfos", "PartInfos", "PartPackagingComponents", "PartSpecifications", "PartVendorPrices", "ProductManagement",
                                                    "ProductSpecifications", "PurchasingMolds", "PurchasingPartInfos", "QAPartInfos", "Skills", "TestStandards", 
                                                    "VendorPriceBreaks"}; //, "SalesPartInfos" removed 3/31/20

        protected List<string> lrh_ignores = new List<string>();
        protected List<string> h2m_ignores = new List<string> { "MarketingWeb.Image" };


        public List<int> CopyParts(DotNetNuke.Entities.Users.UserInfo info, List<int> ids)
        {
            List<int> retval = new List<int>();
            pdm.ThrowErrors = true;

            foreach(int id in ids)
            {
                retval.Add(CopyPart(info, id));
            }

            return retval;
        }

        private Dictionary<string, object> GetFieldsFromTable(DataRow dr)
        {
            Dictionary<string, object> fields = new Dictionary<string, object>();
            foreach (DataColumn col in dr.Table.Columns)
            {
                fields.Add(col.ColumnName, dr[col.ColumnName]);
            }
            return fields;
        }

        private void CopyPartFixDates(Dictionary<string, object> fields)
        {
            if (fields.ContainsKey("CreatedDate")) fields["CreatedDate"] = DateTime.Now;
            if (fields.ContainsKey("ModifiedDate")) fields["ModifiedDate"] = DateTime.Now;
        }

        private bool IsPartIdUnique(string partId)
        {
            string sql = "SELECT COUNT(*) AS THECOUNT FROM vwPartList WHERE PartId = @P0";
            int count = pdm.FetchSingleValue<int>(sql, "THECOUNT", partId);
            return (count == 0);
        }

        private void RemovePrimaryKeys(string tableName, Dictionary<string, object> fields)
        {
            string sql = "select COLUMN_NAME, TABLE_NAME from INFORMATION_SCHEMA.COLUMNS where "
                       + "COLUMNPROPERTY(object_id(TABLE_SCHEMA+'.'+TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 "
                       + "AND TABLE_NAME = @P0";
            DataTable dt = pdm.ExecuteReader(sql, tableName);
            foreach (DataRow dr in dt.Rows)
            {
                string columnName = pdm.GetValue<string>(dr, "COLUMN_NAME", string.Empty);
                if (fields.ContainsKey(columnName)) fields.Remove(columnName);
            }

        }

        private string GetForeignKeyField(string tableName)
        {
            string sql = "SELECT  obj.name AS FK_NAME, sch.name AS [schema_name], tab1.name AS [table], col1.name AS [column], "
                       + "tab2.name AS [referenced_table], col2.name AS [referenced_column] FROM sys.foreign_key_columns fkc "
                       + "INNER JOIN sys.objects obj ON obj.object_id = fkc.constraint_object_id "
                       + "INNER JOIN sys.tables tab1 ON tab1.object_id = fkc.parent_object_id "
                       + "INNER JOIN sys.schemas sch ON tab1.schema_id = sch.schema_id "
                       + "INNER JOIN sys.columns col1 ON col1.column_id = parent_column_id AND col1.object_id = tab1.object_id "
                       + "INNER JOIN sys.tables tab2 ON tab2.object_id = fkc.referenced_object_id "
                       + "INNER JOIN sys.columns col2 ON col2.column_id = referenced_column_id AND col2.object_id = tab2.object_id "
                       + "WHERE tab2.name = 'Parts' AND col2.name = 'Id' AND tab1.name = @P0";
            DataTable dt = pdm.ExecuteReader(sql, tableName);
            if (dt.Rows.Count == 0) return "PartId"; //crap they should have used standards
            DataRow dr = dt.Rows[0];
            return pdm.GetValue<string>(dr, "column", "PartId");
        }

        private object[] ConvertKeysToParameters(Dictionary<string, object> fields)
        {
            List<object> parms = new List<object>();
            foreach (string key in fields.Keys)
            {
                parms.Add(key);
                parms.Add(fields[key]);
            }
            return parms.ToArray();
        }

        private int CopyPart(DotNetNuke.Entities.Users.UserInfo info, int id)
        {
            //load part data so we can get the sku and description, if needed
            DataTable part = pdm.ExecuteReader("SELECT * FROM Parts WHERE ID = @P0", id);
            if (part.Rows.Count == 0) return -1;
            
            Dictionary<string, object>fields = GetFieldsFromTable(part.Rows[0]);
            fields["Status"] = "Draft";
            fields["WasApproved"] = false;
            fields["IsLockedForRevision"] = false;
            fields["IsRevisedPart"] = false;
            fields["LoadPartRevisions"] = false;
            fields["SubmitUserId"] = null;
            fields["RejectionReason"] = null;
            fields["LoadEvents"]= false;
            CopyPartFixDates(fields);
            RemovePrimaryKeys("Parts", fields);

            string origPartId = (string)fields["PartId"];
            string partId = origPartId;
            if (partId.Length <30) partId += ".";

            bool unique = false;
            string newId = Guid.NewGuid().ToString().ToUpper().Replace("-", "");
            int index = -1;

            while ((!unique) && (index < newId.Length) && (partId.Length <= 30))
            {
                index++;
                partId += newId.Substring(index, 1);
                unique = IsPartIdUnique(partId);
            }

            if (unique)
            {
                fields["PartId"] = partId;
            }
            else
            {
                fields["PartId"] = newId;
                string description = string.Format("Copied from {0}.{1}", origPartId, (string)fields["Description"]);
                if (description.Length > 40) description = description.Substring(0, 40);
                fields["Description"] = description;
            }

            object[] parms = ConvertKeysToParameters(fields);
            int recordId = pdm.InsertRecord("Parts", "Id", parms);

            History history = new History(entityId, environment);
            history.Log(recordId, info.Username, "Header", string.Empty, string.Empty, "Copied from Part " + origPartId);

            pdm.InsertRecord("Approvals", "PartId", "PartId", recordId, "CreatedByUserId", info.UserID, "CreatedByUserName", info.Username, "CreatedByEmail", info.Email, "CreatedDate", DateTime.Now);
            CopyPart(id, recordId);
            return recordId;

        }

        private void CopyPart(int srcId, int tgtId)
        {
            string tmpl8 = "SELECT * FROM {0} WHERE {1} = @P0";

            List<string> tables = entityId.Equals("H2M") ? h2m_tables : lrh_tables;
            foreach (string table in tables)
            {
                string foreignKey = GetForeignKeyField(table);
                if (table.Equals("ChildParts")) foreignKey = "Part_Id";
                string sql = string.Format(tmpl8, table, foreignKey);
                DataTable dt = pdm.ExecuteReader(sql, srcId);
                foreach (DataRow dr in dt.Rows)
                {
                    Dictionary<string, object> fields = GetFieldsFromTable(dr);
                    CopyPartFixDates(fields);
                    RemovePrimaryKeys(table, fields);
                    RemoveIgnores(table, fields);
                    fields[foreignKey] = tgtId;
                    object[] parms = ConvertKeysToParameters(fields);
                    pdm.InsertRecord(table, "PartId", parms);
                }
            }
        }

        private void RemoveIgnores(string table, Dictionary<string, object> fields)
        {
            List<string> ignores = entityId.Equals("H2M") ? h2m_ignores : lrh_ignores;
            foreach (string ignore in ignores)
            {
                string[] arr = ignore.Split('.');
                if (table.Equals(arr[0]))
                {
                    if (fields.ContainsKey(arr[1])) fields.Remove(arr[1]);
                }
            }
        }

        public void DeleteParts(string userName, List<int> ids)
        {
            History history = new History(entityId, environment);

            foreach (int id in ids)
            {
                history.Log(id, userName, "Header", string.Empty, string.Empty, "Deleted Part.");
                pdm.UpdateRecord("Parts", "Id", id, "ModifiedDate", DateTime.Now, "Active", false);
            }
        }



    }
}
