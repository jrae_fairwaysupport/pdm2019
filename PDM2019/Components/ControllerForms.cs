using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.UI.WebControls;
using Telerik.Web.UI;

namespace YourCompany.PDM2019.Components
{

    public partial class Controller
    {

        public DataSet PopulateForm(int partId, object parent)
        {
            DataSet ds = LoadPart(partId, parent);
            PopulateForm(parent, ds);
            return ds;
        }

        public void PopulateForm(object parent, DataTable dt)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dt.Copy());
            PopulateForm(parent, ds);
        }

        public void PopulateForm(object parent, DataSet ds)
        {
            string table = string.Empty;
            string field = string.Empty;
            DataTable dt = null;

            foreach (Control child in ((Control)parent).Controls)
            {
                switch (child.GetType().Name)
                {
                    case "TextBox":
                        TextBox txt = (TextBox)child;
                        table = txt.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = txt.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                string fmt = txt.Attributes["Format"];

                                dt = ds.Tables[table];
                                string origValue = null;
                                if (dt.Rows.Count > 0) origValue = pdm.GetValue<string>(dt.Rows[0], field);

                                if (!string.IsNullOrEmpty(fmt) && fmt.Equals("Decimal") && !string.IsNullOrEmpty(origValue))
                                {
                                    string places = txt.Attributes["Places"];

                                    if (!string.IsNullOrEmpty(places))
                                    {
                                        int iPlaces = 0;
                                        string placesFormat = "#.";
                                        if (Int32.TryParse(places, out iPlaces)) placesFormat = placesFormat.PadRight(placesFormat.Length + iPlaces, '0');

                                        decimal decTest = pdm.GetValue<decimal>(dt.Rows[0], field);
                                        origValue = decTest.ToString(placesFormat);
                                    }
                                }
                                
                                txt.Text = origValue;
                                txt.Attributes.Add("Original", origValue);
                            }
                        }
                        break;
                    case "CheckBox":
                        CheckBox chk = (CheckBox)child;
                        table = chk.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = chk.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                dt = ds.Tables[table];
                                bool? orgValue = null;

                                if (dt.Rows.Count > 0)
                                {
                                    if (!dt.Rows[0].IsNull(field))
                                    {
                                        orgValue = pdm.GetValue<bool>(dt.Rows[0], field, false);
                                    }
                                }

                                if (orgValue != null) chk.Checked = (bool)orgValue;
                                if (orgValue != null)
                                {
                                    bool isChecked = (bool)orgValue;
                                    string orgText = isChecked? chk.Attributes["CheckedValue"] : chk.Attributes["UncheckedValue"];
                                    if (orgText == null) orgText = orgValue.ToString();
                                    chk.Attributes.Add("Original", orgText); 
                                }
                            }
                        }
                        break;
                    case "DropDownList":
                        DropDownList ddl = (DropDownList)child;
                        table = ddl.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = ddl.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                string selectedValue = null;
                                dt = ds.Tables[table];
                                if (dt.Rows.Count > 0) selectedValue = pdm.GetValue<string>(dt.Rows[0], field);
                                try { ddl.SelectedValue = selectedValue; } //this used to throw errors, but now it just ignores
                                catch { ddl.SelectedIndex = 0; }

                                if (ddl.SelectedValue != selectedValue)
                                {
                                    if (string.IsNullOrEmpty(selectedValue))
                                    {
                                        ddl.SelectedIndex = 0;
                                    }
                                    else
                                    {
                                        ListItem newbie = new ListItem(selectedValue, selectedValue);
                                        if (ddl.Items.Count > 0)
                                            ddl.Items.Insert(1, newbie);
                                        else
                                            ddl.Items.Add(newbie);
                                        ddl.SelectedValue = selectedValue;
                                    }
                                }


                                ddl.Attributes.Add("Original", selectedValue);
                            }
                        }
                        break;
                    case "RadioButtonList":
                        RadioButtonList rdo = (RadioButtonList)child;
                        table = rdo.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = rdo.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                string selectedValue = null;
                                dt = ds.Tables[table];
                                if (dt.Rows.Count > 0) selectedValue = pdm.GetValue<string>(dt.Rows[0], field);
                                try { rdo.SelectedValue = selectedValue; }
                                catch { rdo.SelectedIndex = 0; }


                                rdo.Attributes.Add("Original", selectedValue);
                            }
                        }
                        break;
                    case "DNNTextSuggest":
                        DNNTextSuggest suggest = (DNNTextSuggest)child;
                        table = suggest.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = suggest.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                string target = suggest.Attributes["TargetField"];
                                string fullText = null;
                                dt = ds.Tables[table];
                                if (dt.Rows.Count > 0) fullText = pdm.GetValue<string>(dt.Rows[0], field);
                                try { suggest.Text = fullText; }
                                catch { suggest.Text = string.Empty; }


                                if (dt.Rows.Count > 0) suggest.Attributes.Add("Original", pdm.GetValue<string>(dt.Rows[0], target));
                            }
                        }
                        break;
                    case "DataList":
                        DataList lst = (DataList)child;
                        table = lst.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            lst.DataSource = ds.Tables[table];
                            lst.DataBind();
                        }
                        break;
                    case "RadDatePicker":
                        RadDatePicker date = (RadDatePicker)child;
                        table = date.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = date.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                dt = ds.Tables[table];
                                DateTime? origValue = null;
                                if (dt.Rows.Count > 0) origValue = pdm.GetValue<DateTime>(dt.Rows[0], field, DateTime.MinValue);
                                if ((origValue != null) && (origValue != DateTime.MinValue))
                                {
                                    try
                                    {
                                        date.DbSelectedDate = origValue;
                                        date.Attributes.Add("Original", ((DateTime)origValue).ToString("MM/dd/yyyy"));
                                    }
                                    catch { date.DbSelectedDate = null; }
                                }
                            }
                        }
                        break;
                    case "Label":
                        Label lbl = (Label)child;
                        string format = lbl.Attributes["Format"];
                        if (!string.IsNullOrEmpty(lbl.Attributes["Table"]))
                        {
                            dt = ds.Tables[lbl.Attributes["Table"]];
                            if (dt.Rows.Count > 0)
                            {
                                switch (format)
                                {
                                    case "Money":
                                        decimal rawDec = pdm.GetValue<decimal>(dt.Rows[0], lbl.Attributes["Field"], -1);
                                        if (rawDec != -1) lbl.Text = rawDec.ToString("$ #,0.00");
                                        break;
                                    case "Date":
                                        DateTime rawDate = pdm.GetValue<DateTime>(dt.Rows[0], lbl.Attributes["Field"], DateTime.MinValue);
                                        if (rawDate != DateTime.MinValue) lbl.Text = rawDate.ToString("MM/dd/yyyy");
                                        break;
                                    case "Decimal":
                                        decimal rawDec2 = pdm.GetValue<decimal>(dt.Rows[0], lbl.Attributes["Field"], -1);
                                        if (rawDec2 != -1)
                                        {
                                            string places = lbl.Attributes["Places"];
                                            int iPlaces = 0;
                                            string placesFormat = "#.";
                                            if (Int32.TryParse(places, out iPlaces)) placesFormat = placesFormat.PadRight(placesFormat.Length + iPlaces, '0');
                                            lbl.Text = rawDec2.ToString(placesFormat);
                                                
                                        }
                                        break;
                                    default:
                                        lbl.Text = pdm.GetValue<string>(dt.Rows[0], lbl.Attributes["Field"], string.Empty);
                                        break;
                                }
                            }
                        }

                        break;
                    case "Image":
                        Image img = (Image)child;
                        if (!string.IsNullOrEmpty(img.Attributes["Table"]))
                        {
                            dt = ds.Tables[img.Attributes["Table"]];
                            bool checkedImage = false;
                            if (dt.Rows.Count > 0) checkedImage = pdm.GetValue<bool>(dt.Rows[0], img.Attributes["Field"], false);
                            if (checkedImage)
                                img.ImageUrl = "~/images/checked.gif";
                            else
                                img.ImageUrl = "~/images/unchecked.gif";
                        }
                        break;
                    case "Panel":
                        Panel pnl = (Panel)child;
                        table = pnl.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = pnl.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                bool visible = false;
                                if (dt.Rows.Count > 0) visible = pdm.GetValue<bool>(dt.Rows[0], field, false);
                                pnl.Visible = visible;
                            }
                        }
                        break;
                    case "RadEditor":
                        RadEditor editor = (RadEditor)child;
                        table = editor.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = editor.Attributes["Field"];
                            if (ds.Tables.Contains(table))
                            {
                                dt = ds.Tables[table];
                                if (dt.Rows.Count > 0)
                                {
                                    editor.Content = pdm.GetValue<string>(dt.Rows[0], "Copy", string.Empty);
                                    editor.Attributes.Add("Original", editor.Content);
                                }
                            }
                        }
                        break;
                    case "HyperLink":
                        HyperLink lnk = (HyperLink)child;
                        table = lnk.Attributes["Table"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            field = lnk.Attributes["Field"];
                            string displayText = lnk.Attributes["Display"];
                            if (string.IsNullOrEmpty(displayText)) displayText = field;

                            if (ds.Tables.Contains(table))
                            {
                                dt = ds.Tables[table];
                                if (dt.Rows.Count > 0)
                                {
                                    string navigateUrl = pdm.GetValue<string>(dt.Rows[0], field, string.Empty);
                                    lnk.Text = pdm.GetValue<string>(dt.Rows[0], displayText, string.Empty);
                                    lnk.NavigateUrl = navigateUrl;
                                    lnk.Visible = !string.IsNullOrEmpty(lnk.Text);
                                }
                            }
                        }
                        break;
                }
                PopulateForm(child, ds);
            }
        }

        public Dictionary<string, TableFields> ParseForm(object parent)
        {
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);
            return tables;
        }

        protected void ParseTables(object parent, List<string> tables)
        {

            foreach (Control child in ((Control)parent).Controls)
            {
                string table = string.Empty;

                switch (child.GetType().Name)
                {
                    case "Label":
                        Label lbl = (Label)child;
                        table = lbl.Attributes["Table"];
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)child;
                        table = txt.Attributes["Table"];
                        break;
                    case "DropDownList":
                        DropDownList ddl = (DropDownList)child;
                        table = ddl.Attributes["Table"];
                        break;
                    case "RadioButtonList":
                        RadioButtonList rdo = (RadioButtonList)child;
                        table = rdo.Attributes["Table"];
                        break;
                    case "CheckBox":
                        CheckBox chk = (CheckBox)child;
                        table = chk.Attributes["Table"];
                        break;
                    case "DNNTextSuggest":
                        DNNTextSuggest suggest = (DNNTextSuggest)child;
                        table = suggest.Attributes["Table"];
                        break;
                    case "RadDatePicker":
                        RadDatePicker dt = (RadDatePicker)child;
                        table = dt.Attributes["Table"];
                        break;
                    case "RadEditor":
                        RadEditor editor = (RadEditor)child;
                        table = editor.Attributes["Table"];
                        break;
                    case "DataList":
                        DataList lst = (DataList)child;
                        table = lst.Attributes["Table"];
                        break;
                    case "Image":
                        Image img = (Image)child;
                        table = img.Attributes["Table"];
                        break;
                }

                if (!string.IsNullOrEmpty(table))
                {
                    if (!tables.Contains(table)) tables.Add(table);
                }
                ParseTables(child, tables);
            }
        }

        
        
        public void ParseForm(object parent, Dictionary<string, TableFields> tables)
        {

            foreach (Control child in ((Control)parent).Controls)
            {
                string table = string.Empty;
                string field = string.Empty;
                string targetField = string.Empty;
                string oldValue = string.Empty;
                string newValue = string.Empty;

                switch (child.GetType().Name)
                {
                    case "TextBox":
                        TextBox txt = (TextBox)child;
                        table = txt.Attributes["Table"];
                        field = txt.Attributes["Field"];
                        targetField = txt.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());

                            newValue = txt.Text.Trim();
                            string origValue = txt.Attributes["Original"];
                            string format = txt.Attributes["Format"];

                            switch (format)
                            {
                                case "Number":
                                    int? intOrig = null;
                                    int? intNew = null;
                                    int test = 0;
                                    if (Int32.TryParse(origValue, out test)) intOrig = test;
                                    if (Int32.TryParse(newValue, out test)) intNew = test;
                                    tables[table].AddField(field, intOrig, intNew);
                                    break;
                                case "Decimal":
                                    decimal? decOrig = null;
                                    decimal? decNew = null;
                                    decimal decTest = 0;

                                    if (Decimal.TryParse(origValue, out decTest)) decOrig = decTest;
                                    if (Decimal.TryParse(newValue, out decTest)) decNew = decTest;

                                    string places = txt.Attributes["Places"];

                                    if (!string.IsNullOrEmpty(places) && (decOrig.HasValue || decNew.HasValue))
                                    {
                                        int iPlaces = 0;
                                        string placesFormat = "#.";
                                        if (Int32.TryParse(places, out iPlaces)) placesFormat = placesFormat.PadRight(placesFormat.Length + iPlaces, '0');

                                        if (decOrig.HasValue) decOrig = Decimal.Parse(decOrig.Value.ToString(placesFormat));
                                        if (decNew.HasValue) decNew = Decimal.Parse(decNew.Value.ToString(placesFormat));
                                    }

                                    

                                    tables[table].AddField(field, decOrig, decNew);
                                    break;
                                default:
                                    string maxLength = txt.Attributes["MaxLength"];
                                    if (!string.IsNullOrEmpty(maxLength))
                                    {
                                        int maxLen = -1;
                                        Int32.TryParse(maxLength, out maxLen);
                                        if (maxLen > 0 && newValue.Length > maxLen) newValue = newValue.Substring(0, maxLen);
                                    }
                                    if (string.IsNullOrEmpty(newValue)) newValue = null;
                                    tables[table].AddField(field, txt.Attributes["Original"], newValue);
                                    break;
                            }
                        }


                        break;
                    case "DropDownList":
                        DropDownList ddl = (DropDownList)child;
                        table = ddl.Attributes["Table"];
                        field = ddl.Attributes["Field"];
                        targetField = ddl.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            newValue = ddl.SelectedValue;
                            if (string.IsNullOrEmpty(newValue)) newValue = null;
                            tables[table].AddField(field, ddl.Attributes["Original"], newValue);
                        }
                        break;
                    case "RadioButtonList":
                        RadioButtonList rdo = (RadioButtonList)child;
                        table = rdo.Attributes["Table"];
                        field = rdo.Attributes["Field"];
                        targetField = rdo.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            newValue = rdo.SelectedValue;
                            if (string.IsNullOrEmpty(newValue)) newValue = null;
                            tables[table].AddField(field, rdo.Attributes["Original"], newValue);
                        }
                        break;
                    case "CheckBox":
                        CheckBox chk = (CheckBox)child;
                        table = chk.Attributes["Table"];
                        field = chk.Attributes["Field"];
                        targetField = chk.Attributes["TargetField"];
                        string checkedValue = chk.Attributes["CheckedValue"];
                        string uncheckedValue = chk.Attributes["UncheckedValue"];

                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            if (!string.IsNullOrEmpty(checkedValue))
                            {
                                //treat this as a text radio
                                string newVal = chk.Checked ? checkedValue : uncheckedValue;
                                string oldVal = chk.Attributes["Original"];
                                tables[table].AddField(field, oldVal, newVal);
                            }
                            else
                            {
                                bool newVal = chk.Checked;
                                bool? oldVal = null;
                                string rawVal = chk.Attributes["Original"];
                                if (!string.IsNullOrEmpty(rawVal))
                                {
                                    bool test = false;
                                    bool.TryParse(rawVal, out test);
                                    oldVal = test;
                                }
                                tables[table].AddField(field, oldVal, newVal);
                            }
                        }
                        break;
                    case "DNNTextSuggest":
                        DNNTextSuggest suggest = (DNNTextSuggest)child;
                        table = suggest.Attributes["Table"];
                        field = suggest.Attributes["Field"];
                        targetField = suggest.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            string oldVal = suggest.Attributes["Original"];
                            string idVal = string.Empty;
                            string nameVal = string.Empty;
                            string rawVal = suggest.Text.Trim();
                            if (!string.IsNullOrEmpty(rawVal)) DNNTextSuggestValues(rawVal, out idVal, out nameVal, '-');
                            if (string.IsNullOrEmpty(idVal)) idVal = null;
                            tables[table].AddField(field, oldVal, idVal);
                        }
                        break;
                    case "RadDatePicker":
                        RadDatePicker dt = (RadDatePicker)child;
                        table = dt.Attributes["Table"];
                        field = dt.Attributes["Field"];
                        targetField = dt.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            DateTime? newDate = dt.SelectedDate;
                            DateTime? oldDate = null;
                            string rawVal = dt.Attributes["Original"];
                            if (!string.IsNullOrEmpty(rawVal))
                            {
                                DateTime test = new DateTime();
                                if (DateTime.TryParse(rawVal, out test)) oldDate = test;
                            }
                            tables[table].AddField(field, oldDate, newDate);
                        }
                        break;
                    case "RadEditor":
                        RadEditor editor = (RadEditor)child;
                        table = editor.Attributes["Table"];
                        field = editor.Attributes["Field"];
                        targetField = editor.Attributes["TargetField"];
                        if (!string.IsNullOrEmpty(targetField)) field = targetField;
                        if (!string.IsNullOrEmpty(field))
                        {
                            if (!tables.ContainsKey(table)) tables.Add(table, new TableFields());
                            string oldVal = editor.Attributes["Original"];
                            string newVal = editor.Content;
                            tables[table].AddField(field, oldVal, newVal);
                        }
                        break;
                }
                ParseForm(child, tables);
            }
        }

        public bool DNNTestSuggestIsSelectionValid(DNNTextSuggest txt, string fieldName, ref string msg)
        {
            StripErrorStyles(txt);

            string validation = txt.Attributes["validation"];
            if (string.IsNullOrEmpty(validation)) return true;

            string split = txt.Attributes["NameIDSplit"];
            if (string.IsNullOrEmpty(split)) split = "-";

            string raw = txt.Text.Trim();

            //leave required values for generic validation
            //only validate non-empty selections
            if (string.IsNullOrEmpty(raw)) return true;

            string idValue = string.Empty;
            string nameValue = string.Empty;
            DNNTextSuggestValues(raw, out idValue, out nameValue, split.ToCharArray()[0]);

            string dataBase = txt.Attributes["dataBase"];
            if (string.IsNullOrEmpty(dataBase)) dataBase = "PDM";
            
            int count = 0;
            switch(dataBase)
            {
                case "VMFG":
                    count = vmfg.FetchSingleValue<int>(validation, "HITS", idValue, nameValue);
                    break;
                case "DCMS":
                    count = dcms.FetchSingleValue<int>(validation, "HITS", idValue, nameValue);
                    break;
                default:
                    count = pdm.FetchSingleValue<int>(validation, "HITS", idValue, nameValue);
                    break;
            }

            if (count > 0) return true;

            txt.Style.Add("border-color", "red");
            txt.Style.Add("border-width", "1px");
            txt.Style.Add("border-style", "solid");

            msg += fieldName + " has an invalid selection. Please select a valid option from the pop-up menu that appears when typing in the field.\r\n";
            return false;
        }

        public void DNNTextSuggestValues(string raw, out string idValue, out string nameValue, char split)
        {
            idValue = string.Empty;
            nameValue = string.Empty;

            raw = raw.Trim();
            if (string.IsNullOrEmpty(raw)) return;



            if (raw.EndsWith("]"))
            {
                int strPos = raw.LastIndexOf("[");
                idValue = raw.Substring(strPos + 1).Replace("]", "").Trim();
                nameValue = raw.Substring(0, strPos).Trim();
                return;
            }
            else if (raw.IndexOf(split) >= 0)
            {
                raw += " "; //just in case, trim should deal

                string newSplit = " " + split + " ";
                string[] arr = raw.Split(new string[] { newSplit }, 2, StringSplitOptions.RemoveEmptyEntries);
                //string[] arr = raw.Split(new char[] { split }, 2);
                idValue = arr[0].Trim();
                if (arr.Length > 1) nameValue = arr[1].Trim();
                return;
            }

            //assumption
            idValue = raw;
            nameValue = string.Empty;

        }

        public void StripAllErrors(object parent)
        {
            foreach (Control child in ((Control)parent).Controls)
            {
                StripErrorStyles(child);
                StripAllErrors(child);
            }
        }

        public void StripErrorStyles(object obj)
        {
            switch (obj.GetType().Name)
            {
                case "DropDownList":
                    DropDownList ddl = (DropDownList)obj;
                    ddl.Style.Remove("border-color");
                    ddl.Style.Remove("border-style");
                    ddl.Style.Remove("border-width");
                    break;
                case "TextBox":
                    TextBox txt = (TextBox)obj;
                    txt.Style.Remove("border-color");
                    txt.Style.Remove("border-style");
                    txt.Style.Remove("border-width");
                    break;
                case "DNNTextSuggest":
                    DNNTextSuggest suggest = (DNNTextSuggest)obj;
                    suggest.Style.Remove("border-color");
                    suggest.Style.Remove("border-style");
                    suggest.Style.Remove("border-width");
                    break;
                case "RadioButtonList":
                    RadioButtonList rdo = (RadioButtonList)obj;
                    rdo.Style.Remove("border-color");
                    rdo.Style.Remove("border-style");
                    rdo.Style.Remove("border-width");
                    break;

            }
        }

        public void ApplyErrorStyles(System.Web.UI.WebControls.WebControl obj)
        {
            obj.Style.Add("border-color", "red");
            obj.Style.Add("border-width", "1px");
            obj.Style.Add("border-style", "solid");
        }

        public void ClearErrorStyles(System.Web.UI.WebControls.WebControl obj)
        {
            obj.Style.Remove("border-color");
            obj.Style.Remove("border-style");
            obj.Style.Remove("border-width");
        }


        public decimal? ValidateDecimal(TextBox txt, bool required, string fieldName, ref string msg)
        {
            txt.Style.Remove("border-color");
            txt.Style.Remove("border-style");
            txt.Style.Remove("border-width");

            string raw = txt.Text.Trim();
            if (string.IsNullOrEmpty(raw) && required)
            {
                msg += string.Format("{0} is required.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            if (string.IsNullOrEmpty(raw)) return null;

            decimal test = -1;
            if (!Decimal.TryParse(raw, out test))
            {
                msg += string.Format("{0} has an invalid decimal value.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            return test;
        }

        public int? ValidateNumber(TextBox txt, bool required, string fieldName, ref string msg)
        {
            txt.Style.Remove("border-color");
            txt.Style.Remove("border-style");
            txt.Style.Remove("border-width");

            string raw = txt.Text.Trim();
            if (string.IsNullOrEmpty(raw) && required)
            {
                msg += string.Format("{0} is required.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            if (string.IsNullOrEmpty(raw)) return null;

            int test = -1;
            if (!Int32.TryParse(raw, out test))
            {
                msg += string.Format("{0} has an invalid numeric value.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            return test;
        }

        public string ValidateText(object obj, bool required, string fieldName, ref string msg)
        {
            string raw = string.Empty;

            switch (obj.GetType().Name)
            {
                case "TextBox":
                    TextBox txt = (TextBox)obj;
                    txt.Style.Remove("border-color");
                    txt.Style.Remove("border-width");
                    txt.Style.Remove("border-style");
                    raw = txt.Text.Trim();
                    if (string.IsNullOrEmpty(raw)) raw = null;
                    if (string.IsNullOrEmpty(raw) && required)
                    {
                        msg += string.Format("{0} is required.\r\n", fieldName);
                        txt.Style.Add("border-color", "red");
                        txt.Style.Add("border-width", "1px");
                        txt.Style.Add("border-style", "solid");
                    }
                    break;
                case "DropDownList":
                    DropDownList ddl = (DropDownList)obj;
                    ddl.Style.Remove("border-color");
                    ddl.Style.Remove("border-width");
                    ddl.Style.Remove("border-style");
                    raw = ddl.Text.Trim();
                    if (string.IsNullOrEmpty(raw)) raw = null;
                    if (string.IsNullOrEmpty(raw) && required)
                    {
                        msg += string.Format("{0} is required.\r\n", fieldName);
                        ddl.Style.Add("border-color", "red");
                        ddl.Style.Add("border-width", "1px");
                        ddl.Style.Add("border-style", "solid");
                    }
                    break;
                case "DNNTextSuggest":
                    DNNTextSuggest suggest = (DNNTextSuggest)obj;
                    suggest.Style.Remove("border-color");
                    suggest.Style.Remove("border-width");
                    suggest.Style.Remove("border-style");
                    raw = suggest.Text.Trim();
                    if (string.IsNullOrEmpty(raw)) raw = null;
                    if (string.IsNullOrEmpty(raw) && required)
                    {
                        msg += string.Format("{0} is required.\r\n", fieldName);
                        suggest.Style.Add("border-color", "red");
                        suggest.Style.Add("border-width", "1px");
                        suggest.Style.Add("border-style", "solid");
                    }
                    break;
                case "RadioButtonList":
                    RadioButtonList rdo = (RadioButtonList)obj;
                    rdo.Style.Remove("border-color");
                    rdo.Style.Remove("border-width");
                    rdo.Style.Remove("border-style");
                    raw = rdo.SelectedValue;
                    if (string.IsNullOrEmpty(raw)) raw = null;
                    if (string.IsNullOrEmpty(raw) && required)
                    {
                        msg += string.Format("{0} is required.\r\n", fieldName);
                        rdo.Style.Add("border-color", "red");
                        rdo.Style.Add("border-width", "1px");
                        rdo.Style.Add("border-style", "solid");
                    }
                    break;

            }
            return raw;
        }

        public DateTime? ValidateDate(RadDatePicker txt, bool required, string fieldName, ref string msg)
        {
            txt.Style.Remove("border-color");
            txt.Style.Remove("border-style");
            txt.Style.Remove("border-width");


            DateTime? raw = txt.SelectedDate;
            if ((raw == null) && required)
            {
                msg += string.Format("{0} is required.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            if (!string.IsNullOrEmpty(txt.InvalidTextBoxValue))
            {
                msg += string.Format("{0} has an invalid date value.\r\n", fieldName);
                txt.Style.Add("border-color", "red");
                txt.Style.Add("border-style", "solid");
                txt.Style.Add("border-width", "1px");
                return null;
            }

            return raw;
        }

        public void PopulateDropDownListOptions(DropDownList ddl, string tabName, string listName, string noSelection = "")
        {
            if (string.IsNullOrEmpty(noSelection)) noSelection = listName;
            string sql = "SELECT StoredValue, DisplayText FROM vwLists WHERE TabName = @P0 AND ListName = @P1 AND Active = 1 ORDER BY DisplayText";
            DataTable dt = pdm.ExecuteReader(sql, tabName, listName);

            DataRow dr = dt.NewRow();
            dr["DisplayText"] = noSelection;
            dr["StoredValue"] = "";
            dt.Rows.InsertAt(dr, 0);

            ddl.DataValueField = "StoredValue";
            ddl.DataTextField = "DisplayText";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        public void PostApprovalLockdown(object parent)
        {
            //fuck me, I really need to put this kind of crap into a base class (ie checkbox.postapprovallockdown()) or something)

            foreach (Control child in ((Control)parent).Controls)
            {
                string table = string.Empty;
                string approved = string.Empty;

                switch (child.GetType().Name)
                {
                    case "TextBox":
                        TextBox txt = (TextBox)child;
                        table = txt.Attributes["Table"];
                        approved = txt.Attributes["Approved"];

                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved)) 
                            {
                                txt.CssClass += " readonly";
                                txt.Enabled = false;
                            }
                        }

                        break;
                    case "CheckBox":
                        CheckBox chk = (CheckBox)child;
                        table = chk.Attributes["Table"];
                        approved = chk.Attributes["Approved"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved))
                            {
                                chk.CssClass += " readonly";
                                chk.Enabled = false;
                            }
                        }break;
                    case "DropDownList":
                        DropDownList ddl = (DropDownList)child;
                        table = ddl.Attributes["Table"];
                        approved = ddl.Attributes["Approved"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved))
                            {
                                ddl.CssClass += " readonly";
                                ddl.Enabled = false;
                            }
                        }
                        break;
                    case "RadioButtonList":
                        RadioButtonList rdo = (RadioButtonList)child;
                        table = rdo.Attributes["Table"];
                        approved = rdo.Attributes["Approved"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved))
                            {
                                rdo.CssClass += " readonly";
                                rdo.Enabled = false;
                            }
                        }
                        break;
                    case "DNNTextSuggest":
                        DNNTextSuggest suggest = (DNNTextSuggest)child;
                        table = suggest.Attributes["Table"];
                        approved = suggest.Attributes["Approved"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved))
                            {
                                suggest.CssClass += " readonly";
                                suggest.Enabled = false;
                            }
                        }
                        break;
                    case "DataList":
                        DataList lst = (DataList)child;
                        table = lst.Attributes["Table"];
                        approved = lst.Attributes["Approved"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved))
                            {
                                //lst.CssClass.Append("readonly", " ");
                                lst.Enabled = false;
                            }
                        }
                        break;
                    case "RadDatePicker":
                        RadDatePicker date = (RadDatePicker)child;
                        table = date.Attributes["Table"];
                        approved = date.Attributes["Approved"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved))
                            {
                                date.CssClass += " readonly";
                                date.Enabled = false;
                            }
                        }
                        break;
                    case "Label":
                        break;
                    case "Image":
                        break;
                    case "Panel":
                        Panel pnl = (Panel)child;
                        table = pnl.Attributes["Table"];
                        approved = pnl.Attributes["Approved"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved))
                            {
                                pnl.Enabled = false;
                            }
                        }
                        break;
                    case "RadEditor":
                        RadEditor editor = (RadEditor)child;
                        table = editor.Attributes["Table"];
                        approved = editor.Attributes["Approved"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved))
                            {
                                editor.CssClass += " readonly";
                                editor.Enabled = false;
                            }
                        }
                        break;
                    case "HyperLink":
                        HyperLink lnk = (HyperLink)child;
                        table = lnk.Attributes["Table"];
                        approved = lnk.Attributes["Approved"];
                        if (!string.IsNullOrEmpty(table))
                        {
                            if (string.IsNullOrEmpty(approved))
                            {
                                //rdo.CssClass.Append("readonly", " ");
                                lnk.Enabled = false;
                            }
                        }
                        break;
                }

                PostApprovalLockdown(child);
            }
        }

    }
}