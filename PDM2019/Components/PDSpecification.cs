using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using DotNetNuke;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;
using System.Data;

namespace YourCompany.PDM2019.Components
{
    public partial class PDM2019Controller 
    {
        public void Battery_Add(string userId, int partId, string batteryFor, string size, int qty, bool included)
        {
            int recordId = pdm.InsertRecord("Batteries", "RecordId", "PartId", partId, "BatteriesFor", batteryFor, "BatterySize", size, "BatteryQuantity", qty, "BatteryIncluded", included);

            string msg = string.Format("Added {0} Battery: {0}, {1}, {2}", size, qty, (included ? "Included" : "Not Included"), batteryFor);
            HistoryLog(partId, userId, "Product Development.Specifications", "Batteries", string.Empty, msg);
        }

        public void Battery_Delete(string userId, int recordId, int partId, string batteryFor, string size, int qty, bool included)
        {
            //our workaround until we can clean up the db design
            if (recordId == -1)
            {
                //we're in the specification database
                string sql = "UPDATE PartSpecifications SET BatterySize = NULL, BatteryQuantity = NULL, BatteriesIncluded = NULL WHERE PartId = @P0";
                pdm.ExecuteCommand(sql, partId);
            }
            else
            {
                string sql = "DELETE FROM Batteries WHERE RecordId = @P0";
                pdm.ExecuteCommand(sql, recordId);
            }

            string msg = string.Format("Deleted {0} Battery: {1}, {2}, {3}", batteryFor, size, qty, (included ? "Included" : "Not Included"));
            HistoryLog(partId, userId, "Product Development.Specifications", "Batteries", string.Empty, msg);


        }

        public DataTable PackagingComponent_Load(int recordId)
        {
            string sql = "SELECT * FROM vwPartPackagingComponents WHERE Id = @P0";
            DataTable dt = pdm.ExecuteReader(sql, recordId);
            dt.TableName = "vwPartPackagingComponents";
            return dt;
        }

        public void PackagingComponent_Save(string userName, int recordId, int partId, object parent)
        {
            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);

            string description = (string)tables["vwPartPackagingComponents"].Fields["Description"].newValue;
            tables["vwPartPackagingComponents"].AddField("Part_Id", null, partId);
            tables["vwPartPackagingComponents"].AddField("ModifiedDate", null, DateTime.Now);

            if (recordId == -1)
            {
                tables["vwPartPackagingComponents"].AddField("CreatedDate", null, DateTime.Now);
                HistoryLog(partId, userName, "Product Development.Specifications", "Packaging Component", string.Empty, "Created Component - " + description);
                pdm.InsertRecord("PartPackagingComponents", "Id", tables["vwPartPackagingComponents"].GetParametersAsArray(true));
            }
            else
            {
                HistoryLog(partId, userName, "Packaging Component - " + description, tables["vwPartPackagingComponents"].Fields, "Part_Id", "ModifiedDate", "CreatedDate");
                pdm.UpdateRecord("PartPackagingComponents", "Id", recordId, tables["vwPartPackagingComponents"].GetParametersAsArray(true, "Part_Id"));
            }
        }

        public void PackagingComponent_Delete(string userName, int recordId, int partId, string description)
        {
            HistoryLog(partId, userName, "Packaging Component", description, string.Empty, "Deleted Component");
            string sql = "DELETE FROM PartPackagingComponents WHERE id = @P0";
            pdm.ExecuteCommand(sql, recordId);

        }



        public void ProductDevelopment_Specifications_Save(string userName, int partId, object parent)
        {
            DataSet ds = LoadPart(partId, "vwPartSpecifications");
            bool newRecord = ds.Tables["vwPartSpecifications"].Rows.Count == 0;
            

            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
            ParseForm(parent, tables);
            TableFields table = tables["vwPartSpecifications"];
            table.RenameField("RemoteBatteryQuantity", "BatteryQuantity");
            table.RenameField("RemoteBatterySize", "BatterySize");
            table.RenameField("RemoteBatteryIncluded", "BatteryIncluded");

            Dictionary<string, FieldChange> fields = tables["vwPartSpecifications"].Fields;
            

            //custom logic
            if (fields["GeneralDescription"].HasChanged())
            {
                tables.Add("PartInfos", new TableFields());
                tables["PartInfos"].AddField("Specification", fields["GeneralDescription"].oldValue, fields["GeneralDescription"].newValue);
            }

            //remote battery
            bool oldValue = false;
            bool.TryParse(fields["IncludesRemote"].oldHistory, out oldValue); //unset or false
            bool newValue = (bool)fields["IncludesRemote"].newValue;
            Dictionary<string, FieldChange> batteries = table.GetSubFields("BatteryQuantity", "BatterySize", "BatteryIncluded");
            if (newValue)
            {
                string newHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].newHistory, batteries["BatteryQuantity"].newHistory, (bool)batteries["BatteryIncluded"].newValue ? "Included" : "Not Included");

                if (oldValue)
                {
                    string sql = "UPDATE Batteries SET BatteryQuantity = @P0, BatterySize = @P1, BatteryIncluded = @P2 WHERE PartId = @P3 AND BatteriesFor = 'Remote'";
                    pdm.ExecuteCommand(sql, batteries["BatteryQuantity"].newValue, batteries["BatterySize"].newValue, batteries["BatteryIncluded"].newValue, partId);
                    string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
                    HistoryLog(partId, userName, "Product Development.Specifications", "Remote Battery", oldHistory, newHistory);
                }
                else
                {
                    pdm.InsertRecord("Batteries", "RecordId", "PartId", partId, "BatteryQuantity", batteries["BatteryQuantity"].newValue, "BatterySize", batteries["BatterySize"].newValue,
                                            "BatteryIncluded", batteries["BatteryIncluded"].newValue, "BatteriesFor", "Remote");
                    HistoryLog(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Created: " + newHistory);
                }
            }
            else
            {
                if (oldValue)
                {
                    string oldHistory = string.Format("{0}, {1}, {2}", batteries["BatterySize"].oldHistory, batteries["BatteryQuantity"].oldHistory, (bool)batteries["BatteryIncluded"].oldValue ? "Included" : "Not Included");
                    HistoryLog(partId, userName, "Product Development.Specifications", "Remote Battery", "", "Deleted: " + oldHistory);

                    string sql = "DELETE FROM Batteries WHERE PartId = @P0 AND BatteriesFor = 'Remote'";
                    pdm.ExecuteCommand(sql, partId);
                }
            }




            fields.Remove("BatteryQuantity");
            fields.Remove("BatterySize");
            fields.Remove("BatteryIncluded");
            fields.Add("PartId", new FieldChange(null, partId));
            fields.Add("ModifiedDate", new FieldChange(null, DateTime.Now));

            if (newRecord)
            {
                fields.Add("CreatedDate", new FieldChange(null, DateTime.Now));
                HistoryLog(partId, userName, "Product Development.Specifications", string.Empty, string.Empty, "Created Specifications");
                pdm.InsertRecord("PartSpecifications", "PartId", tables["vwPartSpecifications"].GetParametersAsArray(true));
            }
            else
            {
                if (tables["vwPartSpecifications"].IsDirty("ModifiedDate"))
                {
                    HistoryLog(partId, userName, "Product Development.Specifications", tables["vwPartSpecifications"].Fields, "ModifiedDate", "CreatedDate", "PartId");
                    pdm.UpdateRecord("PartSpecifications", "PartId", partId, tables["vwPartSpecifications"].GetParametersAsArray(true));
                }
            }


            //additional changes
            if (tables.ContainsKey("PartInfos"))
            {
                tables["PartInfos"].AddField("ModifiedDate", null, DateTime.Now);
                HistoryLog(partId, userName, "Product Development.Specifications", tables["PartInfos"].Fields, "ModifiedDate");
                pdm.UpdateRecord("PartInfos", "PartId", partId, tables["PartInfos"].GetParametersAsArray(true));
            }
        }

    }
}
