using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class SidebarEditor : LRHPDMPopup //: PortalModuleBase, IActionable
    {
        protected override string PAGE_TITLE { get { return "PDM Part Header Editor"; } }

        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);

            if (!Page.IsPostBack && !partStatus.Equals("Draft")) labelUPC.CssClass = "SubSubHead required";
        }

        protected override void LoadData()
        {
            SideBarController controller = new SideBarController(entityId, environment);
            DataTable dt = controller.Load(PartId);
            controller.PopulateForm(pnlMain, dt);
        }


        private bool Validate()
        {
            string msg = string.Empty;

            //string partId = txtPartId.Text.Trim().ToUpper();
            //string upc = txtupc.Text.Trim().ToUpper();
            //string description = txtDescription.Text.Trim().ToUpper();

            //if (partStatus.Equals("Draft"))
            //{
            //    if (string.IsNullOrEmpty(partId) || string.IsNullOrEmpty(description)) msg = "The following fields are required:\r\nPart Id, Description.\r\n\r\n";
            //}
            //else
            //{
            //    if (string.IsNullOrEmpty(partId) || string.IsNullOrEmpty(upc) || string.IsNullOrEmpty(description)) msg = "The following fields are required:\r\nPart Id, Description, UPC.\r\n\r\n";
            //}

            //SideBarController controller = new SideBarController(entityId, environment);
            //if (! controller.IsPartIdUnique(PartId, partId)) msg += "Part Id must be unique.";

            hdnSidebarMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            SideBarController controller = new SideBarController(entityId, environment);
            controller.Save(this.UserInfo.Username, PartId, pnlMain);

            //SaveUserSetting("MainTab", "Creative");
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "id", PartId.ToString()), true);
        }
    }
}
 