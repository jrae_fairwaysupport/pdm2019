using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class AccountingTab: LRHViewer 
    {



        public override bool Initialize()
        {
            if (base.Initialize()) return true;

            pnlActions.Visible = ViewRole.Equals("a") || IsAdministrator;

            lnkEdit.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "AccountingEditor", "id", PartId.ToString(), "r", ViewRole, "e", environment, "eid", entityId);
            Initialized = true;
            return true;
        }

     
    }
}
 