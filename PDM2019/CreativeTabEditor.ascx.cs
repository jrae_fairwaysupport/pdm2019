using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class CreativeTabEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Creative"; } }

        protected override DataSet LoadData()
        {
            DataSet ds = base.LoadData();

            FixAges(txtAgeMin);
            FixAges(txtAgeMax);
            return ds;
        }

        protected override void SetDefaultValues(DataSet ds)
        {
            //bool newRecord = NewRecord(ds, "vwCreativeInfos");
            //chkToggleLabels.Checked = !newRecord;
            chkToggleLabels.Checked = true;
        }

        protected override bool Validate()
        {
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            controller.ValidateNumber(txtPieceCount, true, "Number of Pieces Listed on Box", ref msg);

            decimal? minAge = controller.ValidateDecimal(txtAgeMin, false, "Target Min Age", ref msg);
            decimal? maxAge = controller.ValidateDecimal(txtAgeMax, false, "Target Max Age", ref msg);

            controller.ClearErrorStyles(txtAgeMax);
            if (minAge.HasValue && maxAge.HasValue)
            {
                if (maxAge < minAge)
                {
                    msg += "Target Max Age should be at least the same as Target Min Age.\r\n";
                    controller.ApplyErrorStyles(txtAgeMax);
                }
            }


            hdnCreativeMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            txtGradeMax.Text = hdnGradeMax.Value;
            txtGradeMin.Text = hdnGradeMin.Value;

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwCreativeInfos");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwCreativeInfos", "Creative.Part Information", newRecord, controller);
            SaveTableEdits(tables, "vwPartSpecifications", "Product Development.Specifications", NewRecord(ds, "vwPartSpecifications"), controller);
            SaveTableEdits(tables, "vwPartInfos", "Product Development.Part Information", NewRecord(ds, "vwPartInfos"), controller);

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller) 
        {
            if (!tables.ContainsKey("vwCreativeInfos")) return;


            TableFields table = tables["vwCreativeInfos"];
            if (!table.IsDirty()) return;

            //custom
        }
    }
}
 