<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SalesPriceBreakControl.ascx.cs" Inherits="YourCompany.Modules.PDM2019.SalesPriceBreakControl" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestCustomerId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:25px; top:50px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        
    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

        
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

</script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:800px;" colspan="4">
                <asp:Label ID="labelVendorId" runat="server" CssClass="SubSubHead" Text="Customer Id:<br />" />
                <DNN:DNNTextSuggest ID="txtCustomerId" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggestCustomerId" 
                    Table="CUSTOMER_PRICE" Field="CUSTOMER_SUGGEST" TargetField="CUSTOMER_ID" NameIDSplit="-" Width="600" Placeholder="Customer Id" 
                    queryTmpl8 = "SELECT ID, NAME FROM CUSTOMER WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                    filterTmpl8 = "id like '{0}%' OR NAME LIKE '{0}%'"
                    dataBase="VMFG"
                    validation = "SELECT COUNT(*) AS HITS FROM CUSTOMER WHERE ID = @P0 OR NAME = @P1"
                />
                <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/icon_search_16px.gif" OnCommand="DoSearch"/>
                <asp:ImageButton ID="btnClear" runat="server" ImageUrl="~/images/Search/clearText.png" OnCommand="DoClear" />
            </td>
        </tr>

        <tr>
            <td style="width:200px;">
                <asp:Label ID="labelVendorPartId" runat="server" CssClass="SubSubHead" Text="Customer Part Id:<br />" />
                <asp:TextBox ID="txtCustomerPartId" runat="server" CssClass="Box" Width="180" MaxLength="30" Placeholder="Customer Part Id" Table="CUSTOMER_PRICE" Field="CUSTOMER_PART_ID"  />
            </td>

            <td style="width:200px;" align="center">
                <asp:Label ID="labeluom" runat="server" CssClass='SubSubHead' Text="Unit of Measure:<br />" />
                <asp:DropDownList ID="ddlUnitOfMeasure" runat="server" CssClass="Box" Width="180" Table="CUSTOMER_PRICE" Field="SELLING_UM" />
            </td>
            <td style="width:200px;" align="center">
                <asp:Label ID="labelPrice" runat="server" CssClass="SubSubHead" Text="Default Unit Price:<br />" />
                <asp:TextBox ID="txtDefaultPrice" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Default Price" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="DEFAULT_UNIT_PRICE" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 1:</td>
            <td align="center">
                <asp:Label ID="labelQuantity" runat="server" CssClass="SubSubHead" Text="Quanity:<br />" />
                <asp:TextBox ID="txtQuantity1" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 1" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_1" />
            </td>
            <td align="center">
                <asp:Label ID="labelPrices" runat="server" CssClass="SubSubHead" Text="Unit Price:<br />" />
                <asp:TextBox ID="txtPrice1" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 1" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_1" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 2:</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity2" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 2" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_2" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice2" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 2" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_2" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 3:</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity3" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 3" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_3" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice3" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 3" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_3" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 4:</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity4" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 4" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_4" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice4" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 4" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_4" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 5:</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity5" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 5" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_5" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice5" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 5" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_5" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 6:</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity6" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 6" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_6" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice6" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 6" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_6" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 7:</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity7" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 7" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_7" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice7" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 7" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_7" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 8:</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity8" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 8" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_8" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice8" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 8" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_8" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 9:</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity9" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 9" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_9" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice9" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 9" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_9" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="right" valign="bottom" class="SubSubHead">Break 10:</td>
            <td align="center">
                <asp:TextBox ID="txtQuantity10" runat="server" CssClass="Box" Width="180" PlaceHolder="Quantity 10" Format="Number" MaxLength="10" onkeypress="return isNumberKey(event)" Table="CUSTOMER_PRICE" Field="QTY_BREAK_10" />
            </td>
            <td align="center">
                <asp:TextBox ID="txtPrice10" runat="server" CssClass="Box" style="text-align:right;" Width="180" Placeholder="Unit Price 10" Format="Decimal" MaxLength="25" Table="CUSTOMER_PRICE" Field="UNIT_PRICE_10" />
            </td>
            <td style="width:200px;">&nbsp;</td>
        </tr>

    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnCustomerIdText" runat="server" Value="" />
