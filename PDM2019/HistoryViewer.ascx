<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HistoryViewer.ascx.cs" Inherits="YourCompany.Modules.PDM2019.HistoryViewer" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/TextEditor.ascx"%>



<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

    
    });



</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <table width="850" class="HeaderRow">
            <tr>
                <td style="width:175px;padding-left:4px;">Timestamp/User</td>
                <td style="width:175px;">Tab/Field</td>
                <td style="width:400px;">Edits/Actions</td>
                <td style="width:100px;padding-right:4px;" align="right">&nbsp;<asp:LinkButton id="lnkClearFilter"  runat="server" OnCommand="ClearFilter" Text="Clear Filter" Visible="false" /></td>
            </tr>
    </table>


    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="440" Scrollbars="Auto">

        <asp:DataList ID="lstHistory" runat="server" Width="830" CssClass="table table-hover table-condensed" >
            <ItemTemplate>
                <table width="100%" cellpadding="2" cellspacing="0" border="0">
                    <tr>
                        <td valign="top" align="left" style="width:175px;">
                            <asp:Label ID="lblTimestamp" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TimestampUser")  %>' />
                        </td>
                        <td valign="top" align="center" style="width:175px;">
                            <asp:LinkButton ID="lnkTab" runat="server" style="text-decoration:none;" Text='<%# DataBinder.Eval(Container.DataItem, "TabNameBR") %>' OnCommand="DoFilter" CommandArgument="TabName" TabName='<%# DataBinder.Eval(Container.DataItem, "TabName") %>' />
                            <br />
                            <asp:LinkButton ID="lnkField" runat="server" style="text-decoration:none;" Text='<%# DataBinder.Eval(Container.DataItem, "FieldName") %>' OnCommand="DoFilter" CommandArgument="FieldName" TabName='<%# DataBinder.Eval(Container.DataItem, "TabName") %>' />
                        </td>
                        <td valign="top" align="left" style="width:480px;">
                            <asp:Label ID="lblAction" runat="server"  style="padding-right:4px;vertical-align:top;word-wrap:break-word;" Width="480" Text='<%# DataBinder.Eval(Container.DataItem, "Action") %>' />        
                        </td>
                    </tr>
                </table>

            </ItemTemplate>
            <AlternatingItemStyle BackColor="#e8e2e2" />
        </asp:DataList>



    
    </asp:Panel>    
    <table width="850" cellpadding="4">
        <tr>
            <td align="center" style="width:100%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Close" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;"  />
            </td>
        </tr>
    
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnSku" runat="server" Value="" />