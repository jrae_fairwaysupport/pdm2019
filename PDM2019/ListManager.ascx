<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListManager.ascx.cs" Inherits="YourCompany.Modules.PDM2019.ListManager" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
         
    span.required
    {
        border-bottom: double 2px red;
    }          
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();
        ToggleGrade("Min");
        ToggleGrade("Max");

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

        ToggleRoyalty();
        ToggleRoyaltyType();
        ToggleMoldShare();
        ToggleFabricationLocation();

        
        
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    function ToggleRoyaltyType() {
        //alert(jQuery('[id*="rdoRoyaltyType"]').val());
        var selValue = jQuery('input[id*="rdoRoyaltyType"]:checked').val();
        if (selValue == "PERCENTAGE") {
            jQuery('[id*="labelRoyaltyAmount"]').text("Royalty Percentage:");
            jQuery('[id*="txtRoyaltyAmount"]').attr("placeholder", "Royalty Percentage");
        }
        else {
            jQuery('[id*="labelRoyaltyAmount"]').text("Royalty Amount:");
            jQuery('[id*="txtRoyaltyAmount"]').attr("placeholder", "Royalty Amount");
        }
    }

    function ToggleRoyalty() {
        var checked = jQuery('[id*="checkRoyaltyItem"]:checked').length > 0;
        var showLabels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        if (checked) {
            if (showLabels) jQuery('[id*="labelRoyaltyPayableTo"]').show();
            jQuery('[id*="txtRoyaltyPayableTo"]').show();
            if (showLabels) jQuery('[id*="labelRoyaltyType"]').show();
            if (showLabels) jQuery('[id*="labelRoyaltyAmount"]').show();
            jQuery('[id*="txtRoyaltyAmount"]').show();
            jQuery('[id*="rdoRoyaltyType"]').show();
        }
        else {
            jQuery('[id*="labelRoyaltyPayableTo"]').hide();
            jQuery('[id*="txtRoyaltyPayableTo"]').hide();
            jQuery('[id*="labelRoyaltyType"]').hide();          
            jQuery('[id*="labelRoyaltyAmount"]').hide();
            jQuery('[id*="txtRoyaltyAmount"]').hide();
            jQuery('[id*="rdoRoyaltyType"]').hide();
        }
    }

    function ToggleFabricationLocation() {
        var sel = jQuery('[id*="rdoPurchasedOrFabricated"]:checked').val();
        var showLabels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        if (sel == "Fabricated") {
            if (showLabels)
                jQuery('[id*="labelFabricationLocation"]').show();
            else
                jQuery('[id*="labelFabricationLocation"]').hide();
            jQuery('[id*="ddlFabricationLocation"]').show();
        }
        else {
            jQuery('[id*="labelFabricationLocation"]').hide();
            jQuery('[id*="ddlFabricationLocation"]').hide();

        }
    }

    function ToggleMoldShare() {
        var checked = jQuery('[id*="chkMoldShare"]:checked').length > 0;
        var showLabels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        if (checked) {
            if (showLabels) jQuery('[id*="labelMoldShareVendor"]').show();
            jQuery('[id*="txtMoldShareVendor"]').show();
        }
        else {
            jQuery('[id*="labelMoldShareVendor"]').hide();
            jQuery('[id*="txtMoldShareVendor"]').hide();
        }
    }

    function toggleDiv(className, low, high) {
        var showLabels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;
        var useTop = low + "px";
        if (showLabels) useTop = high + "px";

        $('div[class="' + className + '"]').css('top', useTop);

        //$('div[class="suggestParentPartId"]').css('top', '115px');
        return true;
    }


    function LookupBuyer() {

        var vendor = jQuery('[id*="txtPreferredVendor"]').val();
        jQuery('[id*="txtBuyerUserId"]').val(vendor);
//        var xmlHttp = new XMLHttpRequest();
//        xmlHttp.onreadystatechange = function () {
//            if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
//                callback(xmlHttp.responseText);
//        }
//        xmlHttp.open("GET", theUrl, true); // true for asynchronous 
//        xmlHttp.send(null);

    }

    function ToggleGrade(toggleWhat) {
        //ddl = jQuery('[id*="ddlAge' + toggleWhat + '"] :selected');
        tgt = jQuery('[id*="lblGrade' + toggleWhat + '"]');
        src = jQuery('[id*="txtAge' + toggleWhat + '"]');
        hdn = jQuery('[id*="hdnGrade' + toggleWhat + '"]');

        raw = src.val();


        var age = Number.parseFloat(raw);
        if (Number.isNaN(age)) {
            tgt.text("No Grade");
            hdn.val("No Grade");
            return;
        }

        age = Number.parseInt(age);

        var grades = ["Toddler", "Pre-K", "Kindergarten", "First Grade", "Second Grade", "Third Grade",
                                            "Fourth Grade", "Fifth Grade", "Sixth Grade", "Seventh Grade", "Eighth Grade", "No Grade"];

        if (age == 0) age = 99;

        if (age < 4)
            age = 0;
        else
            age = age - 3;

        //index value, not age
        if (age > 11) age = 11;
        //tgt.text(ddl.attr("grade"));
        tgt.text(grades[age]);
        hdn.val(grades[age]);
    }
    </script>

<asp:Panel ID="pnlContainer" runat="server">
    <table width="850" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td colspan="5">
                <asp:DropDownList ID="ddlListName" runat="server" CssClass="Box" Width="300" AutoPostBack="true" OnSelectedIndexChanged="ToggleList" />
                <asp:Label ID="lblsace" runat="server" Width="25" Text="&nbsp;" />
                <asp:ImageButton ID="btnEditList" runat="server" ToolTip="Edit List" ImageUrl="~/images/edit.gif" OnCommand="ToggleView" CommandName="EditList" />
                <asp:Label ID="lblspace" runat="server" Width="15" Text="&nbsp;" />
                <asp:ImageButton ID="btnNewList" runat="server" ToolTip="New List" ImageUrl="~/images/plus2.gif" OnCommand="ToggleView" CommandName="NewList" />
                <asp:Label ID="Label1" runat="server" Width="15" Text="&nbsp;" />
                <asp:ImageButton ID="btnDeleteList" runat="server" ToolTip="Delete List" ImageUrl="~/images/delete.gif" OnClientClick="javascript:return confirm('Are you sure you want to delete this list?');" OnCommand="DeleteList" />
            </td>
        </tr>
        <tr>
            <td style="width:25px;">&nbsp;</td>
            <td style="width:25px;">&nbsp;</td>
            <td style="width:400px;" class="SubSubHead"><asp:Label ID="lbldisplay" runat="server" Text="Display Text" /></td>
            <td style="width:380px;" class="SubSubHead"><asp:Label ID="lblvalue" runat="server" Text="Stored Value" /></td>
            <td style="width:20px;" align="right"><asp:LinkButton ID="lnkNewRecord" runat="server" Text="New" OnCommand="ToggleView" CommandName="Edit" CommandArgument="-1" /></td>
        </tr>
    </table>
    <asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;border-top:solid 1px black;" Width="850" Height="430" ScrollBars="Auto">
        <asp:Repeater ID="rptList" runat="server">
            <HeaderTemplate>
                <table width="830" cellpadding="4" cellspacing="0" border="0">
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td style="width:25px;" valign="top" align="center">
                            <asp:ImageButton ID="btnEdit" runat="server" ToolTip="Edit Record" ImageUrl="~/images/edit.gif" OnCommand="ToggleView" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' />
                        </td>
                        <td style="width:25px;" valign="top" align="center">
                            <asp:ImageButton ID="btnDeletex" runat="server" ToolTip="Delete Record" ImageUrl="~/images/delete.gif" OnCommand="DeleteRecord" OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' />
                        </td>
                        <td style="width:390px;" valign="top" align="left">
                            <asp:Label ID="lbldislay" runat="server" CssClass="Normal" style="word-wrap:break-word;" Text='<%# DataBinder.Eval(Container.DataItem, "DisplayText") %>' />
                        </td>
                        <td style="width:390px;" valign="top" align="left">
                            <asp:Label ID="lblvalue" runat="server" CssClass="Normal" style="word-wrap:break-word;" Text='<%# DataBinder.Eval(Container.DataItem, "StoredValue") %>' />
                        </td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr>
                        <td style="background-color:#e8e2e2;" valign="top" align="center">
                            <asp:ImageButton ID="btnEdit" runat="server" ToolTip="Edit Record" ImageUrl="~/images/edit.gif" OnCommand="ToggleView" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' />
                        </td>
                        <td style="background-color:#e8e2e2;" valign="top" align="center">
                            <asp:ImageButton ID="btnDelete" runat="server" ToolTip="Delete Record" ImageUrl="~/images/delete.gif" OnCommand="DeleteRecord" OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' />
                        </td>
                        <td style="background-color:#e8e2e2;" valign="top" align="left">
                            <asp:Label ID="lbldislay" runat="server" CssClass="Normal" style="word-wrap:break-word;" Text='<%# DataBinder.Eval(Container.DataItem, "DisplayText") %>' />
                        </td>
                        <td style="background-color:#e8e2e2;" valign="top" align="left">
                            <asp:Label ID="lblvalue" runat="server" CssClass="Normal" style="word-wrap:break-word;" Text='<%# DataBinder.Eval(Container.DataItem, "StoredValue") %>' />
                        </td>
                    </tr>
            
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

    </asp:Panel>

    <table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Close" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
        </td>
        <td align="right" style="width:25%;" valign="middle">
        </td>
    </tr>
    
</table>
</asp:Panel>

<asp:Panel ID="pnlEditor" runat="server" Width="850" Height="450">
    <table width="850" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" valign="middle" style="height:450px;">
                <table width="600" cellpadding="4" cellspacing="0" border="0">
                    <tr>
                        <td colspan="2" align="center" style="border:1px solid black;"><asp:Label id="lblEditAction" runat="server" CssClass="SubHead" /></td>
                    </tr>
                    <tr>
                        <td style="width:100px;border-left:solid 1px black;" class="SubSubHead" valign="top">Display Text:</td>
                        <td style="width:500px;border-right:solid 1px black; class="Normal" valign="top">
                            <asp:TextBox ID="txtDisplayText" runat="server" Width="480" MaxLength="300" CssClass="Box" Placeholder="Display Text" />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-left:solid 1px black;" class="SubSubHead" valign="top">Stored Value:</td>
                        <td style="border-right:solid 1px black; class="Normal" valign="top">
                            <asp:TextBox ID="txtStoredValue" runat="server" Width="480" CssClass="Box" Placeholder="Stored Value" />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-top:solid 1px black;border-bottom:solid 1px black;border-left:solid 1px black;" align="left">
                            <asp:Button ID="btnCancelRecord" runat="server" Text="Cancel" OnCommand="ToggleView" CommandName="Main" />
                        </td>
                        <td style="border-top:solid 1px black; border-bottom:solid 1px black;border-right:solid 1px black;" align="right">
                            <asp:Button ID="btnSaveRecord" runat="server" Text="Save" OnCommand="SaveRecord" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:Panel ID="pnlList" runat="server" Width="850" Height="450">
    <table width="850" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" valign="middle" style="height:450px;">
                <table width="600" cellpadding="4" cellspacing="0" border="0">
                    <tr>
                        <td colspan="2" align="center" style="border:1px solid black;"><asp:Label id="lblListAction" runat="server" CssClass="SubHead" /></td>
                    </tr>
                    <tr>
                        <td style="width:120px;border-left:solid 1px black;" class="SubSubHead">List Name:</td>
                        <td style="width:480px;border-right:solid 1px black;"><asp:TextBox ID="txtListName" runat="server" CssClass="Box" Placeholder="List Name" MaxLength="50" Width="460" /></td>
                    </tr>
                    <tr>
                        <td style="border-left:solid 1px black;" class="SubSubHead">Map to Table:</td>
                        <td style="border-right:solid 1px black;"><asp:DropDownList ID="ddlListTable" runat="server" CssClass="Box" Width="460" AutoPostBack="true" OnSelectedIndexChanged="ListToggleTable" /></td>
                    </tr>
                    <tr>
                        <td style="border-left:solid 1px black;" class="SubSubHead">Map to Column:</td>
                        <td style="border-right:solid 1px black;"><asp:DropDownList ID="ddlListColumn" runat="server" CssClass="Box" Width="460" /></td>
                    </tr>
                    <tr>
                        <td style="border-top:solid 1px black;border-bottom:solid 1px black;border-left:solid 1px black;" align="left">
                            <asp:Button ID="btnCancelList" runat="server" Text="Cancel" OnCommand="ToggleView" CommandName="Main" />
                        </td>
                        <td style="border-top:solid 1px black; border-bottom:solid 1px black;border-right:solid 1px black;" align="right">
                            <asp:Button ID="btnSaveList" runat="server" Text="Save" OnCommand="SaveList" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
