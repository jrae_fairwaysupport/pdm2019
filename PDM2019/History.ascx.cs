using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class HistoryTab: PortalModuleBase, IActionable
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (!IsPostBack)
                {
                }


            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion

        private string environment { get { return GetUserSetting(ParentModuleId, "Environment", "Production"); } }
        private string entityId { get { return GetUserSetting(ParentModuleId, "EntityId", "LRH"); } }


        private string GetUserSetting(int moduleId, string key, string defaultValue = "")
        {
            key = "PDM." + key;
            if (DotNetNuke.Services.Personalization.Personalization.GetProfile(moduleId.ToString(), key) != null)
                return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile(moduleId.ToString(), key);
            else
                return defaultValue;
        }

        private void SaveUserSetting(int moduleId, string key, string value)
        {
            key = "PDM." + key;
            DotNetNuke.Services.Personalization.Personalization.SetProfile(moduleId.ToString(), key, value);
        }


        private int PartId
        {
            get
            {
                string raw = Request.QueryString["id"];
                int retval = -1;
                Int32.TryParse(raw, out retval);
                return retval;
            }
        }
       
        private int ParentModuleId
        {
            get
            {
                try { return Int32.Parse(hdnModuleId.Value); }
                catch { return -1; }
            }

            set
            {
                hdnModuleId.Value = value.ToString();
            }
        }

        protected void DoFilter(object obj, CommandEventArgs args)
        {
            LinkButton btn = (LinkButton)obj;
            string filter = btn.Text;
            string filterWhat = args.CommandArgument.ToString();

            string tabName = string.Empty;
            string fieldName = string.Empty;

            if (filterWhat.Equals("TabName"))
                tabName = btn.Text;
            else
            {
                fieldName = btn.Text;
                tabName = btn.Attributes["TabName"];
            }


            History history = new History(entityId, environment);
            DataTable dt = history.LoadFilter(PartId, tabName, fieldName);
            history.PopulateForm(pnlMain, dt);
            lnkClearFilter.Visible = true;
        }

        protected void ClearFilter(object obj, CommandEventArgs args)
        {
            History history = new History(entityId, environment);
            DataSet ds = history.LoadPart(PartId);
            history.PopulateForm(pnlMain, ds);
            lnkClearFilter.Visible = false;

        }

        public void Initialize(int moduleId)
        {
            ParentModuleId = moduleId;
        }
 

    }
}
 