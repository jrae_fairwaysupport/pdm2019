using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class SidebarReject : LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM - Reject/Request Revisions"; } }


        protected override DataSet LoadData()
        {
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "Parts");

            controller.PopulateForm(pnlMain, ds);
            return ds;

        }



        protected override bool Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);
            controller.ValidateText(txtReason, true, "Rejection/Revision Request", ref msg);

            hdnSidebarMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        private string GetModuleSetting(string key)
        {
            key = entityId + environment + key;

            PDM2019Settings settingsData = new PDM2019Settings(this.TabModuleId);
            return settingsData.ReadSetting<string>(key, string.Empty);
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            txtStatus.Text = "Rejected";

            //SideBarController controller = new SideBarController(entityId, environment);
            //controller.Reject(this.UserInfo.Username, PartId, pnlMain);
            Reject();

            hdnSidebarMessage.Value = "Part has been Rejected.";

            EmailController email = new EmailController(entityId, environment);
            //email.SendRejection(PartId, GetModuleSetting("PartUrlTemplate"), this.PortalId, GetModuleSetting("RejectEmails"));
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "Parts", "vwPartInfos", "vwApprovals");
            Approvals(ds);
            email.SendLRNotification(controller, PartId, ds, GetModuleSetting("PartUrlTemplate"), this.PortalId, this.UserInfo, "Pending", "Rejected");


            //SaveUserSetting("MainTab", "Creative");
            //Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "id", PartId.ToString()), true);
            hdnCloseForm.Value = DotNetNuke.Common.Globals.NavigateURL("", "id", PartId.ToString(), "e", environment, "eid", entityId, "r", ViewRole);
        }


        private void Reject()
        {
            Controller controller = new Controller(entityId, environment);
            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            TableFields table = tables["Parts"];

            if (table.IsDirty())
            {
                controller.history.Log(PartId, this.UserInfo.Username, "Header", table.Fields);
                table.AddField("ModifiedDate", null, DateTime.Now);
                table.AddField("RejectUserId", null, this.UserInfo.Username);
                controller.pdm.UpdateRecord("Parts", "id", PartId, table.GetParametersAsArray(true));
            }

        }

        private void Approvals(DataSet ds)
        {
            Controller controller = new Controller(entityId, environment);
            bool approvalsExist = false;

            DataTable approvals = ds.Tables["vwApprovals"];
            if (approvals != null)
            {
                if (approvals.Rows.Count > 0)
                {
                    int linkedId = controller.pdm.GetValue<int>(approvals.Rows[0], "LinkedId", -1);
                    approvalsExist = (linkedId != -1);
                }
            }

            if (approvalsExist)
                controller.pdm.UpdateRecord("Approvals", "PartId", PartId, "RejectedByUserId", this.UserId, "RejectedByUserName", this.UserInfo.Username, 
                                                "RejectedByEmail", this.UserInfo.Email, "RejectedDate", DateTime.Now);
            else
                controller.pdm.InsertRecord("Approvals", "PartId", "PartId", PartId, "RejectedByUserId", this.UserId, "RejectedByUserName", this.UserInfo.Username, 
                                                "RejectedByEmail", this.UserInfo.Email, "RejectedDate", DateTime.Now);


        }
    }
}
 