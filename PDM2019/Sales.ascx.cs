using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class Sales : LRHViewer // PortalModuleBase, IActionable
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                this.Paging.Navigate += new EventHandler(Paging_Navigated);
                this.Paging.ClearFilter += new EventHandler(Paging_ClearFilter);
                this.Paging.ExportFilter += new EventHandler(Paging_Export);
                Paging.FilterUrl = EditUrl("Filter").Replace("550,950", string.Format("{0},{1}", filterHeight, filterWidth));

                Paging.ShowExcel = true;
                Paging.ShowNavigator = true;
                Paging.ShowPageSize = true;
                Paging.ShowCurrentPage = true;

                //ImageButton btnFilter = (ImageButton)Paging.FindControl("btnFilter");
                //if (btnFilter != null) btnFilter.Visible = false;
                //AFOM need a Hide Filter command on Paging
                Paging.Controls[10].Visible = false;
                Paging.Controls[11].Visible = false;

                if (!IsPostBack)
                {

                    
                }

                SetFilterUrl();

            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        protected void Paging_Navigated(object obj, EventArgs args)
        {
            LoadData();

        }

        protected void Paging_Export(object obj, EventArgs args)
        {

            Filter.LoadFilter();
            Filter.BaseQuery = PartQueryBase;
            Filter.OrderBy = PartQueryOrderBy;
            
            if (partStatus.Equals("Approved"))
                Filter.AddFilterCriteria("PART_ID", sku, true, true);
            else
                Filter.AddFilterCriteria("PART_ID", PartId.ToString(), false, true);

            if (!string.IsNullOrEmpty(customerId)) Filter.AddFilterCriteria("CUSTOMER_ID", customerId, true, true);
            Filter.Export(environment, entityId + "_PDM", true);
        }

        private void Paging_ClearFilter(object obj, EventArgs args)
        {
            Paging.CurrentPage = 1;//PageNumber = 1;
            Paging.PageCount = 0; //PageCount = 0;

            Filter.ClearFilter();
            //Filter.AddFilterCriteria("Environment", environment, true, true);
            Filter.SaveFilter();

            //Paging2.CurrentPage = 1;
            Paging.PageCount = 0;

            LoadData();


        }

        protected string customerId
        {
            get { return hdnCustomerId.Value; }
            set { hdnCustomerId.Value = value; }
        }
        
        protected void LoadData(string SKU = "")
        {
            Controller controller = new Controller(entityId, environment);

            //if (!string.IsNullOrEmpty(SKU)) sku = SKU;

            Filter.LoadFilter();
            Filter.BaseQuery = PartQueryBase;
            Filter.OrderBy = PartQueryOrderBy;

            if (partStatus.Equals("Approved"))
            {
                if (!string.IsNullOrEmpty(sku)) Filter.AddFilterCriteria("PART_ID", sku, true, true);
                if (!string.IsNullOrEmpty(customerId)) Filter.AddFilterCriteria("CUSTOMER_ID", customerId, true, true);
            }
            else
            {
                Filter.AddFilterCriteria("PART_ID", PartId.ToString(), false, true);
                if (!string.IsNullOrEmpty(customerId)) Filter.AddFilterCriteria("CustomerId", customerId, true, true);
            }

            int pageCount = Paging.PageCount;
            int recordCount = Paging.RecordCount;
            int pageNumber = Paging.CurrentPage;
            int pageSize = Paging.PageSize;



            DataTable dt = controller.Search(Filter.Filter(), pageNumber, ref pageCount, ref recordCount, pageSize, partStatus.Equals("Approved"));
            Paging.PageCount = pageCount;
            Paging.RecordCount = recordCount;

            for (int index = 1; index <= 10; index++) dt.Columns.Add("UnitPrice" + index.ToString(), System.Type.GetType("System.String"));
            dt.Columns.Add("DefaultPrice", System.Type.GetType("System.String"));
            dt.Columns.Add("Url", System.Type.GetType("System.String"));

            foreach (DataRow dr in dt.Rows)
            {
                for (int index = 1; index <= 10; index++)
                {
                    string raw = controller.GetValue<string>(dr, "UNIT_PRICE_" + index.ToString(), string.Empty);
                    if (!string.IsNullOrEmpty(raw))
                    {
                        dr["UnitPrice" + index.ToString()] = controller.GetValue<decimal>(dr, "UNIT_PRICE_" + index.ToString(), 0).ToString("$ #,0.000");
                    }
                }
                dr["DefaultPrice"] = controller.GetValue<decimal>(dr, "DEFAULT_UNIT_PRICE", 0).ToString("$ #,0.000");

                if (CanUserEdit)
                {
                    string customer = controller.GetValue<string>(dr, "CUSTOMER_ID", string.Empty);
                    string part_id = controller.GetValue<string>(dr, "PART_ID", string.Empty);
                    dr["Url"] = EditUrl("mid", ParentModuleId.ToString(), "SalesEditor", "id", PartId.ToString(), "cid", customer, "pid", part_id, "r", ViewRole, "s", partStatus, "eid", entityId, "e", environment);
                }
            }




            lstSales.DataSource = dt;
            lstSales.DataBind();
            lblNoPriceBreaks.Visible = (dt.Rows.Count == 0);

        }

        private string exportFolder { get { return Server.MapPath(ControlPath) + "/Exports"; } }

        private int filterHeight
        {
            get
            {
                if (Settings.Contains("FilterHeight")) return (int)Settings["FilterHeight"];
                return 400;
            }
        }

        private int filterWidth
        {
            get
            {
                if (Settings.Contains("FilterWidth")) return (int)Settings["FilterWidth"];
                return 800;
            }
        }

        private void SetFilterUrl()
        {
            Paging.FilterUrl = EditUrl("Filter").Replace("550,950", string.Format("{0},{1}", filterHeight, filterWidth));
        }

        protected void DoFilter(object obj, CommandEventArgs args)
        {
            Controller controller = new Controller(entityId, environment);
            string idValue = string.Empty;
            string nameValue = string.Empty;
            controller.DNNTextSuggestValues(txtVendorId.Text.Trim(), out idValue, out nameValue, '-');
            customerId = idValue;
            LoadData(sku); //feed in sku to reset the paging
        }

        protected void ClearFilter(object obj, CommandEventArgs args)
        {
            customerId = string.Empty;
            txtVendorId.Text = string.Empty;
            LoadData(sku);
        }

        protected bool CanUserEdit
        {
            get { return lnkEdit.Visible; }
            set { lnkEdit.Visible = value; }
        }

        protected void DoDelete(object obj, CommandEventArgs args)
        {
            ImageButton btn = (ImageButton)obj;
            string customerId = btn.Attributes["CustomerId"];


            Controller controller = new Controller(entityId, environment);
            string tabName = "Sales.Price Breaks";
            string fieldName = "Customer: " + customerId;
            
            controller.history.Log(PartId, this.UserInfo.Username, tabName, fieldName, string.Empty, "Deleted All Pricebreaks");

            if (partStatus.Equals("Approved"))
            {
                string sql = "DELETE FROM CUSTOMER_PRICE WHERE CUSTOMER_ID = @P0 AND PART_ID = @P1";
                controller.vmfg.ExecuteCommand(sql, customerId, sku);
            }
            else
            {
                string sql = "DELETE FROM SalesPartInfos WHERE CustomerId = @P0 and PART_ID = @P1";
                controller.pdm.ExecuteCommand(sql, customerId, PartId);
            }
            
            
            
            
            LoadData(sku);
        }

        public override bool Initialize()
        {
            if (base.Initialize()) return true;

            CanUserEdit = ViewRole.Equals("s") || ViewRole.Equals("ad");
            
            lnkEdit.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "SalesEditor", "id", PartId.ToString(), "e", environment, "eid", entityId, "r", ViewRole); //"pid", sku, "s", partStatus);
            LoadData(sku);

            Initialized = true;
            return true;
        }

        private string PartQueryBase
        {
            get
            {
                if (partStatus.Equals("Approved")) return "SELECT C.NAME AS CUSTOMER_NAME, P.CUSTOMER_ID || ' - ' || C.NAME AS CUSTOMER_SUGGEST, "
                         + "CASE WHEN COALESCE(P.CUSTOMER_PART_ID, 'EMPTY') <> 'EMPTY' THEN 'Customer Part Id: ' || P.CUSTOMER_PART_ID ELSE NULL END AS CustomerPartId, "
                         + "P.* "
                         + "FROM CUSTOMER_PRICE P INNER JOIN CUSTOMER C ON P.CUSTOMER_ID = C.ID ";

                string remote = environment.Equals("Development") ? "TEST_VMLREI" : "VMLREI";

                return string.Format("SELECT C.NAME AS CUSTOMER_NAME, dbo.FormatTextSuggest(P.CustomerId, C.NAME) AS CUSTOMER_SUGGEST, "
                        + "CASE WHEN COALESCE(P.CustomerPartId, 'EMPTY') <> 'EMPTY' THEN 'Customer Part Id: ' + P.CustomerPartId ELSE NULL END AS CustomerPartId, "
                        + "P.CustomerId AS CUSTOMER_ID, P.Part_Id AS PART_ID, P.UnitOfMeasure AS SELLING_UM, P.CustomerPartId AS CUSTOMER_PART_ID, "
                        + "P.QuantityBreak1 AS QTY_BREAK_1, P.QuantityBreak2 AS QTY_BREAK_2, P.QuantityBreak3 AS QTY_BREAK_3, P.QuantityBreak4 AS QTY_BREAK_4, P.QuantityBreak5 AS QTY_BREAK_5, "
                        + "P.QuantityBreak6 AS QTY_BREAK_6, P.QuantityBreak7 AS QTY_BREAK_7, P.QuantityBreak8 AS QTY_BREAK_8, P.QuantityBreak9 AS QTY_BREAK_9, P.QuantityBreak10 AS QTY_BREAK_10, "
                        + "P.UnitPrice1 AS UNIT_PRICE_1, P.UnitPrice2 AS UNIT_PRICE_2, P.UnitPrice3 AS UNIT_PRICE_3, P.UnitPrice4 AS UNIT_PRICE_4, P.UnitPrice5 AS UNIT_PRICE_5, "
                        + "P.UnitPrice6 AS UNIT_PRICE_6, P.UnitPrice7 AS UNIT_PRICE_7, P.UnitPrice8 AS UNIT_PRICE_8, P.UnitPrice9 AS UNIT_PRICE_9, P.UnitPrice10 AS UNIT_PRICE_10, "
                        + "P.DefaultPrice AS DEFAULT_UNIT_PRICE FROM SalesPartInfos P INNER JOIN [FWS-REPLR\\LR_REPLICATION].{0}.SYSADM.CUSTOMER C ON P.CustomerId = C.ID ", remote);
                //+ "INNER JOIN Parts ON P.Part_Id = Parts.Id "

            }
        }

        private string PartQueryOrderBy
        {
            get
            {
                if (partStatus.Equals("Approved")) return "P.CUSTOMER_ID";
                return "P.CustomerId";
            }
        }

        private string PartQueryDatabase
        {
            get
            {
                return entityId.Equals("LRH") ? "VMLREI11" : "H2MVMLREI11";
            }
        }

    }
}
 