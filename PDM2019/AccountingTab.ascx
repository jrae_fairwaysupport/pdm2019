<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountingTab.ascx.cs" Inherits="YourCompany.Modules.PDM2019.AccountingTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server" ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td class="SubSubHead" style="width:180px;">Freight In Cost:</td>                        
                <td style="width:180px;" ><asp:Label ID="Label1" runat="server" CssClass="Normal placeholder" Width="100" style="text-align:right;" Table="VMFG.Extensions" Field="FreightInCostText"  /></td>
                <td style="width:210px;" class="SubSubHead"><asp:Image ID="imgcalculateinbound" runat="server" Table="vwAccounting" Field="CalculateInboundFreight" />&nbsp;Calculate Inbound Freight</td>
                <td style="width:180px;" />
            </tr>
            <tr>
                <td class="SubSubHead" >Landed Cost:</td>
                <td ><asp:Label ID="Label2" runat="server" CssClass="Normal placeholder" Width="100"  style="text-align:right;" Table="VMFG.Extensions" Field="LANDED_COST" Format="Decimal" Places="4" /></td>
                <td class="SubSubHead"><asp:Image ID="imgPatent" runat="server" Table="vwMarketingInfos" Field="ExemptFromTax" />&nbsp;Tax Exempt</td>
                <td />
            </tr>
            <tr>
                <td class="SubSubHead">Duties:</td>
                <td ><asp:Label ID="lblduties" runat="server" CssClass="Normal placeholder" Width="100"  style="text-align:right;" Table="vwPartInfos" Field="Duties" Format="Decimal" Places="4" /></td>
                <td class="SubSubHead">Category:</td>
                <td><asp:Label ID="lblcategory" runat="server" CssClass="Normal placeholder" Width="160" Table="vwAccounting" Field="CategoryText" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">Selling/Net Price:</td>                        
                <td><asp:Label ID="Label3" runat="server" CssClass="Normal placeholder required" Width="100"  style="text-align:right;" Table="vwPartInfos" Field="NetPriceText" /></td>
                <td class="SubSubHead" >Quantity Conversion:</td>
                <td class="SubSubHead">
                    <asp:Label id="lblquantityconversion" runat="server" CssClass="Normal placeholder required" Width="100" style="text-align:right;" Table="vwPartInfos" Field="ParentPartQuantityConversion" />
                </td>
            </tr>
            <tr>
                <td class="SubSubHead">Selling/Retail Price:</td>
                <td><asp:Label ID="Label4" runat="server" CssClass="Normal placeholder required" Width="100" style="text-align:right;" Table="vwPartInfos" Field="RetailPriceText" /></td>
                <td />
                <td />
            </tr>


            <tr>
                <td class="SubSubHead">Duty Amount:</td>
                <td><asp:Label ID="lblduty" runat="server" CssClass="Normal placeholder" Width="100"  style="text-align:right;" Table="vwPurchasingPartInfos" Field="DutyRate" Format="Decimal" Places="4" /></td>
                <td class="SubSubHead">Add/CVD Rate (%):</td>
                <td><asp:Label ID="lbladdrate" runat="server" CssClass="Normal placeholder required" Width="100" style="text-align:right;" Table="vwPurchasingPartInfos" Field="AddRateText" Format="Decimal" Places="4" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">First Year Forecast:</td>
                <td><asp:Label ID="Label9" runat="server" CssClass="Normal placeholder" Width="100" style="text-align:right;" Table="vwPartInfos" Field="FirstYearForecast" /></td>
                <td class="SubSubHead"><asp:Image ID="imgSellable" runat="server" Table="vwPartInfos" Field="Sellable" />&nbsp;Sellable</td>
                <td />
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




