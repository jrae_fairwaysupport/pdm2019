<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDVendorPrice.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PDVendorPriceTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server" ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlVendorPrice" runat="server" Width="750" Height="600" ScrollBars="Auto">

        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            
            
            
            <tr>
                <td class="SubSubHead" valign="top"><asp:Image ID="imgcombo" runat="server" Table="PartVendorPrices" Field="ComboPricing" />&nbsp;Combo Pricing:</td>
                <td align="right" valign="top"><asp:Label ID="Label9" runat="server" CssClass="Normal" Table="PartVendorPrices" Field="ComboPrice" Format="Money" /></td>
                <td style="padding-left:10px;" valign="top" class="SubSubHead">&nbsp;</td>
                <td >&nbsp;</td>
            </tr>

            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>

            <tr>
                <td colspan="4" valign="top">
                    <table width="720" class="HeaderRow">
                        <tr>
                            <td style="width:670px;padding-left:4px;">Vendor Price Breaks</td>
                            <td style="width:50px;padding: 2px 4px 2px 2px;" align="right"><asp:HyperLink ID="lnkAddVendorPriceBreak" runat="server" ImageUrl="~/images/plus2.gif" visible="false"/></td>
                        </tr>
                    </table>

                    <asp:DataList ID="lstVendorPriceBreaks" runat="server" Width="720" CssClass="table table-hover table-condensed" Table="vwVendorPriceBreaks"  OnDataBinding="VerifyDataListHasData" NoDataLabel="lblNoVendorPriceBreaks" >
                        <ItemTemplate>
                            <asp:Label width="600" ID="lblVendorId" runat="server" style="vertical-align:top;border-bottom:solid 1px orange;" Text='<%# DataBinder.Eval(Container.DataItem, "VendorSuggest") %>' />
                            
                            <br />

                            <asp:Label ID="lblVendorPart" runat="server" Width="125" style="padding-left:25px;" CssClass="SubSubHead" Text="Vendor Part Id:" />
                            <asp:Label ID="lblvendorPartId" runat="server" style="border-bottom:solid 1px orange;" Width="100" Text='<%# DataBinder.Eval(Container.DataItem, "VendorPartId") %>' />
                            <asp:Label ID="lblQuantity" runat="server" CssClass="SubSubHead" Width="75" Text="Quantity:" />
                            <asp:Label Width="60" id="lblQty1" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity1") %>' />
                            <asp:Label Width="60" id="Label1" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity2") %>' />
                            <asp:Label Width="60" id="Label2" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity3") %>' />
                            <asp:Label Width="60" id="Label10" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity4") %>' />
                            <asp:Label Width="60" id="Label11" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity5") %>' />
                            <br />

                            <asp:Label ID="Label12" runat="server" Width="125" Text="&nbsp;" />
                            <asp:Label ID="Label13" runat="server" Width="100" Text='&nbsp;' />
                            <asp:Label ID="Label14" runat="server" CssClass="SubSubHead" Width="75" Text="Price:" />
                            <asp:Label Width="60" id="Label15" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price1Text") %>' />
                            <asp:Label Width="60" id="Label16" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price2Text") %>' />
                            <asp:Label Width="60" id="Label17" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price3Text") %>' />
                            <asp:Label Width="60" id="Label18" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price4Text") %>' />
                            <asp:Label Width="60" id="Label19" runat="server" style="text-align:right;" Text='<%# DataBinder.Eval(Container.DataItem, "Price5Text") %>' />
                            
                            

                        </ItemTemplate>
                    </asp:DataList>
                    <asp:Label ID="lblNoVendorPriceBreaks" runat="server" CssClass="Normal" Width="720" style="padding-left:4px;" Text="No Vendor Price Breaks have been added to this part." />
                </td>
            </tr>
        </table>
                

    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />



