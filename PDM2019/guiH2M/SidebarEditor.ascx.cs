using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class h2mSidebarEditor : PortalModuleBase, IActionable
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
                currPage.Title = "PDM Part Header Editor";


                if (!IsPostBack)
                {
                    LoadData();
                }


            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion


        private int PartId
        {
            get
            {
                string raw = Request.QueryString["id"];
                int retval = -1;
                Int32.TryParse(raw, out retval);
                return retval;
            }
        }
       
        

        //private string GetUserSetting(string key, string defaultValue = "")
        //{
        //    key = "PDM." + key;
        //    if (DotNetNuke.Services.Personalization.Personalization.GetProfile(this.ModuleId.ToString(), key) != null)
        //        return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile(this.ModuleId.ToString(), key);
        //    else
        //        return defaultValue;
        //}

        //private void SaveUserSetting(string key, string value)
        //{
        //    key = "PDM." + key;
        //    DotNetNuke.Services.Personalization.Personalization.SetProfile(this.ModuleId.ToString(), key, value);
        //}

        private string GetUserSetting(string key, string defaultValue = "")
        {
            if (DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key) != null)
                return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key);
            else
                return defaultValue;
        }

        private void SaveUserSetting(string key, string value)
        {
            DotNetNuke.Services.Personalization.Personalization.SetProfile("PDM", key, value);
        }


        private string environment { get { return GetUserSetting("Environment", "Production"); } }
        private string entityId { get { return GetUserSetting("EntityId", "LRH"); } }



        public void LoadData()
        {
            SideBarController controller = new SideBarController(entityId, environment);
            DataTable dt = controller.Load(PartId);
            controller.PopulateForm(pnlMain, dt);
        }

        private bool Validate()
        {
            string msg = string.Empty;

            string partId = txtPartId.Text.Trim().ToUpper();
            string upc = txtupc.Text.Trim().ToUpper();
            string description = txtDescription.Text.Trim().ToUpper();

            if (string.IsNullOrEmpty(partId) || string.IsNullOrEmpty(upc) || string.IsNullOrEmpty(description)) msg = "The following fields are required:\r\nPart Id, Description, UPC.\r\n\r\n";

            SideBarController controller = new SideBarController(entityId, environment);
            if (! controller.IsPartIdUnique(PartId, partId)) msg += "Part Id must be unique.";

            hdnSidebarMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            SideBarController controller = new SideBarController(entityId, environment);
            controller.Save(this.UserInfo.Username, PartId, pnlMain);

            //SaveUserSetting("MainTab", "Creative");
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "id", PartId.ToString()), true);
        }
    }
}
 