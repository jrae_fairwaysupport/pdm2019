<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SidebarEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.h2mSidebarEditor" %>

<style>
    div.offset { padding-left:4px;
    }
   
</style>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        var msg = jQuery('[id*="hdnSidebarMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnSidebarMessage"]').val("");

        ToggleLabels();

    });

    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

//$( "li" ).each(function( index ) {
//  console.log( index + ": " + $( this ).text() );
//
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>


<asp:Panel ID="pnlMain" runat="server"  style="border-bottom:solid 1px black;">
    <table width="760" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:30%;" align="left" valign="top">
                <asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Part Id:<br />" />
                <asp:TextBox ID="txtPartId" runat="server" CssClass="Box" Width="140" MaxLength="25" Table="Parts" Field="PartId" Placeholder="Part Id" />
            </td>
            <td style="width:70%" align="left" valign="top">
                <asp:Label ID="labelDescription" runat="server" CssClass="SubSubHead" Text="Description:<br />" />
                <asp:TextBox ID="txtDescription" runat="server" CssClass="Box" Width="400" MaxLength="40" Table="Parts" Field="Description" Placeholder="Description" />
            </td>
        </tr>

        <tr>
            <td align="left" valign="top">
                <asp:Label ID="labelABC" runat="server" CssClass="SubSubHead" Text="ABC Code:<br />" />
                <asp:TextBox ID="txtabc" runat="server" CssClass="Box" Width="140" MaxLength="25" Table="Parts" Field="ABCCode" Placeholder="ABC Code" />
            </td>
            <td align="left" valign="top">
                <asp:Label ID="labelUPC" runat="server" CssClass="SubSubHead" Text="UPC Code:<br />" />
                <asp:TextBox ID="txtupc" runat="server" CssClass="Box" Width="140" MaxLength="12" Table="Parts" Field="UPCCode" Placeholder="UPC Code" onkeypress="return isNumberKey(event)" />
            </td>
        </tr>

<!--
        <tr>
            <td align="left" valign="top" colspan="2">
                <asp:Label ID="labelExtended" runat="server" CssClass="SubSubHead" Text="Extended Description:<br />" />
                <asp:TextBox ID="txtExtended" runat="server" CssClass="Box" Width="750" MaxLength="500" TextMode="MultiLine" Height="100" Table="Parts" Field="ExtendedDescription" Placeholder="Extended Description" />
            </td>
        </tr>
-->
    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" Checked="true" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        
        <td align="right" style="width:50%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnSidebarMessage" runat="server" Value="" />






