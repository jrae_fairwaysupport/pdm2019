<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDevelopmentTab.ascx.cs" Inherits="YourCompany.Modules.PDM2019.ProductDevelopmentTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="PDPartInformation.ascx" TagName="partInformation" TagPrefix="pdm" %>
<%@ Register Src="PDSpecifications.ascx" TagName="partSpecifications" TagPrefix="pdm" %>
<%@ Register Src="PDPackaging.ascx" TagName="partPackaging" TagPrefix="pdm" %>
<%@ Register Src="PDVendorPrice.ascx" TagName="partVendorPrices" TagPrefix="pdm" %>
<%@ Register Src="PDWeightsandMeasures.ascx" TagName="partWeightsAndMeasures" TagPrefix="pdm" %>
<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server"  AutoPostBack="true" OnTabClick="LoadData"  >
        <Tabs>
            <telerik:RadTab Text="Part Information"/>
            <telerik:RadTab Text="Specifications" />
            <telerik:RadTab Text="Packaging" />
            <telerik:RadTab Text="Vendor Price" />
            <telerik:RadTab Text="Weights & Measures" />
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;" >

        <telerik:RadPageView runat="server" ID="pgPartInformation" >
            <pdm:partInformation id="partInformation" runat="server" Editors="pd-*|ad-*" Editor="PDPartInformationEditor"  />
        </telerik:RadPageView> 
    
        <telerik:RadPageView runat="server" ID="pgSpecifications">
            <pdm:partSpecifications id="partSpecifications" runat="server"  Editors="pd-*|ad-*"  Editor="PDPartSpecificationsEditor"  />
        </telerik:RadPageView> 

        <telerik:RadPageView runat="server" ID="pgPackaging" >
            <pdm:partPackaging id="partPackagingTab" runat="server" Editors="pd-*|ad-*" Editor="PDPackaging" />
        </telerik:RadPageView> 

        <telerik:RadPageView runat="server" ID="pgVendorPrice" >
            <pdm:partVendorPrices id="partVendorPricesTabx" runat="server" Editors="pd-*|ad-*" Editor="PDVendorPriceEditor" />
        </telerik:RadPageView> 

        <telerik:RadPageView runat="server" ID="pgWeightsAndDimensions" >
            <pdm:partWeightsAndMeasures id="ctlWeightsAndMeasures" runat="server"  />
        </telerik:RadPageView> 

    </telerik:RadMultiPage>
</asp:Panel>


<asp:HiddenField ID="hdnModuleId" runat="server" Value="-1" />




