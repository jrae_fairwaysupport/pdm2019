using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class PDSpecificationsTab: LRHViewer //PortalModuleBase, IActionable
    {
        //public event EventHandler SomethingHistoricalHappened;

        //private void UpdateHistory()
        //{
        //    //Null check makes sure the main page is attached to the event
        //    if (this.SomethingHistoricalHappened != null)
        //        this.SomethingHistoricalHappened(this, new EventArgs());
        //}

        public override bool Initialize()
        {
            if (base.Initialize()) return true; 

            //bool hasData = DoesDatalistHaveData(lstBatteries, "lblbatterysize");
            //lblNoBatteries.Visible = !hasData;
            return true;
        }

        protected bool ShowLegacy(object obj, bool flip)
        {
            string raw = obj.ToString();
            if (string.IsNullOrEmpty(raw)) return (flip ? true : false);
            if (raw.Equals("0")) return (flip ? true : false);
            if (raw.Equals("1")) return (flip ? false : true);

            bool test = false;
            bool.TryParse(raw, out test);
            return (flip ? !test : test);
        }
        
    }
}
 