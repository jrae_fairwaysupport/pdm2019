using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
    public partial class LegalEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Legal"; } }

        protected override void SetDefaultValues(DataSet ds)
        {
            bool newRecord = NewRecord(ds, "vwPartInfos");
            chkToggleLabels.Checked = !newRecord;
        }
   
        protected override bool Validate()
        {
            string msg = string.Empty;


            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {
                CloseForm();
                return;
            }

            if (!Validate()) return;

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwPartInfos");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwPartInfos", "Legal", newRecord, controller);

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields>tables, Controller controller)
        {
            if (!tables.ContainsKey("vwPartInfos")) return;

            TableFields table = tables["vwPartInfos"];
            if (!table.IsDirty()) return;

            //custom


        }

   
    }
}
 