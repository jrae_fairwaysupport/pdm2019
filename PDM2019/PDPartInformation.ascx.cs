using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class PDPartInformationTab: LRHViewer // PortalModuleBase, IActionable
    {


        public override bool Initialize()
        {
            if (base.Initialize()) return true;

            FixAges(lblTargetAgeMin);
            FixAges(lblTargetAgeMax);


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, new string[] { "vwPartInfos" });
            DataTable dt = ds.Tables["vwPartInfos"];
            if (dt.Rows.Count > 0)
            {
                bool test = controller.pdm.GetValue<bool>(dt.Rows[0], "Fabricated", false);

                lblFabricationLocationLabel.Visible = test;
                lblFabricationLocationValue.Visible = test;
                lblUnitAssemblyCost.Visible = test;
                lblUnitAssemblyCostValue.Visible = test;

                if (test)
                {
                    test = controller.pdm.GetValue<string>(dt.Rows[0], "FabricationLocation", "").Equals("FabUs");
                    lblMaterialcost.Visible = !test;
                    lblMaterialCostLabel.Visible = !test;
                }

                test = controller.pdm.GetValue<bool>(dt.Rows[0], "RoyaltyItem", false);
                tblRoyalty.Visible = test;
                if (test)
                {
                    test = controller.pdm.GetValue<string>(dt.Rows[0], "RoyaltyType", "PERCENTAGE").Equals("PERCENTAGE");
                    lblRoyaltyAmountText.Text = test ? "Percentage:" : "Amount:";
                    lblRoyaltyAmountValue.Text = controller.pdm.GetValue<string>(dt.Rows[0], test ? "RoyaltyPercentageText" : "RoyaltyAmountText", string.Empty);
                }
            }



            Initialized = true;
            return true;
        }

      

        private void FixAges(Label txt)
        {
            string test = txt.Text.Trim();
            decimal decVal = 0;
            if (decimal.TryParse(test, out decVal))
            {
                if ((decimal)((int)decVal) == decVal)
                    txt.Text = ((int)decVal).ToString();
                else
                    txt.Text = decVal.ToString("#.0");
            }
            else
                txt.Text = string.Empty;

        }
    }
}
 