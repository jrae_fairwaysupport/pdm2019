using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using Telerik.Web.UI;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    //Base needs to be accessible to all pages, including Part Browser, where PartId, environment, and entityId are not yet set
    //scratch that, Part Browser cannot be the one to set Part Settings as it uses get requests/hyperlinks to open parts
    //SO....when Edit Part is opened, that's when stuff has to be set.
    public partial class LRHBase : PortalModuleBase, IActionable
    {

        protected List<string> PDMLRHRoles = new List<string>() { "Administrators", "PDM LRH Product Development", "PDM LRH Purchasing", "PDM LRH Quality Assurance", "PDM LRH Creative", "PDM LRH Sales", "PDM LRH Marketing", "PDM LRH Accounting", "PDM LRH Viewer" }; //, "PDM LRH Legal"
        protected List<string> PDMH2MRoles = new List<string>() { "Administrators", "PDM H2M Product Development", "PDM H2M Purchasing", "PDM H2M Quality Assurance", "PDM H2M Creative", "PDM H2M Sales", "PDM H2M Marketing", "PDM H2M Viewer" };

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion

        protected string UserSettingContainer
        {
            get { return string.Format("PDM_{0}_{1}", entityId, environment); }
        }

        protected List<int> UserSettingParts
        {
            get
            {
                return (List<int>)DotNetNuke.Services.Personalization.Personalization.GetProfile(UserSettingContainer, "Parts");
            }

            set
            {
                DotNetNuke.Services.Personalization.Personalization.SetProfile(UserSettingContainer, "Parts", value);
            }
        }

        protected List<string> UserSettingKeys
        {
            get
            {
                return (List<string>)DotNetNuke.Services.Personalization.Personalization.GetProfile(UserSettingContainer, "Keys");
            }

            set
            {
                DotNetNuke.Services.Personalization.Personalization.SetProfile(UserSettingContainer, "Keys", value);
            }
        }

        protected string UserSettingAttributeKey(int partId, string attribute)
        {
            return string.Format("{0}_{1}", partId, attribute);
        }

        protected bool UserSettingPartExists(int partId)
        {
            List<int> parts = UserSettingParts;
            if (parts == null) return false;
            if (!parts.Contains(partId)) return false;


            DateTime test = GetUserSetting<DateTime>(partId, "LastAccessed", DateTime.MinValue);
            if (test >= DateTime.Now.AddDays(-1)) return true;

            List<string> keys = UserSettingKeys;
            foreach (string mKey in keys)
            {
                string newKey = UserSettingAttributeKey(partId, mKey);
                DotNetNuke.Services.Personalization.Personalization.RemoveProfile(UserSettingContainer, newKey);
            }

            parts.Remove(partId);
            UserSettingParts = parts;

            return false;
        }

        protected void SaveUserSetting(int partId, string key, object value)
        {
            string attributeKey = UserSettingAttributeKey(partId, key);

            if (!UserSettingPartExists(partId))
            {
                List<int> parts = UserSettingParts;
                if (parts == null) parts = new List<int>();
                parts.Add(partId);
                UserSettingParts = parts;
            }


            string accessKey = UserSettingAttributeKey(partId, "LastAccessed");
            DotNetNuke.Services.Personalization.Personalization.SetProfile(UserSettingContainer, accessKey, DateTime.Now);
            DotNetNuke.Services.Personalization.Personalization.SetProfile(UserSettingContainer, attributeKey, value);
        }

        protected T GetUserSetting<T>(int partId, string key, T defaultValue = default(T))
        {
            string attributeKey = UserSettingAttributeKey(partId, key);

            if (DotNetNuke.Services.Personalization.Personalization.GetProfile(UserSettingContainer, attributeKey) != null)
            {
                //SaveUserSetting(partId, "LastAccessed", DateTime.Now);
                string accessKey = UserSettingAttributeKey(partId, "LastAccessed");
                DotNetNuke.Services.Personalization.Personalization.SetProfile(UserSettingContainer, accessKey, DateTime.Now);
                return (T)DotNetNuke.Services.Personalization.Personalization.GetProfile(UserSettingContainer, attributeKey);
            }

            return defaultValue;
        }

        protected void RemoveUserSetting(int partId, string key)
        {
            string attributeKey = UserSettingAttributeKey(partId, key);
            DotNetNuke.Services.Personalization.Personalization.RemoveProfile(UserSettingContainer, attributeKey);
        }

        protected bool IsAdministrator
        {
            get
            {
                if (this.UserInfo.IsSuperUser) return true;
                if (this.UserInfo.IsInRole("Administrators")) return true;
                return false;
            }
        }

        protected int PartId
        {
            get
            {
                string raw = Request.QueryString["id"];
                int retval = -1;
                Int32.TryParse(raw, out retval);
                return retval;
            }
        }

        //careful here
        protected virtual string environment
        {
            get { return Request.QueryString["e"]; }
        }

        protected virtual string entityId
        {
            get { return Request.QueryString["eid"]; }
        }

        protected string ViewRole
        {
            get
            {
                //what does the user say he is?
                string raw = Request.QueryString["r"];

                string test = "UNSET";
                switch (raw)
                {
                    case "pd":
                        test = string.Format("PDM {0} Product Development", entityId);
                        break;
                    case "p":
                        test = string.Format("PDM {0} Purchasing", entityId);
                        break;
                    case "q":
                        test = string.Format("PDM {0} Quality Assurance", entityId);
                        break;
                    case "c":
                        test = string.Format("PDM {0} Creative", entityId);
                        break;
                    case "s":
                        test = string.Format("PDM {0} Sales", entityId);
                        break;
                    case "m":
                        test = string.Format("PDM {0} Marketing", entityId);
                        break;
                    //case "l":
                    //    test = string.Format("PDM {0} Legal", entityId);
                    //    break;
                    case "a":
                        test = string.Format("PDM {0} Accounting", entityId);
                        break;
                    case "ad":
                        test = "Administrators";
                        break;
                }


                //validate access to role
                if (test.Equals("Administrators") && !IsAdministrator) test = "UNSET";

                if (!test.Equals("UNSET"))
                {
                    if (!this.UserInfo.IsInRole(test)) test = "UNSET";
                }

                //if bonked, find a role for this user...any role 
                //first...go big
                if (test.Equals("UNSET")) 
                {
                    List<string> PDMRoles = entityId.Equals("LRH") ? PDMLRHRoles : PDMH2MRoles;
                    foreach (string role in PDMRoles)
                    {
                        if (this.UserInfo.IsInRole(role))
                        {
                            test = role;
                            break;
                        }
                    }
                }

                //truncate final role -- may be 'u'
                switch (test)
                {
                    case "PDM LRH Product Development":
                        raw = "pd";
                        break;
                    case "PDM H2M Product Development":
                        raw = "pd";
                        break;
                    case "Administrator": //this is wrong, correct role is Administrators
                        raw = "ad";
                        break;
                    case "Administrators":
                        raw = "ad";
                        break;
                    default:
                        raw = test.Replace("PDM", "").Replace("LRH", "").Replace("H2M", "").Trim().Substring(0, 1).ToLower();
                        break;
                }

                return raw;
            }
        }

        protected void VerifyDataListHasData(object sender, EventArgs e)
        {
            DataList lst = (DataList)sender;
            DataTable dt = (DataTable)(lst.DataSource);

            string id = lst.Attributes["NoDataLabel"];
            if (string.IsNullOrEmpty(id)) return;

            Label lbl = (Label)this.FindControl(id);
            if (lbl == null) return;

            bool hasData = false;
            if (dt != null) hasData = (dt.Rows.Count > 0);

            lbl.Visible = !hasData;
        }

        protected string sku { get { return GetUserSetting<string>(PartId, "Sku", string.Empty); } }

        protected string partStatus
        {
            get
            {
                return GetUserSetting<string>(PartId, "Status", "Draft");
                //string status = "Draft";
                //if (Request.QueryString["status"] != null) return Request.QueryString["status"];
                //if (Request.QueryString["s"] != null) return Request.QueryString["s"];
                //return status;
            }
        }

        protected void DoLookup(object source, DotNetNuke.UI.WebControls.DNNTextSuggestEventArgs e)
        {
            DNNTextSuggest txtSearch = (DNNTextSuggest)source;
            txtSearch.CaseSensitive = false;

            string fieldName = txtSearch.Attributes["Field"];
            string entry = e.Text.ToUpper();
            DNNNodeCollection objNodes = e.Nodes;

            entry = entry.Replace("[", "").Replace("]", "").Replace("'", "''");

            if (string.IsNullOrEmpty(entry.Trim())) return;


            string keyParam = entry.ToUpper().Substring(0, 1);
            string key = string.Format("PDM_{2}{0}_{1}", fieldName, keyParam, entityId);

            string queryTmpl8 = txtSearch.Attributes["queryTmpl8"];
            string filterTmpl8 = txtSearch.Attributes["filterTmpl8"];
            string dataBase = txtSearch.Attributes["dataBase"];

            //switch (fieldName)
            //{
            //    case "ParentPartSuggest": 
            //        queryTmpl8 = "SELECT ID, DESCRIPTION AS NAME FROM vwPARTS WHERE ID LIKE '{0}%' ORDER BY ID";
            //        filterTmpl8 = "ID LIKE '{0}%'";
            //        break;
            //    case "MainComponentPartSuggest":
            //        queryTmpl8 = "SELECT ID, DESCRIPTION AS NAME FROM vwPARTS WHERE ID LIKE '{0}%' ORDER BY ID";
            //        filterTmpl8 = "ID LIKE '{0}%'";
            //        break;
            //    default:
            //        //PreferredVendor, PreferredVendorId, MoldShareVendor
            //        queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME";
            //        filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'";
            //        break;

            //}

            Controller controller = new Controller(entityId, environment);
            SvcLibrary.DBHandler db = controller.pdm;

            //placeholder in case we need to add support for calls to vmfg/dcms
            if (!string.IsNullOrEmpty(dataBase))
            {
                switch (dataBase)
                {
                    case "VMFG":
                        db = controller.vmfg;
                        break;
                    case "DCMS":
                        db = controller.dcms;
                        break;
                    default:
                        db = controller.pdm;
                        break;
                }
            }

            DataTable dt = new DataTable();
            if (Application[key] == null)
            {
                dt = db.ExecuteReader(string.Format(queryTmpl8, keyParam));
                Application[key] = dt;
            }
            else
            {
                dt = (DataTable)Application[key];
            }

            string filter = string.Format(filterTmpl8, entry);

            DataRow[] arr = dt.Select(filter);
            int items = 0;
            DNNNode o;

            foreach (DataRow dr in arr)
            {
                string Id = db.GetValue<string>(dr, "ID", string.Empty);
                string Name = db.GetValue<string>(dr, "NAME", string.Empty).Replace("[", "(").Replace("]", ")");

                string displayText = string.Empty;
                if (Id.StartsWith(entry))
                {
                    displayText = Id + " - " + Name;
                }
                else if (Name.StartsWith(entry))
                {
                    displayText = Name + " [" + Id + "]";
                }

                if (!string.IsNullOrEmpty(displayText))
                {
                    o = new DNNNode(displayText);
                    o.ID = Id;
                    o.set_CustomAttribute("selectedID", Id);
                    objNodes.Add(o);

                    items++;

                }

                if (items >= 10) break;
            }

        }

    }

    public partial class LRHViewer : LRHBase
    {
        protected int ParentModuleId
        {
            get
            {
                return GetUserSetting<int>(PartId, "ModuleId", -1);
            }
        }

        protected bool Initialized
        {
            get
            {
                HiddenField hdn = (HiddenField)this.FindControl("hdnInitialized");
                if (hdn == null) return false; //fuck
                string raw = hdn.Value;
                bool retval = false;
                bool.TryParse(raw, out retval);
                return retval;
            }

            set
            {
                HiddenField hdn = (HiddenField)this.FindControl("hdnInitialized");
                if (hdn == null) return;
                hdn.Value = value.ToString();
            }
        }

        public virtual bool Initialize()
        {
            if (Initialized) return true;


            Panel pnl = (Panel)this.FindControl("pnlActions");
            if (pnl != null)
            {
                pnl.Visible = false;
                string raw = this.Attributes["Editors"];
                
                if ( !string.IsNullOrEmpty(raw))
                {
                    string[] editors = raw.Split('|');
                    foreach (string editor in editors)
                    {
                        string role = editor.Split('-')[0];
                        if (role.Equals(ViewRole))
                        {
                            string states = editor.Split('-')[1];
                            if (states.Equals("*"))
                            {
                                pnl.Visible = true;
                                break;
                            }

                            string status = GetUserSetting<string>(PartId, "Status", "Draft").Substring(0, 1).ToLower();
                            if (states.Contains(status))
                            {
                                pnl.Visible = true;
                                break;
                            }

                            break;
                        }
                    }
                }

                HyperLink lnk = (HyperLink)pnl.FindControl("lnkEdit");
                if (lnk != null)
                {
                    string editor = this.Attributes["Editor"];
                    lnk.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), editor, "id", PartId.ToString(), "r", ViewRole, "e", environment, "eid", entityId);
                }

                lnk = (HyperLink)pnl.FindControl("lnkLists");
                if (lnk != null)
                {
                    string tabName = lnk.Attributes["TabName"];
                    lnk.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "ListManager", "id", PartId.ToString(), "r", ViewRole, "e", environment, "eid", entityId, "tb", tabName);
                }
            }

            Initialized = true;
            return false;
        }

     
    }

    public partial class LRHTab : LRHBase
    {

        public void Initialize()
        {
            int selIndex = GetUserSetting<int>(PartId, "SubTab", 0);

            Panel pnlMain = (Panel)this.FindControl("pnlMain");
            if (pnlMain != null)
            {
                RadTabStrip tabs = (RadTabStrip)pnlMain.FindControl("tabs");
                RadMultiPage radTabs = (RadMultiPage)pnlMain.FindControl("radTabs");

                if (tabs != null)
                {
                    if (tabs.Tabs.Count - 1 < selIndex) selIndex = 0;
                }

                if (tabs != null) tabs.SelectedIndex = selIndex;
                if (radTabs != null) radTabs.SelectedIndex = selIndex;
                //radTabs.SelectedPageView = radTabs.PageViews[selIndex];
                ToggleTab();
            }
            //RemoveUserSetting(PartId, "SubTab");
        }

        protected virtual void ToggleTab() { }

    }

    public abstract class LRHPopup : LRHViewer
    {
        protected abstract string PAGE_TITLE { get; }

        protected virtual void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!IsPostBack)
                {
                    PopulateControls();
                    LockDownControls();
                    SetRequirements();
                    DataSet ds  = LoadData();
                    SetDefaultValues(ds);
                    
                    //NO!! This is wrong because, yep...specs suck.
                    //PostApprovalLoad();
                }

                DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
                string title = PAGE_TITLE;
                string mSku = sku;
                if (!string.IsNullOrEmpty(mSku)) title += " - " + mSku;
                currPage.Title = title;
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        protected virtual void PopulateControls() { }

        protected virtual void LockDownControls() { }

        protected virtual void SetDefaultValues(DataSet ds) { }

        protected virtual void SetRequirements() { }

        protected virtual DataSet LoadData()
        {
            DataSet ds = new DataSet();

            //for base
            Panel pnl = (Panel)this.FindControl("pnlMain");
            if (pnl != null)
            {
                Controller controller = new Controller(entityId, environment);
                ds = controller.PopulateForm(PartId, pnl);
            }

            //in override
            return ds;
        }

       

        protected abstract bool Validate();

        protected void CloseForm(params object[] additionalParms)
        {

            string tmpl8 = DotNetNuke.Common.Globals.NavigateURL("", "id", PartId.ToString(), "e", environment, "eid", entityId, "r", ViewRole);


            if (additionalParms != null)
            {
                for (int i = 0; i < additionalParms.Length; i += 2)
                {
                    if (! tmpl8.EndsWith("/")) tmpl8 += "/";
                    tmpl8 += string.Format("{0}/{1}", additionalParms[i], additionalParms[i + 1]);
                }
            }

            Response.Redirect(tmpl8, true);
        }

        protected bool NewRecord(DataSet ds, string tableName)
        {
            if (!ds.Tables.Contains(tableName)) return true;

            DataTable dt = ds.Tables[tableName];
            if (dt == null) return true;

            return dt.Rows.Count == 0;
        }

        protected void SaveTableEdits(Dictionary<string, TableFields> tables, string tableName, string tabName, bool? newRecord, Controller controller = null, string keyField = "PartId")
        {
            if (!tables.ContainsKey(tableName)) return;
            
            TableFields table = tables[tableName];
            if (!table.IsDirty()) return;

            if (controller == null) controller = new Controller(entityId, environment);

            if (!newRecord.HasValue)
            {
                DataSet ds = controller.LoadPart(PartId, tableName);
                newRecord = NewRecord(ds, tableName);
            }

            table.AddField(keyField, null, PartId);
            table.AddField("ModifiedDate", null, DateTime.Now);

            if (tableName.StartsWith("vw")) tableName = tableName.Substring(2);

            if (newRecord.Value)
            {
                table.AddField("CreatedDate", null, DateTime.Now);
                controller.pdm.InsertRecord(tableName, keyField, table.GetParametersAsArray(true));
                controller.history.Log(PartId, this.UserInfo.Username, tabName, string.Empty, string.Empty, string.Format("Added Record"));
            }
            else
            {
                controller.pdm.UpdateRecord(tableName, keyField, PartId, table.GetParametersAsArray(true, keyField));
                controller.history.Log(PartId, this.UserInfo.Username, tabName, table.Fields, "ModifiedDate", "CreatedDate", keyField);
            }

        }

        

        //putting these here since used more than one location, but not every location
        protected void FixAges(TextBox txt)
        {
            string test = txt.Text.Trim();
            decimal decVal = 0;
            if (decimal.TryParse(test, out decVal))
            {
                if ((decimal)((int)decVal) == decVal)
                    txt.Text = ((int)decVal).ToString();
                else
                    txt.Text = decVal.ToString("#.0");
            }
            else
                txt.Text = string.Empty;

        }

        //this is crap, but I imagine will be needed at some point
        protected virtual void PostApprovalLoad()
        {
             Panel pnlMain = (Panel)this.FindControl("pnlMain");
             if (pnlMain != null)
             {
                 Controller controller = new Controller(entityId, environment);
                 controller.PostApprovalLockdown(pnlMain);
             }
        }

    
    }

  
    
}


