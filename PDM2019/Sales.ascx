<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Sales.ascx.cs" Inherits="YourCompany.Modules.PDM2019.Sales" %>

<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Paging" TagPrefix="pg" Namespace="Paging" %>
<%@ Register Assembly="FilterParser" TagPrefix="fltr" Namespace="FilterParser" %>

<style>
    p { padding-top:4px;
        margin-bottom: 0px;
    }
    .suggestVendorId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:275px; top:155px; font-size:small;}          
</style>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        var url = jQuery('[id*="hdnPDF"]').val();
        if (url != "") {
            window.open(url);
        }
        jQuery('[id*="hdnPDF"]').val("");

    });


</script>

<pg:Paging id="Paging" runat="server" ShowPageSize="true" ShowCurrentPage="true" ShowNavigator="true" Width="780"  />

<table width="780" class="HeaderRow">
    <tr>
        <td style="width:545px; padding:2px 0px 2px 2px;">
            <DNN:DNNTextSuggest ID="txtVendorId" runat="server"
                                    onpopulateondemand="DoLookup" MaxSuggestRows="10"
                                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggestVendorId"
                                    NameIDSplit="-" Width="500"
                                    queryTmpl8 = "SELECT ID, NAME FROM CUSTOMER WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                                    filterTmpl8 = "id like '{0}%' OR NAME LIKE '{0}%'"
                                    dataBase = "VMFG"

                            />   
            <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/images/icon_search_16px.gif" OnCommand="DoFilter"/>
            <asp:ImageButton ID="btnClear" runat="server" ImageUrl="~/images/Search/clearText.png" OnCommand="ClearFilter" />
        </td>
        <td style="width:125px;">&nbsp;</td>
        <td style="width:110px;padding:2px 2px 2px 0px;" align="right">&nbsp;<asp:HyperLink ID="lnkEdit" runat="server" ImageUrl="~/images/plus2.gif" /></td>
    </tr>
</table>

<asp:Panel ID="pnlParts" runat="server" Width="780" Height="500" ScrollBars="Auto" style="border-bottom:solid 1px black;">
        <asp:Repeater ID="lstSales" runat="server" >
            <HeaderTemplate>
                <table width="760"  class="table table-hover table-condensed">
            </HeaderTemplate>

            <ItemTemplate>
                    <tr>
                        <td style="width:100%" align="left" valign="top">
                            <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                <tr>
                                    <td colspan="10" align="left" valign="top">
                                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/delete.gif" OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');" Visible='<%# CanUserEdit %>'
                                            OnCommand="DoDelete" CustomerId='<%# DataBinder.Eval(Container.DataItem, "CUSTOMER_ID") %>'
                                         />
                                        <asp:Label ID="lblspace" runat="server" Width="10" Text="&nbsp;" />
                                        <asp:HyperLink ID="lblCustomer" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "CUSTOMER_SUGGEST") %>' NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "Url") %>' />
                                    </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="2" align="right" valign="top" style="font-weight:bold;">Customer Part Id:</td>
                                    <td colspan="2" align="left" valign="top" style="padding-left:2px;"><%# DataBinder.Eval(Container.DataItem, "CUSTOMER_PART_ID") %></td>
                                    <td colspan="2" align="right" valign="top" style="font-weight:bold;">Unit of Measure:</td>
                                    <td align="left" valign="top" style="padding-left:2px;"><%# DataBinder.Eval(Container.DataItem, "SELLING_UM") %></td>
                                    <td colspan="2" align="right" valign="top" style="font-weight:bold;">Default Price:</td>
                                    <td align="right" valign="top" ><%# DataBinder.Eval(Container.DataItem, "DefaultPrice") %></td>
                                </tr>

                                <tr>
                                    <td style="width:90px;font-weight:bold;" align="right" valign="top">Quantity:</td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_1") %></td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_2") %></td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_3") %></td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_4") %></td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_5") %></td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_6") %></td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_7") %></td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_8") %></td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_9") %></td>
                                    <td style="width:60px;" align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "QTY_BREAK_10") %></td>
                                </tr>

                                <tr>
                                    <td align="right" valign="top" style="font-weight:bold;">Unit Price:</td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice1") %></td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice2") %></td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice3") %></td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice4") %></td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice5") %></td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice6") %></td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice7") %></td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice8") %></td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice9") %></td>
                                    <td align="right" valign="top"><%# DataBinder.Eval(Container.DataItem, "UnitPrice10") %></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
            </ItemTemplate>


            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>

 
        <asp:Label ID="lblNoPriceBreaks" runat="server" Width="760" style="padding-left:25px;" Text="No Price Breaks found." />
            
</asp:Panel>



<fltr:FilterParser id="Filter" runat="server" FilterName="PDMFilterSales" SkipParse="true" />

<asp:HiddenField ID="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnPDF" runat="server" Value="" />
<%--<asp:HiddenField ID="hdnModuleId" runat="server" Value="-1" />--%>

<asp:HiddenField ID="hdnCustomerId" runat="server" Value="" />
<asp:HiddenField ID="hdnPartStatus" runat="server" Value="" />
<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />