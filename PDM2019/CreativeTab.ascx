<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreativeTab.ascx.cs" Inherits="YourCompany.Modules.PDM2019.CreativeTab" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }
   
</style>


<script language="javascript" type="text/javascript">
    function ToggleCreativeMore(toggleWhat) {
        var link = jQuery('[id*="lnkCreativeViewMore' + toggleWhat + '"]');
        var lbl = jQuery('[id*="lblCreative' + toggleWhat + '"]');


        if (link.text() == "View More") {
            link.text("View Less");
            lbl.css("max-height", "");
        }
        else {
            link.text("View More");
            lbl.css("max-height", "200px");
        }
        return false;
    }
</script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>

    <asp:Panel id="pnlCreative" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead" valign="top">Links To Documents:</td>
                <td style="width:600px; border:solid 1px #e8e2e2;padding:2px;" colspan="4">
                    <asp:Label id="lblCreativeDocuments" runat="server" CssClass="Normal" Table="vwCreativeInfos" Field="LinksToDocumentsBR" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkCreativeViewMoreDocuments" runat="server" Text="View More" OnClientClick="javascript:return ToggleCreativeMore('Documents');" /></div>
                </td>
            </tr>


            <tr>
                <td class="SubSubHead" valign="top">Other Links:</td>
                <td style="border:solid 1px #e8e2e2;padding:2px;" colspan="4">
                    <asp:Label id="lblCreativeOther" runat="server" CssClass="Normal" Table="vwCreativeInfos" Field="OtherLinksBR" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkCreativeViewMoreOther" runat="server" Text="View More" OnClientClick="javascript:return ToggleCreativeMore('Other');" /></div>
                </td>
            </tr>

            <tr>
                <td class="SubSubHead" valign="top">What's in the Box:</td>
                <td style="border:solid 1px #e8e2e2;padding:2px;" colspan="4">
                    <asp:Label id="lblCreativeBox" runat="server" CssClass="Normal" Table="vwCreativeInfos" Field="WhatsInBoxBR" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkCreativeViewMoreBox" runat="server" Text="View More" OnClientClick="javascript:return ToggleCreativeMore('Box');" /></div>
                </td>
            </tr>

                <tr>
                    <td class="SubSubHead">Main Image:</td>        
                    <td colspan="4"><asp:Label ID="lblMainImage" runat="server" CssClass="Normal placeholder" Width="580" Table="vwCreativeInfos" Field="MainImage" /></td>
                </tr>
                <tr>
                    <td class="SubSubHead">Image 2:</td>        
                    <td colspan="4"><asp:Label ID="Label1" runat="server" CssClass="Normal placeholder" Width="580" Table="vwCreativeInfos" Field="Image2" /></td>
                </tr>
                <tr>
                    <td class="SubSubHead">Image 3:</td>        
                    <td colspan="4"><asp:Label ID="Label3" runat="server" CssClass="Normal placeholder" Width="580" Table="vwCreativeInfos" Field="Image3" /></td>
                </tr>
                <tr>
                    <td class="SubSubHead">Image 4:</td>        
                    <td colspan="4"><asp:Label ID="Label5" runat="server" CssClass="Normal placeholder" Width="580" Table="vwCreativeInfos" Field="Image4" /></td>
                </tr>
                <tr>
                    <td class="SubSubHead">Image 5:</td>        
                    <td colspan="4"><asp:Label ID="Label6" runat="server" CssClass="Normal placeholder" Width="580" Table="vwCreativeInfos" Field="Image5" /></td>
                </tr>
                <tr>
                    <td class="SubSubHead">Image 6:</td>        
                    <td colspan="4"><asp:Label ID="Label7" runat="server" CssClass="Normal placeholder" Width="580" Table="vwCreativeInfos" Field="Image6" /></td>
                </tr>
                <tr>
                    <td class="SubSubHead">Image 7:</td>        
                    <td colspan="4"><asp:Label ID="Label8" runat="server" CssClass="Normal placeholder" Width="580" Table="vwCreativeInfos" Field="Image7" /></td>
                </tr>

                <tr>
                    <td class="SubSubHead"><asp:Image ID="imgElectronic" runat="server" Table="vwPartSpecifications" Field="Electronic" />&nbsp;Electronic</td>
                    <td class="SubSubHead">Power Sources:</td>
                    <td class="SubSubHead"><asp:Image ID="imgAC" runat='server' Table="vwPartSpecifications" Field="ACPower" />&nbsp;A/C Power</td>
                    <td class="SubSubHead"><asp:Image ID="imgusbpower" runat="server" Table="vwPartSpecifications" Field="USBPower" />&nbsp;USB Power</td>
                    <td class="SubSubHead"><asp:Image ID="imgbatteries" runat="server" Table="vwPartSpecifications" Field="Batteries" />&nbsp;Batteries Required</td>

                </tr>
            </table>

            <table width="750" cellpadding="2" cellspacing="0" border="0">
                <tr>
                    <td class="SubSubHead" style="width:225px;">Piece Count on Box:</td>
                    <td style="width:100px;" ><asp:Label ID="lblPieceCount" runat="server" CssClass="Normal placeholder" Width="80" Table="vwCreativeInfos" Field="PieceCountOnBox" /></td>
                    <td class="SubSubHead" style="width:125px;"># of Players:</td>
                    <td style="width:150px;"><asp:Label ID="lblNumberofplayers" runat="server" CssClass="Normal placeholder" Width="130" Table="vwCreativeInfos" Field="NumberOfPlayers" /></td>
                    <td class="SubSubHead" style="width:150px;"><asp:Image ID="imgAssembly" runat="server" Table="vwPartInfos" Field="AssemblyRequired" />&nbsp;Assembly Required</td>
                </tr>

            <tr>
                <td class="SubSubHead">Target Age (Min):</td>
                <td ><asp:Label ID="lblTargetAgeMin" runat="server" CssClass="Normal placeholder" Width="50" Table="vwPartInfos" Field="MinAge" Format="Decimal" Places="1" /></td>
                <td  class="SubSubHead">Target Age (Max):</td>
                <td ><asp:Label ID="lblTargetAgeMax" runat="server" CssClass="Normal placeholder" Width="50" Table="vwPartInfos" Field="MaxAge" Format="Decimal" Places="1" /></td>
                <td >&nbsp;</td>
            </tr>
            <tr>
                <td class="SubSubHead">Target Grade (Min):</td>
                <td><asp:Label ID="Label15" runat="server" CssClass="Normal placeholder" Width="100" Table="vwPartInfos" Field="MinGrade" /></td>
                <td class="SubSubHead">Target Age (Max):</td>
                <td><asp:Label ID="Label16" runat="server" CssClass="Normal placeholder" Width="100" Table="vwPartInfos" Field="MaxGrade" /></td>
                <td></td>
            </tr>
  
            <tr>
                <td class="SubSubHead">Preferred Vendor:</td>
                <td colspan="4"><asp:Label ID="lblpvid" runat="server" CssClass="Normal required placeholder" table="vwPartInfos" field="PreferredVendorSuggest" Width="580" />
            </tr>

            <tr>
                <td class="SubSubHead" style="padding-top:10px;" colspan="5">Package Warnings</td>
            </tr>

            <tr>
                <td class="SubSubHead" colspan="5" style="padding-left:25px;">
                    <asp:Image id="imgcewarning" runat="server" Table="vwQAPartInfos" Field="CEWarning" />&nbsp;CE 
                    &nbsp;&nbsp;<asp:Image id="Image1" runat="server" Table="vwQAPartInfos" Field="SmallPartsWarning" />&nbsp;Small Parts 
                    &nbsp;&nbsp;<asp:Image id="Image2" runat="server" Table="vwQAPartInfos" Field="SmallBallWarning" />&nbsp;Small Ball 
                    &nbsp;&nbsp;<asp:Image id="Image3" runat="server" Table="vwQAPartInfos" Field="OwlPellets" />&nbsp;Owl Pellets
                    &nbsp;&nbsp;<asp:Image id="Image4" runat="server" Table="vwQAPartInfos" Field="MarbleWarning" />&nbsp;Marble 
                    &nbsp;&nbsp;<asp:Image id="Image5" runat="server" Table="vwQAPartInfos" Field="LatexBalloonsWarning" />&nbsp;Latext Balloons
                    &nbsp;&nbsp;<asp:Image id="Image15" runat="server" Table="vwQAPartInfos" Field="NoBabyWarning" />&nbsp;No Baby
                </td>
            </tr>
            <tr>
                <td class="SubSubHead" colspan="5" style="padding-left:25px;">
                    <asp:Image id="Image7" runat="server" Table="vwQAPartInfos" Field="CordWarning" />&nbsp;Cord
                    &nbsp;&nbsp;<asp:Image id="Image8" runat="server" Table="vwQAPartInfos" Field="MagnetWarning" />&nbsp;Magnet
                    &nbsp;&nbsp;<asp:Image id="Image9" runat="server" Table="vwQAPartInfos" Field="SharpFunctionalPointWarning" />&nbsp;Sharp Functional Point
                    &nbsp;&nbsp;<asp:Image id="Image10" runat="server" Table="vwQAPartInfos" Field="MarbleInKit" />&nbsp;Marble In Kit
                    &nbsp;&nbsp;<asp:Image id="Image11" runat="server" Table="vwQAPartInfos" Field="BallInKit" />&nbsp;Ball In Kit
                    &nbsp;&nbsp;<asp:Image id="Image36" runat="server" Table="vwQAPartInfos" Field="Projectile" />&nbsp;Projectile
                </td>
            </tr>

            <tr>
                <td class="SubSubHead" colspan="5" style="padding-left:25px;">
                    <asp:Image id="Image12" runat="server" Table="vwQAPartInfos" Field="NotHumanConsumption" />&nbsp;Not for Human Consumption
                    &nbsp;&nbsp;<asp:Image id="Image25" runat="server" Table="vwQAPartInfos" Field="NotSafetyProtection" />&nbsp;Not for Safety Protection
                    &nbsp;&nbsp;<asp:Image id="Image17" runat="server" Table="vwQAPartInfos" Field="AdultSupervision" />&nbsp;Adult Supervision
                    &nbsp;&nbsp;<asp:Image id="Image18" runat="server" Table="vwQAPartInfos" Field="Prop65CarcinogensWarning" />&nbsp;Prop65-Carcinogens
                    
                </td>
            </tr>

            <tr>
                <td class="SubSubHead" colspan="5" style="padding-left:25px;">
                    <asp:Image id="Image24" runat="server" Table="vwQAPartInfos" Field="MeetsANSIZ87" />&nbsp;Meets ANSIZ87.1 Standards
                    &nbsp;&nbsp;<asp:Image id="Image20" runat="server" Table="vwQAPartInfos" Field="LatextWarning" />&nbsp;Latex Warning
                    &nbsp;&nbsp;<asp:Image id="Image21" runat="server" Table="vwQAPartInfos" Field="Prop65ReproductiveToxicantsWarning" />&nbsp;Prop65-Reproductive Toxicants
                </td>
            </tr>

          
            <tr>
                <td class="SubSubHead" style="padding-left:25px;"><asp:Image id="Image16" runat="server" Table="vwQAPartInfos" Field="CCCWarning" />&nbsp;CCC</td>
                <td class="SubSubHead" colspan="3">Description:&nbsp;<asp:Label ID="lblCCCDescription" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="CCCDescription" /></td>
                <td class="SubSubHead"  >CCC Date:&nbsp;<asp:Label ID="cccdate" runat="server" CssClass="Normal placeholder" Width="75" Table="vwQAPartInfos" Field="CCCDate" Format="Date" /></td>
            </tr>

            <tr>
                <td class="SubSubHead" style="padding-left:25px;"><asp:Image id="Image13" runat="server" Table="vwQAPartInfos" Field="WashBeforeUseWarning" />&nbsp;Wash Before Use</td>
                <td class="SubSubHead" colspan="4">Description:&nbsp;<asp:Label ID="Label2" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="WashBeforeUseDescription" /></td>
            </tr>

            <tr>
                <td class="SubSubHead" style="padding-left:25px;"><asp:Image id="Image14" runat="server" Table="vwQAPartInfos" Field="ShellfishWarning" />&nbsp;Shell Fish</td>
                <td class="SubSubHead" colspan="4">Description:&nbsp;<asp:Label ID="Label4" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="ShellfishDescription" /></td>
            </tr>
	


        </table>


    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




