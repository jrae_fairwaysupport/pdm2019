<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDPackaging.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PDPackaging" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPackaging" runat="server" Width="750" Height="600" ScrollBars="Auto">

        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td colspan="5" class="SubSubHead">Type of Packaging:</td>
            </tr>
            <tr>
                <td style="width:125px; padding-left:25px;" class="SubSubHead"><asp:Image ID="Image8" runat="server" Table="Parts" Field="Header" />&nbsp;Header</td>
                <td style="width:125px;" class="SubSubHead"><asp:Image ID="Image9" runat="server" Table="Parts" Field="Polybag" />&nbsp;Polybag</td>
                <td style="width:125px;" class="SubSubHead"><asp:Image ID="Image10" runat="server" Table="Parts" Field="Tub" />&nbsp;Tub</td>
                <td style="width:125px;" class="SubSubHead"><asp:Image ID="Image11" runat="server" Table="Parts" Field="Blister" />&nbsp;Blister/Clam</td>
                <td style="width:125px;" class="SubSubHead"><asp:Image ID="Image1" runat="server" Table="Parts" Field="Box" />&nbsp;Box</td>
                <td style="width:125px;" class="SubSubHead"><asp:Image ID="Image2" runat="server" Table="Parts" Field="FFP" />&nbsp;FFP</td>
            </tr>
            <tr>
                <td style="padding-left:25px;" class="SubSubHead" colspan="6">Other:&nbsp;<asp:Label ID="lblOtherPackaging" runat="server" CssClass="Normal placeholder" Width="670" Table="Parts" Field="OtherPackaging" /></td>
            </tr>


            <tr>
                <td class="SubSubHead" colspan="6"><asp:Image ID="chkMultilng" runat="server" Table="Parts" Field="MultiLingual" />&nbsp;Multilingual Packaging</td>
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-left:25px;"><asp:Image ID="chkwifi" runat="server" Table="Parts" Field="EnglishPackaging" />&nbsp;English</td>
                <td class="SubSubHead"><asp:Image ID="chkpowerswitch" runat="server" Table="Parts" Field="SpanishPackaging" />&nbsp;Spanish</td>
                <td class="SubSubHead"><asp:Image ID="chkPowerIndicator" runat="server" Table="Parts" Field="FrenchPackaging" />&nbsp;French</td>
                <td class="SubSubHead"><asp:Image ID="chkledpower" runat="server" Table="Parts" Field="GermanPackaging" />&nbsp;German</td>
                <td />
                <td />
            </tr>
            <tr>
                <td class="SubSubHead" style="padding-left:25px;" colspan="6">Other:&nbsp;<asp:Label ID="lblOtherLanguage" runat="server" CssClass="Normal placeholder" Width="670" Table="Parts" Field="OtherLanguagePackaging" /></td>
            </tr>

           <tr>
                <td class="SubSubHead" colspan="2">Age on Box at Part Creation:&nbsp;<asp:Label ID="lblAgeOnBox" runat="server" CssClass="Normal placeholder" Width="50" Table="Parts" Field="AgeOnBoxAtPartCreation" Format="Decimal" Places="1" /></td>
                <td class="SubSubHead" colspan="2">Age at Shipment:&nbsp;<asp:Label ID="lblLengh" runat="server" CssClass="Normal placeholder" Table="Parts" Field="AgeAtShipment" Width="50" Format="Decimal" Places="1"/></td>
                <td class="SubSubHead" colspan="2">Piece Count on Box:&nbsp;<asp:Label ID="lblPieceCountOnBox" runat="server" CssClass="Normal placeholder" Width="50" Table="vwCreativeInfos" Field="PieceCountOnBox" Format="Number" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" colspan="2">Retail Package Size (in.):</td>
                <td class="SubSubHead">Length:&nbsp;<asp:Label ID="lblrpsl" runat="server" CssClass="Normal placeholder" Width="50" Format="Decimal" Places="2" Table="vwCreativeInfos" Field="RetailPackageSizeLength" /></td>
                <td class="SubSubHead">Width:&nbsp;<asp:Label ID="Label1" runat="server" CssClass="Normal placeholder" Width="50" Format="Decimal" Places="2" Table="vwCreativeInfos" Field="RetailPackageSizeWidth" /></td>
                <td class="SubSubHead">Height:&nbsp;<asp:Label ID="Label2" runat="server" CssClass="Normal placeholder" Width="50" Format="Decimal" Places="2" Table="vwCreativeInfos" Field="RetailPackageSizeHeight" /></td>
                <td />
            </tr>
        </table>
                

    </asp:Panel>
</asp:Panel>

<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />