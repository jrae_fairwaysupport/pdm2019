using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class PartInformationEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Part Information"; }}

        protected override void PopulateControls()
        {
            ddlCompany.Items.Clear();
            ddlCompany.Items.Add(new ListItem("Company", ""));

            if (entityId.Equals("LRH"))
            {
                ddlCompany.Items.Add(new ListItem("Educational Insights", "EI"));
                ddlCompany.Items.Add(new ListItem("Learning Resources", "LR"));
            }
            else
            {
                ddlCompany.Items.Add(new ListItem("hand2mind", "H2M"));
            }
         }
    
        protected override void SetRequirements()
        {
            if (partStatus.Equals("Draft")) return;

            labelCompany.CssClass = "SubSubHead required";
            labelPreferredVendor.CssClass = "SubSubHead required";
            labelMaterialCost.CssClass = "SubSubHead required";
            labelLeadTime.CssClass = "SubSubHead required";
            labelMinimumOrderQuantity.CssClass = "SubSubHead required";
            labelYearIntroduced.CssClass = "SubSubHead required";
            labelFirstYearForecast.CssClass = "SubSubHead required";

        }
        
        protected override void LockDownControls()
        {
            if (!partStatus.Equals("Pending")) return;

            //chkSellable.Enabled = false;
            txtFirstYearForecast.Enabled = false;
            chkMoldShare.Enabled = false;
            txtMoldShareVendor.Enabled = false;
            checkRoyaltyItem.Enabled = false;
            txtRoyaltyPayableTo.Enabled = false;
            rdoRoyaltyType.Enabled = false;
            txtRoyaltyAmount.Enabled = false;
        }

        protected override void SetDefaultValues(DataSet ds)
        {
            bool newRecord = NewRecord(ds, "vwPartInfos");

            chkToggleLabels.Checked = !newRecord;


            if (!newRecord) return;

            //chkSellable.Checked = true;
            rdoPurchasedOrFabricated.SelectedValue = "Fabricated";
            //rdoIsNewPart.SelectedValue = "New Part";
        }

        protected override DataSet LoadData()
        {
            DataSet ds = base.LoadData();

            FixAges(txtAgeMin);
            FixAges(txtAgeMax);
            return ds;
        }

        protected override bool Validate()
        {
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            bool required = !partStatus.Equals("Draft");

            //required fields first
            string company = controller.ValidateText(ddlCompany, required, "Company", ref msg);
            


            //client side style changes don't seem to be recognized for this control at the server, soo....
            string sel = rdoPurchasedOrFabricated.SelectedValue;
            if (string.IsNullOrEmpty(sel)) sel = string.Empty;

            string loc = ddlFabricationLocation.SelectedValue;
            if (string.IsNullOrEmpty(loc)) loc = string.Empty;

            if (!(sel.Equals("Fabricated") && loc.Equals("FabUs")))
            {
                if (txtPreferredVendor.Enabled) controller.ValidateText(txtPreferredVendor, required, "Preferred Vendor", ref msg);

                if (txtMaterialCost.Visible) controller.ValidateDecimal(txtMaterialCost, required, "Material Cost", ref msg);
            }
            else
            {
                //we can't set value to null in visual, so
                if (string.IsNullOrEmpty(txtMaterialCost.Text.Trim())) txtMaterialCost.Text = "0";
            }
            

            int? leadTime = controller.ValidateNumber(txtLeadTime, required, "Lead Time", ref msg);
            int? minOrderQty = controller.ValidateNumber(txtMinimumOrderQuantity, required, "Minimum Order Quantity", ref msg);
            decimal? yearIntroduced = controller.ValidateDecimal(txtYearIntroduced, required, "Year Introduced", ref msg);
            int? firstYearForecast = controller.ValidateNumber(txtFirstYearForecast, required, "First Year Forecast", ref msg);
            controller.ValidateNumber(txtMultiplesOf, false, "Multiples Of", ref msg);
            //if (rdoIsNewPart.SelectedValue.Equals("Revision"))
            //{
            //    if (required)
            //    {
            //        string parentPartId = controller.ValidateText(txtParentPartId, true, "Master Part Id is required when the Part is a Version of an Existing Part.", ref msg);
            //    }
            //}

            if (rdoPurchasedOrFabricated.SelectedValue.Equals("Fabricated"))
            {
                if (required)
                {
                    controller.ValidateText(ddlFabricationLocation, true, "Fabrication Location", ref msg);
                }
            }

            if (ddlFabricationLocation.SelectedValue.Equals("FabUs"))
            {
                if (required)
                {
                    decimal? unitAssemblyCost = controller.ValidateDecimal(txtUnitAssemblyCost, true, "Unit Assembly Cost is required when the Fabrication Location is Fab US.", ref msg);
                }
            }
            else
            {
                controller.ValidateDecimal(txtUnitAssemblyCost, false, "Unit Assembly Cost", ref msg);
            }


            string raw = txtYearIntroduced.Text.Trim();
            if (!string.IsNullOrEmpty(raw))
            {
                if (!(raw.Length == 6 || raw.Length == 4)) msg += "\r\nYear Introduced must be either 4 or 6 characters long (eg. 2019.5).";
            }

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowMessage.Value = msg;
                return false;
            }


            //logic errors
            if (checkRoyaltyItem.Checked)
            {
                string royaltyPayableTo = controller.ValidateText(txtRoyaltyPayableTo, true, "Royalty Payable To", ref msg);
                string royaltyType = controller.ValidateText(rdoRoyaltyType, true, "Royalty Type", ref msg);
                if (!string.IsNullOrEmpty(royaltyType))
                {
                    decimal? royaltyValue;
                    if (royaltyType.Equals("PERCENTAGE")) royaltyValue = controller.ValidateDecimal(txtRoyaltyAmount, true, "Royalty Percentage", ref msg);
                    if (royaltyType.Equals("AMOUNT")) royaltyValue = controller.ValidateDecimal(txtRoyaltyAmount, true, "Royalty Amount", ref msg);
                }
                if (!string.IsNullOrEmpty(msg)) msg = "When Part is a Royalty Item:\r\n" + msg + "\r\n\r\n";
            }

            if (chkMoldShare.Checked)
            {
                string subMsg = string.Empty;
                string moldShareVendor = controller.ValidateText(txtMoldShareVendor, true, "Mold Share Vendor Id", ref subMsg);
                if (!string.IsNullOrEmpty(subMsg)) msg += "When Mold Share is checked:\r\n";
                msg += subMsg;
            }


            decimal? ageMin = controller.ValidateDecimal(txtAgeMin, true, "Target Min Age", ref msg);
            decimal? ageMax = controller.ValidateDecimal(txtAgeMax, true, "Target Max Age", ref msg);

            controller.ClearErrorStyles(txtAgeMax);
            if (ageMin.HasValue && ageMax.HasValue)
            {
                if (ageMax < ageMin)
                {
                    msg += "Target Max Age should be at least the same as Target Min Age.\r\n";
                    controller.ApplyErrorStyles(txtAgeMax);
                }
            }


            controller.DNNTestSuggestIsSelectionValid(txtPreferredVendor, "Preferred Vendor", ref msg);
            controller.DNNTestSuggestIsSelectionValid(txtMoldShareVendor, "Mold Share Vendor", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
                
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            txtGradeMax.Text = hdnGradeMax.Value;
            txtGradeMin.Text = hdnGradeMin.Value;

            //clean up data
            bool deleteVendorPriceBreaks = false;
            if (rdoPurchasedOrFabricated.SelectedValue.Equals("Fabricated") && ddlFabricationLocation.SelectedValue.Equals("FabUs"))
            {
                txtPreferredVendor.Text = "99999";
                txtMaterialCost.Text = string.Empty;
                deleteVendorPriceBreaks = true;
            }

            if (rdoPurchasedOrFabricated.SelectedValue.Equals("Purchased"))
            {
                txtUnitAssemblyCost.Text = string.Empty;
                ddlFabricationLocation.ClearSelection();
            }

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "vwPartInfos", "PurchasingPartInfos", "vwVendorPriceBreaks"); //don't use main, because we may have added fields from tables not on form
            bool newRecord = NewRecord(ds, "vwPartInfos");

            if (deleteVendorPriceBreaks)
            {
                DataTable priceBreaks = ds.Tables["vwVendorPriceBreaks"];
                if (priceBreaks.Rows.Count > 0)
                {
                    string sql = "DELETE FROM VendorPriceBreaks WHERE PartVendorPrice_PartId = @P0";
                    controller.pdm.ExecuteCommand(sql, PartId);
                    controller.history.Log(PartId, this.UserInfo.Username, "Product Development.Vendor Price Breaks", string.Empty, string.Empty, "Deleted all Vendor Price Breaks.");
                }

                if (partStatus.Equals("Approved"))
                {
                    string sql = "SELECT * FROM VENDOR_PART WHERE PART_ID = @P0";
                    DataTable dt = controller.vmfg.ExecuteReader(sql, sku);

                    foreach (DataRow dr in dt.Rows)
                    {
                        string vendorId = controller.vmfg.GetValue<string>(dr, "VENDOR_ID", string.Empty);
                        string vendorPartId = controller.vmfg.GetValue<string>(dr, "VENDOR_PART_ID", string.Empty);

                        if (string.IsNullOrEmpty(vendorPartId))
                            sql = "DELETE FROM VENDOR_QUOTE WHERE VENDOR_ID = @P0 AND VENDOR_PART_ID IS NULL";
                        else
                            sql = "DELETE FROM VENDOR_QUOTE WHERE VENDOR_ID = @P0 AND VENDOR_PART_ID = @P1";

                        controller.vmfg.ExecuteCommand(sql, vendorId, vendorPartId);
                    }

                    sql = "DELETE FROM VENDOR_PART WHERE PART_ID = @P0";
                    controller.vmfg.ExecuteCommand(sql, sku);
                }
            }


            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTables(tables, controller);
            SaveTableEdits(tables, "vwPartInfos", "Product Development.Part Information", newRecord, controller);
            SaveTableEdits(tables, "PurchasingPartInfos", "Purchasing.Part Information", NewRecord(ds, "PurchasingPartInfos"), controller);

            SaveOracleData(controller, tables);
            CloseForm();
        }

        private void SaveOracleData(Controller controller, Dictionary<string, TableFields> tables)
        {
            if (!partStatus.Equals("Approved")) return;



            Dictionary<string, object> edits = new Dictionary<string, object>();
            tables["vwPartInfos"].StageOracleEdits(edits, "MinimumOrderQuantity.MINIMUM_ORDER_QTY", "LeadTime.PLANNING_LEADTIME", "MultiplesOf.MULTIPLE_ORDER_QTY",
                                                            "Fabricated.FABRICATED", "MaterialCost.UNIT_MATERIAL_COST", "BuyerUserId.BUYER_USER_ID");
            if (edits.Count > 0)
            {
                if (edits.ContainsKey("FABRICATED")) edits["FABRICATED"] = (bool)edits["FABRICATED"] ? "Y" : "N";
                tables["vwPartInfos"].PushOracleEdits(controller.vmfg, edits, "PART", "ID", sku);
            }

            if (tables["vwPartInfos"].HasFieldChanged("MinimumOrderQuantity"))
                controller.pdm.InsertRecord("CreativeNotifications", "PartId", "PartId", PartId, "ModifiedBy", this.UserId, "ModifiedDate", DateTime.Now,
                                    "ReportType", "PD_TO_PURCHASING", "OldValue", tables["vwPartInfos"].Fields["MinimumOrderQuantity"].oldHistory,
                                    "NewValue", tables["vwPartInfos"].Fields["MinimumOrderQuantity"].newHistory, "Warning", "Min. Order Quantity");

            if (tables["vwPartInfos"].HasFieldChanged("MaterialCost"))
                controller.pdm.InsertRecord("CreativeNotifications", "PartId", "PartId", PartId, "ModifiedBy", this.UserId, "ModifiedDate", DateTime.Now,
                                    "ReportType", "PD_TO_PURCHASING", "OldValue", tables["vwPartInfos"].Fields["MaterialCost"].oldHistory,
                                    "NewValue", tables["vwPartInfos"].Fields["MaterialCost"].newHistory, "Warning", "Material Cost");

        }

        private void PrepTables(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwPartInfos")) return;

            TableFields table = tables["vwPartInfos"];
            if (!table.IsDirty()) return;

            //custom logic
            //moldsharevendor - if removed, make vendorId unset
            if (table.ContainsField("MoldShare"))
            {
                if ((bool)table.Fields["MoldShare"].newValue == false) table.Fields["MoldShareVendorId"].newValue = null;
            }

            //IsNewPart
            if (table.ContainsField("IsNewPartText"))
            {
                if (table.Fields["IsNewPartText"].oldValue == null)
                    table.Fields["IsNewPartText"].oldValue = false;
                else if (table.Fields["IsNewPartText"].oldHistory.Equals("unset"))
                    table.Fields["IsNewPartText"].oldValue = false;
                else if (table.Fields["IsNewPartText"].oldHistory.Equals("Revision"))
                    table.Fields["IsNewPartText"].oldValue = false;
                else
                    table.Fields["IsNewPartText"].oldValue = true;

                //table.Fields["IsNewPartText"].oldValue = (table.Fields["IsNewPartText"].oldHistory.Equals("unset") ? false : true);
                table.Fields["IsNewPartText"].newValue = (table.Fields["IsNewPartText"].newValue.ToString().Equals("New Part") ? true : false);
                table.RenameField("IsNewPartText", "IsNewPart");
            }


            //Fabricated & Purchased
            if (table.ContainsField("FabricatedOrPurchased"))
            {
                string fopOriginal = table.Fields["FabricatedOrPurchased"].oldValue == null ? "" : table.Fields["FabricatedOrPurchased"].oldValue.ToString();
                string fopNew = table.Fields["FabricatedOrPurchased"].newValue == null ? "" : table.Fields["FabricatedOrPurchased"].newValue.ToString();
                table.AddField("Fabricated", fopOriginal.Equals("Fabricated"), fopNew.Equals("Fabricated"));
                table.AddField("Purchased", fopOriginal.Equals("Purchased"), fopNew.Equals("Purchased"));
                table.RemoveField("FabricatedOrPurchased");
            }

            //royalty take 2
            if (table.ContainsField("RoyaltyInitial"))
            {
                //create placeholders
                table.AddField("RoyaltyPercentage", null, table.Fields["RoyaltyInitial"].newValue);
                table.AddField("RoyaltyAmount", null, table.Fields["RoyaltyInitial"].newValue);

                //assign initial values
                string originalType = (string)table.Fields["RoyaltyType"].oldValue;
                if (!string.IsNullOrEmpty(originalType))
                {
                    if (originalType.Equals("PERCENTAGE")) table.Fields["RoyaltyPercentage"].oldValue = table.Fields["RoyaltyInitial"].oldValue;
                    else table.Fields["RoyaltyAmount"].oldValue = table.Fields["RoyaltyInitial"].oldValue;
                }

                //get rid of values we don't need
                string newType = (string)table.Fields["RoyaltyType"].newValue;
                if (!string.IsNullOrEmpty(newType))
                {
                    if (newType.Equals("PERCENTAGE")) table.Fields["RoyaltyAmount"].newValue = null;
                    else table.Fields["RoyaltyPercentage"].newValue = null;
                }

                //see if we're even a royalty item anymore
                if (!(bool)table.Fields["RoyaltyItem"].newValue)
                {
                    table.Fields["RoyaltyPercentage"].newValue = null;
                    table.Fields["RoyaltyAmount"].newValue = null;
                    table.Fields["RoyaltyPayableTo"].newValue = null;
                }

                //get rid of helpers
                table.RemoveField("RoyaltyType");
                table.RemoveField("RoyaltyInitial");
            }

            //PreferredVendor
            if (table.ContainsField("PreferredVendor"))
            {
                if (string.IsNullOrEmpty(((string)table.Fields["PreferredVendor"].newValue)))
                {
                    table.Fields["BuyerUserId"].newValue = null;
                }
                else
                {
                    if (table.Fields["PreferredVendor"].HasChanged() || string.IsNullOrEmpty((string)table.Fields["BuyerUserId"].newValue))
                    {
                        string sql = "SELECT BUYER FROM vwVendors WHERE ID = @P0";
                        table.Fields["BuyerUserId"].newValue = controller.pdm.FetchSingleValue<string>(sql, "BUYER", (string)table.Fields["PreferredVendor"].newValue);
                    }

                }
                if (table.Fields["PreferredVendor"].HasChanged() || table.Fields["BuyerUserId"].HasChanged())
                {
                    tables.Add("PurchasingPartInfos", new TableFields());
                    tables["PurchasingPartInfos"].AddField("PreferredVendorId", table.GetOldValue("PreferredVendor"), table.GetNewValue("PreferredVendor"));
                    tables["PurchasingPartInfos"].AddField("BuyerUserId", table.GetOldValue("BuyerUserId"), table.GetNewValue("BuyerUserId"));
                }
            }

        }
     
    }
}
 