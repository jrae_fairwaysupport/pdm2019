using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
    public partial class AccountingEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Accounting"; } }

        protected override void SetDefaultValues(DataSet ds)
        {
            bool newRecord = NewRecord(ds, "vwPartInfos");
            chkToggleLabels.Checked = !newRecord;

            if (newRecord) txtquantityconversion.Text = "1";

        }


        ////we need the Parts table
        //protected override DataSet LoadData()
        //{
        //    DataSet ds = new DataSet();

        //    //for base
        //    Panel pnl = (Panel)this.FindControl("pnlMain");
        //    if (pnl != null)
        //    {
        //        Controller controller = new Controller(entityId, environment);
        //        ds = controller.PopulateForm(PartId, pnl);
        //    }

        //    //in override
        //    return ds;
        //}

        protected override void PopulateControls()
        {
            Controller controller = new Controller(entityId, environment);
            controller.PopulateDropDownListOptions(ddlCategory, "Accounting", "Category", "Select Category");

            //txtLandedCostPDM.Visible = !partStatus.Equals("Approved");
            //txtLandedCostVMFG.Visible = partStatus.Equals("Approved");

        }
   
        protected override bool Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);
            //controller.ValidateDecimal(txtFreightInCost, false, "Freight in Cost", ref msg);
            //if (partStatus.Equals("Approved")) controller.ValidateDecimal(txtLandedCostVMFG, false, "Landed Cost", ref msg);
            //if (!partStatus.Equals("Approved")) controller.ValidateDecimal(txtLandedCostPDM, false, "Landed Cost", ref msg);
            //controller.ValidateDecimal(txtLandedCost, false, "Landed Cost", ref msg);
            //controller.ValidateDecimal(txtDuties, false, "Duties", ref msg);

            //controller.ValidateDecimal(txtFreightInCost, false, "Freight In Cost", ref msg);
            controller.ValidateDecimal(txtSellingPrice, false, "Selling/Net Price", ref msg);
            controller.ValidateDecimal(txtRetailPrice, false, "Selling/Retail Price", ref msg);
            controller.ValidateNumber(txtquantityconversion, false, "Parent Part Quantity Conversion", ref msg);
            controller.ValidateDecimal(txtDutyAmount, false, "Duty Amount", ref msg);
            controller.ValidateDecimal(txtAddRate, false, "Add Rate", ref msg);
            controller.ValidateNumber(txtFirstYearForecast, false, "First Year Forecast", ref msg);

            hdnShowMessage.Value = msg;

            return string.IsNullOrEmpty(msg);
        }

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {
                CloseForm();
                return;
            }

            if (!Validate()) return;

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwPartInfos");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData_PartInfos(tables, controller);
            //PrepTableData_DCMS(tables, controller); //LandedCost only?
            //PrepTableData_VMFG(tables, controller);

            SaveTableEdits(tables, "vwPartInfos", "Accounting", newRecord, controller);
            SaveTableEdits(tables, "vwPurchasingPartInfos", "Accounting", NewRecord(ds, "vwPurchasingPartInfos"), controller);
            SaveTableEdits(tables, "vwAccounting", "Accounting", NewRecord(ds, "vwAccounting"), controller);
            SaveTableEdits(tables, "vwMarketingInfos", "Accounting", NewRecord(ds, "vwMarketingInfos"), controller);

            //SaveDcmsTableEdits(tables, controller); //LandedCost Only
            
            if (partStatus.Equals("Approved"))
            {
                string msg = string.Empty;
                decimal? netPrice = controller.ValidateDecimal(txtSellingPrice, false, "net price", ref msg);
                decimal? retailPrice = controller.ValidateDecimal(txtRetailPrice, false, "retail price", ref msg);
                string sellable = chkSellable.Checked ? "Y" : "N";

                string sql = "UPDATE PART SET UNIT_PRICE = @P0, WHSALE_UNIT_COST = @P1, LR_SELLABLE = @P2 WHERE ID = @P3";
                controller.vmfg.ExecuteCommand(sql, netPrice, retailPrice, sellable, sku);

                decimal? duties = controller.ValidateDecimal(txtDuties, false, "duties", ref msg);
                int? qtyconv = controller.ValidateNumber(txtquantityconversion, false, "qty conv", ref msg);
                decimal? addRate = controller.ValidateDecimal(txtAddRate, false, "add rate", ref msg);

                sql = "SELECT COUNT(*) AS THECOUNT FROM LR_PART_EXTENSION WHERE PART_ID = @P0";
                int theCount = controller.vmfg.FetchSingleValue<int>(sql, "THECOUNT", sku);
                if (theCount > 0)
                {
                    sql = "UPDATE LR_PART_EXTENSION SET DUTIES = @P0, QTY_CONV = @P1, ADD_RATE = @P2 WHERE PART_ID = @P3";
                    
                }
                else
                {
                    sql = "INSERT INTO LR_PART_EXTENSION (DUTIES, QTY_CONV, ADD_RATE, PART_ID) VALUES (@P0, @P1, @P2, @P3)";
                }
                controller.vmfg.ExecuteCommand(sql, duties, qtyconv, addRate, sku);
            }

            CloseForm();
        }

        //private void SaveDcmsTableEdits(Dictionary<string, TableFields> tables, Controller controller)
        //{
        //    //no...this is calculated during nightly

        //    if (!partStatus.Equals("Approved")) return;
        //    if (!tables.ContainsKey("DCMS")) return;

        //    TableFields table = tables["DCMS"];
        //    if (!table.IsDirty()) return;

        //    if (!table.HasFieldChanged("LandedCost")) return;

        //    //record already exists in vmfg
        //    string sql = "UPDATE PART SET USER_10 = @P0 WHERE PART_ID = @P1";
        //    controller.vmfg.ExecuteCommand(sql, table.Fields["LandedCost"].newValue, sku);

        //    controller.history.Log(PartId, this.UserInfo.Username, "Accounting", "Landed Cost", table.Fields["LandedCost"].oldHistory, table.Fields["LandedCost"].newHistory);
        //}

        private void PrepTableData_VMFG(Dictionary<string, TableFields> tables, Controller controller)
        {
            //if (!partStatus.Equals("Approved")) tables.Remove("VMFG.Extensions");
            //if (!tables.ContainsKey("VMFG.Extensions")) return;

            //TableFields table = tables["VMFG.Extensions"];
            //table.RemoveField("FreightInCostText"); //readonly
            //table.RemoveField("DutiesText"); //readonly

        }

        private void PrepTableData_DCMS(Dictionary<string, TableFields> tables, Controller controller)
        {
            //if (!partStatus.Equals("Approved")) tables.Remove("DCMS");
            //if (!tables.ContainsKey("DCMS")) return;

            //TableFields table = tables["DCMS"];
            //if (!table.IsDirty()) return;

            ////custom
            ////we can't just rename, because this needs to be written to VMFG.PART? fuckme this is falling apart
        }


        private void PrepTableData_PartInfos(Dictionary<string, TableFields>tables, Controller controller)
        {
            //{
        //    if (!tables.ContainsKey("vwPartInfos")) return;

        //    TableFields table = tables["vwPartInfos"];
        //    if (!table.IsDirty()) return;

        //    //custom
        //    if (partStatus.Equals("Approved")) table.RemoveField("LandedCost");

        }

   
    }
}
 