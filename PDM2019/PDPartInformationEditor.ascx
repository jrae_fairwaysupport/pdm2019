<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDPartInformationEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PartInformationEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }

    input.required { border:solid 1px orange; }  
    select.required { border:solid 1px orange; }       
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();
        ToggleGrade("Min");
        ToggleGrade("Max");

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

        ToggleRoyalty();
        ToggleRoyaltyType();
        ToggleMoldShare();
        ToggleFabricationLocation();

        
        
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    function ToggleRoyaltyType() {
        //alert(jQuery('[id*="rdoRoyaltyType"]').val());
        var selValue = jQuery('input[id*="rdoRoyaltyType"]:checked').val();
        if (selValue == "PERCENTAGE") {
            jQuery('[id*="labelRoyaltyAmount"]').text("Royalty Percentage:");
            jQuery('[id*="txtRoyaltyAmount"]').attr("placeholder", "Royalty Percentage");
        }
        else {
            jQuery('[id*="labelRoyaltyAmount"]').text("Royalty Amount:");
            jQuery('[id*="txtRoyaltyAmount"]').attr("placeholder", "Royalty Amount");
        }
    }

    function ToggleRoyalty() {
        var checked = jQuery('[id*="checkRoyaltyItem"]:checked').length > 0;
        var showLabels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        if (checked) {
            if (showLabels) jQuery('[id*="labelRoyaltyPayableTo"]').show();
            jQuery('[id*="txtRoyaltyPayableTo"]').show();
            if (showLabels) jQuery('[id*="labelRoyaltyType"]').show();
            if (showLabels) jQuery('[id*="labelRoyaltyAmount"]').show();
            jQuery('[id*="txtRoyaltyAmount"]').show();
            jQuery('[id*="rdoRoyaltyType"]').show();
        }
        else {
            jQuery('[id*="labelRoyaltyPayableTo"]').hide();
            jQuery('[id*="txtRoyaltyPayableTo"]').hide();
            jQuery('[id*="labelRoyaltyType"]').hide();          
            jQuery('[id*="labelRoyaltyAmount"]').hide();
            jQuery('[id*="txtRoyaltyAmount"]').hide();
            jQuery('[id*="rdoRoyaltyType"]').hide();
        }
    }

    function ToggleFabricationLocation() {
        var sel = jQuery('[id*="rdoPurchasedOrFabricated"]:checked').val();
        var showLabels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;
        var fabLocation = jQuery('[id*="ddlFabricationLocation"] option:selected').val();

        //alert(fabLocation + "\r\n" + (fabLocation == "FabUS"));
        /*
       
            Step 1. If Fabricated...show Fabrication Location, show Unit Assembly Cost, show Material Cost
                    If Purchased...hide Fabrication Location, hide Unit Assembly Cost, hide Material Cost

            Step 2. If Fabrication Location == FAB US then
                    Preferred Vendor = 99999
                    Hide Material Cost
        */
               
        //do the wonky to keep readable...this shit keeps changing in confusing ways because REQUIREMENTS SUCK. Consistently.
        if (sel == "Fabricated" && showLabels) {
            jQuery('[id*="labelFabricationLocation"]').show();
            jQuery('[id*="labelUnitAssemblyCost"]').show();
        }
        else{
            jQuery('[id*="labelFabricationLocation"]').hide();
            jQuery('[id*="labelUnitAssemblyCost"]').hide();
        }

        if ((sel == "Fabricated") && (fabLocation == "FabUs")) {
            jQuery('[id*="labelMaterialCost"]').hide();
            jQuery('[id*="txtMaterialCost"]').hide();

            jQuery('[id*="txtPreferredVendor"]').val('99999');
            jQuery('[id*="txtPreferredVendor"]').attr("disabled", "disabled");
            jQuery('[id*="txtPreferredVendor"]').addClass("readonly");
        }
        else {
            if (showLabels) jQuery('[id*="labelMaterialCost"]').show();
            else jQuery('[id*="labelMaterialCost"]').hide();

            jQuery('[id*="txtMaterialCost"]').show();
            jQuery('[id*="txtPreferredVendor"]').removeAttr("disabled");
            jQuery('[id*="txtPreferredVendor"]').removeClass("readonly");
        }


        if (sel == "Fabricated")
        {
            jQuery('[id*="ddlFabricationLocation"]').show();
            jQuery('[id*="txtUnitAssemblyCost"]').show();

            
        }

        if (sel == "Purchased")
        {
            jQuery('[id*="labelFabricationLocation"]').hide();
            jQuery('[id*="ddlFabricationLocation"]').hide();
            
            jQuery('[id*="labelUnitAssemblyCost"]').hide();
            jQuery('[id*="txtUnitAssemblyCost"]').hide();

        }

    }

    function ToggleMoldShare() {
        var checked = jQuery('[id*="chkMoldShare"]:checked').length > 0;
        var showLabels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        if (checked) {
            if (showLabels) jQuery('[id*="labelMoldShareVendor"]').show();
            jQuery('[id*="txtMoldShareVendor"]').show();
        }
        else {
            jQuery('[id*="labelMoldShareVendor"]').hide();
            jQuery('[id*="txtMoldShareVendor"]').hide();
        }
    }

    function toggleDiv(className, low, high) {
        var showLabels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;
        var useTop = low + "px";
        if (showLabels) useTop = high + "px";

        $('div[class="' + className + '"]').css('top', useTop);

        //$('div[class="suggestParentPartId"]').css('top', '115px');
        return true;
    }


    function LookupBuyer() {

        var vendor = jQuery('[id*="txtPreferredVendor"]').val();
        jQuery('[id*="txtBuyerUserId"]').val(vendor);
//        var xmlHttp = new XMLHttpRequest();
//        xmlHttp.onreadystatechange = function () {
//            if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
//                callback(xmlHttp.responseText);
//        }
//        xmlHttp.open("GET", theUrl, true); // true for asynchronous 
//        xmlHttp.send(null);

    }

    function ToggleGrade(toggleWhat) {
        //ddl = jQuery('[id*="ddlAge' + toggleWhat + '"] :selected');
        tgt = jQuery('[id*="lblGrade' + toggleWhat + '"]');
        src = jQuery('[id*="txtAge' + toggleWhat + '"]');
        hdn = jQuery('[id*="hdnGrade' + toggleWhat + '"]');

        raw = src.val();


        var age = Number.parseFloat(raw);
        if (Number.isNaN(age)) {
            tgt.text("No Grade");
            hdn.val("No Grade");
            return;
        }

        age = Number.parseInt(age);

        var grades = ["Toddler", "Pre-K", "Kindergarten", "First Grade", "Second Grade", "Third Grade",
                                            "Fourth Grade", "Fifth Grade", "Sixth Grade", "Seventh Grade", "Eighth Grade", "No Grade"];

        if (age == 0) age = 99;

        if (age < 4)
            age = 0;
        else
            age = age - 3;

        //index value, not age
        if (age > 11) age = 11;
        //tgt.text(ddl.attr("grade"));
        tgt.text(grades[age]);
        hdn.val(grades[age]);
    }
    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;">                
                <asp:Label ID="labelCompany" runat="server" CssClass="SubSubHead" Text="Company:" />
            </td>
            <td style="width:200px;" />
            <td style="width:200px;" />
            <td style="width:200px;" />
        </tr>
        <tr>
            <td colspan="4" align="left">
                <asp:DropDownList ID="ddlCompany" runat="server" CssClass="Box required" Width="380" Table="vwPartInfos" Field="Company" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="label17" runat="server" CssClass="SubSubHead" Text="Buyer User Id:<br />" />
                <asp:TextBox ID="txtBuyerUserId" runat="server" CssClass="Box readonly" Width="180" Table="vwPartInfos" Field="BuyerUserId" ReadOnly="true" Placeholder="Buyer User Id" />
            </td>
            <td colspan="3" align="left">
                <asp:Label ID="labelPreferredVendor" runat="server" CssClass="SubSubHead" Text="Preferred Vendor:<br />" />
                <DNN:DNNTextSuggest ID="txtPreferredVendor" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                    CssClass="Box required"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPartInfos" Field="PreferredVendorSuggest" TargetField="PreferredVendor" NameIDSplit="-" Width="580" Placeholder="Preferred Vendor" 
                    queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwVENDORS WHERE ID = @P0 OR NAME = @P1"
                />

            </td>
        </tr>
        <tr>
            <td align="left" valign="bottom">
                <asp:Label ID="labelSpacer" runat="server" CssClass="SubSubHead" Text="&nbsp;<br />" />
                <asp:RadioButtonList ID="rdoPurchasedOrFabricated" runat="server" TextAlign="Right" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="SubSubHead" Table="vwPartInfos" Field="FabricatedOrPurchased"
                    onClick="javascript:ToggleFabricationLocation();" >
                    <asp:ListItem Text="&nbsp;Fabricated&nbsp;&nbsp;" Value="Fabricated" Selected="True" />
                    <asp:ListItem Text="&nbsp;Purchased" Value="Purchased" />
                </asp:RadioButtonList>
            </td>
            <td />
            <td valign="bottom" >
                <asp:Label ID="labelFabricationLocation" runat="server" CssClass="SubSubHead" Text="Fabrication Location:<br />" />
                <asp:DropDownList ID="ddlFabricationLocation" runat="server" CssClass="Box" Width="180" Table="vwPartInfos" Field="FabricationLocation" onChange="ToggleFabricationLocation();">
                    <asp:ListItem Text="Fabrication Location" Value="" />
                    <asp:ListItem Text="Fab US" Value="FabUs" />
                    <asp:ListItem Text="Fab INT" Value="FabInt" />
                </asp:DropDownList>
            </td>
            <td align="left" valign="bottom">
                <asp:Label ID="labelUnitAssemblyCost" runat="server" CssClass="SubSubHead" Text="Unit Assembly Cost:<br />" />
                <asp:TextBox ID="txtUnitAssemblyCost" runat="server" CssClass="Box" Width="180" MaxLength="25" Table="vwPartInfos" Field="UnitAssemblyCost" onkeypress="return isDecimal(event)" Placeholder="Unit Assembly Cost" Format="Decimal" />
            </td>
        </tr>

      

        <tr>
            <td align="left">
                <asp:CheckBox ID="chkIsOpenMarketItem" runat="server" TextAlign="Right" CssClass="SubSubHead" Text="&nbsp;Open Market Item" Table="vwPartInfos" Field="IsOpenMarketItem" />
            </td>
            <td colspan="3" />
        </tr>

        <tr>
            <td colspan="4">
                <asp:Label ID="labelBaseMaterial" runat="server" CssClass="SubSubHead" Text="Base Material:<br />" />
                <asp:TextBox ID="txtBaseMaterial" runat="server" CssClass="Box" Width="780" MaxLength="300" Placeholder="Base Material" Table="vwPartInfos" Field="BaseMaterial" />
            </td>
        </tr>
        

        <tr>
            <td>
                <asp:Label ID="labelMaterialCost" runat="server" CssClass="SubSubHead" Text="Material Cost:<br />" />
                <asp:TextBox ID="txtMaterialCost" runat="server" CssClass="Box required" Width="180" MaxLength="25" Table="vwPartInfos" Field="MaterialCost" onkeypress="return isDecimal(event)" PlaceHolder="Material Cost" Format="Decimal" />
            </td>
            <td>
                <asp:Label ID="labelLeadTime" runat="server" CssClass="SubSubHead" Text="Lead Time (Days):<br />" />
                <asp:TextBox ID="txtLeadTime" runat="server" CssClass="Box required" Width="180" MaxLength="25" Table="vwPartInfos" Field="LeadTime" onkeypress="return isNumberKey(event)" Placeholder="Lead Time" Format="Number" />
            </td>
            <td>
                <asp:Label ID="labelMinimumOrderQuantity" runat="server" CssClass="SubSubHead" Text="Minimum Order Quantity:<br />" />
                <asp:TextBox ID="txtMinimumOrderQuantity" runat="server" CssClass="Box required" Width="180" MaxLength="25" Table="vwPartInfos" Field="MinimumOrderQuantity" onkeypress="return isNumberKey(event)" Placeholder="Minimum Order Quantity"  Format="Number" />
            </td>
            <td>
                <asp:Label ID="label8" runat="server" CssClass="SubSubHead" Text="Multiples Of:<br />" />
                <asp:TextBox ID="txtMultiplesOf" runat="server" CssClass="Box" Width="180" MaxLength="25" Table="vwPartInfos" Field="MultiplesOf" onkeypress="return isNumberKey(event)" Placeholder="Multiples Of" Format="Number"  />
            </td>
        </tr>

        <tr>
            <td>
                <asp:Label ID="labelYearIntroduced" runat="server" CssClass="SubSubHead" Text="Year Introduced:<br />" />
                <asp:TextBox ID="txtYearIntroduced" runat="server" CssClass="Box required" Width="180" MaxLength="6" Table="vwPartInfos" Field="YearIntroduced" onkeypress="return isDecimal(event)" Placeholder="Year Introduced"  Format="Decimal" Places="1" />
            </td>
            <td>
                <asp:Label ID="labelFirstYearForecast" runat="server" CssClass="SubSubHead" Text="First Year Forecast:<br />" />
                <asp:TextBox ID="txtFirstYearForecast" runat="server" CssClass="Box required" Width="180" MaxLength="10" Table="vwPartInfos" Field="FirstYearForecast" onkeypress="return isNumberKey(event)" Placeholder="First Year Forecast" Format="Number" />
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>

        <tr>
            <td valign="bottom">
                <asp:CheckBox ID="checkRoyaltyItem" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Royalty Item" Table="vwPartInfos" Field="RoyaltyItem" onclick="javascript:ToggleRoyalty();" />
            </td>
            <td valign="bottom">
                <asp:Label ID="labelRoyaltyPayableTo" runat="server" CssClass="SubSubHead" Text="Royalty Payable To:<br />" />
                <asp:TextBox ID="txtRoyaltyPayableTo" runat="server" CssClass="Box" Width="180" MaxLength="50" Table="vwPartInfos" Field="RoyaltyPayableTo" Placeholder="Royalty Payable To" />
            </td>
            <td valign="bottom">
                <asp:Label ID="labelRoyaltyType" runat="server" CssClass="SubSubHead" Text="Royalty Type:<br />" />
                <asp:RadioButtonList ID="rdoRoyaltyType" runat="server" CssClass="SubSubHead" TextAlign="Right" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" Table="vwPartInfos" Field="RoyaltyType" onclick="javascript:ToggleRoyaltyType();">
                    <asp:ListItem Text="Percentage" Value="PERCENTAGE" />
                    <asp:ListItem Text="Amount" Value="AMOUNT" />
                </asp:RadioButtonList>
            </td>
            <td valign="bottom">
                <asp:Label ID="labelRoyaltyAmount" runat="server" CssClass="SubSubHead" Text="Royalty Amount:<br />"  />
                <asp:TextBox ID="txtRoyaltyAmount" runat="server" CssClass="Box" Width="180" MaxLength="10" onkeypress="return isDecimal(event)" Table="vwPartInfos" Field="RoyaltyInitial" Format="Decimal" />
            </td>
        </tr>

        <tr>
            <td valign="bottom">
                <asp:CheckBox ID="chkMoldShare" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Mold Share" onclick="javascript:ToggleMoldShare();" Table="vwPartInfos" Field="MoldShare" />
            </td>
            <td valign="bottom" colspan="3">
                <asp:Label ID="labelMoldShareVendor" runat="server" CssClass="SubSubHead" Text="Mold Share Vendor:<br />" />
                <DNN:DNNTextSuggest ID="txtMoldShareVendor" runat="server" 
                    onpopulateondemand="DoLookup" MaxSuggestRows="10"
                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                    Table="vwPartInfos" Field="MoldShareVendorSuggest" TargetField="MoldShareVendorId" NameIDSplit="-" Width="580" Placeholder="Mold Share Vendor"  
                    queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                    validation = "SELECT COUNT(*) AS HITS FROM vwVENDORS WHERE ID = @P0 OR NAME = @P1"
                />

            </td>
        </tr>


        <tr>
            <td >
                <asp:Label ID="labelMinAge" runat="server" CssClass="SubSubHead" Text="Target Age (Min) Years:<br />" />
                <asp:TextBox ID="txtAgeMin" runat="server" CssClass="Box required" Width="180" Placeholder="Target Age (Min)" MaxLength="10" Table="vwPartInfos" Field="MinAge" onkeypress="return isDecimal(event);" onchange="javascript:ToggleGrade('Min');" Format="Decimal" Places="1" />
            </td>
            <td >
                <asp:Label ID="labelMinGrade" runat="server" CssClass="SubSubHead" Text="Target Grade (Min):<br />" />
                <asp:Label ID="lblGradeMin" runat="server" CssClass="Normal" Table="vwPartInfos" Field="MinGrade" />
                <asp:TextBox ID="txtGradeMin" runat="server" CssClass="Box" Table="vwPartInfos" Field="MinGrade" Visible="false" />
            </td>
            <td >
                <asp:Label ID="label12" runat="server" CssClass="SubSubHead" Text="Target Age (Max):<br />" />
                <asp:TextBox ID="txtAgeMax" runat="server" CssClass="Box required" Width="180" Placeholder="Target Age (Max)" MaxLength="10" Table="vwPartInfos" Field="MaxAge" onkeypress="return isDecimal(event);" onchange="javascript:ToggleGrade('Max');" Format="Decimal" Places="1" />
            </td>
            <td >
                <asp:Label ID="label13" runat="server" CssClass="SubSubHead" Text="Target Grade (Max):<br />" />
                <asp:Label ID="lblGradeMax" runat="server" CssClass="Normal" Table="vwPartInfos" Field="MaxGrade" />
                <asp:TextBox ID="txtGradeMax" runat="server" CssClass="Box" Table="vwPartInfos" Field="MaxGrade" Visible="false" />
            </td>
        </tr>

    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnGradeMax" runat="server" Value="No Grade" />
<asp:HiddenField ID="hdnGradeMin" runat="server" Value="No Grade" />

