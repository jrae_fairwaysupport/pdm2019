using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
  
    public partial class LRHEditPart : LRHBase //PortalModuleBase, IActionable
    {
        private List<string> headerRoles = new List<string>() { "pd", "p", "ad", "c" };
       
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                //this.ctlProductDevelopment.SomethingHistoricalHappened += new EventHandler(BubbleUp_SomethingHistoricalHappened);
                this.ctlSideBar.SomethingHistoricalHappened += new EventHandler(PartWasSubmitted);
                this.ctlSideBar.OpenBOM += new EventHandler(OpenBom);
                this.ctlSideBar.CloseBOM += new EventHandler(CloseBom);

                if (!IsPostBack)
                {

                    SaveUserSetting(PartId, "ModuleId", this.ModuleId);

                    string referrer = string.Empty;
                    if (Request.UrlReferrer != null) referrer = Request.UrlReferrer.ToString();
                    if (!referrer.Contains("popUp=true"))
                    {
                        string bom = Request.QueryString["b"];
                        if (!string.IsNullOrEmpty(bom))
                        {
                            LoadData();
                            OpenBom(true);
                            return;
                        }

                        //string activeTab = GetUserSetting(PartId, "MainTab", "");
                        //int selTab = 0;
                        //Int32.TryParse(activeTab, out selTab);
                        int selTab = GetUserSetting<int>(PartId, "MainTab", 0);
                        if (selTab < 0) selTab = 0;

                        tabs.SelectedIndex = selTab;
                        radTabs.SelectedIndex = selTab;
                        radTabs.PageViews[selTab].Selected = true;
                        //RemoveUserSetting(PartId, "MainTab"); <-- NO NEED TO REMOVE IT AS IT REMAINS CONSTANT UNTIL A NEW TAB IS CLICKED

                        ToggleTab();
                    }
                    LoadData();
                    if (PartId < 1) throw new Exception("Unexpected Error Encountered.");
                    
                }


            }
            catch (Exception ex)
            {
                //Exceptions.ProcessModuleLoadException(this, ex);

                Exceptions.ProcessModuleLoadException("An unexpected error occurred loading Part data.", this, ex, true);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.Title = "PDM Part Editor - " + lblPartId.Text;
        }

        private void LoadData()
        {
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "Parts", "vwApprovals");
            controller.PopulateForm(pnlMain, ds);
            controller.PopulateForm(ctlSideBar, ds);
            controller.PopulateForm(PartId, pnlMainTabs);

            string status = controller.GetValue<string>(ds.Tables["Parts"].Rows[0], "Status", "Draft");
            SaveUserSetting(PartId, "Status", status);

            string sku = controller.GetValue<string>(ds.Tables["Parts"].Rows[0], "PartId", string.Empty);
            SaveUserSetting(PartId, "Sku", sku);

            lnkEdit.Visible = headerRoles.Contains(ViewRole);

            ctlSideBar.Initialize(ds);


            //PDMController controller;
            //Control ctl = null;

            //int selTab = 0;
            //if (tabs.SelectedTab != null) selTab = tabs.SelectedIndex;

            ////string selTab = "Product Development";
            ////if (tabs.SelectedTab != null) selTab = tabs.SelectedTab.Text;

            //switch ((PDMTab)selTab)
            //{
            //    case PDMTab.Creative:
            //        controller = new Creative(entityId, environment);
            //        ctl = ctlCreative;
            //        break;
            //    case PDMTab.QualityAssurance:
            //        controller = new Quality(entityId, environment);
            //        ctl = ctlQuality;
            //        break;
            //    case PDMTab.Sales:
            //        controller = new SalesController(entityId, environment);
            //        ctl = ctlSalesTab;
            //        break;
            //    case PDMTab.Marketing:
            //        controller = new Marketing(entityId, environment);
            //        ctl = ctlMarketing;
            //        break;
            //    //case "History":
            //    //    controller = new History(entityId, environment);
            //    //    ctl = ctlHistory;
            //    //    break;
            //    case PDMTab.Purchasing:
            //        controller = new Purchasing(entityId, environment);
            //        ctl = ctlPurchasing;
            //        break;
            //    case PDMTab.Accounting:
            //        controller = new Accounting(entityId, environment);
            //        ctl = ctlAccounting;
            //        break;
            //    case PDMTab.Legal:
            //        controller = new Legal(entityId, environment);
            //        ctl = ctlLegal;
            //        break;
            //    default:
            //        //Product Development and null
            //        controller = new ProductDevelopment(entityId, environment);
            //        ctl = ctlProductDevelopment;
            //        break;

            //}

            //DataSet ds = controller.OpenPartEditor(PartId);

            //controller.PopulateForm(pnlPartName, ds);
            //controller.PopulateForm(ctlSideBar, ds);
            //controller.PopulateForm(ctl, ds);
            //ctlSideBar.Initialize(this.ModuleId, ds);

            ////figure the rules
            //string status = "";
            //DataTable dtParts = ds.Tables["Parts"];
            //if (dtParts != null)
            //{
            //    if (dtParts.Rows.Count > 0) status = controller.GetValue<string>(dtParts.Rows[0], "Status", "Draft");
            //}
            //lnkEdit.CommandArgument = status;


            //List<string> headerRoles = new List<string>() { "pd" };
            //List<string> headerStates = new List<string>() { "Draft", "Pending", "Rejected" }; 
            //lnkEdit.Visible = headerRoles.Contains(ViewRole) && headerStates.Contains(status);
            
            ////AFOM should have a generic client side class like we do for the controllers
            //switch ((PDMTab)selTab)
            //{
            //    case PDMTab.Creative:
            //        ctlCreative.Initialize(this.ModuleId, ViewRole, status, ds, IsAdministrator, lblPartId.Text);
            //        break;
            //    case PDMTab.QualityAssurance: //"Quality Assurance":
            //        ctlQuality.Initialize(this.ModuleId, ViewRole, status, ds, IsAdministrator);
            //        break;
            //    case PDMTab.Sales:
            //        ctlSalesTab.Initialize(this.ModuleId, ViewRole, status, ds, IsAdministrator, lblPartId.Text);
            //        break;
            //    case PDMTab.Marketing:
            //        ctlMarketing.Initialize(this.ModuleId, ViewRole, status, ds, IsAdministrator, lblPartId.Text);
            //        break;
            //    //case "History":
            //    //    ctlHistory.Initialize(this.ModuleId);
            //    //    break;
            //    case PDMTab.Legal:
            //        ctlLegal.Initialize(this.ModuleId, ViewRole, status, ds, IsAdministrator, lblPartId.Text);
            //        break;
            //    case PDMTab.Accounting:
            //        ctlAccounting.Initialize(this.ModuleId, ViewRole, status, ds, IsAdministrator, lblPartId.Text);
            //        break;
            //    case PDMTab.Purchasing:
            //        ctlPurchasing.Initialize(this.ModuleId, ViewRole, status, ds, IsAdministrator, lblPartId.Text);
            //        break;
            //    default:
            //        ctlProductDevelopment.Initialize(this.ModuleId, ViewRole, status, ds, IsAdministrator, lblPartId.Text);
            //        break;
            //}

            
        }

        //protected string GetUserSetting(string key, string defaultValue = "")
        //{
        //    if (DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key) != null)
        //        return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key);
        //    else
        //        return defaultValue;
        //}

        //protected void SaveUserSetting(string key, string value)
        //{
        //    DotNetNuke.Services.Personalization.Personalization.SetProfile("PDM", key, value);
            
        //}

        //protected int PartId
        //{
        //    get
        //    {
        //        string raw = Request.QueryString["id"];
        //        int retval = -1;
        //        Int32.TryParse(raw, out retval);
        //        return retval;
        //    }
        //}




        private void PartWasSubmitted(object sender, EventArgs args)
        {
            //lnkEdit.Visible = false; <-- it's only false on Approved
        }

        private void BubbleUp_SomethingHistoricalHappened(object sender, EventArgs args)
        {
            //History controller = new History(entityId, environment);
            //DataSet ds = controller.LoadPart(PartId);
            //controller.PopulateForm(ctlHistory, ds);
        }
        


        protected void DoEditHeader(object obj, CommandEventArgs args)
        {
            hdnOpenForm.Value = EditUrl("id", PartId.ToString(), "SidebarEditor", "e", environment, "eid", entityId, "r", ViewRole);

            //if (tabs.SelectedTab == null)
            //{
            //    SaveUserSetting("MainTab", tabs.SelectedIndex.ToString());
            //    SaveUserSetting("SubTab", ctlProductDevelopment.CurrentTab);
            //}
            //else
            //{
            //    SaveUserSetting("MainTab", tabs.SelectedIndex.ToString());
            //    switch (tabs.SelectedTab.Text)
            //    {
            //        case "Product Development":
            //            SaveUserSetting("SubTab", ctlProductDevelopment.CurrentTab);
            //            break;
            //        default:
            //            break;
            //    }
            //}

            //string status = args.CommandArgument.ToString();
            //hdnOpenForm.Value = EditUrl("id", PartId.ToString(), "SidebarEditor", "status", status).Replace("550,950", "300,850");
        }

        protected void LoadData(object sender, RadTabStripEventArgs e)
        {
            SaveUserSetting(PartId, "SubTab", 0); //reset from a main tab click
            ToggleTab();
        }

        private void ToggleTab()
        {
            //LoadData();
            int selectedIndex = 0;
            if (tabs.SelectedIndex != null) selectedIndex = tabs.SelectedIndex;
            //corresponds to pdm tabs structure
            SaveUserSetting(PartId, "MainTab", selectedIndex);

            switch (selectedIndex)
            {
                case 0:
                    ctlProductDevelopment.Initialize();
                    break;
                case 1:
                    ctlPurchasing.Initialize();
                    break;
                case 2:
                    ctlQuality.Initialize();
                    break;
                case 3:
                    ctlCreative.Initialize();
                    break;
                case 4:
                    ctlSalesTab.Initialize();
                    break;
                case 5:
                    ctlMarketing.Initialize();
                    break;
                //case 6:
                //    ctlLegal.Initialize();
                //    break;
                case 6:
                    ctlAccounting.Initialize();
                    break;
            }
        }

        protected void CloseBom(object obj, EventArgs args)
        {
            pnlMainTabs.Visible = true;
            pnlBOM.Visible = false;
        }

        protected void OpenBom(object obj, EventArgs args)
        {
            OpenBom(false);
        }

        private void OpenBom(bool fromGet)
        {
            pnlMainTabs.Visible = false;
            pnlBOM.Visible = true;
            if (fromGet) ctlSideBar.ShowClose();

            ctlBOMContainer.Initialize();
        }

 
    }
}
 