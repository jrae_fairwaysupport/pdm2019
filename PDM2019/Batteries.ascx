<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Batteries.ascx.cs" Inherits="YourCompany.Modules.PDM2019.Batteries" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }

           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");



    });

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;">
    <table width="375" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:100px;" class="SubSubHead">Battery Size:</td>
            <td style="width:275px;">
                <asp:DropDownList ID="ddlBatteryType" runat="server" CssClass="Box" Width="200" >
                    <asp:ListItem Text="Battery Size" Value="" />
                    <asp:ListItem Text="AAA" Value="AAA" />
                    <asp:ListItem Text="AA" Value="AA" />
                    <asp:ListItem Text="C" Value="C" />
                    <asp:ListItem Text="D" Value="D" />
                    <asp:ListItem Text="9 Volt" Value="9V" />
                    <asp:ListItem Text="Button Cells" Value="Button Cells" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="SubSubHead">Quantity:</td>
            <td>
                <asp:TextBox ID="txtQuantity" runat="server" CssClass="Box" Width="100" Placeholder="Quantity" Format="Number" MaxLength="10"  onkeypress="return isNumberKey(event)" />
                &nbsp;&nbsp;
                <asp:CheckBox ID="chkIncluded" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Included" />
            </td>
        </tr>
    </table>


</asp:Panel>

<table width="375" cellpadding="4">
    <tr>
        <td align="left" style="width:50%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="right" style="width:50%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />

