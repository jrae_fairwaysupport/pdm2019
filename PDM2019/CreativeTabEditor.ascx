<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreativeTabEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.CreativeTabEditor" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }

           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();
        ToggleGrade('Min');
        ToggleGrade('Max');

        var msg = jQuery('[id*="hdnCreativeMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnCreativeMessage"]').val("");



    });

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isNumberOrRangeKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode == 43) return true;
        if (charCode == 45) return true;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });
    }

    function ToggleGrade(toggleWhat) {
        //ddl = jQuery('[id*="ddlAge' + toggleWhat + '"] :selected');
        tgt = jQuery('[id*="lblGrade' + toggleWhat + '"]');
        src = jQuery('[id*="txtAge' + toggleWhat + '"]');
        hdn = jQuery('[id*="hdnGrade' + toggleWhat + '"]');

        raw = src.val();
        
    
        var age = Number.parseFloat(raw);
        if (Number.isNaN(age)) {
            tgt.text("No Grade");
            hdn.val("No Grade");
            return;
        }

        age = Number.parseInt(age);

        var grades = ["Toddler", "Pre-K", "Kindergarten", "First Grade", "Second Grade", "Third Grade",
                                            "Fourth Grade", "Fifth Grade", "Sixth Grade", "Seventh Grade", "Eighth Grade", "No Grade"];

        if (age == 0) age = 99;

        if (age < 4) 
	        age = 0;
        else
	        age = age - 3;

        //index value, not age
        if (age > 11) age = 11;
        //tgt.text(ddl.attr("grade"));
        tgt.text(grades[age]);
        hdn.val(grades[age]);
    }



</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
      <asp:Panel ID="pnlCreative" runat="server" >
            <table width="800" cellpadding="4" cellspacing="0" border="0">
                <tr>
                    <td style="width:200px;">
                        <asp:Label ID="labelLinksToDocs" runat="server" CssClass="SubSubHead" Text="Links to Documents:" />
                    </td>
                    <td style="width:200px;"></td>
                    <td style="width:200px;"></td>
                    <td style="width:200px;"></td>
                </tr>
                <tr>
                    <td colspan="4" align="left" valign="top">
                        <asp:TextBox ID="txtLinksToDocuments" runat="server" Width="780" Rows="4" TextMode="MultiLine" Table="vwCreativeInfos" Field="LinksToDocuments" Placeholder="Links to Documents" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Other Links:<br />" />
                        <asp:TextBox ID="txtOtherLinks" runat="server" Width="780" Rows="4" TextMode="MultiLine" Table="vwCreativeInfos" Field="OtherLinks" Placeholder="Other Links" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="label4" runat="server" CssClass="SubSubHead" Text="What's in the Box:<br />" />
                        <asp:TextBox ID="txtWhatsInBox" runat="server" Width="780" Rows="4" TextMode="MultiLine" Table="vwCreativeInfos" Field="WhatsInBox" Placeholder="What's in the Box?" />
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Label ID="labelMainImage" runat="server" CssClass="SubSubHead" Text="Main Image:<br />" />
                        <asp:TextBox ID="txtMainImage" runat="server" Width="780" MaxLength="300" CssClass="Box" Table="vwCreativeInfos" Field="MainImage" PlaceHolder="Main Image" />
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Image 2:<br />" />
                        <asp:TextBox ID="TextBox1" runat="server" Width="780" MaxLength="300" CssClass="Box" Table="vwCreativeInfos" Field="Image2" PlaceHolder="Image 2" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Image 3:<br />" />
                        <asp:TextBox ID="TextBox2" runat="server" Width="780" MaxLength="300" CssClass="Box" Table="vwCreativeInfos" Field="Image3" PlaceHolder="Image 3" />
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Label ID="label5" runat="server" CssClass="SubSubHead" Text="Image 4:<br />" />
                        <asp:TextBox ID="TextBox3" runat="server" Width="780" MaxLength="300" CssClass="Box" Table="vwCreativeInfos" Field="Image4" PlaceHolder="Image 4" />
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Label ID="label6" runat="server" CssClass="SubSubHead" Text="Image 5:<br />" />
                        <asp:TextBox ID="TextBox4" runat="server" Width="780" MaxLength="300" CssClass="Box" Table="vwCreativeInfos" Field="Image5" PlaceHolder="Image 5" />
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Label ID="label7" runat="server" CssClass="SubSubHead" Text="Image 6:<br />" />
                        <asp:TextBox ID="TextBox5" runat="server" Width="780" MaxLength="300" CssClass="Box" Table="vwCreativeInfos" Field="Image6" PlaceHolder="Image 6" />
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Label ID="label8" runat="server" CssClass="SubSubHead" Text="Image 7:<br />" />
                        <asp:TextBox ID="TextBox6" runat="server" Width="780" MaxLength="300" CssClass="Box" Table="vwCreativeInfos" Field="Image7" PlaceHolder="Image 7" />
                    </td>
                </tr>

                <tr>
                    <td><asp:CheckBox ID="chkElectronic" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Electronic Product" Table="vwPartSpecifications" Field="Electronic"  /></td>
                    <td colspan="3" valign="bottom">
                        <asp:Label ID="lblPOwersources" runat="server" Width="140" CssClass="SubSubHead" Text="Power Sources:" />
                        <asp:CheckBox ID="chkACPower" runat="server" Width="140" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;A/C Power" Table="vwPartSpecifications" Field="ACPower" />
                        <asp:CheckBox ID="chkUSBPower" runat="server" Width="140" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;USB Power" Table="vwPartSpecifications" Field="USBPower" />
                        <asp:CheckBox ID="chkBatteriesRequired" runat="server" Width="140" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Batteries Required" Table="vwPartSpecifications" Field="Batteries"/>
                    </td>
                </tr>

                <tr>
                    <td valign="bottom" >
                        <asp:Label ID="labelPieceCount" runat="server" CssClass="SubSubHead" Text="Piece Count on Box:<br />" />
                        <asp:TextBox ID="txtPieceCount" runat="server" Width="100" MaxLength="10" CssClass="Box" Table="vwCreativeInfos" Field="PieceCountOnBox" Placeholder="Piece Count" onkeypress="return isNumberKey(event)" />
                    </td>
                    <td valign="bottom" >
                        <asp:Label ID="label9" runat="server" CssClass="SubSubHead" Text="Number of Players:<br />" />
                        <asp:TextBox ID="TextBox7" runat="server" Width="100" MaxLength="10" CssClass="Box" Table="vwCreativeInfos" Field="NumberOfPlayers" Placeholder="# of Players" onkeypress="return isNumberOrRangeKey(event)" />
                    </td>

                    <td>
                        <asp:CheckBox ID="chkAssembly" runat="server" Width="140" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Assembly Required" Table="vwPartInfos" Field="AssemblyRequired" />
                    </td>
                </tr>


               <tr>
                  <td >
                        <asp:Label ID="labelMinAge" runat="server" CssClass="SubSubHead" Text="Target Age (Min) Years:<br />" />
                        <asp:TextBox ID="txtAgeMin" runat="server" CssClass="Box" Width="180" Placeholder="Target Age (Min)" MaxLength="10" Table="vwPartInfos" Field="MinAge" onkeypress="return isDecimal(event);" onchange="javascript:ToggleGrade('Min');" Format="Decimal" Places="1" />
                    </td>
                    <td >
                        <asp:Label ID="labelMinGrade" runat="server" CssClass="SubSubHead" Text="Target Grade (Min):<br />" />
                        <asp:Label ID="lblGradeMin" runat="server" CssClass="Normal" Table="vwPartInfos" Field="MinGrade" />
                        <asp:TextBox ID="txtGradeMin" runat="server" CssClass="Box" Table="vwPartInfos" Field="MinGrade" Visible="false" />
                        <asp:HiddenField ID="hdnGradeMin" runat="server" Value="No Grade" />
                    </td>
                    <td >
                        <asp:Label ID="label12" runat="server" CssClass="SubSubHead" Text="Target Age (Max):<br />" />
                        <asp:TextBox ID="txtAgeMax" runat="server" CssClass="Box" Width="180" Placeholder="Target Age (Max)" MaxLength="10" Table="vwPartInfos" Field="MaxAge" onkeypress="return isDecimal(event);" onchange="javascript:ToggleGrade('Max');" Format="Decimal" Places="1"  />
                    </td>
                    <td >
                        <asp:Label ID="label13" runat="server" CssClass="SubSubHead" Text="Target Grade (Max):<br />" />
                        <asp:Label ID="lblGradeMax" runat="server" CssClass="Normal" Table="vwPartInfos" Field="MaxGrade"/>
                        <asp:TextBox ID="txtGradeMax" runat="server" CssClass="Box" Table="vwPartInfos" Field="MaxGrade" Visible="false" />
                        <asp:HiddenField ID="hdnGradeMax" runat="server" Value="No Grade" />
                    </td>
                </tr>

            </table>
        </asp:Panel>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnCreativeMessage" runat="server" Value="" />

