<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchasingTab.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PurchasingTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="PurchasingTabPartInformation.ascx" TagName="pchPartInformation" TagPrefix="pdm" %> 
<%@ Register Src="PDVendorPrice.ascx" TagName="partVendorPrices" TagPrefix="pdm" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server"  AutoPostBack="true" OnTabClick="LoadData" >
        <Tabs>
            <telerik:RadTab Text="Part Information"/>
            <telerik:RadTab Text="Vendor Price" />
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;" >

        <telerik:RadPageView runat="server" ID="pgPartInformation" Editors="p-*|ad-*" Editor="PurchasingPartInformationEditor" >
            <pdm:pchPartInformation id="ctlPartInformation" runat="server"  Editors="p-*|ad-*" Editor="PurchasingPartInformationEditor" />
        </telerik:RadPageView> 
    
        <telerik:RadPageView runat="server" ID="pgVendorPrice" >
            <pdm:partVendorPrices id="partVendorPricesTabx" runat="server" Editors="p-*|ad-*" Editor="PDVendorPriceEditor" />
        </telerik:RadPageView> 


    </telerik:RadMultiPage>
</asp:Panel>







