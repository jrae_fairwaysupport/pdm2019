<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDPackagingEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PDPackagingEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        ToggleLabels();

    });

    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }


</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:800px;" colspan="4"><asp:Label ID="labelPackagingTypes" runat="server" CssClass="SubSubHead" Text="Packaging Types:" /></td>
        </tr>

        <tr>
            <td style="padding-left:25px;" colspan="4">
                <asp:CheckBox ID="chkHeader" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Header" Width="120" Table="Parts" Field="Header" />
                <asp:CheckBox id="chkPolybag" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Polybag" Width="120" Table="Parts" Field="Polybag" />
                <asp:CheckBox ID="chkTub" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Tub" Width="120" Table="Parts" Field="Tub" />
                <asp:CheckBox ID="chkBlister" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Blister/Clam" Width="120" Table="Parts" Field="Blister" />
                <asp:CheckBox ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Box" Width="120" Table="Parts" Field="Box" />
                <asp:CheckBox ID="CheckBox4" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;FFP" Width="120" Table="Parts" Field="FFP" />
                
            </td>
        </tr>

        <tr>
            <td style="padding-left:25px;" valign="bottom"  colspan="4">
                <asp:Label ID="labelOtherPackaging" runat="server" CssClass="SubSubHead" Text="Other Packaging:<br />" />
                <asp:TextBox ID="txtOtherPackaging" runat="server" CssClass="Box" Width="735" Placeholder="Other Packaging Types" MaxLength="250" Table="Parts" Field="OtherPackaging" />
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <asp:CheckBox ID="chkMultilingual" runat="server" TextAlign="Right" CssClass="SubSubHead" Text="&nbsp;Multilingual Packaging" Table="Parts" Field="Multilingual" />
            </td>
        </tr>
        <tr>
            <td style="padding-left:25px;" colspan="4">
                <asp:CheckBox ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;English" Width="120" Table="Parts" Field="EnglishPackaging" />
                <asp:CheckBox id="CheckBox3" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Spanish" Width="120" Table="Parts" Field="SpanishPackaging" />
                <asp:CheckBox ID="CheckBox5" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;French" Width="120" Table="Parts" Field="FrenchPackaging" />
                <asp:CheckBox ID="CheckBox6" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;German" Width="120" Table="Parts" Field="GermanPackaging" />
                
            </td>
        </tr>
        <tr>
            <td style="padding-left:25px;" colspan="4">
                <asp:Label ID="labelOtherPackagingLanguage" runat="server" CssClass="SubSubHead" Text="Other Language(s):" />
                <asp:TextBox ID="txtOtherPackagingLanguage" runat="server" CssClass="Box" Width="735" MaxLength="250" Placeholder="Other Language(s)" Table="Parts" Field="OtherLanguagePackaging" />
            </td>
        </tr>

        <tr>
            <td style="width:200px;" >
                <asp:Label ID="labelAgeOnBox" runat="server" CssClass="SubSubHead" Text="Age on Box at Part Creation:<br />" />
                <asp:TextBox ID="txtAgeOnBox" runat="server" CssClass="Box" Width="50" MaxLength="10" onkeypress="return isDecimal(event);" Table="Parts" Field="AgeOnBoxAtPartCreation" Format="Decimal" Places="1" />
            </td>
            <td style="width:200px;">
                <asp:Label ID="labelAgeAtShipment" runat="server" CssClass="SubSubHead" Text="Age at Shipment:<br />" />
                <asp:TextBox ID="txtAgeAtShipment" runat="server" CssClass="Box" Width="50" MaxLength="10" onkeypress="return isDecimal(event);" Table="Parts" Field="AgeAtShipment" Format="Decimal" Places="1" />
            </td>
            <td style="width:200px;">
                <asp:Label ID="labelPieceCount" runat="server" CssClass="SubSubHead" Text="Piece Count on Box:<br />" />
                <asp:TextBox ID="txtPieceCount" runat="server" CssClass="Box" Width="50" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwCreativeInfos" Field="PieceCountOnBox" />
            </td>
            <td style="width:200px;" />
        </tr>
        <tr>
            <td valign="bottom" class="SubSubHead">Retail Package Size:</td>
            <td>
                <asp:Label ID="labelrpsl" runat="server" CssClass="SubSubHead" Text="Length (in).:<br />" />
                <asp:TextBox ID="txtRetailLength" runat="server" CssClass="Box" Width="75" style="text-align:right;" Placeholder="Retail Package Size Length" Format="Decimal" Places="2" MaxLength="9" onkeypress="return isDecimal(event);" Table="vwCreativeInfos" Field="RetailPackageSizeLength" />
            </td>
            <td>
                <asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Width (in).:<br />" />
                <asp:TextBox ID="txtRetailWidth" runat="server" CssClass="Box" Width="75" style="text-align:right;" Placeholder="Retail Package Size Width" Format="Decimal" Places="2" MaxLength="9" onkeypress="return isDecimal(event);" Table="vwCreativeInfos" Field="RetailPackageSizeWidth" />
            </td>
            <td>
                <asp:Label ID="label2" runat="server" CssClass="SubSubHead" Text="Height (in).:<br />" />
                <asp:TextBox ID="txtRetailHeight" runat="server" CssClass="Box" Width="75" style="text-align:right;" Placeholder="Retail Package Size Height" Format="Decimal" Places="2" MaxLength="9" onkeypress="return isDecimal(event);" Table="vwCreativeInfos" Field="RetailPackageSizeHeight" />
            </td>
        </tr>
    </table>
    </asp:Panel>

    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;"  />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Checked="true" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave"  />
            </td>
        </tr>
    
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<%--<asp:HiddenField ID="hdnShowPanel" runat="server" Value="Main" />--%>
