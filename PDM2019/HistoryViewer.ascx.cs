using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
    public partial class HistoryViewer: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Part History"; } }

        protected void DoFilter(object obj, CommandEventArgs args)
        {
            LinkButton btn = (LinkButton)obj;
            string filter = btn.Text;
            string filterWhat = args.CommandArgument.ToString();

            string tabName = string.Empty;
            string fieldName = string.Empty;

            if (filterWhat.Equals("TabName"))
                tabName = btn.Attributes["TabName"];
            else
            {
                fieldName = btn.Text;
                tabName = btn.Attributes["TabName"];
            }


            History history = new History(entityId, environment);
            DataTable dt = history.LoadFilter(PartId, tabName, fieldName);

            lstHistory.DataSource = dt;
            lstHistory.DataBind();


            lnkClearFilter.Visible = true;
        }

        protected void ClearFilter(object obj, CommandEventArgs args)
        {
            LoadData();
            lnkClearFilter.Visible = false;

        }

        protected override DataSet LoadData()
        {

            History controller = new History(entityId, environment);
            DataTable dt = controller.LoadPart(PartId);
            lstHistory.DataSource = dt;
            lstHistory.DataBind();

            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            return ds;
        }

        protected override bool Validate()
        {
            throw new NotImplementedException();
        }



        
   
    }
}
 