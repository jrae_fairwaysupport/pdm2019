using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class HeaderEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return PartId > 0 ? "PDM Part Header Editor" : "PDM New Part"; }}

        protected override void PopulateControls()
        {
            chkToggleLabels.Checked = (PartId > 0);
            txtabc.ReadOnly = !(ViewRole.Equals("p") || ViewRole.Equals("ad"));
         }
    
        protected override void SetRequirements()
        {
            //labelPartId.CssClass = "SubSubHead required";
            //labelDescription.CssClass = "SubSubHead required";

            //if (!partStatus.Equals("Draft"))
            //{
            //    labelUPC.CssClass = "SubSubHead required";
            //}
        }

        protected override void SetDefaultValues(DataSet ds)
        {
            //this is only valid for HeaderEditor, need another mechanism for all other editors that gets in a DS?
            if (PartId != -1) return;
            txtabc.Text = "N";
            rdoIsNewPart.SelectedValue = "New Part";
        }

        protected override bool Validate()
        {
            txtPartId.Text = txtPartId.Text.Trim().ToUpper();
            txtDescription.Text = txtDescription.Text.Trim().ToUpper();

            
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            string mSku = controller.ValidateText(txtPartId, true, "Part Id", ref msg);
            string description = controller.ValidateText(txtDescription, true, "Description", ref msg);
            //string upc = controller.ValidateText(txtupc, (partStatus != "Draft"), "UPC", ref msg);

            //if it's empty, we'll already have an error to throw
            if (!string.IsNullOrEmpty(mSku))
            {
                string sql = "SELECT COUNT(*) AS THECOUNT FROM vwPartList WHERE PartId = @P0 AND (id <> @P1 OR @P1 = -1)";
                int count = controller.pdm.FetchSingleValue<int>(sql, "THECOUNT", mSku, PartId);
                if (count > 0) msg += "\r\nPart Id must be unique in the database.\r\n";
            }

            //save this for approval
            //if (txtupc.Text.Trim().Length != 12) msg += "\r\nUPC must be 12 digits long.";
            if (!string.IsNullOrEmpty(mSku))
            {
                bool revision = rdoIsNewPart.SelectedValue.Equals("Revision");
                if (revision)
                {
                    controller.DNNTestSuggestIsSelectionValid(txtParentPartId, "Parent Part Id", ref msg);
                }
                else //new part - validate only if the parent part id != mSku
                {
                    string parentPartId = string.Empty;
                    string parentPartDescription = string.Empty;
                    controller.DNNTextSuggestValues(txtParentPartId.Text.Trim().ToUpper(), out parentPartId, out parentPartDescription, '-');
                    if (!mSku.Equals(parentPartId)) controller.DNNTestSuggestIsSelectionValid(txtParentPartId, "Parent Part Id", ref msg);

                }
            }

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
        
        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            string parentPartId = string.Empty;
            string upcCode = DoLookupUPC(false, out parentPartId);
            bool isNewPart = rdoIsNewPart.SelectedValue.Equals("New Part");

           
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);

            bool newPart = (PartId == -1);

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);

            if (isNewPart)
            {
                if (string.IsNullOrEmpty(parentPartId)) tables["vwPartInfos"].Fields["ParentPartId"].newValue = txtPartId.Text.Trim();
            }
            else
            {
                tables["Parts"].Fields["UPCCode"].newValue = upcCode;
            }

            TableFields table = tables["Parts"];
            table.AddField("ModifiedDate", null, DateTime.Now);
            int mPartId = PartId;


            if (newPart)
            {
                table.AddField("CreatedDate", null, DateTime.Now);
                table.AddField("Status", null, "Draft");
                table.AddField("WasApproved", null, false);
                table.AddField("IsLockedForRevision", null, false);
                table.AddField("IsRevisedPart", null, false);
                table.AddField("LoadPartRevisions", null, false);
                table.AddField("Active", null, true);

                mPartId = controller.pdm.InsertRecord("Parts", "Id", table.GetParametersAsArray(false));
                controller.history.Log(mPartId, this.UserInfo.Username, "Header", "Created Part", string.Empty, string.Empty);

                //defaults
                string sql = "INSERT INTO PartInfos (PartId, Sellable, IsOpenMarketItem, MoldShare, ParentPartQuantityConversion, CreatedDate, ModifiedDate, Purchased, IsNewPart, ParentPartId) VALUES (@P0, 1, 0, 0, 1, GetDate(), GetDate(), 1, @P1, @P2)";
                controller.pdm.ExecuteCommand(sql, mPartId, isNewPart, parentPartId);

                sql = "INSERT INTO PurchasingPartInfos (PartId, PrimaryWarehouse, CreatedDate, ModifiedDate) VALUES (@P0, 'P', GetDate(), GetDate())";
                controller.pdm.ExecuteCommand(sql, mPartId);

                sql = "INSERT INTO MarketingInfos (PartId, ExemptFromTax, CreatedDate, ModifiedDate) VALUES (@P0, 0, GetDate(), GetDate())";
                controller.pdm.ExecuteCommand(sql, mPartId);

                sql = "INSERT INTO Approvals (PartId, CreatedByUserId, CreatedByUserName, CreatedByEmail, CreatedDate) VALUES (@P0, @P1, @P2, @P3, GetDate())";
                controller.pdm.ExecuteCommand(sql, mPartId, this.UserInfo.UserID, this.UserInfo.Username, this.UserInfo.Email);

                sql = "INSERT INTO PartSpecifications (PartId, CreatedDate, ModifiedDate, ProductColor) VALUES (@P0, GetDate(), GetDate(), 'MULTI')";
                controller.pdm.ExecuteCommand(sql, mPartId);

                SaveUserSetting(mPartId, "Status", "Draft");
            }
            else
            {
                if (tables["Parts"].IsDirty("Id"))
                {
                    controller.history.Log(mPartId, this.UserInfo.Username, "Header", table.Fields, "ModifiedDate", "CreatedDate", "Id");
                    controller.pdm.UpdateRecord("Parts", "Id", mPartId, table.GetParametersAsArray(true, "Id"));
                }

                if (tables["vwPartInfos"].IsDirty())
                {
                    controller.history.Log(mPartId, this.UserInfo.Username, "Header", tables["vwPartInfos"].Fields);
                    tables["vwPartInfos"].AddField("ModifiedDate", null, DateTime.Now);
                    tables["vwPartInfos"].Fields["IsNewPart"].newValue = rdoIsNewPart.SelectedValue.Equals("New Part");
                    controller.pdm.UpdateRecord("PartInfos", "PartId", mPartId, tables["vwPartInfos"].GetParametersAsArray(true));
                }
            }


            SaveUserSetting(mPartId, "Sku", txtPartId.Text);

            SaveOracleData(controller, tables);


            if (newPart)
            {
                string tmpl8 = string.Format("~/PDMPartEditor{0}/e/{1}/id/{2}/r/{3}/eid/{0}", entityId, environment, mPartId, ViewRole);
                string url = ResolveUrl(tmpl8);

                Response.Redirect(url, true);
                //Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "id", newPartId.ToString(), "new", newPart.ToString(), "r", viewRole), true);

            }
            else
            {
                CloseForm();
            }

        }

        private void SaveOracleData(Controller controller, Dictionary<string, TableFields> tables)
        {
            if (!partStatus.Equals("Approved")) return;

            Dictionary<string, object> edits = new Dictionary<string, object>();
            tables["Parts"].StageOracleEdits(edits, "ABCCode.ABC_CODE", "Description.DESCRIPTION");
            if (edits.Count > 0) tables["Parts"].PushOracleEdits(controller.vmfg, edits, "PART", "ID", sku);
        }

        protected void DoLookupUPC(object obj, CommandEventArgs args)
        {
            string parentPartId = string.Empty;
            DoLookupUPC(true, out parentPartId);
        }

        private string DoLookupUPC(bool refresh, out string parentPartId)
        {
            parentPartId = string.Empty;
            string parentPartDescription = string.Empty;

            Controller controller = new Controller(entityId, environment);
            controller.DNNTextSuggestValues(txtParentPartId.Text.Trim(), out parentPartId, out parentPartDescription, '-');

            string sql = "SELECT UpcCode FROM PARTS WHERE PartId = @P0";
            string upc = controller.pdm.FetchSingleValue<string>(sql, "UpcCode", parentPartId);
            if (refresh) txtupc.Text = upc;

            return upc;
        }
       
     
    }
}
 