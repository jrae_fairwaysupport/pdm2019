using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class PDPackagingEditor : LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Part Packaging Editor"; } }

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = !NewRecord(ds, "Parts");

        }

        protected override bool Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);

            controller.ValidateDecimal(txtAgeOnBox, false, "Age on Box at Part Creation", ref msg);
            controller.ValidateDecimal(txtAgeAtShipment, false, "Age at Shipment", ref msg);
            controller.ValidateNumber(txtPieceCount, false, "Piece Count on Box", ref msg);
            controller.ValidateDecimal(txtRetailLength, false, "Retail Package Size Length", ref msg);
            controller.ValidateDecimal(txtRetailWidth, false, "Retail Package Size Width", ref msg);
            controller.ValidateDecimal(txtRetailHeight, false, "Retail Package Size Height", ref msg);
            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "Parts");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            SaveTableEdits(tables, "Parts", "Product Development.Packaging", newRecord, controller, "Id");
            SaveTableEdits(tables, "vwCreativeInfos", "Product Development.Packaging", NewRecord(ds, "vwCreativeInfos"), controller);


            CloseForm();

        }

        public void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {

            if (!tables.ContainsKey("Parts")) return;


            TableFields table = tables["Parts"];
            if (!table.IsDirty()) return;

        }

    }
 
}
 