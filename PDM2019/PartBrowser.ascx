<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PartBrowser.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PartBrowser" %>

<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Paging" TagPrefix="pg" Namespace="Paging" %>
<%@ Register Assembly="FilterParser" TagPrefix="fltr" Namespace="FilterParser" %>

<style>
    p { padding-top:4px;
        margin-bottom: 0px;
    }
    

    span.actionButton 
    {
        width:75px;
        
        margin-left:15px;
        text-align:center;
        vertical-align:baseline;
        font-size:12px;
        border-radius:3.5px;
        padding: 2px 4px 4px 4px;
        color: #FFFFFF;
        display:inline-block;
        background-color:#224295;
        border-color:#224295;
        border-width:1px;
        border-style:solid;
        
    }
    span.actionButton:hover
    {
        color:#224295;
        background-color:transparent;
    }

    span.button 
    {
        width:75px;
        height:20px;
        margin-left:15px;
        text-align:center;
        vertical-align:baseline;
        font-size:12px;
        border-radius:3.5px;
        padding: 2.1px 6.3px 3.15px 6.3px;
        color: #FFFFFF;
        display:inline-block;
    }
    span.Pending { background-color: #224295; }
    span.Rejected { background-color: #d9534f; }
    span.Draft { background-color: #777; }
    span.Approved { background-color: #5cb85c; }
    span.Revision { background-color: #69f; }                    
   
    a.sort
    {
        text-decoration: none;
        color: #000000;
    }
    
    label 
    {
        font-size:12px;
        color: #000000;
        font-weight:bold;
    }
    
    A, A:link, A:active, A:visited, A:hover, .Link_list li { color: #0932bf; }

</style>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        ToggleLabels();
        OpenNewPart();
        OpenPopup();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        var url = jQuery('[id*="hdnPDF"]').val();
        if (url != "") {
            window.open(url);
        }
        jQuery('[id*="hdnPDF"]').val("");

    });

    function ToggleLabels() {

        jQuery('[id*="lblStatus"]').each(function (index) {
            var cls = $(this).text();
            $(this).addClass(cls);
        });


    }

    function ToggleButtons() {
        var hideOn = jQuery('[id*="ddlRole"] :selected').attr("HideDelete");
        
        var show = true;
        var statuses = "";
        jQuery('[id*="chkPart"]:checked').each(function (index) {
            var status = $(this).closest("span").attr("Status")
            if (hideOn.includes(status)) show = false;
        });

        if (show)
            jQuery('[id*="lnkDeleteParts"]').show();
        else
            jQuery('[id*="lnkDeleteParts"]').hide();
    }

    function OpenParts() {
        var hit = false;

        jQuery('[id*="chkPart"]:checked').each(function (index) {
            hit = true;
            var url = $(this).closest("span").attr("NavigateUrl");
            window.open(url);
        });

        if (!hit) alert("Please specify the Parts you wish to view.");

        return false;
    }

    function OpenNewPart() {
        var url = jQuery('[id*="hdnUrl"]').val();
        //alert(url);
        if (url == "") return;
        jQuery('[id*="hdnUrl"]').val("");

        var arr = url.split('|');
        //alert(arr.length);
        for (var i = 0; i < arr.length; i++) window.open(arr[i]);
    }

    function OpenPopup() {
        var url = jQuery('[id*="hdnPopup"]').val();
        if (url == "") return;
        jQuery('[id*="hdnPopup"]').val("");

        eval(url);
    }

    function CopyParts() {
        var checked = jQuery('[id*="chkPart"]:checked').length > 0;

        if (!checked) {
            alert("Please specify the Parts you wish to copy.");
            return false;
        }

        return confirm('Are you sure you wish to copy the selected Parts?');
    }

    function DeleteParts() {
        var checked = jQuery('[id*="chkPart"]:checked').length > 0;

        if (!checked) {
            alert("Please specify the Parts you wish to delete.");
            return false;
        }

        return confirm('Are you sure you wish to delete the selected Parts?');
    }

</script>

<asp:Panel id="pnlMain" runat="server">
<table width="1000">
    <tr>
        <td style="width:45%;border-top:solid 1px black;border-bottom:solid 1px black;" align="left" valign="middle" class="SubSubHead">
            Status:&nbsp;
            <asp:RadioButtonList ID="rdoStatus" runat="server" CssClass="Normal label" TextAlign="Right" RepeatColumns="4" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="ToggleView">
                <asp:ListItem Text="&nbsp;Rejected&nbsp;&nbsp;" Value="Rejected"  />
                <asp:ListItem Text="&nbsp;Draft&nbsp;&nbsp;" Value="Draft" />
                <asp:ListItem Text="&nbsp;Pending&nbsp;&nbsp;" Value="Pending"  />
                <asp:ListItem Text="&nbsp;Approved&nbsp;&nbsp;" Value="Approved" />
            </asp:RadioButtonList>
            <asp:Label ID="lblFilterWarning" runat="server" CssClass="button Rejected" Text="Filtered" />
        </td>
        <td style="width:30%;border-top:solid 1px black;border-bottom:solid 1px black;" align="center" valign="middle" class="SubSubHead">
            <asp:TextBox ID="txtQuickSearch" runat="server" width="290" CssClass="Box" Placeholder="Quick Search" AutoPostBack="true" OnTextChanged="DoQuickSearch" />
        </td>
        <td style="width:25%;border-top:solid 1px black;border-bottom:solid 1px black;" align="right" valign="middle" class="SubSubHead" >
            Role:&nbsp;
            <asp:DropDownList ID="ddlRole" runat="server" CssClass="Box" AutoPostBack="true" OnSelectedIndexChanged="ToggleRole">
                <asp:ListItem Text="Product Development" Value="Product Development" HideDelete="Pending"  />
                <asp:ListItem Text="Purchasing" Value="Purchasing" HideDelete="Draft|Rejected" />
                <asp:ListItem Text="Quality Assurance" Value="Quality Assurance" HideDelete="Draft|Pending|Rejected" />
                <asp:ListItem Text="Creative" Value="Creative" HideDelete="Draft|Pending|Rejected" />
                <asp:ListItem Text="Sales" Value="Sales" HideDelete="Draft|Pending|Rejected" />
                <asp:ListItem Text="E-Commerce/Marketing" Value="E-Commerce/Marketing" HideDelete="Draft|Pending|Rejected" />
                <%--<asp:ListItem Text="Legal" Value="Legal" HideDelete="Draft|Pending|Rejected" />--%>
                <asp:ListItem Text="Accounting" Value="Accounting" HideDelete="Draft|Pending|Rejected" />
                <asp:ListItem Text="Administrator" Value="Administrator" HideDelete="" />
            </asp:DropDownList>
        </td>
    </tr>
</table>


<pg:Paging id="Paging" runat="server" ShowPageSize="true" ShowCurrentPage="true" ShowNavigator="true" Width="1000"  />

<table width="1000" class="HeaderRow">
    <tr>
        <td style="width:40px;">&nbsp;</td>
        <td style="width:150px;"><asp:LinkButton ID="lnkPartId" runat="server" Text="Part Id" CssClass="sort" OnCommand="DoSort" CommandArgument="PartId" CommandName="ASC" /></td>
        <td style="width:620px;"><asp:LinkButton ID="lnkDescription" runat="server" CssClass="sort" Text="Description" OnCommand="DoSort" CommandArgument="Description" CommandName="ASC" /> </td>
        <td style="width:170px;" align="center">Status</td>
    </tr>
</table>

<asp:Panel ID="pnlParts" runat="server" Width="1000" Height="500" ScrollBars="Auto" style="border-bottom:solid 1px black;">
    <asp:Repeater ID="lstParts" runat="server">
        <HeaderTemplate>
            <table width="980" class="table table-hover table-condensed" >
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td style="width:40px;" valign="top" align="center">
                        <asp:CheckBox ID="chkPart" runat="server" Width="40" onclick="javascript:ToggleButtons();" 
                                    Status='<%# DataBinder.Eval(Container.DataItem, "Status") %>'  
                                    NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "NavigateUrl") %>'
                                    PartId='<%# DataBinder.Eval(Container.DataItem, "Id") %>'
                        />        
                    </td>
                    <td style="width:150px;" valign="top" align="left">
                        <asp:Hyperlink  ID="lnkEditPart" runat="server" Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "PartId") %>' NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "NavigateUrl") %>' />        
                    </td>
                    <td style="width:620px;" valign="top" align="left">
                        <asp:Label ID="lblDescription"  runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' />
                    </td>
                    <td style="width:170px;" align="center" valign="top">
                        <asp:Label ID="lblStatus" CssClass="button"  runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Status") %>' />    
                        <asp:Label ID="lblRevision" runat="server" CssClass="button Revision" Text='Revision' Visible='<%# DataBinder.Eval(Container.DataItem, "IsRevisedPart") %>' />
                    </td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>

<table width="1000" >
    <tr>
        <td style="width:30%;padding-top:4px;padding-bottom:4px;padding-left:4px;background-color:#e8e2e2;" align="left" valign="middle">
            <asp:RadioButtonList ID="rdoEntity" runat="server" TextAlign="Right" CssClass="SubSubHead" OnSelectedIndexChanged="ToggleEntity" AutoPostBack="true" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                <asp:ListItem Text="&nbsp;Learning Resources&nbsp;&nbsp;" Value="LRH" />
                <asp:ListItem Text="&nbsp;Hand2Mind" Value="H2M" />
            </asp:RadioButtonList>
        </td>

        <td style="width:50%;padding-top:4px;padding-bottom:4px;padding-left:4px;background-color:#e8e2e2;" align="center" valign="middle">
            <asp:HyperLink ID="lnkViewParts" runat="server" NavigateUrl="javascript:OpenParts();"><span class="actionButton">View</span></asp:HyperLink>
            <asp:Label ID="lblspace1" runat="server" Width="10" Text="&nbsp;" />
            <asp:LinkButton ID="lnkCopyParts" runat="server" OnCommand="DoCopyParts" OnClientClick="javascript:return CopyParts();"><span class="actionButton">Copy</span></asp:LinkButton> 
            <asp:Label ID="lblspace" runat="server" Width="10" Text="&nbsp;" />
            <asp:LinkButton ID="lnkDeleteParts" runat="server" OnCommand="DoDeleteParts" OnClientClick="javascript:return DeleteParts();"><span class="actionButton">Delete</span></asp:LinkButton>
            <asp:Label ID="lblSpacex" runat="server" Width="20" Text="&nbsp;" />
            <asp:LinkButton ID="lnkNewPart" runat="server" OnCommand="DoNewPart"><span class="actionButton">New Part</span></asp:LinkButton>
        </td>

        <td style="width:20%;padding-top:4px;padding-bottom:4px;padding-right:4px;background-color:#e8e2e2;" align="right" valign="middle" >
            <asp:RadioButtonList ID="rdoEnvironment" runat="server" Visible="false" TextAlign="Right" CssClass="SubSubHead" OnSelectedIndexChanged="ToggleEnvironment" AutoPostBack="true"  RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow">
                <asp:ListItem Text="&nbsp;Production&nbsp;&nbsp;" Value="Production" />
                <asp:ListItem Text="&nbsp;Development" Value="Development" />
            </asp:RadioButtonList>
        </td>
    </tr>    
</table>


</asp:Panel>

<fltr:FilterParser id="Filter" runat="server" FilterName="PDMFilter" SkipParse="true" />

<asp:HiddenField ID="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnPDF" runat="server" Value="" />
<asp:HiddenField ID="hdnUrl" runat="server" Value="" />
<asp:HiddenField ID="hdnPopup" runat="server" Value="" />
