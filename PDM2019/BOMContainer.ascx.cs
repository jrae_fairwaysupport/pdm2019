using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using Telerik.Web.UI;


namespace YourCompany.Modules.PDM2019
{
    public partial class BOMContainer: LRHTab //PortalModuleBase, IActionable
    {
        //public event EventHandler SomethingHistoricalHappened;


        //protected void Page_Load(object sender, EventArgs e)
        //{

        //    try
        //    {
        //        //this.partSpecifications.SomethingHistoricalHappened += new EventHandler(BubbleUp_SomethingHistoricalHappened);
        //        //this.partVendorPricesTabx.SomethingHistoricalHappened += new EventHandler(BubbleUp_SomethingHistoricalHappened);

        //        if (!IsPostBack)
        //        {
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.ProcessModuleLoadException(this, ex);
        //    }
        //}

        //#region IActionable Members

        //public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        //{
        //    get
        //    {
        //        //create a new action to add an item, this will be added to the controls
        //        //dropdown menu
        //        ModuleActionCollection actions = new ModuleActionCollection();
        //        actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
        //            ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
        //             true, false);

        //        return actions;
        //    }
        //}

        //#endregion

        //private void BubbleUp_SomethingHistoricalHappened(object sender, EventArgs args)
        //{
        //    if (this.SomethingHistoricalHappened != null)
        //        this.SomethingHistoricalHappened(this, new EventArgs());

        //}

        //private int PartId
        //{
        //    get
        //    {
        //        string raw = Request.QueryString["id"];
        //        int retval = -1;
        //        Int32.TryParse(raw, out retval);
        //        return retval;
        //    }
        //}
       

        //private int ParentModuleId
        //{
        //    get
        //    {
        //        try { return Int32.Parse(hdnModuleId.Value); }
        //        catch { return -1; }
        //    }

        //    set
        //    {
        //        hdnModuleId.Value = value.ToString();
        //    }
        //}

        //private string GetUserSetting(string key, string defaultValue = "")
        //{
        //    if (DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key) != null)
        //        return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key);
        //    else
        //        return defaultValue;
        //}

        //private void SaveUserSetting(string key, string value)
        //{
        //    DotNetNuke.Services.Personalization.Personalization.SetProfile("PDM", key, value);
        //}

        //protected void LoadData(object sender, RadTabStripEventArgs e)
        //{
        //    ToggleTab();
        //}

        //protected override void ToggleTab()
        //{
        //    //LoadData();
        //    int selectedIndex = 0;
        //    if (tabs.SelectedIndex != null) selectedIndex = tabs.SelectedIndex;
        //    //corresponds to pdm tabs structure
        //    SaveUserSetting(PartId, "SubTab", selectedIndex);

        //    switch (selectedIndex)
        //    {
        //        case 0:
        //            //ctlMasterPart.Initialize();
        //            break;
        //        case 1:
        //            ctlTreeView.Initialize();
        //            break;

        //    }
        //}



        //private void OpenTab(int moduleId)
        //{
        //    string referrer = string.Empty;
        //    if (Request.UrlReferrer != null) referrer = Request.UrlReferrer.ToString();
        //    if (!referrer.Contains("popUp=true"))
        //    {
        //        string activeTab = GetUserSetting("SubTab", "");
        //        int selTab = 0;
        //        Int32.TryParse(activeTab, out selTab);

        //        switch ((PDMTab)selTab)
        //        {
        //            case PDMTab.BOMMasterPart:
        //                tabs.SelectedIndex = 0;
        //                radTabs.SelectedIndex = 0;
        //                pgMasterPart.Selected = true;
        //                SaveUserSetting("SubTab", "");
        //                break;
        //            case PDMTab.BOMTreeView: //really shouldn't be hit as there is no editor on this page
        //                tabs.SelectedIndex = 1;
        //                radTabs.SelectedIndex = 1;
        //                pgTreeView.Selected = true;
        //                SaveUserSetting("SubTab", "");
        //                break;
        //            default:
        //                //radTabs.SelectedIndex = 0;
        //                //pgPartInformation.Selected = true;
        //                break;
        //        }
                
        //    }

        //}

        //public string CurrentTab
        //{
        //    get
        //    {
        //        if (tabs.SelectedTab == null) return "0";
        //        return tabs.SelectedIndex.ToString();
        //    }
        //}

        //private string environment { get { return GetUserSetting("Environment", "Production"); } }
        //private string entityId { get { return GetUserSetting("EntityId", "LRH"); } }

        public void Initialize()
        {
            ctlMasterPart.Initialize();
            ctlTreeView.Initialize();


            tabs.SelectedIndex = 0;
            radTabs.SelectedIndex = 0;
            pgMasterPart.Selected = true;
        }
        
    }
}
 