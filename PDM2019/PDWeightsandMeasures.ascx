<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDWeightsandMeasures.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PDWeightsAndMeasuresTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit" />
    </asp:Panel>
    <asp:Panel id="pnlPackaging" runat="server" Width="750" Height="600" ScrollBars="Auto">

        <table width="100%" cellpadding="4" cellspacing="0" border="0" class="HeaderRow">
            <tr>
                <td style="width:125px;" >&nbsp;</td>
                <td style="width:125px;" align="right">Quantity</td>
                <td style="width:125px;" align="right">Length</td>
                <td style="width:125px;" align="right">Width</td>
                <td style="width:125px;" align="right">Height</td>
                <td style="width:125px;padding-right:4px;" align="right">Weight</td>
            </tr>
        </table>
        <table width="100%" cellpadding="4" cellspacing="0" border="0" class="table table-hover table-condensed" >
            <tr>
                <td>Each</td>
                <td align="right"><asp:Label ID="lblunitqty" runat="server" Table="DCMS" Field="UNIT_QTY" /></td>
                <td align="right"><asp:Label ID="lblunitlength" runat="server" Table="DCMS" Field="UNIT_LENGTH" Format="Decimal" /></td>
                <td align="right"><asp:Label ID="Label1" runat="server" Table="DCMS" Field="UNIT_WIDTH" Format="Decimal" /></td>
                <td align="right"><asp:Label ID="Label2" runat="server" Table="DCMS" Field="UNIT_HEIGHT" Format="Decimal" /></td>
                <td align="right"><asp:Label ID="Label3" runat="server" Table="DCMS" Field="UNIT_WEIGHT" Format="Decimal" /></td>
            </tr>
            <tr>
                <td>Case</td>
                <td align="right"><asp:Label ID="Label4" runat="server" Table="DCMS" Field="CASE_QTY" /></td>
                <td align="right"><asp:Label ID="Label5" runat="server" Table="DCMS" Field="CASE_LENGTH" Format="Decimal" /></td>
                <td align="right"><asp:Label ID="Label6" runat="server" Table="DCMS" Field="CASE_WIDTH" Format="Decimal" /></td>
                <td align="right"><asp:Label ID="Label7" runat="server" Table="DCMS" Field="CASE_HEIGHT" Format="Decimal" /></td>
                <td align="right"><asp:Label ID="Label8" runat="server" Table="DCMS" Field="CASE_WEIGHT" Format="Decimal" /></td>
            </tr>
            <tr>
                <td>Pallets</td>
                <td align="right"><asp:Label ID="Label9" runat="server" Table="DCMS" Field="PALLET_QTY" /></td>
                <td align="right"><asp:Label ID="Label10" runat="server" Table="DCMS" Field="PALLET_LENGTH" Format="Decimal" /></td>
                <td align="right"><asp:Label ID="Label11" runat="server" Table="DCMS" Field="PALLET_WIDTH" Format="Decimal" /></td>
                <td align="right"><asp:Label ID="Label12" runat="server" Table="DCMS" Field="PALLET_HEIGHT" Format="Decimal" /></td>
                <td align="right"></td>
            </tr>

        </table>
        
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td class="SubSubHead" style="width:150px;">CPQ Cubic Feet:</td>
                <td style="width:150px;" ><asp:Label ID="cpqcubic" runat="server" Table="DCMS" Field="CPQ" Format="Decimal" CssClass="Normal" /></td>
                <td style="width:450px;" colspan="3" />
            </tr>
            <tr>
                <td class="SubSubHead" >Inner Pack:&nbsp;<asp:Label ID="lblinnerpack" runat="server" CssClass="Normal" Table="DCMS" Field="INNER_PACK" /></td>
                <td class="SubSubHead" >Inner Pack DIMS:&nbsp;<asp:Label ID="lblInnerPackDims" runat="server" CssClass="Normal" Table="DCMS" Field="INNER_PACK_DIMS" /></td>
                <td class="SubSubHead" style="width:150px;">Inner Packs/Master:&nbsp;<asp:Label ID="lblInnerPacks" runat="server" CssClass='Normal' Table="DCMS" Field="INNER_PACKS_PER_MASTER" /></td>
                <td style="width:300px;" colspan="2" />
            </tr>
        </table>        

    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />



