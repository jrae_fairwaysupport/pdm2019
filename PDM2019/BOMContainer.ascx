<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BOMContainer.ascx.cs" Inherits="YourCompany.Modules.PDM2019.BOMContainer" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="BOMMasterTab.ascx" TagName="masterPart" TagPrefix="pdm" %>
<%@ Register Src="BOMTreeView.ascx" TagName="bomTree" TagPrefix="pdm" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server"   >
        <Tabs>
            <telerik:RadTab Text="Master Part"/>
            <telerik:RadTab Text="Tree View" />
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;" >

        <telerik:RadPageView runat="server" ID="pgMasterPart" >
            <pdm:masterPart id="ctlMasterPart" runat="server" />
        </telerik:RadPageView> 
    
        <telerik:RadPageView runat="server" ID="pgTreeView" >
            <pdm:bomTree id="ctlTreeView" runat="server" />
        </telerik:RadPageView> 

    </telerik:RadMultiPage>
</asp:Panel>


<asp:HiddenField ID="hdnModuleId" runat="server" Value="-1" />




