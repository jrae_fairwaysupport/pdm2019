using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class BOMMasterTab: LRHViewer //PortalModuleBase, IActionable
    {
        private void ToggleEditors(object parent, bool visible)
        {
            foreach (Control child in ((Control)parent).Controls)
            {
                string hint = string.Empty;
                switch (child.GetType().Name)
                {
                    case "ImageButton":
                        ImageButton btn = (ImageButton)child;
                        if (btn.ID.Equals("btnDelete"))
                        {
                            btn.Visible = visible;
                        }
                        break;
                    case "HyperLink":
                        HyperLink lnk = (HyperLink)child;
                        if (lnk.ID.Equals("lnkPartId"))
                        {
                            string recordId = lnk.Attributes["RecordId"];
                            if (visible) lnk.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "BOMAddChild", "id", PartId.ToString(), "child", recordId, "r", ViewRole, "e", environment, "eid", entityId).Replace("550,950", "325,475"); 
                        }
                        break;
                }
                ToggleEditors(child, visible);
            }
        }

        protected bool IsEditable
        {
            get { return pnlActions.Visible; }
            set { pnlActions.Visible = value; }
        }

        protected void DeleteChildPart(object obj, CommandEventArgs args)
        {
            ImageButton btn = (ImageButton)obj;
            string raw = btn.Attributes["RecordId"];
            int recordId = 0;
            if (!Int32.TryParse(raw, out recordId)) return;

            string childPartId = btn.Attributes["PartId"];

            ChildPartDelete(recordId, childPartId);

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "vwChildParts");
            DataTable children = ds.Tables["vwChildParts"];
            rptChildren.DataSource = children;
            rptChildren.DataBind();

            lblNoChildren.Visible = (children.Rows.Count == 0);
            ToggleEditors(rptChildren, IsEditable);

            
        }


        private void ChildPartDelete(int childPartRecordId, string childPartId)
        {
            Controller controller = new Controller(entityId, environment);
            string fieldName = "Child Part: " + childPartId;

            controller.history.Log(PartId, this.UserInfo.Username, "Bill of Materials", fieldName, string.Empty, "Deleted Child");
            string sql = "DELETE FROM ChildParts WHERE Part_Id = @P0 AND Id = @P1";
            controller.pdm.ExecuteCommand(sql, PartId, childPartRecordId);
        }


        public void Initialize()
        {
            IsEditable = (ViewRole.Equals("pd") && partStatus.Equals("Draft"))
                                    || (ViewRole.Equals("p") && (partStatus.Equals("Draft") || partStatus.Equals("Pending")))
                                    || ViewRole.Equals("ad");

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "vwChildParts", "vwBillOfMaterialsInfos");
            controller.PopulateForm(pnlMasterPart, ds);

            DataTable children = ds.Tables["vwChildParts"];
            //children.Columns.Add("url", System.Type.GetType("System.String"));
            //foreach (DataRow dr in children.Rows)
            //{
            //    int id = controller.GetValue<int>(dr, "Id", -1);
            //    dr["url"] = EditUrl("mid", moduleId.ToString(), "BOMAddChild", "id", PartId.ToString(), "child", id.ToString(), "r", viewRole);
            // }

            rptChildren.DataSource = children;
            rptChildren.DataBind();
            lblNoChildren.Visible = (children.Rows.Count == 0);
            ToggleEditors(rptChildren, IsEditable);

            //lnkEdit.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "BOMEditor", "id", PartId.ToString(), "r", ViewRole, "e", environment, "eid", entityId);
            lnkAddChildPart.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "BOMAddChild", "id", PartId.ToString(), "r", ViewRole, "child", "-1", "e", environment,"eid", entityId).Replace("550,950", "325,475");

            
        }



        
    }
}
 