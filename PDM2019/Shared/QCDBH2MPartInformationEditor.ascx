<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QCDBH2MPartInformationEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.QCDBH2MPartInformationEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        //ToggleElectronics();
        //ToggleBatteries();

    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }




    function ToggleCheck(lbl) {
        var t = $("#" + lbl.id).attr("recordid");
        //jQuery('"span"[standardrecordid=" + t + "]').children().css( "background-color", "red" );
        var elem = $(document).find('span[standardrecordid="' + t + '"]').children()[0];

        if (elem.checked)
            elem.checked = false;
        else
            elem.checked = true;
    }

</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
        <table width="800" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="width:200px;"><asp:Label ID="labelGeneralDescription" runat="server" CssClass="SubSubHead" Text="Specification:" /></td>
                <td style="width:200px;"></td>
                <td style="width:200px;"></td>
                <td style="width:200px;"></td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <asp:TextBox ID="txtGeneralDescription" runat="server" CssClass="Box" Width="780" Height="150" TextMode="MultiLine" Table="vwQAPartInfos" Field="Specification" Placeholder="Specifications" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <asp:Label ID="labelInspectionNotes" runat="server" CssClass="SubSubHead" Text="Inspection Notes:<br />" />
                    <asp:TextBox ID="txtInspectionNotes" runat="server" CssClass="Box" Width="780" MaxLength="50" Table="vwQAPartInfos" Field="QAInspectionNotes"  Placeholder="Inspection Notes" />
                </td>
            </tr>
            <tr>
                <td valign="bottom">
                    <asp:CheckBox Width="195" ID="chkFeatureSet" runat="server" CssClass="SubSubHead" TextAlign='Right' Text="&nbsp;Feature Set" Table="vwPartSpecifications" Field="FeatureSet" />
                </td>
                <td valign="bottom">
                    <asp:CheckBox Width="195" ID="CheckBox4" runat="server" CssClass="SubSubHead" TextAlign='Right' Text="&nbsp;Lot Type" Table="vwQAPartInfos" Field="LotTypeChecked" TargetField="LotType" CheckedValue="TOY" UncheckedValue="NOT TOY"/>
                </td>
                <td valign="bottom">
                    <asp:CheckBox Width="195" ID="chkheadphones" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Headphone Jack(s)" Table="vwPartSpecifications" Field="HeadphoneJacks" />
                </td>
                <td valign="bottom">
                    <asp:Label ID="labelheadphoenqty" runat="server" CssClass="SubSubHead" Text="Headphone Jack Quantity:<br />" />
                    <asp:TextBox ID="txtHeadphoneqty" runat="server" CssClass="Box" Width="75" MaxLength="10" Format="Number" onkeypress="return isNumberKey(event);" Table="vwPartSpecifications" Field="HeadphoneJackQuantity" Placeholder="Quantity" />
                </td>
            </tr>
            <tr>
                <td valign="bottom">
                    <asp:CheckBox ID="chkBatteriesRequired" runat="server" Width="195" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Batteries Required" Table="vwPartSpecifications" Field="Batteries" />
                </td>
                <td valign="bottom">
                    <asp:CheckBox ID="chkBatteriesIncluded" runat="server" Width="195" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Batteries Included" Table="vwPartSpecifications" Field="BatteriesIncluded" />
                </td>
                <td valign="bottom">
                    <asp:Label ID="labelbatterysize" runat="server" CssClass="SubSubHead" Text="Battery Size:<br />" />
                    <asp:TextBox ID="txtBatterySize" runat="server" CssClass="Box" Width="75" MaxLength="50" Table="vwPartSpecifications" Field="BatterySize" />
                </td>
                <td valign="bottom">
                    <asp:Label ID="labelbatteryquantity" runat="server" CssClass="SubSubHead" Text="Battery Quantity:<br />" />
                    <asp:TextBox ID="txtBatteryQuantity" runat="server" CssClass="Box" Width="75" MaxLength="10" onkeypress="return isNumberKey(event);" Table="vwPartSpecifications" Field="BatteryQuantity" Placeholder="Quantity" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox Width="195" ID="chkEnglishx" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Dishwasher Safe" Table="vwQAPartInfos" Field="DishwasherSafe" />
                </td>
                <td>
                    <asp:CheckBox Width="195" ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Food Safe" Table="vwQAPartInfos" Field="FoodSafe" />
                </td>
                <td>
                    <asp:CheckBox Width="195" ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Exempt" Table="vwQAPartInfos" Field="Exempt" />
                </td>
                <td>
                    <asp:CheckBox Width="195" ID="CheckBox3" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Component Contains Paint" Table="vwQAPartInfos" Field="PaintTesting" />
                </td>
            </tr>
            <tr>
                <td colspan="4" valign="bottom">
                    <asp:Label ID="labelInitialReportDate" runat="server" CssClass="SubSubHead" Text="Initial Report Date:<br />" />
                    <telerik:RadDatePicker id="dtInitialReportDate" width="195" runat="server" CssClass="Box" Table="vwQAPartInfos" Field="InitialTestReportDate" Placeholder="Initial Test Report Date"  />
                    <asp:Label ID="lblhazardspace" runat="server" Width="186" Text="&nbsp;" />
                    <asp:CheckBox ID="CheckBox5" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Hazardous Material" Table="vwQAPartInfos" Field="HazardousMaterial" />
                </td>
            </tr>
        </table>

        <asp:Label id="lblGroupName" runat="server" CssClass="Head" Text='Allergens' />
        <asp:Panel ID="pnlChecks" runat="server" style="padding-left:10px; border:solid 1px #e8e2e2;">
            <asp:Repeater ID="rptAllergens" runat="server">
                <HeaderTemplate>
                    <table width="782" cellpadding="0" border="0" cellspacing="0">
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
                            <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="chk1" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check0") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId0") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show0") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check0") %>'/></td>
                            <td align="left" valign="top" style="width:235px;"><asp:Label ID="lbl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId0") %>'/></td>
                            <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId1") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show1") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label1") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' /></td>
                            <td align="left" valign="top" style="width:235px;"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label1") %>'  onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId1") %>'/></td>
                            <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId2") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show2") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label2") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' /></td>
                            <td align="left" valign="top" style="width:237px;"><asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label2") %>'  onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId2") %>'/></td>
                        </tr>
                            
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>

        <asp:Label id="Label4" runat="server" CssClass="Head" Text='Package Warnings' />
        <asp:Panel ID="Panel1" runat="server" style="padding-left:10px; border:solid 1px #e8e2e2;">
            <asp:Repeater ID="rptPackageWarnings" runat="server">
                <HeaderTemplate>
                    <table width="782" cellpadding="0" border="0" cellspacing="0">
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
                            <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="chk1" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check0") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId0") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show0") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check0") %>'/></td>
                            <td align="left" valign="top" style="width:235px;"><asp:Label ID="lbl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId0") %>'/></td>
                            <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId1") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show1") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label1") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' /></td>
                            <td align="left" valign="top" style="width:235px;"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label1") %>'  onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId1") %>'/></td>
                            <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId2") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show2") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label2") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' /></td>
                            <td align="left" valign="top" style="width:237px;"><asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label2") %>'  onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId2") %>' /></td>
                        </tr>
                            
                </ItemTemplate>
                <FooterTemplate>
                        <tr>
                            <td colspan="2" valign="bottom">
                                <asp:Label ID="labelCCCDe" runat="server" CssClass="SubSubHead" Text="CCC Warning:<br />" />
                                <asp:CheckBox ID="chkCCC" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwQAPartInfos" Field="CCCWarning"  />
                                <asp:TextBox  Width="205" ID="txtCCCDescription" runat="server" CssClass="Box" MaxLength="50" Placeholder="CCC Description" Table="vwQAPartInfos" Field="CCCDescription" />
                            </td>
                            <td colspan="2" valign="bottom">
                                <asp:Label ID="label3" runat="server" CssClass="SubSubHead" Text="Wash Before Use Warning:<br />"         />
                                <asp:CheckBox ID="CheckBox27" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwQAPartInfos" Field="WashBeforeUseWarning"  />
                                <asp:TextBox  Width="205" ID="TextBox1" runat="server" CssClass="Box" MaxLength="50" Placeholder="Wash Before Use Description" Table="vwQAPartInfos" Field="WashBeforeUseDescription" />
                            </td>
                            <td colspan="2" valign="bottom">
                                <asp:Label ID="label5" runat="server" CssClass="SubSubHead" Text="Shell Fish Warning:<br />" />        
                                <asp:CheckBox ID="CheckBox28" runat="server" CssClass="SubSubHead" TextAlign="Right" Table="vwQAPartInfos" Field="ShellfishWarning"  />
                                <asp:TextBox  Width="205" ID="TextBox2" runat="server" CssClass="Box" MaxLength="50" Placeholder="Shell Fish Description" Table="vwQAPartInfos" Field="ShellfishDescription" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="bottom">
                                <asp:Label ID="labelCCCDescription" runat="server" CssClass="SubSubHead" Text="CCC Date:" />
                                <asp:Label ID="lblspace" runat='server' Width="25" Text="&nbsp;" />
                                <telerik:RadDatePicker id="dtCCCDate" width="180" runat="server" CssClass="Box" Table="vwQAPartInfos" Field="CCCDate" Placeholder="CCC Date"  />
                            </td>
                            <td colspan="4" />
                        </tr>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>

        <asp:Label id="Label6" runat="server" CssClass="Head" Text='Material Info' />
        <asp:Panel ID="Panel2" runat="server" style="padding-left:10px; border:solid 1px #e8e2e2;">
            <asp:Repeater ID="rptMaterialInfo" runat="server">
                <HeaderTemplate>
                    <table width="782" cellpadding="0" border="0" cellspacing="0">
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
                            <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="chk1" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check0") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId0") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show0") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check0") %>'/></td>
                            <td align="left" valign="top" style="width:235px;"><asp:Label ID="lbl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId0") %>'/></td>
                            <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId1") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show1") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label1") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' /></td>
                            <td align="left" valign="top" style="width:235px;"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label1") %>'  onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId1") %>'/></td>
                            <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId2") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show2") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label2") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' /></td>
                            <td align="left" valign="top" style="width:237px;"><asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label2") %>'  onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId2") %>'/></td>
                        </tr>
                            
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>


        <table width="800" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td valign="bottom">
                    <asp:CheckBox width="195" ID="chkWERCs" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Registered with WERCS?" Table="vwQAPartInfos" Field="WERCS" />
                </td>
                <td valign="bottom" colspan="3">
                    <asp:Label ID="labelWERCS" runat="server" CssClass="SubSubHead" Text="WERCS Classification:<br />" />
                    <asp:TextBox ID="txtWERCS" runat="server" Width="380" CssClass="Box" MaxLength="100" Placeholder="WERCS Classification" Table="vwQAPartInfos" Field="WERCSClassification" />
                </td>
            </tr>
        </table>
    </asp:Panel>


    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;"  />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave"  />
            </td>
        </tr>
    
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<%--<asp:HiddenField ID="hdnShowPanel" runat="server" Value="Main" />--%>
