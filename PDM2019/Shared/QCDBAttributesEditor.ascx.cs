using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
    public partial class QCDBAttributesEditor : PortalModuleBase, IActionable
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (!IsPostBack)
                {
                    PopulateControls();
                    LoadData();
                }



            }
            catch (Exception ex)
            {
                //Exceptions.ProcessModuleLoadException(this, ex);

                Exceptions.ProcessModuleLoadException("An unexpected error occurred loading Part data.", this, ex, true);
            }

            DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
            currPage.Title = (recordId == -1 ? "New" : "Edit") + " Checkbox";
        }


        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion

        string EntityId { get { return Request.QueryString["eid"]; } }
        string Environment { get { return Request.QueryString["e"]; } }
        int StandardGroupId { get { return Int32.Parse(Request.QueryString["g"]); } }
        int recordId { get { return Int32.Parse(Request.QueryString["r"]); }}

        private void PopulateControls()
        {
            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT S.RecordId, S.ControllerName, S.TestExpiry, CAST(S.RecordId AS nvarchar(5)) + '|' + S.ControllerName + '|' + CAST(S.TestExpiry as nvarchar(5)) AS StoredValue, "
                       + "CASE	WHEN S.ControllerName = 'StandardFiveYear' THEN 'Five Years'  WHEN S.ControllerName = 'StandardTwoYear' THEN 'Two Years' WHEN S.ControllerName IN ('StandardOneYear', 'StandardGeneric') THEN 'Every Year' "
                       + "WHEN S.ControllerName = 'StandardTestExempt' THEN 'Exempt' WHEN S.ControllerName = 'StandardNoExpiry' THEN 'Once, Never Expires' "
                       + "WHEN S.ControllerName = 'StandardPerPo' THEN 'Per PO' ELSE 'Unknown' END AS ControllerNameDisplay FROM StandardTestController S  WHERE S.Active = 1 "
                       + "ORDER BY S.TestExpiry, S.ControllerName";
            DataTable dt = controller.pdm.ExecuteReader(sql);
            dt.Columns["ControllerNameDisplay"].MaxLength = 1000;

            DataRow dx = dt.NewRow();
            dx["StoredValue"] = "-1||-1";
            dx["ControllerNameDisplay"] = "Select Test Requirement, if applicable";
            dt.Rows.InsertAt(dx, 0);

            ddlTestRequirement.DataTextField = "ControllerNameDisplay";
            ddlTestRequirement.DataValueField = "StoredValue";
            ddlTestRequirement.DataSource = dt;
            ddlTestRequirement.DataBind();
        }

        private void LoadData()
        {
            if (recordId == -1) return;


            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT @P0 AS EntityId, S.RecordId, S.StandardGroupId, G.TabName, G.GroupName, S.DisplayOnCert, CASE WHEN S.DisplayOnCert = 1 THEN 'True' ELSE 'False' END AS DisplayOnCertificate, "
                       + "S.PortalText, S.PortalDisplayOrder, S.StandardRecordId, S.TestCode, S.ControllerId, CASE	WHEN S.ControllerName = 'StandardFiveYear' THEN 'Five Years' "
                       + "WHEN S.ControllerName = 'StandardTwoYear' THEN 'Two Years' WHEN S.ControllerName IN ('StandardOneYear', 'StandardGeneric') THEN 'Every Year' "
                       + "WHEN S.ControllerName = 'StandardTestExempt' THEN 'Exempt' WHEN S.ControllerName = 'StandardNoExpiry' THEN 'Once, Never Expires' "
                       + "WHEN S.ControllerName = 'StandardPerPo' THEN 'Per PO' ELSE 'None' END AS ControllerNameDisplay, S.ControllerName, S.CertificateText, S.TestWarningOther, "
                       + "CASE	WHEN S.TestWarningOther = 'T' THEN 'Test' WHEN S.TestWarningOther = 'W' THEN 'Warning' ELSE 'Other' END AS TestWarningOtherText, "
                       + "COALESCE(I.Items, 0) AS Items "
                       + "FROM "
                       + "Standard S INNER JOIN StandardGroup G ON S.StandardGroupId = G.RecordId "
                       + "LEFT JOIN (SELECT StandardRecordId, COUNT(*) AS Items FROM StandardItem GROUP BY StandardRecordId) I ON S.RecordId = I.StandardRecordId "
                       + "WHERE S.StandardGroupId = @P1 AND S.RecordId = @P2 AND S.Active = 1 ORDER BY G.TabName, G.DisplayOrder, S.PortalDisplayOrder";

            DataTable dt = controller.pdm.ExecuteReader(sql, EntityId, StandardGroupId, recordId);
            DataRow dr = dt.Rows[0];

            txtPortalText.Text = controller.pdm.GetValue<string>(dr, "PortalText", "");
            ddlType.SelectedValue = controller.pdm.GetValue<string>(dr, "TestWarningOther", "O");

            int controllerId = controller.pdm.GetValue<int>(dr, "ControllerId", -1);
            string controllerName = controller.pdm.GetValue<string>(dr, "ControllerName", "");
            int testExpiry = controller.pdm.GetValue<int>(dr, "TestExpiry", -1);
            string key = string.Format("{0}|{1}|{2}", controllerId, controllerName, testExpiry);

            try { ddlTestRequirement.SelectedValue = key; }
            catch { ddlTestRequirement.SelectedIndex = 0; }
            chkDisplayOnCertificate.Checked = controller.pdm.GetValue<bool>(dr, "DisplayOnCert", false);
            txtCertificateText.Text = controller.pdm.GetValue<string>(dr, "CertificateText", string.Empty);

        }

        private bool Validate()
        {
            string msg = string.Empty;

            string portalText = txtPortalText.Text.Trim();
            string testWarningOther = ddlType.SelectedValue;

            string key = ddlTestRequirement.SelectedValue;
            string[] arr = key.Split('|');

            int controllerId = Int32.Parse(arr[0]);
            bool displayOnCertificate = chkDisplayOnCertificate.Checked;
            string certificateText = txtCertificateText.Text.Trim();

            if (string.IsNullOrEmpty(portalText))
            {
                hdnShowMessage.Value = "Checkbox Text is required.";
                return false;
            }

            if (testWarningOther.Equals("T") && controllerId == -1)
            {
                hdnShowMessage.Value = "Please specify the Test Requirement.";
                return false;
            }

            if (displayOnCertificate && string.IsNullOrEmpty(certificateText))
            {
                hdnShowMessage.Value = "Please specify the Certificate Text.";
                return false;
            }



            ////name must be unique WITHIN a group
            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT Count(*) AS TheCount FROM Standard WHERE StandardGroupId = @P0 AND PortalText = @P1 AND RecordId <> @P2";
            int count = controller.pdm.FetchSingleValue<int>(sql, "TheCount", StandardGroupId, portalText, recordId);
            if (count > 0)
            {
                hdnShowMessage.Value = "Checkboxes must be unique within a Group. Please specify a name that is not already in use.";
                return false;
            }


            return true;
        }


        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;
            string portalText = txtPortalText.Text.Trim();
            string testWarningOther = ddlType.SelectedValue;

            string key = ddlTestRequirement.SelectedValue;
            string[] arr = key.Split('|');

            int controllerId = Int32.Parse(arr[0]);
            string controllerName = arr[1];
            int testExpiry = Int32.Parse(arr[2]);

            bool displayOnCertificate = chkDisplayOnCertificate.Checked;
            string certificateText = txtCertificateText.Text.Trim();

            List<object> parms = new List<object>();
            parms.Add("StandardGroupId");
            parms.Add(StandardGroupId);
            parms.Add("DisplayOnCert");
            parms.Add(displayOnCertificate);
            parms.Add("PortalText");
            parms.Add(portalText);
            parms.Add("TestWarningOther");
            parms.Add(testWarningOther);
            parms.Add("Active");
            parms.Add(true);

            if (controllerId != -1)
            {
                parms.Add("ControllerId");
                parms.Add(controllerId);
                parms.Add("ControllerName");
                parms.Add(controllerName);
                parms.Add("TestExpiry");
                parms.Add(testExpiry);
            }
            
            parms.Add("CertificateText");
            if (displayOnCertificate)
                parms.Add(certificateText);
            else
                parms.Add(DBNull.Value);
            parms.Add("LastEditedByUser");
            parms.Add(this.UserId);
            parms.Add("LastEditedDate");
            parms.Add(DateTime.Now);


            Controller controller = new Controller(EntityId, Environment);
            if (recordId == -1)
            {
                string sql = "SELECT MAX(COALESCE(PortalDisplayOrder, 0)) AS DisplayOrder FROM Standard WHERE StandardGroupId = @P0 AND Active = 1";
                int displayOrder = controller.pdm.FetchSingleValue<int>(sql, "DisplayOrder", StandardGroupId);

                parms.Add("PortalDisplayOrder");
                parms.Add(displayOrder + 1);
                parms.Add("CreatedByUser");
                parms.Add(this.UserId);
                parms.Add("CreatedDate");
                parms.Add(DateTime.Now);

                controller.pdm.InsertRecord("Standard", "RecordId", parms.ToArray());
            }
            else
            {
                controller.pdm.UpdateRecord("Standard", "RecordId", recordId, parms.ToArray());
            }

            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            SvcLibrary.DBHandler qcdb = new SvcLibrary.DBHandler(config, null, "Production", "Quality");
            string quality = config.GetValue<string>(EntityId.Equals("LRH") ? "PDM LRH Quality Database" : "PDM H2M Quality Database", "DUMMY");
            string tmpl8 = "INSERT INTO CertificateTexts (CompanyId, RecordType, RecordId, CertificateText, StartDate) SELECT @P0 AS CompanyId, 'Standard', R.RecordId, "
                         + "COALESCE(R.CertificateText, R.PortalText), CASE WHEN C.StartDate IS NULL THEN '1900-01-01' ELSE GetDate() END FROM {0}Standard R "
                         + "LEFT JOIN CertificateTexts C ON R.RecordId = C.RecordId AND C.CompanyId = @P0 AND C.RecordType = 'Standard' WHERE "
                         + "COALESCE(R.CertificateText, R.PortalText) <> C.CertificateText OR C.StartDate IS NULL";
            string msql = string.Format(tmpl8, quality);
            qcdb.ExecuteCommand(msql, EntityId);

            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "e", Environment, "eid", EntityId, "g", StandardGroupId.ToString()), true);

        }

        protected void DoCopy(object obj, CommandEventArgs args)
        {
            //string arg = args.CommandArgument.ToString();

            //if (arg.Equals("PortalText"))
                txtCertificateText.Text = txtPortalText.Text;
            //else
            //    txtCertificateText.Text = ddlTestStandard.SelectedItem.Text;
        }
    }
}
 