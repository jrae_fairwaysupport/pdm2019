using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
    public partial class QCDBGroupsEditor : PortalModuleBase, IActionable
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (!IsPostBack)
                {
                    PopulateControls();
                    LoadData();
                }



            }
            catch (Exception ex)
            {
                //Exceptions.ProcessModuleLoadException(this, ex);

                Exceptions.ProcessModuleLoadException("An unexpected error occurred loading Part data.", this, ex, true);
            }

            DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
            currPage.Title = (recordId == -1 ? "New" : "Edit") + " Group";
        }


        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion

        string EntityId { get { return Request.QueryString["eid"]; } }
        string Environment { get { return Request.QueryString["e"]; } }
        int recordId { get { return Int32.Parse(Request.QueryString["r"]); }}

        private void PopulateControls()
        {
        }

        private void LoadData()
        {
            if (recordId == -1) return;

            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT * FROM StandardGroup WHERE RecordId = @P0";
            DataTable dt = controller.pdm.ExecuteReader(sql, recordId);
            DataRow dr = dt.Rows[0];

            txtName.Text = controller.pdm.GetValue<string>(dr, "GroupName", string.Empty);
            ddlRequirement.SelectedValue = controller.pdm.GetValue<string>(dr, "TabName", "");
        }

        private bool Validate()
        {
            string msg = string.Empty;

            string groupName = txtName.Text.Trim();
            string tabName = ddlRequirement.SelectedValue;

            if (string.IsNullOrEmpty(groupName) || string.IsNullOrEmpty(tabName))
            {
                msg = "The following fields are required:\r\n";
                if (string.IsNullOrEmpty(tabName)) msg += "Tab Name"; 
                if (string.IsNullOrEmpty(groupName)) msg += "Group Name\r\n";                
            }

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowMessage.Value = msg;
                return false;
            }

            //name must be unique
            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT Count(*) AS TheCount FROM StandardGroup WHERE GroupName = @P0 AND RecordId <> @P1";
            int count = controller.pdm.FetchSingleValue<int>(sql, "TheCount", groupName, recordId);
            if (count > 0)
            {
                hdnShowMessage.Value = "Group Names must be unique. Please specify a name that is not already in use.";
                return false;
            }


            return true;
        }


        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            string groupName = txtName.Text.Trim();
            string tabName = ddlRequirement.SelectedValue;

            Controller controller = new Controller(EntityId, Environment);
            if (recordId == -1)
            {
                string sql = "SELECT MAX(COALESCE(DisplayOrder, 0)) AS DisplayOrder FROM StandardGroup";
                int displayOrder = controller.pdm.FetchSingleValue<int>(sql, "DisplayOrder");
                controller.pdm.InsertRecord("StandardGroup", "RecordId", "GroupName", groupName, "TabName", tabName, "DisplayOrder", displayOrder + 1,
                                                "CreatedByUser", this.UserId, "CreatedDate", DateTime.Now, "LastEditedByUser", this.UserId, "LastEditedDate", DateTime.Now);
            }
            else
            {
                controller.pdm.UpdateRecord("StandardGroup", "RecordId", recordId, "GroupName", groupName, "TabName", tabName,
                                                "LastEditedByUser", this.UserId, "LastEditedDate", DateTime.Now);
            }


            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            SvcLibrary.DBHandler qcdb = new SvcLibrary.DBHandler(config, null, "Production", "Quality");
            string quality = config.GetValue<string>(EntityId.Equals("LRH") ? "PDM LRH Quality Database" : "PDM H2M Quality Database", "DUMMY");
            string tmpl8 = "INSERT INTO CertificateTexts (CompanyId, RecordType, RecordId, CertificateText, StartDate)  SELECT @P0 AS CompanyId, 'Group', R.RecordId, R.GroupName, "
                       + "CASE WHEN C.StartDate IS NULL THEN '1900-01-01' ELSE GetDate() END FROM {0}StandardGroup R LEFT JOIN CertificateTexts C ON R.RecordId = C.RecordId AND C.CompanyId = @P0 AND C.RecordType = 'Group' WHERE "
                       + "R.GroupName <> C.CertificateText OR C.StartDate IS NULL";
            string msql = string.Format(tmpl8, quality);
            qcdb.ExecuteCommand(msql, EntityId);



            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "e", Environment, "eid", EntityId), true);

        }

   
    }
}
 