using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
    public partial class QCDBStandardsEditor : PortalModuleBase, IActionable
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (!IsPostBack)
                {
                    PopulateControls();
                    LoadData();
                }



            }
            catch (Exception ex)
            {
                //Exceptions.ProcessModuleLoadException(this, ex);

                Exceptions.ProcessModuleLoadException("An unexpected error occurred loading Part data.", this, ex, true);
            }

            DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
            currPage.Title = (recordId == -1 ? "New" : "Edit") + " Test Standard";
        }


        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion

        string EntityId { get { return Request.QueryString["eid"]; } }
        string Environment { get { return Request.QueryString["e"]; } }
        int recordId { get { return Int32.Parse(Request.QueryString["r"]); }}

        private void PopulateControls()
        {
            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT RecordId, CASE	WHEN ControllerName = 'StandardFiveYear' THEN 'Five Years' "
                       + "WHEN ControllerName = 'StandardTwoYear' THEN 'Two Years' WHEN ControllerName = 'StandardOneYear' THEN 'Every Year' "
                       + "WHEN ControllerName = 'StandardPerPO' THEN 'Per PO' WHEN ControllerName = 'StandardNoExpiry' THEN 'Once, Never Expires' "
                       + "WHEN ControllerName = 'StandardTestExempt' THEN 'Exempt' WHEN ControllerName = 'StandardGeneric' THEN 'Every Year' "
                       + "ELSE 'Unknown' END AS TestRequirement FROM StandardTestController	WHERE Active = 1 "
                       + "ORDER BY CASE	WHEN ControllerName = 'StandardFiveYear' THEN 'Five Years' "
                       + "WHEN ControllerName = 'StandardTwoYear' THEN 'Two Years' WHEN ControllerName = 'StandardOneYear' THEN 'Every Year' "
                       + "WHEN ControllerName = 'StandardPerPO' THEN 'Per PO' WHEN ControllerName = 'StandardNoExpiry' THEN 'Once, Never Expires' "
                       + "WHEN ControllerName = 'StandardTestExempt' THEN 'Exempt' WHEN ControllerName = 'StandardGeneric' THEN 'Every Year' "
                       + "ELSE 'Unknown' END";
            DataTable dt = controller.pdm.ExecuteReader(sql);
            dt.Columns["TestRequirement"].MaxLength = 500;
            DataRow dx = dt.NewRow();
            dx["RecordId"] = -1;
            dx["TestRequirement"] = "Select Test Requirment";
            dt.Rows.InsertAt(dx, 0);

            ddlRequirement.DataValueField = "RecordId";
            ddlRequirement.DataTextField = "TestRequirement";
            ddlRequirement.DataSource = dt;
            ddlRequirement.DataBind();
        }

        private void LoadData()
        {
            if (recordId == -1) return;

            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT * FROM StandardTestControllerMap WHERE RecordId = @P0";
            DataTable dt = controller.pdm.ExecuteReader(sql, recordId);
            DataRow dr = dt.Rows[0];

            txtName.Text = controller.pdm.GetValue<string>(dr, "StandardName", string.Empty);
            txtTestCode.Text = controller.pdm.GetValue<string>(dr, "TestCode", string.Empty);
            ddlRequirement.SelectedValue = controller.pdm.GetValue<string>(dr, "ControllerRecordId", "-1");
        }

        private bool Validate()
        {
            string msg = string.Empty;

            string standardName = txtName.Text.Trim();
            int controllerRecordId = Int32.Parse(ddlRequirement.SelectedValue);

            if (string.IsNullOrEmpty(standardName) || controllerRecordId == -1)
            {
                msg = "The following fields are required:\r\n";
                if (string.IsNullOrEmpty(standardName)) msg += "Test Standard Name\r\n";
                if (controllerRecordId == -1) msg += "Test Requirement";
            }

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowMessage.Value = msg;
                return false;
            }

            //name must be unique
            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT Count(*) AS TheCount FROM StandardTestControllerMap WHERE Active = 1 AND StandardName = @P0 AND RecordId <> @P1";
            int count = controller.pdm.FetchSingleValue<int>(sql, "TheCount", standardName, recordId);
            if (count > 0)
            {
                hdnShowMessage.Value = "Test Standard Names must be unique. Please specify a name that is not already in use.";
                return false;
            }


            return true;
        }


        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            string standardName = txtName.Text.Trim();
            int controllerRecordId = Int32.Parse(ddlRequirement.SelectedValue);

            Controller controller = new Controller(EntityId, Environment);
            if (recordId == -1)
            {
                string sql = "SELECT MAX(COALESCE(DisplayOrder, 0)) AS DisplayOrder FROM vwStandardTestControllerMap";
                int displayOrder = controller.pdm.FetchSingleValue<int>(sql, "DisplayOrder");
                controller.pdm.InsertRecord("StandardTestControllerMap", "RecordId", "StandardName", standardName, "ControllerRecordId", controllerRecordId, "DisplayOrder", displayOrder + 1,
                                                "Active", true, "CreatedByUser", this.UserId, "CreatedDate", DateTime.Now, "LastEditedByUser", this.UserId, "LastEditedDate", DateTime.Now);
            }
            else
            {
                controller.pdm.UpdateRecord("StandardTestControllerMap", "RecordId", recordId, "StandardName", standardName, "ControllerRecordId", controllerRecordId,
                                                "LastEditedByUser", this.UserId, "LastEditedDate", DateTime.Now);
            }

            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "e", Environment, "eid", EntityId), true);

        }

   
    }
}
 