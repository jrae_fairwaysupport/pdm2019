<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QCDBH2MPartInformation.ascx.cs" Inherits="YourCompany.Modules.PDM2019.QCDBH2MPartInformation" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
    .readonly { background-color:#e8e2e2; }
    
</style>

<script language="javascript" type="text/javascript">
    function ToggleMore() {
        lnk = jQuery('[id*="lnkViewMore"]');

        if (lnk.text() == "View More") {
            lnk.text("View Less");
            jQuery('[id*="lblSpecs"]').css("max-height", "");
        }
        else {
            lnk.text("View More");
            jQuery('[id*="lblSpecs"]').css("max-height", "200px");
        }
        return false;
    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead" valign="top">Specifications:</td>
                <td style="width:600px; border:solid 1px #e8e2e2;padding:2px;" colspan="4">
                    <asp:Label id="lblSpecs" runat="server" CssClass="Normal" Table="vwQAPartInfos" Field="SpecificationBR" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkViewMore" runat="server" Text="View More" OnClientClick="javascript:return ToggleMore();" /></div>
                </td>
            </tr>

            <tr>
                <td class="SubSubHead" valign="top">Inspection Notes:</td>
                <td colspan="4" valign="top"><asp:Label ID="lblInspectionNotes" runat="server" CssClass="Normal placeholder" Table="vwQAPartInfos" Field="QAInspectionNotesBR" Width="580" /></td>
            </tr>

            <tr>
                <td class="SubSubHead"><asp:Image ID="imgFeatureSet" runat="server" Table="vwPartSpecifications" Field="FeatureSet" />&nbsp;Feature Set</td>
                <td class="SubSubHead" style="width:150px;"><asp:Image id="imgLotType" runat="server" Table="vwQAPartInfos" Field="LotTypeChecked" />&nbsp;Lot Type</td>
                <td style="width:150px;" />
                <td style="width:150px;" class="SubSubHead"><asp:Image ID="imgHeadphoneJacks" runat="server" Table="vwPartSpecifications" Field="HeadphoneJacks" />&nbsp;Headphone Jacks</td>
                <td class="SubSubHead" style="width:150px;">Quantity:&nbsp;<asp:Label ID="lblHeadphoneJack" runat="server" CssClass="Normal placeholder" Width="75" Table="vwPartSpecifications" Field="HeadphoneJackQuantity" /></td>
            </tr>

            <tr>
                <td />
                <td class="SubSubHead" ><asp:Image ID="imgBatteries" runat="server" Table="vwPartSpecifications" Field="Batteries" />&nbsp;Batteries Required</td>
                <td class="SubSubHead"><asp:Image ID="imgBatteryIncluded" runat="server" Table="vwPartSpecifications" Field="BatteriesIncluded" />&nbsp;Batteries Included</td>
                <td class="SubSubHead">Battery Size:&nbsp;<asp:Label ID="labelBatterySize" runat="server" CssClass="Normal placeholder" Width="75" Table="vwPartSpecifications" Field="BatterySize" /></td>
                <td class="SubSubHead">Quantity:&nbsp;<asp:Label ID="labelBatteryQty" runat="server" CssClass="Normal placeholder" Width="75" Table="vwPartSpecifications" Field="BatteryQuantity" /></td>
            </tr>
            
            
            <tr>
                <td class="SubSubHead"><asp:Image ID="chkDiswasherSave" runat="server" Table="vwQAPartInfos" Field="DishwasherSafe" />&nbsp;Dishwasher Safe</td>
                <td class="SubSubHead"><asp:Image ID="chkFoodSafe" runat="server" Table="vwQAPartInfos" Field="FoodSafe" />&nbsp;Food Safe</td>
                <td class="SubSubHead"><asp:Image ID="chkExempt" runat="server" Table="vwQAPartInfos" Field="Exempt" />&nbsp;Exempt</td>
                <td class="SubSubHead" colspan="2"><asp:Image id="chkComponentContainsPaint" runat="server" Table="vwQAPartInfos" Field="PaintTesting" />&nbsp;Component Contains Paint</td>
            </tr>
            
            <tr>
                <td colspan="3" class="SubSubHead">Initial Report Date:&nbsp;<asp:Label ID="lblInitialReportDate" runat="server" Table="vwQAPartInfos" Field="InitialTestReportDate" Format="Date" Width="75" CssClass="Normal placeholder" /></td>
                <td colspan="2" class="SubSubHead"><asp:Image ID="imgHazardousMaterials" runat="server" Table="vwQAPartInfos" Field="HazardousMaterial" />&nbsp;Hazardous Materials</td>
            </tr>
        </table>


        <asp:Label id="lblGroupName" runat="server" CssClass="Head" Text='Allergens' />
            <asp:Panel ID="pnlChecksAllergens" runat="server" style="padding-left:10px; border:solid 1px #e8e2e2;">
            <asp:Repeater ID="rptAllergens" runat="server">
                <HeaderTemplate>
                    <table width="732" cellpadding="0" border="0" cellspacing="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="img1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check0") %>' /></td>
                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="lbl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' /></td>
                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' /></td>
                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label1") %>' /></td>
                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="Image2" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' /></td>
                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label2") %>' /></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>

            
        <asp:Label id="Label3" runat="server" CssClass="Head" Text='Package Warnings' />
            <asp:Panel ID="Panel1" runat="server" style="padding-left:10px; border:solid 1px #e8e2e2;">
            <asp:Repeater ID="rptPackageWarnings" runat="server">
                <HeaderTemplate>
                    <table width="732" cellpadding="0" border="0" cellspacing="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="img1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check0") %>' /></td>
                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="lbl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' /></td>
                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' /></td>
                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label1") %>' /></td>
                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="Image2" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' /></td>
                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label2") %>' /></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <tr>
                        <td class="SubSubHead" colspan="6" >
                            <asp:Image id="Image16" runat="server" Table="vwQAPartInfos" Field="CCCWarning" />&nbsp;CCC
                            &nbsp;&nbsp;Description:&nbsp;<asp:Label ID="lblCCCDescription" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="CCCDescription" />
                            &nbsp;&nbsp;CCC Date:&nbsp;<asp:Label ID="cccdate" runat="server" CssClass="Normal placeholder" Width="75" Table="vwQAPartInfos" Field="CCCDate" Format="Date" />
                        </td>
                    </tr>

                    <tr>
                        <td class="SubSubHead" colspan="6" >
                            <asp:Image id="Image13" runat="server" Table="vwQAPartInfos" Field="WashBeforeUseWarning" />&nbsp;Wash Before Use
                            &nbsp;&nbsp;Description:&nbsp;<asp:Label ID="Label1" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="WashBeforeUseDescription" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="SubSubHead" colspan="6" >
                            <asp:Image id="Image14" runat="server" Table="vwQAPartInfos" Field="ShellfishWarning" />&nbsp;Shell Fish
                            &nbsp;&nbsp;Description:&nbsp;<asp:Label ID="Label2" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="ShellfishDescription" />
                        </td>
                    </tr>
	          
          
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>

          


        <asp:Label id="Label4" runat="server" CssClass="Head" Text='Material Info' />
            <asp:Panel ID="Panel2" runat="server" style="padding-left:10px; border:solid 1px #e8e2e2;">
            <asp:Repeater ID="rptMaterialInfo" runat="server">
                <HeaderTemplate>
                    <table width="732" cellpadding="0" border="0" cellspacing="0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="img1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check0") %>' /></td>
                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="lbl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' /></td>
                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' /></td>
                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label1") %>' /></td>
                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="Image2" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' /></td>
                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label2") %>' /></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>

        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td class="SubSubHead" colspan="2"><asp:Image ID="imgwercs" runat="server" Table="vwQAPartInfos" Field="WERCS" />&nbsp;Registered with WERCS?</td>
                <td colspan="3" class="SubSubHead">WERCS Classification:&nbsp;<asp:Label ID="lblwercsclassification" runat="server" CssClass="Normal placeholder" Width="180" Table="vwQAPartInfos" Field="WERCSClassification" /></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




