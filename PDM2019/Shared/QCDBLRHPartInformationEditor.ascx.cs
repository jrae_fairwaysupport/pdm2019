using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Text;


namespace YourCompany.Modules.PDM2019
{
    public partial class QCDBLRHPartInformationEditor: LRHPopup
    {
        private const string checkQuery = "SELECT G.RecordId AS StandardGroupId, G.GroupName, S.RecordId, S.PortalText, "
                 + "CASE WHEN SI.PartId IS NOT NULL THEN 1 ELSE 0 END AS Checked "
                 + "FROM StandardGroup G INNER JOIN Standard S ON G.RecordId = S.StandardGroupId "
                 + "LEFT JOIN StandardItem SI ON S.RecordId = SI.StandardRecordId AND SI.PartId = @P0 "
                 + "WHERE G.TabName = 'PartInformation' AND S.Active = 1 ORDER BY G.DisplayOrder, S.PortalDisplayOrder";


        protected override string PAGE_TITLE { get { return "PDM Quality Part Information"; } }

        protected override bool Validate()
        {
            return true;
        }

        protected override void SetDefaultValues(DataSet ds)
        {
            //bool newRecord = NewRecord(ds, "vwQAPartInfos");
            //chkToggleLabels.Checked = !newRecord;
            chkToggleLabels.Checked = true;
        }

 

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {

                CloseForm();
                return;
            }

            if (!Validate()) return;

            DataTable changes = new DataTable();
            changes.Columns.Add("StandardRecordId", System.Type.GetType("System.Int32"));
            changes.Columns.Add("PortalText", System.Type.GetType("System.String"));
            changes.Columns.Add("Added", System.Type.GetType("System.Boolean"));
            ParseSelections(rptAllergens, changes);
            ParseSelections(rptMaterialInfo, changes);
            ParseSelections(rptPackageWarnings, changes);

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            List<string> missingTables = new List<string>();
            if (!ds.Tables.Contains("vwPartInfos")) missingTables.Add("vwPartInfos");
            if (!ds.Tables.Contains("Parts")) missingTables.Add("Parts");
            if (!ds.Tables.Contains("vwApprovals")) missingTables.Add("vwApprovals");

            if (missingTables.Count > 0)
            {
                DataSet dsMissing = controller.LoadPart(PartId, missingTables.ToArray());
                foreach (DataTable dtTemp in dsMissing.Tables) ds.Tables.Add(dtTemp.Copy());
            }
            bool newRecord = NewRecord(ds, "vwQAPartInfos");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            foreach (DataRow dr in changes.Rows)
            {
                int standardRecordId = controller.pdm.GetValue<int>(dr, "StandardRecordId", -1);
                string portalText = controller.pdm.GetValue<string>(dr, "PortalText", string.Empty);
                bool added = controller.pdm.GetValue<bool>(dr, "Added", false);

                controller.history.Log(PartId, this.UserInfo.Username, "Quality - Part Information", portalText, (!added).ToString(), added.ToString());

                string sql = "DELETE FROM StandardItem WHERE StandardRecordId = @P0 AND PartId = @P1";
                if (added) sql = "INSERT INTO StandardItem (StandardRecordId, PartId) VALUES (@P0, @P1)";
                controller.pdm.ExecuteCommand(sql, standardRecordId, PartId);
            }

            SaveTableEdits(tables, "vwPartSpecifications", "Product Development.Specifications", NewRecord(ds, "vwPartSpecifications"), controller);
            SaveTableEdits(tables, "vwQAPartInfos", "Quality.Part Information", newRecord, controller);

            NotifyCreative(changes);
            SaveOracleData(controller, tables);
            CloseForm();
        }

        private void SaveOracleData(Controller controller, Dictionary<string, TableFields> tables)
        {
            if (!partStatus.Equals("Approved")) return;

            //if we cam from DoSave, we can drop out if General Description has not changed
            if (tables != null)
            {
                if (!tables["vwPartSpecifications"].HasFieldChanged("GeneralDescription")) return;
            }

            controller.Specifications_Save(PartId, sku, txtGeneralDescription.Text.Trim());
        }

        private void NotifyCreative(DataTable dt)
        {
            if (dt.Rows.Count == 0) return;
            Controller controller = new Controller(entityId, environment);

            List<string> records = controller.pdm.ConvertToInstring(dt, "StandardRecordId", false, "-1");
            string sql = "SELECT * FROM Standard WHERE RecordId IN ({0}) AND ColumnName IN ('CEWarning', 'SmallPartsWarning', 'SmallBallWarning', 'OwlPellets', 'MarbleWarning', "
                       + "'LatexBalloonsWarning', 'NoBabyWarning', 'CordWarning', 'MagnetWarning', 'SharpFunctionalPointWarning', 'MarbleInKit', 'BallInKit', 'Projectile', 'NotHumanConsumption', "
                       + "'NotSafetyProtection', 'CCCWarning', 'CCCDescription', 'CCCDate', 'WashBeforeUseWarning', 'WashBeforeUseDescription', 'ShellFishWarning', 'ShellFishDescription', "
                       + "'AdultSupervision', 'NaturalLeadWarning', 'MeetsANSIZ81', 'LatextWarning', 'Prop65ReproductiveToxicantsWarning')";
            DataTable dtStandards = controller.pdm.MergeInstrings(sql, records);
            if (dtStandards.Rows.Count == 0) return;

            foreach (DataRow dr in dtStandards.Rows)
            {
                int recordId = controller.pdm.GetValue<int>(dr, "RecordId", -1);
                string portalText = controller.pdm.GetValue<string>(dr, "PortalText", string.Empty);

                DataRow[] arr = dt.Select("StandardRecordId = " + recordId.ToString());
                bool added = controller.pdm.GetValue<bool>(arr[0], "Added", false);

                controller.pdm.InsertRecord("CreativeNotifications", "PartId", "PartId", PartId, "ModifiedBy", this.UserId, "ModifiedDate", DateTime.Now,
                                                "Warning", portalText, "Added", added, "QA_TO_CREATIVE");
                
            }

        }

        private string GetModuleSetting(string key)
        {
            key = entityId + environment + key;
            PDM2019Settings settingsData = new PDM2019Settings(this.TabModuleId);
            return settingsData.ReadSetting<string>(key, string.Empty);
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwQAPartInfos")) return;

            TableFields table = tables["vwQAPartInfos"];
            if (! table.IsDirty()) return;


            //custom logic
            if (!(bool)table.Fields["CCCWarning"].newValue)
            {
                table.Fields["CCCDescription"].newValue = null;
                table.Fields["CCCDate"].newValue = null;
            }

            if (!(bool)table.Fields["WashBeforeUseWarning"].newValue) table.Fields["WashBeforeUseDescription"].newValue = null;
            if (!(bool)table.Fields["ShellfishWarning"].newValue) table.Fields["ShellfishDescription"].newValue = null;
        }


        protected void Battery_Delete(object obj, CommandEventArgs args) 
        {
            ImageButton btn = (ImageButton)obj;

            int recordId = Int32.Parse(args.CommandArgument.ToString());
            string batteryFor = btn.Attributes["BatteriesFor"];
            string size = btn.Attributes["BatterySize"];
            int qty = Int32.Parse(btn.Attributes["BatteryQuantity"]);
            bool included = bool.Parse(btn.Attributes["BatteryIncluded"]);

            Controller controller = new Controller(entityId, environment);
            controller.Battery_Delete(this.UserInfo.Username, recordId, PartId, batteryFor, size, qty, included);
            IsDirty = true;
            Battery_Load();
        }

        private bool Battery_Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);
            controller.ValidateText(ddlAddBatterySize, true, "Battery Size", ref msg);
            controller.ValidateNumber(txtAddBatteryQuantity, true, "Battery Quantity", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void Battery_Save(object obj, CommandEventArgs args)
        {
            if (!Battery_Validate()) return;

            string batterySize = ddlAddBatterySize.SelectedValue;
            int batteryQty = Int32.Parse(txtAddBatteryQuantity.Text.Trim());
            bool included = chkAddBatteryIncluded.Checked;

            Controller controller = new Controller(entityId, environment);
            controller.Battery_Add(this.UserInfo.Username, PartId, "Part", batterySize, batteryQty, included);
            IsDirty = true;
            Battery_Load();
        
        
            Battery_Toggle(false);
        }

        protected void Battery_Toggle(object obj, CommandEventArgs args)
        {
            bool open = args.CommandArgument.ToString().Equals("Open");
            Battery_Toggle(open);
        }

        private void Battery_Toggle(bool open)
        {
            ddlAddBatterySize.SelectedIndex = 0;
            txtAddBatteryQuantity.Text = string.Empty;
            chkAddBatteryIncluded.Checked = false;

            Controller controller = new Controller(entityId, environment);
            controller.StripErrorStyles(ddlAddBatterySize);
            controller.StripErrorStyles(txtAddBatteryQuantity);
            
            pnlMain.Visible = !open;
            pnlAddBattery.Visible = open;
        }

        private void Battery_Load()
        {
            Controller controller = new Controller(entityId, environment);

            DataSet ds = controller.LoadPart(PartId, "vwBatteries");
            lstBatteries.DataSource = ds.Tables["vwBatteries"];
            lstBatteries.DataBind();
            lblNoBatteries.Visible = ds.Tables["vwBatteries"].Rows.Count == 0;
        }



        private void ParseSelections(object parent, DataTable dt)
        {
            foreach (Control child in ((Control)parent).Controls)
            {
                if (child.GetType().Name.Equals("CheckBox"))
                {
                    try
                    {
                        CheckBox chk = (CheckBox)child;
                        bool isChecked = chk.Checked;
                        bool original = Boolean.Parse(chk.Attributes["Original"]);
                        if (isChecked != original)
                        {
                            string portalText = chk.Attributes["PortalText"];
                            int standardRecordId = Int32.Parse(chk.Attributes["StandardRecordId"]);
                            bool added = isChecked;
                            dt.Rows.Add(standardRecordId, portalText, added);
                        }
                    }
                    catch
                    {
                        //not part of the standard checks
                    }
                }
                ParseSelections(child, dt);
            }
        }

        protected DataTable checks { get; set; }

        protected override DataSet LoadData()
        {
            //DataSet ds = base.LoadData();

            Controller controller = new Controller(entityId, environment);


            checks = controller.pdm.ExecuteReader(checkQuery, PartId);

            PopulateGroup(rptAllergens, "Allergens");
            PopulateGroup(rptPackageWarnings, "Package Warnings");
            PopulateGroup(rptMaterialInfo, "Material Info");

            return base.LoadData();
        }

        private void PopulateGroup(Repeater rpt, string groupName)
        {
            DataRow[] arr = checks.Select(string.Format("GroupName = '{0}'", groupName));


            DataTable dt = FormatChecks(arr);
            rpt.DataSource = dt;
            rpt.DataBind();

        }

        protected void GetChecks(object obj, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Repeater rpt = (Repeater)e.Item.FindControl("rptChecks");

                string filter = string.Format("StandardGroupId = {0}", ((DataRowView)e.Item.DataItem)["StandardGroupId"]);
                DataRow[] arr = checks.Select(filter);

                DataTable dt = FormatChecks(arr);
                rpt.DataSource = dt;
                rpt.DataBind();



            }

        }

        private DataTable FormatChecks(DataRow[] arr)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 3; i++)
            {
                dt.Columns.Add("Check" + i.ToString(), System.Type.GetType("System.Boolean"));
                dt.Columns.Add("Label" + i.ToString(), System.Type.GetType("System.String"));
                dt.Columns.Add("StandardRecordId" + i.ToString(), System.Type.GetType("System.Int32"));
                dt.Columns.Add("Show" + i.ToString(), System.Type.GetType("System.Boolean"));
            }

            if (arr.Length == 0) return dt;

            int cell = -1;
            int rows = (int)((arr.Length - 1) / 3) + 1;
            for (int outer = 1; outer <= rows; outer++)
            {
                DataRow dx = dt.NewRow();
                for (int inner = 0; inner < 3; inner++)
                {
                    dx["Check" + inner.ToString()] = false;
                    dx["Label" + inner.ToString()] = string.Empty;
                    dx["StandardRecordId" + inner.ToString()] = -1;
                    dx["Show" + inner.ToString()] = false;

                    cell++;
                    if (cell < arr.Length)
                    {
                        dx["Check" + inner.ToString()] = arr[cell]["Checked"];
                        dx["Label" + inner.ToString()] = arr[cell]["PortalText"];
                        dx["StandardRecordId" + inner.ToString()] = arr[cell]["RecordId"];
                        dx["Show" + inner.ToString()] = true;
                    }
                }
                dt.Rows.Add(dx);
            }
            return dt;
        }
    }
}
 