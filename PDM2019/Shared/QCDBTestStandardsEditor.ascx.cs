using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class QCDBTestStandardsEditor: LRHPopup
    {

        private const string checkQuery = "SELECT G.RecordId AS StandardGroupId, G.GroupName, S.RecordId, S.PortalText, "
                       + "CASE WHEN SI.PartId IS NOT NULL THEN 1 ELSE 0 END AS Checked "
                       + "FROM StandardGroup G INNER JOIN Standard S ON G.RecordId = S.StandardGroupId "
                       + "LEFT JOIN StandardItem SI ON S.RecordId = SI.StandardRecordId AND SI.PartId = @P0 "
                       + "WHERE G.TabName = 'TestStandards' AND S.Active = 1 ORDER BY G.DisplayOrder, S.PortalDisplayOrder";

        protected override string PAGE_TITLE { get { return "PDM Test Standards"; } }

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = true;
        }

        protected override bool Validate()
        {
            return true;
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            Controller controller = new Controller(entityId, environment);

            DataTable changes = new DataTable();
            changes.Columns.Add("StandardRecordId", System.Type.GetType("System.Int32"));
            changes.Columns.Add("PortalText", System.Type.GetType("System.String"));
            changes.Columns.Add("Added", System.Type.GetType("System.Boolean"));
            ParseSelections(grdGroups, changes);

            foreach (DataRow dr in changes.Rows)
            {
                int standardRecordId = controller.pdm.GetValue<int>(dr, "StandardRecordId", -1);
                string portalText = controller.pdm.GetValue<string>(dr, "PortalText", string.Empty);
                bool added = controller.pdm.GetValue<bool>(dr, "Added", false);

                controller.history.Log(PartId, this.UserInfo.Username, "Quality - Test Standards", portalText, (!added).ToString(), added.ToString());

                string sql = "DELETE FROM StandardItem WHERE StandardRecordId = @P0 AND PartId = @P1";
                if (added) sql = "INSERT INTO StandardItem (StandardRecordId, PartId) VALUES (@P0, @P1)";
                controller.pdm.ExecuteCommand(sql, standardRecordId, PartId);
            }
            
            CloseForm();
        }

        private void ParseSelections(object parent, DataTable dt)
        {
            foreach (Control child in ((Control)parent).Controls)
            {
                if (child.GetType().Name.Equals("CheckBox"))
                {
                    CheckBox chk = (CheckBox)child;
                    bool isChecked = chk.Checked;
                    bool original = Boolean.Parse(chk.Attributes["Original"]);
                    if (isChecked != original)
                    {
                        string portalText = chk.Attributes["PortalText"];
                        int standardRecordId = Int32.Parse(chk.Attributes["StandardRecordId"]);
                        bool added = isChecked;
                        dt.Rows.Add(standardRecordId, portalText, added);
                    }
                }
                ParseSelections(child, dt);
            }
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwTestStandards")) return;

            TableFields table = tables["vwTestStandards"];
            if (!table.IsDirty()) return;

            //custom
        }

        protected DataTable checks { get; set; }

        protected override DataSet LoadData()
        {
            //DataSet ds = base.LoadData();

            Controller controller = new Controller(entityId, environment);

            
            checks = controller.pdm.ExecuteReader(checkQuery, PartId);

            DataTable groups = checks.DefaultView.ToTable(true, "GroupName", "StandardGroupId");
            grdGroups.DataSource = groups;
            grdGroups.DataBind();


            return null;
        }

        protected void GetChecks(object obj, DataListItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Repeater rpt = (Repeater)e.Item.FindControl("rptChecks");

                string filter = string.Format("StandardGroupId = {0}", ((DataRowView)e.Item.DataItem)["StandardGroupId"]);
                DataRow[] arr = checks.Select(filter);

                DataTable dt = FormatChecks(arr);
                rpt.DataSource = dt;
                rpt.DataBind();



            }

        }

        private DataTable FormatChecks(DataRow[] arr)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 3; i++)
            {
                dt.Columns.Add("Check" + i.ToString(), System.Type.GetType("System.Boolean"));
                dt.Columns.Add("Label" + i.ToString(), System.Type.GetType("System.String"));
                dt.Columns.Add("StandardRecordId" + i.ToString(), System.Type.GetType("System.Int32"));
                dt.Columns.Add("Show" + i.ToString(), System.Type.GetType("System.Boolean"));
            }

            if (arr.Length == 0) return dt;

            int cell = -1;
            int rows = (int)((arr.Length - 1) / 3) + 1;
            for (int outer = 1; outer <= rows; outer++)
            {
                DataRow dx = dt.NewRow();
                for (int inner = 0; inner < 3; inner++)
                {
                    dx["Check" + inner.ToString()] = false;
                    dx["Label" + inner.ToString()] = string.Empty;
                    dx["StandardRecordId" + inner.ToString()] = -1;
                    dx["Show" + inner.ToString()] = false;

                    cell++;
                    if (cell < arr.Length)
                    {
                        dx["Check" + inner.ToString()] = arr[cell]["Checked"];
                        dx["Label" + inner.ToString()] = arr[cell]["PortalText"];
                        dx["StandardRecordId" + inner.ToString()] = arr[cell]["RecordId"];
                        dx["Show" + inner.ToString()] = true;
                    }
                }
                dt.Rows.Add(dx);
            }
            return dt;
        }
    
    }
}
 