using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
  
    public partial class QCDBGroups : PortalModuleBase, IActionable
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["eid"])) rdoEntity.SelectedValue = Request.QueryString["eid"];
                    if (!string.IsNullOrEmpty(Request.QueryString["e"])) rdoEnvironment.SelectedValue = Request.QueryString["e"];

                    LoadData();
                    lnkAdd.NavigateUrl= EditUrl("r", "-1", "Editor", "e", Environment, "eid", EntityId).Replace("550,950", "250,550");
                }

            }
            catch (Exception ex)
            {
                //Exceptions.ProcessModuleLoadException(this, ex);

                Exceptions.ProcessModuleLoadException("An unexpected error occurred loading Part data.", this, ex, true);
            }
        }

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion
        
        private void LoadData()
        {
            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            Controller controller = new Controller(EntityId, Environment);

            string sql = "SELECT @P0 AS EntityId, G.RecordId, CASE WHEN G.TabName = 'PartInformation' THEN 'Part Information' ELSE 'Test Standards' END AS TabName, "
                       + "G.GroupName, G.DisplayOrder, COALESCE(S.Standards, 0) AS Standards "
                       + "FROM StandardGroup G LEFT JOIN (SELECT StandardGroupId, Count(*) AS Standards FROM Standard WHERE Active = 1 GROUP BY StandardGroupId) S ON G.RecordId = S.StandardGroupId "
                       + "ORDER BY G.TabName, G.DisplayOrder";
            DataTable raw = controller.pdm.ExecuteReader(sql, EntityId);
            DataTable dt = raw.Clone();

            DataRow[] arr = raw.Select("TabName = 'Part Information'");
            DataTable tmp = arr.CopyToDataTable();
            SetUpDowns(tmp);
            dt.Merge(tmp);

            arr = raw.Select("TabName='Test Standards'");
            tmp = arr.CopyToDataTable();
            SetUpDowns(tmp);
            dt.Merge(tmp);

            dt.Columns.Add("Editor", System.Type.GetType("System.String"));
            foreach (DataRow dr in dt.Rows)
            {
                string recordId = controller.pdm.GetValue<string>(dr, "RecordId");
                string url = EditUrl("r", recordId, "Editor", "e", Environment, "eid", EntityId).Replace("550,950", "250,550");
                dr["Editor"] = url;
            }

            rptData.DataSource = dt;
            rptData.DataBind();

            
        }

        private void SetUpDowns(DataTable dt)
        {
            dt.Columns.Add("ShowUp", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("ShowDown", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("UpImage", System.Type.GetType("System.String"));
            dt.Columns.Add("DownImage", System.Type.GetType("System.String"));
            dt.Columns.Add("UpAlt", System.Type.GetType("System.String"));
            dt.Columns.Add("DownAlt", System.Type.GetType("System.String"));
            dt.Columns.Add("ShowDelete", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("DeleteImage", System.Type.GetType("System.String"));
            dt.Columns.Add("DeleteAlt", System.Type.GetType("System.String"));

            int index = -1;
            foreach (DataRow dr in dt.Rows)
            {
                index++;
                dr["ShowUp"] = (index > 0) ? true : false;
                dr["UpImage"] = (index > 0) ? "~/images/up.gif" : "~/images/spacer.gif";
                dr["UpAlt"] = (index > 0) ? "Move Up" : "";

                dr["ShowDown"] = (index < dt.Rows.Count - 1) ? true : false;
                dr["DownImage"] = (index < dt.Rows.Count - 1) ? "~/images/dn.gif" : "~/images/spacer.gif";
                dr["DownAlt"] = (index < dt.Rows.Count - 1) ? "Move Down" : "";

                int standards = Int32.Parse(dr["Standards"].ToString());
                dr["ShowDelete"] = (standards == 0) ? true : false;
                dr["DeleteImage"] = (standards == 0) ? "~/images/delete.gif" : "~/images/spacer.gif";
                dr["DeleteAlt"] = (standards == 0) ? "Delete Group" : "";
            }
        }


        private string Environment { get { return rdoEnvironment.SelectedValue; } }
        private string EntityId { get { return rdoEntity.SelectedValue; } }

        protected void ToggleEnvironment(object obj, EventArgs args)
        {
            lnkAdd.NavigateUrl = EditUrl("r", "-1", "Editor", "e", Environment, "eid", EntityId).Replace("550,950", "250,550");
            LoadData();
        }

        protected void Move(object obj, CommandEventArgs args)
        {
            ImageButton btn = (ImageButton)obj;
            string tabName = btn.Attributes["TabName"].Replace(" ", "");

            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT * FROM StandardGroup WHERE TabName = @P0 ORDER BY DisplayOrder";
            DataTable dt = controller.pdm.ExecuteReader(sql, tabName);

            dt.Columns.Add("Index", System.Type.GetType("System.Int32"));

            for (int index = 0; index < dt.Rows.Count; index++)
            {
                dt.Rows[index]["Index"] = index;
            }

           
            string recordId = btn.Attributes["RecordId"];
            string action = args.CommandArgument.ToString();

            DataRow[] arr = dt.Select("RecordId = " + recordId);
            int curIndex = controller.pdm.GetValue<int>(arr[0], "Index", -1);
            if (action.Equals("Down"))
                curIndex++;
            else
                curIndex--;

            //sets the new index for the one we clicked
            arr[0]["Index"] = curIndex;

            //sets the new index for the next/previous one
            arr = dt.Select(string.Format("RecordId <> {0} AND Index = {1}", recordId, curIndex));
            if (action.Equals("Down"))
                curIndex--;
            else
                curIndex++;

            arr[0]["Index"] = curIndex;

            arr = dt.Select("Index <> DisplayOrder");
            foreach (DataRow dr in dt.Rows)
            {
                int standardRecordId = controller.pdm.GetValue<int>(dr, "RecordId", -1);
                int displayOrder = controller.pdm.GetValue<int>(dr, "Index", -1);
                controller.pdm.UpdateRecord("StandardGroup", "RecordId", standardRecordId, "DisplayOrder", displayOrder);
            }

            LoadData();

        }

        protected void DoDelete(object obj, CommandEventArgs args)
        {
            ImageButton btn = (ImageButton)obj;
            int recordId = Int32.Parse(btn.Attributes["RecordId"]);

            Controller controller = new Controller(EntityId, Environment);
            string sql = "DELETE FROM StandardGroup WHERE RecordId= @P0";
            controller.pdm.ExecuteCommand(sql, recordId);
            LoadData();
        }
    }
}
 