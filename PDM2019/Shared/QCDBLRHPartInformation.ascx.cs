using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class QCDBLRHPartInformation: LRHViewer 
    {

        protected DataTable checks { get; set; }

        public override bool Initialize()
        {
            if (Initialized) return true;

            Controller controller = new Controller(entityId, environment);

            string sql = "SELECT G.RecordId AS StandardGroupId, G.GroupName, S.RecordId, S.PortalText, "
                       + "CASE WHEN SI.PartId IS NOT NULL THEN '~/images/checked.gif' ELSE '~/images/unchecked.gif' END AS CheckImage "
                       + "FROM StandardGroup G INNER JOIN Standard S ON G.RecordId = S.StandardGroupId "
                       + "LEFT JOIN StandardItem SI ON S.RecordId = SI.StandardRecordId AND SI.PartId = @P0 "
                       + "WHERE G.TabName = 'PartInformation' AND S.Active = 1 ORDER BY G.DisplayOrder, S.PortalDisplayOrder";
            checks = controller.pdm.ExecuteReader(sql, PartId);

            PopulateGroup(rptAllergens, "Allergens");
            PopulateGroup(rptPackageWarnings, "Package Warnings");
            PopulateGroup(rptMaterialInfo, "Material Info");

            return base.Initialize();
        }


        private void PopulateGroup(Repeater rpt, string groupName)
        {
            DataRow[] arr = checks.Select(string.Format("GroupName = '{0}'", groupName));


            DataTable dt = FormatChecks(arr);
            rpt.DataSource = dt;
            rpt.DataBind();

        }

        private DataTable FormatChecks(DataRow[] arr)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < 3; i++)
            {
                dt.Columns.Add("Check" + i.ToString(), System.Type.GetType("System.String"));
                dt.Columns.Add("Label" + i.ToString(), System.Type.GetType("System.String"));
            }

            if (arr.Length == 0) return dt;

            int cell = -1;
            int rows = (int)((arr.Length - 1) / 3) + 1;
            for (int outer = 1; outer <= rows; outer++)
            {
                DataRow dx = dt.NewRow();
                for (int inner = 0; inner < 3; inner++)
                {
                    cell++;
                    if (cell < arr.Length)
                    {
                        dx["Check" + inner.ToString()] = arr[cell]["CheckImage"];
                        dx["Label" + inner.ToString()] = arr[cell]["PortalText"];
                    }
                }
                dt.Rows.Add(dx);
            }
            return dt;
        }

     

    }
}
 