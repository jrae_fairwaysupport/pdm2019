<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QCDBTestStandardsEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.QCDBTestStandardsEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }


    function ToggleElectronics() {
        var checked = jQuery('[id*="chkElectronic"]:checked').length > 0;
        var labels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        if (checked) {
            jQuery('[id*="chkheadphones"]').show();
            jQuery('label[for*="chkheadphones"]').show();
            if (labels) jQuery('[id*="labelheadphoneqty"]').show();
            jQuery('[id*="lblheadphoneqty"]').show();
            jQuery('[id*="txtheadphoneqty"]').show();
            jQuery('[id*="chkBatteriesRequired"]').show();
            jQuery('label[for*="chkBatteriesRequired"]').show();
            
        }
        else {
            jQuery('[id*="chkheadphones"]').hide();
            jQuery('label[for*="chkheadphones"]').hide();
            jQuery('[id*="labelheadphoneqty"]').hide();
            jQuery('[id*="lblheadphoneqty"]').hide();
            jQuery('[id*="txtheadphoneqty"]').hide();
            jQuery('[id*="chkBatteriesRequired"]').hide();
            jQuery('label[for*="chkBatteriesRequired"]').hide();
        }
        ToggleBatteries();
    }

    function ToggleBatteries() {
        var checked = jQuery('[id*="chkBatteriesRequired"]:checked').length > 0;
        var electronic = jQuery('[id*="chkElectronic"]:checked').length > 0;

        if (checked && electronic)
            jQuery('[id*="pnlBatteries"]').show();
        else
            jQuery('[id*="pnlBatteries"]').hide();
    }

    function ToggleCheck(lbl) {
        var t = $("#" + lbl.id).attr("recordid");
        //jQuery('"span"[standardrecordid=" + t + "]').children().css( "background-color", "red" );
        var elem = $(document).find('span[standardrecordid="' + t + '"]').children()[0];

        if (elem.checked)
            elem.checked = false;
        else
            elem.checked = true;
    }

</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
        <asp:DataList ID="grdGroups" runat="server" Width="100%" OnItemDataBound="GetChecks">
            <ItemTemplate>
                <asp:Label id="lblGroupName" runat="server" CssClass="Head" Text='<%# DataBinder.Eval(Container.DataItem, "GroupName") %>' />
                <asp:Panel ID="pnlChecks" runat="server" style="padding-left:10px; border:solid 1px #e8e2e2;">
                    <asp:Repeater ID="rptChecks" runat="server">
                        <HeaderTemplate>
                            <table width="782" cellpadding="0" border="0" cellspacing="0">
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr>
                                    <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="chk1" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check0") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId0") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show0") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check0") %>'/></td>
                                    <td align="left" valign="top" style="width:235px;"><asp:Label ID="lbl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId0") %>'/></td>
                                    <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId1") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show1") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label1") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' /></td>
                                    <td align="left" valign="top" style="width:235px;"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label1") %>'  onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId1") %>' /></td>
                                    <td align="center" valign="top" style="width:25px;"><asp:CheckBox ID="CheckBox2" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' StandardRecordId = '<%# DataBinder.Eval(Container.DataItem, "StandardRecordId2") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "Show2") %>' PortalText='<%# DataBinder.Eval(Container.DataItem, "Label2") %>' Original='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' /></td>
                                    <td align="left" valign="top" style="width:237px;"><asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label2") %>'  onClick='javascript:ToggleCheck(this);' RecordId='<%# DataBinder.Eval(Container.DataItem, "StandardRecordId2") %>'/></td>
                                </tr>
                            
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </ItemTemplate>
        </asp:DataList>


    </asp:Panel>


    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" />
            </td>
        </tr>
    
    </table>
</asp:Panel>



<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<%--<asp:HiddenField ID="hdnShowPanel" runat="server" Value="Main" />--%>
