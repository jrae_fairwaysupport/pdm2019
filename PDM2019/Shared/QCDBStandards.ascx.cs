using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
  
    public partial class QCDBStandards : PortalModuleBase, IActionable
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["eid"])) rdoEntity.SelectedValue = Request.QueryString["eid"];
                    if (!string.IsNullOrEmpty(Request.QueryString["e"])) rdoEnvironment.SelectedValue = Request.QueryString["e"];

                    LoadData();
                    lnkAdd.NavigateUrl= EditUrl("r", "-1", "Editor", "e", Environment, "eid", EntityId).Replace("550,950", "250,550");
                }

            }
            catch (Exception ex)
            {
                //Exceptions.ProcessModuleLoadException(this, ex);

                Exceptions.ProcessModuleLoadException("An unexpected error occurred loading Part data.", this, ex, true);
            }
        }

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion
        
        private void LoadData()
        {
            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            Controller controller = new Controller(EntityId, Environment);

            string sql = "SELECT @P0 AS EntityId, RecordId, StandardName, TestCode,  ControllerRecordId, ControllerName, TestExpiry, DisplayOrder, "
                       + "CASE	WHEN ControllerName = 'StandardFiveYear' THEN 'Five Years' WHEN ControllerName = 'StandardTwoYear' THEN 'Two Years' "
                       + "WHEN ControllerName = 'StandardOneYear' THEN 'Every Year' WHEN ControllerName = 'StandardPerPO' THEN 'Per PO' "
                       + "WHEN ControllerName = 'StandardNoExpiry' THEN 'Once, Never Expires' WHEN ControllerName = 'StandardTestExempt' THEN 'Exempt' "
                       + "WHEN ControllerName = 'StandardGeneric' THEN 'Every Year' "
                       + "ELSE 'Unknown' END AS TestRequirement, COALESCE(I.Items, 0) AS Items FROM vwStandardTestControllerMap S "
                       + "LEFT JOIN (SELECT S.StandardRecordId, COUNT(*) AS Items FROM StandardItem ST INNER JOIN Standard S ON ST.StandardRecordId = S.RecordId GROUP BY "
                       + "S.StandardRecordId) I ON S.RecordId = I.StandardRecordId "
                       + "ORDER BY DisplayOrder ";
            DataTable dt = controller.pdm.ExecuteReader(sql, EntityId);
            SetUpDowns(dt);

            dt.Columns.Add("Editor", System.Type.GetType("System.String"));
            foreach (DataRow dr in dt.Rows)
            {
                string recordId = controller.pdm.GetValue<string>(dr, "RecordId");
                string url = EditUrl("r", recordId, "Editor", "e", Environment, "eid", EntityId).Replace("550,950", "250,550");
                dr["Editor"] = url;
            }

            rptData.DataSource = dt;
            rptData.DataBind();

            
        }

        private void SetUpDowns(DataTable dt)
        {
            dt.Columns.Add("ShowUp", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("ShowDown", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("UpImage", System.Type.GetType("System.String"));
            dt.Columns.Add("DownImage", System.Type.GetType("System.String"));
            dt.Columns.Add("UpAlt", System.Type.GetType("System.String"));
            dt.Columns.Add("DownAlt", System.Type.GetType("System.String"));

            int index = -1;
            foreach (DataRow dr in dt.Rows)
            {
                index++;
                dr["ShowUp"] = (index > 0) ? true : false;
                dr["UpImage"] = (index > 0) ? "~/images/up.gif" : "~/images/spacer.gif";
                dr["UpAlt"] = (index > 0) ? "Move Up" : "";

                dr["ShowDown"] = (index < dt.Rows.Count - 1) ? true : false;
                dr["DownImage"] = (index < dt.Rows.Count - 1) ? "~/images/dn.gif" : "~/images/spacer.gif";
                dr["DownAlt"] = (index < dt.Rows.Count - 1) ? "Move Down" : "";
            }
        }


        private string Environment { get { return rdoEnvironment.SelectedValue; } }
        private string EntityId { get { return rdoEntity.SelectedValue; } }

        protected void ToggleEnvironment(object obj, EventArgs args)
        {
            lnkAdd.NavigateUrl = EditUrl("r", "-1", "Editor", "e", Environment, "eid", EntityId).Replace("550,950", "250,550");
            LoadData();
        }

        protected void Move(object obj, CommandEventArgs args)
        {
            Controller controller = new Controller(EntityId, Environment);
            string sql = "SELECT * FROM vwStandardTestControllerMap ORDER BY DisplayOrder";
            DataTable dt = controller.pdm.ExecuteReader(sql);

            dt.Columns.Add("Index", System.Type.GetType("System.Int32"));

            for (int index = 0; index < dt.Rows.Count; index++)
            {
                dt.Rows[index]["Index"] = index;
            }

            ImageButton btn = (ImageButton)obj;
            string recordId = btn.Attributes["RecordId"];
            string action = args.CommandArgument.ToString();

            DataRow[] arr = dt.Select("RecordId = " + recordId);
            int curIndex = controller.pdm.GetValue<int>(arr[0], "Index", -1);
            if (action.Equals("Down"))
                curIndex++;
            else
                curIndex--;

            //sets the new index for the one we clicked
            arr[0]["Index"] = curIndex;

            //sets the new index for the next/previous one
            arr = dt.Select(string.Format("RecordId <> {0} AND Index = {1}", recordId, curIndex));
            if (action.Equals("Down"))
                curIndex--;
            else
                curIndex++;

            arr[0]["Index"] = curIndex;

            arr = dt.Select("Index <> DisplayOrder");
            foreach (DataRow dr in dt.Rows)
            {
                int standardRecordId = controller.pdm.GetValue<int>(dr, "RecordId", -1);
                int displayOrder = controller.pdm.GetValue<int>(dr, "Index", -1);
                controller.pdm.UpdateRecord("StandardTestControllerMap", "RecordId", standardRecordId, "DisplayOrder", displayOrder);
            }

            LoadData();

        }

        protected void DoDelete(object obj, CommandEventArgs args)
        {
            ImageButton btn = (ImageButton)obj;
            int recordId = Int32.Parse(btn.Attributes["RecordId"]);

            Controller controller = new Controller(EntityId, Environment);
            controller.pdm.UpdateRecord("StandardTestControllerMap", "RecordId", recordId, "Active", false, "LastEditedByUser", this.UserId, "LastEditedDate", DateTime.Now);
            LoadData();
        }
    }
}
 