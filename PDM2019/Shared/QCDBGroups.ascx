<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QCDBGroups.ascx.cs" Inherits="YourCompany.Modules.PDM2019.QCDBGroups" %>

<style>
    p { padding-top:4px;
        margin-bottom: 0px;
    }
    
    span.placeholder { border-bottom: solid 1px #e8e2e2; }
   
    label 
    {
        font-size:12px;
        color: #000000;
        font-weight:bold;
    }
    
    A, A:link, A:active, A:visited, A:hover, .Link_list li { color: #0932bf; }
    
    span.required
    {
        border-bottom: solid 1px orange;
    }

</style>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


        var openForm = jQuery('[id*="hdnOpenForm"]').val();
        jQuery('[id*="hdnOpenForm"]').val("");
        if (openForm != "") eval(openForm);

        //var url = jQuery('[id*="hdnPDF"]').val();
        //if (url != "") {
        //    window.open(url);
        // }
        // jQuery('[id*="hdnPDF"]').val("");

    });


</script>

<asp:Panel ID="pnlViewer" runat="server" >
    <table width="1100" cellpadding="0" cellspacing="0" border="0" class="HeaderRow">
        <tr>
            <td style="width:25px;">&nbsp;</td>
            <td style="width:25px;">&nbsp;</td>
            <td style="width:25px;">&nbsp;</td>
            <td style="width:50px;">Entity</td>
            <td style="width:150px;">Tab Name</td>
            <td style="width:600px;">Group Name</td>
            <td style="width:100px;" align="right">Attributes</td>
            <td style="width:25px;padding-right:4px;" align="right"><asp:HyperLink ID="lnkAdd" runat="server" ToolTip="Add Test Standard" ImageUrl="~/images/plus2.gif" /></td>
        </tr>
    
    </table>

    
        <asp:Repeater ID="rptData" runat="server">
            <HeaderTemplate>
                <table width="1000" class="table table-hover table-condensed">        
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="width:25px;" valign="top" align="center" >
                            <asp:ImageButton ID="btnDelete" runat="server" ToolTip='<%# DataBinder.Eval(Container.DataItem, "DeleteAlt") %>' OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');"
                                             RecordId = '<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' EntityId='<%# DataBinder.Eval(Container.DataItem, "EntityId") %>' 
                                             OnCommand="DoDelete" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "DeleteImage") %>' Enabled='<%# DataBinder.Eval(Container.DataItem, "ShowDelete") %>'/>
                    </td>
                    <td style="width:25px;" valign="top" align="center">
                            <asp:ImageButton ID="btnDown" runat="server" ToolTip='<%# DataBinder.Eval(Container.DataItem, "DownAlt") %>' ImageUrl='<%# DataBinder.Eval(Container.DataItem, "DownImage") %>' Enabled='<%# DataBinder.Eval(Container.DataItem, "ShowDown") %>'
                                            RecordId = '<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' EntityId='<%# DataBinder.Eval(Container.DataItem, "EntityId") %>' 
                                            OnCommand="Move" CommandArgument="Down" TabName='<%# DataBinder.Eval(Container.DataItem, "TabName") %>' />
                    </td>
                    <td style="width:25px;" valign="top" align="center">
                            <asp:ImageButton ID="ImageButton1" runat="server" ToolTip='<%# DataBinder.Eval(Container.DataItem, "UpAlt") %>' ImageUrl='<%# DataBinder.Eval(Container.DataItem, "UpImage") %>' Enabled='<%# DataBinder.Eval(Container.DataItem, "ShowUp") %>'
                                            RecordId = '<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' EntityId='<%# DataBinder.Eval(Container.DataItem, "EntityId") %>' 
                                            OnCommand="Move" CommandArgument="Up" TabName='<%# DataBinder.Eval(Container.DataItem, "TabName") %>'/>
                    </td>
                    <td style="width:50px;" valign="top"><%# DataBinder.Eval(Container.DataItem, "EntityId") %></td>
                    <td style="width:150px;" valign='top'><%# DataBinder.Eval(Container.DataItem, "TabName") %></td>
                    <td style="width:700px;" valign="top">
                            <asp:HyperLink ID="lnkEdit" runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "Editor") %>' Text='<%# DataBinder.Eval(Container.DataItem, "GroupName") %>' Width="680"/></td>
                    <td style="width:25px;" valign="top"><%# DataBinder.Eval(Container.DataItem, "Standards") %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <table width="1100" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td align="left" style="background-color:#e8e2e2; width:50%;">
                    <asp:RadioButtonList ID="rdoEntity" runat="server" TextAlign="Right" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="ToggleEnvironment">
                        <asp:ListItem Text="&nbsp;Learning Resources&nbsp;&nbsp;" Value="LRH" />
                        <asp:ListItem Text="&nbsp;Hand2Mind" Value="H2M" Selected="True" />
                    </asp:RadioButtonList>
                </td>
                <td  align="right" style="background-color:#e8e2e2; width:50%;">
                    <asp:RadioButtonList ID="rdoEnvironment" runat="server" TextAlign="Right" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="ToggleEnvironment" Visible="false">
                        <asp:ListItem Text="&nbsp;Production&nbsp;&nbsp;" Value="Production" Selected="True" />
                        <asp:ListItem Text="&nbsp;Development" Value="Development"  />
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
</asp:Panel>

<asp:HiddenField ID="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnOpenForm" runat="server" Value="" />
<asp:HiddenField ID="hdnViewRole" runat="server" Value="" />
<asp:HiddenField ID="hdnPDF" runat="server" Value="" />

