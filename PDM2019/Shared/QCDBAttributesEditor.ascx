<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QCDBAttributesEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.QCDBAttributesEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
 
    .readonly { background-color:#e8e2e2; }
    input.required { border: solid 1px orange; }
    select.required { border: solid 1px orange; }                 
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

   
    });


   
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }


</script>


<asp:Panel ID="pnlMain" runat="server" Width="600">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="180" Scrollbars="Auto">
        <table width="550" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td class="SubSubHead" style="width:150px;">Checkbox Text:</td>
                <td style="width:400px;"><asp:TextBox id="txtPortalText" runat="server" CssClass="Box" Width="380" MaxLength="100" Placeholder="Checkbox Text" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Type:</td>
                <td >
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="Box" Width="380" >
                        <asp:ListItem Text="Other" Value = "O" Selected="True" />
                        <asp:ListItem Text="Test" Value="T" />
                        <asp:ListItem Text="Warning" Value="W" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="SubSubHead">Test Requirement:</td>
                <td ><asp:DropDownList ID="ddlTestRequirement" runat="server" CssClass="Box" Width="380" /></td>
            </tr>
            <tr>
                <td class="SubSubHead"></td>
                <td><asp:CheckBox ID="chkDisplayOnCertificate" runat="server" TextAlign="Right" Text="&nbsp;Display on Certificate" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Certificate Text:</td>
                <td><asp:TextBox ID="txtCertificateText" runat="server" Width="380" MaxLength="100" Placeholder="Certificate Text" /></td>
            </tr>
            <tr>
                <td />
                <td align="right">
                    <asp:LinkButton ID="lnkCopyFromName" runat="server" Text="Use Checkbox Text" OnCommand="DoCopy" CommandArgument="PortalText"/>
                </td>
            </tr>
        </table>

    </asp:Panel>
        <table width="400" cellpadding="4">
            <tr>
                <td align="left" style="width:50%;" valign="middle">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;"  />
                </td>
                <td align="right" style="width:50%;" valign="middle">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" OnClientClick="javascript:return ValidateAwards();" />
                </td>
            </tr>
        </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />

