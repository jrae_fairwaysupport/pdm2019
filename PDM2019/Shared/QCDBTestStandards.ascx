<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QCDBTestStandards.ascx.cs" Inherits="YourCompany.Modules.PDM2019.QCDBTestStandards" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
    .readonly { background-color:#e8e2e2; }
    
</style>

<script language="javascript" type="text/javascript">
    function ToggleMore() {
        lnk = jQuery('[id*="lnkViewMore"]');

        if (lnk.text() == "View More") {
            lnk.text("View Less");
            jQuery('[id*="lblSpecs"]').css("max-height", "");
        }
        else {
            lnk.text("View More");
            jQuery('[id*="lblSpecs"]').css("max-height", "200px");
        }
        return false;
    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <asp:DataList ID="grdGroups" runat="server" Width="100%" OnItemDataBound="GetChecks">
            <ItemTemplate>
                <asp:Label id="lblGroupName" runat="server" CssClass="Head" Text='<%# DataBinder.Eval(Container.DataItem, "GroupName") %>' />
                <asp:Panel ID="pnlChecks" runat="server" style="padding-left:10px; border:solid 1px #e8e2e2;">
                    <asp:Repeater ID="rptChecks" runat="server">
                        <HeaderTemplate>
                            <table width="732" cellpadding="0" border="0" cellspacing="0">
                        </HeaderTemplate>
                        <ItemTemplate>
                                <tr>
                                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="img1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check0") %>' /></td>
                                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="lbl" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label0") %>' /></td>
                                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check1") %>' /></td>
                                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label1") %>' /></td>
                                    <td align="center" valign="top" style="width:25px;"><asp:Image ID="Image2" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Check2") %>' /></td>
                                    <td align="left" valign="top" style="width:219px;"><asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Label2") %>' /></td>
                                </tr>
                            
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </ItemTemplate>
        </asp:DataList>
    </asp:Panel>
</asp:Panel>          
	
	<hr />

	
	


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




