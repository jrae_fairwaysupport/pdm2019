<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QCDBStandardsEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.QCDBStandardsEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
 
    .readonly { background-color:#e8e2e2; }
    input.required { border: solid 1px orange; }
    select.required { border: solid 1px orange; }                 
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

   
    });


   
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }


</script>


<asp:Panel ID="pnlMain" runat="server" Width="500">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="180" Scrollbars="Auto">
        <table width="450" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead">Name:</td>
                <td style="width:300px;" ><asp:TextBox id="txtName" runat="server" width="280" MaxLength="100" Placeholder="Test Standard Name" CssClass="Box" /></td>
            </tr>
            <tr>
                <td class='SubSubHead'>Test Code:</td>
                <td ><asp:TextBox ID="txtTestCode" runat="server" Width="150" Placeholder="Test Code (reference only)" Enabled="false" CssClass="Box readonly" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Test Requirement:</td>
                <td><asp:DropDownList ID="ddlRequirement" runat="server" CssClass="Box" Width="280" /></td>
            </tr>
        </table>

    </asp:Panel>
        <table width="400" cellpadding="4">
            <tr>
                <td align="left" style="width:50%;" valign="middle">
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;"  />
                </td>
                <td align="right" style="width:50%;" valign="middle">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" OnClientClick="javascript:return ValidateAwards();" />
                </td>
            </tr>
        </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />

