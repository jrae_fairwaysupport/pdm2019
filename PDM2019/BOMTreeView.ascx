<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BOMTreeView.ascx.cs" Inherits="YourCompany.Modules.PDM2019.BOMTreeView" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        ToggleLabels();
        
        //var msg = jQuery('[id*="hdnShowMessage"]').val();
        //if (msg != "") {
        //    alert(msg);
       // }

        //jQuery('[id*="hdnShowMessage"]').val("");

        //var url = jQuery('[id*="hdnPDF"]').val();
        //if (url != "") {
        //    window.open(url);
       // }
       // jQuery('[id*="hdnPDF"]').val("");

    });

    function ToggleLabels() {

        jQuery('[id*="lblStatus"]').each(function (index) {
            var cls = $(this).text();
            $(this).addClass(cls);
        });


    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;" Width="750" Height="600" ScrollBars="Auto">
    <telerik:RadTreeView id="tree" runat="server" />

</asp:Panel>



