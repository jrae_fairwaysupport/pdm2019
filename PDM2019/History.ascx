<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="History.ascx.cs" Inherits="YourCompany.Modules.PDM2019.HistoryTab" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }
   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <table width="780" class="HeaderRow">
        <tr>
            <td style="width:150px;padding-left:4px;">Timestamp/User</td>
            <td style="width:150px;">Tab/Field</td>
            <td style="width:400px;">Edits/Actions</td>
            <td style="width:80px;padding-right:4px;" align="right">&nbsp;<asp:LinkButton id="lnkClearFilter"  runat="server" OnCommand="ClearFilter" Text="Clear Filter" Visible="false" /></td>
        </tr>
    </table>

    <asp:Panel ID="pnlHistory" runat="server" Width="780" Height="550" ScrollBars="Auto" style="border-bottom:solid 1px black;">
        <asp:DataList ID="lstHistory" runat="server" Width="760" CssClass="table table-hover table-condensed" Table="vwHistory">
            <ItemTemplate>
                <table width="100%" cellpadding="2" cellspacing="0" border="0">
                    <tr>
                        <td valign="top" align="left">
                            <asp:Label ID="lblTimestamp" runat="server" Width="150" style="padding-left:4px;vertical-align:top;" Text='<%# DataBinder.Eval(Container.DataItem, "TimestampUser")  %>' />
                        </td>
                        <td valign="top" align="left">
                            <asp:LinkButton ID="lnkTab" runat="server" Width="150" style="padding-left:4px;vertical-align:top;text-decoration:none;" Text='<%# DataBinder.Eval(Container.DataItem, "TabName") %>' OnCommand="DoFilter" CommandArgument="TabName" />
                            <br />
                            <asp:LinkButton ID="lnkField" runat="server" Width="150" style="padding-left:4px;vertical-align:top;text-decoration:none;" Text='<%# DataBinder.Eval(Container.DataItem, "FieldName") %>' OnCommand="DoFilter" CommandArgument="FieldName" TabName='<%# DataBinder.Eval(Container.DataItem, "TabName") %>' />
                        </td>
                        <td valign="top" align="left">
                            <asp:Label ID="lblAction" runat="server" Width="420" style="padding-left:4px; padding-right:4px;vertical-align:top;" Text='<%# DataBinder.Eval(Container.DataItem, "Action") %>' />        
                        </td>
                    </tr>
                </table>

            </ItemTemplate>
        </asp:DataList>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnModuleId" runat="server" Value="-1" />




