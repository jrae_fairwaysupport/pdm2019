using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class VendorPriceBreaksControl: PortalModuleBase, IActionable
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
                currPage.Title = "PDM Vendor Price Breaks Editor";


                if (!IsPostBack)
                {
                    PopulateControls();

                    LoadData();
                }


            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion


        private int PartId
        {
            get
            {
                string raw = Request.QueryString["id"];
                int retval = -1;
                Int32.TryParse(raw, out retval);
                return retval;
            }
        }

        private void PopulateControls()
        {
        }

        private string GetUserSetting(string key, string defaultValue = "")
        {
            if (DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key) != null)
                return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key);
            else
                return defaultValue;
        }

        private void SaveUserSetting(string key, string value)
        {
            DotNetNuke.Services.Personalization.Personalization.SetProfile("PDM", key, value);
        }

        private string environment { get { return GetUserSetting("Environment", "Production"); } }
        private string entityId { get { return GetUserSetting("EntityId", "LRH"); } }

        protected void DoLookup(object source, DotNetNuke.UI.WebControls.DNNTextSuggestEventArgs e)
        {
            DNNTextSuggest txtSearch = (DNNTextSuggest)source;
            txtSearch.CaseSensitive = false;

            string fieldName = txtSearch.Attributes["Field"];


            FirstCharSearch(e.Nodes, e.Text.ToUpper(), fieldName);

        }

        private void FirstCharSearch(DNNNodeCollection objNodes, string entry, string fieldName)
        {
            entry = entry.Replace("[", "").Replace("]", "").Replace("'", "''");

            if (string.IsNullOrEmpty(entry.Trim())) return;

            //SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            //string dbName = entityId.Equals("LRH") ? "VMLREI11" : "H2MVMLREI11";
            //SvcLibrary.DBHandler vmfg = new SvcLibrary.DBHandler(config, null, environment, dbName);

            string keyParam = entry.ToUpper().Substring(0, 1);
            string key = string.Format("PDM_{0}_{1}", fieldName, keyParam);

            string queryTmpl8 = string.Empty;
            string filterTmpl8 = string.Empty;

            switch (fieldName)
            {
                case "ParentPartSuggest": 
                    queryTmpl8 = "SELECT ID, DESCRIPTION AS NAME FROM vwPARTS WHERE ID LIKE '{0}%' ORDER BY ID";
                    filterTmpl8 = "ID LIKE '{0}%'";
                    break;
                case "MainComponentPartSuggest":
                    queryTmpl8 = "SELECT ID, DESCRIPTION AS NAME FROM vwPARTS WHERE ID LIKE '{0}%' ORDER BY ID";
                    filterTmpl8 = "ID LIKE '{0}%'";
                    break;
                default:
                    //PreferredVendor, PreferredVendorId, MoldShareVendor
                    queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME";
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'";
                    break;

            }

            ProductDevelopment controller = new ProductDevelopment(entityId, environment);
            DataTable dt = new DataTable();
            if (Application[key] == null)
            {
                dt = controller.ExecuteReader(string.Format(queryTmpl8, keyParam));
                Application[key] = dt;
            }
            else
            {
                dt = (DataTable)Application[key];
            }

            string filter = string.Format(filterTmpl8, entry);

            DataRow[] arr = dt.Select(filter);
            int items = 0;
            DNNNode o;

            foreach (DataRow dr in arr)
            {
                string Id = controller.GetValue<string>(dr, "ID", string.Empty);
                string Name = controller.GetValue<string>(dr, "NAME", string.Empty).Replace("[", "(").Replace("]", ")");

                string displayText = string.Empty;
                if (Id.StartsWith(entry))
                {
                    displayText = Id + " - " + Name;
                }
                else if (Name.StartsWith(entry))
                {
                    displayText = Name + " [" + Id + "]";
                }

                if (!string.IsNullOrEmpty(displayText))
                {
                    o = new DNNNode(displayText);
                    o.ID = Id;
                    o.set_CustomAttribute("selectedID", Id);
                    objNodes.Add(o);

                    items++;

                }

                if (items >= 10) break;
            }

        }



        public void LoadData()
        {
            int recordId = Int32.Parse(Request.QueryString["RecordId"]);
            if (recordId == -1) return;

            ProductDevelopment controller = new ProductDevelopment(entityId, environment);
            DataTable dt = controller.VendorPriceBreak_Load(recordId);
            controller.PopulateForm(pnlMain, dt);
        }



        private bool Validate()
        {
            ProductDevelopment controller = new ProductDevelopment(entityId, environment);
            string msg = string.Empty;

            controller.ValidateText(txtParentPartId, true, "Vendor Id", ref msg);
            controller.ValidateText(txtVendorPartId, true, "Vendor Part Id", ref msg);

            //ideally, there should be at least 1 q/p pair specified and only pairs...but the data doesn't suggest this is a requirement
            controller.ValidateNumber(txtQuantity1, false, "Quantity 1", ref msg);
            controller.ValidateDecimal(txtPrice1, false, "Price 1", ref msg);
            controller.ValidateNumber(txtQuantity2, false, "Quantity 2", ref msg);
            controller.ValidateDecimal(txtPrice2, false, "Price 2", ref msg);
            controller.ValidateNumber(txtQuantity3, false, "Quantity 3", ref msg);
            controller.ValidateDecimal(txtPrice3, false, "Price 3", ref msg);
            controller.ValidateNumber(txtQuantity4, false, "Quantity 4", ref msg);
            controller.ValidateDecimal(txtPrice4, false, "Price 4", ref msg);
            controller.ValidateNumber(txtQuantity5, false, "Quantity 5", ref msg);
            controller.ValidateDecimal(txtPrice5, false, "Price 5", ref msg);




            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            int recordId = Int32.Parse(Request.QueryString["RecordId"]);


            ProductDevelopment controller = new ProductDevelopment(entityId, environment);
            controller.VendorPriceBreak_Save(this.UserInfo.Username, recordId, PartId, pnlMain);

            SaveUserSetting("MainTab", ((int)PDMTab.ProductDevelopment).ToString());
            SaveUserSetting("SubTab", ((int)PDMTab.PDVendorPrice).ToString()); 
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "id", PartId.ToString()), true);
        }

     
    }
}
 