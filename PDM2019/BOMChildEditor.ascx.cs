using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class BOMChildEditor: LRHPopup 
    {
        protected override string PAGE_TITLE { get { return "PDM Child Part"; } }

        protected override DataSet LoadData()
        {
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "vwChildParts"); //Parts
            
            //DataTable parts = ds.Tables["Parts"];
            //if (parts.Rows.Count > 0) sku = controller.GetValue<string>(parts.Rows[0], "PartId", string.Empty);

            if (ChildPartRecordId <= 0)
            {
                txtQuantity.Text = "1";
                return ds;
            }

            DataTable children = ds.Tables["vwChildParts"];

            DataRow[] arr = children.Select("Id = " + ChildPartRecordId.ToString());
            if (arr.Length > 0)
            {
                txtChildPartId.Text = controller.GetValue<string>(arr[0], "ChildPartSuggest", string.Empty);
                txtQuantity.Text = controller.GetValue<decimal>(arr[0], "Quantity", 0).ToString();
                txtQuantity.Attributes.Add("Original", txtQuantity.Text);
                txtQuantity.Attributes.Add("Sku", controller.GetValue<string>(arr[0], "ChildPartId", string.Empty));
            }

            return ds;
        }

        protected override bool Validate()
        {
            throw new NotImplementedException();
        }

        private bool Validate(out int childPartUid, out decimal qty, out string childPartId, out string childPartDescription)
        {
            childPartUid = -1;
            qty = 0;
            childPartId = string.Empty;
            childPartDescription = string.Empty;

            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            //Basic requirements
            string raw = controller.ValidateText(txtChildPartId, (ChildPartRecordId <= 0), "Child Part Id", ref msg);
            decimal? test = controller.ValidateDecimal(txtQuantity, true, "Qty/Per", ref msg);
            if (test.HasValue) qty = test.Value;

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowMessage.Value = msg;
                return false;
            }

            //New Part - Child is unique to parent
            if (ChildPartRecordId <= 0)
            {
                controller.DNNTextSuggestValues(raw, out childPartId, out childPartDescription, '-');

                //MAKE SURE WE'RE NOT OUR OWN PARENT
                if (childPartId.Equals(sku))
                {
                    hdnShowMessage.Value = "Cannot assign the Parent Part as the Child.";
                    return false;
                }


                DataSet ds = controller.LoadPart(PartId, "vwChildParts");
                DataTable dt = ds.Tables["vwChildParts"];
                DataRow[] arr = dt.Select(string.Format("ChildPartId = '{0}'", childPartId));
                if (arr.Length > 0)
                {
                    hdnShowMessage.Value = string.Format("Child {0} has already been added to this Part.", childPartId);
                    return false;
                }

                //Child part is valid
                string sql = "SELECT * FROM Parts WHERE PartId = @P0";
                dt = controller.pdm.ExecuteReader(sql, childPartId);
                if (dt.Rows.Count == 0)
                {
                    hdnShowMessage.Value = childPartId + " does not appear to be a valid Part.";
                    return false;
                }

                childPartDescription = controller.GetValue<string>(dt.Rows[0], "Description", string.Empty);
                childPartUid = controller.GetValue<int>(dt.Rows[0], "Id", -1);
            }

            controller.DNNTestSuggestIsSelectionValid(txtChildPartId, "Child Part Id", ref msg);



            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected override void  LockDownControls()
        {
            string child = Request.QueryString["child"];
            if (string.IsNullOrEmpty(child)) return;
            int test = 0;
            if (Int32.TryParse(child, out test)) ChildPartRecordId = test;

            txtChildPartId.Enabled = (ChildPartRecordId <= 0);
        }
        
        private int ChildPartRecordId
        {
            get { return Int32.Parse(hdnChildPartId.Value); }
            set { hdnChildPartId.Value = value.ToString(); }
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            int childPartUid = -1;
            decimal qty = 0;
            string childPartId = string.Empty;
            string childPartDescription = string.Empty;

            string raw = txtQuantity.Attributes["Original"];
            decimal originalQty = 0;
            decimal.TryParse(raw, out originalQty);

            if (!Validate(out childPartUid, out qty, out childPartId, out childPartDescription)) return;
            if (string.IsNullOrEmpty(childPartId)) childPartId = txtQuantity.Attributes["Sku"];

            ChildPartSave(ChildPartRecordId, childPartUid, childPartId, childPartDescription, qty, originalQty);


            CloseForm("b", "true");

        }

        public void ChildPartSave(int childPartRecordId, int childPartUid, string childPartId, string childPartDescription, decimal quantity, decimal originalQuantity)
        {
            Controller controller = new Controller(entityId, environment);
            string fieldName = "Child Part: " + childPartId;

            if (childPartRecordId == -1)
            {
                controller.history.Log(PartId, this.UserInfo.Username, "Bill of Materials", fieldName, string.Empty, "Added to BOM<br />Quantity: " + quantity.ToString());
                controller.pdm.InsertRecord("ChildParts", "id", "Part_Id", PartId, "ChildPartUid", childPartUid, "Quantity", quantity, "CreatedDate", DateTime.Now, "ModifiedDate", DateTime.Now);
            }
            else
            {
                controller.history.Log(PartId, this.UserInfo.Username, "Bill of Materials", fieldName, originalQuantity.ToString(), quantity.ToString());
                controller.pdm.UpdateRecord("ChildParts", "id", childPartRecordId, "Quantity", quantity, "ModifiedDate", DateTime.Now);
            }
        }

    }
}
 