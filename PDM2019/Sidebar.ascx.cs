using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class Sidebar : LRHViewer //PortalModuleBase, IActionable
    {
        public event EventHandler SomethingHistoricalHappened;
        public event EventHandler OpenBOM;
        public event EventHandler CloseBOM;
        private List<string> headerRoles = new List<string>() { "pd", "p", "ad", "c" };
       
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //    try
        //    {
                
        //        if (!IsPostBack)
        //        {
                    
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.ProcessModuleLoadException(this, ex);
        //    }
        //}

        
        //#region IActionable Members

        //public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        //{
        //    get
        //    {
        //        //create a new action to add an item, this will be added to the controls
        //        //dropdown menu
        //        ModuleActionCollection actions = new ModuleActionCollection();
        //        actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
        //            ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
        //             true, false);

        //        return actions;
        //    }
        //}

        //#endregion



        
        //private int PartId
        //{
        //    get
        //    {
        //        string raw = Request.QueryString["id"];
        //        int retval = -1;
        //        Int32.TryParse(raw, out retval);
        //        return retval;
        //    }
        //}


        //private string GetUserSetting(string key, string defaultValue = "")
        //{
        //    if (DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key) != null)
        //        return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key);
        //    else
        //        return defaultValue;
        //}

        //private void SaveUserSetting(string key, string value)
        //{
        //    DotNetNuke.Services.Personalization.Personalization.SetProfile("PDM", key, value);
        //}


        
        //private void PopulateForm(object parent, DataSet ds, PDM2019Controller controller)
        //{
        //    DataTable dt = null;

        //    foreach (Control child in ((Control)parent).Controls)
        //    {
        //        switch(child.GetType().Name)
        //        {
        //            case "Label":
        //                Label lbl = (Label)child;
        //                if (!string.IsNullOrEmpty(lbl.Attributes["Table"]))
        //                {
        //                    dt = ds.Tables[lbl.Attributes["Table"]];
        //                    lbl.Text = controller.GetValue<string>(dt.Rows[0], lbl.Attributes["Field"], string.Empty);
        //                }
        //                break;
        //            default:
        //                break;
        //        }

        //        PopulateForm(child, ds, controller);
        //    }
        //}
    
        //private string environment { get { return GetUserSetting("Environment", "Production"); } }
        //private string entityId { get { return GetUserSetting("EntityId", "LRH"); } }

        public void SetWidth(int newWidth)
        {
            pnlMain.Width = (Unit)newWidth;
        }

        //public void LoadData(DataSet ds)
        //{
        //    PDM2019Controller controller = new PDM2019Controller(entityId, environment);
        //    PopulateForm(pnlMain, ds, controller);
        //}


        //private int ParentModuleId
        //{
        //    get
        //    {
        //        try { return Int32.Parse(hdnModuleId.Value); }
        //        catch { return -1; }
        //    }

        //    set
        //    {
        //        hdnModuleId.Value = value.ToString();
        //    }
        //}

        //private bool IsAdministrator
        //{
        //    get
        //    {
        //        if (this.UserInfo.IsSuperUser) return true;
        //        if (this.UserInfo.IsInRole("Administrators")) return true;
        //        return false;
        //    }
        //}

        //public void Initialize(int moduleId, DataSet ds)
        //{
        //    ParentModuleId = moduleId;

        //    DataTable dt = ds.Tables["Parts"];
        //    if (dt == null) return;
        //    if (dt.Rows.Count == 0) return;
        //    string status = dt.Rows[0]["Status"].ToString();
        //    lblStatus.Text = status;
        //    lblStatus.CssClass = lblStatus.CssClass.Append(status, " ");
        //    if (status.Equals("Rejected")) lblStatus.ToolTip = dt.Rows[0]["RejectionReason"].ToString();

        //    string sku = dt.Rows[0]["PartId"].ToString();
        //    lnkPartHistory.NavigateUrl = EditUrl("mid", moduleId.ToString(), "History", "id", PartId.ToString()); //sku's become useless with weird characters

        //    string viewRole = GetUserSetting("View Role");
        //    lnkSubmit.Visible = viewRole.Equals("Product Development") || IsAdministrator;
        //    lnkApprove.Visible = viewRole.Equals("Purchasing") || IsAdministrator;
        //    lnkReject.Visible = viewRole.Equals("Purchasing") || IsAdministrator;
        //    lnkReject.NavigateUrl = EditUrl("mid", moduleId.ToString(), "Reject", "id", PartId.ToString());

        //    if (status.Equals("Approved"))
        //    {
        //        lnkSubmit.Visible = false;
        //        lnkApprove.Visible = false;
        //        lnkReject.Visible = false;
        //    }


        //}

        public void Initialize(DataSet ds)
        {
            DataTable dt = ds.Tables["Parts"];
            if (dt == null) return;
            if (dt.Rows.Count == 0) return;

            string workflowStatus = string.Empty;
            DataTable approvals = ds.Tables["vwApprovals"];
            if (approvals != null)
            {
                if (approvals.Rows.Count > 0)
                {
                    if (!approvals.Rows[0].IsNull("WorkflowStatus")) workflowStatus = approvals.Rows[0]["WorkflowStatus"].ToString();
                }
            }


            string status = GetUserSetting<string>(PartId, "Status", "Draft");
            lblStatus.Text = status;
            lblStatus.CssClass = lblStatus.CssClass.Append(status, " ");
            if (status.Equals("Rejected")) lblStatus.ToolTip = dt.Rows[0]["RejectionReason"].ToString();

            //string sku = GetUserSetting<string>(PartId, "Sku", string.Empty);
            lnkPartHistory.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "History", "id", PartId.ToString(), "e", environment, "eid", entityId); //sku's become useless with weird characters

            string viewRole = ViewRole;
            lnkSubmit.Visible = (viewRole.Equals("pd") || viewRole.Equals("ad")) && (status.Equals("Draft") || status.Equals("Rejected"));
            lnkApprove.Visible = ((viewRole.Equals("p") || viewRole.Equals("ad")) && status.Equals("Pending") && workflowStatus.Contains("Purchasing"))
                                    || ((viewRole.Equals("a") || viewRole.Equals("ad")) && status.Equals("Pending") && workflowStatus.Contains("Accounting"));
            lnkReject.Visible = (viewRole.Equals("p") || viewRole.Equals("ad")) && (status.Equals("Pending"));
            lnkReject.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "Reject", "id", PartId.ToString(), "e", environment, "eid", entityId);

            if (status.Equals("Approved"))
            {
                lnkSubmit.Visible = false;
                lnkApprove.Visible = false;
                lnkReject.Visible = false;
            }

            lnkEditHeader.NavigateUrl = EditUrl("id", PartId.ToString(), "SidebarEditor", "e", environment, "eid", entityId, "r", ViewRole, "mid", ParentModuleId.ToString());
            lnkEditHeader.Visible = headerRoles.Contains(ViewRole);

        }

        
        private string GetModuleSetting(string key)
        {
            key = entityId + environment + key;
            PDM2019Settings settingsData = new PDM2019Settings(this.TabModuleId);
            return settingsData.ReadSetting<string>(key, string.Empty);
        }

        

        protected void DoBOM(object obj, CommandEventArgs args)
        {
            string action = args.CommandName;
            if (action.Equals("Open"))
            {
                lnkOpenBOM.Visible = false;
                lnkCloseBOM.Visible = true;
                if (this.OpenBOM != null) this.OpenBOM(this, new EventArgs());
            }
            else
            {
                lnkOpenBOM.Visible = true;
                lnkCloseBOM.Visible = false;
                if (this.CloseBOM != null) this.CloseBOM(this, new EventArgs());
            }
        }

        public void ShowClose()
        {
            lnkOpenBOM.Visible = false;
            lnkCloseBOM.Visible = true;
        }

        

        protected void DoSubmit(object obj, CommandEventArgs args)
        {
            string msg = string.Empty;

            DataSet ds = SubmitPart(ref msg);
            Controller controller = new Controller(entityId, environment);

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowSidebarMessage.Value = msg;
                return;
            }

            EmailController email = new EmailController(entityId, environment);
            //email.SendSubmit(PartId, GetModuleSetting("PartUrlTemplate"), this.PortalId, this.UserInfo.Email, "PDM LRH Purchasing");
            email.SendLRNotification(controller, PartId, ds, GetModuleSetting("PartUrlTemplate"), this.PortalId, this.UserInfo, lblStatus.Text, "Pending");

            lblStatus.Text = "Pending";
            lblStatus.CssClass = "button Pending";
            lnkSubmit.Visible = false;
            lblWorkflowStatus.Text = "<br />Pending Purchasing Approval.";

            hdnShowSidebarMessage.Value = "Part has been submitted for Approval.";



            if (this.SomethingHistoricalHappened != null) this.SomethingHistoricalHappened(this, new EventArgs());
        }

        private DataSet SubmitPart(ref string msg)
        {
            msg = string.Empty;
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "Parts", "vwPartInfos", "vwApprovals", "vwVendorPriceBreaks");

            SubmitPart_Validate(controller, ds, "Parts", "PartId", "Part Id", ref msg);
            SubmitPart_Validate(controller, ds, "Parts", "UPCCode", "UPC Code", ref msg);
            SubmitPart_Validate(controller, ds, "Parts", "Description", "Description", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "Company", "Company", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "ParentPartId", "Master Part Id", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "PreferredVendor", "Preferred Vendor", ref msg);
            //SubmitPart_Validate(controller, ds, "vwPartInfos", "MaterialCost", "Material Cost", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "LeadTime", "Lead Time", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "MinimumOrderQuantity", "Minimum Order Quantity", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "YearIntroduced", "Year Introduced", ref msg);
            SubmitPart_Validate(controller, ds, "vwPartInfos", "FirstYearForecast", "First Year Forecast", ref msg);
            SubmitPart_Validate_MoldShare(controller, ds, ref msg);
            SubmitPart_Validate_Magnets(controller, ds, ref msg);
            SubmitPart_Validate_Royalty(controller, ds, ref msg);

            //Fab or Purchased Crap
            DataTable vwPartInfos = ds.Tables["vwPartInfos"];
            bool purchased = controller.pdm.GetValue<bool>(vwPartInfos.Rows[0], "Purchased", false);
            string fabLocation = string.Empty;
            if (purchased)
            {
                SubmitPart_Validate(controller, ds, "vwPartInfos", "MaterialCost", "Material Cost", ref msg);
            }
            else
            {
                fabLocation = controller.pdm.GetValue<string>(vwPartInfos.Rows[0], "FabricationLocation", string.Empty);
                if (!fabLocation.Equals("FabUs")) SubmitPart_Validate(controller, ds, "vwPartInfos", "MaterialCost", "Material Cost", ref msg);
            }
            
            if (!string.IsNullOrEmpty(msg))
            {
                msg = "The following fields need values before the Part may be submitted:\r\n" + msg;
                return null;
            }




            //if it's a new part, it must be unique and 12 digits
            //if it's based on existing, must have 12 digits
            DataTable dt = ds.Tables["Parts"];
            string upccode = controller.GetValue<string>(dt.Rows[0], "UpcCode", string.Empty);
            dt = ds.Tables["vwPartInfos"];
            bool isNewPart = controller.GetValue<bool>(dt.Rows[0], "IsNewPart", false);

            if (isNewPart)
            {
                string sql = "SELECT COUNT(*) AS THECOUNT FROM Parts WHERE UpcCode = @P0 AND Id <> @P1 AND Active = 1";
                int count = controller.pdm.FetchSingleValue<int>(sql, "THECOUNT", upccode, PartId);
                if (count > 0) msg = "When creating a New Part, the UPC must be unique.\r\n";
            }

            if (upccode.Length != 12) msg = "UPC must be 12 digits long.";
            if (!string.IsNullOrEmpty(msg)) return null;

            //2020-03-02 -would be awesome if the BAs could get their shit together.
            ////vendor price breaks are required in some cases
            //if (!(purchased && fabLocation.Equals("FabUs")))
            //{
            //    DataTable vwVendorPriceBreaks = ds.Tables["vwVendorPriceBreaks"];
            //    if (vwVendorPriceBreaks.Rows.Count == 0)
            //    {
            //        msg = "At least one Vendor Price Break must be specified.";
            //        return null;
            //    }
            //}
            if (purchased)
            {
                DataTable vwVendorPriceBreaks = ds.Tables["vwVendorPriceBreaks"];
                if (vwVendorPriceBreaks.Rows.Count == 0)
                {
                    msg = "At least one Vendor Price Break must be specified.";
                    return null;
                }
            }

            controller.history.Log(PartId, this.UserInfo.Username, "Header", "Status", partStatus, "Pending");
            controller.pdm.UpdateRecord("Parts", "id", PartId, "Status", "Pending", "SubmitUserId", this.UserInfo.Username, "ModifiedDate", DateTime.Now);

            //approvals
            if (ApprovalsExist(ds))
                controller.pdm.UpdateRecord("Approvals", "PartId", PartId, "SubmittedByUserId", this.UserId, "SubmittedByUserName", this.UserInfo.Username, "SubmittedByEmail", this.UserInfo.Email, "SubmittedDate", DateTime.Now);
            else
                controller.pdm.InsertRecord("Approvals", "PartId", "PartId", PartId, "SubmittedByUserId", this.UserId, "SubmittedByUserName", this.UserInfo.Username, "SubmittedByEmail", this.UserInfo.Email, "SubmittedDate", DateTime.Now);

            return ds;
        }

        private bool ApprovalsExist(DataSet ds)
        {
            DataTable approvals = ds.Tables["vwApprovals"];
            if (approvals == null) return false;

            //defense
            if (approvals.Rows.Count == 0) return false;

            DataRow dr = approvals.Rows[0];
            Controller controller = new Controller(entityId, environment);
            int linkedId = controller.pdm.GetValue<int>(dr, "LinkedId", -1);
            return (linkedId != -1);
        }

        private void SubmitPart_Validate_Royalty(Controller controller, DataSet ds, ref string msg)
        {
            DataTable dt = ds.Tables["vwPartInfos"];
            if (dt == null) return;
            if (dt.Rows.Count == 0) return;
            DataRow dr = dt.Rows[0];

            bool royaltyItem = controller.pdm.GetValue<bool>(dr, "RoyaltyItem", false);
            if (! royaltyItem) return;

            string payableTo = controller.pdm.GetValue<string>(dr, "RoyaltyPayableTo", string.Empty);
            string royaltyType = controller.pdm.GetValue<string>(dr, "RoyaltyType", string.Empty);
            decimal royaltyAmount = controller.pdm.GetValue<decimal>(dr, "RoyaltyAmount", 0) + controller.pdm.GetValue<decimal>(dr, "RoyaltyPercentage", 0); //not concerned with amount

            if (string.IsNullOrEmpty(payableTo))
            {
                if (! string.IsNullOrEmpty(msg)) msg += ", ";
                msg += "Royalty Payable To";
            }

            if (string.IsNullOrEmpty(royaltyType))
            {
                if (! string.IsNullOrEmpty(msg)) msg += ", ";
                msg += "Royalty Type";
            }

            if (royaltyAmount == 0)
            {
                if (! string.IsNullOrEmpty(msg)) msg += ", ";
                msg += "Royalty Amount/Percentage";
            }

        
        }

        private void SubmitPart_Validate_Magnets(Controller controller, DataSet ds, ref string msg)
        {
            DataTable dt = ds.Tables["Magnets"];
            if (dt == null) return;
            if (dt.Rows.Count == 0) return;
            DataRow dr = dt.Rows[0];

            bool containsMagnets = controller.pdm.GetValue<bool>(dr, "ContainsMagnets", false);
            if (!containsMagnets) return;
            SubmitPart_Validate(controller, ds, "Magnets", "FileName", "Legal Document for Magnets", ref msg);
        }

        private void SubmitPart_Validate_MoldShare(Controller controller, DataSet ds, ref string msg)
        {
            DataTable dt = ds.Tables["vwPartInfos"];
            if (dt == null) return;
            if (dt.Rows.Count == 0) return;
            DataRow dr = dt.Rows[0];

            bool moldShare = controller.pdm.GetValue<bool>(dr, "MoldShare", false);
            if (!moldShare) return;
            SubmitPart_Validate(controller, ds, "vwPartInfos", "MoldShareVendorId", "Mold Share Vendor", ref msg);
        }

        private void SubmitPart_Validate(Controller controller, DataSet ds, string table, string column, string field, ref string msg)
        {
            bool error = false;

            DataTable dt = ds.Tables[table];
            if (dt == null)
            {
                error = true;
            }
            else
            {
                if (dt.Rows.Count == 0) error = true;
            }

            if (!error)
            {
                DataRow dr = dt.Rows[0];
                if (string.IsNullOrEmpty(controller.pdm.GetValue<string>(dr, column, string.Empty))) error = true;
            }

            if (error)
            {
                if (!string.IsNullOrEmpty(msg)) msg += ", ";
                msg += field;
            }
        }



        protected void DoApprove(object obj, CommandEventArgs args)
        {
            string msg = string.Empty;
            string targetStatus = string.Empty;

            DataSet ds = ApprovePart(ref msg, ref targetStatus);

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowSidebarMessage.Value = msg;
                return;
            }

            EmailController email = new EmailController(entityId, environment);
            Controller controller = new Controller(entityId, environment);
            email.SendLRNotification(controller, PartId, ds, GetModuleSetting("PartUrlTemplate"), this.PortalId, this.UserInfo, lblStatus.Text, targetStatus);

            lblStatus.Text = targetStatus;
            lblStatus.CssClass = "button " + targetStatus;


            if (targetStatus.Equals("Pending"))
            {
                hdnShowSidebarMessage.Value = "Accounting has been notified, requesting their edits/Approval.";
                lblWorkflowStatus.Text = "<br />Pending Accounting Approval.";
            }
            else
            {
                hdnShowSidebarMessage.Value = "Part has been Approved and sent to Visual.";
                lblWorkflowStatus.Text = string.Empty;
            }


            if (this.SomethingHistoricalHappened != null) this.SomethingHistoricalHappened(this, new EventArgs());
        }

        private DataSet ApprovePart(ref string msg, ref string targetStatus)
        {
            msg = string.Empty;
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "Parts", "vwPurchasingPartInfos", "vwApprovals", "vwMarketingInfos", "vwPartInfos");

            DataTable approvals = ds.Tables["vwApprovals"];
            DataRow dr = approvals.Rows[0];
            string workflowStatus = controller.pdm.GetValue<string>(dr, "WorkflowStatus", "");
            targetStatus = "Pending"; //if done by purchasing
            if (workflowStatus.Contains("Accounting")) targetStatus = "Approved";


            if (targetStatus.Equals("Pending")) //PURCHASING TOOK THIS ACTION
            {
                SubmitPart_Validate(controller, ds, "Parts", "UpcCode", "UPC Code", ref msg);
                SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "SafetyStock", "Safety Stock", ref msg);
                SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "HtsCode", "HTS Code", ref msg);
                SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "MaterialCode", "Material Code", ref msg);
                SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "CountryOfOrigin", "Country of Manufacture", ref msg);
                SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "PortOfOrigin", "Port of Origin", ref msg);
            }
            else //ACCOUNTING
            {
                //SubmitPart_Validate(controller, ds, "vwPartInfos", "FreightInCost", "Freight in Cost", ref msg);
                SubmitPart_Validate(controller, ds, "vwPurchasingPartInfos", "AddRate", "Add Rate", ref msg);
                SubmitPart_Validate(controller, ds, "vwPartInfos", "ParentPartQuantityConversion", "Parent Part Quantity Conversion", ref msg);
                SubmitPart_Validate(controller, ds, "vwPartInfos", "NetPrice", "Net Price", ref msg);
                SubmitPart_Validate(controller, ds, "vwPartInfos", "RetailPrice", "Retail Price", ref msg);
            }

            if (!string.IsNullOrEmpty(msg))
            {
                msg = "The following fields need values before the Part may be approved:\r\n" + msg;
                return null;
            }



            if (targetStatus.Equals("Pending"))
            {
                controller.history.Log(PartId, this.UserInfo.Username, "Header", "Status", partStatus, "Approved by Purchasing.");
                if (ApprovalsExist(ds))
                    controller.pdm.UpdateRecord("Approvals", "PartId", PartId, "PurchasingApprovalByUserId", this.UserId, "PurchasingApprovalByUserName", this.UserInfo.Username,
                                                    "PurchasingApprovalByEmail", this.UserInfo.Email, "PurchasingApprovalDate", DateTime.Now);
                else
                    controller.pdm.InsertRecord("Approvals", "PartId", "PartId", PartId, "PurchasingApprovalByUserId", this.UserId, "PurchasingApprovalByUserName", this.UserInfo.Username,
                                                    "PurchasingApprovalByEmail", this.UserInfo.Email, "PurchasingApprovalDate", DateTime.Now);

            }
            else
            {
                controller.history.Log(PartId, this.UserInfo.Username, "Header", "Status", partStatus, "Approved by Accounting.");
                controller.history.Log(PartId, this.UserInfo.Username, "Header", "Status", partStatus, "Approved");
                controller.pdm.UpdateRecord("Parts", "id", PartId, "Status", "Approved", "ModifiedDate", DateTime.Now, "WasApproved", true, "LoadEvents", true);

                if (ApprovalsExist(ds))
                    controller.pdm.UpdateRecord("Approvals", "PartId", PartId, "AccountingApprovalByUserId", this.UserInfo.UserID, "AccountingApprovalByUserName", this.UserInfo.Username,
                                                        "AccountingApprovalByEmail", this.UserInfo.Email, "AccountingApprovalDate", DateTime.Now);
                else
                    controller.pdm.InsertRecord("Approvals", "PartId", "PartId", PartId, "AccountingApprovalByUserId", this.UserInfo.UserID, "AccountingApprovalByUserName", this.UserInfo.Username,
                                                        "AccountingApprovalByEmail", this.UserInfo.Email, "AccountingApprovalDate", DateTime.Now);

                DataTable purchasingPartInfos = ds.Tables["vwPurchasingPartInfos"];
                if (purchasingPartInfos != null)
                {
                    if (purchasingPartInfos.Rows.Count > 0)
                    {
                        bool holdReceiptsCheck = controller.pdm.GetValue<bool>(purchasingPartInfos.Rows[0], "HoldReceiptsCheck", false);
                        string holdReceipts = holdReceiptsCheck ? "Y" : "N";
    
                        Exchange x = new Exchange();
                        string batchId = x.CreateBatch(entityId, sku, PartId, "DCMS", "SKU");
                        x.AddBatchFields(batchId, "HOLD_RECEIPTS", holdReceipts, null, true);
                    }
                }
            }

            lnkApprove.Visible = false;
            lnkReject.Visible = false;

            return ds;
        }

    }
}
 