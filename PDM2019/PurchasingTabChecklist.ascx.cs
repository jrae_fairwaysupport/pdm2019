using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class PurchasingChecklistTab: PortalModuleBase, IActionable
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (!IsPostBack)
                {
                }


            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion


        private int PartId
        {
            get
            {
                string raw = Request.QueryString["id"];
                int retval = -1;
                Int32.TryParse(raw, out retval);
                return retval;
            }
        }
       
  

        private int ParentModuleId
        {
            get
            {
                try { return Int32.Parse(hdnModuleId.Value); }
                catch { return -1; }
            }

            set
            {
                hdnModuleId.Value = value.ToString();
            }
        }

        private string GetUserSetting(string key, string defaultValue = "")
        {
            if (DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key) != null)
                return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key);
            else
                return defaultValue;
        }

        private void SaveUserSetting(string key, string value)
        {
            DotNetNuke.Services.Personalization.Personalization.SetProfile("PDM", key, value);
        }


        public void Initialize(int moduleId, string viewRole, string partStatus, DataSet ds, bool IsAdministrator, string sku)
        {
            pnlActions.Visible = viewRole.Equals("p") || IsAdministrator;
            ParentModuleId = moduleId;

            ctlProductDevelopment.Initialize(moduleId, viewRole, partStatus, ds, IsAdministrator, sku, true);

            //lnkEdit.NavigateUrl = EditUrl("mid", moduleId.ToString(), "PurchasingPartInformationEditor", "id", PartId.ToString());
            lnkEdit.NavigateUrl = "javascrit:alert('Aprovals & etc. Not Yet Implemented.');";
        }
        
    }
}
 