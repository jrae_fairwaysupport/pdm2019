<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BOMMasterEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.BOMMasterEditor" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }

           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");



    });

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });
    }




</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
      <asp:Panel ID="pnlBOM" runat="server" >
            <table width="800" cellpadding="4" cellspacing="0" border="0">
                <tr>
                    <td style="width:200px;">
                        <asp:Label ID="labelassembly" runat="server" CssClass="SubSubHead" Text="Assembly Cost:" />
                    </td>
                    <td style="width:200px;">
                        <asp:Label ID="labelAssemblyLocation" runat="server" CssClass="SubSubHead" Text="Assembly Location:" />
                    </td>
                    <td style="width:200px;">
                        
                    </td>
                    <td style="width:200px;"></td>
                </tr>

                <tr>
                    <td><asp:TextBox ID="txtAssemblyCost" runat="server" Width="180" CssClass="Box" MaxLength="10" style="text-align:right;" Table="vwBillOfMaterialsInfos" Field="AssemblyCost" Placeholder="Assembly Cost" onkeypress="return isDecimal(event);"/></td>
                    <td colspan="3"><asp:TextBox ID="txtAsemblyLocation" runat="server" Width="580" CssClass="Box" MaxLength="50" Table="vwBillOfMaterialsInfos" Field="Location" Placeholder="Assembly Location"/></td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="labelduty" runat="server" CssClass="SubSubHead" Text="Duty & Freight:<br />" />
                        <asp:TextBox ID="txtDutyAndFreight" runat="server" Width="180" CssClass="Box" MaxLength="10" style="text-align:right;" Table="vwBillOfMaterialsInfos" Field="DutyAndFreight" Placeholder="Duty & Freight" onkeypress="return isDecimal(event);" />
                    </td>
                    <td />
                    <td />
                    <td />
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="labellanded" runat="server" CssClass="SubSubHead" Text="Total Landed Cost:<br />" />
                        <asp:TextBox ID="txtTotalLandedCost" runat="server" Width="180" CssClass="Box" MaxLength="10" style="text-align:right;" Table="vwBillOfMaterialsInfos" Field="TotalLandedCost" Placeholder="Total Landed Cost" onkeypress="return isDecimal(event);" />
                    </td>
                    <td />
                    <td />
                    <td />
                </tr>

                <tr>
                    <td colspan="4">
                        <asp:Label ID="labelinstructions" runat="server" CssClass="SubSubHead" Text="Assembly Instructions:<br />" />
                        <asp:TextBox ID="txtInstructions" runat="server" Width="780" CssClass="Box" Height="150" TextMode="MultiLine" Table="vwBillOfMaterialsInfos" Field="AssemblyInstructions" Placeholder="Assembly Instructions" />
                    </td>
                </tr>

            </table>
        </asp:Panel>
        <asp:Label ID="lblSku" runat="server" Visible="false" Table="Parts" Field="PartId" />
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />

