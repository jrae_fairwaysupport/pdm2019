using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
    public partial class MarketingTabEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Marketing"; } }

        protected override void PopulateControls()
        {
            Controller controller = new Controller(entityId, environment);
            PopulateProductCategory(controller, ddlProductCategory1, 1);
            PopulateProductCategory(controller, ddlProductCategory2, 2);
            PopulateProductCategory(controller, ddlProductCategory3, 3);
            PopulateProductCategory(controller, ddlProductCategory4, 4);

        }

        protected override void LockDownControls()
        {
            
            EditorToolGroup main = new EditorToolGroup();
            txtCopy.Tools.Clear();
            txtCopy.Tools.Add(main);

            //main.Tools.Add(new EditorTool("PasteStrip"));
            main.Tools.Add(new EditorTool("InsertParagraph"));
            main.Tools.Add(new EditorTool("Undo"));
            main.Tools.Add(new EditorTool("Redo"));
            main.Tools.Add(new EditorSeparator());

            main.Tools.Add(new EditorTool("Bold"));
            main.Tools.Add(new EditorTool("Italic"));
            main.Tools.Add(new EditorTool("Underline"));
            main.Tools.Add(new EditorSeparator());

            main.Tools.Add(new EditorTool("Indent"));
            main.Tools.Add(new EditorTool("Outdent"));
            main.Tools.Add(new EditorSeparator());

            main.Tools.Add(new EditorTool("InsertUnorderedList"));
            main.Tools.Add(new EditorTool("InsertOrderedList"));
            main.Tools.Add(new EditorSeparator());

            main.Tools.Add(new EditorTool("ToggleScreenMode"));

        }

        private void PopulateProductCategory(Controller controller, DropDownList ddl, int index)
        {
            string sql = string.Format("SELECT * FROM LR_CATEGORY_{0} ORDER BY DESCRIPTION", index);
            DataTable dt = controller.vmfg.ExecuteReader(sql);

            DataRow dr = dt.NewRow();
            dr["ID"] = -1;
            dr["DESCRIPTION"] = "Product Category " + index.ToString();
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "DESCRIPTION";
            ddl.DataValueField = "ID";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        protected override void SetDefaultValues(DataSet ds)
        {
            chkToggleLabels.Checked = true;
        }
   
        protected override DataSet LoadData()
        {
            DataSet ds = base.LoadData();

            Controller controller = new Controller(entityId, environment);

            DataSet dx = controller.LoadPart(PartId, "Awards");
            DataTable awards = dx.Tables["Awards"];
            rptAwards.DataSource = awards;
            rptAwards.DataBind();


            foreach (DataRow dr in awards.Rows)
            {
                string recordId = controller.GetValue<string>(dr, "RecordId", "-1");
                string name = controller.GetValue<string>(dr, "Name", "");
                Award_Add(recordId, name, true);
            }

            DataTable partInfos = ds.Tables["vwPartInfos"];
            if (partInfos.Rows.Count > 0)
            {
                DataRow dr = partInfos.Rows[0];
                SetRadio(rdoPatent, controller, dr, "Patent", "PatentPending");
                SetRadio(rdoTrademark, controller, dr, "Trademark", "RegisterTrademark");
            }

            return ds;
        }

        private void SetRadio(RadioButtonList rdo, Controller controller, DataRow dr, params string[] fields)
        {
            string fieldName = rdo.Attributes["Field"];

            foreach (string field in fields)
            {
                if (controller.GetValue<bool>(dr, field, false))
                {
                    rdo.SelectedValue = field;
                    rdo.Attributes.Remove("Original");
                    rdo.Attributes.Add("Original", field);
                }
            }
        }

        private void GetRadio(RadioButtonList rdo, TableFields tbl, params string[] fields)
        {
            string fieldName = rdo.Attributes["Field"];
            string original = rdo.Attributes["Original"];
            tbl.RemoveField(fieldName);

            //this is a radio...so if there is no selection, there should never have been one
            if (string.IsNullOrEmpty(rdo.SelectedValue)) return;
            if (string.IsNullOrEmpty(original)) original = string.Empty;

            string newValue = rdo.SelectedValue;
            foreach (string field in fields)
            {
                tbl.AddField(field, original.Equals(field), newValue.Equals(field));                    
            }

        }

        protected override bool Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);
            controller.ValidateText(txtInspectionNotes, true, "Product Description/Title", ref msg);


            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {
                CloseForm();
                return;
            }


            if (!Validate()) return;

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwMarketingInfos");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            SaveAwards(controller);
            SaveTableEdits(tables, "vwMarketingInfos", "Marketing", newRecord, controller);
            SaveTableEdits(tables, "Parts", "Header", false, controller, "Id");
            SaveTableEdits(tables, "vwPartInfos", "PartInfos", NewRecord(ds, "vwPartInfos"), controller);

            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields>tables, Controller controller)
        {
            if (!tables.ContainsKey("vwPartInfos")) return;

            TableFields table = tables["vwPartInfos"];
            if (!table.IsDirty()) return;

            //custom
            GetRadio(rdoPatent, table, "Patent", "PatentPending");
            GetRadio(rdoTrademark, table, "Trademark", "RegisterTrademark");


        }

        private void SaveAwards(Controller controller)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(hdnAwards.Value);

            XmlNode root = doc.ChildNodes[0];
            foreach (XmlNode award in root.ChildNodes)
            {
                string recordId = award.Attributes["recordId"].Value;
                string name = award.Attributes["name"].Value;
                string deleted = award.Attributes["deleted"].Value;

                int test = 0;
                if (Int32.TryParse(recordId, out test))
                {
                    //existing record
                    if (deleted == "true")
                    {
                        controller.history.Log(PartId, this.UserInfo.Username, "Marketing", "Awards", string.Empty, "Deleted Award: " + name);
                        string sql = "DELETE FROM AWARDS WHERE PartId = @P0 AND RecordId = @P1";
                        controller.pdm.ExecuteCommand(sql, PartId, test);
                    }

                }
                else
                {
                    //newbies with guid's
                    if (deleted == "false")
                    {
                        controller.history.Log(PartId, this.UserInfo.Username, "Marketing", "Awards", string.Empty, "Added Award: " + name);
                        controller.pdm.InsertRecord("Awards", "RecordId", "PartId", PartId, "Name", name);
                    }
                }
            }
        }



        protected void Award_Delete(object obj, CommandEventArgs args)
        {
            string recordId = args.CommandArgument.ToString();
            int test = -1;
            Int32.TryParse(recordId, out test);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(hdnAwards.Value);

            XmlNode tgt = doc.SelectSingleNode(string.Format("/root/award[@recordId='{0}']", recordId));
            if (test == 0)
            {
                doc.ChildNodes[0].RemoveChild(tgt);
            }
            else
            {
                tgt.Attributes["deleted"].Value = "true";
            }
            hdnAwards.Value = doc.InnerXml;
            Award_Refresh(doc);
        }

        protected void Award_Add(object obj, CommandEventArgs args)
        {
            string newId = Guid.NewGuid().ToString();
            string newbie = hdnNewAward.Value;

            Award_Add(newId, newbie);
        }

        private void Award_Add(string recordId, string name, bool skipRefresh = false)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(hdnAwards.Value);
            XmlNode root = doc.ChildNodes[0];

            XmlElement newbie = doc.CreateElement("award");
            newbie.AddAttribute("recordId", recordId);
            newbie.AddAttribute("name", name);
            newbie.AddAttribute("deleted", "false");
            root.AppendChild(newbie);
            hdnAwards.Value = doc.InnerXml;

            if (!skipRefresh) Award_Refresh(doc);
        }

        private void Award_Refresh(XmlDocument doc = null)
        {
            if (doc == null)
            {
                doc = new XmlDocument();
                doc.LoadXml(hdnAwards.Value);
            }
            XmlNode root = doc.ChildNodes[0];

            DataTable dt = new DataTable();
            dt.Columns.Add("RecordId", System.Type.GetType("System.String"));
            dt.Columns.Add("Name", System.Type.GetType("System.String"));

            foreach (XmlNode award in root.ChildNodes)
            {
                if (award.Attributes["deleted"].Value == "false")
                {
                    DataRow dr = dt.NewRow();
                    dr["RecordId"] = award.Attributes["recordId"].Value;
                    dr["Name"] = award.Attributes["name"].Value;
                    dt.Rows.Add(dr);
                }
            }

            rptAwards.DataSource = dt;
            rptAwards.DataBind();
        }
   
    }
}
 