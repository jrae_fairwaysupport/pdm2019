using System;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Exceptions;

using YourCompany.PDM2019.Components;
using System.Web.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class Settings : ModuleSettingsBase
    {

        /// <summary>
        /// handles the loading of the module setting for this
        /// control
        /// </summary>
        public override void LoadSettings()
        {
            try
            {
                if (!IsPostBack)
                {
                    string url = Request.QueryString["ReturnURL"];
                    rdoEntity.SelectedValue = url.Contains("eid/H2M") ? "H2M" : "LRH";
                    rdoEntity.Enabled = false;

                    LoadData();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        private void SetChecks(CheckBoxList lst, string roles)
        {
            foreach (ListItem item in lst.Items)
            {
                item.Selected = roles.Contains(item.Value);
            }
        }

        private string GetChecks(CheckBoxList lst)
        {
            string retval = string.Empty;
            foreach (ListItem item in lst.Items)
            {
                if (item.Selected)
                {
                    if (!string.IsNullOrEmpty(retval)) retval += ", ";
                    retval += item.Value;
                }
            }
            return retval;
        }

        protected void ToggleEnvironment(object obj, EventArgs args)
        {
            LoadData();
        }

        private void LoadData()
        {
            PDM2019Settings settingsData = new PDM2019Settings(this.TabModuleId);
            string preKey = rdoEntity.SelectedValue + rdoEnvironment.SelectedValue;

            txtPortalId.Text = settingsData.ReadSetting<string>(preKey + "PortalId", this.PortalId.ToString());
            txtPartUrlTemplate.Text = settingsData.ReadSetting<string>(preKey + "PartUrlTemplate", "");
            SetChecks(chkSubmit, settingsData.ReadSetting<string>(preKey + "SubmitEmails", ""));
            SetChecks(chkReject, settingsData.ReadSetting<string>(preKey + "RejectEmails", ""));
            SetChecks(chkApprove, settingsData.ReadSetting<string>(preKey + "ApproveEmails", ""));

        }


        /// <summary>
        /// handles updating the module settings for this control
        /// </summary>
        public override void UpdateSettings()
        {
            try
            {
                string preKey = rdoEntity.SelectedValue + rdoEnvironment.SelectedValue;

                PDM2019Settings settingsData = new PDM2019Settings(this.TabModuleId);
                settingsData.WriteSetting(preKey + "PortalId", txtPortalId.Text.Trim());
                settingsData.WriteSetting(preKey + "PartUrlTemplate", txtPartUrlTemplate.Text.Trim());
                settingsData.WriteSetting(preKey + "SubmitEmails", GetChecks(chkSubmit));
                settingsData.WriteSetting(preKey + "RejectEmails", GetChecks(chkReject));
                settingsData.WriteSetting(preKey + "ApproveEmails", GetChecks(chkApprove));
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }
    }
}