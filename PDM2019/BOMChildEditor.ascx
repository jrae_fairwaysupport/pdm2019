<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BOMChildEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.BOMChildEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:25px; top:75px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {


        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //


        
        
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="400" Height="200" ScrollBars="Auto">
    <table width="400" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:200px;"><asp:Label ID="labelChildPartId" runat="server" CssClass="SubSubHead" Text="Child Part:" /></td>
            <td style="width:200px;" />
        </tr>
        <tr>
            <td colspan="2" align="left">
                    <DNN:DNNTextSuggest ID="txtChildPartId" runat="server" 
                            onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                            CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggestPartId" 
                            Table="vwChildParts" Field="ChildPartSuggest" NameIDSplit="-" Width="380" Placeholder="Child Part Id"
                            queryTmpl8 = "SELECT PartId AS ID, Description AS NAME FROM Parts WHERE ((PartId LIKE '{0}%') OR (Description LIKE '{0}%')) ORDER BY PartId"
                            filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                            validation = "SELECT COUNT(*) AS HITS FROM Parts WHERE PartId = @P0 OR Description = @P1"
                    />
                    
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="labelQuantity" runat="server" CssClass="SubSubHead" Text="Qty/Per:<br />" />
                <asp:TextBox ID="txtQuantity" runat="server" CssClass="Box" Width="180" MaxLength="10" Placeholder="Qty/Per" Table="vwChildParts" Field="Quantity" onkeypress="return isDecimal(event);" Format="Decimal" Places="3" />
                        
            </td>
        </tr>
    </table>
</asp:Panel>

<table width="400" cellpadding="4">
    <tr>
        <td align="left" style="width:50%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="right" style="width:50%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnChildPartId" runat="server" Value="-1" />

