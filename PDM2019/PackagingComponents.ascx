<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PackagingComponents.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PackagingComponents" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        
    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

        
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

</script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:100px;">
                <asp:Label ID="labelQuantity" runat="server" CssClass="SubSubHead" Text="Quantity:<br />" />
                <asp:TextBox ID="txtQuantity" runat="server" CssClass="Box" Width="80" PlaceHolder="Quantity" Format="Number" onkeypress="return isNumberKey(event)" Required="true" Table="vwPartPackagingComponents" Field="Quantity" />
            </td>
            <td style="width:700px;">
                <asp:Label ID="labelSize" runat="server" CssClass="SubSubHead" Text="Size:<br />" />
                <asp:TextBox ID="txtSize" runat="server" CssClass="Box" Width="480" Placeholder="Size" MaxLength="50" Required="true" Table="vwPartPackagingComponents" Field="Size" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="labelDescription" runat="server" CssClass="SubSubHead" Text="Description:<br />" />
                <asp:TextBox ID="txtDescription" runat="server" CssClass="Box" Width="680" Placeholder="Description" MaxLength="80" Required="true" Table="vwPartPackagingComponents" Field="Description" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="labelMaterials" runat="server" CssClass="SubSubHead" Text="Materials:<br />" />
                <asp:TextBox ID="txtMaterials" runat="server" CssClass="Box" Width="680" Placeholder="Material" MaxLength="500" Height="150" Required="true" TextMode="MultiLine" Table="vwPartPackagingComponents" Field="Materials"  />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="label12" runat="server" CssClass="SubSubHead" Text="Printing:<br />" />
                <asp:TextBox ID="txtPrinting" runat="server" CssClass="Box" Width="680" Placeholder="Printing" MaxLength="500" Height="150" Required="true" TextMode="MultiLine" Table="vwPartPackagingComponents" Field="Printing"  />
            </td>
        </tr>
    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />

