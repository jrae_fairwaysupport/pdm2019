using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;
using System.Text;

namespace YourCompany.Modules.PDM2019
{
    public partial class PurchasingTabPartInformationEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Purchasing Editor"; } }

        protected override void  PopulateControls()
        {
            Controller controller = new Controller(entityId, environment);
            
            PopulateDropDownList(controller.vmfg, ddlHTSCode, "HTS Code", string.Empty, "SELECT HTS_CODE AS DISPLAY_VALUE, HTS_CODE || ' - ' || DESCRIPTION AS DISPLAY_TEXT FROM INFO_HTS "
                                                                                            + "WHERE HTS_CODE IS NOT NULL ORDER BY HTS_CODE");

            PopulateDropDownList(controller.vmfg, ddlPrimaryWarehouse, "Primary Warehouse", string.Empty, "SELECT ID AS DISPLAY_VALUE, ID || ' - ' || DESCRIPTION AS DISPLAY_TEXT FROM WAREHOUSE ORDER BY DESCRIPTION");
            PopulateDropDownList(controller.vmfg, ddlForeignWarehouse, "Foreign Warehouse", string.Empty, "SELECT ID AS DISPLAY_VALUE, ID || ' - ' || DESCRIPTION AS DISPLAY_TEXT FROM WAREHOUSE ORDER BY DESCRIPTION");

            PopulateDropDownList(controller.vmfg, ddlProductStatus, "Product Status", string.Empty, "SELECT DISTINCT PRODUCT_STATUS AS DISPLAY_VALUE, PRODUCT_STATUS AS DISPLAY_TEXT FROM PART "
                                                                                                      + "WHERE PRODUCT_STATUS IS NOT NULL ORDER BY PRODUCT_STATUS");

            controller.PopulateDropDownListOptions(ddlCountryOfOrigin, "Purchasing", "COO", "Select Country");
            controller.PopulateDropDownListOptions(ddlPortOfOrigin, "Purchasing", "POO", "Select Port");

            //PopulateDropDownList(controller.vmfg, ddlCountryOfOrigin, "Country of Origin", string.Empty, "SELECT UPPER(DESCRIPTION) AS DISPLAY_TEXT, UPPER(DESCRIPTION) AS DISPLAY_VALUE FROM COUNTRY ORDER BY DESCRIPTION");
            //PopulateDropDownList(controller.vmfg, ddlPortOfOrigin, "Port of Origin", string.Empty, "SELECT UPPER(DESCRIPTION) AS DISPLAY_TEXT, UPPER(DESCRIPTION) AS DISPLAY_VALUE FROM COUNTRY ORDER BY DESCRIPTION");

            PopulateDropDownList(controller.dcms, ddlClassCode, "Class Code", string.Empty, "select DISTINCT NMFC_CODE_ID AS DISPLAY_VALUE, NMFC_CODE_ID AS DISPLAY_TEXT FROM SKU WHERE NMFC_CODE_ID IS NOT NULL ORDER BY NMFC_CODE_ID");

            PopulateDropDownList(controller.vmfg, ddlCommodityCode, "Commodity Code", string.Empty, "select DISTINCT COMMODITY_CODE AS DISPLAY_VALUE, COMMODITY_CODE AS DISPLAY_TEXT FROM PART WHERE COMMODITY_CODE IS NOT NULL ORDER BY COMMODITY_CODE");

            controller.PopulateDropDownListOptions(ddlMaterialCode, "Purchasing", "Materials Short List", "Material Code");
            controller.PopulateDropDownListOptions(ddlMoldMaterial, "Purchasing", "Mold Material", "Mold Material");
            controller.PopulateDropDownListOptions(ddlProductMaterial, "Purchasing", "Mold Product Material", "Product Material");
            controller.PopulateDropDownListOptions(ddlMoldType, "Purchasing", "Mold Type", "Mold Type");
        }

        private void PopulateDropDownList(SvcLibrary.DBHandler db, DropDownList ddl, string defaultText, string defaultValue, string sql)
        {
            DataTable dt = db.ExecuteReader(sql);

            DataRow dr = dt.NewRow();
            dr["DISPLAY_VALUE"] = defaultValue;
            dr["DISPLAY_TEXT"] = defaultText;
            dt.Rows.InsertAt(dr, 0);

            ddl.DataTextField = "DISPLAY_TEXT";
            ddl.DataValueField = "DISPLAY_VALUE";
            ddl.DataSource = dt;
            ddl.DataBind();
        }

        protected override DataSet LoadData()
        {
            DataSet ds = base.LoadData();

            Controller controller = new Controller(entityId, environment);
            DataSet tmp = controller.LoadPart(PartId, "vwPartContainers", "vwPurchasingMolds");

            foreach (DataTable dt in tmp.Tables)
            {
                DataTable newbie = dt.Copy();
                ds.Tables.Add(newbie);
            }


            DataTable containers = ds.Tables["vwPartContainers"];
            ContainerSize_Load(containers);

            DataTable molds = ds.Tables["vwPurchasingMolds"];
            Mold_Load(molds);

            return ds;
        }

        protected override void SetDefaultValues(DataSet ds)
        {
            bool newRecord = NewRecord(ds, "vwPurchasingPartInfos");

            if (newRecord)
            {
                ddlPrimaryWarehouse.SelectedValue = "P";
            }

            chkToggleLabels.Checked = !newRecord;
        }

        protected override bool Validate()
        {
            string msg = string.Empty;

            //nothing gets validated until approving
            //Purchasing controller = new Purchasing(entityId, environment);
            //controller.ValidateText(txtBuyer, approving, "Buyer User Id", ref msg);
            //controller.ValidateNumber(txtSafetyStock, approving, "Safety Stock", ref msg);
            //controller.ValidateText(ddlHTSCode, approving, "HTS Code", ref msg);
            //controller.ValidateText(ddlCountryOfOrigin, approving, "Country of Origin/Manufacture", ref msg);
            //controller.ValidateText(ddlPortOfOrigin, approving, "Port of Origin", ref msg);
            Controller controller = new Controller(entityId, environment);
            if (chkMagnets.Checked)
            {
                if (!fuMagnets.HasFile)
                {
                    if (string.IsNullOrEmpty(lnkMagnets.NavigateUrl))
                    {
                        msg += "When the Anti-Dumping option is checked, the legal document must be uploaded.\r\n";
                    }
                }
            }

            if (chkObsolete.Checked)
            {
                if (!partStatus.Equals("Approved"))
                {
                    msg += "Parts that haven't yet been Approved cannot be flagged as Obsolete.\r\n";
                }
                else
                {

                    string sql = "SELECT COUNT (*) AS THECOUNT FROM CUSTOMER_ORDER O INNER JOIN CUST_ORDER_LINE OL ON O.ID = OL.CUST_ORDER_ID AND OL.PART_ID = @P0 "
                                + "WHERE COALESCE(O.STATUS, 'EMPTY') NOT IN ('X', 'C') AND COALESCE(OL.LINE_STATUS, 'EMPTY') NOT IN ('X', 'C')";
                    int orders = controller.vmfg.FetchSingleValue<int>(sql, "THECOUNT", sku);
                    if (orders > 0)
                    {
                        msg += string.Format("There are currently {0} open orders for this part. Parts with open orders cannot be flagged as Obsolete.\r\n", orders);
                    }
                }
            }

            controller.DNNTestSuggestIsSelectionValid(txtPreferredVendor, "Preferred Vendor", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {

                CloseForm();
                return;
            }


            if (!Validate()) return;

            SaveMolds();
            SaveContainers();
            
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwPurchasingPartInfos");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwPurchasingPartInfos", "Purchasing.Part Information", newRecord, controller);
            SaveTableEdits(tables, "vwPartInfos", "Product Development.Part Information", NewRecord(ds, "vwPartInfos"), controller);
            SaveTableEdits(tables, "vwPartSpecifications", "Product Development.Specifications", NewRecord(ds, "vwPartSpecifications"), controller);
            SaveMagnets(tables, controller, ds.Tables["Magnets"]);

            SaveOracleData(controller, tables);

            CloseForm();

        }

        private void SaveOracleData(Controller controller, Dictionary<string, TableFields> tables)
        {
            if (!partStatus.Equals("Approved")) return;

            Dictionary<string, object> edits = new Dictionary<string, object>();

            //vmfg.part
            tables["vwPurchasingPartInfos"].StageOracleEdits(edits, "SafetyStock.SAFETY_STOCK_QTY", "PreferredVendorId.PREF_VENDOR_ID", "HtsCode.HTS_CODE", "PrimaryWarehouse.PRIMARY_WHS_ID",
                                                                "PortOfOrigin.DEF_ORIG_COUNTRY", "ProductStatus.PRODUCT_STATUS", "BuyerUserId.BUYER_USER_ID",
                                                                "PlannerUserId.PLANNER_USER_ID", "Obsolete.STATUS"); //"ForeignWarehouse.FOREIGN_WHS_ID", 
            if (edits.Count > 0)
            {
                if (edits.ContainsKey("STATUS"))
                {
                    //different type
                    if (chkObsolete.Checked)
                        edits["STATUS"] = "O";
                    else
                        edits["STATUS"] = DBNull.Value;
                }
                tables["vwPurchasingPartInfos"].PushOracleEdits(controller.vmfg, edits, "PART", "ID", sku);
            }

            //vmfg.lr_part_mfg
            edits.Clear();
            tables["vwPurchasingPartInfos"].StageOracleEdits(edits, "CountryOfOrigin.COUNTRY_MFG");
            if (edits.Count > 0) tables["vwPurchasingPartInfos"].PushOracleEdits(controller.vmfg, edits, "LR_PART_MFG", "PART_ID", sku);

            //dcms.sku
            edits.Clear();
            tables["vwPurchasingPartInfos"].StageOracleEdits(edits, "HoldReceiptsCheck.HOLD_RECEIPTS");
            if (edits.Count > 0)
            {
                edits["HOLD_RECEIPTS"] = (bool)edits["HOLD_RECEIPTS"] ? "Y" : "N";
                tables["vwPurchasingPartInfos"].PushOracleEdits(controller.dcms, edits, "SKU", "ID", sku);
            }


            //primary warehouse
            if (tables["vwPurchasingPartInfos"].HasFieldChanged("PrimaryWarehouse"))
                controller.PrimaryWarehouse_Save(sku, ddlPrimaryWarehouse.SelectedValue);

            //foreign
            if (tables["vwPurchasingPartInfos"].HasFieldChanged("ForeignWarehouse"))
                controller.ForeignWarehouse_Save(sku, ddlForeignWarehouse.SelectedValue);


            if (tables["vwPartSpecifications"].HasFieldChanged("GeneralDescription"))
                controller.Specifications_Save(PartId, sku, txtGeneralDescription.Text.Trim());
        }



        private void SaveMagnets(Dictionary<string, TableFields> tables, Controller controller, DataTable dt)
        {
            //History on original checkbox, and when File uploaded
            if (!tables.ContainsKey("Magnets")) return;
            TableFields table = tables["Magnets"];

            //only concerned with updating if the tic changes or if the uploader contains a file, so
            if (!(table.IsDirty() || fuMagnets.HasFile)) return;

            string originalFile = string.Empty;
            string fileName = string.Empty;
            string filePath = string.Empty;
            string fileUrl = string.Empty;
            string rawFileName = string.Empty;

            if (dt.Rows.Count > 0)
            {
                originalFile = controller.pdm.GetValue<string>(dt.Rows[0], "OriginalFileName", string.Empty);
            }

            if (fuMagnets.HasFile)
            {
                rawFileName = fuMagnets.FileName;

                //get extension
                string extension = string.Empty;
                string[] arr = fuMagnets.FileName.Split('.');
                extension = arr[arr.Length - 1];

                SvcLibrary.Configuration config = new SvcLibrary.Configuration();
                string root = config.GetValue<string>("PDM Documents", "c:\\junk");

                fileName = string.Format("{0}_{1}_{2}.{3}", sku, this.UserInfo.UserID, Guid.NewGuid().ToString(), extension);
                filePath = System.IO.Path.Combine(root, entityId, environment, "Magnets", fileName);
                fileUrl = string.Format("~/PDMDocs/{0}/{1}/Magnets/{2}", entityId, environment, fileName);

                SvcLibrary.Impersonation impersonate = new SvcLibrary.Impersonation();
                System.Security.Principal.WindowsImpersonationContext context = impersonate.ImpersonateUser("lrservice", "LR", "Fa!rway380!");
                fuMagnets.SaveAs(filePath);
                context.Dispose();
            }

            table.AddField("Anti-dumping Legal Document", originalFile, rawFileName);
            controller.history.Log(PartId, this.UserInfo.Username, "Purchasing.Part Information", table.Fields); //, "PartId", "FileName", "FilePath", "FileUrl");
            table.RemoveField("Anti-dumping Legal Document");

            table.AddField("PartId", PartId, PartId);
            table.AddField("FileName", string.Empty, fileName);
            table.AddField("FilePath", string.Empty, filePath);
            table.AddField("FileUrl", string.Empty, fileUrl);
            table.AddField("OrignalFileName", originalFile, rawFileName);


            if (dt.Rows.Count > 0)
            {
                controller.pdm.UpdateRecord("Magnets", "PartId", PartId, "ContainsMagnets", table.Fields["ContainsMagnets"].newValue, "OriginalFileName", rawFileName, "FileName", fileName, "FilePath", filePath, "FileUrl", fileUrl);
            }
            else
            {
                controller.pdm.InsertRecord("Magnets", "PartId", "PartId", PartId, "ContainsMagnets", table.Fields["ContainsMagnets"].newValue, "OriginalFileName", rawFileName, "FileName", fileName, "FilePath", filePath, "FileUrl", fileUrl);
            }

        }


        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {

            if (tables.ContainsKey("vwPartInfos"))
            {
                TableFields table = tables["vwPartInfos"];
                if (table.Fields["PreferredVendor"].HasChanged())
                {
                    string sql = "SELECT BUYER FROM vwVendors WHERE ID = @P0";
                    table.Fields["BuyerUserId"].newValue = controller.pdm.FetchSingleValue<string>(sql, "BUYER", (string)table.Fields["PreferredVendor"].newValue);

                    if (tables.ContainsKey("vwPurchasingPartInfos"))
                    {
                        tables["vwPurchasingPartInfos"].AddField("PreferredVendorId", table.GetOldValue("PreferredVendor"), table.GetNewValue("PreferredVendor"));
                        tables["vwPurchasingPartInfos"].AddField("BuyerUserId", table.GetOldValue("BuyerUserId"), table.GetNewValue("BuyerUserId"));
                    }
                }
            }


            if (tables.ContainsKey("vwPartSpecifications"))
            {
                if (tables["vwPartSpecifications"].HasFieldChanged("GeneralDescription"))
                {
                    if (tables.ContainsKey("vwPurchasingPartInfos"))
                    {
                        tables["vwPurchasingPartInfos"].AddField("Specification", tables["vwPartSpecifications"].GetOldValue("GeneralDescription"), tables["vwPartSpecifications"].GetNewValue("GeneralDescription"));
                    }
                }
            }

        }

        private void SaveMolds()
        {
            DataTable molds = Mold_Data();
            if (molds.Rows.Count == 0) return;

            Controller controller = new Controller(entityId, environment);

            foreach (DataRow dr in molds.Rows)
            {
                StringBuilder msg = new StringBuilder();

                bool deleted = controller.pdm.GetValue<bool>(dr, "deleted", false);
                bool updated = controller.pdm.GetValue<bool>(dr, "updated", false);
                if (deleted) msg.Append("Deleted Mold:<br />");

                string raw = controller.pdm.GetValue<string>(dr, "RecordId", string.Empty);
                int recordId = -1;
                Int32.TryParse(raw, out recordId);

                int numberOfMolds = controller.pdm.GetValue<int>(dr, "NumberOfMolds", 0);
                msg.AppendFormat("Cavities: {0}<br />", numberOfMolds);

                DateTime test = controller.pdm.GetValue<DateTime>(dr, "DateMovedText", DateTime.MinValue);
                DateTime? dateMoved = null;
                if (test != DateTime.MinValue) dateMoved = test;
                msg.AppendFormat("Date Moved: {0}<br />", (dateMoved != null) ? dateMoved.Value.ToString("MM/dd/yyyy") : "");

                string vendorSuggest = controller.pdm.GetValue<string>(dr, "VendorSuggest", string.Empty);
                string vendorId = string.Empty;
                string vendorName = string.Empty;
                controller.DNNTextSuggestValues(vendorSuggest, out vendorId, out vendorName, '-');
                msg.AppendFormat("Vendor: {0}<br />", vendorSuggest);

                string trackingId = controller.pdm.GetValue<string>(dr, "TrackingId", string.Empty);
                msg.AppendFormat("Mold Id: {0}<br />", trackingId);

                decimal cost = controller.pdm.GetValue<decimal>(dr, "Cost", 0);
                decimal? moldCost = null;
                if (cost > 0) moldCost = cost;
                string costText = moldCost.HasValue ? cost.ToString("$ #.00") : "";
                msg.AppendFormat("Cost: {0}<br />", costText);

                bool active = controller.pdm.GetValue<bool>(dr, "Active", false);
                msg.AppendFormat("Active: {0}<br />", active.ToString());

                string agreementUrl = controller.pdm.GetValue<string>(dr, "AgreementUrl", string.Empty);
                string agreementFile = controller.pdm.GetValue<string>(dr, "AgreementFile", string.Empty);
                string agreementPath = controller.pdm.GetValue<string>(dr, "AgreementPath", string.Empty);
                msg.AppendFormat("Agreement: {0}<br />", agreementFile);

                bool slides = controller.pdm.GetValue<bool>(dr, "Slides", false);
                msg.AppendFormat("Slides: {0}<br />", slides.ToString());

                string moldMaterial = controller.pdm.GetValue<string>(dr, "MoldMaterial", string.Empty);
                msg.AppendFormat("Mold Material: {0}<br />", moldMaterial);

                string productMaterial = controller.pdm.GetValue<string>(dr, "ProductMaterial", string.Empty);
                msg.AppendFormat("Product Material: {0}<br />", productMaterial);

                string moldType = controller.pdm.GetValue<string>(dr, "MoldType", string.Empty);
                msg.AppendFormat("Mold Type: {0}<br />", moldType);

                string snapshotUrl = controller.pdm.GetValue<string>(dr, "SnapshotUrl", string.Empty);
                string snapshotFile = controller.pdm.GetValue<string>(dr, "SnapshotFile", string.Empty);
                string snapshotPath = controller.pdm.GetValue<string>(dr, "SnapshotPath", string.Empty);
                msg.AppendFormat("Snapshot: {0}", snapshotFile);

                if (deleted)
                {
                    if (recordId > 0)
                    {
                        controller.history.Log(PartId, this.UserInfo.Username, "Purchasing.Part Information", "Molds", string.Empty, msg.ToString());
                        string sql = "DELETE FROM PurchasingMolds WHERE ID = @P0";
                        controller.pdm.ExecuteCommand(sql, recordId);
                    }
                }
                else
                {
                    if (recordId > 0)
                    {
                        if (updated)
                        {
                            controller.history.Log(PartId, this.UserInfo.Username, "Purchasing.Part Information", "Molds", string.Empty, "Updated Mold:<br />" + msg.ToString());
                            controller.pdm.UpdateRecord("PurchasingMolds", "ID", recordId, "NumberOfMolds", numberOfMolds, "DateMoved", dateMoved, "Vendor", vendorId, "TrackingId", trackingId,
                                                "ModifiedDate", DateTime.Now, "Cost", moldCost, "Active", active, "AgreementFile", agreementFile, "AgreementPath", agreementPath, "AgreementUrl", agreementUrl,
                                                "MoldMaterial", moldMaterial, "ProductMaterial", productMaterial, "MoldType", moldType, "Slides", slides, "SnapshotFile", snapshotFile, "SnapshotPath", snapshotPath, "SnapshotUrl", snapshotUrl);
                        }
                    }
                    else
                    {
                        controller.history.Log(PartId, this.UserInfo.Username, "Purchasing.Part Information", "Molds", string.Empty, "Created Mold:<br />" + msg.ToString());
                        controller.pdm.InsertRecord("PurchasingMolds", "ID", "PurchasingPartInfo_PartId", PartId, "NumberOfMolds", numberOfMolds, "DateMoved", dateMoved, "Vendor", vendorId, "TrackingId", trackingId,
                                            "ModifiedDate", DateTime.Now, "CreatedDate", DateTime.Now, "Cost", moldCost, "Active", active, "AgreementFile", agreementFile, "AgreementPath", agreementPath, "AgreementUrl", agreementUrl,
                                                "MoldMaterial", moldMaterial, "ProductMaterial", productMaterial, "MoldType", moldType, "Slides", slides, "SnapshotFile", snapshotFile, "SnapshotPath", snapshotPath, "SnapshotUrl", snapshotUrl);

                    }
                }
            }
        }

        private void SaveContainers()
        {
            DataTable containers = ContainerSize_Data();
            if (containers.Rows.Count == 0) return;

            Controller controller = new Controller(entityId, environment);

            foreach (DataRow dr in containers.Rows)
            {
                StringBuilder msg = new StringBuilder();

                bool deleted = controller.pdm.GetValue<bool>(dr, "deleted", false);
                if (deleted) msg.Append("Deleted Mold:<br />");

                string raw = controller.pdm.GetValue<string>(dr, "RecordId", string.Empty);
                int recordId = -1;
                Int32.TryParse(raw, out recordId);

                string containerSize = controller.pdm.GetValue<string>(dr, "ContainerSize", string.Empty);
                msg.AppendFormat("Container Size: {0}", containerSize);

                //only deletes and adds
                if (deleted)
                {
                    if (recordId > 0)
                    {
                        controller.history.Log(PartId, this.UserInfo.Username, "Purchasing.Part Information", "Container Size", string.Empty, msg.ToString());
                        string sql = "DELETE FROM PartContainers WHERE RecordId = @P0";
                        controller.pdm.ExecuteCommand(sql, recordId);
                    }
                }
                else
                {

                    if (recordId == 0)
                    {
                        controller.history.Log(PartId, this.UserInfo.Username, "Purchasing.Part Information", "Container Size", string.Empty, "Added Container Size:<br />" + msg.ToString());
                        controller.pdm.InsertRecord("PartContainers", "RecordId", "PartId", PartId, "ContainerSize", containerSize);
                    }
                }
            }
        }


        private void ContainerSize_Load(DataTable dt)
        {
            if (!dt.Columns.Contains("deleted"))
            {
                dt.Columns.Add("deleted", System.Type.GetType("System.Boolean"));
                dt.Columns.Add("show", System.Type.GetType("System.Boolean"));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["deleted"] = false;
                    dr["show"] = true;
                }
            }

            rptContainer.DataSource = dt;
            rptContainer.DataBind();
            ContainerSize_PopulateOptions(rptContainer);
        }

        private DataTable ContainerSize_Data()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RecordId", System.Type.GetType("System.String"));
            dt.Columns.Add("ContainerSize", System.Type.GetType("System.String"));
            dt.Columns.Add("deleted", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("show", System.Type.GetType("System.Boolean"));

            ContainerSize_Data(rptContainer, dt);
            return dt;
        }

        private void ContainerSize_Data(object parent, DataTable dt)
        {
            foreach(Control child in ((Control)parent).Controls)
            {
                if (child.GetType().Name.Equals("ImageButton"))
                {
                    ImageButton btn = (ImageButton)child;
                    if (btn.ID == "btnDeleteContainerSize")
                    {
                        DataRow dr = dt.NewRow();
                        dr["RecordId"] = btn.Attributes["RecordId"];
                        dr["ContainerSize"] = btn.Attributes["ContainerSize"];
                        bool deleted = Boolean.Parse(btn.Attributes["deleted"]);
                        dr["deleted"] = deleted;
                        dr["show"] = !deleted;
                        dt.Rows.Add(dr);
                    }
                }
                ContainerSize_Data(child, dt);
            }
        }

        protected void ContainerSize_Delete(object obj, CommandEventArgs args)
        {
            string recordId = args.CommandArgument.ToString();
            DataTable dt = ContainerSize_Data();

            DataRow[] arr = dt.Select(string.Format("RecordId = '{0}'", recordId));
            if (arr.Length > 0)
            {
                arr[0]["deleted"] = true;
                arr[0]["show"] = false;
            }

            ContainerSize_Load(dt);
        }

        protected void ContainerSize_Add(object obj, CommandEventArgs args)
        {
            string newId = Guid.NewGuid().ToString();
            string newbie = hdnNewContainerSize.Value;

            DataTable dt = ContainerSize_Data();
            DataRow dr = dt.NewRow();
            dr["RecordId"] = newId;
            dr["ContainerSize"] = newbie;
            dr["deleted"] = false;
            dr["show"] = true;
            dt.Rows.Add(dr);

            hdnNewContainerSize.Value = string.Empty;
            ContainerSize_Load(dt);
        }

        private void ContainerSize_PopulateOptions(object parent)
        {
            foreach (Control child in ((Control)parent).Controls)
            {
                if (child.GetType().Name.Equals("DropDownList"))
                {
                    DropDownList ddl = (DropDownList)child;
                    Controller controller = new Controller(entityId, environment);
                    controller.PopulateDropDownListOptions(ddl, "Purchasing", "Container Size");
                    return;
                }
                ContainerSize_PopulateOptions(child);
            }
        }

        protected void Mold_Delete(object obj, CommandEventArgs args)
        {
            ImageButton btn = (ImageButton)obj;

            DataTable dt = Mold_Data();
            string recordId = args.CommandArgument.ToString();
            DataRow[] arr = dt.Select(string.Format("RecordId = '{0}'", recordId));
            if (arr.Length > 0)
            {
                arr[0]["deleted"] = true;
                arr[0]["style"] = false;
            }
            Mold_Load(dt);
        }

        protected void Mold_Save(object obj, CommandEventArgs args)
        {
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;
            string action = args.CommandName;
            bool updated = args.CommandName.Equals("Update");

            string recordId = args.CommandArgument.ToString();
            int? numberOfMolds = controller.ValidateNumber(txtNumberOfMolds, true, "Number of Molds", ref msg);
            DateTime? date = controller.ValidateDate(dtDateMoved, true, "Date Moved", ref msg);
            string vendorSuggest = controller.ValidateText(txtMoldVendor, true, "Vendor", ref msg);
            string trackingId = txtTrackingId.Text.Trim();
            decimal? cost = controller.ValidateDecimal(txtMoldCost, false, "Mold Cost", ref msg);

            string moldMaterial = ddlMoldMaterial.SelectedValue;
            string productMaterial = ddlProductMaterial.SelectedValue;
            string moldType = ddlMoldType.SelectedValue;
            bool slides = chkSlides.Checked;

            string agreementFile = lnkMoldAgreement.ToolTip;
            string agreementUrl = lnkMoldAgreement.NavigateUrl;
            string agreementPath = lnkMoldAgreement.Attributes["AgreementPath"];

            string snapshotFile = lnkSnapshotFile.ToolTip;
            string snapshotUrl = lnkSnapshotFile.NavigateUrl;
            string snapshotPath = lnkSnapshotFile.Attributes["SnapshotPath"];


            controller.DNNTestSuggestIsSelectionValid(txtMoldVendor, "Vendor", ref msg);

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowMessage.Value = msg;
                return;
            }



            DataTable dt = Mold_Data();
            DataRow[] arr = dt.Select(string.Format("RecordId = '{0}'", recordId));

            DataRow dr = dt.NewRow();
            if (arr.Length > 0) dr = arr[0];

            DateTime dateMoved = DateTime.Parse(date.ToString());

            decimal moldCost = cost.HasValue ? cost.Value : 0;

            Mold_ParseFile(fuMoldAgreement, ref agreementFile, ref agreementPath, ref agreementUrl);
            Mold_ParseFile(fuSnapshotFile, ref snapshotFile, ref snapshotPath, ref snapshotUrl);

            dr["RecordId"] = recordId;
            dr["NumberOfMolds"] = numberOfMolds.ToString();
            dr["DateMovedText"] = dateMoved.ToString("MM/dd/yyyy");
            dr["VendorSuggest"] = vendorSuggest;
            dr["TrackingId"] = trackingId;
            dr["Cost"] = moldCost;
            dr["CostText"] = moldCost.ToString("$ #.00");
            dr["Active"] = chkMoldActive.Checked.ToString();
            dr["ActiveImage"] = chkMoldActive.Checked ? "~/images/checked.gif" : "~/images/unchecked.gif";
            dr["AgreementFile"] = agreementFile;
            dr["AgreementUrl"] = agreementUrl;
            dr["AgreementPath"] = agreementPath;
            dr["deleted"] = false;
            dr["style"] = true;
            dr["updated"] = updated;
            dr["MoldMaterial"] = moldMaterial;
            dr["ProductMaterial"] = productMaterial;
            dr["MoldType"] = moldType;
            dr["Slides"] = chkSlides.Checked;
            dr["SlidesImage"] = chkSlides.Checked ? "~/images/checked.gif" : "~/images/unchecked.gif";
            dr["SnapshotFile"] = snapshotFile;
            dr["SnapshotUrl"] = snapshotUrl;
            dr["SnapshotPath"] = snapshotPath;
            if (arr.Length == 0) dt.Rows.Add(dr);

            Mold_Load(dt);
            Mold_Toggle(false);

        }

        private void Mold_ParseFile(FileUpload fu, ref string fileName, ref string filePath, ref string fileUrl)
        {
            if (!fu.HasFile) return;

            fileName = fu.FileName;

            int iPos = fileName.LastIndexOf('.');
            string ext = "txt";
            if (iPos > -1) ext = fileName.Substring(iPos + 1);
            string newName = string.Format("{0}_{1}_{2}.{3}", PartId, this.UserId, Guid.NewGuid().ToString().ToUpper(), ext);

            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            filePath = config.GetValue<string>("PDM Documents", "c:\\junk");
            filePath = System.IO.Path.Combine(filePath, entityId, environment, newName);

            fileUrl = string.Format("~/PDMDocs/{0}/{1}/{2}", entityId, environment, newName);

            fu.PostedFile.SaveAs(filePath);
            
        }

        protected void Mold_Edit(object obj, CommandEventArgs args)
        {
            Mold_Toggle(true);

            ImageButton btn = (ImageButton)obj;
            txtNumberOfMolds.Text = btn.Attributes["NumberOfMolds"];
            
            string raw = btn.Attributes["DateMovedText"];
            DateTime? dt = null;
            DateTime test = DateTime.MinValue;
            if (DateTime.TryParse(raw, out test)) dt = test;
            dtDateMoved.DbSelectedDate = dt;

            txtMoldVendor.Text = btn.Attributes["VendorSuggest"];
            txtTrackingId.Text = btn.Attributes["TrackingId"];
            string cost = btn.Attributes["Cost"];
            decimal dCost = 0;
            if (Decimal.TryParse(cost, out dCost)) txtMoldCost.Text = dCost.ToString("#.00");
            string active = btn.Attributes["Active"];
            bool bActive = false;
            bool.TryParse(active, out bActive);
            chkMoldActive.Checked = bActive;
            lnkMoldAgreement.Attributes.Remove("AgreementPath");

            try { ddlMoldMaterial.SelectedValue = btn.Attributes["MoldMaterial"]; }
            catch { ddlMoldMaterial.SelectedIndex = 0; }

            try { ddlProductMaterial.SelectedValue = btn.Attributes["ProductMaterial"]; }
            catch { ddlProductMaterial.SelectedIndex = 0; }

            try { ddlMoldType.SelectedValue = btn.Attributes["MoldType"]; }
            catch { ddlMoldType.SelectedIndex = 0; }

            active = btn.Attributes["Slides"];
            bActive = false;
            bool.TryParse(active, out bActive);
            chkSlides.Checked = bActive;
            lnkSnapshotFile.Attributes.Remove("SnapshotPath");


            string recordId = btn.Attributes["RecordId"];
            if (string.IsNullOrEmpty(recordId))
            {
                btnSaveMolde.CommandArgument = Guid.NewGuid().ToString();
                btnSaveMolde.CommandName = "New";
                lblEditMold.Text = "Add Mold";
                lnkMoldAgreement.Visible = false;
                chkMoldActive.Checked = true;
                lnkMoldAgreement.NavigateUrl = "";
                lnkMoldAgreement.ToolTip = "";


                lnkSnapshotFile.Visible = false;
                lnkSnapshotFile.NavigateUrl = "";
                lnkSnapshotFile.ToolTip = "";
                chkSlides.Checked = false;

            }
            else
            {
                btnSaveMolde.CommandArgument = recordId;
                btnSaveMolde.CommandName = "Update";
                lblEditMold.Text= "Edit Mold";
                lnkMoldAgreement.Visible = ! string.IsNullOrEmpty(btn.Attributes["AgreementUrl"]);
                lnkMoldAgreement.ToolTip = btn.Attributes["AgreementFile"];
                lnkMoldAgreement.NavigateUrl = btn.Attributes["AgreementUrl"];
                lnkMoldAgreement.Attributes.Add("AgreementPath", btn.Attributes["AgreementPath"]);

                lnkSnapshotFile.Visible = !string.IsNullOrEmpty(btn.Attributes["SnapshotUrl"]);
                lnkSnapshotFile.ToolTip = btn.Attributes["SnapshotFile"];
                lnkSnapshotFile.NavigateUrl = btn.Attributes["SnapshotUrl"];
                lnkSnapshotFile.Attributes.Add("SnapshotPath", btn.Attributes["SnapshotPath"]);
            }

        }

        protected void Mold_Toggle(object obj, CommandEventArgs args)
        {
            bool open = args.CommandArgument.ToString().Equals("Open");
            Mold_Toggle(open);

            

        }

        private void Mold_Toggle(bool open)
        {
            txtNumberOfMolds.Text = string.Empty;
            dtDateMoved.DbSelectedDate = null;
            txtMoldVendor.Text = string.Empty;
            txtTrackingId.Text = string.Empty;
            txtMoldCost.Text = string.Empty;
            ddlMoldMaterial.SelectedIndex = 0;
            ddlProductMaterial.SelectedIndex = 0;
            ddlMoldType.SelectedIndex = 0;
            chkSlides.Checked = false;


            Controller controller = new Controller(entityId, environment);
            controller.StripAllErrors(pnlAddMold);
            
            pnlMain.Visible = !open;
            pnlAddMold.Visible = open;
        }

        private void Mold_Load(DataTable dt)
        {
            if (!dt.Columns.Contains("deleted"))
            {
                dt.Columns.Add("deleted", System.Type.GetType("System.Boolean"));
                dt.Columns.Add("style", System.Type.GetType("System.Boolean"));
                dt.Columns.Add("updated", System.Type.GetType("System.Boolean"));

                foreach (DataRow dr in dt.Rows) 
                {
                    dr["deleted"] = false;
                    dr["style"] = true;
                    dr["updated"] = false;
                }
            }

            lstMolds.DataSource = dt;
            lstMolds.DataBind();

            DataRow[] arr = dt.Select("deleted = 0");
            lblNoMolds.Visible = (arr.Length == 0);
        }

        private DataTable Mold_Data()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("RecordId", System.Type.GetType("System.String"));
            dt.Columns.Add("NumberOfMolds", System.Type.GetType("System.String"));
            dt.Columns.Add("DateMovedText", System.Type.GetType("System.String"));
            dt.Columns.Add("VendorSuggest", System.Type.GetType("System.String"));
            dt.Columns.Add("TrackingId", System.Type.GetType("System.String"));
            dt.Columns.Add("Cost", System.Type.GetType("System.String"));
            dt.Columns.Add("CostText", System.Type.GetType("System.String"));
            dt.Columns.Add("AgreementFile", System.Type.GetType("System.String"));
            dt.Columns.Add("AgreementUrl", System.Type.GetType("System.String"));
            dt.Columns.Add("AgreementPath", System.Type.GetType("System.String"));
            dt.Columns.Add("Active", System.Type.GetType("System.String"));
            dt.Columns.Add("ActiveImage", System.Type.GetType("System.String"));
            dt.Columns.Add("deleted", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("updated", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("style", System.Type.GetType("System.Boolean"));
            dt.Columns.Add("MoldMaterial", System.Type.GetType("System.String"));
            dt.Columns.Add("ProductMaterial", System.Type.GetType("System.String"));
            dt.Columns.Add("MoldType", System.Type.GetType("System.String"));
            dt.Columns.Add("Slides", System.Type.GetType("System.String"));
            dt.Columns.Add("SlidesImage", System.Type.GetType("System.String"));
            dt.Columns.Add("SnapshotFile", System.Type.GetType("System.String"));
            dt.Columns.Add("SnapshotUrl", System.Type.GetType("System.String"));
            dt.Columns.Add("SnapshotPath", System.Type.GetType("System.String"));

            Mold_Data(lstMolds, dt);
            return dt;
        }

        private void Mold_Data(object parent, DataTable dt)
        {
            foreach (Control child in ((Control)parent).Controls)
            {
                if (child.GetType().Name.Equals("ImageButton"))
                {
                    ImageButton btn = (ImageButton)child;
                    if (btn.ID == "btnEditMold")
                    {
                        DataRow dr = dt.NewRow();
                        dr["RecordId"] = btn.Attributes["RecordId"];
                        dr["NumberOfMolds"] = btn.Attributes["NumberOfMolds"];
                        dr["DateMovedText"] = btn.Attributes["DateMovedText"];
                        dr["VendorSuggest"] = btn.Attributes["VendorSuggest"];
                        dr["TrackingId"] = btn.Attributes["TrackingId"];
                        dr["Active"] = btn.Attributes["Active"];
                        dr["Cost"] = btn.Attributes["Cost"];
                        dr["CostText"] = btn.Attributes["CostText"];
                        dr["AgreementFile"] = btn.Attributes["AgreementFile"];
                        dr["AgreementUrl"] = btn.Attributes["AgreementUrl"];
                        dr["AgreementPath"] = btn.Attributes["AgreementPath"];
                        dr["ActiveImage"] = btn.Attributes["ActiveImage"];
                        bool deleted = Boolean.Parse(btn.Attributes["deleted"]);
                        dr["Deleted"] = deleted;
                        dr["style"] = !deleted;
                        dr["updated"] = Boolean.Parse(btn.Attributes["updated"]);
                        dr["MoldMaterial"] = btn.Attributes["MoldMaterial"];
                        dr["ProductMaterial"] = btn.Attributes["ProductMaterial"];
                        dr["MoldType"] = btn.Attributes["MoldType"];
                        dr["Slides"] = btn.Attributes["Slides"];
                        dr["SlidesImage"] = btn.Attributes["SlidesImage"];
                        dr["SnapshotFile"] = btn.Attributes["SnapshotFile"];
                        dr["SnapshotUrl"] = btn.Attributes["SnapshotUrl"];
                        dr["SnapshotPath"] = btn.Attributes["SnapshotPath"];

                        dt.Rows.Add(dr);
                    }
                }
                Mold_Data(child, dt);
            }

        }
        
        protected bool ShowAgreement(object obj)
        {
            string url = obj.ToString();
            return !string.IsNullOrEmpty(url);
        }


        protected override void PostApprovalLoad()
        {
            base.PostApprovalLoad();

            Controller controller = new Controller(entityId, environment);
            controller.PostApprovalLockdown(rptContainer);
        }
    }
}
 