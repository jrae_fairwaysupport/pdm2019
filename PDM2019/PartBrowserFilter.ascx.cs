using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class PartBrowserFilter : PortalModuleBase, IActionable
    {

        protected void Page_Load(object sender, EventArgs e)
        {


            try
            {
                DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
                currPage.Title = "PDM Part Filter";

                if (!IsPostBack)
                {
                    //environment = QueryStringValue<string>("Environment", "Production");

                    string curFilter = Filter.LoadFilter();
                    Filter.BaseQuery = "SELECT Id, PartId, Description, Status, IsRevisedPart FROM vwPartList";
                    Filter.OrderBy = "Id";
                    LockDownControls();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }

        }

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion

        private void LockDownControls()
        {
            if (entityId.Equals("LRH"))
            {
                ddlCompany.Items.Add(new ListItem("Educational Insights", "EI"));
                ddlCompany.Items.Add(new ListItem("Learning Resources", "LR"));
            }
            else
            {
                ddlCompany.Items.Add(new ListItem("Hand2Mind", "H2M"));
            }
            
        }

        private string GetUserSetting(string key, string defaultValue = "")
        {
            if (DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key) != null)
                return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key);
            else
                return defaultValue;
        }

        //private void SaveUserSetting(string key, string value)
        //{
        //    DotNetNuke.Services.Personalization.Personalization.SetProfile("PDM", key, value);
        //}

        string environment { get { return GetUserSetting("Environment", "Production"); } }
        string entityId { get { return GetUserSetting("EntityId", "LRH"); } }



        protected void DoLookup(object source, DotNetNuke.UI.WebControls.DNNTextSuggestEventArgs e)
        {
            DNNTextSuggest txtSearch = (DNNTextSuggest)source;
            txtSearch.CaseSensitive = false;

            string fieldName = txtSearch.Attributes["FieldName"];


            FirstCharSearch(e.Nodes, e.Text.ToUpper(), fieldName);

        }

        private void FirstCharSearch(DNNNodeCollection objNodes, string entry, string fieldName)
        {
            entry = entry.Replace("[", "").Replace("]", "").Replace("'", "''");

            if (string.IsNullOrEmpty(entry.Trim())) return;

            //SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            //string dbName = entityId.Equals("LRH") ? "VMLREI11" : "H2MVMLREI11";
            //SvcLibrary.DBHandler vmfg = new SvcLibrary.DBHandler(config, null, environment, dbName);

            string keyParam = entry.ToUpper().Substring(0, 1);
            string key = string.Format("PDM_{0}_{1}", fieldName, keyParam);

            string queryTmpl8 = string.Empty;
            string filterTmpl8 = string.Empty;

            switch (fieldName)
            {
                case "ParentPartId":
                    queryTmpl8 = "SELECT ID, DESCRIPTION AS NAME FROM vwPARTS WHERE ID LIKE '{0}%' ORDER BY ID";
                    filterTmpl8 = "ID LIKE '{0}%'";
                    break;
                default:
                    //PreferredVendor, PreferredVendorId
                    queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME";
                    filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'";
                    break;

            }

            PartBrowserController controller = new PartBrowserController(entityId, environment);
            DataTable dt = new DataTable();
            if (Application[key] == null)
            {
                dt = controller.ExecuteReader(string.Format(queryTmpl8, keyParam));
                Application[key] = dt;
            }
            else
            {
                dt = (DataTable)Application[key];
            }

            string filter = string.Format(filterTmpl8, entry);

            DataRow[] arr = dt.Select(filter);
            int items = 0;
            DNNNode o;

            foreach (DataRow dr in arr)
            {
                string Id = controller.GetValue<string>(dr, "ID", string.Empty);
                string Name = controller.GetValue<string>(dr, "NAME", string.Empty).Replace("[", "(").Replace("]", ")");

                string displayText = string.Empty;
                if (Id.StartsWith(entry))
                {
                    displayText = Id + " - " + Name;
                }
                else if (Name.StartsWith(entry))
                {
                    displayText = Name + " [" + Id + "]";
                }

                if (!string.IsNullOrEmpty(displayText))
                {
                    o = new DNNNode(displayText);
                    o.ID = Id;
                    o.set_CustomAttribute("selectedID", Id);
                    objNodes.Add(o);

                    items++;

                }

                if (items >= 10) break;
            }

        }



        protected void DoClear(object obj, CommandEventArgs args)
        {
            Filter.ClearFilter();
            lblRecords.Text = string.Empty;
        }

        protected void DoTest(object obj, CommandEventArgs args)
        {
            PartBrowserController controller = new PartBrowserController(entityId, environment);
            int records = controller.FetchRecordCount(Filter.Filter());
            lblRecords.Text = records.ToString() + " records.";

        }

        protected void DoApply(object obj, CommandEventArgs args)
        {
            Filter.SaveFilter();
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(""), true);

        }

    }
}