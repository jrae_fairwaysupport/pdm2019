using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Text;


namespace YourCompany.Modules.PDM2019
{
    public partial class QualityTabPartInformationEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Quality Part Information"; } }

        protected override bool Validate()
        {
            return true;
        }

        protected override void SetDefaultValues(DataSet ds)
        {
            //bool newRecord = NewRecord(ds, "vwQAPartInfos");
            //chkToggleLabels.Checked = !newRecord;
            chkToggleLabels.Checked = true;
        }

 

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {

                CloseForm();
                return;
            }

            if (!Validate()) return;

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            List<string> missingTables = new List<string>();
            if (!ds.Tables.Contains("vwPartInfos")) missingTables.Add("vwPartInfos");
            if (!ds.Tables.Contains("Parts")) missingTables.Add("Parts");
            if (!ds.Tables.Contains("vwApprovals")) missingTables.Add("vwApprovals");

            if (missingTables.Count > 0)
            {
                DataSet dsMissing = controller.LoadPart(PartId, missingTables.ToArray());
                foreach (DataTable dtTemp in dsMissing.Tables) ds.Tables.Add(dtTemp.Copy());
            }
            bool newRecord = NewRecord(ds, "vwQAPartInfos");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);

            SaveTableEdits(tables, "vwPartSpecifications", "Product Development.Specifications", NewRecord(ds, "vwPartSpecifications"), controller);
            SaveTableEdits(tables, "vwQAPartInfos", "Quality.Part Information", newRecord, controller);

            NotifyCreative(ds, tables["vwQAPartInfos"]);
            SaveOracleData(controller, tables);
            CloseForm();
        }

        private void SaveOracleData(Controller controller, Dictionary<string, TableFields> tables)
        {
            if (!partStatus.Equals("Approved")) return;

            //if we cam from DoSave, we can drop out if General Description has not changed
            if (tables != null)
            {
                if (!tables["vwPartSpecifications"].HasFieldChanged("GeneralDescription")) return;
            }

            controller.Specifications_Save(PartId, sku, txtGeneralDescription.Text.Trim());
        }

        private void NotifyCreative(DataSet ds, TableFields qa)
        {
            if (!qa.IsDirty()) return;

            List<string> fields = new List<string> { "CEWarning", "SmallPartsWarning", "SmallBallWarning", "OwlPellets", "MarbleWarning", "LatexBalloonsWarning", "NoBabyWarning",
                                                        "CordWarning", "MagnetWarning", "SharpFunctionalPointWarning", "MarbleInKit", "BallInKit", "Projectile", "NotHumanConsumption",
                                                        "NotSafetyProtection", "CCCWarning", "CCCDescription", "CCCDate", "WashBeforeUseWarning", "WashBeforeUseDescription",
                                                        "ShellFishWarning", "ShellFishDescription", "AdultSupervision", "NaturalLeadWarning", "MeetsANSIZ81", "LatextWarning",
                                                        "Prop65ReproductiveToxicantsWarning" };

            //StringBuilder msg = new StringBuilder();
            //string rowTmpl8 = "<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>";

            //msg.AppendLine("The following warnings have been added and/or modified to this part:<br />");
            //msg.AppendLine(@"<table cellspacing=""0"" cellpadding=""4"" border=""1"">");
            //msg.AppendFormat(rowTmpl8, "Warning", "Old Value", "New Value");
            Controller controller = new Controller(entityId, environment);
            
            //bool sendEmail = false;
            foreach (string field in fields)
            {
                if (qa.HasFieldChanged(field))
                {
                    //msg.AppendFormat(rowTmpl8, field, qa.Fields[field].oldHistory, qa.Fields[field].newHistory);
                    if (qa.Fields[field].oldValue != null) controller.pdm.InsertRecord("CreativeNotifications", "PartId", "PartId", PartId, "ModifiedBy", this.UserId, "ModifiedDate", DateTime.Now,
                                                        "Warning", field, "Added", (bool)qa.Fields[field].newValue, "ReportType", "QA_TO_CREATIVE");
                }
            }
            //msg.AppendLine("</table>");

            //if (!sendEmail) return;

            //EmailController email = new EmailController(entityId, environment);
            //Controller controller = new Controller(entityId, environment);
            //email.SendLRNotification_WarningToCreative(controller, PartId, ds, GetModuleSetting("PartUrlTemplate"), this.PortalId, this.UserInfo, msg.ToString());



        }

        private string GetModuleSetting(string key)
        {
            key = entityId + environment + key;
            PDM2019Settings settingsData = new PDM2019Settings(this.TabModuleId);
            return settingsData.ReadSetting<string>(key, string.Empty);
        }

        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwQAPartInfos")) return;

            TableFields table = tables["vwQAPartInfos"];
            if (! table.IsDirty()) return;


            //custom logic
            if (!(bool)table.Fields["CCCWarning"].newValue)
            {
                table.Fields["CCCDescription"].newValue = null;
                table.Fields["CCCDate"].newValue = null;
            }

            if (!(bool)table.Fields["WashBeforeUseWarning"].newValue) table.Fields["WashBeforeUseDescription"].newValue = null;
            if (!(bool)table.Fields["ShellfishWarning"].newValue) table.Fields["ShellfishDescription"].newValue = null;
        }


        protected void Battery_Delete(object obj, CommandEventArgs args) 
        {
            ImageButton btn = (ImageButton)obj;

            int recordId = Int32.Parse(args.CommandArgument.ToString());
            string batteryFor = btn.Attributes["BatteriesFor"];
            string size = btn.Attributes["BatterySize"];
            int qty = Int32.Parse(btn.Attributes["BatteryQuantity"]);
            bool included = bool.Parse(btn.Attributes["BatteryIncluded"]);

            Controller controller = new Controller(entityId, environment);
            controller.Battery_Delete(this.UserInfo.Username, recordId, PartId, batteryFor, size, qty, included);
            IsDirty = true;
            Battery_Load();
        }

        private bool Battery_Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);
            controller.ValidateText(ddlAddBatterySize, true, "Battery Size", ref msg);
            controller.ValidateNumber(txtAddBatteryQuantity, true, "Battery Quantity", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void Battery_Save(object obj, CommandEventArgs args)
        {
            if (!Battery_Validate()) return;

            string batterySize = ddlAddBatterySize.SelectedValue;
            int batteryQty = Int32.Parse(txtAddBatteryQuantity.Text.Trim());
            bool included = chkAddBatteryIncluded.Checked;

            Controller controller = new Controller(entityId, environment);
            controller.Battery_Add(this.UserInfo.Username, PartId, "Part", batterySize, batteryQty, included);
            IsDirty = true;
            Battery_Load();
        
        
            Battery_Toggle(false);
        }

        protected void Battery_Toggle(object obj, CommandEventArgs args)
        {
            bool open = args.CommandArgument.ToString().Equals("Open");
            Battery_Toggle(open);
        }

        private void Battery_Toggle(bool open)
        {
            ddlAddBatterySize.SelectedIndex = 0;
            txtAddBatteryQuantity.Text = string.Empty;
            chkAddBatteryIncluded.Checked = false;

            Controller controller = new Controller(entityId, environment);
            controller.StripErrorStyles(ddlAddBatterySize);
            controller.StripErrorStyles(txtAddBatteryQuantity);
            
            pnlMain.Visible = !open;
            pnlAddBattery.Visible = open;
        }

        private void Battery_Load()
        {
            Controller controller = new Controller(entityId, environment);

            DataSet ds = controller.LoadPart(PartId, "vwBatteries");
            lstBatteries.DataSource = ds.Tables["vwBatteries"];
            lstBatteries.DataBind();
            lblNoBatteries.Visible = ds.Tables["vwBatteries"].Rows.Count == 0;
        }

        

    
    }
}
 