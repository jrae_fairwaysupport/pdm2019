<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QualityTabPartInformationEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.QualityTabPartInformationEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        ToggleElectronics();
        //ToggleBatteries();

    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }


    function ToggleElectronics() {
        var checked = jQuery('[id*="chkElectronic"]:checked').length > 0;
        var labels = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        if (checked) {
            jQuery('[id*="chkheadphones"]').show();
            jQuery('label[for*="chkheadphones"]').show();
            if (labels) jQuery('[id*="labelheadphoneqty"]').show();
            jQuery('[id*="lblheadphoneqty"]').show();
            jQuery('[id*="txtheadphoneqty"]').show();
            jQuery('[id*="chkBatteriesRequired"]').show();
            jQuery('label[for*="chkBatteriesRequired"]').show();
            
        }
        else {
            jQuery('[id*="chkheadphones"]').hide();
            jQuery('label[for*="chkheadphones"]').hide();
            jQuery('[id*="labelheadphoneqty"]').hide();
            jQuery('[id*="lblheadphoneqty"]').hide();
            jQuery('[id*="txtheadphoneqty"]').hide();
            jQuery('[id*="chkBatteriesRequired"]').hide();
            jQuery('label[for*="chkBatteriesRequired"]').hide();
        }
        ToggleBatteries();
    }

    function ToggleBatteries() {
        var checked = jQuery('[id*="chkBatteriesRequired"]:checked').length > 0;
        var electronic = jQuery('[id*="chkElectronic"]:checked').length > 0;

        if (checked && electronic)
            jQuery('[id*="pnlBatteries"]').show();
        else
            jQuery('[id*="pnlBatteries"]').hide();
    }

    

</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
        <table width="800" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="width:200px;"><asp:Label ID="labelGeneralDescription" runat="server" CssClass="SubSubHead" Text="Specification:" /></td>
                <td style="width:200px;"></td>
                <td style="width:200px;"></td>
                <td style="width:200px;"></td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <asp:TextBox ID="txtGeneralDescription" runat="server" CssClass="Box" Width="780" Height="150" TextMode="MultiLine" Table="vwPartSpecifications" Field="GeneralDescription" Placeholder="Specifications" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <asp:Label ID="labelInspectionNotes" runat="server" CssClass="SubSubHead" Text="Inspection Notes:<br />" />
                    <asp:TextBox ID="txtInspectionNotes" runat="server" CssClass="Box" Width="780" MaxLength="50" Table="vwQAPartInfos" Field="QAInspectionNotes"  Placeholder="Inspection Notes" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:CheckBox Width="195" ID="chkFeatureSet" runat="server" CssClass="SubSubHead" TextAlign='Right' Text="&nbsp;Feature Set" Table="vwPartSpecifications" Field="FeatureSet" />
                    <asp:CheckBox Width="195" ID="CheckBox4" runat="server" CssClass="SubSubHead" TextAlign='Right' Text="&nbsp;Lot Type" Table="vwQAPartInfos" Field="LotTypeChecked" TargetField="LotType" CheckedValue="TOY" UncheckedValue="NOT TOY"/>
                </td>
            </tr>
            <tr>
                <td valign="bottom" colspan="2">
                    <asp:CheckBox Width="195" ID="chkElectronic" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Electronic Product" Table="vwPartSpecifications" Field="Electronic" onclick="javascript:ToggleElectronics();" />
                    <asp:CheckBox Width="195" ID="chkheadphones" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Headphone Jack(s)" Table="vwPartSpecifications" Field="HeadphoneJacks" />
                </td>
                <td valign="bottom">
                    <asp:Label ID="lblheadphoneqty" runat="server" CssClass="SubSubHead" Text="Headphone Jack Quantity:" />
                </td>
                <td>
                    <asp:TextBox ID="txtheadphoneqty" runat="server" CssClass="Box" Width="75" MaxLength="10" Format="Number" onkeypress="return isNumberKey(event);" Table="vwPartSpecifications" Field="HeadphoneJackQuantity" Placeholder="Quantity" />
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top" >
                    <asp:Label ID="lblbatteryspace" runat="server" Width="194" Text="&nbsp;" />
                    <asp:CheckBox ID="chkBatteriesRequired" runat="server" Width="190" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Batteries Required" Table="vwPartSpecifications" Field="Batteries" onclick="javascript:ToggleBatteries();"/>
                </td>

                <td colspan="2" valign="top">
                    <asp:Panel ID="pnlBatteries" runat="server" width="350">
                        <table width="325" class="HeaderRow">
                            <tr>
                                <td style="width:150px;padding-left:4px;">Battery Size</td>
                                <td style="width:75px;" align="center">Quantity</td>
                                <td style="width:50px;" align="center">Included</td>
                                <td style="width:50px; padding:2px 4px 2px 2px;" align="right" valign="middle">
                                    <asp:ImageButton ID="btnAddBattery" runat="server" ImageUrl="~/images/plus2.gif" OnCommand="Battery_Toggle" CommandArgument="Open" />
                                </td>
                            </tr>
                        </table>
                        <asp:DataList ID="lstBatteries" runat="server" CssClass="table table-hover table-condensed" Table="vwBatteries" Width="325" OnDataBinding="VerifyDataListHasData" NoDataLabel="lblNoBatteries" >
                            <ItemTemplate>
                                <asp:Label ID="lblbatterysize" Width="150" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BatterySize") %>' />
                                <asp:Label ID="lblbatteryqty" runat="server" Width="75" style="text-align:center;" Text='<%# DataBinder.Eval(Container.DataItem, "BatteryQuantity") %>' />
                                <asp:Label ID="Label15" runat="server" Width="20" Text="&nbsp;" /><asp:Image id="imgbatteryincldue" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "BatteryIncludedImage") %>' />
                                <asp:Label ID="lblSpace2" runat="server" Width="20" Text="&nbsp;" />
                                <asp:ImageButton id="imgBatteryDelete" runat="server" ImageUrl="~/images/delete.gif" 
                                                        OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');" Hint="Editor" 
                                                        CommandArgument ='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>'
                                                        BatteriesFor = '<%# DataBinder.Eval(Container.DataItem, "BatteriesFor") %>'
                                                        BatterySize = '<%# DataBinder.Eval(Container.DataItem, "BatterySize") %>'
                                                        BatteryQuantity = '<%# DataBinder.Eval(Container.DataItem, "BatteryQuantity") %>'
                                                        BatteryIncluded = '<%# DataBinder.Eval(Container.DataItem, "BatteryIncluded") %>'
                                                        OnCommand="Battery_Delete"
                                />
                            </ItemTemplate>
                            <AlternatingItemStyle BackColor="#e8e2e2" />
                        </asp:DataList>
                        <asp:Label ID="lblNoBatteries" runat="server" CssClass="Normal" Width="325" Text="No batteries have been added to Part." style="text-align:left;padding-left:4px;" />

                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:CheckBox Width="195" ID="chkEnglishx" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Dishwasher Safe" Table="vwQAPartInfos" Field="DishwasherSafe" />
                    <asp:CheckBox Width="195" ID="CheckBox1" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Food Safe" Table="vwQAPartInfos" Field="FoodSafe" />
                    <asp:CheckBox Width="195" ID="CheckBox2" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Exempt" Table="vwQAPartInfos" Field="Exempt" />
                    <asp:CheckBox Width="195" ID="CheckBox3" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Component Contains Paint" Table="vwQAPartInfos" Field="PaintTesting" />
                </td>
            </tr>
            <tr>
                <td colspan="4" valign="bottom">
                    <asp:Label ID="labelInitialReportDate" runat="server" CssClass="SubSubHead" Text="Initial Report Date:<br />" />
                    <telerik:RadDatePicker id="dtInitialReportDate" width="195" runat="server" CssClass="Box" Table="vwQAPartInfos" Field="InitialTestReportDate" Placeholder="Initial Test Report Date"  />
                    <asp:Label ID="lblhazardspace" runat="server" Width="186" Text="&nbsp;" />
                    <asp:CheckBox ID="CheckBox5" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Hazardous Material" Table="vwQAPartInfos" Field="HazardousMaterial" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Label ID="labelAllergens" runat="server" CssClass="SubSubHead" Width="780" Text="Allergens:" />
                </td>
            </tr>
            <tr>
                <td colspan="4" style="border:solid 1px #e8e2e2;">
                    <asp:CheckBox Width="195" ID="CheckBox6" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Latex" Table="vwQAPartInfos" Field="LatexAllergen"  />
                    <asp:CheckBox Width="195" ID="CheckBox7" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;PVC" Table="vwQAPartInfos" Field="PvcAllergen" />
                    <asp:CheckBox Width="195" ID="CheckBox8" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;BPA" Table="vwQAPartInfos" Field="BPAAllergen"  />
                    <asp:CheckBox Width="195" ID="CheckBox9" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Polycarbonates" Table="vwQAPartInfos" Field="PolycarbonatesAllergen"  />
                    <br />

                    <asp:CheckBox Width="195" ID="CheckBox10" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;TCEP" Table="vwQAPartInfos" Field="TCEPAllergen"  />
                    <asp:CheckBox Width="195" ID="CheckBox11" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;PVS" Table="vwQAPartInfos" Field="PVSAllergen"/>
                    <asp:CheckBox Width="195" ID="CheckBox12" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Toluene" Table="vwQAPartInfos" Field="TolueneAllergen" />
                    <asp:CheckBox Width="195" ID="CheckBox13" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Formaldehyde" Table="vwQAPartInfos" Field="FormaldehydeAllergen"  />

                </td>
            </tr>
            <tr>
                <td colspan="4"><asp:Label ID="label1" runat="server" CssClass="SubSubHead" Text="Package Warnings:" /></td>
            </tr>
            <tr>
                <td colspan="4" style="border:solid 1px #e8e2e2;">
                    <asp:CheckBox Width="195" ID="CheckBox14" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;CE" Table="vwQAPartInfos" Field="CEWarning"  />
                    <asp:CheckBox Width="195" ID="CheckBox15" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Small Parts" Table="vwQAPartInfos" Field="SmallPartsWarning"  />
                    <asp:CheckBox Width="195" ID="CheckBox16" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Small Ball" Table="vwQAPartInfos" Field="SmallBallWarning"  />
                    <asp:CheckBox Width="195" ID="CheckBox17" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Owl Pellets" Table="vwQAPartInfos" Field="OwlPellets"  />
                    <br />
            
                    <asp:CheckBox Width="195" ID="CheckBox18" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Marble" Table="vwQAPartInfos" Field="MarbleWarning"  />
                    <asp:CheckBox Width="195" ID="CheckBox19" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Latex Balloons" Table="vwQAPartInfos" Field="LatexBalloonsWarning"  />
                    <asp:CheckBox Width="195" ID="CheckBox20" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;No Baby" Table="vwQAPartInfos" Field="NoBabyWarning"  />
                    <asp:CheckBox Width="195" ID="CheckBox21" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Cord" Table="vwQAPartInfos" Field="CordWarning"  />
                    <br />

                    <asp:CheckBox Width="195" ID="CheckBox22" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Magnet" Table="vwQAPartInfos" Field="MagnetWarning"  />
                    <asp:CheckBox Width="195" ID="CheckBox23" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Sharp Functional Point" Table="vwQAPartInfos" Field="SharpFunctionalPointWarning"  />
                    <asp:CheckBox Width="195" ID="CheckBox24" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Marble In Kit" Table="vwQAPartInfos" Field="MarbleInKit"  />
                    <asp:CheckBox Width="195" ID="CheckBox25" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;BallInKit" Table="vwQAPartInfos" Field="BallInKit"  />
                    <br />

                    <asp:CheckBox Width="195" ID="CheckBox61" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Projectile" Table="vwQAPartInfos" Field="Projectile"  />
                    <br />

                    <asp:CheckBox Width="393" ID="CheckBox26" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Not for Human Consumption" Table="vwQAPartInfos" Field="NotHumanConsumption"  />
                    <asp:CheckBox Width="390" ID="CheckBox29" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Not for Safety Protection" Table="vwQAPartInfos" Field="NotSafetyProtection"  />
                    <br />

                    <asp:Label Width="195" ID="labelCCCSpace" CssClass="SubSubHead" runat="server" Text="&nbsp;" />
                    <asp:Label Width="400" ID="labelCCCDescription" runat="server" CssClass="SubSubHead" Text="CCC Description:" />
                    <asp:Label id="labelCCCDate" runat="server" CssClass="SubSubHead" Text="CCC Date:<br />" />

                
                    <asp:CheckBox Width="195" ID="chkCCC" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;CCC" Table="vwQAPartInfos" Field="CCCWarning"  />
                    <asp:TextBox  Width="380" ID="txtCCCDescription" runat="server" CssClass="Box" MaxLength="50" Placeholder="CCC Description" Table="vwQAPartInfos" Field="CCCDescription" />
                    <telerik:RadDatePicker id="dtCCCDate" width="180" runat="server" CssClass="Box" Table="vwQAPartInfos" Field="CCCDate" Placeholder="CCC Date"  />
                    <br />

                    <asp:Label Width="195" ID="label2" CssClass="SubSubHead" runat="server" Text="&nbsp;" />
                    <asp:Label Width="400" ID="label3" runat="server" CssClass="SubSubHead" Text="Wash Before Use Description:<br />" />
                
                    <asp:CheckBox Width="195" ID="CheckBox27" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Wash Before Use" Table="vwQAPartInfos" Field="WashBeforeUseWarning"  />
                    <asp:TextBox  Width="380" ID="TextBox1" runat="server" CssClass="Box" MaxLength="50" Placeholder="Wash Before Use Description" Table="vwQAPartInfos" Field="WashBeforeUseDescription" />
                    <br />

                    <asp:Label Width="195" ID="label4" CssClass="SubSubHead" runat="server" Text="&nbsp;" />
                    <asp:Label Width="400" ID="label5" runat="server" CssClass="SubSubHead" Text="Shell Fish Description:<br />" />
                
                    <asp:CheckBox Width="195" ID="CheckBox28" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Shell Fish" Table="vwQAPartInfos" Field="ShellfishWarning"  />
                    <asp:TextBox  Width="380" ID="TextBox2" runat="server" CssClass="Box" MaxLength="50" Placeholder="Shell Fish Description" Table="vwQAPartInfos" Field="ShellfishDescription" />
                    <br />

                    <asp:CheckBox Width="195" ID="CheckBox30" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Adult Supervision" Table="vwQAPartInfos" Field="AdultSupervision"  />
                    <asp:CheckBox Width="195" ID="CheckBox31" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Prop65-Carcinogens" Table="vwQAPartInfos" Field="Prop65CarcinogensWarning" TargetField="NaturalLeadWarning"  />
                    <asp:CheckBox ID="CheckBox32" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Meets ANSIZ87.1 Standards" Table="vwQAPartInfos" Field="MeetsANSIZ87"  />
                    <br />

                    <asp:CheckBox Width="195" ID="CheckBox33" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Latex Warning" Table="vwQAPartInfos" Field="LatextWarning"  />
                    <asp:CheckBox ID="CheckBox34" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Prop65-Reproductive Toxicants" Table="vwQAPartInfos" Field="Prop65ReproductiveToxicantsWarning"  />
                </td>
	        </tr>
            <tr>
                <td colspan="4"><asp:Label ID="labelMaterialInfos" runat="server" CssClass="SubSubHead" Text="Material Info:" /></td>
            </tr>
            <tr>
                <td colspan="4" style="border:solid 1px #e8e2e2;">
                    <asp:CheckBox Width="195" ID="CheckBox35" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Steel" Table="vwQAPartInfos" Field="Steel" TargetField="MetalMaterial"  />
                    <asp:CheckBox Width="195" ID="CheckBox36" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;MDF Wood" Table="vwQAPartInfos" Field="MDFWood" TargetField="WoodMaterial"  />
                    <asp:CheckBox Width="195" ID="CheckBox37" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Cotton" Table="vwQAPartInfos" Field="Cotton" TargetField="TextilesMaterial"  />
                    <asp:CheckBox Width="195" ID="CheckBox38" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Mineral" Table="vwQAPartInfos" Field="MineralMaterial"  />
                    <br />

                    <asp:CheckBox Width="195" ID="CheckBox39" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Latex" Table="vwQAPartInfos" Field="LatexMaterial" />
                    <asp:CheckBox Width="195" ID="CheckBox40" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;ABS" Table="vwQAPartInfos" Field="ABS" TargetField="PlasticMaterial"  />
                    <asp:CheckBox Width="195" ID="CheckBox41" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Stainless Steel" Table="vwQAPartInfos" Field="StainlessSteelMaterial"/>
                    <asp:CheckBox Width="195" ID="CheckBox42" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Iron" Table="vwQAPartInfos" Field="IronMaterial"  />
                    <br />

                    <asp:CheckBox Width="195" ID="CheckBox43" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Beech Wood" Table="vwQAPartInfos" Field="BeechwoodMaterial" />
                    <asp:CheckBox Width="195" ID="CheckBox44" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Pine Wood" Table="vwQAPartInfos" Field="PinewoodMaterial" />
                    <asp:CheckBox Width="195" ID="CheckBox45" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Polyester" Table="vwQAPartInfos" Field="PolyesterMaterial"/>
                    <asp:CheckBox Width="195" ID="CheckBox46" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Poly-cotton Blend" Table="vwQAPartInfos" Field="PolyCottonBlendMaterial"  />
                    <br />

                    <asp:CheckBox Width="393" ID="CheckBox47" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;CoPP (Co-polymer Polypropylene)" Table="vwQAPartInfos" Field="PolypropyleneMaterial" />
                    <asp:CheckBox Width="380" ID="CheckBox48" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;GPPS (General Purpose Polystyrene)" Table="vwQAPartInfos" Field="GPPSGeneral" />
                    <br />

                    <asp:CheckBox Width="393" ID="CheckBox49" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;HDPE (High Density Polyethylene)" Table="vwQAPartInfos" Field="HDPE" />
                    <asp:CheckBox Width="380" ID="CheckBox50" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;HIPS (High Impact Polystyrene)" Table="vwQAPartInfos" Field="HIPS" />
                    <br />

                    <asp:CheckBox Width="393" ID="CheckBox51" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;LDPE (Low Density Polyethylene)" Table="vwQAPartInfos" Field="LDPE" />
                    <asp:CheckBox Width="380" ID="CheckBox52" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;PE (Polyethylene)" Table="vwQAPartInfos" Field="PePolyethylene" />
                    <br />

                    <asp:CheckBox Width="393" ID="CheckBox53" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;PET (Polyethylene Terephthalate)" Table="vwQAPartInfos" Field="PETPolyethylene" />
                    <asp:CheckBox Width="380" ID="CheckBox54" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;PP (Polypropylene)" Table="vwQAPartInfos" Field="PPPolyethylene" />
                    <br />

                    <asp:CheckBox Width="393" ID="CheckBox55" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;POM (Polyoxmethylene)" Table="vwQAPartInfos" Field="PomPolyoxmethylene" />
                    <asp:CheckBox Width="380" ID="CheckBox56" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Thermoplastic Rubber" Table="vwQAPartInfos" Field="Thermoplastic" />
                    <br />

                    <asp:CheckBox Width="195" ID="CheckBox57" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;PMMA" Table="vwQAPartInfos" Field="PMMA" />
                    <asp:CheckBox Width="195" ID="CheckBox58" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;EVA" Table="vwQAPartInfos" Field="EVAMaterial" />
                    <asp:CheckBox Width="195" ID="CheckBox59" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;PS (Polystyrene)" Table="vwQAPartInfos" Field="PsPolystyrene"/>
                    <asp:CheckBox Width="195" ID="CheckBox60" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;PVC" Table="vwQAPartInfos" Field="PVC"  />

                </td>
            </tr>

            <tr>
                <td valign="bottom">
                    <asp:CheckBox width="195" ID="chkWERCs" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Registered with WERCS?" Table="vwQAPartInfos" Field="WERCS" />
                </td>
                <td valign="bottom" colspan="3">
                    <asp:Label ID="labelWERCS" runat="server" CssClass="SubSubHead" Text="WERCS Classification:<br />" />
                    <asp:TextBox ID="txtWERCS" runat="server" Width="380" CssClass="Box" MaxLength="100" Placeholder="WERCS Classification" Table="vwQAPartInfos" Field="WERCSClassification" />
                </td>
            </tr>
        </table>
    </asp:Panel>


    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" />
            </td>
        </tr>
    
    </table>
</asp:Panel>

<asp:Panel ID="pnlAddBattery" runat="server"  Visible="false">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr >
            <td style="height:450px;" align="center" valign="middle">
                <asp:Panel ID="pnladdbatteryinner" runat="server" Width="375" style="border:solid 1px black;padding:4px;">
                    <asp:Label ID="lblAddBatteryForm" runat="server" CssClass="Head" Text="Add Battery" Width="370" style="border-bottom:solid 1px black; text-align:center;" />
                    
                    <p style="padding: 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="lblAddBatterySize" runat="server" CssClass="SubSubHead" Width="100" Text="Battery Size:" />
                        <asp:DropDownList ID="ddlAddBatterySize" runat="server" CssClass="Box" Width="200" >
                            <asp:ListItem Text="Battery Size" Value="" />
                            <asp:ListItem Text="AAA" Value="AAA" />
                            <asp:ListItem Text="AA" Value="AA" />
                            <asp:ListItem Text="C" Value="C" />
                            <asp:ListItem Text="D" Value="D" />
                            <asp:ListItem Text="9 Volt" Value="9V" />
                            <asp:ListItem Text="Button Cells" Value="Button Cells" />
                        </asp:DropDownList>
                    </p>

                    <p style="padding: 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="lblAddBatteryQty" runat="server" CssClass="SubSubHead" Width="100" Text="Quantity:" />
                        <asp:TextBox ID="txtAddBatteryQuantity" runat="server" CssClass="Box" Width="100" Placeholder="Quantity" Format="Number" MaxLength="10"  onkeypress="return isNumberKey(event)" />
                        &nbsp;&nbsp;
                        <asp:CheckBox ID="chkAddBatteryIncluded" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Included" />
                    </p>

                    <p style="padding: 5px 5px 5px 5px; text-align:left;">
                        <asp:Button ID="btnCancelBattery" runat="server" Text="Cancel" OnCommand="Battery_Toggle" CommandArgument="Close" Width="75" />
                        <asp:Label ID="lblAddBatteryFormSpace" runat="server" Width="200" Text="&nbsp;" />
                        <asp:Button ID="btnSaveBattery" runat="server" Text="Save" Width="75" OnCommand="Battery_Save" />
                    </p>
                </asp:Panel>


            </td>
        </tr>
        
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<%--<asp:HiddenField ID="hdnShowPanel" runat="server" Value="Main" />--%>
