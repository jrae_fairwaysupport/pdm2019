<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchasingTabChecklist.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PurchasingChecklistTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="ProductDevelopmentTab.ascx" TagName="productDevelopment" TagPrefix="pdm" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<script language="javascript" type="text/javascript">
    function TogglePurchasingMore(toggleWhat) {
        var link = jQuery('[id*="lnkPurchasingViewMore' + toggleWhat + '"]');
        var lbl = jQuery('[id*="lblPurchasing' + toggleWhat + '"]');


        if (link.text() == "View More") {
            link.text("View Less");
            lbl.css("max-height", "");
        }
        else {
            link.text("View More");
            lbl.css("max-height", "200px");
        }
        return false;
    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        NOT YET IMPLEMENTED - HOW DOES A PART GET REJECTED?<br />
        Handle nested display<br />
        View More Link on Specifications<br />
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPurchasing" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <pdm:productDevelopment id="ctlProductDevelopment" runat="server" />

    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnModuleId" runat="server" Value="-1" />




