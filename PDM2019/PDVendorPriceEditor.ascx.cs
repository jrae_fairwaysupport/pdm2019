using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class VendorPriceEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Vendor Price Breaks"; } }

        protected override DataSet LoadData()
        {
            DataSet ds = base.LoadData();

            lblNoVendorPriceBreaks.Visible = ds.Tables["vwVendorPriceBreaks"].Rows.Count == 0;
            return ds;
        }

        protected override void LockDownControls()
        {
            if (!partStatus.Equals("Pending")) return;
            if (!ViewRole.Equals("pd")) return;

            lnkAddVendorPriceBreak.Visible = false;

        }

        protected override bool Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);
            controller.DNNTestSuggestIsSelectionValid(txtVendorId, "Vendor Id", ref msg);


            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {

                CloseForm();
                return;
            }


            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "PartVendorPrices");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);
            SaveTableEdits(tables, "PartVendorPrices", "Product Development.Vendor Price", newRecord, controller);
            CloseForm();
        }

        private void PrepTableData(Dictionary<string, TableFields>tables, Controller controller)
        {
            if (!tables.ContainsKey("PartVendorPrices")) return;


            TableFields table = tables["PartVendorPrices"];
            if (!table.IsDirty()) return; 

            //custom logic

        }
        
        protected void VendorPriceBreak_Delete(object obj, CommandEventArgs args) 
        {
            ImageButton btn = (ImageButton)obj;
            int recordId = Int32.Parse(btn.Attributes["RecordId"]);

            Controller controller = new Controller(entityId, environment);
            DataTable dt = null;
            string sql = string.Empty;

            if (partStatus.Equals("Approved"))
            {
                sql = "SELECT * FROM vwVendorPriceBreaks WHERE Id = @P0";
                dt = controller.pdm.ExecuteReader(sql, recordId);        
            }


            controller.VendorPriceBreak_Delete(this.UserInfo.Username, recordId, PartId);
            IsDirty = true;
            VendorPriceBreak_Load();

            //if dt is not null, our part is approved, update visual
            if (dt == null) return;
            if (dt.Rows.Count == 0) return;
            DataRow dr = dt.Rows[0];
            string vendorId = controller.GetValue<string>(dr, "VendorId", string.Empty);
            string vendorPartId = controller.GetValue<string>(dr, "VendorPartId", string.Empty);

            //price breaks
            sql = "DELETE FROM VENDOR_QUOTE WHERE VENDOR_ID = @P0 AND VENDOR_PART_ID " + (string.IsNullOrEmpty(vendorPartId) ? "IS NULL" : "= @P1");
            controller.vmfg.ExecuteCommand(sql, vendorId, vendorPartId);

            //vendor part
            sql = "DELETE FROM VENDOR_PART WHERE PART_ID = @P0 AND VENDOR_ID = @P1 AND VENDOR_PART_ID " + (string.IsNullOrEmpty(vendorPartId) ? "IS NULL" : "= @P2");
            controller.vmfg.ExecuteCommand(sql, sku, vendorId, vendorPartId);



        }

        protected void VendorPriceBreak_Save(object obj, CommandEventArgs args) 
        {
            string msg = string.Empty;
            Controller controller = new Controller(entityId, environment);

            controller.ValidateText(txtVendorId, true, "Vendor Id", ref msg);
            controller.ValidateText(txtVendorPartId, true, "Vendor Part Id", ref msg);
            int? q1 = controller.ValidateNumber(txtQuantity1, false, "Quantity 1", ref msg);
            decimal? p1 = controller.ValidateDecimal(txtPrice1, false, "Unit Price 1", ref msg);
            int? q2 = controller.ValidateNumber(txtQuantity2, false, "Quantity 2", ref msg);
            decimal? p2 = controller.ValidateDecimal(txtPrice2, false, "Unit Price 2", ref msg);
            int? q3 = controller.ValidateNumber(txtQuantity3, false, "Quantity 3", ref msg);
            decimal? p3 = controller.ValidateDecimal(txtPrice3, false, "Unit Price 3", ref msg);
            int? q4 = controller.ValidateNumber(txtQuantity4, false, "Quantity 4", ref msg);
            decimal? p4 = controller.ValidateDecimal(txtPrice4, false, "Unit Price 4", ref msg);
            int? q5 = controller.ValidateNumber(txtQuantity5, false, "Quantity 5", ref msg);
            decimal? p5 = controller.ValidateDecimal(txtPrice5, false, "Unit Price 5", ref msg);
            
            hdnShowMessage.Value = msg;

            if (!string.IsNullOrEmpty(msg)) return;

            int recordId = Int32.Parse(btnAddComponentSave.CommandArgument.ToString());
            controller.VendorPriceBreak_Save(this.UserInfo.Username, recordId, PartId, pnlAddVendorPriceBreak);
            IsDirty = true;
            VendorPriceBreak_Load();

            VendorPriceBreak_Toggle(false);

            if (partStatus.Equals("Approved"))
            {
                //do we need to add Vendor_Part?
                if (recordId == -1)
                {
                    string vendorId = txtVendorId.Text.Trim().ToUpper();
                    string vendorPartId = txtVendorPartId.Text.Trim().ToUpper();
                    controller.vmfg.InsertRecord("VENDOR_PART", "PART_ID", "PART_ID", sku, "VENDOR_ID", vendorId, "VENDOR_PART_ID", vendorPartId);
                }
            }
        }

        protected void VendorPriceBreak_Load () 
        {
            Controller controller = new Controller(entityId, environment);

            DataSet ds = controller.LoadPart(PartId, "vwVendorPriceBreaks");
            lstVendorPriceBreaks.DataSource = ds.Tables["vwVendorPriceBreaks"];
            lstVendorPriceBreaks.DataBind();
            lblNoVendorPriceBreaks.Visible = ds.Tables["vwVendorPriceBreaks"].Rows.Count == 0;
        }

        protected void VendorPriceBreak_Toggle(object obj, CommandEventArgs args)
        {
            bool open = args.CommandArgument.ToString().Equals("Open");
            VendorPriceBreak_Toggle(open);

            if (!open) return;
            ImageButton btn = (ImageButton)obj;
            int recordId = Int32.Parse(btn.Attributes["RecordId"]);
            btnAddComponentSave.CommandArgument = recordId.ToString();
            lblAddPriceBreak.Text = (recordId == -1) ? "Add Vendor Price Break" : "Edit Vendor Price Break";


            if (recordId == -1)
            {
                txtVendorPartId.Text = sku;
                return;
            }

            Controller controller = new Controller(entityId, environment);
            DataTable dt = controller.VendorPriceBreak_Load(recordId);
            if (dt.Rows.Count == 0) return;

            controller.PopulateForm(pnlAddVendorPriceBreak, dt);

        }

        private void VendorPriceBreak_Toggle(bool open)
        {
            pnlMain.Visible = !open;
            pnlAddVendorPriceBreak.Visible = open;

            Controller controller = new Controller(entityId, environment);
            DataTable dt = controller.VendorPriceBreak_Load(-1);

            controller.PopulateForm(pnlAddVendorPriceBreak, dt);
            controller.StripAllErrors(pnlAddVendorPriceBreak);
        }

       
 }
}
 