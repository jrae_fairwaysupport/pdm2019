using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class PDVendorPriceTab: LRHViewer
    {
        public override bool Initialize()
        {
            if (base.Initialize()) return true;

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, new string[] { "vwPartInfos" });
            bool hide = false;

            DataTable dt = ds.Tables["vwPartInfos"];
            if ((dt != null) && (dt.Rows.Count > 0))
            {
                bool fabricated = controller.GetValue<bool>(dt.Rows[0], "Fabricated", false);
                string fabLocation = controller.GetValue<string>(dt.Rows[0], "FabricationLocation", string.Empty);

                if (fabricated && fabLocation.Equals("FabUs")) hide = true;
            }
            
            if (!hide)
            {
                Initialized = true;
                return true;
            }



            Panel pnl = (Panel)this.FindControl("pnlActions");
            if (pnl != null)
            {
                HyperLink lnk = (HyperLink)pnl.FindControl("lnkEdit");
                lnk.ToolTip = "Fabrication Location is Fab Us, so Vendor Price Breaks are not relevant.";
                lnk.Enabled = false;
            }

            Initialized = true;
            return true;
        }

    }
}
 