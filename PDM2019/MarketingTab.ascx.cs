using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class MarketingTab: LRHViewer
    {


        public override bool Initialize()
        {
            if (base.Initialize()) return true;

            pnlActions.Visible = ViewRole.Equals("e") || IsAdministrator;
            lnkEdit.NavigateUrl = EditUrl("mid", ParentModuleId.ToString(), "MarketingEditor", "id", PartId.ToString(), "r", ViewRole, "e", environment, "eid", entityId);

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "Awards");
            rptAwards.DataSource = ds.Tables["Awards"];
            rptAwards.DataBind();

            Initialized = true;
            return true;
        }

    }
}
 