using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class BOMMasterEditor: LRHPopup 
    {
        protected override string PAGE_TITLE { get { return "PDM Master Part"; } }

        protected override void SetDefaultValues(DataSet ds)
        {
            bool newRecord = NewRecord(ds, "vwBillOfMaterialsInfos");
            chkToggleLabels.Checked = !newRecord;
        }

        protected override bool Validate()
        {
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            controller.ValidateDecimal(txtAssemblyCost, false, "Assembly Cost", ref msg);
            controller.ValidateDecimal(txtDutyAndFreight, false, "Duty & Freight", ref msg);
            controller.ValidateDecimal(txtTotalLandedCost, false, "Total Landed Cost", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            SaveMaster();

            CloseForm("b", "true");
        }

        public void SaveMaster()
        {
            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwBillOfMaterialsInfos");


            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            TableFields table = tables["vwBillOfMaterialsInfos"];

            if (!table.IsDirty()) return;
            table.AddField("PartId", null, PartId);
            table.AddField("ModifiedDate", null, DateTime.Now);

            if (newRecord)
            {
                controller.history.Log(PartId, this.UserInfo.Username, "Bill of Materials", string.Empty, string.Empty, "Added Bill of Materials Data");
                table.AddField("CreatedDate", null, DateTime.Now);
                controller.pdm.InsertRecord("BillOfMaterialsInfos", "PartId", table.GetParametersAsArray(true));

            }
            else
            {
                controller.history.Log(PartId, this.UserInfo.Username, "Bill of Materials", table.Fields, "ModifiedDate", "PartId");
                controller.pdm.UpdateRecord("BillOfMaterialsInfos", "PartId", PartId, table.GetParametersAsArray(true, "PartId"));
            }
        }

    }
}
 