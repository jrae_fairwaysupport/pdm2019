<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreativeTabs.ascx.cs" Inherits="YourCompany.Modules.PDM2019.CreativeTabs" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Src="CreativeTab.ascx" TagName="ctlCreative" TagPrefix="pdm" %> 
<%@ Register Src="PDPackaging.ascx" TagName="ctlPackaging" TagPrefix="pdm" %>

<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server" AutoPostBack="true" OnTabClick="LoadData"   >
        <Tabs>
            <telerik:RadTab Text="Part Information"/>
            <telerik:RadTab Text="Packaging" />
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;" >

        <telerik:RadPageView runat="server" ID="pgPartInformation" >
            <pdm:ctlCreative id="ctlPartInformation" runat="server" Editors="c-*|ad-*" Editor="CreativeEditor" />
        </telerik:RadPageView> 
    
        <telerik:RadPageView runat="server" ID="pgPackaging" >
            <pdm:ctlPackaging id="ctlPackaging" runat="server" Editors="c-*|ad-*" Editor="PDPackaging" />
        </telerik:RadPageView> 


    </telerik:RadMultiPage>
</asp:Panel>






