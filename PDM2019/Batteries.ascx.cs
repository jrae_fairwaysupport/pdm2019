using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;

namespace YourCompany.Modules.PDM2019
{
    public partial class Batteries: PortalModuleBase, IActionable
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
                currPage.Title = "Add Batteries";


                if (!IsPostBack)
                {
                }


            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion


        private int PartId
        {
            get
            {
                string raw = Request.QueryString["id"];
                int retval = -1;
                Int32.TryParse(raw, out retval);
                return retval;
            }
        }
       
        

        private string GetUserSetting(string key, string defaultValue = "")
        {
            key = "PDM." + key;
            if (DotNetNuke.Services.Personalization.Personalization.GetProfile(this.ModuleId.ToString(), key) != null)
                return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile(this.ModuleId.ToString(), key);
            else
                return defaultValue;
        }

        private void SaveUserSetting(string key, string value)
        {
            key = "PDM." + key;
            DotNetNuke.Services.Personalization.Personalization.SetProfile(this.ModuleId.ToString(), key, value);
        }

        private string environment { get { return GetUserSetting("Environment", "Production"); } }
        private string entityId { get { return GetUserSetting("EntityId", "LRH"); } }



        private bool Validate()
        {
            ProductDevelopment controller = new ProductDevelopment(entityId, environment);
            string msg = string.Empty;

            controller.ValidateText(ddlBatteryType, true, "Battery Size", ref msg);
            controller.ValidateNumber(txtQuantity, true, "Battery Quantity", ref msg);


            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void DoSave(object obj, CommandEventArgs args)
        {
            if (!Validate()) return;

            string size = ddlBatteryType.SelectedValue;
            int qty = 0;
            Int32.TryParse(txtQuantity.Text.Trim(), out qty);
            bool included = chkIncluded.Checked;


            ProductDevelopment controller = new ProductDevelopment(entityId, environment);
            controller.Battery_Add(this.UserInfo.Username, PartId, "Part", size, qty, included);

            SaveUserSetting("MainTab", "Product Development");
            SaveUserSetting("SubTab", "Specifications");
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "id", PartId.ToString()), true);
        }
    }
}
 