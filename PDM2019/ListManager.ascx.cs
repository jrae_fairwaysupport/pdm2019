using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;

namespace YourCompany.Modules.PDM2019
{
    public partial class ListManager: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM List Manager"; }}

        protected string tabName
        {
            get
            {
                try
                {
                    if (Request.QueryString["tb"] == null) return string.Empty;
                    return Request.QueryString["tb"];
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender, e);


            DotNetNuke.Framework.CDefault currPage = (DotNetNuke.Framework.CDefault)this.Page;
            string title = PAGE_TITLE;
            currPage.Title = title;
        }

        protected override DataSet LoadData()
        {
            return new DataSet(); //nothing to load
        }

        protected override void PopulateControls()
        {
            LoadLists();
            ToggleList();
            ToggleView("Main");

            btnNewList.Visible = IsAdministrator;
        }

        private void LoadLists(string listName = "")
        {
            Controller controller = new Controller(entityId, environment);
            string sql = "SELECT DISTINCT ListName as DisplayText, ListName as StoredValue FROM ListsMap WHERE TabName=@P0 ORDER BY ListName";
            DataTable dt = controller.pdm.ExecuteReader(sql, tabName);

            DataRow dx = dt.NewRow();
            dx["DisplayText"] = "Select List";
            dx["StoredValue"] = "";

            dt.Rows.InsertAt(dx, 0);

            ddlListName.DataTextField = "DisplayText";
            ddlListName.DataValueField = "StoredValue";
            ddlListName.DataSource = dt;
            ddlListName.DataBind();

            ddlListName.SelectedValue = listName;

        }

        protected void ToggleList(object obj, EventArgs args)
        {
            ToggleList();
        }

        private void ToggleList()
        {
            string listName = ddlListName.SelectedValue;
            string sql = "SELECT * FROM Lists WHERE Active = 1 AND TabName = @P0 AND ListName = @P1 ORDER BY DisplayText";
            Controller controller = new Controller(entityId, environment);
            DataTable dt = controller.pdm.ExecuteReader(sql, tabName, listName);

            rptList.DataSource = dt;
            rptList.DataBind();

            lnkNewRecord.Visible = !string.IsNullOrEmpty(ddlListName.SelectedValue);
            lbldisplay.Visible = !string.IsNullOrEmpty(ddlListName.SelectedValue);
            lblvalue.Visible = !string.IsNullOrEmpty(ddlListName.SelectedValue);

            btnEditList.Visible = (!string.IsNullOrEmpty(ddlListName.SelectedValue)) && IsAdministrator;
            btnDeleteList.Visible = (!string.IsNullOrEmpty(ddlListName.SelectedValue)) && IsAdministrator;
        }

        protected void ToggleView(object obj, CommandEventArgs args)
        {
 
            string cmd = args.CommandName;
            int recordId = -1;

            string raw = args.CommandArgument.ToString();
            Int32.TryParse(raw, out recordId);


            ToggleView(cmd, recordId);
        }

        private void ToggleView(string pnl, int recordId = -1)
        {
            pnlContainer.Visible = false;
            pnlEditor.Visible = false;
            pnlList.Visible = false;

            switch (pnl)
            {
                case "Main":
                    pnlContainer.Visible = true;
                    break;
                case "Edit":
                    EditRecord(recordId);
                    break;
                default: //EditList, NewList
                    EditList(pnl.Equals("EditList"));
                    break;
            }
           
        }

        private void EditRecord(int recordId)
        {
            pnlEditor.Visible = true;
            txtStoredValue.MaxLength = 0;

            Controller controller = new Controller(entityId, environment);

            if (recordId == -1)
            {
                lblEditAction.Text = "New List Item";
                txtDisplayText.Text = string.Empty;
                txtStoredValue.Text = string.Empty;
            }
            else
            {
                lblEditAction.Text = "Edit List Item";
                string sql = "SELECT * FROM vwLists WHERE RecordId = @P0";
                DataTable dt = controller.pdm.ExecuteReader(sql, recordId);
                if (dt.Rows.Count > 0)
                {
                    txtDisplayText.Text = controller.pdm.GetValue<string>(dt.Rows[0], "DisplayText", string.Empty);
                    txtStoredValue.Text = controller.pdm.GetValue<string>(dt.Rows[0], "StoredValue", string.Empty);
                    txtStoredValue.MaxLength = controller.pdm.GetValue<int>(dt.Rows[0], "MaxLength", 0);
                }
            }

            if (txtStoredValue.MaxLength == 0)
            {
                string sql = "SELECT * FROM vwListsLength WHERE TabName = @P0 AND ListName = @P1";
                string listName = ddlListName.SelectedValue;
                DataTable dt = controller.pdm.ExecuteReader(sql, tabName, listName);
                if (dt.Rows.Count > 0) txtStoredValue.MaxLength = controller.pdm.GetValue<int>(dt.Rows[0], "MaxLength", 300);
            }

            btnSaveRecord.CommandArgument = recordId.ToString();
        }

        protected void SaveRecord(object obj, CommandEventArgs args)
        {
            //validate both are required
            //validate both are unique
            Controller controller = new Controller(entityId, environment);
            string listName = ddlListName.SelectedValue;
            string msg = string.Empty;

            string displayText = controller.ValidateText(txtDisplayText, true, "Display Text", ref msg);
            string storedValue = controller.ValidateText(txtStoredValue, true, "Stored Value", ref msg);
            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowMessage.Value = msg;
                return;
            }

            string raw = args.CommandArgument.ToString();
            int recordId = -1;
            Int32.TryParse(raw, out recordId);

            string tmpl8 = "SELECT COUNT(*) AS THECOUNT FROM Lists WHERE TabName = @P0 AND ListName = @P1 AND Active = 1 AND {0} = @P2 AND RecordId <> @P3";
            string sql = string.Format(tmpl8, "DisplayText");
            int count = controller.pdm.FetchSingleValue<int>(sql, "THECOUNT", tabName, listName, displayText, recordId);

            if (count > 0)
            {
                hdnShowMessage.Value = "In order to dislay a unique list of items in the dropdown list boxes, each dislay text value must be unique.\r\nThe value specified has already been used.";
                return;
            }

            sql = string.Format(tmpl8, "StoredValue");
            count = controller.pdm.FetchSingleValue<int>(sql, "THECOUNT", tabName, listName, storedValue, recordId);

            if (count > 0)
            {
                hdnShowMessage.Value = "In order to distinguish values stored in the database, each stored value must be unique.\r\nThe value specified has already been used.";
                return;
            }

            if (recordId == -1)
            {
                controller.pdm.InsertRecord("Lists", "RecordId", "TabName", tabName, "ListName", listName, "DisplayText", displayText, "StoredValue", storedValue, "Active", true);
            }
            else
            {
                sql = "SELECT * FROM vwLists WHERE RecordId = @P0";
                DataTable dt = controller.pdm.ExecuteReader(sql, recordId);
                string orgValue = string.Empty;
                if (dt.Rows.Count > 0)
                {
                    orgValue = controller.pdm.GetValue<string>(dt.Rows[0], "StoredValue", string.Empty);
                    if (orgValue != storedValue)
                    {
                        string tableName = controller.pdm.GetValue<string>(dt.Rows[0], "TableName", "DUMMY");
                        string columnName = controller.pdm.GetValue<string>(dt.Rows[0], "ColumnName", "DUMMY");

                        tmpl8 = "UPDATE {0} SET {1} = @P0 WHERE {1} = @P1";
                        sql = string.Format(tmpl8, tableName, columnName);
                        controller.pdm.ExecuteCommand(sql, storedValue, orgValue);
                    }
                }

                controller.pdm.UpdateRecord("Lists", "RecordId", recordId, "DisplayText", displayText, "StoredValue", storedValue);
            }

            ToggleList();
            ToggleView("Main");
        }

        protected void DeleteRecord(object obj, CommandEventArgs args)
        {
            string raw = args.CommandArgument.ToString();
            int recordId = -1;
            Int32.TryParse(raw, out recordId);

            if (recordId != -1)
            {
                Controller controller = new Controller(entityId, environment);
                controller.pdm.UpdateRecord("Lists", "RecordId", recordId, "Active", false);

                ToggleList();
            }
            ToggleView("Main");
        }

        protected void DeleteList(object obj, CommandEventArgs args)
        {
            string listName = ddlListName.SelectedValue;

            Controller controller = new Controller(entityId, environment);
            string sql = "UPDATE Lists SET Active = 0 WHERE TabName = @P0 AND ListName = @P1";
            controller.pdm.ExecuteCommand(sql, tabName, listName);

            sql = "DELETE FROM ListsMap WHERE TabName = @P0 AND ListName = @P1";
            controller.pdm.ExecuteCommand(sql, tabName, listName);

            LoadLists();
            ToggleList();
            ToggleView("Main");
        }

        private void EditList(bool editList)
        {
            pnlList.Visible = true;
            Controller controller = new Controller(entityId, environment);

            string sql = "SELECT TABLE_NAME AS DisplayText, TABLE_NAME as StoredValue FROM INFORMATION_SCHEMA.Tables WHERE TABLE_TYPE = 'BASE TABLE' ORDER BY TABLE_NAME";
            DataTable dt = controller.pdm.ExecuteReader(sql);
            DataRow dx = dt.NewRow();
            dx["DisplayText"] = "Select Table";
            dx["StoredValue"] = "";
            dt.Rows.InsertAt(dx, 0);
            ddlListTable.DataTextField = "DisplayText";
            ddlListTable.DataValueField = "StoredValue";
            ddlListTable.DataSource = dt;
            ddlListTable.DataBind();
            

            if (editList)
            {
                lblListAction.Text = "Edit List";
                txtListName.Enabled = false;
                txtListName.Text = ddlListName.SelectedValue;

                sql = "SELECT * FROM ListsMap WHERE TabName = @P0 AND ListName = @P1";
                dt = controller.pdm.ExecuteReader(sql, tabName, ddlListName.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    ddlListTable.SelectedValue = controller.pdm.GetValue<string>(dt.Rows[0], "TableName", string.Empty);
                    string columnName = controller.pdm.GetValue<string>(dt.Rows[0], "ColumnName", string.Empty);
                    ListToggleTable(columnName);
                }
            }
            else
            {
                lblListAction.Text = "New List";
                txtListName.Enabled = true;
                txtListName.Text = string.Empty;
                ListToggleTable();
            }
        }

        protected void SaveList(object obj, CommandEventArgs args)
        {
            string msg = string.Empty;
            Controller controller = new Controller(entityId, environment);

            string listName = controller.ValidateText(txtListName, true, "List Name", ref msg);
            string tableName = controller.ValidateText(ddlListTable, true, "Table Name", ref msg);
            string columnName = controller.ValidateText(ddlListColumn, true, "Column Name", ref msg);

            if (!string.IsNullOrEmpty(msg))
            {
                hdnShowMessage.Value = "The following fields are required:\r\n" + msg;
                return;
            }

            if (lblListAction.Text.Equals("New List"))
            {
                string sql = "SELECT COUNT(*) AS THECOUNT FROM Lists WHERE Active = 1 AND TabName = @P0 AND ListName = @P1";
                int count = controller.pdm.FetchSingleValue<int>(sql, "THECOUNT", tabName, listName);
                if (count > 0)
                {
                    hdnShowMessage.Value = "The specified list name is already in use.";
                    return;
                }

                controller.pdm.InsertRecord("ListsMap", "TabName", "TabName", tabName, "ListName", listName, "TableName", tableName, "ColumnName", columnName);
                LoadLists(listName);
                ToggleList();
            }
            else
            {
                string sql = "UPDATE ListsMap SET TableName = @P0, ColumnName = @P1 WHERE TabName = @P2 AND ListName = @P3";
                controller.pdm.ExecuteCommand(sql, tableName, columnName, tabName, listName);
            }

            ToggleView("Main");
        }

        protected void ListToggleTable(object obj, EventArgs args)
        {
            ListToggleTable();
        }

        private void ListToggleTable(string columnName = "")
        {
            Controller controller = new Controller(entityId, environment);
            string sql = "SELECT COLUMN_NAME AS DisplayText, COLUMN_NAME AS StoredValue FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = @P0 ORDER BY COLUMN_NAME";
            string tableName = ddlListTable.SelectedValue;
            DataTable dt = controller.pdm.ExecuteReader(sql, tableName);

            DataRow dx = dt.NewRow();
            dx["DisplayText"] = "Select Column";
            dx["StoredValue"] = "";
            dt.Rows.InsertAt(dx, 0);

            ddlListColumn.DataTextField = "DisplayText";
            ddlListColumn.DataValueField = "StoredValue";
            ddlListColumn.DataSource = dt;
            ddlListColumn.DataBind();

            ddlListColumn.SelectedValue = columnName;
        }

        protected override bool Validate()
        {
            Controller controller = new Controller(entityId, environment);
            string msg = string.Empty;

            

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }
                

     
     
    }
}
 