<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Legal.ascx.cs" Inherits="YourCompany.Modules.PDM2019.LegalTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server" ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td class="SubSubHead">
                    <asp:Image ID="imgPatent" runat="server" Table="vwPartInfos" Field="Patent" />&nbsp;Patent
                </td>
                <td class="SubSubHead" colspan="3">
                    <asp:Image ID="imgPatentExists" runat="server" Table="vwPartInfos" Field="PatentExistsAtCreation" />&nbsp;Patent Exists at Time of Part Creation.
                </td>
            </tr>
            <tr>
                <td style="width:150px;" class="SubSubHead">
                    <asp:Image ID="Image2" runat="server" Table="vwPartInfos" Field="PatentPending" />&nbsp;Patent Pending
                </td>
                <td style="width:200px;" class="SubSubHead">
                    <asp:Image ID="Image3" runat="server" Table="vwPartInfos" Field="Trademark" />&nbsp;Trademark
                </td>
                <td style="width:200px;" class="SubSubHead">
                    <asp:Image ID="Image4" runat="server" Table="vwPartInfos" Field="RegisterTrademark" />&nbsp;Register Trademark
                </td>
                <td style="width:200px;" class="SubSubHead">
                </td>
            </tr>
    
        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />




