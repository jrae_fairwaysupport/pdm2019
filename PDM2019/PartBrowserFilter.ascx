<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PartBrowserFilter.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PartBrowserFilter" %>

<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Paging" TagPrefix="pg" Namespace="Paging" %>
<%@ Register Assembly="FilterParser" TagPrefix="fltr" Namespace="FilterParser" %>

<style type="text/css">
    .hdr  {border-left:1px black solid; border-top:1px black solid; text-align:center; font-weight:bold; background-color: #999999}
    .cell {border-left:1px black solid; border-top:1px black solid; text-align:center;}
    .brdr {border-right:1px black solid;}
    .brdb {border-bottom:1px black solid;}
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:175px; top:72px; font-size:small;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:300px; top:72px; font-size:small;}
    .suggestPreferredVendorId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:175px; top:92px; font-size:small;}
    .hdrCell {border: solid 1px black; text-align:center; vertical-align:middle; background-color:#C8C8C8; font-size:small; font-weight:bold; text-transform:uppercase;}
    .dtlCell {text-align:center; vertical-align:middle; font-size:small; }
    .small { font-size:small; }

</style> 


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");


    });


</script>

<asp:Panel ID="pnlFilter" runat="server" Width="750" style="border-bottom:solid 1px black;" >

    <telerik:RadTabStrip ID="tabs" MultiPageID="radTabs" runat="server"  >
        <Tabs>
            <telerik:RadTab Text="Part"  Selected="true" />
            <telerik:RadTab Text="Product Development"/>
            <telerik:RadTab Text="Purchasing" />
            <telerik:RadTab Text="Quality Assurance" />
            <telerik:RadTab Text="Creative" />
            <telerik:RadTab Text="Sales" />
            <telerik:RadTab Text="Marketing" />
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radTabs" runat="server" SelectedIndex="0" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" style="padding:4px;">

        <telerik:RadPageView runat="server" ID="pgPart" >
            <asp:Panel id="pnlPart" runat="server" Width="725" Height="300">
                <table width="700" cellpadding="2" cellspacing="0" border="0">
                    <tr>
                        <td class="SubSubHead" style="width:125px;" valign="top">
                            <asp:Label ID="lbl" runat="server" Text="Company:" />
                        </td>
                        <td class="Normal" style="width:225px;" valign="top">
                            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="Box"  FieldName="Company" Quotes="true" Range="False" Width="180" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                            </asp:DropDownList>
                        </td>
                        <td class="SubSubHead" style="width:125px;" valign="top">
                            <asp:Label ID="lblpart" runat="server" Text="Part Id:" />
                        </td>
                        <td class="Normal" style="width:225px;" valign="top">
                            <asp:TextBox ID="txtPartId" runat="server" CssClass="Box" FieldName="PartId" Quotes="true" Range="False" Width="180" />
                            <asp:CheckBox ID="chkPartId" runat="server" FieldName="PartId" WildCard="true" />
                     </tr>

                    <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="lblABCCode" runat="server" Text="ABC Code:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox4" runat="server" CssClass="Box" FieldName="ABCCode" Quotes="true" Range="False" Width="180" />
                            <asp:CheckBox ID="CheckBox10" runat="server" FieldName="ABCCode" WildCard="true" />
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label3" runat="server" Text="Status:" Visible="false" />
                        </td>
                        <td class="Normal" style="width:225px;" valign="top">
                            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="Box"  FieldName="Status" Quotes="true" Range="False" Width="180" Visible="false" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Approved" Value="Approved" />
                                <asp:ListItem Text="Draft" Value="Draft" />
                                <asp:ListItem Text="Pending" Value="Pending" />
                                <asp:ListItem Text="Rejected" Value="Rejected" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label4" runat="server" Text="UPC Code:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox5" runat="server" CssClass="Box" FieldName="UPCCode" Quotes="true" Range="False" Width="180" />
                            <asp:CheckBox ID="CheckBox11" runat="server" FieldName="UPCCode" WildCard="true" />
                        </td>
                        <td class="SubSubHead" valign="top">
                            &nbsp;
                        </td>
                        <td class="Normal" style="width:225px;" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label7" runat="server" Text="Description:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox8" runat="server" CssClass="Box" FieldName="Description" Quotes="true" Range="False" Width="180" />
                            <asp:CheckBox ID="CheckBox14" runat="server" FieldName="Description" WildCard="true" Visible="false" Checked="true" />
                        </td>
                        <td class="SubSubHead" valign="top">
                            <%--<asp:Label ID="Label8" runat="server" Text="Extended:" />--%>
                        </td>
                        <td class="Normal" valign="top">
<%--                            <asp:TextBox ID="TextBox9" runat="server" CssClass="Box" FieldName="ExtendedDescription" Quotes="true" Range="False" Width="180" />
                            <asp:CheckBox ID="CheckBox15" runat="server" FieldName="ExtendedDescription" WildCard="true" Visible="false" Checked="true" />
--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label1" runat="server" Text="Submitted By:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox1" runat="server" CssClass="Box" FieldName="SubmitUserId" Quotes="true" Range="False" Width="180" />
                            <asp:CheckBox ID="CheckBox1" runat="server" FieldName="SubmitUserId" WildCard="true" />
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label2" runat="server" Text="Rejected By:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox2" runat="server" CssClass="Box" FieldName="RejectUserId" Quotes="true" Range="False" Width="180" />
                            <asp:CheckBox ID="CheckBox2" runat="server" FieldName="RejectUserId" WildCard="true"  />
                        </td>
                    </tr>


                </table>

            </asp:Panel>
        </telerik:RadPageView>


        <telerik:RadPageView runat="server" ID="pgProductDevelopment" >
            <asp:Panel id="pnlProductDevelopment" runat="server" Width="725" Height="300">
                <table width="700" cellpadding="2" cellspacing="0" border="0">
                    <tr>
                        <td class="SubSubHead" style="width:175px;" valign="top">
                            <asp:Label ID="Label5" runat="server" Text="Parent Part:" />
                        </td>
                        <td class="Normal" style="width:175px;" valign="top">
                            <DNN:DNNTextSuggest ID="txtParentPart" runat="server" 
                                    onpopulateondemand="DoLookup" MaxSuggestRows="10"
                                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggestParentPartId"
                                    FieldName="ParentPartId" Quotes="true" NameIDSplit="-" Width="140"
                            />

                        </td>
                        <td class="SubSubHead" style="width:175px;" valign="top">
                            <asp:Label ID="Label6" runat="server" Text="Preferred Vendor:" />
                        </td>
                        <td class="Normal" style="width:175px;" valign="top">
                            <DNN:DNNTextSuggest ID="txtPreferredVendor" runat="server" 
                                    onpopulateondemand="DoLookup" MaxSuggestRows="10"
                                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggestPreferredVendor"
                                    FieldName="PreferredVendor" Quotes="true" NameIDSplit="-" Width="140"
                            />
                        </td>
                     </tr>

                     <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="lblyearint" runat="server" Text="Year Introduced:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="txtyearintroduced" runat="server" CssClass="Box" FieldName="YearIntroduced" Quotes="false" Range="False" Width="140" />
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="lblbuyer" runat="server" Text="Buyer User Id:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="txtBuyer" runat="server" CssClass="Box" FieldName="BuyerUserId" Quotes="true" Range="false" Width="130" />
                            <asp:CheckBox ID="chkBuyer" runat="server" FieldName="BuyerUserId" WildCard="true" />
                        </td>
                     </tr>

                    <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label9" runat="server" Text="First Forecast:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox3" runat="server" CssClass="Box" FieldName="FirstYearForecast" Quotes="false" Range="False" Width="140" />
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="lblFabricated" runat="server" Text="Fabricated:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="Box"  FieldName="Fabricated" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>

                        </td>
                     </tr>

                    <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label11" runat="server" Text="Multilingual Product:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList4" runat="server" CssClass="Box"  FieldName="MultilingualProduct" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label12" runat="server" Text="Purchased:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList3" runat="server" CssClass="Box"  FieldName="Purchased" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>

                        </td>
                     </tr>

                      <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label13" runat="server" Text="English:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList5" runat="server" CssClass="Box"  FieldName="English" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label14" runat="server" Text="Royalty Item:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList6" runat="server" CssClass="Box"  FieldName="RoyaltyItem" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>

                        </td>
                     </tr>

                      <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label15" runat="server" Text="Spanish:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList7" runat="server" CssClass="Box"  FieldName="Spanish" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label16" runat="server" Text="Patent:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList8" runat="server" CssClass="Box"  FieldName="Patent" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>

                        </td>
                     </tr>

                      <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label17" runat="server" Text="German:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList9" runat="server" CssClass="Box"  FieldName="German" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label18" runat="server" Text="Patent Pending:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList10" runat="server" CssClass="Box"  FieldName="PatentPending" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>

                        </td>
                     </tr>

                      <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label19" runat="server" Text="French:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList11" runat="server" CssClass="Box"  FieldName="French" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label20" runat="server" Text="Trademark:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList12" runat="server" CssClass="Box"  FieldName="Trademark" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>

                        </td>
                     </tr>

                  <tr>
                        <td class="SubSubHead" valign="top">
                            &nbsp;
                        </td>
                        <td class="Normal" valign="top">
                            &nbsp;
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label22" runat="server" Text="Register Trademark:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:DropDownList ID="DropDownList14" runat="server" CssClass="Box"  FieldName="RegisterTrademark" Quotes="false" Range="False" Width="165" >
                                <asp:ListItem Text="" Value="" Selected="True" />
                                <asp:ListItem Text="Yes" Value="1" />
                                <asp:ListItem Text="No" Value="0" />
                            </asp:DropDownList>

                        </td>
                     </tr>

                </table>
            </asp:Panel>
        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="pgPurchasing" >
            <asp:Panel id="pnlPurchasing" runat="server" Width="725" Height="300">
               <table width="700" cellpadding="2" cellspacing="0" border="0">
                     <tr>
                        <td class="SubSubHead" style="width:175px;" valign="top">
                            <asp:Label ID="Label38" runat="server" Text="Planner User Id:" />
                        </td>
                        <td class="Normal" style="width:175px;" valign="top">
                            <asp:TextBox ID="TextBox11" runat="server" CssClass="Box" FieldName="PlannerUserId" Quotes="true" Range="False" Width="130" />
                            <asp:CheckBox ID="chkplanner" runat="server" FieldName="PlannerUserId" WildCard="true" />
                        </td>
                        <td class="SubSubHead" style="width:175px;" valign="top">
                            <asp:Label ID="Label39" runat="server" Text="Buyer User Id:" />
                        </td>
                        <td class="Normal" style="width:175px;" valign="top">
                            <asp:TextBox ID="TextBox12" runat="server" CssClass="Box" FieldName="PurchasingBuyerUserId" Quotes="true" Range="false" Width="130" />
                            <asp:CheckBox ID="CheckBox4" runat="server" FieldName="PurchasingBuyerUserId" WildCard="true" />
                        </td>
                    </tr>
                    <tr>
                        <td class="SubSubHead" style="width:175px;" valign="top">
                            <asp:Label ID="Label10" runat="server" Text="Preferred Vendor:" />
                        </td>
                        <td class="Normal" style="width:175px;" valign="top">
                            <DNN:DNNTextSuggest ID="txtPreferredVendorId" runat="server" 
                                    onpopulateondemand="DoLookup" MaxSuggestRows="10"
                                    CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggestPreferredVendorId"
                                    FieldName="PreferredVendorId" Quotes="true" NameIDSplit="-" Width="140"
                            />

                        </td>
                        <td class="SubSubHead" style="width:175px;" valign="top">
                            <asp:Label ID="Label21" runat="server" Text="HTS Code:" />
                        </td>
                        <td class="Normal" style="width:175px;" valign="top">
                            <asp:TextBox ID="TextBox13" runat="server" CssClass="Box" FieldName="HTSCode" Quotes="true" Range="false" Width="130" />
                            <asp:CheckBox ID="CheckBox5" runat="server" FieldName="HTSCode" WildCard="true" />
                        </td>
                     </tr>

                     <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label23" runat="server" Text="Material Code:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox6" runat="server" CssClass="Box" FieldName="MaterialCode" Quotes="true" Range="false" Width="130" />
                            <asp:CheckBox ID="CheckBox6" runat="server" FieldName="MaterialCode" WildCard="true" />
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label24" runat="server" Text="Primary Warehouse:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox7" runat="server" CssClass="Box" FieldName="PrimaryWarehouse" Quotes="true" Range="false" Width="130" />
                            <asp:CheckBox ID="CheckBox3" runat="server" FieldName="PrimaryWarehouse" WildCard="true" />
                        </td>
                     </tr>

                    <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label25" runat="server" Text="Country of Origin:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox10" runat="server" CssClass="Box" FieldName="CountryOfOrigin" Quotes="true" Range="false" Width="130" />
                            <asp:CheckBox ID="CheckBox7" runat="server" FieldName="CountryOfOrigin" WildCard="true" />

                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label26" runat="server" Text="Port of Origin:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox14" runat="server" CssClass="Box" FieldName="PortOfOrigin" Quotes="true" Range="false" Width="130" />
                            <asp:CheckBox ID="CheckBox8" runat="server" FieldName="PortOfOrigin" WildCard="true" />
                        </td>
                     </tr>


                </table>
            </asp:Panel>
        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="pgQuality" >
            <asp:Panel id="pnlQuality" runat="server" Width="725" Height="300">
	            Need Filters?
            </asp:Panel>
        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="pgCreative" >
            <asp:Panel id="pnlCreative" runat="server" Width="725" Height="300">
	            Need Filters?
            </asp:Panel>
        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="pgSales" >
            <asp:Panel id="pnlSales" runat="server" Width="725" Height="300">
	            Need Filters?
            </asp:Panel>
        </telerik:RadPageView>

        <telerik:RadPageView runat="server" ID="pgMarketing" >
            <asp:Panel id="pnlMarketing" runat="server" Width="725" Height="300">
                <table width="700" cellpadding="2" cellspacing="0" border="0">
                    <tr>
                        <td class="SubSubHead" style="width:175px;" valign="top">
                            <asp:Label ID="Label27" runat="server" Text="Web Part #:" />
                        </td>
                        <td class="Normal" style="width:175px;" valign="top">
                            <asp:TextBox ID="TextBox15" runat="server" CssClass="Box" FieldName="WebPartNumber" Quotes="true" Range="False" Width="130" />
                            <asp:CheckBox ID="CheckBox9" runat="server" FieldName="WebPartNumber" WildCard="true" />
                        </td>
                        <td class="SubSubHead" style="width:175px;" valign="top">
                            &nbsp;
                        </td>
                        <td class="Normal" style="width:175px;" valign="top">
                            &nbsp;
                        </td>
                   </tr>
                    <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label28" runat="server" Text="Product Category 1:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox16" runat="server" CssClass="Box" FieldName="ProductCategory1" Quotes="true" Range="False" Width="130" />
                            <asp:CheckBox ID="CheckBox12" runat="server" FieldName="ProductCategory1" WildCard="true" />
                        </td>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label29" runat="server" Text="Product Category 2:" />
                        </td>
                        <td class="Normal"  valign="top">
                            <asp:TextBox ID="TextBox17" runat="server" CssClass="Box" FieldName="ProductCategory2" Quotes="true" Range="False" Width="130" />
                            <asp:CheckBox ID="CheckBox13" runat="server" FieldName="ProductCategory2" WildCard="true" />
                        </td>
                   </tr>
                    <tr>
                        <td class="SubSubHead" valign="top">
                            <asp:Label ID="Label30" runat="server" Text="Product Category 3:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox18" runat="server" CssClass="Box" FieldName="ProductCategory3" Quotes="true" Range="False" Width="130" />
                            <asp:CheckBox ID="CheckBox16" runat="server" FieldName="ProductCategory3" WildCard="true" />
                        </td>
                        <td class="SubSubHead"  valign="top">
                            <asp:Label ID="Label31" runat="server" Text="Product Category 4:" />
                        </td>
                        <td class="Normal" valign="top">
                            <asp:TextBox ID="TextBox19" runat="server" CssClass="Box" FieldName="ProductCategory4" Quotes="true" Range="False" Width="130" />
                            <asp:CheckBox ID="CheckBox17" runat="server" FieldName="ProductCategory4" WildCard="true" />
                        </td>
                   </tr>
                   <tr>
                    <td class="SubSubHead" valign="top">
                        <asp:Label ID="lblCopy" runat="server" Text="Copy:" />
                    </td>
                    <td class="Normal" colspan="3" valign="top">
                        <asp:TextBox ID="txtCopy" runat="server" CssClass="Box" FieldName="Copy" Quotes="true" Range="False" Width="485" />
                        <asp:CheckBox ID="chkCopy" runat="server" FieldName="Copy" WildCard="true" />
                    </td>
                   </tr>
                   <tr>
                    <td class="SubSubHead" valign="top">
                        <asp:Label ID="Label32" runat="server" Text="Web Part Description:" />
                    </td>
                    <td class="Normal" colspan="3" valign="top">
                        <asp:TextBox ID="TextBox20" runat="server" CssClass="Box" FieldName="WebPartDescription" Quotes="true" Range="False" Width="485" />
                        <asp:CheckBox ID="CheckBox18" runat="server" FieldName="WebPartDescription" WildCard="true" />
                    </td>
                   </tr>
                   <tr>
                    <td class="SubSubHead" valign="top">
                        <asp:Label ID="Label33" runat="server" Text="Web Long Description:" />
                    </td>
                    <td class="Normal" colspan="3" valign="top">
                        <asp:TextBox ID="TextBox21" runat="server" CssClass="Box" FieldName="WebLongDescription" Quotes="true" Range="False" Width="485" />
                        <asp:CheckBox ID="CheckBox19" runat="server" FieldName="WebLongDescription" WildCard="true" />
                    </td>
                   </tr>
                   <tr>
                    <td class="SubSubHead" valign="top">
                        <asp:Label ID="Label34" runat="server" Text="Web Short Description:" />
                    </td>
                    <td class="Normal" colspan="3" valign="top">
                        <asp:TextBox ID="TextBox22" runat="server" CssClass="Box" FieldName="WebShortDescription" Quotes="true" Range="False" Width="485" />
                        <asp:CheckBox ID="CheckBox20" runat="server" FieldName="WebShortDescription" WildCard="true" />
                    </td>
                   </tr>
                   <tr>
                    <td class="SubSubHead" valign="top">
                        <asp:Label ID="Label35" runat="server" Text="Keywords:" />
                    </td>
                    <td class="Normal" colspan="3" valign="top">
                        <asp:TextBox ID="TextBox23" runat="server" CssClass="Box" FieldName="Keywords" Quotes="true" Range="False" Width="485" />
                        <asp:CheckBox ID="CheckBox21" runat="server" FieldName="Keywords" WildCard="true" />
                    </td>
                   </tr>

                </table>
	           
            </asp:Panel>
        </telerik:RadPageView>

    </telerik:RadMultiPage>
    
</asp:Panel>


<table width="725" cellpadding="4" cellspacing="0">
    <tr>
        <td align="left" valign="bottom" class="Normal" style="width:20%;">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" valign="bottom" class="Normal" style="width:20%;">
            <asp:Button ID="btnClear" runat="server" Text="Clear Filter" OnCommand="DoClear" />
        </td>
        <td align="right" valign="bottom" class="Normal" style="width:20%;" >
            <asp:Button ID="btnTest" runat="server" Text="Record Count" OnCommand="DoTest" />
        </td>
        <td align="left" valign="bottom" class="Normal" style="width:20%;">
            <asp:Label ID="lblRecords" runat="server" />
        </td>
        <td align="right" valign="bottom" class="Normal" style="width:20%;">
            <asp:Button ID="btnApply" runat="server" Text="Apply Filter" OnCommand="DoApply" />
        </td>
    </tr>
</table>


<fltr:FilterParser id="Filter" runat="server" ParserFor="pnlFilter" FilterName="PDMFilter" />
<asp:HiddenField ID="hdnShowMessage" runat="server" Value="" />

