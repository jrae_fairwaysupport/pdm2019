<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDPartInformation.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PDPartInformationTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"><span class="actionButton"  >Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPartInformation" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead">Company:</td>
                <td style="width:600px;" colspan="3"><asp:Label id="lblCompany" runat="server" CssClass="Normal required placeholder" Width="580" Table="vwPartInfos" Field="CompanyName" />
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgIsNewPart" runat="server" Table="vwPartInfos" Field="IsNewPart" />&nbsp;New Part</td>
                <td class="SubSubHead"><asp:Image ID="imgRevision" runat="server" Table="vwPartInfos" Field="IsVersionOfExistingPart" />&nbsp;Is Version of Existing Part</td>
                <td class="SubSubHead">Master Part Id:</td>
                <td ><asp:Label ID="lblParentPartId" runat="server" CssClass="Normal required placeholder" Width="180" Table="vwPartInfos" Field="ParentPartSuggest" />
            </tr>
            <tr>
                <td class="SubSubHead">Buyer User Id:</td>
                <td ><asp:Label ID="lblbuyeruserid" runat="server" CssClass="Normal placeholder" Width="180" Table="vwPartInfos" Field="BuyerUserId" /></td>
                <td colspan="2" />
            </tr>
            <tr>
                <td class="SubSubHead">Preferred Vendor:</td>
                <td colspan="3"><asp:Label ID="lblpreferredvendor" runat="server" CssClass="Normal required placeholder" Width="580" Table="vwPartInfos" Field="PreferredVendorSuggest" /></td>
            </tr>
            
            <tr>
                <td class="SubSubHead"><asp:Image ID="chkPurchasedOrFabricated" runat="server" ImageUrl="~/images/checked.gif" />&nbsp;&nbsp;<asp:Label ID="lblPurchasedOrFabricated" runat="server" Table="vwPartInfos" Field="FabricatedOrPurchased" /></td>
                <td class="SubSubHead"><asp:Label ID="lblFabricationLocationLabel" runat="server" Text="Fabrication Location:" /></td>
                <td class="Normal"><asp:Label ID="lblFabricationLocationValue" runat="server" CssClass="Normal placeholder required" Width="180" Table="vwPartInfos" Field="FabricationLocationText" /></td>
                <td class="SubSubHead"><asp:Image ID="imgOpenMarketItem" runat="server" Table="vwPartInfos" Field="IsOpenMarketItem" />&nbsp;Open Market Item</td>
            </tr>

            <tr>
                <td />
                <td class="SubSubHead"><asp:Label ID="lblUnitAssemblyCost" runat="server" Text="Unit Assenbly Cost:" /></td>
                <td ><asp:Label ID="lblUnitAssemblyCostValue" runat="server" CssClass="Normal placeholder required" Width="180" Table="vwPartInfos" Field="UnitAssemblyCostText" /></td>
                <td />
            </tr>
           

            <tr>
                <td class="SubSubHead">Base Material:</td>
                <td colspan="3"><asp:Label ID="lblBaseMaterial" runat="server" CssClass="Normal placeholder" Width="580" Table="vwPartInfos" Field="BaseMaterial" /> </td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Label ID="lblMaterialCostLabel" runat="server" Text="Material Cost:" /></td>                        
                <td><asp:Label ID="lblMaterialcost" runat="server" CssClass="Normal placeholder required" style="text-align:right;" Width="100" Table="vwPartInfos" Field="MaterialCostText" /></td>
                <td class="SubSubHead"></td>
                <td></td>
            </tr>
            <tr>
                <td class="SubSubHead">Selling / Net Price:</td>
                <td><asp:Label ID="lblnetprice" runat="server" CssClass="Normal placeholder" Width="100" style="text-align:right;" Table="vwPartInfos" Field="NetPriceText" /> </td>
                <td class="SubSubHead">Selling / Retail Price:</td>
                <td><asp:Label ID="Label1" runat="server" CssClass="Normal placeholder" Width="100" style="text-align:right;" Table="vwPartInfos" Field="RetailPriceText" /> </td>
            </tr>
            <tr>
                <td class="SubSubHead">Lead Time (Days):</td>
                <td colspan="3"><asp:Label ID="Label5" runat="server" CssClass="Normal placeholder required" Width="100" Table="vwPartInfos" Field="LeadTime" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Minimum Order Qty:</td>                        
                <td><asp:Label ID="Label6" runat="server" CssClass="Normal placeholder required" Width="100" Table="vwPartInfos" Field="MinimumOrderQuantity" /></td>
                <td class="SubSubHead">Multiples Of:</td>
                <td><asp:Label ID="Label7" runat="server" CssClass="Normal placeholder" Width="100" Table="vwPartInfos" Field="MultiplesOf" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Year Introduced:</td>                        
                <td><asp:Label ID="Label8" runat="server" CssClass="Normal placeholder required" Width="100" Table="vwPartInfos" Field="YearIntroducedText" /></td>
                <td class="SubSubHead">First Year Forecast:</td>
                <td><asp:Label ID="Label9" runat="server" CssClass="Normal placeholder required" Width="100" Table="vwPartInfos" Field="FirstYearForecast" /></td>
            </tr>
            <tr>
                <td class="SubSubHead" valign="top"><asp:Image ID="Image1" runat="server" Table="vwPartInfos" Field="RoyaltyItem" />&nbsp;Royalty Item</td>
                <td colspan="3" style="padding: 0 0 0 0;" >
                    <table width="100%" cellpadding="4" cellspacing="0" border="0" id="tblRoyalty" runat="server">
                        <tr>
                            <td style="width:100px;" class="SubSubHead">Payable To:</td>
                            <td style="width:500px;" colspan="4"><asp:Label ID="lblPayableTo" runat="server" Width="480" CssClass="Normal placeholder" Table="vwPartInfos" Field="RoyaltyPayableTo" /> </td>
                        </tr>
                        <tr>
                            <td class="SubSubHead" style="width:125px;">Royalty Type:</td>
                            <td style="width:125px;" ><asp:Label id="lblRoyaltyType" runat="server" Width="105" CssClass="Normal placeholder" Table="vwPartInfos" Field="RoyaltyType" /></td>
                            <td style="width:125px;" class="SubSubHead"><asp:Label ID="lblRoyaltyAmountText" runat="server" Width="105" /></td>
                            <td style="width:125px;"><asp:Label id="lblRoyaltyAmountValue" runat="server" CssClass="Normal placeholder" Width="105" /></td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td class="SubSubHead"><asp:Image ID="imgmold" runat="server" Table="vwPartInfos" Field="MoldShare" />&nbsp;Mold Share</td>
                <td class="SubSubHead" colspan="3">
                    <asp:Label ID="lblVendormolde" runat="server" Width="100" Text="Vendor:&nbsp;" />
                    <asp:Label ID="lblbmoldvendor" runat="server" CssClass="Normal placeholder" Width="480" Table="vwPartInfos" Field="MoldShareVendorSuggest" />
                </td>
            </tr>
            
            <tr>
                <td class="SubSubHead">Target Age (Min):</td>
                <td><asp:Label ID="lblTargetAgeMin" runat="server" CssClass="Normal placeholder required" Width="100" Table="vwPartInfos" Field="MinAge" Format="Decimal" Places="1"/></td>
                <td class="SubSubHead">Target Age (Max):</td>
                <td><asp:Label ID="lblTargetAgeMax" runat="server" CssClass="Normal placeholder required" Width="100" Table="vwPartInfos" Field="MaxAge" Format="Decimal" Places="1" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Target Grade (Min):</td>
                <td><asp:Label ID="Label15" runat="server" CssClass="Normal placeholder" Width="100" Table="vwPartInfos" Field="MinGrade" /></td>
                <td class="SubSubHead">Target Age (Max):</td>
                <td><asp:Label ID="Label16" runat="server" CssClass="Normal placeholder" Width="100" Table="vwPartInfos" Field="MaxGrade" /></td>
            </tr>

        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" Value="false" runat="server" />




