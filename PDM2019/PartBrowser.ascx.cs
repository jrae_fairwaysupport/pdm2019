using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using FilterParser;

namespace YourCompany.Modules.PDM2019
{
    public partial class PartBrowser : PortalModuleBase, IActionable
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                this.Paging.Navigate += new EventHandler(Paging_Navigated);
                this.Paging.ClearFilter += new EventHandler(Paging_ClearFilter);
                this.Paging.ExportFilter += new EventHandler(Paging_Export);
                Paging.FilterUrl = EditUrl("Filter").Replace("550,950", string.Format("{0},{1}", filterHeight, filterWidth));

                Paging.ShowExcel = true;
                Paging.ShowNavigator = true;
                Paging.ShowPageSize = true;
                Paging.ShowCurrentPage = true;
                

                if (!IsPostBack)
                {

                    Environment = GetUserSetting("Environment", "Production");
                    //Environment = "Development";
                    EntityId = GetUserSetting("EntityId", "LRH");
                    if (!LockDownControls()) return;
                    SetDefaultView();
                    LoadData();

                    if (!string.IsNullOrEmpty(Request.QueryString["new"]))
                    {
                        bool newPart = Boolean.Parse(Request.QueryString["new"]);
                        if (newPart)
                        {
                            string id = "0";
                            if (!string.IsNullOrEmpty(Request.QueryString["id"])) id = Request.QueryString["id"];
                            hdnUrl.Value = PartEditorUrl(id);
                        }
                    }
                }

                SetFilterUrl();
                
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        private void FilterWarning()
        {
            bool filtered = false;

            try
            {
                List<FilterCriteria> filters = Filter.GetCriteria();
                foreach (FilterCriteria crt in filters)
                {
                    if (!crt.FieldName.Equals("Status"))
                    {
                        filtered = true;
                        break;
                    }
                }
            }
            catch { }
            lblFilterWarning.Visible = filtered;
        }

        #region IActionable Members

        public DotNetNuke.Entities.Modules.Actions.ModuleActionCollection ModuleActions
        {
            get
            {
                //create a new action to add an item, this will be added to the controls
                //dropdown menu
                ModuleActionCollection actions = new ModuleActionCollection();
                actions.Add(GetNextActionID(), Localization.GetString(ModuleActionType.AddContent, this.LocalResourceFile),
                    ModuleActionType.AddContent, "", "", EditUrl(), false, DotNetNuke.Security.SecurityAccessLevel.Edit,
                     true, false);

                return actions;
            }
        }

        #endregion

        protected void ToggleRole(object obj, EventArgs args)
        {
            SaveUserSetting("View Role", ddlRole.SelectedValue);
            SetDefaultView();

            Paging.CurrentPage = 1;//PageNumber = 1;
            Paging.PageCount = 0; //PageCount = 0;

            //Filter.ClearFilter();
            //Filter.AddFilterCriteria("Environment", environment, true, true);
            //Filter.SaveFilter();

            //Paging2.CurrentPage = 1;
            Paging.PageCount = 0;
            LoadData();
        }

        private void SetDefaultView()
        {
            string stored = GetUserSetting("Part View", "");
            if (!string.IsNullOrEmpty(stored))
            {
                rdoStatus.SelectedValue = stored;
                ToggleView();
                return;
            }

            if (ddlRole.SelectedValue.ToString().Equals("Product Development"))
            {
                rdoStatus.SelectedValue = "Draft";
            }
            else
            {
                rdoStatus.SelectedValue = "Approved";
            }
            ToggleView();
            SaveUserSetting("Part View", rdoStatus.SelectedValue);
        }

        private bool LockDownControls(bool skipRadios = false)
        {
            ddlRole.Items.Clear();
            bool isAdmin = this.UserInfo.IsSuperUser || this.UserInfo.IsInRole("Administrators");
            bool isLRH = isAdmin || CanAccessEntityData("LRH");
            bool isH2M = isAdmin || CanAccessEntityData("H2M");

            if (!skipRadios)
            {
                if (!isAdmin)
                {
                    rdoEnvironment.SelectedValue = "Production";
                    rdoEnvironment.Enabled = false;
                    rdoEnvironment.Visible = false;
                }

                if (isLRH && !isH2M)
                {
                    rdoEntity.SelectedValue = "LRH";
                    rdoEntity.Enabled = false;
                    rdoEntity.Visible = false;
                }
                else if (isH2M && !isLRH)
                {
                    rdoEntity.SelectedValue = "H2M";
                    rdoEntity.Enabled = false;
                    rdoEntity.Visible = false;
                }
            }

            LockDownControls("Product Development", "Pending", isAdmin);
            LockDownControls("Purchasing", "Draft|Rejected", isAdmin);
            LockDownControls("Quality Assurance", "Draft|Pending|Rejected", isAdmin);
            LockDownControls("Creative", "Draft|Pending|Rejected", isAdmin);
            LockDownControls("Sales", "Draft|Pending|Rejected", isAdmin);
            LockDownControls("E-Commerce/Marketing", "Draft|Pending|Rejected", isAdmin, "Marketing");
            //if (EntityId.Equals("LRH")) LockDownControls("Legal", "Draft|Pending|Rejected", isAdmin);
            if (EntityId.Equals("LRH")) LockDownControls("Accounting", "Draft|Pending|Rejected", isAdmin);
            LockDownControls("View Only", "Draft|Pending|Rejected", isAdmin, "Viewer");
            LockDownControls("Administrator", string.Empty, isAdmin);

            if (ddlRole.Items.Count == 0) return HideAllControls();

            try
            {
                ddlRole.SelectedValue = GetUserSetting("View Role", "");
            }
            catch
            {
                ddlRole.SelectedIndex = 0;
            }

            return true;
        }

        private bool HideAllControls()
        {
            pnlMain.Visible = false;
            hdnShowMessage.Value = "You have not yet been assigned a PDM role.\r\nPlease open a Help Desk ticket and specify which role you need assigned to your profile.";
            return false;
        }

        private bool CanAccessEntityData(string entity)
        {
            List<string> roles = new List<string> { "Product Development", "Purchasing", "Quality Assurance",
                                                    "Creative", "Sales", "Marketing", "Account", "Viewer" }; //"Legal", 
            
            foreach (string role in roles)
            {
                string roleName = string.Format("PDM {0} {1}", entity, role);
                if (this.UserInfo.IsInRole(roleName)) return true;
            }
            return false;
        }

        private void LockDownControls(string text, string hideDelete, bool isAdmin, string role = "")
        {
            string value = string.IsNullOrEmpty(role) ? text : role;
            if (string.IsNullOrEmpty(role)) role = text;
            string test = string.Format("PDM {0} {1}", EntityId, role);
            if (isAdmin || this.UserInfo.IsInRole(test))
            {
                ListItem item = new ListItem(text, role);
                item.Attributes.Add("HideDelete", hideDelete);
                ddlRole.Items.Add(item);
            }
        }

  
        //private void LockDownControls()
        //{

        //    ddlRole.SelectedValue = GetUserSetting("View Role", "");

        //    bool isAdmin = this.UserInfo.IsSuperUser || this.UserInfo.IsInRole("Administrators");
        //    if (isAdmin) return;

        //    ddlRole.Items.Remove("Administrator");
        //    if (!this.UserInfo.IsInRole("PDM Product Development")) ddlRole.Items.Remove("Product Development");
        //    if (!this.UserInfo.IsInRole("PDM Purchasing")) ddlRole.Items.Remove("Purchasing");
        //    if (!this.UserInfo.IsInRole("PDM Quality Assurance")) ddlRole.Items.Remove("Quality Assurance");
        //    if (!this.UserInfo.IsInRole("PDM Creative")) ddlRole.Items.Remove("Creative");
        //    if (!this.UserInfo.IsInRole("PDM Sales")) ddlRole.Items.Remove("Sales");
        //    if (!this.UserInfo.IsInRole("PDM Marketing")) ddlRole.Items.Remove("E-Commerce/Marketing");
        //    if (!this.UserInfo.IsInRole("PDM Legal")) ddlRole.Items.Remove("Legal");
        //    if (!this.UserInfo.IsInRole("PDM Accounting")) ddlRole.Items.Remove("Accounting");
        //    ddlRole.SelectedValue = GetUserSetting("View Role", "");

        //}


        protected void Paging_Navigated(object obj, EventArgs args)
        {
            LoadData();

        }

        protected void Paging2_Navigated(object obj, EventArgs args)
        {
            //Paging.PageSize = Paging2.PageSize;
            //Paging.CurrentPage = Paging2.CurrentPage;
            //Paging.PageCount = Paging2.PageCount;
            LoadData();

        }

        protected void Paging_Export(object obj, EventArgs args)
        {
            Filter.LoadFilter();
            Filter.BaseQuery = "SELECT * FROM vwPartList";
            //Filter.Export(environment, "B2B2017", true);
            Filter.Export(Environment, EntityId + "_PDM", true);
        }

        private void Paging_ClearFilter(object obj, EventArgs args)
        {
            Paging.CurrentPage = 1;//PageNumber = 1;
            Paging.PageCount = 0; //PageCount = 0;

            Filter.ClearFilter();
            //Filter.AddFilterCriteria("Environment", environment, true, true);
            Filter.SaveFilter();

            //Paging2.CurrentPage = 1;
            Paging.PageCount = 0;

            LoadData();


        }

        protected void DoSort(object obj, CommandEventArgs args)
        {
            LinkButton btn = (LinkButton)obj;
            string sortOrder = args.CommandName.ToString();
            string sortField = args.CommandArgument.ToString();

            if (sortOrder.Equals("ASC"))
                btn.CommandName = "DESC";
            else
                btn.CommandName = "ASC";
            OrderBy = string.Format("{0} {1}", sortField, sortOrder);
            LoadData();
        }

        protected string OrderBy
        {
            get
            {
                return GetUserSetting("OrderBy", "PartId ASC");
            }
            set
            {
                SaveUserSetting("OrderBy", value);
            }
        }



        private string GetUserSetting(string key, string defaultValue = "")
        {
            if (DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key) != null)
                return (string)DotNetNuke.Services.Personalization.Personalization.GetProfile("PDM", key);
            else
                return defaultValue;
        }

        private void SaveUserSetting(string key, string value)
        {
            DotNetNuke.Services.Personalization.Personalization.SetProfile("PDM", key, value);
        }


        protected void ToggleEntity(object obj, EventArgs args)
        {
            LockDownControls(true);
            ToggleEnvironment();
        }

        protected void ToggleEnvironment(object obj, EventArgs args)
        {
            ToggleEnvironment();
        }

        private void ToggleEnvironment()
        {
            Environment = rdoEnvironment.SelectedValue;
            EntityId = rdoEntity.SelectedValue;
            LoadData();

            //RadioButtonList rdo = (RadioButtonList)obj;
            //if (rdo.ID == "rdoEntity" && EntityId.Equals("H2M")) hdnShowMessage.Value = "PDM has not yet been implemented for Hand2Mind.";
        }

        protected string Environment
        {
            get
            {
                return rdoEnvironment.SelectedValue;
            }

            set
            {
                SaveUserSetting("Environment", value);
                rdoEnvironment.SelectedValue = value;
            }
        }

        protected string EntityId
        {
            get
            {
                return rdoEntity.SelectedValue;
            }
            set
            {
                SaveUserSetting("EntityId", value);
                rdoEntity.SelectedValue = value;
            }
        }

        protected void LoadData()
        {
            PartBrowserController controller = new PartBrowserController(EntityId, Environment);
            Filter.LoadFilter();
            Filter.BaseQuery = "SELECT ID, PartId, Description, Status, IsRevisedPart FROM vwPartList";
            Filter.AddFilterCriteria("Status", rdoStatus.SelectedValue, true, true);

            Filter.OrderBy = OrderBy; // "DESIRED_RECV_DATE DESC";

            int pageCount = Paging.PageCount;
            int recordCount = Paging.RecordCount;
            int pageNumber = Paging.CurrentPage;
            int pageSize = Paging.PageSize;

            //controller.ApplyRestrictions(this.UserInfo, Filter, "V.CompanyName", (string)this.Settings["Companies"]);
            //controller.ApplyRestrictions(this.UserInfo, Filter, "V.VendorName", (string)this.Settings["Vendors"]);

            DataTable dt = controller.Search(Filter.Filter(), pageNumber, ref pageCount, ref recordCount, pageSize);
            Paging.PageCount = pageCount;
            Paging.RecordCount = recordCount;

            //Paging2.PageCount = pageCount;
            //Paging2.RecordCount = recordCount;
            //Paging2.CurrentPage = Paging.CurrentPage;

            dt.Columns.Add("NavigateUrl", System.Type.GetType("System.String"));
            foreach (DataRow dr in dt.Rows)
            {
                string id = controller.GetValue<string>(dr, "ID", "-1");
                //string tmpl8 = string.Format("~/PDMPartEditor{0}/e/{1}/id/{2}", EntityId, Environment, id);
                dr["NavigateUrl"] = PartEditorUrl(id); // ResolveUrl(tmpl8);
            }

            lstParts.DataSource = dt;
            lstParts.DataBind();

            bool isAdmin = this.UserInfo.IsInRole("Administrators") || this.UserInfo.IsSuperUser;
            FilterWarning();
            SaveUserSetting("View Role", ddlRole.SelectedValue);

            lnkNewPart.Visible = (ddlRole.SelectedValue == "Product Development" || ddlRole.SelectedValue == "Administrator");

           
        }

        private string ViewRole
        {
            get
            {
                switch (ddlRole.SelectedValue)
                {
                    case "Product Development":
                        return "pd";
                        break;
                    case "Administrator":
                        return "ad";
                        break;
                    default:
                        try
                        {
                            return ddlRole.SelectedValue.Substring(0, 1).ToLower();
                        }
                        catch
                        {
                            return string.Empty;
                        }
                        break;
                }
            }
        }

        private string PartEditorUrl(string id)
        {
            string tmpl8 = string.Format("~/PDMPartEditor{0}/e/{1}/id/{2}/r/{3}/eid/{0}", EntityId, Environment, id, ViewRole);
            return ResolveUrl(tmpl8);
        }

        private string exportFolder { get { return Server.MapPath(ControlPath) + "/Exports"; } }

        protected void ToggleView(object obj, EventArgs args)
        {
            ToggleView();
            LoadData();
            SaveUserSetting("Part View", rdoStatus.SelectedValue);
        }

        private void ToggleView()
        {
            string status = rdoStatus.SelectedValue;

            Filter.LoadFilter();
            Filter.AddFilterCriteria("Status", status, true, true);
            Filter.SaveFilter();

            Paging.CurrentPage = 1;//PageNumber = 1;
            Paging.PageCount = 0; //PageCount = 0;

            Paging.PageCount = 0;

            

        }

        protected void DoQuickSearch(object obj, EventArgs args)
        {
            string test = txtQuickSearch.Text.Trim().ToUpper().Replace("'", "''");
            if (string.IsNullOrEmpty(test)) return;

            txtQuickSearch.Text = string.Empty;

            string tmpl8 = "SELECT * FROM PARTS WHERE PartId LIKE '%{0}%' OR DESCRIPTION LIKE '%{0}%'";
            string sql = string.Format(tmpl8, test);
            Controller controller = new Controller(EntityId, Environment);
            DataTable dt = controller.ExecuteReader(sql);
            if (dt.Rows.Count == 0)
            {
                hdnShowMessage.Value = "No Parts were found searching on: " + test;
                return;
            }

            Filter.LoadFilter();
            foreach (DataRow dr in dt.Rows)
            {
                string partId = controller.GetValue<string>(dr, "PartId", string.Empty);
                if (!string.IsNullOrEmpty(partId)) Filter.AddFilterCriteria("PartId", partId, true, false);
            }
            Filter.SaveFilter();
            Paging.CurrentPage = 1;//PageNumber = 1;
            Paging.PageCount = 0; //PageCount = 0;

            Paging.PageCount = 0;

            LoadData();

        }

        protected void DoExport(object obj, CommandEventArgs args)
        {
            //POExporter.Settings settings = new POExporter.Settings(PortalEntity, environment);

            //string purchaseOrder = args.CommandArgument.ToString();

            //POExporter.Exporter exporter = new POExporter.Exporter(PortalEntity, environment.Equals("Production"), settings.FedEx, settings.DomesticRouting, exportFolder);
            //POExporter.PRINT_RESULT result = exporter.GeneratePDF(purchaseOrder);

            //if (result != POExporter.PRINT_RESULT.OK)
            //{
            //    hdnShowMessage.Value = "There was an error generating the PDF:\r\n\r\n" + exporter.error.Message + "\r\n" + exporter.error.StackTrace;
                
            //    return;
            //}

            //hdnPDF.Value = string.Format("{0}/{1}", ResolveUrl("~/PurchaseOrders"), exporter.FILE_NAME);
        }

        private int filterHeight
        {
            get
            {
                if (Settings.Contains("FilterHeight")) return (int)Settings["FilterHeight"];
                return 400;
            }
        }

        private int filterWidth
        {
            get
            {
                if (Settings.Contains("FilterWidth")) return (int)Settings["FilterWidth"];
                return 800;
            }
        }

        private void SetFilterUrl()
        {
            Paging.FilterUrl = EditUrl("Filter").Replace("550,950", string.Format("{0},{1}", filterHeight, filterWidth));
        }

        private void OpenParts(object parent, ref string urls)
        {
            foreach(Control child in ((Control)parent).Controls)
            {
                if (child.GetType().Name.Equals("CheckBox"))
                {
                    CheckBox chk = (CheckBox)child;
                    if (chk.Checked)
                    {
                        if (!string.IsNullOrEmpty(urls)) urls += "|";
                        urls += chk.Attributes["NavigateUrl"];
                    }
                }
                OpenParts(child, ref urls);
            }
        }

        private void GetSelectedParts(object parent, List<int> ids)
        {
            foreach (Control child in ((Control)parent).Controls)
            {
                if (child.GetType().Name.Equals("CheckBox"))
                {
                    CheckBox chk = (CheckBox)child;
                    if (chk.Checked)
                    {
                        ids.Add(Int32.Parse(chk.Attributes["PartId"]));
                    }
                }
                GetSelectedParts(child, ids);
            }
        }

        protected void DoCopyParts(object parent, CommandEventArgs args) 
        {
            List<int> ids = new List<int>();
            GetSelectedParts(lstParts, ids);
            if (ids.Count == 0) return;

            PartBrowserController controller = new PartBrowserController(EntityId, Environment);
            List<int> newbies = controller.CopyParts(this.UserInfo, ids);

            string url = string.Empty;
            foreach (int newbie in newbies)
            {
                string tgt = PartEditorUrl(newbie.ToString());
                if (!string.IsNullOrEmpty(url)) url += "|";
                url += tgt;
            }
            hdnUrl.Value = url;


            Paging.CurrentPage = 1;//PageNumber = 1;
            Paging.PageCount = 0; //PageCount = 0;

            Paging.PageCount = 0;
            LoadData();
        }

        protected void DoDeleteParts(object obj, CommandEventArgs args)
        {
            List<int> ids = new List<int>();
            GetSelectedParts(lstParts, ids);

            if (ids.Count == 0) return;

            PartBrowserController controller = new PartBrowserController(EntityId, Environment);
            controller.DeleteParts(this.UserInfo.Username, ids);

            Paging.CurrentPage = 1;
            Paging.PageCount = 0;
            LoadData();

            hdnShowMessage.Value = "The selected parts have been deleted.";

        }

        protected void DoNewPart(object obj, CommandEventArgs args)
        {
            //clear out any settings assigned to part -1
            string container = string.Format("PDM_{0}_{1}", EntityId, Environment);
            List<string> attributes = (List<string>)DotNetNuke.Services.Personalization.Personalization.GetProfile(container, "Keys");
            if (attributes != null)
            {
                foreach (string attribute in attributes)
                {
                    string key = "-1_" + attribute;
                    DotNetNuke.Services.Personalization.Personalization.RemoveProfile(container, key);
                }
            }

            //create the basics
            DotNetNuke.Services.Personalization.Personalization.SetProfile(container, "-1_Status", "Draft");
            DotNetNuke.Services.Personalization.Personalization.SetProfile(container, "-1_LastAccessed", DateTime.Now);

            string tgt = "NewPart";
            if (EntityId.Equals("H2M")) tgt = "H2MNewPart";
            string url = EditUrl("id", "-1", tgt, "e", Environment, "eid", EntityId, "r", ViewRole);
            hdnPopup.Value = url;
        }
    }
}
 