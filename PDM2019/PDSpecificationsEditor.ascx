<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDSpecificationsEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PartSpecificationsEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        ToggleLanguages();
        ToggleElectronics();
        ToggleBatteries();
        //ToggleMagnets();
        //TogglePanels();
    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    function ToggleLanguages() {
        var checked = jQuery('[id*="chkMultilingual"]:checked').length > 0;

        if (checked)
            jQuery('[id*="pnlLanguage"]').show();
        else
            jQuery('[id*="pnlLanguage"]').hide();
    }

    function ToggleElectronics() {
        var checked = jQuery('[id*="chkElectronic"]:checked').length > 0;

        if (checked)
            jQuery('[id*="pnlElectronics"]').show();
        else
            {
                jQuery('[id*="pnlElectronics"]').hide();
                //var prev = jQuery('[id*="chkElectronic"]').closest('span').attr("Original");
                //if (prev == "True") alert("Warning! Upon saving, any Electronic data will be removed from this part, including Battery information.");
            }
    }

    function ToggleBatteries() {
        var checked = jQuery('[id*="chkBatteriesRequired"]:checked').length > 0;

        if (checked)
            jQuery('[id*="pnlBatteries"]').show();
        else
            jQuery('[id*="pnlBatteries"]').hide();
    }


    // client side toggle works, but then individual controls get mucked up and can't get focused? 
    function TogglePanels(which) {
        
        if (which)
            jQuery('[id*="hdnShowPanel"]').val(which);
        else
            which = jQuery('[id*="hdnShowPanel"]').val();

        switch (which) {
            case "Battery":
                jQuery('[id*="pnlMain"]').hide();
                jQuery('[id*="pnlAddBattery"]').show();
                jQuery('[id*="pnlAddComponent"]').hide();
                break;
            case "Component":
                jQuery('[id*="pnlMain"]').hide();
                jQuery('[id*="pnlAddBattery"]').hide();
                jQuery('[id*="pnlAddComponent"]').show();
                break;
            default:
                jQuery('[id*="pnlMain"]').show();
                jQuery('[id*="pnlAddBattery"]').hide();
                jQuery('[id*="pnlAddComponent"]').hide();
                break;
        }


    }

    //crap doesn't work right
    function isEmpty(evt) {
        var raw = jQuery('[id*="txtComponentSize"]').val().trim();

        if (raw != "") return;

        jQuery('[id*="txtComponentSize"]').hide();
        jQuery('[id*="lblComponentL"]').show();
        jQuery('[id*="txtComponentL"]').show();
        jQuery('[id*="lblComponentW"]').show();
        jQuery('[id*="txtComponentW"]').show();
        jQuery('[id*="lblComponentH"]').show();
        jQuery('[id*="txtComponentH"]').show();
        
    }
</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td colspan="4" align="left">
                <asp:Label ID="labelGeneralDescription" runat="server" CssClass="SubSubHead" Text="General Description:<br />" />
                <asp:TextBox ID="txtGeneralDescription" runat="server" CssClass="Box" Width="780" Height="150" TextMode="MultiLine" Table="vwPartSpecifications" Field="GeneralDescription" />
            </td>
        </tr>
        <tr>
            <td align="left" style="width:200px;" valign="bottom">
                <asp:Label ID="labelColor" runat="server" CssClass="SubSubHead" Text="Color:<br />" />
            </td>
            <td style="width:200px;" />
            <td style="width:200px;" />
            <td style="width:200px;" />
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlColor" runat="server" Width="180" Table="vwPartSpecifications" Field="ProductColor" CssClass="Box">
                    <asp:ListItem Text="Color" Value="" />
                    <asp:ListItem Text="Red" Value="RED" />
                    <asp:ListItem Text="Orange" Value="ORANGE" />
                    <asp:ListItem Text="Yellow" Value="YELLOW" />
                    <asp:ListItem Text="Green" Value="GREEN" />
                    <asp:ListItem Text="Blue" Value="BLUE" />
                    <asp:ListItem Text="Purple" Value="PURPLE" />
                    <asp:ListItem Text="Black" Value="BLACK" />
                    <asp:ListItem Text="White" Value="WHITE" />
                    <asp:ListItem Text="Pink" Value="PINK" />
                    <asp:ListItem Text="Silver" Value="SILVER" />
                    <asp:ListItem Text="Gold" value="GOLD" />
                    <asp:ListItem text="Multicolor" value="MULTI" selected="true" />
                </asp:DropDownList>
            </td>
            <td colspan="3" />
        </tr>
        <tr>
            <td valign="top">
                <asp:CheckBox ID="chkMultilingual" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Multilingual Product" Table="vwPartSpecifications" Field="MultilingualProduct" onclick="javascript:ToggleLanguages();" />
            </td>
            <td valign="top" colspan="3">
                <asp:Panel ID="pnlLanguage" runat="server" style="border:solid 1px #e8e2e2;" width="580">
                    <asp:CheckBox ID="chkEnglish" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;English" Width="140" Table="vwPartSpecifications" Field="English" />
                    <asp:CheckBox id="chkSpanish" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Spanish" Width="140" Table="vwPartSpecifications" Field="Spanish" />
                    <asp:CheckBox ID="chkFrench" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;French" Width="140" Table="vwPartSpecifications" Field="French" />
                    <asp:CheckBox ID="chkGerman" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;German" Width="140" Table="vwPartSpecifications" Field="German" />
                    <br />
                    &nbsp;<br />
                    <asp:Label ID="labelOtherLanguage" runat="server" CssClass="SubSubHead" Text="Other Language(s):<br />" />
                    <asp:TextBox ID="txtOtherLanguage" runat="server" Width="560" Table="vwPartSpecifications" Field="MultiLingualProductLanguage" PlaceHolder="Other Language(s)" MaxLength="255" />
                </asp:Panel>
            </td>
        </tr>

        <tr>
            <td align="left">
                <asp:CheckBox ID="chkMagnets" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Magnets" Table="vwPartSpecifications" Field="Magnets" />
            </td>
            <td align="left">
                <asp:CheckBox ID="chkAssemblyRequired" runat="server" TextAlign="Right" CssClass="SubSubHead" Text="&nbsp;Assembly Required" Table="vwPartInfos" Field="AssemblyRequired" />
            </td>
            <td align="left">
                <asp:CheckBox ID="chkWaterproof" runat="server" TextAlign="Right" CssClass="SubSubHead" Text="&nbsp;Waterproof" Table="vwPartInfos" Field="Waterproof" />
            </td>
            <td align="left">
                <asp:CheckBox ID="chkTempcontrolled" runat="server" TextAlign="Right" CssClass="SubSubHead" Text="&nbsp;Temperature Controlled" Table="vwPartInfos" Field="TempuratureControlled" />
            </td>
        </tr>

        <tr>
            <td align="left" colspan="4">
                <asp:CheckBox ID="chkPaintTesting" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Component Contains Paint (not screen-printed)" Table="vwQAPartInfos" Field="PaintTesting" />
            </td>
        </tr>

        <tr>
            <td valign="top">
                <asp:CheckBox ID="chkElectronic" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Electronic Product" Table="vwPartSpecifications" Field="Electronic" onclick="javascript:ToggleElectronics();" />
            </td>

            <td valign="top" colspan="3">
                <asp:Panel ID="pnlElectronics" runat="server" style="border:solid 1px #e8e2e2;" width="580">
                    <asp:CheckBox ID="chkWifi" runat="server" CssClass="SubSubHead" Width="140" TextAlign="Right" Text="&nbsp;Wi-fi Enabled" Table="vwPartSpecifications" Field="WifiEnabled" />
                    <asp:CheckBox ID="CheckBox1" runat="server" CssClass="SubSubHead" Width="140" TextAlign="Right" Text="&nbsp;Power Switch" Table="vwPartSpecifications" Field="PowerSwitch" />
                    <asp:CheckBox ID="CheckBox2" runat="server" CssClass="SubSubHead" Width="140" TextAlign="Right" Text="&nbsp;Power Indicator" Table="vwPartSpecifications" Field="PowerIndicator" />
                    <asp:CheckBox ID="CheckBox3" runat="server" CssClass="SubSubHead" Width="140" TextAlign="Right" Text="&nbsp;LED Technology" Table="vwPartSpecifications" Field="LEDTechnology" />
                    <br />

                                        <asp:Label ID="lblPOwersources" runat="server" Width="140" CssClass="SubSubHead" Text="Power Sources:" />
                    <asp:CheckBox ID="chkACPower" runat="server" Width="140" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;A/C Power" Table="vwPartSpecifications" Field="ACPower" />
                    <asp:CheckBox ID="chkUSBPower" runat="server" Width="140" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;USB Power" Table="vwPartSpecifications" Field="USBPower" />
                    <asp:CheckBox ID="chkBatteriesRequired" runat="server" Width="140" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Batteries Required" Table="vwPartSpecifications" Field="Batteries" onclick="javascript:ToggleBatteries();"/>
                    <br />

                    <asp:Panel ID="pnlBatteries" runat="server" style="padding-top:10px;padding-right:5px;float:right;" width="350">
                        <table width="325" class="HeaderRow">
                            <tr>
                                <td style="width:150px;padding-left:4px;">Battery Size</td>
                                <td style="width:75px;" align="center">Quantity</td>
                                <td style="width:50px;" align="center">Included</td>
                                <td style="width:50px; padding:2px 4px 2px 2px;" align="right" valign="middle">
                                    <asp:ImageButton ID="btnAddBattery" runat="server" ImageUrl="~/images/plus2.gif" OnCommand="Battery_Toggle" CommandArgument="Open" />
                                </td>
                            </tr>
                        </table>
                        <asp:DataList ID="lstBatteries" runat="server" CssClass="table table-hover table-condensed" Table="vwBatteries" Width="325" >
                            <ItemTemplate>
                                <asp:Label ID="lblbatterysize" Width="150" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BatterySize") %>' />
                                <asp:Label ID="lblbatteryqty" runat="server" Width="75" style="text-align:center;" Text='<%# DataBinder.Eval(Container.DataItem, "BatteryQuantity") %>' />
                                <asp:Label ID="Label15" runat="server" Width="20" Text="&nbsp;" /><asp:Image id="imgbatteryincldue" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "BatteryIncludedImage") %>' />
                                <asp:Label ID="lblSpace2" runat="server" Width="20" Text="&nbsp;" />
                                <asp:ImageButton id="imgBatteryDelete" runat="server" ImageUrl="~/images/delete.gif" 
                                                        OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');" Hint="Editor" 
                                                        CommandArgument ='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>'
                                                        BatteriesFor = '<%# DataBinder.Eval(Container.DataItem, "BatteriesFor") %>'
                                                        BatterySize = '<%# DataBinder.Eval(Container.DataItem, "BatterySize") %>'
                                                        BatteryQuantity = '<%# DataBinder.Eval(Container.DataItem, "BatteryQuantity") %>'
                                                        BatteryIncluded = '<%# DataBinder.Eval(Container.DataItem, "BatteryIncluded") %>'
                                                        OnCommand="Battery_Delete"
                                />
                            </ItemTemplate>
                            <AlternatingItemStyle BackColor="#e8e2e2" />
                        </asp:DataList>
                        <asp:Label ID="lblNoBatteries" runat="server" CssClass="Normal" Width="325" Text="No batteries have been added to Part." style="text-align:left;padding-left:4px;" />

                    </asp:Panel>




                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td style="width:250px; border:solid 1px #e8e3e3;" valign="top">
                                <asp:CheckBox ID="chkIncludesRemote" runat="server" CssClass="SubSubHead" Text="&nbsp;Includes Remote" Table="vwPartSpecifications" Field="IncludesRemote" />
                                <br />
                                
                                <asp:Label ID="lblRemoteBattery" runat="server" Width="240" style="padding: 2px 0px 2px 25px; text-decoration:underline;" Text="Remote Battery" />
                                <br />
                                
                                <asp:Label ID="lblRemoteBatterySize" runat="server" Width="75" CssClass="SubSubHead" style="padding: 2px 0px 2px 25px;" Text="Size:" />
                                <asp:DropDownList ID="ddlBatterySize" runat="server" CssClass="Box" Table="vwPartSpecifications" Field="RemoteBatterySize" Width="100" >
                                    <asp:ListItem Text="Remote Battery Size" Value="" />
                                    <asp:ListItem Text="AAA" Value="AAA" />
                                    <asp:ListItem Text="AA" Value="AA" />
                                    <asp:ListItem Text="C" Value="C" />
                                    <asp:ListItem Text="D" Value="D" />
                                    <asp:ListItem Text="9 Volt" Value="9V" />
                                    <asp:ListItem Text="Button Cells" Value="Button Cells" />
                                </asp:DropDownList>
                                <br />

                                <asp:Label ID="lblRemoteBatteryQty" runat="server" Width="75" CssClass="SubSubHead" style="padding: 2px 0px 2px 25px;" Text="Quantity:" />
                                <asp:Textbox ID="txtBatteryQty" runat="server" CssClass="Box" Width="100" Table="vwPartSpecifications" Field="RemoteBatteryQuantity" Format="Number"   onkeypress="return isNumberKey(event);" Placeholder="Quantity" />
                                <br />

                                <asp:Label ID="lblRemoteBatteryinc" runat="server" Width="75" style="padding: 2px 0px 2px 25px;" Text="&nbsp;" />
                                <asp:CheckBox ID="chkBatteryinclude" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Included" Table="vwPartSpecifications" Field="RemoteBatteryIncluded" />         
                            </td>
                            <td style="width:15px;">&nbsp;</td>
                            <td style="width:310px; border:solid 1px #e8e2e2;" valign="top">
                                <asp:Label ID="lblJacks" runat="server" CssClass="SubSubHead" style="text-align:center;text-decoration:underline;" Text="Jacks" Width="300" />
                                <br />

                                <asp:CheckBox ID="chkusbjack" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;USB Jack" Table="vwPartSpecifications" Field="USB" />
                                <br />
                                
                                <asp:CheckBox ID="chkusbcable" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;USB Cable Included" Table="vwPartSpecifications" Field="USBCableIncluded" />
                                <br />

                                <asp:CheckBox ID="chkheadphones" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Headphone Jack(s)" Table="vwPartSpecifications" Field="HeadphoneJacks" />
                                <br />
                                <asp:Label ID="lblheadphoneqty" runat="server" CssClass="SubSubHead" style="padding: 2px 0px 2px 25px;" Width="75" Text="Quantity:" />
                                <asp:TextBox ID="txtheadphoneqty" runat="server" CssClass="Box" Width="75" MaxLength="10" Format="Number" onkeypress="return isNumberKey(event);" Table="vwPartSpecifications" Field="HeadphoneJackQuantity" Placeholder="Quantity" />
                            </td>
                            <td style="width:5px;">&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="HeaderRow">
                    <tr>
                        <td style="width:730px;">Components:</td>
                        <td style="width:20px;padding-right:4px;" align="right"><asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/images/plus2.gif" OnCommand="Component_Toggle" CommandArgument="Open" RecordId="-1" /></td>
                    </tr>
                </table>
                <asp:DataList ID="lstComponents" runat="server" Width="100%" CssClass="table table-hover table-condensed" Table="vwPartPackagingComponents" OnDataBinding="VerifyDataListHasData" NoDataLabel="lblNoComponents">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDeleteComponent" runat="server" ImageUrl="~/images/delete.gif" OnClientClick="javascript:return confirm('Are you sure you want to delete this record?');" RecordId='<%# DataBinder.Eval(Container.DataItem, "Id") %>' OnCommand="Component_Delete" style="vertical-align:top;" />
                        <asp:ImageButton ID="btnEditComponent" runat="server" ImageUrl="~/images/edit.gif" OnCommand="Component_Toggle" RecordId='<%# DataBinder.Eval(Container.DataItem, "Id") %>' CommandArgument="Open" style="vertical-align:top;" />
                        <asp:Label ID="lblMolds" runat="server" Width="710" CssClass="SubSubHead" Text='<%# DataBinder.Eval(Container.DataItem, "Description") %>' />
                        <br />
                        <asp:Label ID="lblQuantity" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="75" Text="Quantity:" />
                        <asp:Label ID="lblQty" runat="server" CssClass="Normal" Width="100" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>' />
                        <asp:Label ID="lblSize" runat="server" CssClass="SubSubHead" Width="75" Text="Size:" Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), false) %>' />
                        <asp:Label ID="Label12" runat="server" CssClass="SubSubHead" Width="75" Text="Size (in.):" Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                        <asp:Label ID= "lblSize1" runat="server" CssClass="Normal" Width="225" Text='<%# DataBinder.Eval(Container.DataItem, "Size") %>' Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), false) %>' />
                        <asp:Label ID="lblSizeL" runat="server" CssClass="SubSubHead" Width="25" Text='L:' Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                        <asp:Label id="lblSizeLV" runat="server" CssClass="Normal" Width="50" Text='<%# DataBinder.Eval(Container.DataItem, "Length") %>' Visible = '<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                        <asp:Label ID="Label8" runat="server" CssClass="SubSubHead" Width="25" Text='W:' Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                        <asp:Label id="Label9" runat="server" CssClass="Normal" Width="50" Text='<%# DataBinder.Eval(Container.DataItem, "Width") %>' Visible = '<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                        <asp:Label ID="Label10" runat="server" CssClass="SubSubHead" Width="25" Text='H:' Visible='<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                        <asp:Label id="Label11" runat="server" CssClass="Normal" Width="50" Text='<%# DataBinder.Eval(Container.DataItem, "Height") %>' Visible = '<%# ShowLegacy(DataBinder.Eval(Container.DataItem, "ShowLegacy"), true) %>' />
                        <br />
                        <asp:Label ID="Label13" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="75" Text="Material:" />
                        <asp:Label ID="Label14" runat="server" CssClass="Normal" Width="600" Text='<%# DataBinder.Eval(Container.DataItem, "MaterialsDisplay") %>' />
                        <br />
                        <asp:Label ID="Label16" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="75" Text="Details:" />
                        <asp:Label ID="Label17" runat="server" CssClass="Normal" Width="600" Text='<%# DataBinder.Eval(Container.DataItem, "Details") %>' />
                        
                    </ItemTemplate>
                    <AlternatingItemStyle BackColor="#e8e2e2" />
                </asp:DataList>
                <asp:Label id="lblNoComponents" runat="server" style="padding-left:15px;"  CssClass="Normal" Text="No Components have been added to this part." />                
            </td>
        </tr>
    </table>
    </asp:Panel>

    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" Checked="true" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" />
            </td>
        </tr>
    
    </table>
</asp:Panel>

<asp:Panel ID="pnlAddBattery" runat="server"  Visible="false">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr >
            <td style="height:450px;" align="center" valign="middle">
                <asp:Panel ID="pnladdbatteryinner" runat="server" Width="375" style="border:solid 1px black;padding:4px;">
                    <asp:Label ID="lblAddBatteryForm" runat="server" CssClass="Head" Text="Add Battery" Width="370" style="border-bottom:solid 1px black; text-align:center;" />
                    
                    <p style="padding: 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="lblAddBatterySize" runat="server" CssClass="SubSubHead" Width="100" Text="Battery Size:" />
                        <asp:DropDownList ID="ddlAddBatterySize" runat="server" CssClass="Box" Width="200" >
                            <asp:ListItem Text="Battery Size" Value="" />
                            <asp:ListItem Text="AAA" Value="AAA" />
                            <asp:ListItem Text="AA" Value="AA" />
                            <asp:ListItem Text="C" Value="C" />
                            <asp:ListItem Text="D" Value="D" />
                            <asp:ListItem Text="9 Volt" Value="9V" />
                            <asp:ListItem Text="Button Cells" Value="Button Cells" />
                        </asp:DropDownList>
                    </p>

                    <p style="padding: 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="lblAddBatteryQty" runat="server" CssClass="SubSubHead" Width="100" Text="Quantity:" />
                        <asp:TextBox ID="txtAddBatteryQuantity" runat="server" CssClass="Box" Width="100" Placeholder="Quantity" Format="Number" MaxLength="10"  onkeypress="return isNumberKey(event)" />
                        &nbsp;&nbsp;
                        <asp:CheckBox ID="chkAddBatteryIncluded" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Included" />
                    </p>

                    <p style="padding: 5px 5px 5px 5px; text-align:left;">
                        <asp:Button ID="btnCancelBattery" runat="server" Text="Cancel" OnCommand="Battery_Toggle" CommandArgument="Close" Width="75" />
                        <asp:Label ID="lblAddBatteryFormSpace" runat="server" Width="200" Text="&nbsp;" />
                        <asp:Button ID="btnSaveBattery" runat="server" Text="Save" Width="75" OnCommand="Battery_Save" />
                    </p>
                </asp:Panel>


            </td>
        </tr>
        
    </table>
</asp:Panel>

<asp:Panel ID="pnlAddComponent" runat="server"  Visible="false">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr >
            <td style="height:450px;" align="center" valign="middle">
                <asp:Panel ID="Panel2" runat="server" Width="375" style="border:solid 1px black;padding:4px;">
                    <asp:Label ID="lblAddComponent" runat="server" CssClass="Head" Width="370" style="border-bottom:solid 1px black; text-align:center;" />
                    
                    <p style="padding: 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="Label2" runat="server" CssClass="SubSubHead" Width="100" Text="Quantity:" />
                        <asp:TextBox ID="txtComponentQuantity" runat="server" CssClass="Box" Width="200" MaxLength="10" onkeypress="return isNumberKey(event);" Placeholder="Quantity" />
                    </p>

                    <p style="padding: 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="Label3" runat="server" CssClass="SubSubHead" Width="100" Text="Description:" />
                        <asp:TextBox ID="txtComponentDescription" runat="server" CssClass="Box" Width="200" Placeholder="Description" MaxLength="80" />
                    </p>

                    <p style="padding: 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="Label1" runat="server" CssClass="SubSubHead" Width="100" Text="Size (in.):" />
                        <asp:TextBox ID="txtComponentSize" runat="server" CssClass="Box" Width="200" Placeholder="Size" MaxLength="50" />
                        <asp:Label ID="lblComponentL" runat="server" CssClass="SubSubHead" Text="L:" />
                        <asp:TextBox ID="txtComponentL" runat="server" Width="35" CssClass="Box" MaxLength="9" onkeypress="return isDecimal(event);" Format="Decimal" Places="2" />
                        <asp:Label ID="lblComponentW" runat="server" CssClass="SubSubHead" Text="&nbsp;W:" />
                        <asp:TextBox ID="txtComponentW" runat="server" Width="35" CssClass="Box" MaxLength="9" onkeypress="return isDecimal(event);" Format="Decimal" Places="2" />
                        <asp:Label ID="lblComponentH" runat="server" CssClass="SubSubHead" Text="H:" />
                        <asp:TextBox ID="txtComponentH" runat="server" Width="35" CssClass="Box" MaxLength="9" onkeypress="return isDecimal(event);" Format="Decimal" Places="2" />
                    </p>

                    <p style="padding: 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="Label5" runat="server" CssClass="SubSubHead" Width="100" Text="Material Type:" />
                        <asp:DropDownList ID="ddlComponentMaterialType" runat="server" CssClass="Box" Width="200" AutoPostBack="true" OnSelectedIndexChanged="Component_ToggleMaterial" />
                    </p>

                    <p style="padding: 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="Label6" runat="server" CssClass="SubSubHead" Width="100" Text="Material:" />
                        <asp:DropDownList ID="ddlComponentMaterial" runat="server" CssClass="Box" Width="200" />
                    </p>

                    <p style="padding 5px 0px 5px 5px; text-align:left;">
                        <asp:Label ID="Label7" runat="server" CssClass="SubSubHead" Width="100" Text="Details:" />
                        <asp:TextBox ID="txtComponentDetails" runat="server" CssClass="Box" Width="200" Placeholder="Details" MaxLength="100" />
                    </p>
                    
                    <p style="padding: 5px 5px 5px 5px; text-align:left;">
                        <asp:Button ID="Button1" runat="server" Text="Cancel" OnCommand="Component_Toggle" CommandArgument="Close" Width="75" />
                        <asp:Label ID="Label4" runat="server" Width="200" Text="&nbsp;" />
                        <asp:Button ID="btnAddComponentSave" runat="server" Text="Save" Width="75" OnCommand="Component_Save" />
                    </p>
                </asp:Panel>


            </td>
        </tr>
        
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<%--<asp:HiddenField ID="hdnShowPanel" runat="server" Value="Main" />--%>


