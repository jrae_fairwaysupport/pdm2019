<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchasingTabPartInformation.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PurchasingPartInformationTab" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<style>
    div.offset { padding-left:4px; padding-bottom:4px;
    }

    td.indent { padding-left:10px; }   
</style>

<script language="javascript" type="text/javascript">
    function TogglePurchasingMore(toggleWhat) {
        var link = jQuery('[id*="lnkPurchasingViewMore' + toggleWhat + '"]');
        var lbl = jQuery('[id*="lblPurchasing' + toggleWhat + '"]');


        if (link.text() == "View More") {
            link.text("View Less");
            lbl.css("max-height", "");
        }
        else {
            link.text("View More");
            lbl.css("max-height", "200px");
        }
        return false;
    }
</script>

<asp:Panel ID="pnlMain" runat="server" style="padding: 4px;">
    <asp:Panel ID="pnlActions" runat="server" Width="750" style="border-bottom:solid 1px black; padding:5px; text-align:right;">
        <asp:HyperLink ID="lnkEdit" runat="server"  ><span class="actionButton">Edit</span></asp:HyperLink>
    </asp:Panel>
    <asp:Panel id="pnlPurchasing" runat="server" Width="750" Height="600" ScrollBars="Auto">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width:150px;" class="SubSubHead">Planner User Id:</td>
                <td style="width:225px;"><asp:Label ID="lblplannerUserId" runat="server" CssClass="Normal placeholder" Width="205" Table="vwPartInfos" Field="PlannerUserId" /></td>
                <td style="width:150px;" class="SubSubHead">Buyer User Id:</td>
                <td style="width:225px;"><asp:Label ID="lblbuyeruserid" runat="server" CssClass="Normal placehodler required" Width="205" Table="vwPartInfos" Field="BuyerUserId" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Safety Stock:</td>
                <td><asp:Label ID="lblSafetyStock" runat="server" CssClass="Normal placeholder required" Width="205" Table="vwPurchasingPartInfos" Field="SafetyStock" /></td>
                <td class="SubSubHead">Class Code:</td>
                <td><asp:Label ID="lblClassCode" runat="server" CssClass="Normal placeholder" Width="205" Table="vwPurchasingPartInfos" Field="ClassCode" /></td>
            </tr>
            <tr>
                <td class="SubSubHead">Preferred Vendor:</td>
                <td colspan="3">
                    <asp:Label ID="lblpreferredvendor" runat="server" CssClass="Normal placeholder" Width="580" Table="vwPartInfos" Field="PreferredVendorSuggest" />
                </td>
            </tr>

            <tr>
                <td class="SubSubHead">Duty Amount:</td>
                <td><asp:Label ID="lblduty" runat="server" CssClass="Normal placeholder" Width="205" style="text-align:right;" Table="vwPurchasingPartInfos" Field="DutyRateText" /></td>
                <td class="SubSubHead">Add Rate:</td>
                <td><asp:Label ID="lbladdrate" runat="server" CssClass="Normal placeholder" Width="205" style="text-align:right;" Table="vwPurchasingPartInfos" Field="AddRateText" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">HTS Code:</td>
                <td colspan="3"><asp:Label ID="lblhtscode" runat="server" CssClass="Normal placeholder required" Width="580" Table="vwPurchasingPartInfos" Field="HtsCodeSuggest" /></td>
            </tr>

          

            <tr>
                <td class="SubSubHead">Material Code:</td>
                <td><asp:Label ID="lblmaterialcode" runat="server" CssClass="Normal placeholder required" Width="205" Table="vwPurchasingPartInfos" Field="MaterialCode" /></td>
                <td class="SubSubHead">Engineering Master Id:</td>
                <td><asp:Label ID="lblengineeringmasterid" runat="server" CssClass="Normal placeholder" Width="205" Table="vwPurchasingPartInfos" Field="EngineeringMasterId" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">Primary Warehouse:</td>
                <td><asp:Label ID="Label1" runat="server" CssClass="Normal placeholder required" Width="205" Table="vwPurchasingPartInfos" Field="PrimaryWarehouse" /></td>
                <td class="SubSubHead">Foreign Warehouse:</td>
                <td><asp:Label ID="Label2" runat="server" CssClass="Normal placeholder" Width="205" Table="vwPurchasingPartInfos" Field="ForeignWarehouse" /></td>
            </tr>

            <tr>
                <td class="SubSubHead">Product Status:</td>
                <td><asp:Label ID="Label3" runat="server" CssClass="Normal placeholder" Width="205" Table="vwPurchasingPartInfos" Field="ProductStatus" /></td>
                <td class="SubSubHead">Commodity Code:</td>
                <td><asp:Label ID="Label6" runat="server" CssClass="Normal placeholder" Width="205" Table="vwPurchasingPartInfos" Field="CommodityCode" /></td>
                
            </tr>

            <tr>
                <td class="SubSubHead">Country of Origin:</td>
                <td><asp:Label ID="Label4" runat="server" CssClass="Normal placeholder required" Width="205" Table="vwPurchasingPartInfos" Field="CountryOfOrigin" /></td>
                <td class="SubSubHead">Port of Origin:</td>
                <td><asp:Label ID="Label5" runat="server" CssClass="Normal placeholder required" Width="205" Table="vwPurchasingPartInfos" Field="PortOfOrigin" /></td>
            </tr>

            <tr>
                <td class="SubSubHead" valign="top">Container Size:</td>
                <td valign="top">
                    <asp:Repeater ID="rptContainers" runat="server" >
                        <ItemTemplate>
                            <asp:Label ID="lblContainer" runat="server" CssClass="Normal placeholder" Text='<%# DataBinder.Eval(Container.DataItem, "ContainerSize") %>' />
                        </ItemTemplate>
                        <SeparatorTemplate>
                            ,&nbsp;
                        </SeparatorTemplate>
                    </asp:Repeater>
                </td>
                <td class="SubSubHead" valign="top"><asp:Image ID="imgHoldReceipts" runat="server" Table="vwPurchasingPartInfos" Field="HoldReceiptsCheck" />&nbsp;Hold Receipts</td>
                <td class="SubSubHead" valign="top"><asp:Image ID="imgObsolete" runat="server" Table="vwPurchasingPartInfos" Field="Obsolete" />&nbsp;Obsolete</td>
            </tr>

            <tr>
                <td class="SubSubHead" ><asp:Image ID="imgMagnet" runat="server" Table="Magnets" Field="ContainsMagnets" />&nbsp;Anti-Dumping:</td>
                <td colspan="3">
                    <asp:HyperLink ID="lnkMagnetLegal" runat="server" CssClass="Normal placeholder" Width="280" Target="_blank" ToolTip="Legal Document for Anti-dumping." Table="Magnets" Field="FileUrl" Display="OriginalFileName" />                    
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="HeaderRow">
                        <tr>
                            <td style="width:750px;">Molds:</td>
                        </tr>
                    </table>
                    <asp:DataList ID="lstMolds" runat="server" Width="100%" CssClass="table table-hover table-condensed" Table="vwPurchasingMolds" OnDataBinding="VerifyDataListHasData" NoDataLabel="lblNoMolds">
                        <ItemTemplate>
                            <asp:Label ID="lblVendor" runat="server" Width="700" CssClass="SubSubHead" Text='<%# DataBinder.Eval(Container.DataItem, "VendorSuggest") %>' />
                            <br />

                            <asp:Label ID="lblMoldIdlbl" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="125" Text="Mold Id:" />
                            <asp:Label ID="lblTracking" runat="server"  Width="125" Text='<%# DataBinder.Eval(Container.DataItem, "TrackingId") %>' />
                            <asp:Label ID="lblMoldsLabel" runat="server" CssClass="SubSubHead" width="100" Text='Cavities:' />
                            <asp:Label ID="lblMolds" runat="server" CssClass="Normal" Width="90" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfMolds") %>' />
                            <asp:Label ID="lblDateMovedlbl" runat="server" CssClass="SubSubHead" Width="100" Text="Date Moved:" />
                            <asp:Label ID="lblDateMoved" runat="server" Width="90" Text='<%# DataBinder.Eval(Container.DataItem, "DateMovedText") %>' />
                            <asp:Image ID="imgActive" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ActiveImage") %>' />
                            <asp:Label ID="lblActive" runat="server" CssClass="SubSubHead" Text="Active" />
                            <br />

                            <asp:Label ID="lblMoldMateriallbl" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="150" Text="Mold Material:" />
                            <asp:Label ID="lblMoldMaterial" runat="server" CssClass="Normal" Width="200" Text='<%# DataBinder.Eval(Container.DataItem, "MoldMaterialDisplayText") %>' />
                            <asp:Label ID="lblProdutM" runat="server" CssClass="SubSubHead" Width="125" Text="Product Material:" />
                            <asp:Label ID="lblProductMaterial" runat="server" CssClass="Normal" Width="155" Text='<%# DataBinder.Eval(Container.DataItem, "ProductMaterialDisplayText") %>' />
                            <asp:Image ID="imgSlides" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "SlidesImage") %>' />
                            <asp:Label ID="lblSlides" runat="server" CssClass="SubSubHead" Text="Slides" />
                            <br />

                            <asp:Label ID="lblMoldTypeLabel" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="150" Text="Mold Type:" />
                            <asp:Label ID="lblMoldType" runat="server" CssClass="Normal" Width="200" Text='<%# DataBinder.Eval(Container.DataItem, "MoldTypeDisplayText") %>' />
                            <asp:Label ID="lblCostText" runat="server" CssClass="SubSubHead" Width="125" Text="Cost:" />
                            <asp:Label ID="lblCost" runat="server" Width="75" Text='<%# DataBinder.Eval(Container.DataItem, "CostText") %>' />
                            <br />

                            <asp:Label ID="lblAgreement" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="150" Text="Agreement File:" />
                            <asp:HyperLink ID="lnkAgreement" runat="server" Target="_blank"
                                            ToolTip='<%# DataBinder.Eval(Container.DataItem, "AgreementFile") %>'
                                            NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "AgreementUrl") %>'
                                            Visible='<%# ShowAgreement(DataBinder.Eval(Container.DataItem, "AgreementUrl")) %>'
                                            ImageUrl="~/images/FileManager/Icons/file.gif" />
                            <asp:Label ID="Label7" runat="server" CssClass="SubSubHead" width="325" style="padding-left:184px;" Text="Snapshot File:" />
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank"
                                            ToolTip='<%# DataBinder.Eval(Container.DataItem, "SnapshotFile") %>'
                                            NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "SnapshotUrl") %>'
                                            Visible='<%# ShowAgreement(DataBinder.Eval(Container.DataItem, "SnapshotUrl")) %>'
                                            ImageUrl="~/images/FileManager/Icons/file.gif" />
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:Label id="lblNoMolds" runat="server" style="padding-left:15px;"  CssClass="Normal" Text="No Molds have been added to this part." />
                </td>
            </tr>

            <tr>
                <td class="SubSubHead" valign="top">Specification:</td>
                <td style="border:solid 1px #e8e2e2;padding:2px;" colspan="4">
                    <asp:Label id="lblPurchasingSpecification" runat="server" CssClass="Normal" Table="vwPartSpecifications" Field="GeneralDescriptionBR" Width="580" style="max-height:200px; overflow:hidden;" />
                    <div style="float:right;"><asp:LinkButton ID="lnkPurchasingViewMoreSpecification" runat="server" Text="View More" OnClientClick="javascript:return TogglePurchasingMore('Specification');" /></div>
                </td>
            </tr>        
        </table>
    </asp:Panel>
</asp:Panel>


<asp:HiddenField ID="hdnInitialized" runat="server" Value="false" />



