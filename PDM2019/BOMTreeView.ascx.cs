using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using Telerik.Web.UI;

namespace YourCompany.Modules.PDM2019
{
    public partial class BOMTreeView: LRHViewer 
    {

        public override bool Initialize()
        {
            if (base.Initialize()) return true;
            tree.Nodes.Clear();

            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, "Parts");

            DataTable parts = ds.Tables["Parts"];
            if (parts == null)
            {
                Initialized = true;
                return true;
            }
            if (parts.Rows.Count == 0)
            {
                Initialized = true;
                return true;
            }

            DataRow dr = parts.Rows[0];
            string description = controller.GetValue<string>(dr, "Description", string.Empty);

            RadTreeNode root = new RadTreeNode("root");
            Label lblStatus = new Label();
            lblStatus.Text = partStatus;
            lblStatus.CssClass = "button " + partStatus;
            root.Controls.Add(lblStatus);

            Label lblPart = new Label();
            lblPart.Text = string.Format("{0} - {1}", sku, description);
            lblPart.Style.Add("padding-left", "4px");
            lblPart.CssClass = "Normal";
            root.Controls.Add(lblPart);
            tree.Nodes.Add(root);

            AddChildren(controller, root, PartId);
            Initialized = true;
            return true;
        }

        private string PartEditorUrl(int id)
        {
            string tmpl8 = string.Format("~/PDMPartEditor{0}/e/{1}/id/{2}/eid/{0}/r/{3}", entityId, environment, id, ViewRole);
            return ResolveUrl(tmpl8);
        }

        private void AddChildren(Controller controller, RadTreeNode parent, int parentPartId)
        {
            string sql = "SELECT * FROM vwChildParts WHERE Part_id = @P0 ORDER BY PartId";
            DataTable dt = controller.pdm.ExecuteReader(sql, parentPartId);
            foreach (DataRow dr in dt.Rows)
            {
                int childPartUid = controller.GetValue<int>(dr, "ChildPartUid", -1);
                string childPartSuggest = controller.GetValue<string>(dr, "ChildPartSuggest", string.Empty);
                string status = controller.GetValue<string>(dr, "Status", "Draft");
                string key = string.Format("{0}:{1}", childPartUid, Guid.NewGuid().ToString());

                RadTreeNode child = new RadTreeNode(key);
                Label childStatus = new Label();
                childStatus.Text = status;
                childStatus.CssClass = "button " + status;
                child.Controls.Add(childStatus);

                HyperLink lnk = new HyperLink();
                lnk.Text = childPartSuggest;
                lnk.Style.Add("padding-left", "4px");
                lnk.Target = "_blank";
                lnk.CssClass = "Normal";
                lnk.NavigateUrl = PartEditorUrl(childPartUid);
                child.Controls.Add(lnk);
                parent.Nodes.Add(child);

                AddChildren(controller, child, childPartUid);
            }

        }


        
    }
}
 