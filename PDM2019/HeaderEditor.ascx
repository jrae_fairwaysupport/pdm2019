<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.HeaderEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>

<style>
    .readonly { background-color:#e8e2e2; }

    .suggest {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
            
    
    input.required
    {
        border: solid 1px orange;
    }
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();
        ToggleUPC();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        jQuery('[id*="txtParentPartId"]').bind('input', function () {
            var sel = jQuery('[id*="rdoIsNewPart"]:checked').val();
            if (sel == "Revision") jQuery('[id*="lnkLookupUPC"]').show();

        });

    });


        function ToggleLabels() {

            var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

            jQuery('[id*="label"]').each(function (index) {
                if (checked)
                    $(this).show();
                else
                    $(this).hide();
            });

            //$( "li" ).each(function( index ) {
            //  console.log( index + ": " + $( this ).text() );
            //

        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function isDecimal(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;

            if (charCode == 46) return true;
            return isNumberKey(evt);
        }

        function ToggleUPC() {
            var sel = jQuery('[id*="rdoIsNewPart"]:checked').val();
            
            jQuery('[id*="lnkLookupUPC"]').hide();

            if (sel == "Revision") {
                jQuery('[id*="txtupc"]').attr("disabled", "disabled");
                jQuery('[id*="txtupc"]').addClass("readonly");
                jQuery('[id*="lblNote"]').text('NOTE: UPC will be copied from Master Part upon saving.');
            }
            else {
                jQuery('[id*="txtupc"]').removeAttr("disabled");
                jQuery('[id*="txtupc"]').removeClass("readonly");
                jQuery('[id*="lblNote"]').text('NOTE: A unique UPC will be required upon Submitting for Approval.');
                
            }
        }


    //});
    </script>


<asp:Panel ID="pnlMain" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Width="850" Height="450" ScrollBars="Auto">
    <table width="800" cellpadding="4" cellspacing="0" border="0">
        <tr>
            <td style="width:300px;" align="left" valign="top">
                <asp:Label ID="labelPartId" runat="server" CssClass="SubSubHead" Text="Part Id:<br />" />
                <asp:TextBox ID="txtPartId" runat="server" CssClass="Box required" Width="180" MaxLength="25" Table="Parts" Field="PartId" Placeholder="Part Id" />
            </td>
            <td style="width:500px;" align="left" valign="top">
                <asp:Label ID="labelDescription" runat="server" CssClass="SubSubHead" Text="Part Name:<br />" />
                <asp:TextBox ID="txtDescription" runat="server" CssClass="Box required" Width="480" MaxLength="40" Table="Parts" Field="Description" Placeholder="Part Name"  />
            </td>
        </tr>

        
        <tr>
            <td valign="bottom" align="left">
                <asp:RadioButtonList ID="rdoIsNewPart" runat="server" TextAlign="Right" CssClass="SubSubHead" RepeatLayout="Flow" RepeatDirection="Horizontal" Table="vwPartInfos" Field="IsNewPartText" TargetField="IsNewPart" OnClick="javascript:ToggleUPC();">
                    <asp:ListItem Text="&nbsp;New Part&nbsp;&nbsp;" Value="New Part" Selected="True"/>
                    <asp:ListItem Text="&nbsp;Is Version of Existing Part" Value="Revision" />
                </asp:RadioButtonList>
            </td>
            <td valign="bottom" align="left">
                <asp:Label ID="labelParentPartId" runat="server" CssClass="SubSubHead" Text="Master Part Id:<br />" />
                <DNN:DNNTextSuggest ID="txtParentPartId" runat="server"  
                                    onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                                    CssClass="Box required"  CaseSensitive="false" TextSuggestCssClass="suggest" 
                                    Table="vwPartInfos" Field="ParentPartSuggest" TargetField="ParentPartId" NameIDSplit="-" Width="380" Placeholder="Master Part Id"

                                    queryTmpl8 = "SELECT ID, DESCRIPTION AS NAME FROM vwPARTS WHERE ID LIKE '{0}%' ORDER BY ID"
                                    filterTmpl8 = "ID LIKE '{0}%'"
                                    validation="SELECT COUNT(*) AS HITS FROM vwPARTS WHERE ID = @P0 OR DESCRIPTION = @P1"
                                    

                />
                
            </td>
        </tr>




        <tr>
            <td>
                <asp:Label ID="labelABC" runat="server" CssClass="SubSubHead" Text="ABC Code:<br />" />
                <asp:TextBox ID="txtabc" runat="server" CssClass="Box" Width="140" MaxLength="25" Table="Parts" Field="ABCCode" Placeholder="ABC Code" />
            </td>
            <td>
                <asp:Label ID="labelUPC" runat="server" CssClass="SubSubHead" Text="UPC Code:<br />" />
                <asp:TextBox ID="txtupc" runat="server" CssClass="Box required" Width="140" MaxLength="12" Table="Parts" Field="UPCCode" Placeholder="UPC Code" onkeypress="return isNumberKey(event)"  />
                <asp:LinkButton ID="lnkLookupUPC" runat="server" Text="Lookup from Master Part" OnCommand="DoLookupUPC" />
            </td>
        </tr>
        <tr>
            <td />
            <td><asp:Label ID="lblNote" runat="server" CssClass="Normal" /></td>
        </tr>
<%--        <tr>
            <td colspan="2">
                        <asp:Label ID="labelExtended" runat="server" CssClass="SubSubHead" Text="Extended Description:<br />" />
                <asp:TextBox ID="txtExtended" runat="server" CssClass="Box" Width="580" MaxLength="500" TextMode="MultiLine" Height="100" Table="Parts" Field="ExtendedDescription" Placeholder="Extended Description" />

            </td>
        </tr>
--%>
        <tr>
            <td>
                <asp:CheckBox ID="chkSalsify" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="E-commerce Item" Table="Parts" Field="Salsify" />
            </td>
            <td />
        </tr>

    </table>
</asp:Panel>

<table width="800" cellpadding="4">
    <tr>
        <td align="left" style="width:25%;" valign="middle">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" />
        </td>
        <td align="center" style="width:50%;" valign="middle">
            <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
        </td>
        <td align="right" style="width:25%;" valign="middle">
            <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" />
        </td>
    </tr>
    
</table>

<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
