using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System.Data;
using YourCompany.PDM2019.Components;
using DotNetNuke.UI.WebControls;
using System.Text;

namespace YourCompany.Modules.PDM2019
{
    public partial class PartSpecificationsEditor: LRHPopup
    {
        protected override string PAGE_TITLE { get { return "PDM Part Specifications"; }}

        protected override void SetDefaultValues(DataSet ds)
        {
            lblNoBatteries.Visible = true;

            if (ds.Tables.Contains("vwBatteries"))
            {
                lblNoBatteries.Visible = ds.Tables["vwBatteries"].Rows.Count == 0;
            }

            if (NewRecord(ds, "vwPartSpecifications"))
            {
                ddlColor.SelectedValue = "MULTI";
            }
            else
            {
                chkToggleLabels.Checked = true;
            }

        }

        protected override bool Validate()
        {
            string msg = string.Empty;

            if (chkMultilingual.Checked)
            {
                string other = txtOtherLanguage.Text.Trim();

                if (!chkEnglish.Checked && !chkSpanish.Checked && !chkFrench.Checked && !chkGerman.Checked && string.IsNullOrEmpty(other))
                {
                    msg = "When Multilingual is checked, at least one language must be specified.\r\n";
                }

            }

            
            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        private bool IsDirty
        {
            get
            {
                return bool.Parse(hdnDirty.Value);
            }
            set
            {
                hdnDirty.Value = value.ToString();
                if (value)
                {
                    btnCancel.OnClientClick = null;
                }
            }

        }

        protected void DoSave(object obj, CommandEventArgs args)
        {

            Button btn = (Button)obj;
            bool closeOnly = args.CommandArgument.ToString().Equals("CloseOnly");
            if (closeOnly)
            {

                //Response.Redirect(DotNetNuke.Common.Globals.NavigateURL("", "id", PartId.ToString(), "r", viewRole), true);
                CloseForm();
                return;
            }


            if (!Validate()) return;


            Controller controller = new Controller(entityId, environment);
            DataSet ds = controller.LoadPart(PartId, pnlMain);
            bool newRecord = NewRecord(ds, "vwPartSpecifications");

            Dictionary<string, TableFields> tables = controller.ParseForm(pnlMain);
            PrepTableData(tables, controller);
            SaveTableEdits(tables, "vwPartSpecifications", "Product Development.Specifications", newRecord, controller);
            SaveTableEdits(tables, "vwPartInfos", "Product Development.Specifications", newRecord, controller);
            SaveTableEdits(tables, "vwQAPartInfos", "Product Development.Specifications", newRecord, controller);
            SaveOracleData(controller, tables);
            CloseForm();
        }

        private void SaveOracleData(Controller controller, Dictionary<string, TableFields> tables)
        {
            if (!partStatus.Equals("Approved")) return;

            //if we cam from DoSave, we can drop out if General Description has not changed
            if (tables != null)
            {
                if (!tables["vwPartSpecifications"].HasFieldChanged("GeneralDescription")) return;
            }

            controller.Specifications_Save(PartId, sku, txtGeneralDescription.Text.Trim());
        }
        
        private void PrepTableData(Dictionary<string, TableFields> tables, Controller controller)
        {
            if (!tables.ContainsKey("vwPartSpecifications")) return;

            TableFields table = tables["vwPartSpecifications"];
            if (!table.IsDirty()) return;

            controller.Battery_SaveRemote(this.UserInfo.Username, PartId, table);
            table.RemoveField("BatteryQuantity");
            table.RemoveField("BatterySize");
            table.RemoveField("BatteryIncluded");


        }








        protected void Battery_Delete(object obj, CommandEventArgs args) 
        {
            ImageButton btn = (ImageButton)obj;

            int recordId = Int32.Parse(args.CommandArgument.ToString());
            string batteryFor = btn.Attributes["BatteriesFor"];
            string size = btn.Attributes["BatterySize"];
            int qty = Int32.Parse(btn.Attributes["BatteryQuantity"]);
            bool included = bool.Parse(btn.Attributes["BatteryIncluded"]);

            Controller controller = new Controller(entityId, environment);
            controller.Battery_Delete(this.UserInfo.Username, recordId, PartId, batteryFor, size, qty, included);
            IsDirty = true;
            Battery_Load();
        }

        private bool Battery_Validate()
        {
            string msg = string.Empty;

            Controller controller = new Controller(entityId, environment);
            controller.ValidateText(ddlAddBatterySize, true, "Battery Size", ref msg);
            controller.ValidateNumber(txtAddBatteryQuantity, true, "Battery Quantity", ref msg);

            hdnShowMessage.Value = msg;
            return string.IsNullOrEmpty(msg);
        }

        protected void Battery_Save(object obj, CommandEventArgs args)
        {
            if (!Battery_Validate()) return;

            string batterySize = ddlAddBatterySize.SelectedValue;
            int batteryQty = Int32.Parse(txtAddBatteryQuantity.Text.Trim());
            bool included = chkAddBatteryIncluded.Checked;

            Controller controller = new Controller(entityId, environment);
            controller.Battery_Add(this.UserInfo.Username, PartId, "Part", batterySize, batteryQty, included);
            IsDirty = true;
            Battery_Load();
        
        
            Battery_Toggle(false);
        }

        protected void Battery_Toggle(object obj, CommandEventArgs args)
        {
            bool open = args.CommandArgument.ToString().Equals("Open");
            Battery_Toggle(open);
        }

        private void Battery_Toggle(bool open)
        {
            ddlAddBatterySize.SelectedIndex = 0;
            txtAddBatteryQuantity.Text = string.Empty;
            chkAddBatteryIncluded.Checked = false;

            Controller controller = new Controller(entityId, environment);
            controller.StripErrorStyles(ddlAddBatterySize);
            controller.StripErrorStyles(txtAddBatteryQuantity);
            
            pnlMain.Visible = !open;
            pnlAddBattery.Visible = open;
        }

        private void Battery_Load()
        {
            Controller controller = new Controller(entityId, environment);

            DataSet ds = controller.LoadPart(PartId, "vwBatteries");
            lstBatteries.DataSource = ds.Tables["vwBatteries"];
            lstBatteries.DataBind();
            lblNoBatteries.Visible = ds.Tables["vwBatteries"].Rows.Count == 0;
        }



        protected void Component_Delete(object obj, CommandEventArgs args)
        {
            ImageButton btn = (ImageButton)obj;
            int recordId = Int32.Parse(btn.Attributes["RecordId"]);

            Controller controller = new Controller(entityId, environment);
            string sql = "SELECT * FROM vwPartPackagingComponents WHERE Id = @P0";
            DataTable dt = controller.pdm.ExecuteReader(sql, recordId);
            DataRow dr = dt.Rows[0];

            StringBuilder history = new StringBuilder();
            history.AppendFormat("Description: {0}<br />", controller.pdm.GetValue<string>(dr, "Description", ""));
            history.AppendFormat("Quantity: {0}<br />", controller.pdm.GetValue<string>(dr, "Quantity", ""));
            history.AppendFormat("Size: {0}<br />", controller.pdm.GetValue<string>(dr, "Size", ""));
            history.AppendFormat("Materials: {0}<br />", controller.pdm.GetValue<string>(dr, "MaterialsDisplay", ""));
            history.AppendFormat("Id: {0}", recordId);

            sql = "DELETE FROM PartPackagingComponents WHERE Id = @P0";
            controller.pdm.ExecuteCommand(sql, recordId);

            controller.history.Log(PartId, this.UserInfo.Username, "Product Development.Specifications", "Deleted Component", string.Empty, history.ToString());

            IsDirty = true;
            
            sql = "SELECT * FROM vwPartPackagingComponents WHERE Part_Id = @P0";
            dt = controller.pdm.ExecuteReader(sql, PartId);
            lstComponents.DataSource = dt;
            lstComponents.DataBind();

            UpdateQuality(dt);
            SaveOracleData(controller, null);
        }

        protected void Component_Save(object obj, CommandEventArgs args)
        {
            string msg = string.Empty;
            Controller controller = new Controller(entityId, environment);

            int? quantity = controller.ValidateNumber(txtComponentQuantity, true, "Quantity", ref msg);
            decimal? l = controller.ValidateDecimal(txtComponentL, false, "Length", ref msg);
            decimal? w = controller.ValidateDecimal(txtComponentW, false, "Width", ref msg);
            decimal? h = controller.ValidateDecimal(txtComponentH, false, "Height", ref msg);

            string size = string.Empty;
            if (txtComponentSize.Visible)
            {
                size = controller.ValidateText(txtComponentSize, true, "Size", ref msg);
            }
            else
            {
                if (!l.HasValue && !w.HasValue && !h.HasValue)
                {
                    msg += "Size is required.\r\n";
                    string dummy = string.Empty;
                    controller.ValidateDecimal(txtComponentL, true, "Length", ref dummy);
                    controller.ValidateDecimal(txtComponentW, true, "Width", ref dummy);
                    controller.ValidateDecimal(txtComponentH, true, "Height", ref dummy);
                }
                size = string.Format("{0}\"x{1}\"x{2}\"", l, w, h);
            }
            
            string description = controller.ValidateText(txtComponentDescription, true, "Description", ref msg);
            string materials = controller.ValidateText(ddlComponentMaterial, true, "Materials", ref msg);
            string detail = controller.ValidateText(txtComponentDetails, false, "Details", ref msg);

            hdnShowMessage.Value = msg;

            if (!string.IsNullOrEmpty(msg)) return;

            int recordId = Int32.Parse(btnAddComponentSave.CommandArgument.ToString());

            if (recordId == -1)
            {
                //add
                StringBuilder history = new StringBuilder();
                history.AppendFormat("Description: {0}<br />", description);
                history.AppendFormat("Quantity: {0}<br />", quantity);
                history.AppendFormat("Size: {0}<br />", size);
                history.AppendFormat("Material: {0}<br />", materials);
                history.AppendFormat("Detail: {0}<br />", detail);

                if (!txtComponentSize.Visible) size = null;

                recordId = controller.pdm.InsertRecord("PartPackagingComponents", "Id", "Part_Id", PartId, "Description", description, "Quantity", quantity,
                    "Size", size, "Length", l, "Width", w, "Height", h, "Materials", materials, "CreatedDate", DateTime.Now, "ModifiedDate", DateTime.Now, "Printing", detail);

                history.AppendFormat("Id: {0}", recordId);
                controller.history.Log(PartId, this.UserInfo.Username, "Product Development.Specifications", "Added Component", string.Empty, history.ToString());
            }
            else
            {
                //update
                string sql = "SELECT * FROM vwPartPackagingComponents WHERE Id = @P0";
                DataTable dt = controller.pdm.ExecuteReader(sql, recordId);
                DataRow dr = dt.Rows[0];

                Dictionary<string, FieldChange> edits = new Dictionary<string, FieldChange>();
                edits.Add("Description", new FieldChange(controller.pdm.GetValue<string>(dr, "Description", ""), description));
                edits.Add("Quantity", new FieldChange(controller.pdm.GetValue<int>(dr, "Quantity", 0), quantity.Value));
                edits.Add("Size", new FieldChange(controller.pdm.GetValue<string>(dr, "Size", ""), size));
                edits.Add("Material", new FieldChange(controller.pdm.GetValue<string>(dr, "Materials", ""), materials));
                edits.Add("Details", new FieldChange(controller.pdm.GetValue<string>(dr, "Details", ""), detail));
                edits.Add("Length", new FieldChange(controller.pdm.GetValue<decimal>(dr, "Length", 0), l));
                edits.Add("Width", new FieldChange(controller.pdm.GetValue<decimal>(dr, "Width", 0), w));
                edits.Add("Height", new FieldChange(controller.pdm.GetValue<decimal>(dr, "Height", 0), h));

                if (!txtComponentSize.Visible) size = null;

                controller.pdm.UpdateRecord("PartPackagingComponents", "Id", recordId, "Description", description, "Quantity", quantity, "Size", size,
                                                "Length", l, "Width", w, "Height", h, "Printing", detail,
                                                "Materials", materials, "ModifiedDate", DateTime.Now);

                StringBuilder history = new StringBuilder();
                List<string>fields = new List<string> { "Description", "Quantity", "Size", "Material", "Length", "Width", "Height", "Details"};
                foreach(string field in fields)
                {
                    if (edits[field].HasChanged()) history.AppendFormat("{0}: {1} -> {2}<br />", field, edits[field].oldHistory, edits[field].newHistory);
                }
                history.AppendFormat("Id: {0}", recordId);
                controller.history.Log(PartId, this.UserInfo.Username, "Product Development.Specificiations", "Modified Component", string.Empty, history.ToString());
            }
            
            
            IsDirty = true;

            string sel = "SELECT * FROM vwPartPackagingComponents WHERE Part_id = @P0";
            DataTable dx = controller.pdm.ExecuteReader(sel, PartId);
            lstComponents.DataSource = dx;
            lstComponents.DataBind();


            Component_Toggle(false);
            UpdateQuality(dx);
            SaveOracleData(controller, null);
        }

       
        private void Component_LoadMaterialTypes()
        {
            ddlComponentMaterialType.Items.Clear();

            Controller controller = new Controller(entityId, environment);
            string sql = "SELECT * FROM Lists WHERE TabName = 'Purchasing' AND ListName = 'Material Code' ORDER BY DisplayText";
            DataTable dt = controller.pdm.ExecuteReader(sql);

            List<string> types = new List<string>();
            foreach(DataRow dr in dt.Rows)
            {
                string raw = controller.pdm.GetValue<string>(dr, "DisplayText", "");
                string[] arr = raw.Split('-');
                if (arr.Length > 1)
                {
                    string test = arr[0].Trim();
                    {
                        if (!types.Contains(test))
                        {
                            types.Add(test);
                            ddlComponentMaterialType.Items.Add(new ListItem(test, test));
                        }
                    }
                }
            }

            ListItem ps = new ListItem("Select Material Type", "");
            ddlComponentMaterialType.Items.Insert(0, ps);

            Component_ToggleMaterial();
        }

        protected void Component_Toggle(object obj, CommandEventArgs args)
        {
            bool open = args.CommandArgument.ToString().Equals("Open");

            if (ddlComponentMaterialType.Items.Count == 0) Component_LoadMaterialTypes();
            Component_Toggle(open);

            if (!open) return;

            

            ImageButton btn = (ImageButton)obj;
            int recordId = Int32.Parse(btn.Attributes["RecordId"]);
            btnAddComponentSave.CommandArgument = recordId.ToString();
            lblAddComponent.Text = (recordId == -1) ? "Add Component" : "Edit Component";


            if (recordId == -1)
            {
                txtComponentSize.Visible = false;
                return;
            }

            Controller controller = new Controller(entityId, environment);
            string sql = "SELECT * FROM vwPartPackagingComponents WHERE ID = @P0";
            DataTable dt = controller.pdm.ExecuteReader(sql, recordId);
            if (dt.Rows.Count == 0) return;

            DataRow dr = dt.Rows[0];
            txtComponentQuantity.Text = controller.GetValue<string>(dr, "Quantity", "");
            txtComponentSize.Text = controller.GetValue<string>(dr, "Size", "");
            txtComponentDescription.Text = controller.GetValue<string>(dr, "Description", "");
            txtComponentL.Text = controller.GetValue<decimal>(dr, "Length", 0).ToString();
            txtComponentH.Text = controller.GetValue<decimal>(dr, "Width", 0).ToString();
            txtComponentW.Text = controller.GetValue<decimal>(dr, "Height", 0).ToString();
            txtComponentDetails.Text = controller.GetValue<string>(dr, "Details", string.Empty);

            bool showLegacy = controller.GetValue<bool>(dr, "ShowLegacy", false);
            txtComponentSize.Visible = showLegacy;
            txtComponentL.Visible = !showLegacy;
            txtComponentH.Visible = !showLegacy;
            txtComponentW.Visible = !showLegacy;
            lblComponentL.Visible = !showLegacy;
            lblComponentH.Visible = !showLegacy;
            lblComponentW.Visible = !showLegacy;

            string raw = controller.GetValue<string>(dr, "MaterialsDisplay", "");
            string[] arr = raw.Split(new char[] { '-' }, 2);
            if (arr.Length >= 1)
            {
                try { ddlComponentMaterialType.SelectedValue = arr[0].Trim(); }
                catch { ddlComponentMaterialType.SelectedIndex = 0; }
            }

            if (arr.Length >= 2)
            {
                Component_ToggleMaterial(arr[1].Trim());
            }
            else
            {
                Component_ToggleMaterial(raw);
            }

        }

        private void Component_Toggle(bool open)
        {
            pnlMain.Visible = !open;
            pnlAddComponent.Visible = open;

            txtComponentQuantity.Text = string.Empty;
            txtComponentDescription.Text = string.Empty;
            txtComponentSize.Text = string.Empty;
            ddlComponentMaterialType.SelectedIndex = 0;
            Component_ToggleMaterial();
            txtComponentSize.Visible = true;
            lblComponentL.Visible = true;
            txtComponentL.Visible = true;
            lblComponentW.Visible = true;
            txtComponentW.Visible = true;
            lblComponentH.Visible = true;
            txtComponentH.Visible = true;

            Controller controller = new Controller(entityId, environment);
            controller.StripErrorStyles(txtComponentQuantity);
            controller.StripErrorStyles(txtComponentSize);
            controller.StripErrorStyles(txtComponentDescription);
            controller.StripErrorStyles(ddlComponentMaterialType);
            controller.StripErrorStyles(ddlComponentMaterial);
            controller.StripErrorStyles(txtComponentL);
            controller.StripErrorStyles(txtComponentW);
            controller.StripErrorStyles(txtComponentH);
        }

        protected void Component_ToggleMaterial(object obj, EventArgs args)
        {
            Component_ToggleMaterial();
        }

        private void Component_ToggleMaterial(string selectedValue = "")
        {
            ddlComponentMaterial.Items.Clear();
            ddlComponentMaterial.Items.Add(new ListItem("Select Material", ""));

            string materialType = ddlComponentMaterialType.SelectedValue;
            if (string.IsNullOrEmpty(materialType))
            {
                if (!string.IsNullOrEmpty(selectedValue)) ddlComponentMaterial.Items.Add(new ListItem(selectedValue, selectedValue));
                return;
            }

            materialType += "-%";
            string sql = string.Format("SELECT * FROM Lists WHERE TabName = 'Purchasing' AND ListName = 'Material Code' AND DisplayText LIKE '{0}' ORDER BY DisplayText", materialType);
            Controller controller = new Controller(entityId, environment);
            DataTable dt = controller.pdm.ExecuteReader(sql);

            bool found = false;
            foreach (DataRow dr in dt.Rows)
            {
                string test = controller.pdm.GetValue<string>(dr, "StoredValue", "");
                if (test.Equals(selectedValue)) found = true;
                ddlComponentMaterial.Items.Add(new ListItem(test, test));
            }

            if (!found) ddlComponentMaterial.Items.Add(new ListItem(selectedValue, selectedValue));
            ddlComponentMaterial.SelectedValue = selectedValue;
        }

        private void UpdateQuality(DataTable dt)
        {
            Dictionary<string, string> fields = new Dictionary<string, string>();
            fields.Add("ABS", "PlasticMaterial");
            fields.Add("CoPP (Co-polypropylene)", "PolypropyleneMaterial");
            fields.Add("EVA", "EVAMaterial");
            fields.Add("GPPS (General Purpose Polystyrene)", "GPPSGeneral");
            fields.Add("HDPE (High Density Polyethylene)", "HDPE");
            fields.Add("HIPS (High Impact Polystyrene)", "HIPS");
            fields.Add("LDPE (Low Density Polyethylene)", "LDPE");
            fields.Add("PE (Polyethylene)", "PePolyethylene");
            fields.Add("PET (Polyethylene Terephthalate)", "PETPolyethylene");
            fields.Add("PMMA", "PMMA");
            fields.Add("POM (Polyoxymethylene)", "PomPolyoxmethylene");
            fields.Add("PP (Polypropylene)", "PPPolyethylene");
            fields.Add("PS (Polystyrene)", "PsPolystyrene");
            fields.Add("PVC", "PVC");
            fields.Add("Steel", "MetalMaterial");
            fields.Add("MDF Wood", "WoodMaterial");
            fields.Add("Cotton", "TextilesMaterial");
            fields.Add("Mineral", "MineralMaterial");
            fields.Add("Latex", "LatexMaterial");
            fields.Add("Stainless Steel", "StainlessSteelMaterial");
            fields.Add("Iron", "IronMaterial");
            fields.Add("Beech Wood", "BeechwoodMaterial");
            fields.Add("Pine Wood", "PinewoodMaterial");
            fields.Add("Polyester", "PolyesterMaterial");
            fields.Add("Poly-cotton Blend", "PolyCottonBlendMaterial");
            fields.Add("Rubber", "Thermoplastic");

            
//Thermoplastic Rubber
//Thermoplastic Elastomer (TPE)
//Tetraphenyl Ethylene (TPE)
//Styrene/Butadiene (BS)
//PVC
//PU Foam



//Polyisoprene
//Polyisobutylene
//Polycarbonate
//Polycarbonate
//Polycarbonate
//Polycarbonate
//Polybutene
//PMMA






//EVA

//ABS

            //fields.Add("PLASTIC-ABS", "PlasticMaterial");
            //fields.Add("PLASTIC-CoPP", "PolypropyleneMaterial");
            //fields.Add("PLASTIC-EVA", "EVAMaterial");
            //fields.Add("PLASTIC-GPPS", "GPPSGeneral");
            //fields.Add("PLASTIC-HDPE", "HDPE");
            //fields.Add("PLASTIC-HIPS", "HIPS");
            //fields.Add("PLASTIC-LDPE", "LDPE");
            //fields.Add("PLASTIC-LE", "PePolyethylene");
            //fields.Add("PLASTIC-PET", "PETPolyethylene");
            //fields.Add("PLASTIC-PMMA", "PMMA");
            //fields.Add("PLASTIC-PON", "PomPolyoxmethylene");
            //fields.Add("PLASTIC-PP", "PPPolyethylene");
            //fields.Add("PLASTIC-PS", "PsPolystyrene");
            //fields.Add("PLASTIC-PVC", "PVC");
            //fields.Add("METAL-Steel", "MetalMaterial");
            //fields.Add("WOOD-MDF Wood", "WoodMaterial");
            //fields.Add("FABRIC-Cotton", "TextilesMaterial");
            //fields.Add("MISC-Mineral", "MineralMaterial");
            //fields.Add("MISC-Latex", "LatexMaterial");
            //fields.Add("METAL-Stainless Steel", "StainlessSteelMaterial");
            //fields.Add("METAL-Iron", "IronMaterial");
            //fields.Add("WOOD-Beech Wood", "BeechwoodMaterial");
            //fields.Add("WOOD-Pine Wood", "PinewoodMaterial");
            //fields.Add("FABRIC-Polyester", "PolyesterMaterial");
            //fields.Add("FABRIC-Poly-cotton Blend", "PolyCottonBlendMaterial");
            //fields.Add("MISC-Rubber", "Thermoplastic");

            List<object> updates = new List<object> { "ModifiedDate", DateTime.Now };

            Controller controller = new Controller(entityId, environment);
            string sql = "SELECT * FROM QAPartInfos WHERE PartId = @P0";
            DataTable dx = controller.pdm.ExecuteReader(sql, PartId);
            bool inserting = (dx.Rows.Count == 0);

            if (inserting)
            {
                updates.Add("CreatedDate");
                updates.Add(DateTime.Now);
            }

            foreach (string key in fields.Keys)
            {
                string filter = string.Format("Materials = '{0}'", key);
                DataRow[] arr = dt.Select(filter);
                updates.Add(fields[key]);
                updates.Add(arr.Length > 0);
            }

            if (inserting)
            {
                updates.Add("PartId");
                updates.Add(PartId);
                controller.pdm.InsertRecord("QAPartInfos", "PartId", updates.ToArray());
            }
            else
            {
                controller.pdm.UpdateRecord("QaPartInfos", "PartId", PartId, updates.ToArray());
            }
        }

        protected bool ShowLegacy(object obj, bool flip)
        {
            string raw = obj.ToString();
            if (string.IsNullOrEmpty(raw)) return (flip ? true : false);
            if (raw.Equals("0")) return (flip ? true : false);
            if (raw.Equals("1")) return (flip ? false : true);

            bool test = false;
            bool.TryParse(raw, out test);
            return (flip ? !test : test);
        }
    }
}
 