<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MarketingTabEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.MarketingTabEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/TextEditor.ascx"%>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMoldShareVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

   
    });


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    function AddAward() {
        var newbie = jQuery('[id*="txtAddAward"]').val().trim();
        if (newbie == "") {
            alert("New Award cannot be blank.");
            return false;
        }

        jQuery('[id*="hdnNewAward"]').val(newbie);
        return true;

    }

    function ValidateAwards() {
        var newbie = jQuery('[id*="txtAddAward"]').val().trim();
        if (newbie == "") return true;

        var msg = "Proceed without adding award " + newbie + "?";
        return confirm(msg);
    }    

</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
        <table width="800" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="width:200px;"><asp:Label ID="labelCopy" runat="server" CssClass="SubSubHead" Text="Copy:" /></td>
                <td style="width:200px;"></td>
                <td style="width:200px;"></td>
                <td style="width:200px;"></td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <telerik:RadEditor RenderMode="Lightweight" runat="server" ID="txtCopy" Height="250px" Width="780px" SkinID="MinimalSetOfTools" Table="vwMarketingInfos" Field="Copy" >
                    </telerik:RadEditor>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <asp:Label ID="labelInspectionNotes" runat="server" CssClass="SubSubHead" Text="Product Description/Title:<br />" />
                    <asp:TextBox ID="txtInspectionNotes" runat="server" CssClass="Box" Width="780" MaxLength="40" Table="Parts" Field="Description"  Placeholder="Description / Title" />
                </td>
            </tr>

            <tr>
                <td colspan="2" align="left">
                    <asp:Label ID="labelProductCategory1" runat="server" CssClass="SubSubHead" Text="Product Category 1:<br />" />
                    <asp:DropDownList ID="ddlProductCategory1" runat="server" CssClass="Box" Width="380" Table="vwMarketingInfos" Field="ProductCategory1" />
                </td>
                <td colspan="2" align="left">
                    <asp:Label ID="label6" runat="server" CssClass="SubSubHead" Text="Product Category 2:<br />" />
                    <asp:DropDownList ID="ddlProductCategory2" runat="server" CssClass="Box" Width="380" Table="vwMarketingInfos" Field="ProductCategory2" />
                </td>
            </tr>

            <tr>
                <td colspan="2" align="left">
                    <asp:Label ID="label7" runat="server" CssClass="SubSubHead" Text="Product Category 3:<br />" />
                    <asp:DropDownList ID="ddlProductCategory3" runat="server" CssClass="Box" Width="380" Table="vwMarketingInfos" Field="ProductCategory3" />
                </td>
                <td colspan="2" align="left">
                    <asp:Label ID="label8" runat="server" CssClass="SubSubHead" Text="Product Category 4:<br />" />
                    <asp:DropDownList ID="ddlProductCategory4" runat="server" CssClass="Box" Width="380" Table="vwMarketingInfos" Field="ProductCategory4" />
                </td>
            </tr>

            <tr>
                <td colspan="4">SUB BRAND NYI - NEED SPECS</td>
            </tr>

            <tr>
                <td colspan="4">
                    <asp:Label ID="labelKeywords" runat="server" CssClass="SubSubHead" Text="Keywords:<br />" />
                    <asp:TextBox ID="txtKeywords" runat="server" CssClass="Box" TextMode="MultiLine" MaxLength="300" Width="780" Height="50" Table="vwMarketingInfos" Field="Keywords" Placeholder="Keywords" />
                </td>
            </tr>

            <tr>
                <td colspan="4" align="left">
                    <asp:Label ID="labelAwards" runat="server" CssClass="SubSubHead" Text="Awards:<br />" />
                    <asp:Repeater ID="rptAwards" runat="server">
                        <ItemTemplate>
                            <div style="background-color:#e8e2e2; display:inline; padding:6px 2px 2px 2px;">
                                <asp:Label id="lblAward" runat="server"  CssClass="Normal" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />&nbsp;
                                <asp:ImageButton id="btnDeleteAward" runat="server" ImageUrl="~/images/delete.gif" OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' OnCommand="Award_Delete" />
                            </div>
                            &nbsp;
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtAddAward" runat="server" Width="200" MaxLength="100" CssClass="Box" PlaceHolder="New Award" />
                            <asp:ImageButton ID="btnAddAward" runat="server" ImageUrl="~/images/plus2.gif" OnCommand="Award_Add" OnClientClick="javascript:return AddAward();" />
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <asp:Label ID="label9" runat="server" CssClass="SubSubHead" Text="Product Instructions:<br />" />
                    <asp:TextBox ID="TextBox3" runat="server" CssClass="Box" TextMode="MultiLine" MaxLength="300" Width="780" Height="50" Table="vwMarketingInfos" Field="ProductInstructions" Placeholder="Product Instructions" />
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <asp:Label ID="label10" runat="server" CssClass="SubSubHead" Text="Video:<br />" />
                    <asp:TextBox ID="TextBox4" runat="server" CssClass="Box" TextMode="MultiLine" MaxLength="300" Width="780" Height="50" Table="vwMarketingInfos" Field="Video" Placeholder="Videos" />
                </td>
            </tr>

            <tr>
                <td valign="bottom">
                    <asp:CheckBox Width="195" ID="chkExemptFromTax" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Exempt from Tax" Table="vwMarketingInfos" Field="ExemptFromTax"  />
                </td>
                <td colspan="2" valign="bottom">
                    <asp:RadioButtonList ID="rdoPatent" runat="server" CssClass="SubSubHead" TextAlign="Right" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" Table="vwPartInfos" Field="Patent">
                        <asp:ListItem Text="&nbsp;Patent&nbsp;&nbsp;" Value="Patent" />
                        <asp:ListItem Text="&nbsp;Patent Pending" Value="PatentPending" />
                    </asp:RadioButtonList>
                    
                </td>
                <td valign="bottom">
                    <asp:CheckBox id="chkPatentExists" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="Patent Exists at Part Creation" Table="vwPartInfos" Field="PatentExistsAtCreation" />
                </td>
            </tr>

            <tr>
                <td />
                <td colspan="2" valign="bottom">
                    <asp:RadioButtonList ID="rdoTrademark" runat="server" CssClass="SubSubHead" TextAlign="Right" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Flow" Table="vwPartInfos" Field="Trademark">
                        <asp:ListItem Text="&nbsp;Trademark&nbsp;&nbsp;" Value="Trademark" />
                        <asp:ListItem Text="&nbsp;Register Trademark" Value="RegisterTrademark" />
                    </asp:RadioButtonList>
                </td>
                </td>
            </tr>

        
        </table>
    </asp:Panel>


    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" OnClientClick="javascript:return ValidateAwards();" />
            </td>
        </tr>
    
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<asp:HiddenField ID="hdnNewAward" runat="server" Value="" />
<asp:HiddenField ID="hdnAwards" runat="server" Value="<root />" />