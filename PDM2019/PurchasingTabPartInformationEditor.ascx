<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchasingTabPartInformationEditor.ascx.cs" Inherits="YourCompany.Modules.PDM2019.PurchasingTabPartInformationEditor" %>
<%@ Register assembly="DotNetNuke.WebControls" namespace="DotNetNuke.UI.WebControls" tagprefix="DNN" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/TextEditor.ascx"%>

<style>
    div { padding-left:4px; padding-bottom:4px;
    }
    .suggestParentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}
    .suggestPreferredVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:25px; top:80px; font-size:small; width:300px;}
    .suggestPreferredVendorOffset {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:25px; top:115px; font-size:small; width:300px;}
    .suggestMoldVendor {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:325px; top:200px; font-size:small; width:280px;}
    .suggestMainComponentPartId {border:solid 1px black; text-align:left; background-color:#C8C8C8; padding:5px; position:absolute; left:540px; top:0px; font-size:small; width:300px;}

    .readonly { background-color:#e8e2e2; }
    input.required { border: solid 1px orange; }
    select.required { border: solid 1px orange; }           
</style>


<script language="javascript" type="text/javascript">
    $(document).ready(function () {

        ToggleLabels();

        var msg = jQuery('[id*="hdnShowMessage"]').val();
        if (msg != "") {
            alert(msg);
        }

        jQuery('[id*="hdnShowMessage"]').val("");

        ToggleMagnets();
    });

    function ToggleMagnets() {
        var checked = jQuery('[id*="chkMagnets"]:checked').length > 0;
        if (checked) {
            jQuery('[id*="lnkMagnets"]').show();
            jQuery('[id*="fuMagnets"]').show();
        }
        else {
            jQuery('[id*="lnkMagnets"]').hide();
            jQuery('[id*="fuMagnets"]').hide();
        }
    }


    function ToggleLabels() {

        var checked = jQuery('[id*="chkToggleLabels"]:checked').length > 0;

        jQuery('[id*="label"]').each(function (index) {
            if (checked)
                $(this).show();
            else
                $(this).hide();
        });

        if (checked)
            jQuery('div[class="suggestPreferredVendor"]').attr("class", "suggestPreferredVendorOffset");
        else
            jQuery('div[class="suggestPreferredVendorOffset"]').attr("class", "suggestPreferredVendor");

        //$( "li" ).each(function( index ) {
        //  console.log( index + ": " + $( this ).text() );
        //

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimal(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode == 46) return true;
        return isNumberKey(evt);
    }

    function AddContainerSize() {
        var newbie = jQuery('[id*="ddlNewContainerSize"]').val().trim();
        if (newbie == "") {
            alert("New Container Size cannot be empty.");
            return false;
        }

        jQuery('[id*="hdnNewContainerSize"]').val(newbie);
        return true;

    }

    function ValidateAwards() {
        var newbie = jQuery('[id*="txtAddAward"]').val().trim();
        if (newbie == "") return true;

        var msg = "Proceed without adding award " + newbie + "?";
        return confirm(msg);
    }    

</script>


<asp:Panel ID="pnlMain" runat="server" Width="850">
    <asp:Panel ID="pnlMainContainer" runat="server" style="padding: 4px; border-bottom:solid 1px black;" Height="450" Scrollbars="Auto">
        <table width="800" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="width:200px;"><asp:Label ID="labelCopy" runat="server" CssClass="SubSubHead" Text="Planner User Id:" /></td>
                <td style="width:200px;"><asp:Label ID="labelBuyer" runat="server" CssClass="SubSubHead" Text="Buyer User Id:" /></td>
                <td style="width:200px;"><asp:Label ID="labelSafetyStock" runat="server" CssClass="SubSubHead" Text="Safety Stock:" /></td>
                <td style="width:200px;"><asp:Label ID="labelClassCode" runat="server" CssClass="SubSubHead" Text="Class Code:" /></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtPlanner" runat="server" CssClass="Box" Width="180" MaxLength="25" Placeholder="Planner User Id" Table="vwPurchasingPartInfos" Field="PlannerUserId"  />
                </td>
                <td><asp:TextBox ID="txtBuyer" runat="server" CssClass="Box required" Width="180" MaxLength="25" Placeholder="Buyer User Id" Table="vwPartInfos" Field="BuyerUserId"   /></td>
                <td><asp:TextBox ID="txtSafetyStock" runat="server" CssClass="Box required" Width="180" MaxLength="10" Placeholder="Safety Stock" Table="vwPurchasingPartInfos" Field="SafetyStock" Format="Number" onkeypress="return isNumberKey(event);"  /> </td>
                <td><asp:DropDownList ID="ddlClassCode" runat="server" CssClass="Box" Width="180" Table="vwPurchasingPartInfos" Field="ClassCode" /></td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <asp:Label ID="labelPreferredVendor" runat="server" CssClass="SubSubHead" Text="Preferred Vendor:<br />" />
                    <DNN:DNNTextSuggest ID="txtPreferredVendor" runat="server" 
                        onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                        CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggestPreferredVendor" 
                        Table="vwPartInfos" Field="PreferredVendorSuggest" TargetField="PreferredVendor" NameIDSplit="-" Width="580" Placeholder="Preferred Vendor" 
                        queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                        filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                        validation = "SELECT COUNT(*) AS HITS FROM vwVENDORS WHERE ID = @P0 OR NAME = @P1"
                        />

                </td>
            </tr>

            <tr>
                <td colspan="4" align="left">
                    <asp:Label ID="labelProductCategory1" runat="server" CssClass="SubSubHead" Text="HTS Code:<br />" />
                    <asp:DropDownList ID="ddlHTSCode" runat="server" CssClass="Box required" Width="780" Table="vwPurchasingPartInfos" Field="HtsCode" />
                </td>
            </tr>

            

            <tr>
                <td>
                    <asp:Label ID="labelMaterialCode" runat="server" CssClass="SubSubHead" Text="Material Code:<br />" />
                    <asp:DropDownList ID="ddlMaterialCode" runat="server" Width="180" CssClass="Box required" Table="vwPurchasingPartInfos" Field="MaterialCode" />
                </td>
                <td>
                    <asp:Label ID="labelEngin" runat="server" CssClass="SubSubHead" Text="Engineering Master Id:<br />" />
                    <asp:TextBox ID="txtEngineeringMaster" runat="server" Width="180" CssClass="Box" MaxLength="25" Placeholder="Engineering Master Id" Table="vwPurchasingPartInfos" Field="EngineeringMasterId" />
                </td>
                <td >
                    <asp:Label id="labelPrimaryWarehouse" runat="server" CssClass="SubSubHead" Text="Primary Warehouse:<br />" />
                    <asp:DropDownList ID="ddlPrimaryWarehouse" runat="server" CssClass="Box required" Width="180" Table="vwPurchasingPartInfos" Field="PrimaryWarehouse" />
                </td>
                <td>
                    <asp:Label ID="labelForeign" runat="server" CssClass="SubSubHead" Text="Foreign Warehouse:<br />" />
                    <asp:DropDownList ID="ddlForeignWarehouse" runat="server" CssClass="Box" Width="180" Table="vwPurchasingPartInfos" Field="ForeignWarehouse" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="labelProductStatus" runat="server" CssClass="SubSubHead" Text="Product Status:<br />" />
                    <asp:DropDownList ID="ddlProductStatus" runat="server" CssClass="Box" Width="180" Table="vwPurchasingPartInfos" Field="ProductStatus"  />
                </td>
                <td>
                    <asp:Label ID="labelCommodityCode" runat="server" CssClass="SubSubHead" Text="Commodity Code:<br />" />
                    <asp:DropDownList ID="ddlCommodityCode" runat="server" CssClass="Box" Width="180" Table="vwPurchasingPartInfos" Field="CommodityCode" />
                </td>
                <td>
                    <asp:Label ID="labelCountryOfOrigin" runat="server" CssClass="SubSubHead" Text="Country of Origin:<br />" />
                    <asp:DropDownList ID="ddlCountryOfOrigin" runat="server" CssClass="Box required" Width="180" Table="vwPurchasingPartInfos" Field="CountryOfOrigin" />
                </td>
                <td>
                    <asp:Label ID="labelPortofOrigin" runat="server" CssClass="SubSubHead" Text="Port of Origin:<br />" />
                    <asp:DropDownList ID="ddlPortOfOrigin" runat="server" CssClass="Box required" Width="180" Table="vwPurchasingPartInfos" Field="PortOfOrigin" />
                </td>
                
            </tr>
            
            <tr>
                <td colspan="2" valign="bottom" align="left">
                    <asp:Label ID="labelContainer" runat="server" CssClass="SubSubHead" Text="Container Size:<br />" />
                    <asp:Repeater ID="rptContainer" runat="server">
                        <ItemTemplate>
                            <asp:Panel ID="pnlContainer" runat="server" style="background-color:#e8e2e2; display:inline; padding:6px 2px 2px 2px;margin:1px;" Visible='<%# DataBinder.Eval(Container.DataItem, "show") %>'>
                                <asp:Label id="lblContainerSize" runat="server"  CssClass="Normal" Text='<%# DataBinder.Eval(Container.DataItem, "ContainerSize") %>' />&nbsp;
                                <asp:ImageButton id="btnDeleteContainerSize" runat="server" ImageUrl="~/images/delete.gif" OnClientClick="javascript:return confirm('Are you sure you wish to delete this record?');" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' OnCommand="ContainerSize_Delete" 
                                        RecordId='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>'
                                        ContainerSize = '<%# DataBinder.Eval(Container.DataItem, "ContainerSize") %>'
                                        deleted = '<%# DataBinder.Eval(Container.DataItem, "deleted") %>'
                                        show = '<%# DataBinder.Eval(Container.DataItem, "show") %>'

                                />
                            </asp:Panel>

                        </ItemTemplate>
                        <FooterTemplate>
                            &nbsp;
                            <asp:DropDownList ID="ddlNewContainerSize" runat="server"  CssClass="Box" />
                            <asp:ImageButton ID="btnAddNewContainer" runat="server" ImageUrl="~/images/plus2.gif" OnCommand="ContainerSize_Add" OnClientClick="return AddContainerSize();" />
                        </FooterTemplate>
                    </asp:Repeater>
                    
                </td>
                <td valign="bottom">
                    <asp:CheckBox ID="chkHoldReceipts" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Hold Receipts" Table="vwPurchasingPartInfos" Field="HoldReceiptsCheck"  />
                </td>
                <td valign="bottom">
                    <asp:CheckBox ID="chkObsolete" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Obsolete" Table="vwPurchasingPartInfos" Field="Obsolete" />
                </td>
            </tr>

            <tr>
                <td align="right" valign="bottom">
                <asp:CheckBox ID="chkMagnets" runat="server" CssClass="SubSubHead" TextAlign="Right" Text="&nbsp;Anti-dumping:" Table="Magnets" Field="ContainsMagnets" onclick="javascript:ToggleMagnets();" />
                </td>
                <td colspan="2">
                <asp:HyperLink ID="lnkMagnets" runat="server" Table="Magnets" Field="FileUrl" Display="OriginalFileName" />
                <asp:FileUpload ID="fuMagnets" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="4" >
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="HeaderRow">
                        <tr>
                            <td style="width:755px;">Components:</td>
                            <td style="width:25px; padding:2px 2px 0px 0px;" align="right"><asp:ImageButton id="btnAddMold" runat="server" ImageUrl="~/images/plus2.gif"  OnCommand="Mold_Edit"  /></td>
                        </tr>
                    </table>
                    <asp:Repeater id="lstMolds" runat="server" >
                        <ItemTemplate>
                            <asp:Panel ID="pnlMold" runat="server" visible='<%# DataBinder.Eval(Container.DataItem, "style") %>' style="border-bottom:dotted 1px #e8e2e2;" Width="780" >
                            <asp:ImageButton ID="btnEditMold" runat="server" ImageUrl="~/images/edit.gif" RecordId='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' OnCommand="Mold_Edit"  
                                            NumberOfMolds = '<%# DataBinder.Eval(Container.DataItem, "NumberOfMolds") %>'
                                            DateMovedText = '<%# DataBinder.Eval(Container.DataItem, "DateMovedText") %>'
                                            VendorSuggest = '<%# DataBinder.Eval(Container.DataItem, "VendorSuggest") %>'
                                            TrackingId = '<%# DataBinder.Eval(Container.DataItem, "TrackingId") %>'
                                            Cost='<%# DataBinder.Eval(Container.DataItem, "Cost") %>'
                                            CostText='<%# DataBinder.Eval(Container.DataItem, "CostText") %>'
                                            Active='<%# DataBinder.Eval(Container.DataItem, "Active") %>'
                                            AgreementFile='<%# DataBinder.Eval(Container.DataItem, "AgreementFile") %>'
                                            AgreementUrl='<%# DataBinder.Eval(Container.DataItem, "AgreementUrl") %>'
                                            AgreementPath = '<%# DataBinder.Eval(Container.DataItem, "AgreementPath") %>'
                                            ActiveImage = '<%# DataBinder.Eval(Container.DataItem, "ActiveImage") %>'
                                            SnapshotFile = '<%# DataBinder.Eval(Container.DataItem, "SnapshotFile") %>'
                                            SnapshotUrl = '<%# DataBinder.Eval(Container.DataItem, "SnapshotUrl") %>'
                                            SnapshotPath = '<%# DataBinder.Eval(Container.DataItem, "SnapshotPath") %>'
                                            MoldMaterial = '<%# DataBinder.Eval(Container.DataItem, "MoldMaterial") %>'
                                            ProductMaterial = '<%# DataBinder.Eval(Container.DataItem, "ProductMaterial") %>'
                                            MoldType = '<%# DataBinder.Eval(Container.DataItem, "MoldType") %>'
                                            Slides = '<%# DataBinder.Eval(Container.DataItem, "Slides") %>'
                                            SlidesImage = '<%# DataBinder.Eval(Container.DataItem, "SlidesImage") %>'

                                            Deleted = '<%# DataBinder.Eval(Container.DataItem, "deleted") %>'
                                            updated = '<%# DataBinder.Eval(Container.DataItem, "updated") %>'
                            />
                            <asp:ImageButton ID="btnDeleteMold" runat="server" ImageUrl="~/images/delete.gif" OnClientClick="javascript:return confirm('Are you sure you want to delete this record?');" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "RecordId") %>' OnCommand="Mold_Delete"  />
                            <asp:Label ID="Label4" runat="server" Width="700" CssClass="SubSubHead" Text='<%# DataBinder.Eval(Container.DataItem, "VendorSuggest") %>' />
                            <br />

                            <asp:Label ID="lblMoldIdlbl" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="125" Text="Mold Id:" />
                            <asp:Label ID="Label5" runat="server"  Width="125" Text='<%# DataBinder.Eval(Container.DataItem, "TrackingId") %>' />
                            <asp:Label ID="lblMoldsLabel" runat="server" CssClass="SubSubHead" width="100" Text='Cavities:' />
                            <asp:Label ID="lblMolds" runat="server" CssClass="Normal" Width="90" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfMolds") %>' />
                            <asp:Label ID="lblDateMovedlbl" runat="server" CssClass="SubSubHead" Width="100" Text="Date Moved:" />
                            <asp:Label ID="Label6" runat="server" Width="90" Text='<%# DataBinder.Eval(Container.DataItem, "DateMovedText") %>' />
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "ActiveImage") %>' />
                            <asp:Label ID="lblActive" runat="server" CssClass="SubSubHead" Text="Active" />
                            <br />

                            <asp:Label ID="lblMoldMateriallbl" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="150" Text="Mold Material:" />
                            <asp:Label ID="lblMoldMaterial" runat="server" CssClass="Normal" Width="200" Text='<%# DataBinder.Eval(Container.DataItem, "MoldMaterial") %>' />
                            <asp:Label ID="lblProdutM" runat="server" CssClass="SubSubHead" Width="125" Text="Product Material:" />
                            <asp:Label ID="lblProductMaterial" runat="server" CssClass="Normal" Width="155" Text='<%# DataBinder.Eval(Container.DataItem, "ProductMaterial") %>' />
                            <asp:Image ID="imgSlides" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "SlidesImage") %>' />
                            <asp:Label ID="lblSlides" runat="server" CssClass="SubSubHead" Text="Slides" />
                            <br />

                            <asp:Label ID="lblMoldTypeLabel" runat="server" CssClass="SubSubHead" style="padding-left:50px;" width="150" Text="Mold Type:" />
                            <asp:Label ID="lblMoldType" runat="server" CssClass="Normal" Width="200" Text='<%# DataBinder.Eval(Container.DataItem, "MoldType") %>' />
                            <asp:Label ID="lblCostText" runat="server" CssClass="SubSubHead" Width="125" Text="Cost:" />
                            <asp:Label ID="lblCost" runat="server" Width="75" Text='<%# DataBinder.Eval(Container.DataItem, "CostText") %>' />
                            <br />

                            <asp:Label ID="lblspace" runat="server" Width="50" Text="&nbsp;" />
                            <asp:HyperLink ID="lnkAgreement" runat="server" Target="_blank"
                                            ToolTip='<%# DataBinder.Eval(Container.DataItem, "AgreementFile") %>'
                                            NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "AgreementUrl") %>'
                                            Visible='<%# ShowAgreement(DataBinder.Eval(Container.DataItem, "AgreementUrl")) %>'
                                            ImageUrl="~/images/FileManager/Icons/file.gif" />
                            <asp:Label ID="lblAgreement" runat="server" CssClass="SubSubHead" width="150" Text="Agreement File" />
                            <asp:Label ID="lbspace" runat="server" Width="184" Text="&nbsp;" />
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank"
                                            ToolTip='<%# DataBinder.Eval(Container.DataItem, "SnapshotFile") %>'
                                            NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "SnapshotUrl") %>'
                                            Visible='<%# ShowAgreement(DataBinder.Eval(Container.DataItem, "SnapshotUrl")) %>'
                                            ImageUrl="~/images/FileManager/Icons/file.gif" />
                            <asp:Label ID="Label7" runat="server" CssClass="SubSubHead" Text="Snapshot File" />

                            
                            
                            
                            </asp:Panel>
                        </ItemTemplate>
                        
                        
                    </asp:Repeater>
                    <asp:Label ID="lblNoMolds" runat="server" Text="No Molds have been added to this part." />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Label ID="labelGeneralDescription" runat="server" CssClass="SubSubHead" Text="Specification:<br />" />
                    <asp:TextBox ID="txtGeneralDescription" runat="server" CssClass="Box" Width="780" Height="150" TextMode="MultiLine" Table="vwPartSpecifications" Field="GeneralDescription" />
                </td>
            </tr>
        </table>
    </asp:Panel>


    <table width="800" cellpadding="4">
        <tr>
            <td align="left" style="width:25%;" valign="middle">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClientClick="javascript:dnnModal.closePopUp(false, '');return false;" OnCommand="DoSave" CommandArgument="CloseOnly" />
            </td>
            <td align="center" style="width:50%;" valign="middle">
                <asp:CheckBox ID="chkToggleLabels" runat="server" TextAlign="Right" Text="&nbsp;Show Labels" onclick="javascript:ToggleLabels();" />
            </td>
            <td align="right" style="width:25%;" valign="middle">
                <asp:Button ID="btnSubmit" runat="server" Text="Save Edits" OnCommand="DoSave" CommandArgument="SaveAndClose" OnClientClick="javascript:return ValidateAwards();" />
            </td>
        </tr>
    
    </table>
</asp:Panel>

<asp:Panel ID="pnlAddMold" runat="server" Visible="false">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td style="height:450px;" align="center" valign="middle">
                <asp:Panel ID="pnlMoldEditor" runat="server" style="padding:4px; border:solid 1px black;" Width="520">
                    <table width="500" cellpadding="4" cellspacing="0" border="0">
                        <tr>
                            <td style="width:100%; border-bottom:solid 1px black;" colspan="4">
                                <asp:Label id="lblEditMold" runat="server" CssClass="Head" Text="Add Mold"  />
                            </td>
                        </tr>

                        <tr>
                            <td style="width:125px;" valign="bottom">
                                <asp:Label ID="lblNumberofmolds" runat="server" CssClass="SubSubHead" Text="# of Cavities:<br />" />
                                <asp:TextBox ID="txtNumberOfMolds" runat="server" CssClass="Box" Width="75" MaxLength="10" onkeypress="return isNumberKey(event);" />
                            </td>
                            <td style="width:125px;" valign="bottom">
                                <asp:Label ID="lblDateMoved" runat="server" CssClass="SubSubHead" Text="Date Moved:<br />" />
                                <telerik:RadDatePicker id="dtDateMoved" width="105" runat="server" CssClass="Box" Placeholder="Date Moved"  />
                            </td>
                            <td style="width:125px;" align="right" valign="bottom">
                                <asp:CheckBox ID="chkSlides" runat="server" TextAlign="Right" CssClass="SubSubHead" Text="&nbsp;Slides&nbsp;&nbsp;" />
                            </td>
                            <td style="width:125px;" align="right" valign="bottom">
                                <asp:CheckBox runat="server" ID="chkMoldActive" TextAlign="Right" CssClass="SubSubHead" Text="&nbsp;Active" />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblMoldVendor" runat="server" CssClass="SubSubHead" Text="Vendor:<br />" />
                                    <DNN:DNNTextSuggest ID="txtMoldVendor" runat="server" 
                                        onpopulateondemand="DoLookup" MaxSuggestRows="10" 
                                        CssClass="Box"  CaseSensitive="false" TextSuggestCssClass="suggestMoldVendor" 
                                        NameIDSplit="-" Width="230" Placeholder="Vendor"
                                         queryTmpl8 = "SELECT ID, NAME FROM vwVENDORS WHERE ((ID LIKE '{0}%') OR (NAME LIKE '{0}%')) ORDER BY NAME"
                                         filterTmpl8 = "ID LIKE '{0}%' OR NAME LIKE '{0}%'"
                                         validation = "SELECT COUNT(*) AS HITS FROM vwVENDORS WHERE ID = @P0 OR NAME = @P1"
                                        />
                            </td>
                            <td colspan="2">
                                <asp:Label ID="lblTracking" runat="server" CssClass="SubSubHead" Text="Mold Id:<br />" />
                                <asp:TextBox ID="txtTrackingId" runat="server" CssClass="Box" Width="230" MaxLength="500" Placeholder="Mold Id" />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblMoldMaterial" runat="server" CssClass="SubSubHead" Text="Mold Material:<br />" />
                                <asp:DropDownList ID="ddlMoldMaterial" runat="server" CssClass="Box" Width="230" />
                            </td>
                            <td colspan="2">
                                <asp:Label ID="Label1" runat="server" CssClass="SubSubHead" Text="Product Material:<br />" />
                                <asp:DropDownList ID="ddlProductMaterial" runat="server" CssClass="Box" Width="230" />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:Label ID="Label2" runat="server" CssClass="SubSubHead" Text="Mold Type:<br />" />
                                <asp:DropDownList ID="ddlMoldType" runat="server" CssClass="Box" Width="230" />
                            </td>
                            <td align="left">
                                <asp:Label ID="lblMoldCost" runat="server" CssClass="SubSubHead" Text="Cost:<br />" />
                                <asp:TextBox ID="txtMoldCost" runat="server" CssClass="Box" Width="75" MaxLength="10" onkeypress="return isDecimal(event);" Placeholder="Cost" style="text-align:right;" />
                            </td>
                            <td align="right" valign="bottom">
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <asp:HyperLink ID="lnkMoldAgreement" runat="server" Target="_blank" ImageUrl="~/images/FileManager/Icons/file.gif" />
                                <asp:Label ID="lblMoldAgreement" runat="server"  CssClass="SubSubHead" Text="Agreement File:<br />" />
                                
                                <asp:FileUpload runat="server" ID="fuMoldAgreement" />
                            </td>
                            <td colspan="2">
                                <asp:HyperLink ID="lnkSnapshotFile" runat="server" Target="_blank" ImageUrl="~/images/FileManager/Icons/file.gif" />
                                <asp:Label ID="Label3" runat="server"  CssClass="SubSubHead" Text="Snapshot File:<br />" />
                                <asp:FileUpload runat="server" ID="fuSnapshotFile" />
                            </td>
                        </tr>
                        <tr>
                            <td style="border-top:solid 1px black;padding-top:10px;" valign="middle" align="left" colspan="2">
                                <asp:Button ID="btnCancelMold" runat="server" Text="Cancel" OnCommand="Mold_Toggle" CommandArgument="Close" Width="75" />        
                            </td>
                            <td style="border-top:solid 1px black;padding-top:10px;" valign="middle" align="right" colspan="2">
                                <asp:Button ID="btnSaveMolde" runat="server" Text="Save" Width="75" OnCommand="Mold_Save" />
                            </td>
                        </tr>
                    </table>
                    
                    
                    <br />



                    
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>


<asp:HiddenField id="hdnShowMessage" runat="server" Value="" />
<asp:HiddenField ID="hdnDirty" runat="server" Value="false" />
<asp:HiddenField ID="hdnNewContainerSize" runat="server" Value="" />
