﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

 namespace Salsify
{
    public class DataMover 
    {
        string environment { get; set; }
        string entityId { get; set; }
        DataMoverBase salsify { get; set; }

        public DataMover(string EntityId, string Environment)
        {
            environment = Environment;
            entityId = EntityId;

            //placeholder only for h2m
            if (entityId.Equals("LRH"))
                salsify = new DataMoverLRH(environment);
            else
                salsify = new DataMoverH2M(environment); 
        }

        public void Import(string fileName)
        {
            salsify.Import(fileName);
        }

        public string Export(string folderName, int? year = null)
        {
            return salsify.Export(folderName, year);
        }
    }
}
