﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Salsify
{
    
    class DataMoverLRH : DataMoverBase
    {

        public DataMoverLRH(string Environment) : base(Environment)
        {
            entityId = "LRH";
            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            pdm = new SvcLibrary.DBHandler(config, null, environment, "LRH_PDM");
            dcms = new SvcLibrary.DBHandler(config, null, environment, "LRHDCMS11");
            vmfg = new SvcLibrary.DBHandler(config, null, environment, "VMLREI11");


        }

        //protected override DataSet LoadPart(int partId, params string[] tables)
        //{
        //    tables = new string[] { "Parts", "vwPartInfos", "vwPartSpecifications", "vwBatteries", "vwMarketingInfos", "vwCreativeInfos",
        //                                                        "vwPurchasingPartInfos"};

            
        //    return base.LoadPart(partId, tables);
        //}

        protected override void Import(DataTable sheet1, Dictionary<string, string> colMap, DataTable skus, params string[] tables)
        {
            tables = new string[] { "vwPartInfos", "vwPartSpecifications", "vwBatteries", "vwMarketingInfos", "vwCreativeInfos",
                                                                    "vwPurchasingPartInfos"};
    
            base.Import(sheet1, colMap, skus, tables);
        }

        protected override DataSet ExportFetchParts(int year, params string[] tables)
        {
            tables = new string[] { "vwPartInfos", "vwPartSpecifications", "vwBatteries", "vwMarketingInfos", "vwCreativeInfos", "vwPurchasingPartInfos", "vwQAPartInfos", "Awards" };

            DataSet ds = base.ExportFetchParts(year, tables);

            //customer specific skus
            string sql = "SELECT * FROM ( SELECT C.PART_ID AS PartId, C.CUSTOMER_ID, C.CUSTOMER_PART_ID, CASE WHEN C.CUSTOMER_ID IN ('14329', '18804', '45788') THEN 1 ELSE 2 END AS SORT_ORDER, "
                       + "COALESCE(C.UNIT_PRICE_1, C.DEFAULT_UNIT_PRICE) AS UNIT_PRICE, CASE WHEN P.PRODUCT_CODE = 'EI' AND C.CUSTOMER_ID = '20330' THEN 'EDUBP' "
                       + "WHEN P.PRODUCT_CODE = 'LR' AND C.CUSTOMER_ID = '20330' THEN 'LEAFJ' WHEN P.PRODUCT_CODE = 'EI' AND C.CUSTOMER_ID = '14329' THEN 'EDUS9' "
                       + "WHEN P.PRODUCT_CODE = 'LR' AND C.CUSTOMER_ID = '14329' THEN 'LEHK0' ELSE NULL END AS VENDOR_CODE "
                       + "FROM  CUSTOMER_PRICE C INNER JOIN PART P ON C.PART_ID = P.ID WHERE C.CUSTOMER_ID IN ('14329', '18804', '45788', '20330', 'ED100216', '49585')   "
                       + ") ORDER BY PartID, SORT_ORDER ";
            DataTable dt = vmfg.ExecuteReader(sql);
            dt.TableName = "CUSTOMER_PRICE";
            ds.Tables.Add(dt);


            return ds;
        }
    
    
    }
}
