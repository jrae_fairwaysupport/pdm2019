﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Salsify
{
    
    public abstract partial class DataMoverBase 
    {
     



        protected virtual DataSet ExportFetchParts(int year, params string[] tables)
        {
            //if (year == null) year = DateTime.Today.Year;
            //string sql = "select PART_ID from lr_catalog_page where ed_cat_yr = @P0";
            //string sql = "SELECT PART_ID FROM PARTS WHERE Salsify = 1";
            //DataTable skus = vmfg.ExecuteReader(sql); //, year);
            //return FetchParts(skus, "PART_ID", tables);

            string sql = "SELECT PartId FROM Parts WHERE Salsify = 1 AND Status = 'Approved'";
            DataTable skus = pdm.ExecuteReader(sql);
            return FetchParts(skus, "PartId", tables);
        }


        private DataSet ExportGetTemplate()
        {
            DataSet ds = new DataSet();
            
            //placeholder for data
            DataTable dt = new DataTable();
            dt.TableName = "Sheet1";
            ds.Tables.Add(dt);

            DataTable hasHeaders = new DataTable();
            hasHeaders.TableName = "HasHeaders";
            hasHeaders.Columns.Add("Worksheet", System.Type.GetType("System.String"));
            hasHeaders.Rows.Add("Sheet1");
            ds.Tables.Add(hasHeaders);

            DataTable spreadsheetHeaders = new DataTable();
            spreadsheetHeaders.TableName = "SpreadsheetHeaders";
            spreadsheetHeaders.Columns.Add("Worksheet", System.Type.GetType("System.String"));
            spreadsheetHeaders.Columns.Add("Index", System.Type.GetType("System.Int32"));
            spreadsheetHeaders.Columns.Add("ColumnName", System.Type.GetType("System.String"));
            ds.Tables.Add(spreadsheetHeaders);

            return ds;
        }

        private void ExportAddSku(DataSet ds, string sku)
        {
            DataTable dt = ds.Tables["Sheet1"];
            if (!dt.Columns.Contains("SKU"))
            {
                dt.Columns.Add("SKU", System.Type.GetType("System.String"));

                DataTable hd = ds.Tables["SpreadsheetHeaders"];
                hd.Rows.Add("Sheet1", dt.Columns.Count, "SKU");
            }

            DataRow newRow = dt.NewRow();
            newRow["SKU"] = sku;
            dt.Rows.InsertAt(newRow, 0);
        }


        private void ExportField(DataSet xl, string colName, Type dtType, DataSet ds, string tableName, string srcCol)
        {
            DataTable dt = xl.Tables["Sheet1"];
            if (!dt.Columns.Contains(colName))
            {
                dt.Columns.Add(colName, dtType);

                DataTable hd = xl.Tables["SpreadsheetHeaders"];
                hd.Rows.Add("Sheet1", dt.Columns.Count, colName);
            }

            DataTable src = ds.Tables[tableName];
            if (src.Rows.Count == 0) return;

            DataRow dr = src.Rows[0];
            if (!dr.IsNull(srcCol))
            {
                switch (dtType.Name)
                {
                    case "Decimal":

                        dt.Rows[0][colName] = Math.Round(pdm.GetValue<decimal>(dr, srcCol, 0), 2);
                        break;
                    default:
                        dt.Rows[0][colName] = dr[srcCol];
                        break;
                }
            }
        }

        private void ExportField(DataSet xl, string colName, Type dtType, string value)
        {
            DataTable dt = xl.Tables["Sheet1"];
            if (!dt.Columns.Contains(colName))
            {
                dt.Columns.Add(colName, dtType);

                DataTable hd = xl.Tables["SpreadsheetHeaders"];
                hd.Rows.Add("Sheet1", dt.Columns.Count, colName);
            }

            dt.Rows[0][colName] = value;

        }

        private void ExportCustomerSku(DataSet xl, string colName, DataSet ds, params string[] customerIds)
        {
            string instr = string.Empty;
            foreach(string customerId in customerIds)
            {
                if (!string.IsNullOrEmpty(instr)) instr += ", ";
                instr += string.Format("'{0}'", customerId);
            }

            DataTable dt = ds.Tables["CUSTOMER_PRICE"];
            DataRow[] arr = dt.Select(string.Format("CUSTOMER_ID IN ({0})", instr));

            string sku = string.Empty;
            if (arr.Length > 0) sku = vmfg.GetValue<string>(arr[0], "CUSTOMER_PART_ID", string.Empty);
            ExportField(xl, colName, System.Type.GetType("System.String"), sku);
        }

        

        private void ExportPlaceholder(DataSet xl, string colName)
        {
            DataTable dt = xl.Tables["Sheet1"];
            if (!dt.Columns.Contains(colName))
            {
                dt.Columns.Add(colName, System.Type.GetType("System.String"));

                DataTable hd = xl.Tables["SpreadsheetHeaders"];
                hd.Rows.Add("Sheet1", dt.Columns.Count, colName);
            }

            dt.Rows[0][colName] = "PLACEHOLDER";
        }

        private void ExportUnitPrice(DataSet xl, string colName, DataSet ds, params string[] customerIds)
        {
            List<string> instr = pdm.ConvertToInstring(customerIds.ToList(), true, "jtrtmp");
            string filter = string.Format("CUSTOMER_ID IN ({0})", instr[0]);

            DataTable prices = ds.Tables["CUSTOMER_PRICE"];
            if (prices == null)
            {
                ExportPlaceholder(xl, colName);
                return;
            }

            decimal unitPrice = -1;
            DataRow[] arr = prices.Select(filter);
            if (arr.Length > 0)
            {
                unitPrice = pdm.GetValue<decimal>(arr[0], "UNIT_PRICE", -1);
            }
            if (unitPrice > -1) ExportField(xl, colName, System.Type.GetType("System.Decimal"), unitPrice.ToString());
        }

        private void ExportGTIN(DataSet xl, string colName, DataSet ds)
        {
            string upc = string.Empty;
            DataTable src = ds.Tables["DCMS"];
            if (src != null && src.Rows.Count > 0)
            {
                upc = pdm.GetValue<string>(src.Rows[0], "UPC", string.Empty);
            }

            if (string.IsNullOrEmpty(upc))
            {
                ExportField(xl, "GTIN", System.Type.GetType("System.String"), string.Empty);
                return;
            }



            upc = upc.Replace(" ", "").Trim().PadLeft(12, '0').Substring(0, 11);
            string raw = "20" + upc;

            int total = 0;
            int multiplier = 1;
            int weight = 3;

            for (int i = raw.Length - 1; i >= 0; i -= 1)
            {
                if (multiplier == 1)
                    multiplier = weight;
                else
                    multiplier = 1;

                string number = raw.Substring(i, 1);
                total += (Int32.Parse(number) * multiplier);
            }

            total = total * 9;
            string check = total.ToString();
            check = check.Substring(check.Length - 1, 1);
            raw += check;

            ExportField(xl, "GTIN", System.Type.GetType("System.String"), raw);
        }

        private void ExportList(DataSet xl, string colName, DataSet ds, string tableName, string fieldName, string delim = "|")
        {
            DataTable dt = xl.Tables["Sheet1"];
            if (!dt.Columns.Contains(colName))
            {
                dt.Columns.Add(colName, System.Type.GetType("System.String"));

                DataTable hd = xl.Tables["SpreadsheetHeaders"];
                hd.Rows.Add("Sheet1", dt.Columns.Count, colName);
            }

            string value = string.Empty;
            foreach (DataRow dr in ds.Tables[tableName].Rows)
            {
                string newVal = pdm.GetValue<string>(dr, fieldName, string.Empty);
                if (!string.IsNullOrEmpty(newVal))
                {
                    if (!string.IsNullOrEmpty(value)) value += delim;
                    value += newVal;
                }
            }
            dt.Rows[0][colName] = value;


        }

        //private void ExportBatteries(DataSet xl, string colName, DataSet ds)
        //{
        //    DataTable dt = xl.Tables["Sheet1"];
        //    if (!dt.Columns.Contains(colName))
        //    {
        //        dt.Columns.Add(colName, System.Type.GetType("System.String"));

        //        DataTable hd = xl.Tables["SpreadsheetHeaders"];
        //        hd.Rows.Add("Sheet1", dt.Columns.Count, colName);
        //    }

        //    DataTable dtBatteries = ds.Tables["vwBatteries"];
        //    string value = string.Empty;
        //    foreach (DataRow dr in dtBatteries.Rows)
        //    {
        //        string batteryType = "Alkaline";
        //        int batteryQuantity = pdm.GetValue<int>(dr, "BatteryQuantity", 0);
        //        string batterySize = pdm.GetValue<string>(dr, "BatterySize", string.Empty);
        //        bool batteryIncluded = pdm.GetValue<bool>(dr, "BatteryIncluded", false);

        //        if (!string.IsNullOrEmpty(value)) value += ";";
        //        value += string.Format("{0}|{1}|{2}|{3}", batteryType, batterySize, batteryQuantity, batteryIncluded);
                 
        //    }
        //    dt.Rows[0][colName] = value;
        //}
        private void ExportBatteries(DataSet xl, DataSet ds)
        {
            DataTable dt = xl.Tables["Sheet1"];
            DataTable dtBatteries = ds.Tables["vwBatteries"];
            int index = 0;
            foreach (DataRow dr in dtBatteries.Rows)
            {
                index++;
                string[] fields = new string[] {"Battery Type " , "Battery Quantity ", "Battery Size ", "Battery Included "};
                foreach (string field in fields)
                {
                    string fieldName = field + index.ToString();

                    if (!dt.Columns.Contains(fieldName))
                    {
                        dt.Columns.Add(fieldName, System.Type.GetType("System.String"));

                        DataTable hd = xl.Tables["SpreadsheetHeaders"];
                        hd.Rows.Add("Sheet1", dt.Columns.Count, fieldName);
                    }
                }

                string batteryType = "Alkaline";
                int batteryQuantity = pdm.GetValue<int>(dr, "BatteryQuantity", 0);
                string batterySize = pdm.GetValue<string>(dr, "BatterySize", string.Empty);
                bool batteryIncluded = pdm.GetValue<bool>(dr, "BatteryIncluded", false);

                dt.Rows[0]["Battery Type " + index.ToString()] = batteryType;
                dt.Rows[0]["Battery Quantity " + index.ToString()] = batteryQuantity;
                dt.Rows[0]["Battery Size " + index.ToString()] = batterySize;
                dt.Rows[0]["Battery Included " + index.ToString()] = batteryIncluded;

            }
        }

        private void ExportWarnings(ref string message, DataRow dr, string colName, string colText, string colSrc = "")
        {
            bool isChecked = pdm.GetValue<bool>(dr, colName, false);
            if (! isChecked) return;

            string newVal = colText;
            if (!string.IsNullOrEmpty(colSrc))
            {
                string add = pdm.GetValue<string>(dr, colSrc, string.Empty);
                if (!string.IsNullOrEmpty(add)) newVal += " - " + add;
            }

            if (!string.IsNullOrEmpty(message)) message += ", ";
            message += newVal;
        }

        private void ExportWarnings(DataSet xl, DataSet ds)
        {
            DataTable dt = xl.Tables["Sheet1"];
            DataTable dtPart = ds.Tables["vwQAPartInfos"];
            if (dtPart.Rows.Count == 0) return;
            DataRow dr = dtPart.Rows[0];


            string fieldName = "Additional Warnings";
            if (!dt.Columns.Contains(fieldName))
            {
                dt.Columns.Add(fieldName, System.Type.GetType("System.String"));

                DataTable hd = xl.Tables["SpreadsheetHeaders"];
                hd.Rows.Add("Sheet1", dt.Columns.Count, fieldName);
            }


            string allergens = string.Empty;
            string warnings = string.Empty;

            ExportWarnings(ref allergens, dr, "LatexAllergen", "Latex");
            ExportWarnings(ref allergens, dr, "PvcAllergen", "PVC");
            ExportWarnings(ref allergens, dr, "BPAAllergen", "BPA");
            ExportWarnings(ref allergens, dr, "PolycarbonatesAllergen", "Polycarbonates");
            ExportWarnings(ref allergens, dr, "TcepAllergen", "TCEP");
            ExportWarnings(ref allergens, dr, "PVCAllergen", "PVS");
            ExportWarnings(ref allergens, dr, "TolueneAllergen", "Toluene");
            ExportWarnings(ref allergens, dr, "Formaldehyde", "FormaldehydeAllergen");
            ExportWarnings(ref warnings, dr, "CEWarning", "CE");
            ExportWarnings(ref warnings, dr, "OwlPellets", "Owl Pellets");
            ExportWarnings(ref warnings, dr, "LatexBalloonsWarning", "Latex Balloons");
            ExportWarnings(ref warnings, dr, "NoBabyWarning", "No Baby");
            ExportWarnings(ref warnings, dr, "CordWarning", "Cord");
            ExportWarnings(ref warnings, dr, "MagnetWarning", "Magnet");
            ExportWarnings(ref warnings, dr, "SharpFunctionalPointWarning", "Sharp Functional Point");
            ExportWarnings(ref warnings, dr, "MarlbeInKit", "Marble In Kit");
            ExportWarnings(ref warnings, dr, "BallInKit", "Ball In Kit");
            ExportWarnings(ref warnings, dr, "Projectile", "Projectile");
            ExportWarnings(ref warnings, dr, "NotHumanConsumption", "Not for Human Consumption");
            ExportWarnings(ref warnings, dr, "NotSafetyProtection", "Not for Safety Protection");
            ExportWarnings(ref warnings, dr, "AdultSupervision", "Adult Supervision");
            ExportWarnings(ref warnings, dr, "LatextWarning", "Latex");
            ExportWarnings(ref warnings, dr, "CCCWarning", "CCC", "CCCDescription");
            ExportWarnings(ref warnings, dr, "WashBeforeUseWarning", "Wash Before Use", "WashBeforeUseDescription");
            ExportWarnings(ref warnings, dr, "ShellfishWarning", "Shell Fish", "ShellFishDescription");

            if (!string.IsNullOrEmpty(allergens)) allergens = "Allergens: " + allergens;
            if (!string.IsNullOrEmpty(warnings)) warnings = "Warnings: " + warnings;

            string message = allergens;
            if (!string.IsNullOrEmpty(warnings))
            {
                if (!string.IsNullOrEmpty(message)) message += "; ";
                message += warnings;
            }

            dt.Rows[0]["Additional Warnings"] = message;
            
        }

        private string FormatSku(string raw, out string magentoSku)
        {
            string retval = raw;
            if (retval.EndsWith("-")) retval = retval.Substring(0, retval.Length - 1);

            magentoSku = string.Empty;

            bool? prevIsChar = null;
            bool spaceInserted = false;

            for (int i = 0; i < retval.Length; i++)
            {
                string test = retval.Substring(i, 1);
                
                int intTest = 0;
                bool isNumber = Int32.TryParse(test, out intTest);

                if (prevIsChar.HasValue)
                {
                    if (!spaceInserted)
                    {
                        if (prevIsChar.Value && isNumber)
                        {
                            magentoSku += " ";
                            spaceInserted = true;
                        }
                    }
                }
                prevIsChar = !isNumber;

                magentoSku += test;
            }

            return retval;
        }

        public string Export(string exportFolder, int? year = null)
        {
            pdm.ThrowErrors = true;
            vmfg.ThrowErrors = true;
            dcms.ThrowErrors = true;

            DataSet ds = ExportGetTemplate();
            if (year == null) year = DateTime.Now.Year;

            DataSet dsParts = ExportFetchParts(year.Value);
            DataTable partIds = dsParts.Tables["Parts"].DefaultView.ToTable(true, "Id", "PartId");

            foreach (DataRow dr in partIds.Rows)
            {
                int partId = pdm.GetValue<int>(dr, "ID", -1);
                string sku = pdm.GetValue<string>(dr, "PartId", string.Empty);
                if (!string.IsNullOrEmpty(sku))
                {
                    string magentoSku = string.Empty;

                    DataSet dsPart = FilterPart(dsParts, partId, sku);
                    sku = FormatSku(sku, out magentoSku);

                    ExportAddSku(ds, sku);
                    ExportField(ds, "Magento Sku", System.Type.GetType("System.String"), magentoSku);

                    //page 1
                    ExportCustomerSku(ds, "ASIN", dsPart, "14329", "20330");
                    ExportField(ds, "Model Name", System.Type.GetType("System.String"), dsPart, "DCMS", "Description");
                    ExportField(ds, "Color", System.Type.GetType("System.String"), dsPart, "vwPartSpecifications", "ProductColor");
                    ExportField(ds, "What's in the box?", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "WhatsInBox");
                    ExportField(ds, "Includes Remote", System.Type.GetType("System.Boolean"), dsPart, "vwPartSpecifications", "IncludesRemote");
                    ExportField(ds, "Model Year", System.Type.GetType("System.String"), dsPart, "DCMS", "ModelYear");
                    ExportPlaceholder(ds, "Date released");
                    ExportField(ds, "Hazardous Material", System.Type.GetType("System.Boolean"), dsPart, "DCMS", "HazardousMaterial");
                    ExportField(ds, "Catalog Year", System.Type.GetType("System.String"), dsPart, "DCMS", "CatalogYear");
                    ExportField(ds, "Catalog Copy", System.Type.GetType("System.String"), dsPart, "vwMarketingInfos", "Copy");
                    ExportField(ds, "UPC", System.Type.GetType("System.String"), dsPart, "DCMS", "UPC");
                    ExportField(ds, "Product Title", System.Type.GetType("System.String"), dsPart, "DCMS", "Description");
                    ExportField(ds, "Brand Name", System.Type.GetType("System.String"), dsPart, "vwPartInfos", "CompanyName");
                    ExportField(ds, "Sub Brand", System.Type.GetType("System.String"), dsPart, "vwMarketingInfos", "SubBrand");
                    ExportField(ds, "Piece Count on Box", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "PieceCountOnBox");
                    ExportField(ds, "Min Target Age on Box", System.Type.GetType("System.String"), dsPart, "vwPartInfos", "MinAgeDisplay");
                    ExportField(ds, "Max Target Age", System.Type.GetType("System.String"), dsPart, "vwPartInfos", "MaxAgeDisplay");
                    ExportField(ds, "Min Grade", System.Type.GetType("System.String"), dsPart, "vwPartInfos", "MinGrade");
                    ExportField(ds, "Max Grade", System.Type.GetType("System.String"), dsPart, "vwPartInfos", "MaxGrade");
                    ExportField(ds, "Year", System.Type.GetType("System.String"), dsPart, "DCMS", "CatalogYear"); //"YearIntroducedText");

                    //Page 2
                    ExportBatteries(ds, dsPart);
                    //ExportField(ds, "Battery Count", System.Type.GetType("System.Int32"), dsPart, "vwBatteries", "BatteryQuantity");
                    //ExportField(ds, "Battery Type", System.Type.GetType("System.String"), dsPart, "vwBatteries", "BatterySize");
                    //ExportField(ds, "Battery Inlcuded", System.Type.GetType("System.Boolean"), dsPart, "vwBatteries", "BatteryIncluded");
                    ExportField(ds, "Guide", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "LinksToDocuments");
                    ExportGTIN(ds, "GTIN", dsPart);
                    ExportField(ds, "Main Image", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "MainImage");
                    ExportField(ds, "Image 2", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "Image2");
                    ExportField(ds, "Image 3", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "Image3");
                    ExportField(ds, "Image 4", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "Image4");
                    ExportField(ds, "Image 5", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "Image5");
                    ExportField(ds, "Image 6", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "Image6");
                    ExportField(ds, "Image 7", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "Image7");
                    ExportField(ds, "Videos", System.Type.GetType("System.String"), dsPart, "vwMarketingInfos", "Video");
                    ExportField(ds, "Current SRP", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "WHSALE_UNIT_COST");
                    ExportField(ds, "Current Net", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "UNIT_PRICE");
                    ExportField(ds, "Current Drop Ship", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "WHSALE_UNIT_COST");
                    ExportList(ds, "Awards", dsPart, "Awards", "Name");
                    ExportField(ds, "Port of Exit", System.Type.GetType("System.String"), dsPart, "vwPurchasingPartInfos", "PortOfOrigin");
                    ExportField(ds, "Country of Origin", System.Type.GetType("System.String"), dsPart, "vwPurchasingPartInfos", "CountryOfOrigin");
                    ExportField(ds, "HTS Code", System.Type.GetType("System.String"), dsPart, "vwPurchasingPartInfos", "HtsCode");
                    ExportField(ds, "Class Code", System.Type.GetType("System.String"), dsPart, "vwPurchasingPartInfos", "ClassCode");

                    //Page 3
                    ExportField(ds, "Cubic Ft/CPQ", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "CPQ");
                    ExportField(ds, "Case Quantity", System.Type.GetType("System.Int32"), dsPart, "DCMS", "CASE_QTY");
                    ExportField(ds, "Unit Weight", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "UNIT_WEIGHT");
                    ExportField(ds, "Case Weight", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "CASE_WEIGHT");
                    ExportField(ds, "Unit Length", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "UNIT_LENGTH");
                    ExportField(ds, "Unit Width", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "UNIT_WIDTH");
                    ExportField(ds, "Unit Height", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "UNIT_HEIGHT");
                    ExportField(ds, "Case Length", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "CASE_LENGTH");
                    ExportField(ds, "Case Width", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "CASE_WIDTH");
                    ExportField(ds, "Case Height", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "CASE_HEIGHT");
                    ExportField(ds, "Pallet Quantity", System.Type.GetType("System.Int32"), dsPart, "DCMS", "PALLET_QTY");
                    ExportField(ds, "Pallet Length", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "PALLET_LENGTH");
                    ExportField(ds, "Pallet Width", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "PALLET_WIDTH");
                    ExportField(ds, "Pallet Height", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "PALLET_HEIGHT");
                    ExportField(ds, "KG Unit Weight", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "UNIT_WEIGHT_KG");
                    ExportField(ds, "KG Case Weight", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "CASE_WEIGHT_KG");
                    ExportField(ds, "CM Unit Length", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "UNIT_LENGTH_CM");
                    ExportField(ds, "CM Unit Width", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "UNIT_WIDTH_CM");
                    ExportField(ds, "CM Unit Height", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "UNIT_HEIGHT_CM");
                    ExportField(ds, "CM Case Length", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "CASE_LENGTH_CM");
                    ExportField(ds, "CM Case Width", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "CASE_WIDTH_CM");
                    ExportField(ds, "CM Case Height", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "CASE_HEIGHT_CM");

                    //Page 4
                    ExportField(ds, "Temperature Controlled", System.Type.GetType("System.Boolean"), dsPart, "vwPartInfos", "TempuratureControlled");
                    ExportField(ds, "Item Registered with WERCS? (Yes/No)", System.Type.GetType("System.Boolean"), dsPart, "vwQAPartInfos", "WERCS");
                    ExportField(ds, "WERCS Product Classification", System.Type.GetType("System.String"), dsPart, "vwQAPartInfos", "WERCSClassification");
                    //ExportPlaceholder(ds, "Ethnicity Target");
                    //ExportPlaceholder(ds, "Genre");
                    ExportField(ds, "Power Source", System.Type.GetType("System.String"), dsPart, "vwPartSpecifications", "PowerSources");
                    ExportField(ds, "Powered Indicator? (Yes/No)", System.Type.GetType("System.Boolean"), dsPart, "vwPartSpecifications", "PowerIndicator");
                    ExportField(ds, "Pre-assembled/Requires Assembly", System.Type.GetType("System.String"), dsPart, "vwPartInfos", "AssemblyRequiredText");
                    ExportField(ds, "Type of Base Material", System.Type.GetType("System.String"), dsPart, "vwPartInfos", "BaseMaterial");
                    ExportField(ds, "Waterproof", System.Type.GetType("System.Boolean"), dsPart, "vwPartInfos", "Waterproof");
                    ExportField(ds, "On/Off Switch", System.Type.GetType("System.Boolean"), dsPart, "vwPartSpecifications", "PowerSwitch");
                    ExportField(ds, "Theme or Character", System.Type.GetType("System.String"), dsPart, "vwMarketingInfos", "ThemeOrCharacter");
                    ExportField(ds, "Other Type of Toy", System.Type.GetType("System.String"), dsPart, "vwMarketingInfos", "OtherTypeOfToy");
                    ExportField(ds, "Type of Toy", System.Type.GetType("System.String"), dsPart, "vwMarketingInfos", "TypeOfToy");
                    ExportField(ds, "# of Players", System.Type.GetType("System.String"), dsPart, "vwCreativeInfos", "NumberOfPlayers");
                    ExportField(ds, "Inner Pack", System.Type.GetType("System.Boolean"), dsPart, "DCMS", "INNER_PACK");
                    ExportPlaceholder(ds, "Inner Pack Dims");
                    ExportField(ds, "Inner Pack Per Master Pack", System.Type.GetType("System.Decimal"), dsPart, "DCMS", "INNER_PACKS_PER_MASTER");
                    ExportCustomerSku(ds, "TCIN", dsPart, "18804", "ED100216");
                    ExportCustomerSku(ds, "ToolID", dsPart, "45788");
                    ExportField(ds, "CA prop 65", System.Type.GetType("System.Boolean"), dsPart, "vwQAPartInfos", "Prop65Applies");
                    ExportField(ds, "Small Parts Warning", System.Type.GetType("System.Boolean"), dsPart, "vwQAPartInfos", "SmallPartsWarning");

                    //Page 5
                    ExportWarnings(ds, dsPart);
                    ExportField(ds, "Small Ball Warning", System.Type.GetType("System.Boolean"), dsPart, "vwQAPartInfos", "SmallBallWarning");
                    ExportField(ds, "Marble Warning", System.Type.GetType("System.Boolean"), dsPart, "vwQAPartInfos", "MarbleWarning");

                    //2019-06-04
                    if (sku.EndsWith("-DS")) ExportUnitPrice(ds, "Amazon Dropship Cost", dsPart, "49585");
                    ExportUnitPrice(ds, "Amazon School & Office Cost", dsPart, "20330");
                    ExportUnitPrice(ds, "Amazon Toy & Game Cost", dsPart, "14329");
                    ExportUnitPrice(ds, "Target Cost", dsPart, "18804", "ED100216");
                    ExportUnitPrice(ds, "Walmart Cost", dsPart, "45788");
                    ExportField(ds, "Amazon Vendor Code", System.Type.GetType("System.String"), dsPart, "CUSTOMER_PRICE", "VENDOR_CODE");

                }
            }



            string fileName = "Salsify" + Guid.NewGuid().ToString().ToUpper().Replace("-", "").Substring(0, 8) + ".xlsx";
            fileName = System.IO.Path.Combine(exportFolder, fileName);
            SvcLibrary.Excel xl = new SvcLibrary.Excel();
            xl.Save(fileName, ds);
            return fileName;
        }

    }
}
