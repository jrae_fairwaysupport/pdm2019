﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Salsify
{
    
    public abstract partial class DataMoverBase 
    {
     
        protected virtual DataSet LoadPart(int partId, params string[] tables)
        {

            DataSet ds = new DataSet();
            string tmpl8 = "SELECT * FROM {0} WHERE {1} = @P0";

            foreach (string table in tables)
            {
                string key = (table.Equals("Parts")) ? "Id" : "PartId";
                string sql = string.Format(tmpl8, table, key);

                switch (table)
                {
                    case "vwBatteries":
                        sql += " AND BatteriesFor = 'Part' ORDER BY RecordId";
                        break;
                    default:
                        break;
                }

                DataTable dt = pdm.ExecuteReader(sql, partId);
                dt.TableName = table;
                ds.Tables.Add(dt);
            }

            string sku = "jtrtmp";
            DataTable parts = ds.Tables["Parts"];
            if (parts != null)
            {
                if (parts.Rows.Count > 0) sku = pdm.GetValue<string>(parts.Rows[0], "PartId", sku);
            }

            string partTable = entityId.Equals("LRH") ? "VMLREI.LEARNINGRESOURCES.COM" : "h2mvmfg.learningresources.com";
            string qry = "SELECT 1 AS UNIT_QTY, E.LENGTH AS UNIT_LENGTH, E.WIDTH AS UNIT_WIDTH, E.HEIGHT AS UNIT_HEIGHT, S.WEIGHT AS UNIT_WEIGHT, "
                       + "P.CASE_QTY, C.LENGTH AS CASE_LENGTH, C.WIDTH AS CASE_WIDTH, C.HEIGHT AS CASE_HEIGHT, S.CASE_WEIGHT, PLT.PACK_QTY AS PALLET_QTY, "
                       + "PLT.LENGTH AS PALLET_LENGTH, PLT.WIDTH AS PALLET_WIDTH, PLT.HEIGHT AS PALLET_HEIGHT, ROUND(NVL(C.LENGTH, 0) * NVL(C.WIDTH, 0) * NVL(C.HEIGHT, 0) / 1728, 3) AS CPQ, "
                       + "CASE WHEN COALESCE(P.USER_4, 'EMPTY') <> 'EMPTY' THEN 1 ELSE 0 END AS INNER_PACK, 'NOT YET IMPLEMENTED' AS INNER_PACK_DIMS "
                       + "FROM SKU S LEFT JOIN Part@" + partTable + " P ON S.ID = P.ID LEFT JOIN SKU_PACK E ON E.SKU_ID = S.ID AND E.PACK_ID = 'EA' "
                       + "LEFT JOIN SKU_PACK C ON C.SKU_ID = S.ID AND C.PACK_ID = 'Cases' LEFT JOIN SKU_PACK PLT ON PLT.SKU_ID = S.ID AND PLT.PACK_ID = 'Pallets' "
                       + "WHERE S.ID = @P0";

            DataTable dcmsData = dcms.ExecuteReader(qry, sku);
            dcmsData.TableName = "DCMS";
            ds.Tables.Add(dcmsData);



            return ds;

        }

        

        public virtual DataSet FindPartIdBySku(string sku, out int id)
        {
            string sql = "SELECT ID FROM Parts WHERE PartId = @P0";
            id = pdm.FetchSingleValue<int>(sql, "ID", sku);
            if (id > 0) return LoadPart(id);
            return null;
        }

        //Ideally should be in SvcLibrary
        protected Dictionary<string, string> BuildMap(DataSet ds)
        {
            Dictionary<string, string> retval = new Dictionary<string, string>();

            DataTable ws = ds.Tables["Sheet1"];
            DataRow dr = ws.Rows[0];

            DataTable cols = ds.Tables["SpreadsheetHeaders"];

            for (int i = 0; i < ws.Columns.Count; i++)
            {
                string columnName = dr[i].ToString();
                string newColName = columnName;
                bool included = ws.Columns.Contains(columnName);
                int index = i;

                if (!included)
                {
                    index = -1;
                    DataRow[] arr = cols.Select(string.Format("Worksheet = 'Sheet1' AND ColumnName = '{0}'", columnName.Trim().Replace("'", "''")));
                    if (arr.Length > 0)
                    {
                        index = (int)arr[0]["Index"];
                        newColName = ws.Columns[index].ColumnName;
                    }
                }

                //may be duplicates...dunno why...only grab first
                try { retval.Add(columnName, newColName); }
                catch { }
            }

            return retval;
        }

        protected void ImportField(Dictionary<string, string> colMap, Dictionary<string, TableFields> tables, DataSet ds, DataRow dr, string tableName, string salsifyField, string pdmField, string pdmTab, string format = "string")
        {
            if (!ds.Tables.Contains(tableName)) return;
            bool hasData = ds.Tables[tableName].Rows.Count > 0;


            TableFields table = null;
            if (tables.ContainsKey(tableName))
            {
                table = tables[tableName];
            }
            else
            {
                table = new TableFields();
                tables.Add(tableName, table);
            }

            string raw = pdm.GetValue<string>(dr, colMap[salsifyField]);
            FieldChange field = new FieldChange(null, null);
            field.tabName = pdmTab;


            switch (format)
            {
                case "int":
                    if (hasData)
                    {
                        if (!ds.Tables[tableName].Rows[0].IsNull(pdmField)) field.oldValue = pdm.GetValue<int>(ds.Tables[tableName].Rows[0], pdmField, 0);
                    }

                    int test = 0;
                    if (Int32.TryParse(raw, out test)) field.newValue = test;
                    break;
                case "dec":
                    if (hasData)
                    {
                        if (!ds.Tables[tableName].Rows[0].IsNull(pdmField)) field.oldValue = pdm.GetValue<decimal>(ds.Tables[tableName].Rows[0], pdmField, 0);
                    }


                    decimal decTest = 0;
                    if (decimal.TryParse(raw, out decTest)) field.newValue = decTest;
                    break;
                case "bool":
                    if (hasData)
                    {
                        if (!ds.Tables[tableName].Rows[0].IsNull(pdmField)) field.oldValue = pdm.GetValue<bool>(ds.Tables[tableName].Rows[0], pdmField, false);
                    }

                    if (!string.IsNullOrEmpty(raw))
                    {
                        bool bTest = false;
                        raw = raw.ToLower(); //True False
                        if (raw.Equals("1")) raw = "true";
                        if (raw.Equals("0")) raw = "false";
                        if (raw.Equals("y")) raw = "true";
                        if (raw.Equals("n")) raw = "false";
                        if (raw.Equals("yes")) raw = "true";
                        if (raw.Equals("n")) raw = "false";
                        if (bool.TryParse(raw.ToLower(), out bTest))
                        {
                            field.newValue = bTest;
                        }
                    }
                    break;
                default: //string
                    if (hasData)
                    {
                        field.oldValue = pdm.GetValue<string>(ds.Tables[tableName].Rows[0], pdmField);
                    }


                    if (!dr.IsNull(colMap[salsifyField]))
                    {
                        if (pdmField.Equals("Description"))
                        {
                            raw = raw.ToUpper();
                            if (raw.Length > 40) raw = raw.Substring(0, 40);
                        }
                        field.newValue = raw;
                    }
                    break;
            }
            table.AddField(pdmField, field);

        }

        protected void ImportFixAssembly(TableFields table)
        {
            string assemblyRequired = (string)table.Fields["AssemblyRequired"].newValue;
            if (string.IsNullOrEmpty(assemblyRequired)) assemblyRequired = "";


            if (assemblyRequired.Equals("Pre-assembled"))
                table.Fields["AssemblyRequired"].newValue = false;
            else if (assemblyRequired.Equals("Requires Assembly"))
                table.Fields["AssemblyRequired"].newValue = true;
            else
                table.Fields["AssemblyRequired"].newValue = null;
        }

        protected void ImportConvertAgeToGrade(TableFields table, DataTable dt, string ageField)
        {
            string ageKey = ageField + "Age";
            string gradeKey = ageField + "Grade";
            bool hasData = dt.Rows.Count > 0;

            if (!table.Fields.ContainsKey(ageKey)) return;

            decimal? age = (decimal?)table.Fields[ageKey].newValue;
            if (age == null)
            {
                FieldChange newbie = new FieldChange(hasData ? dt.Rows[0][gradeKey] : null, "No Grade");
                newbie.tabName = "Product Development.Part Information";
                table.AddField(gradeKey, newbie);
                return;
            }


            string[] grades = new string[] {"Toddler", "Pre-K", "Kindergarten", "First Grade", "Second Grade", "Third Grade",
                                            "Fourth Grade", "Fifth Grade", "Sixth Grade", "Seventh Grade", "Eighth Grade", "No Grade"}; //,
            //"Ninth Grade", "Tenth Grade", "Eleventh Grade", "Twelfth Grade", "No Grade"};

            if (age == 0) age = 99;

            if (age < 4)
                age = 0;
            else
                age = age - 3;

            //index value, not age
            if (age > 11) age = 11;

            FieldChange nb = new FieldChange(hasData ? dt.Rows[0][gradeKey] : null, grades[(int)age]);
            nb.tabName = "Product Development.Part Information";
            table.AddField(gradeKey, nb);
        }

        protected void ImportFixCompany(TableFields table)
        {
            string raw = (string)table.Fields["Company"].newValue;
            if (raw == null) raw = "";

            if (raw.Equals("Learning Resources"))
                table.Fields["Company"].newValue = "LR";
            else if (raw.Equals("Educational Insights"))
                table.Fields["Company"].newValue = "EI";
            else
                table.Fields["Company"].newValue = null;
        }

        protected void Battery_Add(string userId, int partId, string batteryFor, string size, int qty, bool included)
        {
            int recordId = pdm.InsertRecord("Batteries", "RecordId", "PartId", partId, "BatteriesFor", batteryFor, "BatterySize", size, "BatteryQuantity", qty, "BatteryIncluded", included);

            string msg = string.Format("Added {0} Battery: {0}, {1}, {2}", size, qty, (included ? "Included" : "Not Included"), batteryFor);

            History history = new History(pdm);
            history.Log(partId, userId, "Part Specifications", "Batteries", string.Empty, msg);
        }

        public void Battery_Delete(string userId, int recordId, int partId, string batteryFor, string size, int qty, bool included)
        {
            //our workaround until we can clean up the db design
            if (recordId == -1)
            {
                //we're in the specification database
                string sql = "UPDATE PartSpecifications SET BatterySize = NULL, BatteryQuantity = NULL, BatteriesIncluded = NULL WHERE PartId = @P0";
                pdm.ExecuteCommand(sql, partId);
            }
            else
            {
                string sql = "DELETE FROM Batteries WHERE RecordId = @P0";
                pdm.ExecuteCommand(sql, recordId);
            }

            string msg = string.Format("Deleted {0} Battery: {1}, {2}, {3}", batteryFor, size, qty, (included ? "Included" : "Not Included"));
            History history = new History(pdm);
            history.Log(partId, userId, "Part Specifications", "Batteries", string.Empty, msg);


        }


        //right now this is a singleton, but need to support multiple
        protected void ImportSaveBatteries(int partId, DataTable dt, TableFields table)
        {
            string size = (string)table.Fields["BatterySize"].newValue;
            int qty = 0;
            Int32.TryParse((string)table.Fields["BatteryQuantity"].newValue, out qty);
            bool included = false;
            bool.TryParse((string)table.Fields["BatteryIncluded"].newValue, out included);
            if (string.IsNullOrEmpty(size) || qty == 0) return;

            History history = new History(pdm);
            foreach (DataRow dr in dt.Rows)
            {
                int recordId = pdm.GetValue<int>(dr, "RecordId", -1);
                string batteriesFor = pdm.GetValue<string>(dr, "BatteriesFor", string.Empty);
                string batterySize = pdm.GetValue<string>(dr, "BatterySize", string.Empty);
                int batteryQuantity = pdm.GetValue<int>(dr, "BatteryQuantity", 0);
                bool batteryIncluded = pdm.GetValue<bool>(dr, "BatteryIncluded", false);
                Battery_Delete("Salsify Import", recordId, partId, batteriesFor, batterySize, batteryQuantity, batteryIncluded);
            }

            Battery_Add("Salsify Import", partId, "Part", size, qty, included);


        }


        protected void ImportSave(int partId, DataTable dt, Dictionary<string, TableFields> tables)
        {
            string src = dt.TableName;
            string tgt = src;
            if (tgt.StartsWith("vw")) tgt = tgt.Substring(2);

            if (!tables.ContainsKey(src)) return;
            TableFields table = tables[src];

            if (src.Equals("vwBatteries"))
            {
                ImportSaveBatteries(partId, dt, table);
                return;
            }


            if (src.Equals("Parts"))
            {
                FieldChange salsifyUpload = new FieldChange(null, DateTime.Now);
                salsifyUpload.tabName = "Header";
                table.AddField("SalsifyUpload", salsifyUpload);
            }
            History history = new History(pdm);
            history.Log(partId, "Salsify Import", table.Fields);

            string partIdKey = src.Equals("Parts") ? "ID" : "PartId";
            table.AddField("ModifiedDate", null, DateTime.Now);
            table.AddField(partIdKey, null, partId);

            if (dt.Rows.Count == 0)
            {
                table.AddField("CreatedDate", null, DateTime.Now);
                pdm.InsertRecord(tgt, partIdKey, table.GetParametersAsArray(true));
            }
            else
            {
                pdm.UpdateRecord(tgt, partIdKey, partId, table.GetParametersAsArray(true, partIdKey));
            }
        }


        public void Import(string filePath)
        {
            History history = new History(pdm);

            SvcLibrary.Excel xl = new SvcLibrary.Excel();
            xl.Load(filePath);
            xl.ConvertFirstRowToHeaders("Sheet1");
            DataSet dsx = xl.SpreadSheet;
            Dictionary<string, string> colMap = BuildMap(dsx);

            DataTable sheet1 = dsx.Tables["Sheet1"];

            Dictionary<string, int> columns = new Dictionary<string, int>();
            DataTable xlCols = dsx.Tables["ColumnHeaders"];

            DataTable skus = sheet1.DefaultView.ToTable(true, "SKU");
            Import(sheet1, colMap, skus);
        }

        protected virtual void Import(DataTable sheet1, Dictionary<string, string> colMap, DataTable skus, params string[] pdmTables)
        {
            pdm.ThrowErrors = true;
            DataSet dsParts = FetchParts(skus, "SKU", pdmTables);
            DataTable dtParts = dsParts.Tables["Parts"];

            foreach (DataRow dr in sheet1.Rows)
            {
                string sku = pdm.GetValue<string>(dr, "SKU", string.Empty);
                DataRow[] arr = dtParts.Select(string.Format("PartId = '{0}' OR PartId = '{0}-'", sku));
                if (arr.Length > 0)
                {
                    int partId = pdm.GetValue<int>(arr[0], "Id", -1);
                    if (partId > 0)
                    {
                        DataSet ds = FilterPart(dsParts, partId, sku);
                        if (ds != null)
                        {
                            Dictionary<string, TableFields> tables = new Dictionary<string, TableFields>();
                            ImportField(colMap, tables, ds, dr, "Parts", "UPC", "UPCCode", "Header");
                            ImportField(colMap, tables, ds, dr, "Parts", "Product Title", "Description", "Header");
                            ImportField(colMap, tables, ds, dr, "vwPartInfos", "Brand Name", "Company", "Product Development.Part Information");
                            ImportField(colMap, tables, ds, dr, "vwMarketingInfos", "Sub Brand", "SubBrand", "Marketing");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "Piece Count", "PieceCountOnBox", "Creative", "int");
                            ImportField(colMap, tables, ds, dr, "vwPartSpecifications", "Color", "ProductColor", "Product Development.Specifications");
                            ImportField(colMap, tables, ds, dr, "vwMarketingInfos", "Keywords", "Keywords", "Marketing");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "Guide", "LinksToDocuments", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "What's in the box?", "WhatsInBox", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwPurchasingPartInfos", "Port of Exit", "PortOfOrigin", "Purchasing.Part Information");
                            ImportField(colMap, tables, ds, dr, "vwPurchasingPartInfos", "Country of Origin", "CountryOfOrigin", "Purchasing.Part Information");
                            ImportField(colMap, tables, ds, dr, "vwPurchasingPartInfos", "HTS Code", "HTSCode", "Purchasing.Part Information");
                            ImportField(colMap, tables, ds, dr, "vwPurchasingPartInfos", "Class Code", "ClassCode", "Purchasing.Part Information");
                            ImportField(colMap, tables, ds, dr, "vwPartInfos", "Pre-assembled/Requires Assembly", "AssemblyRequired", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "Main Image", "MainImage", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "Image 2", "Image2", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "Image 3", "Image3", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "Image 4", "Image4", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "Image 5", "Image5", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "Image 6", "Image6", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwCreativeInfos", "Image 7", "Image7", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwMarketingInfos", "Videos", "Video", "Creative");
                            ImportField(colMap, tables, ds, dr, "vwMarketingInfos", "Catalog Copy", "Copy", "Marketing");
                            ImportField(colMap, tables, ds, dr, "vwPartSpecifications", "Batteries", "Batteries", "Product Development.Specifications", "bool");
                            ImportField(colMap, tables, ds, dr, "vwBatteries", "Battery Count", "BatteryQuantity", "Product Development.Specifications");
                            ImportField(colMap, tables, ds, dr, "vwBatteries", "Battery Type", "BatterySize", "Product Development.Specifications");
                            ImportField(colMap, tables, ds, dr, "vwBatteries", "Battery Included", "BatteryIncluded", "Product Development.Specifications");
                            ImportField(colMap, tables, ds, dr, "vwPartInfos", "Min Target Age", "MinAge", "Product Development.Part Information", "dec");
                            ImportField(colMap, tables, ds, dr, "vwPartInfos", "Max Target Age", "MaxAge", "Product Development.Part Information", "dec");
                            //ImportField(colMap, tables, ds, dr, "DCMS", "Inner Pack", "INNER_PACK", "Product Development.Weights And Measures", "bool");
                            //SKU	External Product ID How to Play	Subject Keywords	Web Guide	Small Parts Warning	Additional Warnings	Small Ball Warning	Marble Warning	Number of Items	2018 SRP	2018 NET	2018 Drop Ship	Image 8	Image 9	Image 10	Image 11	Image 12	Image 13	Image 14	Image 15	Image 16	Image 17 Catalog Copy Bullet 1	Catalog Copy Bullet 2	Catalog Copy Bullet 3	Catalog Copy Bullet 4	Catalog Copy Bullet 5	Catalog Copy Bullet 6	TCIN ToolID Description - LR.com	Product Title - LR.com	URL	Case Pack Quantity	Cost (Last order)	Direct Website Long Description	QTY (Last order)	Status	Warnings	Weight	What is Included in the Box	salsify:parent_id	salsify:profile_asset_id 

                            TableFields table = tables["vwPartInfos"];
                            ImportFixAssembly(table);
                            ImportConvertAgeToGrade(table, ds.Tables["vwPartInfos"], "Min");
                            ImportConvertAgeToGrade(table, ds.Tables["vwPartInfos"], "Max");
                            ImportFixCompany(table);

                            table = tables["vwPartSpecifications"];
                            bool? raw = (bool?)table.Fields["Batteries"].newValue;
                            if (raw.HasValue)
                            {
                                if (raw.Value) table.AddField("Electronic", (ds.Tables["vwPartSpecifications"].Rows.Count > 0 ? ds.Tables["vwPartSpecifications"].Rows[0]["Electronic"] : null), raw.Value);
                            }

                            foreach (DataTable dt in ds.Tables) ImportSave(partId, dt, tables);
                        }


                    }
                }
            }

        }
    }
}
