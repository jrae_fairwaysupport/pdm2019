﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Salsify
{
    
    public abstract partial class DataMoverBase 
    {
        protected string environment { get; set; }
        protected string entityId { get; set; }

        protected SvcLibrary.DBHandler pdm { get; set; }
        protected SvcLibrary.DBHandler vmfg { get; set; }
        protected SvcLibrary.DBHandler dcms { get; set; }


        public DataMoverBase(string Environment)
        {
            environment = Environment;
        }


        protected DataSet FetchParts(DataTable skus, string sourceCol, params string[] tables)
        {
            DataSet ds = new DataSet();

            List<string> skuList = pdm.ConvertToInstring(skus, sourceCol, true, "jtrtmp");

            string sql = "SELECT * FROM Parts WHERE PartId IN ({0}) ";
            DataTable parts = pdm.MergeInstrings(sql, skuList);
            parts.TableName = "Parts";
            ds.Tables.Add(parts);
            List<string> idList = pdm.ConvertToInstring(parts, "Id", false, "-1");

            foreach (string table in tables)
            {
                sql = "SELECT * FROM " + table + " WHERE PartId IN ({0})";

                switch (table)
                {
                    case "vwBatteries":
                        sql += " AND BatteriesFor = 'Part' ORDER BY PartId, RecordId";
                        break;
                    default:
                        break;
                }

                DataTable dt = pdm.MergeInstrings(sql, idList);
                dt.TableName = table;
                ds.Tables.Add(dt);
            }

            string partTable = entityId.Equals("LRH") ? "VMLREI.LEARNINGRESOURCES.COM" : "h2mvmfg.learningresources.com";
            string qry = "SELECT P.ID AS PART_ID, 1 AS UNIT_QTY, E.LENGTH AS UNIT_LENGTH, E.WIDTH AS UNIT_WIDTH, E.HEIGHT AS UNIT_HEIGHT, S.WEIGHT AS UNIT_WEIGHT, "
                       + "P.CASE_QTY, C.LENGTH AS CASE_LENGTH, C.WIDTH AS CASE_WIDTH, C.HEIGHT AS CASE_HEIGHT, S.CASE_WEIGHT, PLT.PACK_QTY AS PALLET_QTY, "
                       + "PLT.LENGTH AS PALLET_LENGTH, PLT.WIDTH AS PALLET_WIDTH, PLT.HEIGHT AS PALLET_HEIGHT, ROUND(NVL(C.LENGTH, 0) * NVL(C.WIDTH, 0) * NVL(C.HEIGHT, 0) / 1728, 3) AS CPQ, "
                       //+ "CASE WHEN COALESCE(P.USER_4, 'EMPTY') <> 'EMPTY' THEN 1 ELSE 0 END AS INNER_PACK, 'NOT YET IMPLEMENTED' AS INNER_PACK_DIMS, "
                       + "CAST(COALESCE(P.USER_4, '1') AS NUMBER) AS INNER_PACK, "
                       + "CASE WHEN COALESCE(P.USER_4, '1') IN ('0', '1') THEN P.CASE_QTY ELSE  ROUND(P.CASE_QTY / CAST(COALESCE(P.USER_4, '1') AS NUMBER), 2) END AS INNER_PACKS_PER_MASTER, "
                       + "P.WHSALE_UNIT_COST, P.UNIT_PRICE, S.WEIGHT * 0.453592 AS UNIT_WEIGHT_KG, S.CASE_WEIGHT * 0.453592 AS CASE_WEIGHT_KG, "
                       + "C.LENGTH * 2.54 AS CASE_LENGTH_CM, C.WIDTH * 2.54 AS CASE_WIDTH_CM, C.HEIGHT * 2.54 AS CASE_HEIGHT_CM, "
                       + "E.LENGTH * 2.54 AS UNIT_LENGTH_CM, E.WIDTH * 2.54 AS UNIT_WIDTH_CM, E.HEIGHT * 2.54 AS UNIT_HEIGHT_CM, "
                       + "P.ID as SKU, P.USER_1 AS UPC, P.USER_9 AS ModelYear, P.USER_9 AS CatalogYear, P.USER_9 AS Year, 0 AS HazardousMaterial, P.DEF_ORIG_COUNTRY as PortOfExit, "
                       + "MFG.COUNTRY_MFG AS CountryOfOrigin, P.DESCRIPTION, P.PRODUCT_CODE AS BrandName "
                       + "FROM SKU S LEFT JOIN Part@" + partTable + " P ON S.ID = P.ID LEFT JOIN SKU_PACK E ON E.SKU_ID = S.ID AND E.PACK_ID = 'EA' "
                       + "LEFT JOIN SKU_PACK C ON C.SKU_ID = S.ID AND C.PACK_ID = 'Cases' LEFT JOIN SKU_PACK PLT ON PLT.SKU_ID = S.ID AND PLT.PACK_ID = 'Pallets' "
                       + "LEFT JOIN LR_PART_MFG@" + partTable + " MFG ON P.ID = MFG.PART_ID "
                       + "WHERE S.ID IN ({0})";

            DataTable dcmsData = dcms.MergeInstrings(qry, skuList);
            dcmsData.TableName = "DCMS";
            ds.Tables.Add(dcmsData);


            return ds;

        }

        protected DataSet FilterPart(DataSet dsParts, int partId, string sku)
        {
            DataSet ds = new DataSet();
            foreach (DataTable dt in dsParts.Tables)
            {
                DataTable newbie = dt.Clone();

                string filter = (dt.TableName.Equals("Parts") ? "ID" : "PartId") + " = " + partId.ToString();
                if (dt.TableName.Equals("DCMS")) filter = string.Format("PART_ID = '{0}' OR PART_ID = '{0}-'", sku);
                if (dt.TableName.Equals("CUSTOMER_PRICE")) filter = string.Format("PartId = '{0}' OR PartId = '{0}-'", sku);

                DataRow[] arr = dt.Select(filter);
                if (arr.Length > 0)
                {
                    newbie = arr.CopyToDataTable();
                }
                newbie.TableName = dt.TableName;
                ds.Tables.Add(newbie);
            }
            return ds;
        }

    }
}
