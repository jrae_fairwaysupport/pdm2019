﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Salsify
{
    
    class DataMoverH2M : DataMoverBase 
    {

        public DataMoverH2M(string Environment) : base(Environment)
        {
            entityId = "H2M";
            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            pdm = new SvcLibrary.DBHandler(config, null, environment, "H2M_PDM");
            dcms = new SvcLibrary.DBHandler(config, null, environment, "LRHDCMS11");
            vmfg = new SvcLibrary.DBHandler(config, null, environment, "VMLREI11");


        }
    }
}
