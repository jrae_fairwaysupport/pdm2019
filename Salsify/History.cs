using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;

namespace Salsify
{
    public class History
    {
        SvcLibrary.DBHandler pdm { get; set; }

        public History(SvcLibrary.DBHandler PDM)  
        {
            pdm = PDM;
        }


        public void Log(int partId, string userName, string tabName, string fieldName, string oldValue, string newValue)
        {
            if (string.IsNullOrEmpty(tabName)) tabName = "";
            if (tabName.Length > 100) tabName = tabName.Substring(0, 100);
            if (fieldName.Length > 50) fieldName = fieldName.Substring(0, 50);
            if (oldValue.Length > 400) oldValue = oldValue.Substring(0, 397) + "...";
            if (newValue.Length > 400) newValue = newValue.Substring(0, 397) + "...";

            pdm.InsertRecord("History", "PartId", "PartId", partId, "EventTime", DateTime.Now, "UserName", userName, "TabName", tabName, "FieldName", fieldName, "OldValue", oldValue, "NewValue", newValue);
        }


        public void Log(int partId, string userName, string tabName, Dictionary<string, FieldChange> fields, params string[] omits)
        {
            List<string> excludes = new List<string>();
            foreach (string omit in omits) excludes.Add(omit);

            foreach (string key in fields.Keys)
            {
                if (!excludes.Contains(key))
                {
                    string oldValue = fields[key].oldHistory;
                    string newValue = fields[key].newHistory;
                    if (!oldValue.Equals(newValue)) Log(partId, userName, tabName, key, oldValue, newValue);
                }
            }
        }

        public void Log(int partId, string userName, Dictionary<string, FieldChange> fields, params string[] omits)
        {
            List<string> excludes = new List<string>();
            foreach (string omit in omits) excludes.Add(omit);

            foreach (string key in fields.Keys)
            {
                if (!excludes.Contains(key))
                {
                    if (fields[key].HasChanged())
                    {
                        if (key.Equals("MinAge") || key.Equals("MaxAge")) break;
                        string tabName = fields[key].tabName;
                        Log(partId, userName, tabName, key, fields[key].oldHistory, fields[key].newHistory);
                    }
                }
            }
        }
    }
}
