﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YourCompany.PDM2019.Components;

namespace SalsifyUITester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Salsify.DataMover importer = new Salsify.DataMover("LRH", "Production");
            importer.Import(@"c:\junk\SalsifyExport.xlsx");
            textBox1.Text = "Done.";

        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            Salsify.DataMover exporter = new Salsify.DataMover("LRH", "Production");
            string fileName = exporter.Export(@"c:\junk");
            textBox1.Text = fileName;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SvcLibrary.Configuration config = new SvcLibrary.Configuration();
            SvcLibrary.DBHandler db = new SvcLibrary.DBHandler(config, null, "Development", "VMLREI11");
            db.ThrowErrors = true;

            string sku = "LER2629-P";
            string test = Guid.NewGuid().ToString();

            while (test.Length < 128000000) test += ", " + test + Guid.NewGuid().ToString();

            bool ok = false;
            //string sql = "INSERT INTO PART_BINARY (PART_ID, TYPE, BITS, BITS_LENGTH) VALUES (@P0, 'D', @P1, @P2)";
            string sql = "UPDATE PART_BINARY SET BITS = @P0, BITS_LENGTH = @P1 WHERE PART_ID = @P2";

            while (!ok)
            {
                byte[] bits = System.Text.Encoding.Default.GetBytes(test);
                int bitLength = bits.Length + 1;


                try
                {
                    db.ExecuteCommand(sql, bits, bitLength, sku);
                    ok = true;
                }
                catch (Exception ex)
                {
                    int len = test.Length - 1000;
                    if (len < 0)
                    {
                        int i = 0;
                    }
                    test = test.Substring(0, len);
                    ok = false;
                }
            }
        }
    }

}
